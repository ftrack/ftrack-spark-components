// :copyright: Copyright (c) 2017 ftrack
const fs = require("fs");
const path = require("path");

const INPUT_FILE_PATH = path.resolve(__dirname, "lib/i18n/messages/en.json");
const OUTPUT_FILE_PATH = path.resolve(__dirname, "source/story/en-TEST.json");

console.info(
  "Creating test translation:",
  INPUT_FILE_PATH,
  "->",
  OUTPUT_FILE_PATH
);

function translateToken(value) {
  const alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  const replacement = "åß¢Ðê£ghïjklmñðþq®§†µvwx¥zÄßÇÐÈ£GHÌJKLMñÖþQR§†ÚVW×¥Z";
  let result = "";
  for (let i = 0; i < value.length; i += 1) {
    const character = value[i];
    const alphabetIndex = alphabet.indexOf(character);
    result += replacement[alphabetIndex] || character;
  }
  return result;
}

fs.readFile(INPUT_FILE_PATH, (err, data) => {
  if (err) throw err;
  const messages = JSON.parse(data);
  for (const id of Object.keys(messages)) {
    // Message is not already translated
    if (messages[id][0] !== "~") {
      const tokens = messages[id].split(/(\{.+\})/g);
      const translatedMessage = tokens.reduce((result, token) => {
        if (token.includes("{")) {
          result += token; // eslint-disable-line no-param-reassign
        } else {
          result += translateToken(token); // eslint-disable-line no-param-reassign
        }
        return result;
      }, "");

      messages[id] = `~~${translatedMessage}~~`;
    }
  }

  fs.writeFile(
    OUTPUT_FILE_PATH,
    JSON.stringify(messages, null, 4),
    "utf-8",
    (err) => {
      if (err) throw err;
      console.info("Done");
    }
  );
});
