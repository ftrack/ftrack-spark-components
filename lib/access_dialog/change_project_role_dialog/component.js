"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.array.sort.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.string.includes.js");

require("core-js/modules/es7.array.includes.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactIntl = require("react-intl");

var _dialog = _interopRequireDefault(require("react-toolbox/lib/dialog"));

var _progress_bar = _interopRequireDefault(require("react-toolbox/lib/progress_bar"));

var _ftrackJavascriptApi = require("ftrack-javascript-api");

var _radio = require("react-toolbox/lib/radio");

var _checkbox = _interopRequireDefault(require("react-toolbox/lib/checkbox"));

var _safe_inject_intl = _interopRequireDefault(require("../../util/hoc/safe_inject_intl"));

var _with_session = _interopRequireDefault(require("../../util/hoc/with_session"));

var _style = _interopRequireDefault(require("./style.scss"));

var _dialog_theme = _interopRequireDefault(require("./dialog_theme.scss"));

var _radio_button_theme = _interopRequireDefault(require("./radio_button_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SELECTION_MULTI = "SELECTION_MULTI";
var messages = (0, _reactIntl.defineMessages)({
  "title-user": {
    "id": "ftrack-spark-components.change-access-dialog.title-user",
    "defaultMessage": "Roles for {selectedUser}"
  },
  description: {
    "id": "ftrack-spark-components.change-access-dialog.description",
    "defaultMessage": "Select which role {selectedUser} should have in this project."
  },
  save: {
    "id": "ftrack-spark-components.change-access-dialog.save",
    "defaultMessage": "Save"
  },
  cancel: {
    "id": "ftrack-spark-components.change-access-dialog.cancel",
    "defaultMessage": "Cancel"
  },
  "user-has-no-access": {
    "id": "ftrack-spark-components.change-access-dialog.user-has-no-access",
    "defaultMessage": "{selectedUser} does not have any roles that can be applied. New roles can be added from system settings."
  },
  "user-has-single-role": {
    "id": "ftrack-spark-components.change-access-dialog.user-has-single-role",
    "defaultMessage": "{selectedUser} only has the {roleName} role. Additional roles can be given from system settings."
  },
  "assignee-role": {
    "id": "ftrack-spark-components.change-access-dialog.assignee-role",
    "defaultMessage": "Added on projects with open access and assigned tasks"
  },
  "role-open-access": {
    "id": "ftrack-spark-components.change-access-dialog.role-open-access",
    "defaultMessage": "Added on projects with open access"
  }
});

function RoleLabel(_ref) {
  var name = _ref.name,
      type = _ref.type,
      isAllProjects = _ref.isAllProjects;

  if (type === "ASSIGNED") {
    return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      children: [name, /*#__PURE__*/(0, _jsxRuntime.jsx)("br", {}), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["assignee-role"]))]
    });
  } else if (isAllProjects) {
    return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      children: [name, /*#__PURE__*/(0, _jsxRuntime.jsx)("br", {}), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["role-open-access"]))]
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    children: name
  });
}

RoleLabel.propTypes = {
  name: _propTypes.default.string,
  type: _propTypes.default.string,
  isAllProjects: _propTypes.default.bool
};

var ChangeProjectRoleDialog = /*#__PURE__*/function (_Component) {
  _inherits(ChangeProjectRoleDialog, _Component);

  var _super = _createSuper(ChangeProjectRoleDialog);

  function ChangeProjectRoleDialog() {
    var _this;

    _classCallCheck(this, ChangeProjectRoleDialog);

    _this = _super.call(this);
    _this.state = {
      processing: true,
      saveEnabled: false,
      selectedUser: null,
      roles: [],
      currentProjectRoleIds: []
    };
    _this.handleChange = _this.handleChange.bind(_assertThisInitialized(_this));
    _this.handleSave = _this.handleSave.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(ChangeProjectRoleDialog, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var _this$props = this.props,
          userId = _this$props.userId,
          projectId = _this$props.projectId,
          session = _this$props.session;

      if (!projectId) {
        return;
      }

      session.call([_ftrackJavascriptApi.operation.query("select security_role.name, security_role.type, security_role_id, is_all_open_projects " + "from UserSecurityRole where user_id is \"".concat(userId, "\"")), _ftrackJavascriptApi.operation.query("select first_name from User where id is \"".concat(userId, "\"")), _ftrackJavascriptApi.operation.query("select is_private from Project where id is \"".concat(projectId, "\"")), _ftrackJavascriptApi.operation.query("select user_security_role.security_role_id from UserSecurityRoleProject where " + "project_id is \"".concat(projectId, "\" and user_security_role.user_id is \"").concat(userId, "\"")), _ftrackJavascriptApi.operation.query("select id from Appointment where resource_id is \"".concat(userId, "\" and context[TypedContext].project_id is \"").concat(projectId, "\" limit 1"))]).then(function (_ref2) {
        var _ref3 = _slicedToArray(_ref2, 5),
            userSecurityRolesResponse = _ref3[0],
            nameResponse = _ref3[1],
            projectResponse = _ref3[2],
            currentProjectRoleResponse = _ref3[3],
            appointmentResponse = _ref3[4];

        var firstName = nameResponse.data[0].first_name;
        var isPrivate = projectResponse.data[0].is_private;
        var isAssigned = appointmentResponse.data.length > 0;
        var currentProjectRoleIds = currentProjectRoleResponse.data.map(function (userSecurityRoleProject) {
          return userSecurityRoleProject.user_security_role.security_role_id;
        });
        var roles = userSecurityRolesResponse.data.map(function (userSecurityRole) {
          return {
            id: userSecurityRole.security_role_id,
            isAllProjects: !isPrivate && userSecurityRole.is_all_open_projects,
            name: userSecurityRole.security_role.name,
            type: userSecurityRole.security_role.type
          };
        }).sort(function (a, b) {
          return a.name.localeCompare(b.name);
        });
        var selectedRoleIds = roles.filter(function (role) {
          if (role.type === "ASSIGNED") {
            return !isPrivate && isAssigned;
          }

          return currentProjectRoleIds.includes(role.id) || role.isAllProjects;
        }).map(function (role) {
          return role.id;
        });
        var disabledRoleIds = roles.filter(function (role) {
          return role.type === "ASSIGNED" || role.isAllProjects;
        }).map(function (role) {
          return role.id;
        });

        _this2.setState({
          processing: false,
          selectedUser: firstName,
          currentProjectRoleIds: currentProjectRoleIds,
          selectedRoleIds: selectedRoleIds,
          disabledRoleIds: disabledRoleIds,
          selectionMode: SELECTION_MULTI,
          roles: roles
        });
      });
    }
  }, {
    key: "handleChange",
    value: function handleChange(roleId, selectionMode, value) {
      var _this$state = this.state,
          selectedRoleIds = _this$state.selectedRoleIds,
          disabledRoleIds = _this$state.disabledRoleIds;
      var nextSelectedRoleIds = selectedRoleIds;

      if (selectionMode === SELECTION_MULTI) {
        if (value) {
          if (!selectedRoleIds.includes(roleId)) {
            nextSelectedRoleIds = [].concat(_toConsumableArray(selectedRoleIds), [roleId]);
          }
        } else {
          nextSelectedRoleIds = selectedRoleIds.filter(function (selectedRoleId) {
            return selectedRoleId !== roleId;
          });
        }
      } else {
        var selectedAndDisabledRoleIds = disabledRoleIds.filter(function (disabledRoleId) {
          return selectedRoleIds.includes(disabledRoleId);
        });
        nextSelectedRoleIds = [].concat(_toConsumableArray(selectedAndDisabledRoleIds), [roleId]);
      }

      this.setState({
        selectedRoleIds: nextSelectedRoleIds,
        saveEnabled: selectedRoleIds.length > 0
      });
    }
  }, {
    key: "handleSave",
    value: function handleSave() {
      var _this3 = this;

      var _this$props2 = this.props,
          projectId = _this$props2.projectId,
          userId = _this$props2.userId,
          session = _this$props2.session;
      var _this$state2 = this.state,
          selectedRoleIds = _this$state2.selectedRoleIds,
          currentProjectRoleIds = _this$state2.currentProjectRoleIds;
      this.setState({
        processing: true
      });
      var revokedRolesList = currentProjectRoleIds.filter(function (currentRoleId) {
        return selectedRoleIds.includes(currentRoleId) === false;
      }).map(function (roleId) {
        return {
          action: "revoke_user_security_role_project",
          role_id: roleId,
          user_id: userId,
          project_id: projectId
        };
      });
      var grantedProjectList = selectedRoleIds.filter(function (selectedRoleId) {
        return currentProjectRoleIds.includes(selectedRoleId) === false;
      }).map(function (roleId) {
        return {
          action: "grant_user_security_role_project",
          role_id: roleId,
          user_id: userId,
          project_id: projectId
        };
      });
      session.call([].concat(_toConsumableArray(revokedRolesList), _toConsumableArray(grantedProjectList))).then(function () {
        _this3.setState({
          processing: false
        });

        _this3.props.onSave();
      }).catch(function (error) {
        _this3.setState({
          processing: false
        });

        if (_this3.props.onError) {
          _this3.props.onError(error);
        }
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      var onClose = this.props.onClose;
      var _this$state3 = this.state,
          processing = _this$state3.processing,
          selectedUser = _this$state3.selectedUser,
          roles = _this$state3.roles,
          saveEnabled = _this$state3.saveEnabled,
          selectedRoleIds = _this$state3.selectedRoleIds,
          disabledRoleIds = _this$state3.disabledRoleIds,
          selectionMode = _this$state3.selectionMode;
      var actions = [{
        label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.cancel)),
        onClick: onClose
      }, {
        label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.save)),
        primary: saveEnabled,
        onClick: this.handleSave,
        disabled: !saveEnabled
      }];
      var content;

      if (processing) {
        content = /*#__PURE__*/(0, _jsxRuntime.jsx)(_progress_bar.default, {
          className: _style.default.spinner,
          type: "circular",
          mode: "indeterminate"
        });
      } else if (roles.length === 0) {
        content = /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
          className: _style.default["no-user"],
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["user-has-no-access"]), {}, {
            values: {
              selectedUser: selectedUser
            }
          }))
        });
      } else if (roles.length === 1) {
        var roleName = /*#__PURE__*/(0, _jsxRuntime.jsx)("strong", {
          className: _style.default.name,
          children: roles[0].name
        });
        content = /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
          className: _style.default["no-user"],
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["user-has-single-role"]), {}, {
            values: {
              selectedUser: selectedUser,
              roleName: roleName
            }
          }))
        });
      } else {
        var SelectionComponent = selectionMode === SELECTION_MULTI ? _checkbox.default : _radio.RadioButton;
        content = /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default["radio-container"],
          children: roles.map(function (role) {
            var onChange = function onChange(value) {
              _this4.handleChange(role.id, selectionMode, value);
            };

            return /*#__PURE__*/(0, _jsxRuntime.jsx)(SelectionComponent, {
              theme: _radio_button_theme.default,
              className: _style.default["radio-buttons"],
              checked: selectedRoleIds.includes(role.id),
              disabled: disabledRoleIds.includes(role.id),
              label: /*#__PURE__*/(0, _jsxRuntime.jsx)(RoleLabel, _objectSpread({}, role)),
              onChange: onChange
            }, role.id);
          })
        });
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_dialog.default, {
        active: true,
        actions: actions,
        title: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["title-user"]), {}, {
          values: {
            selectedUser: selectedUser
          }
        })),
        onEscKeyDown: onClose,
        theme: _dialog_theme.default,
        type: "small",
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
          className: _style.default.description,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages.description), {}, {
            values: {
              selectedUser: selectedUser
            }
          }))
        }), content]
      });
    }
  }]);

  return ChangeProjectRoleDialog;
}(_react.Component);

ChangeProjectRoleDialog.propTypes = {
  onClose: _propTypes.default.func,
  userId: _propTypes.default.string.isRequired,
  projectId: _propTypes.default.string.isRequired,
  onSave: _propTypes.default.func,
  session: _propTypes.default.shape({
    call: _propTypes.default.func.isRequired
  }),
  onError: _propTypes.default.func
};

var _default = (0, _with_session.default)((0, _safe_inject_intl.default)(ChangeProjectRoleDialog));

exports.default = _default;