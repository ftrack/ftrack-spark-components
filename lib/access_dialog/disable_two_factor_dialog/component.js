"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _react = require("react");

var _dialog = _interopRequireDefault(require("react-toolbox/lib/dialog"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactIntl = require("react-intl");

var _two_factor_verification = _interopRequireDefault(require("../two_factor_dialog/two_factor_verification"));

var _with_session = _interopRequireDefault(require("../../util/hoc/with_session"));

var _icon_button = _interopRequireDefault(require("../../icon_button"));

var _safe_inject_intl = _interopRequireDefault(require("../../util/hoc/safe_inject_intl"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var messages = (0, _reactIntl.defineMessages)({
  title: {
    "id": "ftack-spark-components.disable-two-factor-dialog.title",
    "defaultMessage": "Disable two-factor authentication?"
  },
  subtitle: {
    "id": "ftrack-spark-components.disable-two-factor-dialog.subtitle",
    "defaultMessage": "Enter the 6-digit code received from your authentication application to disable two-factor authentication for your account."
  },
  "button-label": {
    "id": "ftrack-spark-components.disable-two-factor-dialog.button-label",
    "defaultMessage": "Verify code and disable"
  }
});

var DisableTwoFactorDialog = /*#__PURE__*/function (_Component) {
  _inherits(DisableTwoFactorDialog, _Component);

  var _super = _createSuper(DisableTwoFactorDialog);

  function DisableTwoFactorDialog() {
    var _this;

    _classCallCheck(this, DisableTwoFactorDialog);

    _this = _super.call(this);
    _this.state = {
      code: "",
      processing: false,
      validationError: false
    };
    _this.handleVerification = _this.handleVerification.bind(_assertThisInitialized(_this));
    _this.handleInputChanged = _this.handleInputChanged.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(DisableTwoFactorDialog, [{
    key: "handleInputChanged",
    value: function handleInputChanged(value) {
      this.setState({
        code: value
      });
    }
  }, {
    key: "handleVerification",
    value: function handleVerification() {
      var _this2 = this;

      var _this$props = this.props,
          session = _this$props.session,
          onDisabled = _this$props.onDisabled,
          onClose = _this$props.onClose;
      var code = this.state.code;
      this.setState({
        processing: true
      });
      session.call([{
        action: "disable_2fa",
        code: code
      }]).then(function (_ref) {
        var _ref2 = _slicedToArray(_ref, 1),
            response = _ref2[0];

        var success = response.data.success;

        _this2.setState({
          success: success,
          processing: false
        });

        onDisabled();
        onClose();
      }).catch(function (error) {
        if (error.errorCode === "2fa_invalid_code") {
          _this2.setState({
            processing: false,
            validationError: true
          });
        } else {
          if (_this2.props.onError) {
            _this2.props.onError(error);
          }

          _this2.setState({
            processing: false
          });
        }
      });
    }
  }, {
    key: "render",
    value: function render() {
      var onClose = this.props.onClose;
      var _this$state = this.state,
          validationError = _this$state.validationError,
          processing = _this$state.processing,
          code = _this$state.code;
      return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_dialog.default, {
        active: true,
        className: _style.default.dialog,
        title: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.title)),
        onEscKeyDown: onClose,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_icon_button.default, {
          icon: "clear",
          onClick: onClose,
          className: _style.default["close-button"]
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
          className: _style.default.subtitle,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.subtitle))
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_two_factor_verification.default, {
          value: code,
          onChange: this.handleInputChanged,
          onVerify: this.handleVerification,
          processing: processing,
          label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["button-label"])),
          error: validationError,
          primary: true
        })]
      });
    }
  }]);

  return DisableTwoFactorDialog;
}(_react.Component);

DisableTwoFactorDialog.propTypes = {
  onClose: _propTypes.default.func,
  onError: _propTypes.default.func,
  onDisabled: _propTypes.default.func,
  session: _propTypes.default.shape({
    call: _propTypes.default.func
  })
};

var _default = (0, _with_session.default)((0, _safe_inject_intl.default)(DisableTwoFactorDialog));

exports.default = _default;