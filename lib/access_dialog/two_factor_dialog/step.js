"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactIntl = require("react-intl");

var _heading = _interopRequireDefault(require("../../heading"));

var _safe_inject_intl = _interopRequireDefault(require("../../util/hoc/safe_inject_intl"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var messages = (0, _reactIntl.defineMessages)({
  label: {
    "id": "ftrack-spark-components.two-factor-dialog.label",
    "defaultMessage": "Step {number}"
  }
});

function Step(_ref) {
  var title = _ref.title,
      image = _ref.image,
      number = _ref.number,
      children = _ref.children;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("section", {
    className: _style.default.box,
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default["image-container"],
      children: image
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default["inner-text"],
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_heading.default, {
        variant: "label",
        color: "secondary",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages.label), {}, {
          values: {
            number: number
          }
        }))
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_heading.default, {
        variant: "title",
        color: "default",
        children: title
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
        children: children
      })]
    })]
  });
}

Step.propTypes = {
  image: _propTypes.default.node,
  children: _propTypes.default.node,
  number: _propTypes.default.number,
  title: _propTypes.default.node
};

var _default = (0, _safe_inject_intl.default)(Step);

exports.default = _default;