"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.weak-map.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _react = require("react");

var _reactIntl = require("react-intl");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _dialog = _interopRequireDefault(require("react-toolbox/lib/dialog"));

var _progress_bar = _interopRequireDefault(require("react-toolbox/lib/progress_bar"));

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _qrcode = _interopRequireDefault(require("qrcode.react"));

var _reveal = _interopRequireDefault(require("../../reveal"));

var _with_session = _interopRequireDefault(require("../../util/hoc/with_session"));

var _icon_button = _interopRequireDefault(require("../../icon_button"));

var _step = _interopRequireDefault(require("./step"));

var _formatted_code = _interopRequireWildcard(require("./formatted_code"));

var _configure_two_factor_success_dialog = _interopRequireDefault(require("./configure_two_factor_success_dialog"));

var _two_factor_verification = _interopRequireDefault(require("./two_factor_verification"));

var _safe_inject_intl = _interopRequireDefault(require("../../util/hoc/safe_inject_intl"));

var _style = _interopRequireDefault(require("./style.scss"));

var _main_dialog_theme = _interopRequireDefault(require("./main_dialog_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var messages = (0, _reactIntl.defineMessages)({
  title: {
    "id": "ftrack-spark.components.two-factor-dialog.title",
    "defaultMessage": "Enable two-factor authentication"
  },
  subtitle: {
    "id": "ftrack-spark-components.two-factor-dialog.subtitle",
    "defaultMessage": "Once configured, you'll need to enter a code from an authentication app in order to sign into your ftrack account. For more information about using two-factor authentication, visit our {helpPage}."
  },
  "code-instructions": {
    "id": "ftrack-spark-components.two-factor-dialog.code-instructions",
    "defaultMessage": "Insert this code into your application"
  },
  "verification-code-title": {
    "id": "ftrack-spark-components.two-factor-dialog.verification-code-title",
    "defaultMessage": "Enter verification code"
  },
  "verification-code-text": {
    "id": "ftrack-spark-components.two-factor-dialog.verification-code-text",
    "defaultMessage": "When the QR-code above has been scanned, enter the 6-digit verification code generated by the app."
  },
  "scan-code-title": {
    "id": "ftrack-spark-components.two-factor-dialog.scan-code-title",
    "defaultMessage": "Open the authentication app"
  },
  "scan-code-text": {
    "id": "ftrack-spark-components.two-factor-dialog.scan-code-text",
    "defaultMessage": "Open your newly downloaded app and scan the QR-code with your camera."
  },
  "scan-button-text": {
    "id": "ftrack-spark-components.two-factor-dialog.scan-button-text",
    "defaultMessage": "I can't scan this QR code"
  },
  "get-the-app-title": {
    "id": "ftrack-spark-components.two-factor-dialog.get-the-app-title",
    "defaultMessage": "Get the app"
  },
  "get-the-app-text": {
    "id": "ftrack-spark-components.two-factor-dialog.get-the-app-text",
    "defaultMessage": "Download and install either {applicationOne}, {applicationTwo} or {applicationThree} for your phone or tablet and get started right away."
  },
  "button-label": {
    "id": "ftrack-spark-components.two-factor-dialog.button-label",
    "defaultMessage": "Verify code and enable"
  },
  "generic-error": {
    "id": "ftrack-spark-components.two-factor-dialog.generic-error",
    "defaultMessage": "Something went wrong when loading the data. Try again."
  },
  "help-center": {
    "id": "ftrack-spark-components.two-factor-dialog.help-center",
    "defaultMessage": "help center"
  }
});

var TwoFactorDialog = /*#__PURE__*/function (_Component) {
  _inherits(TwoFactorDialog, _Component);

  var _super = _createSuper(TwoFactorDialog);

  function TwoFactorDialog() {
    var _this;

    _classCallCheck(this, TwoFactorDialog);

    _this = _super.call(this);
    _this.state = {
      secret: "",
      processing: false,
      loading: true,
      code: "",
      uri: "",
      validationError: false,
      success: false,
      genericError: false
    };
    _this.handleInputChanged = _this.handleInputChanged.bind(_assertThisInitialized(_this));
    _this.handleVerification = _this.handleVerification.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(TwoFactorDialog, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var session = this.props.session;
      session.call([{
        action: "generate_totp"
      }]).then(function (_ref) {
        var _ref2 = _slicedToArray(_ref, 1),
            generateResponse = _ref2[0];

        var secret = generateResponse.data.secret;
        var uri = "otpauth://totp/".concat(session.apiUser, "?secret=").concat(secret, "&issuer=ftrack");

        _this2.setState({
          secret: secret,
          uri: uri,
          loading: false
        });
      }).catch(function (error) {
        _this2.setState({
          loading: false,
          genericError: true
        });

        if (_this2.props.onError) {
          _this2.props.onError(error);
        }
      });
    }
  }, {
    key: "handleInputChanged",
    value: function handleInputChanged(value) {
      this.setState({
        code: value
      });
    }
  }, {
    key: "handleVerification",
    value: function handleVerification() {
      var _this3 = this;

      var _this$props = this.props,
          session = _this$props.session,
          onActivated = _this$props.onActivated;
      var _this$state = this.state,
          code = _this$state.code,
          secret = _this$state.secret;
      this.setState({
        processing: true
      });
      session.call([{
        action: "configure_totp",
        code: code,
        secret: secret
      }]).then(function (response) {
        var success = response[0].data.success;

        _this3.setState({
          success: success,
          processing: false
        });

        onActivated();
      }).catch(function (error) {
        _this3.setState({
          processing: false
        });

        if (error.errorCode === "2fa_invalid_code") {
          _this3.setState({
            processing: false,
            validationError: true
          });
        }

        if (_this3.props.onError) {
          _this3.props.onError(error);
        }
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$state2 = this.state,
          code = _this$state2.code,
          uri = _this$state2.uri,
          processing = _this$state2.processing,
          secret = _this$state2.secret,
          validationError = _this$state2.validationError,
          success = _this$state2.success,
          genericError = _this$state2.genericError,
          loading = _this$state2.loading;
      var HELP_CENTER_URL = "https://www.ftrack.com/help";
      var helpPage = /*#__PURE__*/(0, _jsxRuntime.jsx)("a", {
        className: _style.default.links,
        href: HELP_CENTER_URL,
        target: "_blank",
        rel: "noopener noreferrer",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["help-center"]))
      });
      var links = {
        one: /*#__PURE__*/(0, _jsxRuntime.jsx)("a", {
          className: _style.default.links,
          href: "https://authy.com/",
          target: "_blank",
          rel: "noopener noreferrer",
          children: "Authy"
        }),
        two: /*#__PURE__*/(0, _jsxRuntime.jsx)("a", {
          className: _style.default.links,
          href: "https://duo.com/",
          target: "_blank",
          rel: "noopener noreferrer",
          children: "Duo Mobile"
        }),
        three: /*#__PURE__*/(0, _jsxRuntime.jsx)("a", {
          className: _style.default.links,
          href: "https://www.google.com/landing/2step/",
          target: "_blank",
          rel: "noopener noreferrer",
          children: "Google Authenticator"
        })
      };

      if (success) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_configure_two_factor_success_dialog.default, {
          onClick: this.props.onClose
        });
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_dialog.default, {
        active: true,
        className: _style.default.wrapper,
        theme: _main_dialog_theme.default,
        title: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.title)),
        onEscKeyDown: this.props.onClose,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_icon_button.default, {
          icon: "clear",
          onClick: this.props.onClose,
          className: _style.default["close-button"]
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
          className: _style.default.subtitle,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages.subtitle), {}, {
            values: {
              helpPage: helpPage
            }
          }))
        }), genericError ? /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
          className: _style.default["generic-error"],
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["generic-error"]))
        }) : null, /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
          className: _style.default["inner-wrapper"],
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_step.default, {
            number: 1,
            title: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["get-the-app-title"])),
            image: /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
              value: "phonelink_lock",
              className: _style.default.icon
            }),
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["get-the-app-text"]), {}, {
              values: {
                applicationOne: links.one,
                applicationTwo: links.two,
                applicationThree: links.three
              }
            }))
          }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_step.default, {
            number: 2,
            title: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["scan-code-title"])),
            image: loading ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_progress_bar.default, {
              className: _style.default.spinner,
              mode: "indeterminate",
              type: "circular"
            }) : /*#__PURE__*/(0, _jsxRuntime.jsx)(_qrcode.default, {
              value: uri,
              includeMargin: true
            }),
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
              children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["scan-code-text"]))
            }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reveal.default, {
              label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["scan-button-text"])),
              className: _style.default["reveal-text"],
              children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
                className: _style.default["show-code"],
                children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["code-instructions"])), /*#__PURE__*/(0, _jsxRuntime.jsx)(_formatted_code.default, {
                  className: _style.default.code,
                  value: secret,
                  formatValue: _formatted_code.formatValueWithDashes
                })]
              })
            })]
          }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_step.default, {
            number: 3,
            title: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["verification-code-title"])),
            image: /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
              value: "smartphone",
              className: _style.default.icon
            }),
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
              children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["verification-code-text"]))
            }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_two_factor_verification.default, {
              value: code,
              onChange: this.handleInputChanged,
              onVerify: this.handleVerification,
              processing: processing,
              label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["button-label"])),
              error: validationError,
              primary: true
            })]
          })]
        })]
      });
    }
  }]);

  return TwoFactorDialog;
}(_react.Component);

TwoFactorDialog.propTypes = {
  onClose: _propTypes.default.func,
  onActivated: _propTypes.default.func,
  onError: _propTypes.default.func,
  session: _propTypes.default.shape({
    call: _propTypes.default.func
  })
};

var _default = (0, _with_session.default)((0, _safe_inject_intl.default)(TwoFactorDialog));

exports.default = _default;