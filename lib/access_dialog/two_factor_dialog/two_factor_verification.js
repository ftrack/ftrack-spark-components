"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reactIntl = require("react-intl");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _input = _interopRequireDefault(require("react-toolbox/lib/input"));

var _processing_button = _interopRequireDefault(require("../../processing_button"));

var _safe_inject_intl = _interopRequireDefault(require("../../util/hoc/safe_inject_intl"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["onChange", "value", "onVerify", "error", "processing", "label", "children"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var messages = (0, _reactIntl.defineMessages)({
  "input-text": {
    "id": "ftrack-spark-components.two-factor-dialog.input-text",
    "defaultMessage": "Insert code..."
  },
  error: {
    "id": "ftrack-spark-components.two-factor-dialog.error",
    "defaultMessage": "Wrong or expired code"
  },
  verifying: {
    "id": "ftrack-spark-components.two-factor-dialog.verifying",
    "defaultMessage": "Verifying..."
  }
});

function TwoFactorVerification(_ref) {
  var onChange = _ref.onChange,
      value = _ref.value,
      onVerify = _ref.onVerify,
      error = _ref.error,
      processing = _ref.processing,
      label = _ref.label,
      children = _ref.children,
      props = _objectWithoutProperties(_ref, _excluded);

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_input.default, {
      label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["input-text"])),
      value: value,
      className: _style.default.input,
      onChange: onChange,
      error: error ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.error)) : null,
      onKeyPress: function onKeyPress(e) {
        if (e.key === "Enter") {
          e.preventDefault();
          onVerify();
        }
      }
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_processing_button.default, _objectSpread(_objectSpread({
      processing: processing,
      onClick: onVerify,
      className: _style.default["verify-button"]
    }, props), {}, {
      children: processing ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.verifying)) : label
    })), children]
  });
}

TwoFactorVerification.propTypes = {
  onChange: _propTypes.default.func,
  onVerify: _propTypes.default.func,
  value: _propTypes.default.string,
  children: _propTypes.default.node,
  label: _propTypes.default.node,
  processing: _propTypes.default.bool,
  error: _propTypes.default.node
};

var _default = (0, _safe_inject_intl.default)(TwoFactorVerification);

exports.default = _default;