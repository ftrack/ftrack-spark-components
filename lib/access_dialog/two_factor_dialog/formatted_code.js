"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formatValueWithDashes = formatValueWithDashes;
exports.default = void 0;

require("core-js/modules/es6.regexp.constructor.js");

require("core-js/modules/es6.regexp.match.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _formatted_code = _interopRequireDefault(require("./formatted_code.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["formatValue", "value", "className"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var everyFourCharactersRegex = new RegExp(".{4}", "g");

function formatValueWithDashes(value) {
  return value.match(everyFourCharactersRegex).join("-");
}

function FormattedCode(_ref) {
  var formatValue = _ref.formatValue,
      _ref$value = _ref.value,
      value = _ref$value === void 0 ? "" : _ref$value,
      className = _ref.className,
      props = _objectWithoutProperties(_ref, _excluded);

  var classes = (0, _classnames.default)(_formatted_code.default.root, className);
  var formattedValue = value;

  if (formatValue) {
    formattedValue = formatValue(value);
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("pre", _objectSpread(_objectSpread({
    className: classes
  }, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("code", {
      children: formattedValue
    })
  }));
}

FormattedCode.propTypes = {
  formatValue: _propTypes.default.func,
  value: _propTypes.default.string,
  className: _propTypes.default.string
};
var _default = FormattedCode;
exports.default = _default;