"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.array.filter.js");

var _react = require("react");

var _dialog = _interopRequireDefault(require("react-toolbox/lib/dialog"));

var _list = require("react-toolbox/lib/list");

var _button = require("react-toolbox/lib/button");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _progress_bar = _interopRequireDefault(require("react-toolbox/lib/progress_bar"));

var _reactIntl = require("react-intl");

var _recompose = require("recompose");

var _entity_avatar = _interopRequireDefault(require("../../entity_avatar"));

var _with_session = _interopRequireDefault(require("../../util/hoc/with_session"));

var _resource_selector = _interopRequireDefault(require("../../selector/resource_selector"));

var _role_selector = _interopRequireDefault(require("./role_selector"));

var _style = _interopRequireDefault(require("./style.scss"));

var _safe_inject_intl = _interopRequireDefault(require("../../util/hoc/safe_inject_intl"));

var _list_item_theme = _interopRequireDefault(require("./list_item_theme.scss"));

var _dialog_theme = _interopRequireDefault(require("./dialog_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var messages = (0, _reactIntl.defineMessages)({
  save: {
    "id": "ftrack-spark-components.add-access-dialog.save",
    "defaultMessage": "Save"
  },
  cancel: {
    "id": "ftrack-spark-components.add-access-dialog.cancel",
    "defaultMessage": "Cancel"
  },
  title: {
    "id": "ftrack-spark-components.add-access-dialog.title",
    "defaultMessage": "Add user access to project"
  },
  "button-add": {
    "id": "ftrack-spark-components.add-access-dialog.button-add",
    "defaultMessage": "Add"
  },
  "search-user-description": {
    "id": "ftrack-spark-components.add-access-dialog.search-user-description",
    "defaultMessage": "Select a user and role to add access to this project"
  },
  "search-user-hint": {
    "id": "ftrack-spark-components.add-access-dialog.search-user-hint",
    "defaultMessage": "Search for a person by name or email address"
  },
  "user-label": {
    "id": "ftrack-spark-components.add-access-dialog.user-label",
    "defaultMessage": "User"
  },
  "roles-label": {
    "id": "ftrack-spark-components.add-access-dialog.roles-label",
    "defaultMessage": "Role"
  },
  "no-matching-users": {
    "id": "ftrack-spark-components.add-access-dialog.no-matching-users",
    "defaultMessage": "No matching users without access found"
  }
});
var UserSelector = (0, _recompose.compose)((0, _recompose.withProps)({
  users: true
}), _with_session.default)(_resource_selector.default);
var EntityAvatarWithSession = (0, _with_session.default)(_entity_avatar.default);

function UserList(_ref) {
  var users = _ref.users,
      onUserRemove = _ref.onUserRemove,
      enableRemove = _ref.enableRemove,
      userId = _ref.userId;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_list.List, {
    className: _style.default.userList,
    children: users.map(function (user) {
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_list.ListItem, {
        theme: _list_item_theme.default,
        className: _style.default.list,
        caption: "".concat(user.name) || "".concat(user.first_name, " ").concat(user.last_name),
        legend: "".concat(user.role.name),
        rightActions: enableRemove && user.id !== userId ? [/*#__PURE__*/(0, _jsxRuntime.jsx)(_button.IconButton, {
          icon: "clear" // eslint-disable-next-line react/jsx-no-bind
          ,
          onClick: function onClick() {
            return onUserRemove(user.id);
          }
        })] : [],
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(EntityAvatarWithSession, {
          entity: user,
          className: _style.default["avatar"]
        })
      }, "user-item-".concat(user.id));
    })
  });
}

UserList.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  users: _propTypes.default.array.isRequired,
  enableRemove: _propTypes.default.bool.isRequired,
  userId: _propTypes.default.string.isRequired,
  onUserRemove: _propTypes.default.func.isRequired
};

var AddProjectAccessDialog = /*#__PURE__*/function (_Component) {
  _inherits(AddProjectAccessDialog, _Component);

  var _super = _createSuper(AddProjectAccessDialog);

  function AddProjectAccessDialog() {
    var _this;

    _classCallCheck(this, AddProjectAccessDialog);

    _this = _super.call(this);
    _this.state = {
      processing: false,
      selectedRole: null,
      selectedUser: null,
      add: []
    };
    _this.onUserChange = _this.onUserChange.bind(_assertThisInitialized(_this));
    _this.onRoleChange = _this.onRoleChange.bind(_assertThisInitialized(_this));
    _this.onAddClicked = _this.onAddClicked.bind(_assertThisInitialized(_this));
    _this.onUserRemove = _this.onUserRemove.bind(_assertThisInitialized(_this));
    _this.handleSave = _this.handleSave.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(AddProjectAccessDialog, [{
    key: "onAddClicked",
    value: function onAddClicked() {
      var add = this.state.add;
      var _this$state = this.state,
          selectedUser = _this$state.selectedUser,
          selectedRole = _this$state.selectedRole;
      var nextAdd = [{
        thumbnail_id: selectedUser.data.thumbnailId,
        name: selectedUser.label,
        id: selectedUser.value,
        type: "user",
        role: {
          id: selectedRole.value,
          name: selectedRole.label
        }
      }].concat(_toConsumableArray(add));
      this.setState({
        selectedRole: null,
        selectedUser: null,
        add: nextAdd
      });
    }
  }, {
    key: "onUserRemove",
    value: function onUserRemove(userId) {
      var add = this.state.add;

      if (add.some(function (candidate) {
        return candidate.id === userId;
      })) {
        add = add.filter(function (candidate) {
          return candidate.id !== userId;
        });
      }

      this.setState({
        add: add
      });
    }
  }, {
    key: "onUserChange",
    value: function onUserChange(selectedUser) {
      var selectedUserId = selectedUser && selectedUser.value;
      this.setState({
        selectedUser: selectedUser,
        selectedUserId: selectedUserId
      });
    }
  }, {
    key: "onRoleChange",
    value: function onRoleChange(selectedRole) {
      this.setState({
        selectedRole: selectedRole
      });
    }
  }, {
    key: "handleSave",
    value: function handleSave() {
      var _this2 = this;

      var projectId = this.props.projectId;
      var add = this.state.add;
      this.setState({
        processing: true
      });
      var operations = add.map(function (user) {
        return {
          action: "grant_user_security_role_project",
          role_id: user.role.id,
          user_id: user.id,
          project_id: projectId
        };
      });
      this.props.session.call(operations).then(function () {
        _this2.setState({
          processing: false
        });

        _this2.props.onSave();
      }).catch(function (error) {
        _this2.setState({
          processing: false
        });

        if (_this2.props.onError) {
          _this2.props.onError(error);
        }
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          onClose = _this$props.onClose,
          session = _this$props.session,
          userId = _this$props.userId,
          projectId = _this$props.projectId;
      var _this$state2 = this.state,
          processing = _this$state2.processing,
          selectedUser = _this$state2.selectedUser,
          add = _this$state2.add,
          selectedUserId = _this$state2.selectedUserId,
          selectedRole = _this$state2.selectedRole;
      var pendingChanges = add.length > 0;
      var actions = [{
        label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.cancel)),
        onClick: onClose
      }, {
        label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.save)),
        disabled: !pendingChanges || processing,
        primary: pendingChanges,
        onClick: this.handleSave
      }];
      var userFilters = "id is_not \"".concat(userId, "\" and is_active is true and\n            id not_in (select id from User where user_security_roles.user_id is \"").concat(projectId, "\")");

      if (add.length) {
        var quotedAdd = add.map(function (item) {
          return "\"".concat(item.id, "\"");
        }).join(", ");
        userFilters = "".concat(userFilters, " and id not_in (").concat(quotedAdd, ")");
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_dialog.default, {
        active: true,
        actions: actions,
        theme: _dialog_theme.default,
        onEscKeyDown: onClose,
        title: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.title)),
        type: "small",
        children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
            className: _style.default["users-description"],
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["search-user-description"]))
          }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
            className: _style.default["users-fields"],
            children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
              className: _style.default["select-user-field"],
              children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("label", {
                htmlFor: "user",
                className: _style.default["select-users-label"],
                children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["user-label"]))
              }), /*#__PURE__*/(0, _jsxRuntime.jsx)(UserSelector, {
                name: "user",
                onFormattedValueChange: this.onUserChange,
                value: selectedUser,
                userFilters: userFilters,
                searchPromptText: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["no-matching-users"]))
              })]
            }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
              className: _style.default["select-roles-field"],
              children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("label", {
                htmlFor: "role",
                className: _style.default["select-roles-label"],
                children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["roles-label"]))
              }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_role_selector.default, {
                name: "role",
                userId: selectedUserId,
                onChange: this.onRoleChange,
                value: selectedRole,
                session: session
              })]
            }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_button.Button, {
              className: _style.default["add-button"],
              disabled: (selectedUser && selectedRole) === null,
              label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["button-add"])),
              onClick: this.onAddClicked
            })]
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
            className: _style.default["text-hint"],
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["search-user-hint"]))
          })]
        }, "entry-section"), processing ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_progress_bar.default, {
          className: _style.default.spinner,
          type: "circular",
          mode: "indeterminate"
        }) : /*#__PURE__*/(0, _jsxRuntime.jsx)(UserList, {
          enableRemove: true,
          users: add,
          onUserRemove: this.onUserRemove
        })]
      });
    }
  }]);

  return AddProjectAccessDialog;
}(_react.Component);

AddProjectAccessDialog.propTypes = {
  onClose: _propTypes.default.func,
  onSave: _propTypes.default.func,
  onError: _propTypes.default.func,
  projectId: _propTypes.default.string,
  session: _propTypes.default.shape({
    call: _propTypes.default.func.isRequired
  }),
  userId: _propTypes.default.string
};

var _default = (0, _with_session.default)((0, _safe_inject_intl.default)(AddProjectAccessDialog));

exports.default = _default;