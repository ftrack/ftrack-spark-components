"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _recompose = require("recompose");

var _button = _interopRequireDefault(require("react-toolbox/lib/button"));

var _reactIntl = require("react-intl");

var _safe_inject_intl = _interopRequireDefault(require("../../util/hoc/safe_inject_intl"));

var _with_session = _interopRequireDefault(require("../../util/hoc/with_session"));

var _heading = _interopRequireDefault(require("../../heading"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["children", "renderSelector", "session", "value", "onChange", "onButtonClick", "className", "disabled"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var messages = (0, _reactIntl.defineMessages)({
  add: {
    "id": "ftrack-spark-components.add-header.add",
    "defaultMessage": "Add"
  }
});

function AddHeader(_ref) {
  var children = _ref.children,
      renderSelector = _ref.renderSelector,
      session = _ref.session,
      value = _ref.value,
      onChange = _ref.onChange,
      onButtonClick = _ref.onButtonClick,
      className = _ref.className,
      disabled = _ref.disabled,
      props = _objectWithoutProperties(_ref, _excluded);

  var classes = (0, _classnames.default)(_style.default.header, _defineProperty({}, _style.default.headerDisabled, disabled), className);
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("header", _objectSpread(_objectSpread({
    className: classes
  }, props), {}, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_heading.default, {
      variant: "subheading",
      children: children
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default.addNewRow,
      children: [renderSelector({
        className: _style.default.addNewSelector,
        session: session,
        value: value,
        onFormattedValueChange: onChange
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_button.default, {
        onClick: onButtonClick,
        disabled: !value,
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.add))
      })]
    })]
  }));
}

AddHeader.propTypes = {
  children: _propTypes.default.node.isRequired,
  renderSelector: _propTypes.default.func.isRequired,
  session: _propTypes.default.object.isRequired,
  value: _propTypes.default.any,
  onChange: _propTypes.default.func.isRequired,
  onButtonClick: _propTypes.default.func.isRequired,
  className: _propTypes.default.string,
  disabled: _propTypes.default.bool
};

var _default = (0, _recompose.compose)(function (BaseComponent) {
  return (0, _safe_inject_intl.default)(BaseComponent);
}, _with_session.default, (0, _recompose.withState)("value", "onChange", null), (0, _recompose.withHandlers)({
  onButtonClick: function onButtonClick(props) {
    return function () {
      props.onChange(null);
      props.onAdd(props.value);
    };
  }
}))(AddHeader);

exports.default = _default;