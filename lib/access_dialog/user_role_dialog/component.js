"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.reflect.construct.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.array.find-index.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.sort.js");

require("core-js/modules/es6.array.find.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactIntl = require("react-intl");

var _dialog = _interopRequireDefault(require("react-toolbox/lib/dialog"));

var _list = require("react-toolbox/lib/list");

var _progress_bar = _interopRequireDefault(require("react-toolbox/lib/progress_bar"));

var _reactFlipMove = _interopRequireDefault(require("react-flip-move"));

var _empty_text = _interopRequireDefault(require("../../empty_text"));

var _selector = _interopRequireDefault(require("../../selector"));

var _project_selector = _interopRequireDefault(require("../../selector/project_selector"));

var _style = _interopRequireDefault(require("./style.scss"));

var _dialog_theme = _interopRequireDefault(require("./dialog_theme.scss"));

var _list_theme = _interopRequireDefault(require("./list_theme.scss"));

var _add_header = _interopRequireDefault(require("./add_header"));

var _constant = require("../../util/constant");

var _with_session = _interopRequireDefault(require("../../util/hoc/with_session"));

var _safe_inject_intl = _interopRequireDefault(require("../../util/hoc/safe_inject_intl"));

var _overlay = _interopRequireDefault(require("../../overlay"));

var _all_open_projects_list_item = _interopRequireDefault(require("./all_open_projects_list_item"));

var _project_list_item = _interopRequireDefault(require("./project_list_item"));

var _user_role_list_item = _interopRequireDefault(require("./user_role_list_item"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var messages = (0, _reactIntl.defineMessages)({
  title: {
    "id": "ftrack-spark-components.user-role-editor.title",
    "defaultMessage": "Manage roles for {userName}"
  },
  "security-roles-header": {
    "id": "ftrack-spark-components.user-role-editor.security-roles-header",
    "defaultMessage": "Roles"
  },
  "projects-header": {
    "id": "ftrack-spark-components.user-role-editor.projects-header",
    "defaultMessage": "Projects"
  },
  "action-cancel": {
    "id": "ftrack-spark-components.user-role-editor.action-cancel",
    "defaultMessage": "Cancel"
  },
  "action-save": {
    "id": "ftrack-spark-components.user-role-editor.save-changes",
    "defaultMessage": "Save changes"
  },
  "no-role-selected": {
    "id": "ftrack-spark-components.user-role-editor.no-role-selected",
    "defaultMessage": "Select a role to view associated projects"
  },
  "no-roles": {
    "id": "ftrack-spark-components.user-role-editor.no-roles",
    "defaultMessage": "{userName} has no roles, add one above."
  },
  "assigned-role": {
    "id": "ftrack-spark-components.user-role-editor.assigned-role",
    "defaultMessage": "The \"{roleName}\" role is automatically granted on any project with open access where {userName} is assigned to a task."
  },
  "error-message": {
    "id": "ftrack-spark-components.user-role-editor.error-message",
    "defaultMessage": "An error occurred"
  },
  "no-matching-projects": {
    "id": "ftrack-spark-components.user-role-editor.no-matching-projects",
    "defaultMessage": "No matching projects found"
  },
  "no-matching-private-projects": {
    "id": "ftrack-spark-components.user-role-editor.no-matching-private-projects",
    "defaultMessage": "No matching private projects found"
  },
  "no-matching-roles": {
    "id": "ftrack-spark-components.user-role-editor.no-matching-roles",
    "defaultMessage": "No roles found"
  }
});
/** Return list of persisted user roles combined with pending operations */

function getUserRoles(persistedUserRoles, operations) {
  var userRoles = _toConsumableArray(persistedUserRoles);

  operations.forEach(function (operation) {
    if (operation.action === "add_user_security_role") {
      userRoles.push(operation.data);
    } else if (operation.action === "remove_user_security_role") {
      userRoles = userRoles.filter(function (userRole) {
        return userRole.security_role.id !== operation.role_id;
      });
    }
  }); // Update project list for role

  userRoles = userRoles.map(function (userRole) {
    var nextUserRole = _objectSpread({}, userRole);

    operations.forEach(function (op) {
      if (op.role_id === nextUserRole.security_role.id) {
        if (op.action === "grant_user_security_role_project") {
          if (op.all_open_projects === true) {
            nextUserRole.is_all_projects = true;
          } else {
            nextUserRole.user_security_role_projects = [].concat(_toConsumableArray(nextUserRole.user_security_role_projects), [{
              project: op.data
            }]);
          }
        } else if (op.action === "revoke_user_security_role_project") {
          if (op.all_open_projects === true) {
            nextUserRole.is_all_projects = false;
          } else {
            var index = nextUserRole.user_security_role_projects.findIndex(function (candidate) {
              return candidate.project.id === op.project_id;
            });

            if (index !== -1) {
              nextUserRole.user_security_role_projects = [].concat(_toConsumableArray(nextUserRole.user_security_role_projects.slice(0, index)), _toConsumableArray(nextUserRole.user_security_role_projects.slice(index + 1)));
            }
          }
        }
      }
    });
    nextUserRole.user_security_role_projects = nextUserRole.user_security_role_projects.sort(function (a, b) {
      return a.project.full_name.localeCompare(b.project.full_name);
    });
    return nextUserRole;
  });
  userRoles = userRoles.sort(function (a, b) {
    return a.security_role.name.localeCompare(b.security_role.name);
  });
  return userRoles;
}
/** Return list of projects and isAllProjects flag for selected user role. */


function getProjects(userRoles, operations, selectedRoleId) {
  var selectedRole = userRoles.find(function (candidate) {
    return candidate.security_role.id === selectedRoleId;
  });
  var isAllProjects = false;
  var isRoleAssigned = false;
  var projects = [];

  if (selectedRole) {
    isAllProjects = selectedRole.is_all_projects;
    isRoleAssigned = selectedRole.security_role.type === "ASSIGNED";
    projects = selectedRole.user_security_role_projects.map(function (item) {
      return item.project;
    });
  }

  return {
    projects: projects,
    isAllProjects: isAllProjects,
    isRoleAssigned: isRoleAssigned
  };
}
/** Operation action which is the inverts each action */


var inverseActionForAction = {
  add_user_security_role: "remove_user_security_role",
  remove_user_security_role: "add_user_security_role",
  grant_user_security_role_project: "revoke_user_security_role_project",
  revoke_user_security_role_project: "grant_user_security_role_project"
};
/** Identifying keys for each operation identified by action */

var operationKeysForAction = {
  add_user_security_role: ["user_id", "role_id"],
  remove_user_security_role: ["user_id", "role_id"],
  grant_user_security_role_project: ["user_id", "role_id", "is_all_projects", "project_id"],
  revoke_user_security_role_project: ["user_id", "role_id", "is_all_projects", "project_id"]
};
/**
 * Return next operations list with *newOperation* added.
 *
 * If there is an identical operation in the list, ignore the new operation.
 *
 * If there is an inverse operation in the list, remove it instead of adding
 * the new operation.
 *
 * If removing a role, also remove any pending operations for that role.
 */

function getNextOperations(operations, newOperation) {
  var nextOperations = _toConsumableArray(operations);

  var action = newOperation.action;
  var inverseAction = inverseActionForAction[action];
  var keys = operationKeysForAction[action];

  var matchesOperation = function matchesOperation(testAction, a, b) {
    return a.action === testAction && keys.every(function (key) {
      return a[key] === b[key];
    });
  };

  var existingOperation = nextOperations.findIndex(function (operation) {
    return matchesOperation(newOperation.action, operation, newOperation);
  });
  var inverseOperationIndex = nextOperations.findIndex(function (operation) {
    return matchesOperation(inverseAction, operation, newOperation);
  });

  if (existingOperation !== -1) {// Ignore duplicate operations.
  } else if (inverseOperationIndex !== -1) {
    // Remove inverse operation instead of adding new.
    nextOperations.splice(inverseOperationIndex, 1);
  } else {
    // Add new operation
    nextOperations.push(newOperation); // Remove pending project operations for role when removing role.

    if (newOperation.action === "remove_user_security_role") {
      nextOperations = nextOperations.filter(function (candidate) {
        var isRoleProjectAction = candidate.action === "grant_user_security_role_project" || candidate.action === "revoke_user_security_role_project";
        var isRemovedRole = candidate.role_id === newOperation.role_id;
        return !(isRemovedRole && isRoleProjectAction);
      });
    }
  }

  return nextOperations;
}
/** User role dialog */


var UserRoleDialog = /*#__PURE__*/function (_Component) {
  _inherits(UserRoleDialog, _Component);

  var _super = _createSuper(UserRoleDialog);

  function UserRoleDialog(props) {
    var _this;

    _classCallCheck(this, UserRoleDialog);

    _this = _super.call(this, props);
    _this.handleAddRole = _this.handleAddRole.bind(_assertThisInitialized(_this));
    _this.handleSave = _this.handleSave.bind(_assertThisInitialized(_this));
    _this.initialState = {
      loading: true,
      data: [],
      roles: [],
      projects: [],
      operations: [],
      error: null,
      selectedRoleId: null
    };
    _this.state = _objectSpread(_objectSpread({}, _this.initialState), {}, {
      userName: ""
    });
    return _this;
  }

  _createClass(UserRoleDialog, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.reloadData();
    }
    /** Load data for userId and update state. */

  }, {
    key: "reloadData",
    value: function reloadData() {
      var _this2 = this;

      var _this$props = this.props,
          userId = _this$props.userId,
          session = _this$props.session;
      var attributes = ["is_all_projects", "security_role.name", "security_role.type", "user_security_role_projects.project.id", "user_security_role_projects.project.full_name", "user_security_role_projects.project.is_private"].join(",");
      session.query("select ".concat(attributes, " from UserSecurityRole where user_id is \"").concat(userId, "\" order by security_role.name")).then(function (response) {
        _this2.updateState(response.data, [], null);
      }).catch(function (error) {
        _this2.setState(_objectSpread(_objectSpread({}, _this2.initialState), {}, {
          loading: false,
          error: error
        }));
      });
      session.query("select first_name, last_name from User where id is \"".concat(userId, "\"")).then(function (response) {
        return _this2.setState({
          userName: "".concat(response.data[0].first_name, " ").concat(response.data[0].last_name)
        });
      });
    }
    /** When a role is selected, update state. */

  }, {
    key: "handleRoleSelected",
    value: function handleRoleSelected(userRole) {
      var selectedRoleId = userRole.security_role.id;
      this.updateState(this.state.data, this.state.operations, selectedRoleId);
    }
    /** Add *newOperation* and update state. */

  }, {
    key: "addOperation",
    value: function addOperation(newOperation, nextSelectedRoleId) {
      var _this$state = this.state,
          operations = _this$state.operations,
          data = _this$state.data;
      var selectedRoleId = typeof nextSelectedRoleId === "undefined" ? this.state.selectedRoleId : nextSelectedRoleId;
      var nextOperations = getNextOperations(operations, newOperation);
      this.updateState(data, nextOperations, selectedRoleId);
    }
    /** Update state with new operations. */

  }, {
    key: "updateState",
    value: function updateState(data, operations, selectedRoleId) {
      var roles = getUserRoles(data, operations);

      var _getProjects = getProjects(roles, operations, selectedRoleId),
          projects = _getProjects.projects,
          isAllProjects = _getProjects.isAllProjects,
          isRoleAssigned = _getProjects.isRoleAssigned;

      this.setState(_objectSpread(_objectSpread({}, this.initialState), {}, {
        loading: false,
        data: data,
        operations: operations,
        selectedRoleId: selectedRoleId,
        roles: roles,
        projects: projects,
        isAllProjects: isAllProjects,
        isRoleAssigned: isRoleAssigned
      }));
    }
  }, {
    key: "handleAddRole",
    value: function handleAddRole(value) {
      var roleId = value.data.id;
      this.addOperation({
        action: "add_user_security_role",
        role_id: roleId,
        user_id: this.props.userId,
        data: {
          id: roleId,
          is_all_projects: false,
          user_security_role_projects: [],
          security_role: _objectSpread({}, value.data)
        }
      }, roleId);
    }
  }, {
    key: "handleRemoveRole",
    value: function handleRemoveRole(userRole) {
      var roleId = userRole.security_role.id;
      var nextSelectedRoleId = this.state.selectedRoleId;

      if (this.state.selectedRoleId === roleId) {
        nextSelectedRoleId = null;
      }

      this.addOperation({
        action: "remove_user_security_role",
        role_id: roleId,
        user_id: this.props.userId
      }, nextSelectedRoleId);
    }
  }, {
    key: "handleIsAllProjectsChange",
    value: function handleIsAllProjectsChange(roleId, value) {
      if (value) {
        this.addOperation({
          action: "grant_user_security_role_project",
          role_id: roleId,
          user_id: this.props.userId,
          project_id: null,
          all_open_projects: true
        });
      } else {
        this.addOperation({
          action: "revoke_user_security_role_project",
          role_id: roleId,
          user_id: this.props.userId,
          project_id: null,
          all_open_projects: true
        });
      }
    }
  }, {
    key: "handleRevokeProject",
    value: function handleRevokeProject(roleId, projectId) {
      this.addOperation({
        action: "revoke_user_security_role_project",
        role_id: roleId,
        user_id: this.props.userId,
        project_id: projectId
      });
    }
  }, {
    key: "handleAddProject",
    value: function handleAddProject(roleId, project) {
      this.addOperation({
        action: "grant_user_security_role_project",
        role_id: roleId,
        user_id: this.props.userId,
        project_id: project.id,
        data: _objectSpread({}, project)
      });
    }
  }, {
    key: "handleSave",
    value: function handleSave() {
      var _this3 = this;

      var session = this.props.session;
      var operations = this.state.operations;
      this.setState({
        loading: true
      }); // Omit any unknown keys for API operations.

      var apiOperations = operations.map(function (_ref) {
        var action = _ref.action,
            role_id = _ref.role_id,
            user_id = _ref.user_id,
            project_id = _ref.project_id,
            all_open_projects = _ref.all_open_projects;
        return {
          action: action,
          role_id: role_id,
          user_id: user_id,
          project_id: project_id,
          all_open_projects: all_open_projects
        };
      });
      session.call(apiOperations).then(function (response) {
        return _this3.props.onSave(operations, response);
      }).catch(function (error) {
        return _this3.setState({
          loading: false,
          error: error
        });
      });
    }
  }, {
    key: "renderProjectsList",
    value: function renderProjectsList() {
      var _this4 = this;

      var _this$state2 = this.state,
          selectedRoleId = _this$state2.selectedRoleId,
          isRoleAssigned = _this$state2.isRoleAssigned,
          isAllProjects = _this$state2.isAllProjects,
          roles = _this$state2.roles,
          projects = _this$state2.projects,
          userName = _this$state2.userName;
      var selectedRole = roles.find(function (candidate) {
        return candidate.security_role.id === selectedRoleId;
      });

      if (!selectedRoleId) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_empty_text.default, {
          label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["no-role-selected"]))
        });
      }

      if (isRoleAssigned) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_empty_text.default, {
          label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["assigned-role"]), {}, {
            values: {
              userName: userName,
              roleName: selectedRole.security_role.name
            }
          }))
        });
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_list.List, {
        theme: _list_theme.default,
        children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactFlipMove.default, {
          duration: _constant.TRANSITION_DURATION,
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_all_open_projects_list_item.default, {
            value: isAllProjects,
            onChange: function onChange(value) {
              return _this4.handleIsAllProjectsChange(selectedRoleId, value);
            }
          }, "all_open_projects"), projects.map(function (project) {
            return /*#__PURE__*/(0, _jsxRuntime.jsx)(_project_list_item.default, {
              project: project,
              onDelete: function onDelete() {
                return _this4.handleRevokeProject(selectedRoleId, project.id);
              }
            }, project.id);
          })]
        })
      }, selectedRoleId);
    }
  }, {
    key: "renderUserRolesList",
    value: function renderUserRolesList() {
      var _this5 = this;

      var _this$state3 = this.state,
          roles = _this$state3.roles,
          selectedRoleId = _this$state3.selectedRoleId,
          userName = _this$state3.userName;

      if (!roles || !roles.length) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_list.List, {
          theme: _list_theme.default,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_empty_text.default, {
            label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["no-roles"]), {}, {
              values: {
                userName: userName
              }
            }))
          })
        });
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_list.List, {
        theme: _list_theme.default,
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactFlipMove.default, {
          duration: _constant.TRANSITION_DURATION,
          children: roles.map(function (userRole) {
            return /*#__PURE__*/(0, _jsxRuntime.jsx)(_user_role_list_item.default, {
              userRole: userRole,
              selectedRoleId: selectedRoleId,
              onSelect: function onSelect() {
                return _this5.handleRoleSelected(userRole);
              },
              onDelete: function onDelete() {
                return _this5.handleRemoveRole(userRole);
              }
            }, userRole.id);
          })
        })
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this6 = this;

      var classes = (0, _classnames.default)(_style.default.dialog, this.props.className);
      var intl = this.props.intl;
      var _this$state4 = this.state,
          roles = _this$state4.roles,
          projects = _this$state4.projects,
          operations = _this$state4.operations,
          selectedRoleId = _this$state4.selectedRoleId,
          userName = _this$state4.userName,
          isAllProjects = _this$state4.isAllProjects;
      var dialogActions = [{
        label: intl.formatMessage(messages["action-cancel"]),
        onClick: this.props.onClose
      }, {
        label: intl.formatMessage(messages["action-save"]),
        primary: true,
        disabled: !operations.length,
        onClick: this.handleSave
      }];
      var roleFilter = 'type != "API"';

      if (roles.length) {
        var rolesIds = roles.map(function (role) {
          return "\"".concat(role.security_role.id, "\"");
        });
        roleFilter += " and id not_in (".concat(rolesIds.join(","), ")");
      }

      var searchPromptText = isAllProjects ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["no-matching-private-projects"])) : /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["no-matching-projects"]));
      var projectFilter = isAllProjects ? "is_private is True" : "";

      if (projects.length) {
        if (projectFilter) {
          projectFilter += " and ";
        }

        projectFilter += "id not_in (".concat(projects.map(function (project) {
          return "\"".concat(project.id, "\"");
        }).join(","), ")");
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_dialog.default, {
        active: true,
        title: intl.formatMessage(messages.title, {
          userName: userName
        }),
        theme: _dialog_theme.default,
        className: classes,
        actions: dialogActions,
        onEscKeyDown: this.props.onClose,
        children: [this.state.loading ? /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default.sectionWrapper,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_progress_bar.default, {
            className: _style.default.loadingProgress,
            type: "circular",
            mode: "indeterminate"
          })
        }) : null, this.state.error ? /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default.sectionWrapper,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_overlay.default, {
            active: true,
            icon: "warning",
            message: intl.formatMessage(messages["error-message"]),
            details: this.state.error.message,
            dismissable: true,
            onDismss: function onDismss() {
              return _this6.setState({
                error: null
              });
            }
          })
        }) : null, !this.state.loading && !this.state.error ? /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
          className: _style.default.sectionWrapper,
          children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("section", {
            className: _style.default.section,
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_add_header.default, {
              renderSelector: function renderSelector(props) {
                return /*#__PURE__*/(0, _jsxRuntime.jsx)(_selector.default, _objectSpread({
                  entityType: "SecurityRole",
                  extraFields: ["type"],
                  baseFilter: roleFilter,
                  searchPromptText: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["no-matching-roles"]))
                }, props));
              },
              onAdd: this.handleAddRole,
              children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["security-roles-header"]))
            }), this.renderUserRolesList()]
          }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("section", {
            className: _style.default.section,
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_add_header.default, {
              disabled: !selectedRoleId,
              renderSelector: function renderSelector(props) {
                return /*#__PURE__*/(0, _jsxRuntime.jsx)(_project_selector.default, _objectSpread({
                  extraFields: ["color", "is_private"],
                  baseFilter: projectFilter,
                  searchPromptText: searchPromptText
                }, props));
              },
              onAdd: function onAdd(item) {
                return _this6.handleAddProject(selectedRoleId, item.data);
              },
              children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["projects-header"]))
            }), this.renderProjectsList()]
          })]
        }) : null]
      });
    }
  }]);

  return UserRoleDialog;
}(_react.Component);

UserRoleDialog.propTypes = {
  userId: _propTypes.default.string.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  session: _propTypes.default.object.isRequired,
  intl: _reactIntl.intlShape,
  className: _propTypes.default.string,
  onSave: _propTypes.default.func.isRequired,
  onClose: _propTypes.default.func
};
UserRoleDialog.defaultProps = {};

var _default = (0, _safe_inject_intl.default)((0, _with_session.default)(UserRoleDialog));

exports.default = _default;