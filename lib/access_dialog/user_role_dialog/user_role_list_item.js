"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.messages = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactIntl = require("react-intl");

var _list = require("react-toolbox/lib/list");

var _button = require("react-toolbox/lib/button");

var _list_item_hover_action_theme = _interopRequireDefault(require("./list_item_hover_action_theme.scss"));

var _safe_inject_intl = _interopRequireDefault(require("../../util/hoc/safe_inject_intl"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var messages = (0, _reactIntl.defineMessages)({
  "role-all-open-projects": {
    "id": "ftrack-spark-components.user-role-editor.role-all-open-projects",
    "defaultMessage": "All open projects"
  },
  "assigned-projects": {
    "id": "ftrack-spark-components.user-role-editor.assigned-projects",
    "defaultMessage": "Open projects with assigned tasks"
  }
}); // react-flip-move requires children to be class components.
// eslint-disable-next-line react/prefer-stateless-function

exports.messages = messages;

var UserRoleListItem = /*#__PURE__*/function (_Component) {
  _inherits(UserRoleListItem, _Component);

  var _super = _createSuper(UserRoleListItem);

  function UserRoleListItem() {
    _classCallCheck(this, UserRoleListItem);

    return _super.apply(this, arguments);
  }

  _createClass(UserRoleListItem, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          intl = _this$props.intl,
          selectedRoleId = _this$props.selectedRoleId,
          userRole = _this$props.userRole,
          onSelect = _this$props.onSelect,
          onDelete = _this$props.onDelete;
      var isSelected = selectedRoleId === userRole.security_role.id;
      var legendItems = userRole.user_security_role_projects.map(function (item) {
        return item.project.full_name;
      });

      if (userRole.is_all_projects) {
        legendItems.unshift(intl.formatMessage(messages["role-all-open-projects"]));
      }

      if (userRole.security_role.type === "ASSIGNED") {
        legendItems.unshift(intl.formatMessage(messages["assigned-projects"]));
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_list.ListItem, {
        theme: _list_item_hover_action_theme.default,
        className: isSelected ? _list_item_hover_action_theme.default.selected : null,
        leftIcon: "supervisor_account",
        selectable: true,
        caption: userRole.security_role.name,
        legend: legendItems.join(", "),
        onClick: onSelect,
        rightActions: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_button.IconButton, {
          icon: "delete",
          onClick: onDelete
        })]
      });
    }
  }]);

  return UserRoleListItem;
}(_react.Component);

UserRoleListItem.propTypes = {
  intl: _reactIntl.intlShape,
  selectedRoleId: _propTypes.default.string,
  userRole: _propTypes.default.shape({
    id: _propTypes.default.string.isRequired,
    security_role: _propTypes.default.PropTypes.shape({
      name: _propTypes.default.string.isRequired,
      id: _propTypes.default.string.isRequired
    }).isRequired,
    user_security_role_projects: _propTypes.default.PropTypes.arrayOf(_propTypes.default.PropTypes.shape({
      project: _propTypes.default.PropTypes.shape({
        full_name: _propTypes.default.string.isRequired
      }).isRequired
    }))
  }).isRequired,
  onSelect: _propTypes.default.func.isRequired,
  onDelete: _propTypes.default.func.isRequired
};
UserRoleListItem.defaultProps = {};

var _default = (0, _safe_inject_intl.default)(UserRoleListItem);

exports.default = _default;