"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.weak-map.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _react = require("react");

var _dialog = _interopRequireDefault(require("react-toolbox/lib/dialog"));

var _reactIntl = require("react-intl");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _with_session = _interopRequireDefault(require("../../util/hoc/with_session"));

var _safe_inject_intl = _interopRequireDefault(require("../../util/hoc/safe_inject_intl"));

var _icon_button = _interopRequireDefault(require("../../icon_button"));

var _two_factor_verification = _interopRequireDefault(require("../two_factor_dialog/two_factor_verification"));

var _formatted_code = _interopRequireWildcard(require("../two_factor_dialog/formatted_code"));

var _dialog_theme = _interopRequireDefault(require("./dialog_theme.scss"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var messages = (0, _reactIntl.defineMessages)({
  title: {
    "id": "ftrack-spark.components.generate-backup-codes-dialog.title",
    "defaultMessage": "Two-factor authentication backup codes"
  },
  subtitle: {
    "id": "ftrack-spark-components.generate-backup-codes-dialog.subtitle",
    "defaultMessage": "If you lose access to your authentication device, you can use one of these backup codes to sign in to your account. Make a copy of these codes and store it somewhere safe."
  },
  description: {
    "id": "ftrack-spark-components.generate-backup-codes-dialog.description",
    "defaultMessage": "Each code may be used only once and generating new codes will revoke any previously generated codes."
  },
  "copy-button": {
    "id": "ftrack-spark-components.generate-backup-codes-dialog.copy-button",
    "defaultMessage": "Copy codes"
  },
  "print-button": {
    "id": "ftrack-spark-components.generate-backup-codes-dialog.print-button",
    "defaultMessage": "Print codes"
  },
  "button-label-default": {
    "id": "ftrack-spark-components.generate-backup-codes-dialog.button-label-default",
    "defaultMessage": "Verify and get codes"
  },
  "copied-code": {
    "id": "ftrack-spark-components.generate-backup-codes-dialog.copied-code",
    "defaultMessage": "Codes copied to clipboard"
  },
  "print-window-title": {
    "id": "ftrack-spark-components.generate-backup-codes-dialog.print-window-title",
    "defaultMessage": "Two-factor authentication backup codes for ftrack"
  }
});

function isClipboardApiSupported() {
  return window.navigator && window.navigator.clipboard && window.navigator.clipboard.writeText;
}

var GenerateBackupCodesDialog = /*#__PURE__*/function (_Component) {
  _inherits(GenerateBackupCodesDialog, _Component);

  var _super = _createSuper(GenerateBackupCodesDialog);

  function GenerateBackupCodesDialog() {
    var _this;

    _classCallCheck(this, GenerateBackupCodesDialog);

    _this = _super.call(this);
    _this.state = {
      processing: false,
      initialState: true,
      code: "",
      oneTimeCodes: "",
      error: false,
      copiedCode: null,
      printCode: false
    };
    _this.handleInputChanged = _this.handleInputChanged.bind(_assertThisInitialized(_this));
    _this.handleVerification = _this.handleVerification.bind(_assertThisInitialized(_this));
    _this.handleCopyingOfCodes = _this.handleCopyingOfCodes.bind(_assertThisInitialized(_this));
    _this.handlePrintingOfCodes = _this.handlePrintingOfCodes.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(GenerateBackupCodesDialog, [{
    key: "handleInputChanged",
    value: function handleInputChanged(value) {
      this.setState({
        code: value
      });
    }
  }, {
    key: "handleCopyingOfCodes",
    value: function handleCopyingOfCodes() {
      var _this2 = this;

      var oneTimeCodes = this.state.oneTimeCodes;

      try {
        window.navigator.clipboard.writeText(oneTimeCodes).then(function () {
          _this2.setState({
            copiedCode: true
          });
        });
      } catch (error) {
        this.setState({
          copiedCode: false
        });
      }
    }
  }, {
    key: "handlePrintingOfCodes",
    value: function handlePrintingOfCodes() {
      var intl = this.props.intl;
      var oneTimeCodes = this.state.oneTimeCodes;
      var myWindow = window.open("", "_blank");
      var title = intl.formatMessage(messages["print-window-title"]);
      myWindow.document.write("<html>\n            <head><title>".concat(title, "</title></head>\n            <body>\n            <h2>").concat(title, "</h2>\n            <pre>").concat(oneTimeCodes, "</pre>\n            </body>\n            </html>"));
      myWindow.document.close();
      myWindow.focus();
      myWindow.print();
      myWindow.close();
      this.setState({
        printCode: true
      });
    }
  }, {
    key: "handleVerification",
    value: function handleVerification() {
      var _this3 = this;

      var session = this.props.session;
      var code = this.state.code;
      this.setState({
        processing: true
      });
      session.call([{
        action: "configure_otp",
        code: code
      }]).then(function (_ref) {
        var _ref2 = _slicedToArray(_ref, 1),
            response = _ref2[0];

        var codes = response.data.codes.map(function (x) {
          return x;
        });
        var oneTimeCodes = codes.reduce(function (result, item, index) {
          var formattedItem = (0, _formatted_code.formatValueWithDashes)(item);

          if (index % 2 === 0) {
            return "".concat(result).concat(formattedItem, "\t");
          }

          return "".concat(result).concat(formattedItem, "\n");
        }, "");

        _this3.setState({
          oneTimeCodes: oneTimeCodes,
          processing: false,
          initialState: false
        });
      }).catch(function (error) {
        if (error.errorCode === "2fa_invalid_code") {
          _this3.setState({
            processing: false,
            error: true
          });
        } else {
          if (_this3.props.onError) {
            _this3.props.onError(error);
          }

          _this3.setState({
            processing: false
          });
        }
      });
    }
  }, {
    key: "render",
    value: function render() {
      var onClose = this.props.onClose;
      var _this$state = this.state,
          initialState = _this$state.initialState,
          oneTimeCodes = _this$state.oneTimeCodes,
          processing = _this$state.processing,
          error = _this$state.error,
          code = _this$state.code,
          copiedCode = _this$state.copiedCode;
      var actions = [];

      if (initialState === false) {
        if (isClipboardApiSupported()) {
          actions.push({
            label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["copy-button"])),
            onClick: this.handleCopyingOfCodes,
            primary: true
          });
        }

        actions.push({
          label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["print-button"])),
          onClick: this.handlePrintingOfCodes,
          primary: true
        });
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_dialog.default, {
        active: true,
        title: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.title)),
        className: _style.default.dialog,
        actions: actions,
        theme: _dialog_theme.default,
        onEscKeyDown: onClose,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_icon_button.default, {
          icon: "clear",
          onClick: onClose,
          className: _style.default["close-button"]
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
          className: _style.default.description,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.subtitle))
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
          className: _style.default.description,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.description))
        }), initialState ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_two_factor_verification.default, {
          label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["button-label-default"])),
          onVerify: this.handleVerification,
          onChange: this.handleInputChanged,
          processing: processing,
          error: error,
          value: code,
          primary: true
        }) : /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default.codeContainer,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_formatted_code.default, {
            className: _style.default.code,
            value: oneTimeCodes
          })
        }), copiedCode === true ? /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
          className: _style.default.copiedMessage,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["copied-code"]))
        }) : null]
      });
    }
  }]);

  return GenerateBackupCodesDialog;
}(_react.Component);

GenerateBackupCodesDialog.propTypes = {
  intl: _reactIntl.intlShape,
  onClose: _propTypes.default.func,
  onError: _propTypes.default.func,
  session: _propTypes.default.shape({
    call: _propTypes.default.func
  })
};

var _default = (0, _with_session.default)((0, _safe_inject_intl.default)(GenerateBackupCodesDialog));

exports.default = _default;