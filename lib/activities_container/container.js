"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.activitiesLoad = activitiesLoad;
exports.activitiesLoadNextPage = activitiesLoadNextPage;
exports.activitiesLoaded = activitiesLoaded;
exports.reducer = reducer;
exports.activitiesLoadSaga = activitiesLoadSaga;
exports.activitiesLoadNextPageSaga = activitiesLoadNextPageSaga;
exports.default = exports.sagas = exports.actions = void 0;

require("regenerator-runtime/runtime.js");

require("core-js/modules/es6.object.assign.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.array.sort.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.find.js");

require("core-js/modules/es6.regexp.split.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactRedux = require("react-redux");

var _loglevel = _interopRequireDefault(require("loglevel"));

var _reactWaypoint = require("react-waypoint");

var _reactIntl = require("react-intl");

var _effects = require("redux-saga/effects");

var _reduxSaga = require("redux-saga");

var _ftrackJavascriptApi = require("ftrack-javascript-api");

var _moment = _interopRequireDefault(require("moment"));

var _style = _interopRequireDefault(require("./style.scss"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _constant = require("../util/constant");

var _activity = _interopRequireDefault(require("../activity"));

var _skeleton = _interopRequireDefault(require("../activity/skeleton"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(loadActivitiesHandler),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(activitiesLoadSaga),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(activitiesLoadNextPageSaga);

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var logger = _loglevel.default.getLogger("saga:activities");

var ACTIVITIES_LOAD = "ACTIVITIES_LOAD";
var ACTIVITIES_LOAD_NEXT_PAGE = "ACTIVITIES_LOAD_NEXT_PAGE";
var ACTIVITIES_LOADED = "ACTIVITIES_LOADED";
var actions = {
  ACTIVITIES_LOAD: ACTIVITIES_LOAD,
  ACTIVITIES_LOAD_NEXT_PAGE: ACTIVITIES_LOAD_NEXT_PAGE,
  ACTIVITIES_LOADED: ACTIVITIES_LOADED
};
exports.actions = actions;
var ALL_ACTIVITY_TYPES = {
  createdAssetVersion: true,
  createdReviewSessionObjects: true,
  createdReviewSession: true,
  collaboratorSetStatus: true,
  createdProject: true,
  userSharedVersionInReviewSession: true,
  renamedAsset: true,
  changeAsset: true,
  shareAssetVersion: true,
  notes: true
};
/** Load activities action creator. */

function activitiesLoad(id, type, clusterInterval, activityTypes) {
  return {
    type: ACTIVITIES_LOAD,
    payload: {
      entity: {
        id: id,
        type: type
      },
      clusterInterval: clusterInterval,
      activityTypes: activityTypes
    }
  };
}
/** Load next page of activities action creator. */


function activitiesLoadNextPage(id, type, nextOffset, clusterInterval, activityTypes) {
  return {
    type: ACTIVITIES_LOAD_NEXT_PAGE,
    payload: {
      entity: {
        id: id,
        type: type
      },
      nextOffset: nextOffset,
      clusterInterval: clusterInterval,
      activityTypes: activityTypes
    }
  };
}
/** Activities loaded action creator. */


function activitiesLoaded(entity, items, nextOffset, firstPage, activityTypes) {
  return {
    type: ACTIVITIES_LOADED,
    payload: {
      entity: entity,
      items: items,
      nextOffset: nextOffset,
      firstPage: firstPage,
      activityTypes: activityTypes
    }
  };
}
/**
 * Reduce state for application-wide activities.
 */


function reducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var action = arguments.length > 1 ? arguments[1] : undefined;
  var nextState = state;

  if (action.type === actions.ACTIVITIES_LOAD) {
    nextState = Object.assign({}, state, {
      entity: action.payload.entity,
      items: [],
      nextOffset: null,
      loading: true
    });
  }

  if (action.type === actions.ACTIVITIES_LOAD_NEXT_PAGE) {
    nextState = Object.assign({}, state, {
      loading: true
    });
  } else if (action.type === actions.ACTIVITIES_LOADED) {
    var items = [];

    if (action.payload.firstPage) {
      // Overwrite existing items.
      items = _toConsumableArray(action.payload.items);
    } else {
      items = [].concat(_toConsumableArray(state.items), _toConsumableArray(action.payload.items));
    }

    nextState = Object.assign({}, state, {
      items: items,
      nextOffset: action.payload.nextOffset,
      loading: false
    });
  }

  return nextState;
}
/** Return list of activities attributes to use in select. */


function eventSelect() {
  var select = ["id", "data", "user_id", "created_at", "action", "insert", "parent_id", "parent_type"];
  return "select ".concat(select.join(", "), " from Event");
}

var sortItems = function sortItems(a, b) {
  if (a.created_at.isSame(b.created_at)) {
    if (a.id < b.id) {
      return -1;
    }

    if (a.id > b.id) {
      return 1;
    }

    return 0;
  }

  if (a.created_at.isBefore(b.created_at.toDate())) {
    return 1;
  }

  return -1;
};
/** Return activity data from activity *item*.
 *
 * This function will process a raw input event *item* and return an object
 * containing `clusterIdentifier`, `type`, `lookup` and `data`.
 *
 * `type` is the determined type of activity, this maps to a React JSX class
 * to return such activity items (see components/actitivy).
 *
 * `clusterIdentifier` is a string that can be used to cluster together events.
 * An exampel is multiple asset versions created by a user: cluster_add_asset_version|<user-id>
 *
 * `lookup` is an array of additional data that must be looked up before the acitvity
 * information is complete.
 *
 * `data` is the complete data of the activity, intended to be used as props into
 * a React JSX activity item.
 *
 */


function processItem(item, inputData) {
  var parentId = item.parent_id;
  var rawEventData = JSON.parse(item.data);
  var lookup = [];
  var data = Object.assign({}, inputData);
  var type;
  var clusterIdentifier;

  if (item.action === "asset.published" && rawEventData.assetid && rawEventData.versionid) {
    type = "asset_version";
    data.asset_id = rawEventData.assetid.new;
    data.asset_version_id = rawEventData.versionid.new;
    data.user_id = item.user_id;
    clusterIdentifier = "cluster_add_asset_version|".concat(item.user_id);
    lookup.push({
      query: "select asset.name, version, thumbnail_url, thumbnail_id, id, asset.parent.name\n            from AssetVersion where id is \"".concat(data.asset_version_id, "\""),
      callback: function callback(_ref) {
        var _ref2 = _slicedToArray(_ref, 1),
            _ref2$ = _ref2[0],
            asset = _ref2$.asset,
            version = _ref2$.version,
            thumbnail_url = _ref2$.thumbnail_url,
            thumbnail_id = _ref2$.thumbnail_id,
            id = _ref2$.id;

        return {
          assetVersion: {
            name: asset.name,
            version: version,
            thumbnail_url: thumbnail_url,
            thumbnail_id: thumbnail_id,
            id: id,
            parent: asset.parent.__entity_type__ !== "Project" ? asset.parent.name : "",
            type: "AssetVersion"
          }
        };
      }
    });
  }

  if (item.action === "db.all.review_session_object") {
    type = "review_session_object";
    clusterIdentifier = "cluster_manage_review_session|".concat(rawEventData.review_session_id.new, "-").concat(item.user_id);
    lookup.push({
      query: "select name, review_session.name, review_session.id, asset_version.thumbnail_url, asset_version.thumbnail_id, \n                asset_version.version, asset_version.asset.parent.name from ReviewSessionObject where id is \"".concat(parentId, "\""),
      callback: function callback(_ref3) {
        var _ref4 = _slicedToArray(_ref3, 1),
            _ref4$ = _ref4[0],
            id = _ref4$.id,
            name = _ref4$.name,
            asset_version = _ref4$.asset_version,
            review_session = _ref4$.review_session;

        return {
          reviewSession: {
            name: review_session.name,
            id: review_session.id,
            type: "ReviewSession"
          },
          reviewSessionObject: {
            name: name,
            id: id,
            type: "ReviewSessionObject",
            thumbnail_url: asset_version.thumbnail_url,
            thumbnail_id: asset_version.thumbnail_id,
            version: asset_version.version,
            parent: asset_version.asset.parent.__entity_type__ !== "Project" ? asset_version.asset.parent.name : ""
          }
        };
      }
    });
  }

  if (item.action === "db.all.review_session") {
    type = "review_session";
    data.reviewSession = {
      name: rawEventData.name.new,
      id: parentId,
      type: "ReviewSession"
    };
    clusterIdentifier = "cluster_manage_review_session|".concat(parentId, "-").concat(item.user_id);
  }

  if (item.action === "db.all.review_session_object_status") {
    type = "object_status";
    data.review_session_object_status_id = parentId;
    data.status = rawEventData.status.new;
    lookup.push({
      query: "select review_session_object.name, resource.first_name, resource.last_name, review_session_object.asset_version.version\n            from ReviewSessionObjectStatus where id is \"".concat(parentId, "\""),
      callback: function callback(_ref5) {
        var _ref6 = _slicedToArray(_ref5, 1),
            _ref6$ = _ref6[0],
            review_session_object = _ref6$.review_session_object,
            resource = _ref6$.resource;

        return {
          reviewSessionObject: Object.assign({
            type: "ReviewSessionObject"
          }, review_session_object),
          user: resource,
          version: review_session_object.asset_version.version
        };
      }
    });
  }

  if (item.action === "db.all.note" && rawEventData.parent_type.new === "review_session_object") {
    type = "note";
    data.noteText = rawEventData.text.new;
    lookup.push({
      query: "select name, id, asset_version.version, review_session, review_session.name\n            from ReviewSessionObject where id is \"".concat(rawEventData.parent_id.new, "\""),
      callback: function callback(_ref7) {
        var _ref8 = _slicedToArray(_ref7, 1),
            _ref8$ = _ref8[0],
            name = _ref8$.name,
            id = _ref8$.id,
            asset_version = _ref8$.asset_version,
            review_session = _ref8$.review_session;

        return {
          entity: {
            name: name,
            id: id,
            version: asset_version.version,
            review_session: review_session,
            type: "ReviewSessionObject"
          }
        };
      }
    }); // Review session notes can be written by collaborators, which are not
    // Stored as Event.user_id, so need to be fetched explicitly.

    lookup.push({
      query: "select id, first_name, last_name from BaseUser where id in (select user_id from Note where id is \"".concat(parentId, "\")"),
      callback: function callback(_ref9) {
        var _ref10 = _slicedToArray(_ref9, 1),
            baseUser = _ref10[0];

        return {
          user: baseUser || data.user
        };
      }
    });
  }

  if (item.action === "db.all.note" && rawEventData.parent_type.new === "task") {
    type = "note";
    data.noteText = rawEventData.text.new;
    lookup.push({
      query: "select name, id from Context where id is \"".concat(rawEventData.parent_id.new, "\""),
      callback: function callback(_ref11) {
        var _ref12 = _slicedToArray(_ref11, 1),
            _ref12$ = _ref12[0],
            name = _ref12$.name,
            id = _ref12$.id;

        return {
          entity: {
            name: name,
            id: id
          }
        };
      }
    });
  }

  if (item.action === "db.all.note" && rawEventData.parent_type.new === "show") {
    type = "note";
    data.noteText = rawEventData.text.new;
    lookup.push({
      query: "select full_name, id from Project where id is \"".concat(rawEventData.parent_id.new, "\""),
      callback: function callback(_ref13) {
        var _ref14 = _slicedToArray(_ref13, 1),
            _ref14$ = _ref14[0],
            full_name = _ref14$.full_name,
            id = _ref14$.id;

        return {
          entity: {
            name: full_name,
            id: id
          }
        };
      }
    });
  }

  if (item.action === "db.all.note" && rawEventData.parent_type.new === "asset_version") {
    type = "note";
    data.noteText = rawEventData.text.new;
    lookup.push({
      query: "select asset.name, id, version\n            from AssetVersion where id is \"".concat(rawEventData.parent_id.new, "\""),
      callback: function callback(_ref15) {
        var _ref16 = _slicedToArray(_ref15, 1),
            _ref16$ = _ref16[0],
            asset = _ref16$.asset,
            id = _ref16$.id,
            version = _ref16$.version;

        return {
          entity: {
            name: asset.name,
            id: id,
            version: version,
            type: "AssetVersion"
          }
        };
      }
    });
  }

  if (item.action === "db.all.show") {
    type = "project";
    lookup.push({
      query: "select full_name, id from Project where id is \"".concat(parentId, "\""),
      callback: function callback(_ref17) {
        var _ref18 = _slicedToArray(_ref17, 1),
            _ref18$ = _ref18[0],
            full_name = _ref18$.full_name,
            id = _ref18$.id;

        return {
          project: {
            name: full_name,
            type: "Project",
            id: id
          }
        };
      }
    });
  }

  if (item.action === "db.all.asset") {
    type = "asset";
    data.isRename = !!rawEventData.name;
    data.isMove = !!rawEventData.context_id;
    lookup.push({
      query: "select name, id from Asset where id is \"".concat(parentId, "\""),
      callback: function callback(_ref19) {
        var _ref20 = _slicedToArray(_ref19, 1),
            _ref20$ = _ref20[0],
            name = _ref20$.name,
            id = _ref20$.id;

        return {
          asset: {
            name: name,
            id: id,
            type: "Asset"
          }
        };
      }
    });
  }

  if (!type) {
    type = "unknown";
    Object.assign(data, item);
  }

  return {
    type: type,
    data: data,
    lookup: lookup,
    clusterIdentifier: clusterIdentifier
  };
}
/** Handle load activities and load next page *action*. */


function loadActivitiesHandler(action) {
  var offset, limit, session, _action$payload, entity, clusterInterval, activityTypes, filters, clusterFeed, reviewSessionObjects, ids, _yield$session$query, _yield$session$query$, assetVersionId, _yield$session$query2, _reviewSessionObjects, _ids, _yield$session$query3, _reviewSessionObjects2, reviewSessionObjectIds, _yield$session$query4, assetVersions, assetVersionIds, pageFilter, queries, response, items, userIds, users, usersResponse, processedItems, lookupCallbacks, responseQueryResponses, storyItems, nextOffset, lastItem;

  return regeneratorRuntime.wrap(function loadActivitiesHandler$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          offset = false;
          limit = 25;

          if (action.type === actions.ACTIVITIES_LOAD_NEXT_PAGE) {
            offset = action.payload.nextOffset;
          }

          _context.next = 5;
          return (0, _effects.getContext)("ftrackSession");

        case 5:
          session = _context.sent;
          _action$payload = action.payload, entity = _action$payload.entity, clusterInterval = _action$payload.clusterInterval;
          activityTypes = action.payload.activityTypes;
          filters = [];
          clusterFeed = !!clusterInterval;

          if (!(entity.type === "Project")) {
            _context.next = 19;
            break;
          }

          // The following activity items are generated for a project:
          //
          // * User created asset version. (aggregate if multiple)
          // * User created a review session object.
          // * User created Review session. (aggregate shared assets)
          // * User wrote a note.
          // * Collaborator wrote a note.
          // * Collaborator set a review session object status.
          // * User created the project. (this will be the initial event)
          if (activityTypes.createdReviewSession) {
            filters.push("action is \"db.all.review_session\" and\n                insert is \"insert\" and\n                parent_id in (\n                    select id from ReviewSession where project.id is \"".concat(entity.id, "\"\n                )"));
          }

          if (activityTypes.createdReviewSessionObjects) {
            filters.push("action is \"db.all.review_session_object\" and\n                insert is \"insert\" and\n                parent_id in (\n                    select id from ReviewSessionObject where review_session.project.id is \"".concat(entity.id, "\"\n                )"));
          }

          if (activityTypes.collaboratorSetStatus) {
            filters.push("action is \"db.all.review_session_object_status\" and\n                parent_id in (\n                    select id from ReviewSessionObjectStatus where\n                    review_session_object.review_session.project.id is \"".concat(entity.id, "\"\n                )"));
          }

          if (activityTypes.createdAssetVersion) {
            filters.push("action is \"asset.published\" and project_id is \"".concat(entity.id, "\" and insert is \"insert\""));
          }

          if (activityTypes.notes) {
            filters.push("action is \"db.all.note\" and project_id is \"".concat(entity.id, "\" and insert is \"insert\""));
          }

          if (activityTypes.createdProject) {
            filters.push("action is \"db.all.show\" and project_id is \"".concat(entity.id, "\" and insert is \"insert\""));
          }

          _context.next = 74;
          break;

        case 19:
          if (!(entity.type === "ReviewSession")) {
            _context.next = 30;
            break;
          }

          _context.next = 22;
          return session.query("select id, version_id from ReviewSessionObject where review_session_id is \"".concat(entity.id, "\""));

        case 22:
          reviewSessionObjects = _context.sent;
          ids = reviewSessionObjects.data.reduce(function (accumulator, item) {
            accumulator.push(item.id, item.version_id);
            return accumulator;
          }, []);

          if (ids.length && activityTypes.notes) {
            filters.push("action is \"db.all.note\" and\n                insert is \"insert\" and\n                parent_id in (select id from Note where parent_id in (".concat(ids.join(","), "))"));
          }

          if (activityTypes.collaboratorSetStatus) {
            filters.push("action is \"db.all.review_session_object_status\" and\n                parent_id in (\n                    select id from ReviewSessionObjectStatus where\n                    review_session_object.review_session_id is \"".concat(entity.id, "\"\n                )"));
          }

          if (activityTypes.shareAssetVersion) {
            filters.push("action is \"db.all.review_session_object\" and\n                insert is \"insert\" and parent_id in (\n                    select id from ReviewSessionObject where review_session_id is \"".concat(entity.id, "\"\n                )"));
          }

          if (activityTypes.createdReviewSession) {
            filters.push("action is \"db.all.review_session\" and insert is \"insert\" and parent_id is \"".concat(entity.id, "\""));
          }

          _context.next = 74;
          break;

        case 30:
          if (!(entity.type === "ReviewSessionObject")) {
            _context.next = 41;
            break;
          }

          // The following activity items are generated for a review session object:
          //
          // * Collaborator wrote a note. (review session object or asset version)
          // * User wrote a note. (review session object or asset version)
          // * Collaborator set a review session object status.
          // * User shared a version in a review session (this will be the initial event)
          if (activityTypes.shareAssetVersion) {
            filters.push("action is \"db.all.review_session_object\" and\n                insert is \"insert\" and parent_id is \"".concat(entity.id, "\""));
          }

          _context.next = 34;
          return session.query("select version_id from ReviewSessionObject where id is \"".concat(entity.id, "\""));

        case 34:
          _yield$session$query = _context.sent;
          _yield$session$query$ = _slicedToArray(_yield$session$query.data, 1);
          assetVersionId = _yield$session$query$[0].version_id;

          if (activityTypes.notes) {
            filters.push("action is \"db.all.note\" and\n                insert is \"insert\" and\n                parent_id in (\n                    select id from Note where parent_id in (\"".concat(entity.id, "\", \"").concat(assetVersionId, "\")\n                )"));
          }

          if (activityTypes.collaboratorSetStatus) {
            filters.push("action is \"db.all.review_session_object_status\" and\n                parent_id in (\n                    select id from ReviewSessionObjectStatus where\n                    review_session_object.id is \"".concat(entity.id, "\"\n                )"));
          }

          _context.next = 74;
          break;

        case 41:
          if (!(entity.type === "AssetVersion")) {
            _context.next = 54;
            break;
          }

          _context.next = 44;
          return session.query("select id from ReviewSessionObject where version_id is \"".concat(entity.id, "\""));

        case 44:
          _yield$session$query2 = _context.sent;
          _reviewSessionObjects = _yield$session$query2.data;
          _ids = [entity.id].concat(_toConsumableArray(_reviewSessionObjects.map(function (item) {
            return item.id;
          })));

          if (activityTypes.createdAssetVersion) {
            filters.push("action is \"asset.published\" and parent_id is \"".concat(entity.id, "\" and insert is \"insert\""));
          }

          if (activityTypes.shareAssetVersion) {
            filters.push("action is \"db.all.review_session_object\" and\n                insert is \"insert\" and\n                parent_id in (select id from ReviewSessionObject where version_id is \"".concat(entity.id, "\")"));
          }

          if (activityTypes.notes) {
            filters.push("action is \"db.all.note\" and insert is \"insert\" and\n                parent_id in (\n                    select id from Note where parent_id in (".concat(_ids.join(", "), ")\n                )"));
          }

          if (activityTypes.collaboratorSetStatus) {
            filters.push("action is \"db.all.review_session_object_status\" and\n                parent_id in (\n                    select id from ReviewSessionObjectStatus where\n                    review_session_object.version_id is \"".concat(entity.id, "\"\n                )"));
          }

          if (activityTypes.changeAsset) {
            filters.push("action is \"db.all.asset\" and\n                insert is \"update\" and\n                parent_id in (\n                    select asset_id from AssetVersion where id is \"".concat(entity.id, "\"\n                )"));
          }

          _context.next = 74;
          break;

        case 54:
          if (!(entity.type === "Asset")) {
            _context.next = 73;
            break;
          }

          _context.next = 57;
          return session.query("select id from ReviewSessionObject where asset_version.asset_id is \"".concat(entity.id, "\""));

        case 57:
          _yield$session$query3 = _context.sent;
          _reviewSessionObjects2 = _yield$session$query3.data;
          reviewSessionObjectIds = _reviewSessionObjects2.map(function (item) {
            return item.id;
          });
          _context.next = 62;
          return session.query("select id from AssetVersion where asset_id is \"".concat(entity.id, "\""));

        case 62:
          _yield$session$query4 = _context.sent;
          assetVersions = _yield$session$query4.data;
          assetVersionIds = assetVersions.map(function (item) {
            return item.id;
          });

          if (activityTypes.createdAssetVersion) {
            filters.push("action is \"asset.published\" and insert is \"insert\" and parent_id in (\n                    select id from AssetVersion where asset_id is \"".concat(entity.id, "\"\n                )"));
          }

          if (activityTypes.shareAssetVersion) {
            filters.push("action is \"db.all.review_session_object\" and insert is \"insert\" and\n                parent_id in (\n                    select id from ReviewSessionObject where asset_version.asset_id is \"".concat(entity.id, "\"\n                )"));
          }

          if (reviewSessionObjectIds.length && activityTypes.notes) {
            filters.push("action is \"db.all.note\" and insert is \"insert\" and\n                parent_id in (\n                    select id from Note where parent_id in (".concat(reviewSessionObjectIds.join(", "), ")\n                )"));
          }

          if (assetVersionIds.length && activityTypes.notes) {
            filters.push("action is \"db.all.note\" and insert is \"insert\" and\n                parent_id in (\n                    select id from Note where parent_id in (".concat(assetVersionIds.join(", "), ")\n                )"));
          }

          if (activityTypes.collaboratorSetStatus) {
            filters.push("action is \"db.all.review_session_object_status\" and\n                parent_id in (\n                    select id from ReviewSessionObjectStatus where\n                    review_session_object.asset_version.asset_id is \"".concat(entity.id, "\"\n                )"));
          }

          if (activityTypes.changeAsset) {
            filters.push("action is \"db.all.asset\" and insert is \"update\" and parent_id is \"".concat(entity.id, "\""));
          }

          _context.next = 74;
          break;

        case 73:
          throw new Error("Activities cannot be fetched for type: ".concat(entity.type, "."));

        case 74:
          pageFilter = "";

          if (offset) {
            pageFilter = " and (created_at < \"".concat(offset.date.format(_constant.ENCODE_DATETIME_FORMAT), "\" or ") + "(created_at = \"".concat(offset.date.format(_constant.ENCODE_DATETIME_FORMAT), "\" and id >  \"").concat(offset.id, "\"))");
          }

          queries = filters.map(function (filter) {
            return "".concat(eventSelect(), " where ") + "(".concat(filter, ") ").concat(pageFilter, " ") + "order by created_at desc, id limit ".concat(limit);
          });
          logger.debug('Loading activities with "', queries, '" from action', action);
          _context.next = 80;
          return (0, _effects.call)([session, session.call], queries.map(function (query) {
            return _ftrackJavascriptApi.operation.query(query);
          }));

        case 80:
          response = _context.sent;
          logger.debug("Activities query result: ", response);
          items = response.reduce(function (accumulator, result) {
            return accumulator.concat(result.data);
          }, []);
          items.sort(sortItems);
          items = items.slice(0, limit);
          userIds = items.reduce(function (accumulator, item) {
            if (item.user_id) {
              accumulator.push("\"".concat(item.user_id, "\""));
            }

            return accumulator;
          }, []);
          users = [];

          if (!userIds.length) {
            _context.next = 92;
            break;
          }

          _context.next = 90;
          return session.query("select first_name, last_name, thumbnail_id, thumbnail_url, id from User where id in (".concat(userIds, ")"));

        case 90:
          usersResponse = _context.sent;
          users = usersResponse.data;

        case 92:
          processedItems = items.reduce(function (accumulator, item) {
            var data = {
              user: users.find(function (user) {
                return user.id === item.user_id;
              }),
              createdAt: (0, _moment.default)(item.created_at).local()
            };

            try {
              accumulator.push(processItem(item, data));
            } catch (e) {
              logger.error("Could not process item", item, data);
              accumulator.push({
                type: "error",
                data: data,
                lookup: []
              });
            }

            return accumulator;
          }, []);
          lookupCallbacks = processedItems.reduce(function (accumulator, processedItem) {
            accumulator.push.apply(accumulator, _toConsumableArray(processedItem.lookup.map(function (lookupChunk) {
              return Object.assign({}, lookupChunk, {
                item: processedItem
              });
            })));
            return accumulator;
          }, []);
          logger.debug("Running addtional queries:", lookupCallbacks.map(function (item) {
            return item.query;
          }));
          _context.next = 97;
          return (0, _effects.call)([session, session.call], lookupCallbacks.map(function (item) {
            return _ftrackJavascriptApi.operation.query(item.query);
          }));

        case 97:
          responseQueryResponses = _context.sent;
          logger.debug("Activities additional query result:", responseQueryResponses);
          responseQueryResponses.forEach(function (responseItem, index) {
            var callbackResult = {};

            try {
              callbackResult = lookupCallbacks[index].callback(responseItem.data);
            } catch (e) {
              logger.error("Could not process item to extract callback result", responseItem);
              lookupCallbacks[index].item.type = "error";
            } // Mutate the data on activity item.


            Object.assign(lookupCallbacks[index].item.data, callbackResult);
          });
          storyItems = clusterFeed === false ? processedItems : processedItems.reduce(function (accumulator, item, index) {
            // Process items again and create clusters of events. If two events with the
            // same clusterIdentifier has occured within the time limit they will be merged
            // together.
            var previous = index > 0 ? accumulator[accumulator.length - 1] : null;

            if (previous === null) {
              // First item to be processed so no cluster.
              accumulator.push(item);
            } else if (previous.data.createdAt.diff(item.data.createdAt, "s") > clusterInterval) {
              // Previous item happend outside of the cluster time limit.
              accumulator.push(item);
            } else if (item.clusterIdentifier && item.clusterIdentifier === previous.clusterIdentifier) {
              // A cluster has beem found and current item should be merged
              // into the previous.
              var _item$clusterIdentifi = item.clusterIdentifier.split("|"),
                  _item$clusterIdentifi2 = _slicedToArray(_item$clusterIdentifi, 1),
                  clusterType = _item$clusterIdentifi2[0];

              if (previous.type !== clusterType) {
                // Previous item was not a clustered item and must be
                // overwritten.
                previous = accumulator[accumulator.length - 1] = {
                  type: clusterType,
                  clusterIdentifier: previous.clusterIdentifier,
                  data: {
                    createdAt: previous.data.createdAt,
                    user: previous.data.user,
                    // Add it-self as a first item in items.
                    items: [previous]
                  }
                };
              }

              previous.data.items.push(item);
            } else {
              accumulator.push(item);
            }

            return accumulator;
          }, []);
          nextOffset = null;

          if (items.length && items.length >= limit) {
            lastItem = items[items.length - 1];
            nextOffset = {
              date: lastItem.created_at,
              id: lastItem.id
            };
          }

          _context.next = 105;
          return (0, _effects.put)(activitiesLoaded({
            id: action.payload.entity.id,
            type: action.payload.entity.type
          }, storyItems, nextOffset, // Indicate if this is the first page or not.
          offset === false, activityTypes));

        case 105:
        case "end":
          return _context.stop();
      }
    }
  }, _marked);
}
/** Handle ACTIVITIES_LOAD action. */


function activitiesLoadSaga() {
  return regeneratorRuntime.wrap(function activitiesLoadSaga$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return (0, _reduxSaga.takeEvery)(actions.ACTIVITIES_LOAD, loadActivitiesHandler);

        case 2:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2);
}
/** Handle ACTIVITIES_LOAD action. */


function activitiesLoadNextPageSaga() {
  return regeneratorRuntime.wrap(function activitiesLoadNextPageSaga$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return (0, _reduxSaga.takeEvery)(actions.ACTIVITIES_LOAD_NEXT_PAGE, loadActivitiesHandler);

        case 2:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3);
}

var sagas = [activitiesLoadSaga, activitiesLoadNextPageSaga]; // TODO: Render using Heading component for correct style

exports.sagas = sagas;

function Header(_ref21) {
  var children = _ref21.children;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: _style.default.header,
    children: children
  });
}

Header.propTypes = {
  children: _propTypes.default.oneOfType([_propTypes.default.arrayOf(_propTypes.default.node), _propTypes.default.node]).isRequired
};

var ActivitiesList = /*#__PURE__*/function (_Component) {
  _inherits(ActivitiesList, _Component);

  var _super = _createSuper(ActivitiesList);

  function ActivitiesList() {
    _classCallCheck(this, ActivitiesList);

    return _super.apply(this, arguments);
  }

  _createClass(ActivitiesList, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props = this.props,
          entity = _this$props.entity,
          onLoad = _this$props.onLoad,
          onRef = _this$props.onRef,
          loading = _this$props.loading,
          activityTypes = _this$props.activityTypes;

      if (!loading) {
        // Make sure that we only load if not already in loading state.
        onLoad(entity, activityTypes);
      }

      if (onRef) {
        onRef(this);
      }
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var _this$props2 = this.props,
          onLoad = _this$props2.onLoad,
          entity = _this$props2.entity,
          activityTypes = _this$props2.activityTypes;
      var entityId = entity && entity.id;
      var prevPropsEntityId = prevProps && prevProps.entity && prevProps.entity.id;
      var prevActivityTypes = prevProps.activityTypes;

      if (activityTypes !== prevActivityTypes || entityId && entityId !== prevPropsEntityId) {
        onLoad(entity, activityTypes);
      }
    }
  }, {
    key: "reload",
    value: function reload() {
      var entity = this.props.entity;
      this.props.onLoad(entity);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props3 = this.props,
          items = _this$props3.items,
          entity = _this$props3.entity,
          loading = _this$props3.loading,
          nextOffset = _this$props3.nextOffset,
          onFetchMore = _this$props3.onFetchMore,
          onActivityLinkClick = _this$props3.onActivityLinkClick,
          small = _this$props3.small,
          activityTypes = _this$props3.activityTypes;
      logger.debug("Rendering activities", entity, items);
      var yesterday = (0, _moment.default)().startOf("day").subtract(1, "days");
      var activities = items.reduce(function (accumulator, item, index, all) {
        var StoryComponent = _activity.default[item.type];
        var createdAt = item.data.createdAt;
        var isHeader = // Always show header on first item.
        index === 0 || // Or show header if item is on a different date.
        !(0, _moment.default)(createdAt).isSame(all[index - 1].data.createdAt, "d");

        if (isHeader) {
          if ((0, _moment.default)(createdAt).isAfter(yesterday)) {
            // Yesterday or today.
            accumulator.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(Header, {
              children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedRelative, {
                units: "day",
                value: createdAt.toDate()
              })
            }, "header-".concat(index)));
          } else {
            // Earlier, show full date.
            accumulator.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(Header, {
              children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedDate, {
                value: createdAt.toDate(),
                month: "long",
                day: "2-digit"
              })
            }, "header-".concat(index)));
          }
        }

        var component;

        if (StoryComponent) {
          component = /*#__PURE__*/(0, _jsxRuntime.jsx)(StoryComponent, _objectSpread(_objectSpread({
            small: small
          }, item.data), {}, {
            onClick: onActivityLinkClick
          }));
        } else {
          component = /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
              children: item.type
            }), /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
              children: JSON.stringify(item)
            })]
          });
        }

        accumulator.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default["activity-item-wrapper"],
          children: component
        }, "component-".concat(index)));
        return accumulator;
      }, []);
      var content = [];
      content.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        children: activities
      }, "activities-list-inner"));

      if (loading) {
        // Add loading indicator.
        content.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {}, "activity-skeleton"));
      }

      if (loading === false && nextOffset !== null && items.length) {
        // Only add way point if not loading and there are more items to load.
        activities.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactWaypoint.Waypoint, {
          onEnter: function onEnter() {
            return onFetchMore(entity, nextOffset, activityTypes);
          },
          bottomOffset: -100
        }, "activities-list-waypoint"));
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        children: content
      });
    }
  }]);

  return ActivitiesList;
}(_react.Component);

ActivitiesList.propTypes = {
  items: _propTypes.default.array.isRequired,
  // eslint-disable-line
  entity: _propTypes.default.shape({
    id: _propTypes.default.string.isRequired,
    type: _propTypes.default.string.isRequired
  }).isRequired,
  loading: _propTypes.default.bool,
  nextOffset: _propTypes.default.shape({
    date: _propTypes.default.object.isRequired,
    // eslint-disable-line
    id: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.number]).isRequired
  }),
  onFetchMore: _propTypes.default.func,
  onLoad: _propTypes.default.func,
  onRef: _propTypes.default.func,
  onActivityLinkClick: _propTypes.default.func.isRequired,
  clusterInterval: _propTypes.default.number,
  // eslint-disable-line
  small: _propTypes.default.bool,
  // eslint-disable-next-line react/forbid-prop-types
  activityTypes: _propTypes.default.object
};
ActivitiesList.defaultProps = {
  activityTypes: ALL_ACTIVITY_TYPES
};

var mapStateToProps = function mapStateToProps(state) {
  var items = state && state.items || [];
  var loading = state && state.loading || false;
  var nextOffset = state && state.nextOffset;
  return {
    items: items,
    nextOffset: nextOffset,
    loading: loading
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch, props) {
  return {
    onFetchMore: function onFetchMore(entity, nextOffset, activityTypes) {
      dispatch(activitiesLoadNextPage(entity.id, entity.type, nextOffset, props.clusterInterval, activityTypes));
    },
    onLoad: function onLoad(entity, activityTypes) {
      dispatch(activitiesLoad(entity.id, entity.type, props.clusterInterval, activityTypes));
    }
  };
};

var _default = (0, _safe_inject_intl.default)((0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(ActivitiesList));

exports.default = _default;