"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2021 ftrack
var MediaPropType = _propTypes.default.shape({
  id: _propTypes.default.string,
  name: _propTypes.default.string,
  thumbnailUrl: _propTypes.default.string,
  assetVersion: _propTypes.default.shape({
    status: _propTypes.default.shape({
      color: _propTypes.default.string
    })
  }),
  fileType: _propTypes.default.string,
  encoding: _propTypes.default.bool,
  status: _propTypes.default.shape({
    status: _propTypes.default.string
  }),
  description: _propTypes.default.string,
  version: _propTypes.default.string
});

var _default = MediaPropType;
exports.default = _default;