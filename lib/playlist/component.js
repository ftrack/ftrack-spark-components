"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.MediaObjectPlaylistItem = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.string.includes.js");

require("core-js/modules/es7.array.includes.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _withHandlers = _interopRequireDefault(require("recompose/withHandlers"));

var _reactIntl = require("react-intl");

var _reactBeautifulDnd = require("react-beautiful-dnd");

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _breadcrumbs = _interopRequireDefault(require("../breadcrumbs"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _version_number = _interopRequireDefault(require("../formatter/version_number"));

var _file_type_icon = _interopRequireDefault(require("../file_type_icon"));

var _media_prop_type = _interopRequireDefault(require("./media_prop_type"));

var _show_more_versions_menu = _interopRequireDefault(require("./show_more_versions_menu"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["className", "variant", "tooltip"],
    _excluded2 = ["className", "thumbnailUrl", "fileType", "encoding", "children", "extraChildren", "active", "disabled", "versionSelected", "assetId", "currentVersion", "versions", "status", "onItemClick", "provided"],
    _excluded3 = ["object", "playingMedia", "versionSelected"],
    _excluded4 = ["media", "index", "activeItem", "onClick", "activeSequenceIds", "canSort"],
    _excluded5 = ["className", "items", "activeItem", "activeSequenceIds", "onItemClick", "canSort", "onDragEnd", "playingMedia"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var messages = (0, _reactIntl.defineMessages)({
  "media-type-tooltip": {
    "id": "ftrack-spark-components.playlist.media-type-tooltip",
    "defaultMessage": "{mediaType, select, video {Video} image {Image} pdf {PDF} other {Other}}"
  },
  "stacked-versions-tooltip": {
    "id": "ftrack-spark-components.playlist.stacked-versions-tooltip",
    "defaultMessage": "{number} versions"
  },
  "empty-playlist-message": {
    "id": "ftrack-spark-components.playlist.empty-playlist-message",
    "defaultMessage": "No media added yet"
  }
});
/** Overlay text for playlist item */

function PlaylistItemOverlayText(_ref) {
  var _classNames;

  var className = _ref.className,
      variant = _ref.variant,
      tooltip = _ref.tooltip,
      props = _objectWithoutProperties(_ref, _excluded);

  var classes = (0, _classnames.default)(_style.default.playlistItemOverlayText, className, (_classNames = {}, _defineProperty(_classNames, _style.default.playlistItemOverlayTitle, variant === "title"), _defineProperty(_classNames, _style.default.playlistItemOverlaySubtitle, variant === "subtitle"), _defineProperty(_classNames, _style.default.playlistItemOverlayPrimary, variant === "primary"), _classNames));

  if (variant === "title") {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)("p", _objectSpread({
      title: tooltip,
      className: classes
    }, props));
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("p", _objectSpread({
    className: classes
  }, props));
}

PlaylistItemOverlayText.propTypes = {
  className: _propTypes.default.string,
  variant: _propTypes.default.string,
  tooltip: _propTypes.default.string
};
/** Approval Status bar for playlist item */

function PlaylistItemStatus(_ref2) {
  var _classNames2;

  var className = _ref2.className,
      status = _ref2.status;
  var classes = (0, _classnames.default)(_style.default.playListItemOverlayAssetVersionStatus, className, (_classNames2 = {}, _defineProperty(_classNames2, _style.default.playlistItemStatusApproved, status.status === "approved"), _defineProperty(_classNames2, _style.default.playlistItemStatusRequireChanges, status.status === "require_changes"), _classNames2));
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: classes
  });
}

PlaylistItemStatus.propTypes = {
  className: _propTypes.default.string,
  status: _propTypes.default.string
};
/** Asset status bar for playlist item */

function PlaylistItemAssetStatus(_ref3) {
  var status = _ref3.status;
  var classes = (0, _classnames.default)(_style.default.playListItemOverlayAssetVersionStatus);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: classes,
    style: {
      backgroundColor: status.color
    }
  });
}

PlaylistItemAssetStatus.propTypes = {
  status: _propTypes.default.shape({
    color: _propTypes.default.string.isRequired
  })
};

function isApprovalStatus(status) {
  return status.status === "approved" || status.status === "require_changes";
}
/**
 * Generic playlist item
 */


var PlaylistItem = /*#__PURE__*/function (_Component) {
  _inherits(PlaylistItem, _Component);

  var _super = _createSuper(PlaylistItem);

  function PlaylistItem(props) {
    var _this;

    _classCallCheck(this, PlaylistItem);

    _this = _super.call(this, props);
    _this.state = {
      menuOpen: false
    };
    return _this;
  }

  _createClass(PlaylistItem, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.active) {
        this.scrollIntoView();
      }
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if (this.props.active && !prevProps.active) {
        this.scrollIntoView();
      }
    }
  }, {
    key: "scrollIntoView",
    value: function scrollIntoView() {
      if (this.node && this.node.scrollIntoView) {
        this.node.scrollIntoView({
          block: "center",
          inline: "center",
          behavior: "smooth"
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _classNames3,
          _this2 = this,
          _classNames4;

      var _this$props = this.props,
          className = _this$props.className,
          thumbnailUrl = _this$props.thumbnailUrl,
          fileType = _this$props.fileType,
          encoding = _this$props.encoding,
          children = _this$props.children,
          extraChildren = _this$props.extraChildren,
          active = _this$props.active,
          disabled = _this$props.disabled,
          versionSelected = _this$props.versionSelected,
          assetId = _this$props.assetId,
          currentVersion = _this$props.currentVersion,
          versions = _this$props.versions,
          status = _this$props.status,
          onItemClick = _this$props.onItemClick,
          provided = _this$props.provided,
          props = _objectWithoutProperties(_this$props, _excluded2);

      var classes = (0, _classnames.default)(_style.default.playlistItem, className, (_classNames3 = {}, _defineProperty(_classNames3, _style.default.playlistItemActive, active), _defineProperty(_classNames3, _style.default.playlistItemMenuOpen, this.state.menuOpen), _defineProperty(_classNames3, _style.default.playlistItemVersionSelected, versionSelected), _classNames3));
      return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", _objectSpread(_objectSpread({
        className: _style.default.playlistItemContainer,
        ref: provided ? provided.innerRef : null
      }, props), {}, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
          ref: function ref(node) {
            _this2.node = node;
          },
          className: classes,
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            className: _style.default.playlistItemThumbnail,
            style: !disabled && thumbnailUrl ? {
              backgroundImage: "url('".concat(thumbnailUrl, "')")
            } : null
          }), disabled || !thumbnailUrl ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_file_type_icon.default, {
            className: _style.default.playlistItemFileIcon,
            variant: "playlist",
            label: fileType,
            loading: encoding
          }) : null, (versionSelected || active) && /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
            className: _style.default["active-indicator"]
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            className: _style.default.playlistItemBackground
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            className: _style.default.playlistItemOverlay,
            children: children
          }), extraChildren, versions.length > 1 && /*#__PURE__*/(0, _jsxRuntime.jsx)(_show_more_versions_menu.default, {
            assetId: assetId,
            currentVersion: currentVersion,
            versions: versions,
            selectVersion: function selectVersion(version) {
              return onItemClick(version);
            },
            menuOpen: this.state.menuOpen,
            setMenuOpen: function setMenuOpen(menuOpen) {
              return _this2.setState({
                menuOpen: menuOpen
              });
            }
          }), status && isApprovalStatus(status) && /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
            "data-ignoreonclick": assetId,
            value: status.status === "approved" ? "check" : "clear",
            className: (0, _classnames.default)(_style.default["approval-icon"], (_classNames4 = {}, _defineProperty(_classNames4, _style.default.approved, status.status === "approved"), _defineProperty(_classNames4, _style.default.requiresChanges, status.status === "require_changes"), _classNames4))
          })]
        })
      }));
    }
  }]);

  return PlaylistItem;
}(_react.Component);

PlaylistItem.propTypes = {
  className: _propTypes.default.string,
  thumbnailUrl: _propTypes.default.string,
  fileType: _propTypes.default.string,
  encoding: _propTypes.default.bool,
  children: _propTypes.default.node,
  extraChildren: _propTypes.default.node,
  assetId: _propTypes.default.string,
  versionSelected: _propTypes.default.bool,
  currentVersion: _propTypes.default.string,
  versions: _propTypes.default.arrayOf(_propTypes.default.objectOf(_media_prop_type.default)),
  status: _propTypes.default.shape({
    status: _propTypes.default.string.isRequired
  }),
  active: _propTypes.default.bool,
  disabled: _propTypes.default.bool,
  object: _media_prop_type.default,
  onItemClick: _propTypes.default.func,
  provided: _propTypes.default.object
};
/**
 * Review session object playlist item.
 */

function MediaObjectPlaylistItemBase(_ref4) {
  var object = _ref4.object,
      playingMedia = _ref4.playingMedia,
      versionSelected = _ref4.versionSelected,
      props = _objectWithoutProperties(_ref4, _excluded3);

  var assetId = object.assetId,
      versions = object.versions;

  var _ref5 = playingMedia && playingMedia.assetId === assetId ? playingMedia : object,
      objectId = _ref5.id,
      thumbnailUrl = _ref5.thumbnailUrl,
      fileType = _ref5.fileType,
      encoding = _ref5.encoding,
      status = _ref5.status,
      link = _ref5.link,
      name = _ref5.name,
      version = _ref5.version,
      assetVersion = _ref5.assetVersion;

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(PlaylistItem, _objectSpread(_objectSpread({
    assetId: assetId,
    thumbnailUrl: thumbnailUrl,
    fileType: fileType,
    encoding: encoding,
    currentVersion: objectId,
    versions: versions || [],
    status: status,
    versionSelected: versionSelected
  }, props), {}, {
    children: [link && /*#__PURE__*/(0, _jsxRuntime.jsx)(PlaylistItemOverlayText, {
      variant: "subtitle",
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_breadcrumbs.default, {
        truncation: "tooltip",
        items: link.map(function (_ref6) {
          var id = _ref6.id,
              title = _ref6.name;
          return {
            id: id,
            title: title,
            disabled: true
          };
        })
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(PlaylistItemOverlayText, {
      variant: "title",
      tooltip: name,
      children: name
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(PlaylistItemOverlayText, {
      variant: "primary",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_version_number.default, {
        version: version
      }), versions && versions.length > 1 && /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        className: _style.default.versionCount,
        children: " (".concat(versions.length, ")")
      })]
    }), assetVersion && assetVersion.status && /*#__PURE__*/(0, _jsxRuntime.jsx)(PlaylistItemAssetStatus, {
      status: assetVersion.status
    })]
  }));
}

MediaObjectPlaylistItemBase.propTypes = {
  object: _media_prop_type.default,
  playingMedia: _media_prop_type.default,
  intl: _reactIntl.intlShape.isRequired,
  versionSelected: _propTypes.default.bool
};
var MediaObjectPlaylistItem = (0, _withHandlers.default)({
  onClick: function onClick(props) {
    return function (event) {
      if (!props.versionSelected && !event.defaultPrevented) {
        props.onClick(props.object);
      }
    };
  }
})((0, _safe_inject_intl.default)(MediaObjectPlaylistItemBase));
exports.MediaObjectPlaylistItem = MediaObjectPlaylistItem;
MediaObjectPlaylistItem.defaultProps = {
  disabled: false,
  versionSelected: false
};

function DraggablePlaylistItem(_ref7) {
  var media = _ref7.media,
      index = _ref7.index,
      activeItem = _ref7.activeItem,
      onClick = _ref7.onClick,
      activeSequenceIds = _ref7.activeSequenceIds,
      canSort = _ref7.canSort,
      props = _objectWithoutProperties(_ref7, _excluded4);

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBeautifulDnd.Draggable, {
    isDragDisabled: !canSort,
    draggableId: media.id,
    index: index,
    children: function children(provided, snapshot) {
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(MediaObjectPlaylistItem, _objectSpread(_objectSpread(_objectSpread({
        object: media,
        disabled: !media.playable,
        active: media.id === (activeItem === null || activeItem === void 0 ? void 0 : activeItem.id),
        className: (0, _classnames.default)(_defineProperty({}, _style.default.notPlayableInSequence, activeSequenceIds && !activeSequenceIds.includes(media.id))),
        dragging: snapshot.dragging,
        onClick: onClick,
        provided: provided
      }, provided.draggableProps), provided.dragHandleProps), props), media.id);
    }
  });
}

DraggablePlaylistItem.propTypes = {
  media: _propTypes.default.object,
  activeItem: _propTypes.default.object,
  onClick: _propTypes.default.func,
  index: _propTypes.default.number,
  activeSequenceIds: _propTypes.default.arrayOf(_propTypes.default.string),
  canSort: _propTypes.default.bool
};
/**
 * Playlist, showing scrollable list of thumbnails.
 *
 * Currently only supports Review Session Objects
 */

var Playlist = function Playlist(_ref8) {
  var className = _ref8.className,
      items = _ref8.items,
      activeItem = _ref8.activeItem,
      activeSequenceIds = _ref8.activeSequenceIds,
      onItemClick = _ref8.onItemClick,
      canSort = _ref8.canSort,
      onDragEnd = _ref8.onDragEnd,
      playingMedia = _ref8.playingMedia,
      props = _objectWithoutProperties(_ref8, _excluded5);

  var classes = (0, _classnames.default)(_style.default.playlist, className);
  var playlist = !items.length ? /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: _style.default.playlistEmpty,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["empty-playlist-message"]))
  }) : /*#__PURE__*/(0, _jsxRuntime.jsx)("div", _objectSpread(_objectSpread({
    className: classes
  }, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBeautifulDnd.DragDropContext, {
      onDragEnd: onDragEnd,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBeautifulDnd.Droppable, {
        direction: "horizontal",
        droppableId: "horizontal-playlist",
        isDropDisabled: !canSort,
        children: function children(provided) {
          return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", _objectSpread(_objectSpread({
            ref: provided.innerRef
          }, provided.droppableProps), {}, {
            children: [items.map(function (item, index) {
              return /*#__PURE__*/(0, _jsxRuntime.jsx)(DraggablePlaylistItem, {
                media: item,
                index: index,
                activeItem: activeItem,
                activeSequenceIds: activeSequenceIds,
                onClick: function onClick(event) {
                  if (!event.defaultPrevented) {
                    onItemClick(event);
                  }
                },
                onItemClick: onItemClick,
                canSort: canSort,
                playingMedia: playingMedia
              }, item.id);
            }), provided.placeholder]
          }));
        }
      })
    })
  }));
  return playlist;
};

Playlist.propTypes = {
  items: _propTypes.default.arrayOf(_propTypes.default.shape({
    id: _propTypes.default.string,
    playable: _propTypes.default.bool
  })),
  className: _propTypes.default.string,
  activeItem: _propTypes.default.object,
  // eslint-disable-line
  onItemClick: _propTypes.default.func,
  playingMedia: _propTypes.default.shape({
    id: _propTypes.default.string
  }),
  intl: _reactIntl.intlShape.isRequired,
  activeSequenceIds: _propTypes.default.arrayOf(_propTypes.default.string),
  onDragEnd: _propTypes.default.func,
  canSort: _propTypes.default.bool
};
Playlist.defaultProps = {
  onDragEnd: function onDragEnd() {},
  canSort: false
};

var _default = (0, _safe_inject_intl.default)(Playlist);

exports.default = _default;