"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _material = require("@mui/material");

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _useOnclickoutside = _interopRequireDefault(require("use-onclickoutside"));

var _component = require("./component");

var _media_prop_type = _interopRequireDefault(require("./media_prop_type"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2018 ftrack

/** Show more menu for version selector in playlist */
var ShowMoreVersionsMenu = function ShowMoreVersionsMenu(_ref) {
  var assetId = _ref.assetId,
      currentVersion = _ref.currentVersion,
      versions = _ref.versions,
      selectVersion = _ref.selectVersion,
      menuOpen = _ref.menuOpen,
      setMenuOpen = _ref.setMenuOpen;
  var menuRef = (0, _react.useRef)(null);
  (0, _useOnclickoutside.default)(menuRef, function (e) {
    var closestIgnore = e.target.closest("[data-ignoreonclick]");
    var ignoreonclick = closestIgnore && closestIgnore.getAttribute("data-ignoreonclick") === assetId;

    if (!ignoreonclick) {
      setMenuOpen(false);
    }
  });
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    "data-ignoreonclick": assetId,
    ref: menuRef,
    className: _style.default["show-more-container"],
    onClick: function onClick(e) {
      e.stopPropagation();
      setMenuOpen(!menuOpen);
    },
    children: [!menuOpen && /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
      "data-ignoreonclick": assetId,
      value: "expand_less",
      className: _style.default["show-more-button"]
    }), menuRef.current && /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.Popper, {
      open: menuOpen,
      anchorEl: menuRef.current.parentNode,
      placement: "top-start",
      popperOptions: {
        modifiers: {
          preventOverflow: {
            padding: {
              left: 0,
              right: 0
            },
            boundariesElement: "window"
          }
        }
      },
      children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        "data-ignoreonclick": assetId,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default["show-more-menu"],
          style: {
            overflowY: versions.length > 5 ? "scroll" : "inherit"
          },
          children: versions.map(function (version) {
            return /*#__PURE__*/(0, _jsxRuntime.jsx)(_component.MediaObjectPlaylistItem, {
              className: _style.default["show-more-item"],
              object: version,
              disabled: !version.playable,
              versionSelected: version.id === currentVersion,
              onClick: function onClick() {
                return selectVersion(version);
              }
            }, version.id);
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
          "data-ignoreonclick": assetId,
          value: "expand_more",
          className: _style.default["show-more-button"],
          style: {
            right: versions.length > 5 ? "1.8rem" : "0.8rem"
          }
        })]
      })
    })]
  });
};

ShowMoreVersionsMenu.propTypes = {
  assetId: _propTypes.default.string,
  currentVersion: _propTypes.default.string,
  versions: _propTypes.default.arrayOf(_media_prop_type.default),
  selectVersion: _propTypes.default.func,
  menuOpen: _propTypes.default.bool,
  setMenuOpen: _propTypes.default.func
};
var _default = ShowMoreVersionsMenu;
exports.default = _default;