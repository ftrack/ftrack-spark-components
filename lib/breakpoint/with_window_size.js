"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _react = require("react");

var _throttle = _interopRequireDefault(require("lodash/throttle"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function getWindowSize() {
  var fallback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    width: 1024,
    height: 768
  };
  return {
    width: typeof window !== "undefined" ? window.innerWidth : fallback.width,
    height: typeof window !== "undefined" ? window.innerHeight : fallback.heigh
  };
}

var WINDOW_RESIZE_THROTTLE = 200;

var withWindowSize = function withWindowSize() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      fallback = _ref.fallback,
      _ref$mapSizeToProps = _ref.mapSizeToProps,
      mapSizeToProps = _ref$mapSizeToProps === void 0 ? function (size) {
    return size;
  } : _ref$mapSizeToProps;

  return function (WrappedComponent) {
    var ComponentWithWindowSize = /*#__PURE__*/function (_Component) {
      _inherits(ComponentWithWindowSize, _Component);

      var _super = _createSuper(ComponentWithWindowSize);

      function ComponentWithWindowSize(props, context) {
        var _this;

        _classCallCheck(this, ComponentWithWindowSize);

        _this = _super.call(this, props, context);
        var size = getWindowSize(fallback);
        _this.state = {
          size: size,
          props: mapSizeToProps(size)
        };
        _this.updateWindowSize = _this.updateWindowSize.bind(_assertThisInitialized(_this));
        _this.updateWindowSizeThrottled = (0, _throttle.default)(_this.updateWindowSize, WINDOW_RESIZE_THROTTLE);
        return _this;
      }

      _createClass(ComponentWithWindowSize, [{
        key: "componentDidMount",
        value: function componentDidMount() {
          window.addEventListener("resize", this.updateWindowSizeThrottled);
        }
      }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
          window.removeEventListener("resize", this.updateWindowSizeThrottled);
        }
      }, {
        key: "updateWindowSize",
        value: function updateWindowSize() {
          var size = getWindowSize(fallback);

          if (size.width !== this.state.size.width || size.height !== this.state.size.height) {
            var props = mapSizeToProps(size);
            this.setState({
              size: size,
              props: props
            });
          }
        }
      }, {
        key: "render",
        value: function render() {
          return /*#__PURE__*/(0, _jsxRuntime.jsx)(WrappedComponent, _objectSpread(_objectSpread({}, this.props), this.state.props));
        }
      }]);

      return ComponentWithWindowSize;
    }(_react.Component);

    ComponentWithWindowSize.displayName = "withWindowSize()";
    return ComponentWithWindowSize;
  };
};

var _default = withWindowSize;
exports.default = _default;