"use strict";

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.withMobile = exports.withBreakpoints = exports.MobileShown = exports.MobileHidden = exports.withIsMobile = void 0;

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.object.assign.js");

var _react = require("react");

var _with_window_size = _interopRequireDefault(require("./with_window_size"));

var _breakpoints = _interopRequireDefault(require("./breakpoints.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["isMobile"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function mapSizeToBreakpoints(_ref) {
  var width = _ref.width;
  return Object.keys(_breakpoints.default).reduce(function (accumulator, breakpoint) {
    accumulator[breakpoint] = width >= _breakpoints.default[breakpoint];
    return accumulator;
  }, {});
}

function mapSizeToMobile(_ref2) {
  var width = _ref2.width;
  return {
    isMobile: width < _breakpoints.default.md
  };
}

var withIsMobile = (0, _with_window_size.default)({
  mapSizeToProps: mapSizeToMobile
});
exports.withIsMobile = withIsMobile;
var MobileHidden = withIsMobile(function (_ref3) {
  var isMobile = _ref3.isMobile,
      children = _ref3.children;
  return isMobile ? null : _react.Children.only(children);
});
exports.MobileHidden = MobileHidden;
var MobileShown = withIsMobile(function (_ref4) {
  var isMobile = _ref4.isMobile,
      children = _ref4.children;
  return isMobile ? _react.Children.only(children) : null;
});
exports.MobileShown = MobileShown;

var withBreakpoints = function withBreakpoints(BaseComponent) {
  return (0, _with_window_size.default)({
    mapSizeToProps: mapSizeToBreakpoints
  })(function (_ref5) {
    var props = Object.assign({}, _ref5);
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(BaseComponent, _objectSpread({}, props));
  });
};

exports.withBreakpoints = withBreakpoints;

var withMobile = function withMobile(MobileComponent, DesktopComponent) {
  return withIsMobile(function (_ref6) {
    var isMobile = _ref6.isMobile,
        props = _objectWithoutProperties(_ref6, _excluded);

    return isMobile ? /*#__PURE__*/(0, _jsxRuntime.jsx)(MobileComponent, _objectSpread({}, props)) : /*#__PURE__*/(0, _jsxRuntime.jsx)(DesktopComponent, _objectSpread({}, props));
  });
};

exports.withMobile = withMobile;
var _default = withBreakpoints;
exports.default = _default;