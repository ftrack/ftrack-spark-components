"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.object.assign.js");

var _jsxRuntime = require("react/jsx-runtime");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// :copyright: Copyright (C) ftrack 2019
function FtrackIcon(_ref) {
  var props = Object.assign({}, _ref);
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("svg", _objectSpread(_objectSpread({
    width: "240px",
    height: "240px",
    viewBox: "0 0 240 240",
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg",
    xmlnsXlink: "http://www.w3.org/1999/xlink"
  }, props), {}, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("desc", {
      children: "ftrack icon"
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("defs", {}), /*#__PURE__*/(0, _jsxRuntime.jsx)("g", {
      id: "ftrack-icon",
      stroke: "none",
      strokeWidth: "1",
      fill: "none",
      fillRule: "evenodd",
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        // eslint-disable-next-line max-len
        d: "M131.972533,79.3979246 C122.931128,79.3979246 116.191171,82.5213192 116.191171,94.3559711 L116.191171,102.246652 L141.507106,102.246652 L141.507106,108.329052 C141.507106,116.219734 133.123258,120.822631 125.725744,120.822631 L116.191171,120.822631 L116.191171,180 L107.315525,180 C100.246789,180 94,174.575157 94,166.848865 L94,92.54769 C94,80.0554814 102.711257,60 128.52036,60 C138.054933,60 145.452447,65.2604541 145.452447,73.9730813 L145.452447,81.3705949 L144.79352,81.3705949 C141.342717,80.2198705 137.397377,79.3979246 131.972533,79.3979246 Z",
        id: "f",
        fill: "#FFFFFF"
      })
    })]
  }));
}

var _default = FtrackIcon;
exports.default = _default;