"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.PresenceIndicator = void 0;

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.map.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactIntl = require("react-intl");

var _reactFlipMove = _interopRequireDefault(require("react-flip-move"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _entity_avatar = _interopRequireDefault(require("../entity_avatar"));

var _style = _interopRequireDefault(require("./style.scss"));

var _Badge = _interopRequireDefault(require("@mui/material/Badge"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var messages = (0, _reactIntl.defineMessages)({
  "tooltip-sync": {
    "id": "ftrack-spark-components.avatar-list.tooltip-sync",
    "defaultMessage": "{user} is in sync"
  },
  "tooltip-online": {
    "id": "ftrack-spark-components.avatar-list.tooltip-online",
    "defaultMessage": "{user} is online"
  },
  "tooltip-sync-paused": {
    "id": "ftrack-spark-components.avatar-list.tooltip-sync-paused",
    "defaultMessage": "{user} has paused sync"
  }
});

var PresenceIndicator = function PresenceIndicator(_ref) {
  var presence = _ref.presence,
      className = _ref.className;
  var indicatorClasses = (0, _classnames.default)(_style.default["presence-indicator"], _defineProperty({}, _style.default.active, presence === "sync" || presence === "sync-pending"), className);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: indicatorClasses
  });
};

exports.PresenceIndicator = PresenceIndicator;
PresenceIndicator.propTypes = {
  presence: _propTypes.default.string,
  className: _propTypes.default.string
};

function PresenceTooltip(_ref2) {
  var name = _ref2.name,
      presence = _ref2.presence;
  var userPresence = /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["tooltip-online"]), {}, {
    values: {
      user: name
    }
  }));

  if (presence === "sync-pending") {
    userPresence = /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["tooltip-sync-paused"]), {}, {
      values: {
        user: name
      }
    }));
  } else if (presence === "sync") {
    userPresence = /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["tooltip-sync"]), {}, {
      values: {
        user: name
      }
    }));
  }

  return userPresence;
}

function AvatarList(props) {
  var maxNumberOfVisible = props.maxNumberOfVisible,
      avatars = props.avatars,
      className = props.className,
      session = props.session,
      children = props.children,
      showPresenceTooltip = props.showPresenceTooltip,
      animation = props.animation;
  var avatarsToShow = avatars.length > maxNumberOfVisible ? avatars.slice(0, maxNumberOfVisible - 1) : avatars;
  var numberOfHiddenAvatars = null;

  if (avatars.length > maxNumberOfVisible) {
    numberOfHiddenAvatars = avatars.length - (maxNumberOfVisible - 1) > 99 ? "99+" : "+".concat(avatars.length - (maxNumberOfVisible - 1));
  }

  var avatarClasses = (0, _classnames.default)(_style.default.avatars, className);
  var listOfAvatars = avatarsToShow.map(function (avatar) {
    var _classNames2;

    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_Badge.default, {
      overlap: "circular",
      anchorOrigin: {
        vertical: "top",
        horizontal: "right"
      },
      badgeContent: avatar.presence && avatar.presence !== "offline" ? /*#__PURE__*/(0, _jsxRuntime.jsx)(PresenceIndicator, {
        presence: avatar.presence,
        className: (0, _classnames.default)(_style.default["align-presence-indicators"], (_classNames2 = {}, _defineProperty(_classNames2, _style.default["presence-wave"], avatar.presence === "sync"), _defineProperty(_classNames2, _style.default["presence-pulse"], avatar.presence === "sync-pending"), _classNames2))
      }) : null,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_avatar.default, {
        entity: avatar.entity,
        session: session,
        color: avatar.entity.color,
        tooltip: showPresenceTooltip ? /*#__PURE__*/(0, _jsxRuntime.jsx)(PresenceTooltip, {
          name: avatar.entity.name,
          presence: avatar.presence
        }) : true
      }, avatar.entity.id)
    }, "avatar-".concat(avatar.entity.id));
  }); // Wrap avatars in FlipMove if *animation* is enabled.

  if (animation) {
    listOfAvatars = /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactFlipMove.default, {
      leaveAnimation: "accordionHorizontal",
      enterAnimation: "accordionHorizontal",
      typeName: null,
      children: listOfAvatars
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: avatarClasses,
    children: [listOfAvatars, numberOfHiddenAvatars && /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default["number-of-hidden-container"],
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        className: _style.default["number-of-hidden"],
        children: numberOfHiddenAvatars
      })
    }), children]
  });
}

AvatarList.propTypes = {
  className: _propTypes.default.string,
  avatars: _propTypes.default.array,
  // eslint-disable-line react/forbid-prop-types
  session: _propTypes.default.object,
  // eslint-disable-line react/forbid-prop-types
  maxNumberOfVisible: _propTypes.default.number,
  children: _propTypes.default.node,
  showPresenceTooltip: _propTypes.default.bool,
  animation: _propTypes.default.bool
};
AvatarList.defaultProps = {
  className: "",
  avatars: [],
  maxNumberOfVisible: 24,
  children: null
};

var _default = (0, _safe_inject_intl.default)(AvatarList);

exports.default = _default;