"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.object.assign.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _classnames = _interopRequireDefault(require("classnames"));

var _reactIntl = require("react-intl");

var _progress_bar = _interopRequireDefault(require("react-toolbox/lib/progress_bar"));

var _droppable_area = _interopRequireDefault(require("../droppable_area"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _icon_button = _interopRequireDefault(require("../icon_button"));

var _style = _interopRequireDefault(require("./style.scss"));

var _constant = require("../util/constant");

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["loading", "loadingComponent", "value", "style", "className", "dropzoneProps", "thumbnailSize", "intl", "label", "children", "emptyState", "deletable", "onDelete"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var messages = (0, _reactIntl.defineMessages)({
  "browse-button": {
    "id": "ftrack-spark-components.droppable-thumbnail.browse-button",
    "defaultMessage": "click here"
  },
  message: {
    "id": "ftrack-spark-components.droppable-thumbnail.message",
    "defaultMessage": "Drop image or {browseButton} to add new thumbnail"
  },
  "remove-button-tooltip": {
    "id": "ftrack-spark-components.droppable-thumbnail.remove-button-tooltip",
    "defaultMessage": "Delete"
  }
});

var Loading = function Loading(_ref) {
  var className = _ref.className;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: (0, _classnames.default)(className, _style.default.loading),
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_progress_bar.default, {
      type: "circular",
      mode: "indeterminate"
    })
  });
};

Loading.propTypes = {
  className: _propTypes.default.object //eslint-disable-line

}; // Time in ms to wait after upload completing to hide spinner,
// wait a while to allow thumbor to load image.
// Can be removed if a browser preview of the image is displayed instead.

var THUMBNAIL_UPLOAD_COMPLETE_DELAY = 1000;
/**
 * Droppable thumbnail component.
 *
 * Shows a thumbnail which can be replaced by dragging and dropping image
 * files.
 */

function DroppableThumbnail(_ref2) {
  var loading = _ref2.loading,
      loadingComponent = _ref2.loadingComponent,
      value = _ref2.value,
      inputStyle = _ref2.style,
      inputClassName = _ref2.className,
      dropzoneProps = _ref2.dropzoneProps,
      thumbnailSize = _ref2.thumbnailSize,
      intl = _ref2.intl,
      label = _ref2.label,
      children = _ref2.children,
      emptyState = _ref2.emptyState,
      deletable = _ref2.deletable,
      onDelete = _ref2.onDelete,
      props = _objectWithoutProperties(_ref2, _excluded);

  var className = {};
  var inlineStyle = Object.assign({}, inputStyle);

  if (value) {
    var thumbnailUrl = props.session.thumbnailUrl(value, {
      size: _constant.THUMBNAIL_SIZES[thumbnailSize]
    });
    className = _defineProperty({}, _style.default.hasValue, true);
    inlineStyle = Object.assign({}, inlineStyle, {
      backgroundImage: "url(\"".concat(thumbnailUrl, "\")")
    });
  }

  var browseButton = /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
    className: _style.default.highlight,
    children: intl.formatMessage(messages["browse-button"])
  });

  if (loading && loadingComponent) {
    return /*#__PURE__*/(0, _react.cloneElement)(loadingComponent, {
      className: (0, _classnames.default)(className, inputClassName)
    });
  }

  var thumbnailClasses = (0, _classnames.default)(_style.default.droppableThumbnail, className, inputClassName, _defineProperty({}, _style.default.hasEmptyState, !!emptyState));
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: thumbnailClasses,
    style: inlineStyle,
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_droppable_area.default, _objectSpread({
      multiple: false,
      dropzoneProps: Object.assign({}, dropzoneProps, {
        activeClassName: _style.default.dropzoneActive,
        rejectClassName: _style.default.dropzoneReject
      }),
      uploadCompleteDelay: THUMBNAIL_UPLOAD_COMPLETE_DELAY,
      accept: "image/*",
      maxSize: 1e7 // 10 MB
      ,
      message: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, label), {}, {
        values: {
          browseButton: browseButton
        }
      }))
    }, props)), !value && emptyState && /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default["empty-state-container"],
      children: emptyState
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default.children,
      children: children
    }), deletable ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_icon_button.default, {
      className: _style.default["deletable-button"],
      onClick: onDelete,
      icon: "delete",
      tooltip: intl.formatMessage(messages["remove-button-tooltip"])
    }) : null]
  });
}

DroppableThumbnail.propTypes = {
  className: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.object]),
  dropzoneProps: _propTypes.default.object,
  // eslint-disable-line
  onChange: _propTypes.default.func,
  onUploadError: _propTypes.default.func,
  session: _propTypes.default.shape({
    createComponent: _propTypes.default.func.isRequired,
    thumbnailUrl: _propTypes.default.func.isRequired
  }).isRequired,
  style: _propTypes.default.objectOf(_propTypes.default.string),
  thumbnailSize: _propTypes.default.oneOf(["small", "medium", "large", "xlarge"]),
  value: _propTypes.default.string,
  intl: _reactIntl.intlShape,
  label: _propTypes.default.shape({
    id: _propTypes.default.string.isRequired,
    defaultMessage: _propTypes.default.string.isRequired
  }).isRequired,
  children: _propTypes.default.element,
  loading: _propTypes.default.bool,
  loadingComponent: _propTypes.default.element,
  emptyState: _propTypes.default.element,
  deletable: _propTypes.default.bool,
  onDelete: _propTypes.default.func
};
DroppableThumbnail.defaultProps = {
  style: {},
  className: "",
  value: null,
  dropzoneProps: {},
  thumbnailSize: "medium",
  label: messages.message,
  loadingComponent: /*#__PURE__*/(0, _jsxRuntime.jsx)(Loading, {})
};

var _default = (0, _safe_inject_intl.default)(DroppableThumbnail);

exports.default = _default;