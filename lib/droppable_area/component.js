"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.promise.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactDropzone = _interopRequireDefault(require("react-dropzone"));

var _classnames = _interopRequireDefault(require("classnames"));

var _loglevel = _interopRequireDefault(require("loglevel"));

var _reactToolbox = require("react-toolbox");

var _reactIntl = require("react-intl");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _style = _interopRequireDefault(require("./style.scss"));

var _uploading_progress = _interopRequireDefault(require("./uploading_progress"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var messages = (0, _reactIntl.defineMessages)({
  "drop-files-label": {
    "id": "ftrack.spark-components.droppable-area.drop-files-label",
    "defaultMessage": "Drop files or"
  },
  "drop-files-label-highlighted": {
    "id": "ftrack.spark-components.droppable-area.drop-files-label-highlighted",
    "defaultMessage": "click here to browse"
  }
});
/**
 * Droppable area component.
 *
 * Shows an area where users can drag & drop files for upload.
 *
 */

var DroppableArea = /*#__PURE__*/function (_Component) {
  _inherits(DroppableArea, _Component);

  var _super = _createSuper(DroppableArea);

  function DroppableArea(props) {
    var _this;

    _classCallCheck(this, DroppableArea);

    _this = _super.call(this, props);
    _this.state = {
      uploading: false,
      uploadedPercentage: 0
    };
    _this.handleDropAccepted = _this.handleDropAccepted.bind(_assertThisInitialized(_this));
    _this.droppableAreaRef = null;
    return _this;
  }

  _createClass(DroppableArea, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var _this$props = this.props,
          showFileBrowser = _this$props.showFileBrowser,
          onShowFileBrowser = _this$props.onShowFileBrowser;

      if (showFileBrowser !== prevProps.showFileBrowser && showFileBrowser && this.droppableAreaRef) {
        this.droppableAreaRef.open();

        if (onShowFileBrowser) {
          onShowFileBrowser();
        }
      }
    }
  }, {
    key: "setLoadIndicator",
    value: function setLoadIndicator(percentage) {
      this.setState({
        uploadedPercentage: percentage
      });
    }
  }, {
    key: "resetUploadState",
    value: function resetUploadState() {
      this.setState({
        uploading: false,
        uploadedPercentage: 0
      });
    }
  }, {
    key: "handleDropAccepted",
    value: function handleDropAccepted(files) {
      _loglevel.default.debug("Dropped files", files);

      if (files.length) {
        this.uploadFile(files);
      }
    }
  }, {
    key: "uploadFile",
    value: function uploadFile(files) {
      var _this2 = this;

      this.setState({
        uploading: true
      });
      var promises = files.map(function (file) {
        return _this2.props.session.createComponent(file, {
          onProgress: function onProgress(progress) {
            return _this2.setLoadIndicator(progress);
          }
        });
      });
      Promise.all(promises).then(function (responses) {
        if (_this2.props.onChange) {
          responses.forEach(function (response) {
            _this2.props.onChange(response[0].data.id);
          });
        }

        setTimeout(function () {
          _this2.resetUploadState();
        }, _this2.props.uploadCompleteDelay);
      }).catch(function (error) {
        _this2.resetUploadState();

        if (_this2.props.onUploadError) {
          _this2.props.onUploadError(error);
        }
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var _this$props2 = this.props,
          message = _this$props2.message,
          intl = _this$props2.intl,
          disabled = _this$props2.disabled,
          multiple = _this$props2.multiple,
          maxSize = _this$props2.maxSize,
          accept = _this$props2.accept,
          dropzoneProps = _this$props2.dropzoneProps,
          border = _this$props2.border,
          dark = _this$props2.dark,
          onUploadError = _this$props2.onUploadError;
      var _this$state = this.state,
          uploadedPercentage = _this$state.uploadedPercentage,
          uploading = _this$state.uploading;
      var formatMessage = intl.formatMessage;
      var content = null;

      if (uploading) {
        content = /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default.content,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_uploading_progress.default, {
            progress: uploadedPercentage
          })
        });
      } else {
        content = /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
          className: _style.default.content,
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            className: _style.default["icon-container"],
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactToolbox.FontIcon, {
              className: _style.default.icon,
              value: "cloud_queue"
            })
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            className: (0, _classnames.default)(_style.default.dropMessage, _style.default["add-files-text"]),
            children: message || /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
              children: [formatMessage(messages["drop-files-label"]), " ", /*#__PURE__*/(0, _jsxRuntime.jsx)("br", {}), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
                className: _style.default.highlight,
                children: formatMessage(messages["drop-files-label-highlighted"])
              })]
            })
          })]
        });
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactDropzone.default, _objectSpread(_objectSpread({
        multiple: multiple,
        maxSize: maxSize,
        accept: accept,
        onDropAccepted: this.handleDropAccepted,
        onDropRejected: function onDropRejected(files) {
          var fileSize = files[0].size;

          if (onUploadError && fileSize > maxSize) {
            onUploadError({
              errorCode: "file-size-error"
            });
          }
        }
      }, dropzoneProps), {}, {
        disabled: disabled,
        ref: function ref(node) {
          _this3.droppableAreaRef = node;
        },
        children: function children(_ref) {
          var _classNames;

          var getRootProps = _ref.getRootProps,
              getInputProps = _ref.getInputProps,
              isDragAccept = _ref.isDragAccept,
              isDragReject = _ref.isDragReject;
          var classes = (0, _classnames.default)(_style.default.dropzone, (_classNames = {}, _defineProperty(_classNames, _style.default.border, border), _defineProperty(_classNames, _style.default.dark, dark), _defineProperty(_classNames, _style.default.dropzoneActive, isDragAccept), _defineProperty(_classNames, _style.default.dropzoneReject, isDragReject), _classNames));
          return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", _objectSpread(_objectSpread({
            className: classes
          }, getRootProps()), {}, {
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("input", _objectSpread({}, getInputProps())), content]
          }));
        }
      }));
    }
  }]);

  return DroppableArea;
}(_react.Component);

DroppableArea.propTypes = {
  accept: _propTypes.default.string,
  dropzoneProps: _propTypes.default.object,
  // eslint-disable-line
  onChange: _propTypes.default.func,
  onUploadError: _propTypes.default.func,
  session: _propTypes.default.shape({
    createComponent: _propTypes.default.func.isRequired
  }).isRequired,
  message: _propTypes.default.element,
  multiple: _propTypes.default.bool,
  maxSize: _propTypes.default.number,
  uploadCompleteDelay: _propTypes.default.number,
  intl: _reactIntl.intlShape.isRequired,
  disabled: _propTypes.default.bool,
  border: _propTypes.default.bool,
  dark: _propTypes.default.bool,
  showFileBrowser: _propTypes.default.bool,
  onShowFileBrowser: _propTypes.default.func
};
DroppableArea.defaultProps = {
  uploadCompleteDelay: 0,
  value: null,
  dropzoneProps: {},
  multiple: false,
  maxSize: 1e9,
  // 1000 MB
  accept: null,
  disabled: false,
  showFileBrowser: false
};

var _default = (0, _safe_inject_intl.default)(DroppableArea);

exports.default = _default;