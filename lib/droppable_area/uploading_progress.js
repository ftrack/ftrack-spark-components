"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _progress_bar = _interopRequireDefault(require("react-toolbox/lib/progress_bar"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactIntl = require("react-intl");

var _style = _interopRequireDefault(require("./style.scss"));

var _spinner_background_theme = _interopRequireDefault(require("./spinner_background_theme.scss"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["message", "mode", "className", "progress"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var messages = (0, _reactIntl.defineMessages)({
  "loading-message": {
    "id": "ftrack-spark-components.droppable-area.loading-message",
    "defaultMessage": "Uploading..."
  },
  "loading-message-finalizing": {
    "id": "ftrack-spark-components.droppable-area.loading-message-finalizing",
    "defaultMessage": "Finalizing upload..."
  }
});

function ProgressMessage(_ref) {
  var progress = _ref.progress;

  if (progress <= 0) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["loading-message"]));
  } else if (progress >= 100) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["loading-message-finalizing"]));
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
    children: [progress, "%"]
  });
}

ProgressMessage.propTypes = {
  progress: _propTypes.default.number
};

function UploadingProgress(_ref2) {
  var message = _ref2.message,
      mode = _ref2.mode,
      className = _ref2.className,
      progress = _ref2.progress,
      props = _objectWithoutProperties(_ref2, _excluded);

  var classes = (0, _classnames.default)(_style.default.uploadingProgress, className); // Force indeterminate spinner when upload is starting.

  var progressMode = mode;

  if (progressMode === "determinate" && progress <= 0) {
    progressMode = "indeterminate";
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", _objectSpread(_objectSpread({
    className: classes
  }, props), {}, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      children: [progressMode === "determinate" && /*#__PURE__*/(0, _jsxRuntime.jsx)(_progress_bar.default, {
        theme: _spinner_background_theme.default,
        type: "circular",
        mode: "determinate",
        value: 100
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_progress_bar.default, {
        type: "circular",
        mode: progressMode,
        value: progress
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
      className: _style.default.dropMessage,
      children: message || /*#__PURE__*/(0, _jsxRuntime.jsx)(ProgressMessage, {
        progress: progress
      })
    })]
  }));
}

UploadingProgress.propTypes = {
  mode: _propTypes.default.oneOf(["determinate", "indeterminate"]),
  progress: _propTypes.default.number,
  message: _propTypes.default.node,
  className: _propTypes.default.string
};
UploadingProgress.defaultProps = {
  mode: "determinate",
  message: null
};

var _default = (0, _safe_inject_intl.default)(UploadingProgress);

exports.default = _default;