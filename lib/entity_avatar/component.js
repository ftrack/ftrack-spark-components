"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _Avatar = _interopRequireDefault(require("@mui/material/Avatar"));

var _Tooltip = _interopRequireDefault(require("@mui/material/Tooltip"));

var _constant = require("../util/constant");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2017 ftrack

/**
 * EntityAvatar component - displays *entity* (user, invitee, context) as an Avatar.
 */
function EntityAvatar(_ref) {
  var session = _ref.session,
      entity = _ref.entity,
      className = _ref.className,
      size = _ref.size,
      color = _ref.color,
      tooltip = _ref.tooltip;

  var _classNames = (0, _classnames.default)(className);

  var url = null;

  if (entity !== null && entity !== void 0 && entity.thumbnail_id) {
    url = entity.thumbnail_url && entity.thumbnail_url.value;

    if (!url) {
      url = session.thumbnailUrl(entity.thumbnail_id, _constant.THUMBNAIL_SIZES.small);
    }
  }

  var title = null;

  if (entity !== null && entity !== void 0 && entity.full_name) {
    title = entity.full_name;
  } else if (entity !== null && entity !== void 0 && entity.name) {
    title = entity.name;
  } else if (entity !== null && entity !== void 0 && entity.first_name) {
    title = entity.first_name;

    if (entity !== null && entity !== void 0 && entity.last_name) {
      title = "".concat(title, " ").concat(entity.last_name);
    }
  }

  var tooltipProp = tooltip === true ? title : tooltip;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_Tooltip.default, {
    title: tooltipProp || "",
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Avatar.default, {
      className: _classNames,
      size: size,
      sx: color ? {
        backgroundColor: "".concat(color, "77"),
        borderColor: "".concat(color)
      } : {
        border: "none"
      },
      alt: title,
      src: url,
      children: title ? title.charAt(0) : ""
    })
  });
}

EntityAvatar.propTypes = {
  session: _propTypes.default.shape({
    thumbnailUrl: _propTypes.default.func.isRequired
  }),
  entity: _propTypes.default.object,
  // eslint-disable-line react/forbid-prop-types
  className: _propTypes.default.string,
  tooltip: _propTypes.default.bool,
  size: _propTypes.default.oneOf(["small", "medium", "large"]),
  color: _propTypes.default.string
};
EntityAvatar.defaultProps = {
  className: "",
  entity: {},
  tooltip: false,
  size: "medium"
};
var _default = EntityAvatar;
exports.default = _default;