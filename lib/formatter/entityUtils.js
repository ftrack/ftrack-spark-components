"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Entity = void 0;

var _reactIntl = require("react-intl");

var _jsxRuntime = require("react/jsx-runtime");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var messages = (0, _reactIntl.defineMessages)({
  user: {
    "id": "ftrack-spark-components.entity-type.user",
    "defaultMessage": "user"
  },
  group: {
    "id": "ftrack-spark-components.entity-type.group",
    "defaultMessage": "group"
  },
  list: {
    "id": "ftrack-spark-components.entity-type.list",
    "defaultMessage": "list"
  },
  context: {
    "id": "ftrack-spark-components.entity-type.context",
    "defaultMessage": "project or object"
  },
  "typed-context": {
    "id": "ftrack-spark-components.entity-type.typed-context",
    "defaultMessage": "object"
  },
  project: {
    "id": "ftrack-spark-components.entity-type.project",
    "defaultMessage": "project"
  },
  component: {
    "id": "ftrack-spark-components.entity-type.component",
    "defaultMessage": "component"
  },
  asset: {
    "id": "ftrack-spark-components.entity-type.asset",
    "defaultMessage": "asset"
  },
  "asset-version": {
    "id": "ftrack-spark-components.entity-type.asset-version",
    "defaultMessage": "asset version"
  },
  "review-session": {
    "id": "ftrack-spark-components.entity-type.review-session",
    "defaultMessage": "review session"
  }
});
var Entity = {
  User: {
    linkProjection: ["user.id", "user.first_name", "user.last_name"],
    linkRelation: "UserCustomAttributeLink",
    labelProjection: ["id", "first_name", "last_name"],
    order: "first_name, last_name",
    formatter: function formatter(_ref) {
      var first_name = _ref.first_name,
          last_name = _ref.last_name;
      return "".concat(first_name, " ").concat(last_name);
    },
    name: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.user))
  },
  Group: {
    linkProjection: ["group.link"],
    linkRelation: "GroupCustomAttributeLink",
    labelProjection: ["id", "name", "link"],
    order: "link",
    formatter: function formatter(_ref2) {
      var name = _ref2.name;
      return name;
    },
    name: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.group)),
    filter: "local != true"
  },
  List: {
    linkProjection: ["list.id", "list.name", "list.project.id", "list.project.full_name"],
    linkRelation: "ListCustomAttributeLink",
    labelProjection: ["id", "name"],
    order: "project.full_name, category.name, name",
    formatter: function formatter(_ref3) {
      var name = _ref3.name;
      return name;
    },
    name: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.list))
  },

  get TypedContextList() {
    return this.List;
  },

  get AssetVersionList() {
    return this.List;
  },

  Context: {
    linkProjection: ["context.link"],
    linkRelation: "ContextCustomAttributeLink",
    labelProjection: ["id", "name"],
    order: "name",
    formatter: function formatter(_ref4) {
      var name = _ref4.name;
      return name;
    },
    name: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.context))
  },
  TypedContext: {
    linkProjection: ["context.link"],
    linkRelation: "ContextCustomAttributeLink",
    labelProjection: ["id", "name"],
    order: "name",
    formatter: function formatter(_ref5) {
      var name = _ref5.name;
      return name;
    },
    name: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["typed-context"]))
  },
  Project: {
    linkProjection: ["context.link"],
    linkRelation: "ContextCustomAttributeLink",
    labelProjection: ["id", "full_name"],
    order: "full_name",
    formatter: function formatter(_ref6) {
      var full_name = _ref6.full_name;
      return full_name;
    },
    name: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.project))
  },
  Asset: {
    linkProjection: ["asset.name", "asset.parent.link", "asset.latest_version.id"],
    linkRelation: "AssetCustomAttributeLink",
    labelProjection: ["id", "name"],
    order: "name",
    formatter: function formatter(_ref7) {
      var name = _ref7.name;
      return name;
    },
    name: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.asset))
  },
  AssetVersion: {
    linkProjection: ["asset_version.link", "asset_version.status.name", "asset_version.status.color"],
    linkRelation: "AssetVersionCustomAttributeLink",
    labelProjection: ["id", "asset.name", "version"],
    order: "asset.name, version desc",
    formatter: function formatter(_ref8) {
      var name = _ref8.asset.name,
          version = _ref8.version;
      return "".concat(name, " - v").concat(version);
    },
    name: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["asset-version"]))
  },
  ReviewSession: {
    labelProjection: ["id", "name"],
    order: "created_at desc",
    formatter: function formatter(_ref9) {
      var name = _ref9.name;
      return name;
    },
    name: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["review-session"]))
  }
};
exports.Entity = Entity;