"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.array.slice.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TextFormatter = TextFormatter;
exports.VersionWithStatusFormatter = VersionWithStatusFormatter;
exports.SentenceCaseTextFormatter = SentenceCaseTextFormatter;
exports.FullnameFormatter = FullnameFormatter;
exports.DateFormatter = DateFormatter;
exports.LocalizedDateFormatter = LocalizedDateFormatter;
exports.AvatarThumbnailFormatter_ = AvatarThumbnailFormatter_;
exports.ColorLabelFormatter = ColorLabelFormatter;
exports.NamesFormatter = NamesFormatter;
exports.SecondToHoursFormatter = SecondToHoursFormatter;
exports.ColorFormatter = ColorFormatter;
exports.StorageSizeFormatter = StorageSizeFormatter;
exports.ComponentFormatter = ComponentFormatter;
exports.AssetFormatter = AssetFormatter;
exports.ListsFormatter = ListsFormatter;
exports.withAttributeLoadingState = exports.BidTimeFormatter = exports.SecondToDaysFormatter = exports.AvatarThumbnailFormatter = exports.ThumbnailFormatter = exports.LinkFormatter = exports.BooleanFormatter = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.object.assign.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _recompose = require("recompose");

var _reactIntl = require("react-intl");

var _classnames = _interopRequireDefault(require("classnames"));

var _button = require("react-toolbox/lib/button");

var _tooltip = _interopRequireDefault(require("react-toolbox/lib/tooltip"));

var _moment = _interopRequireDefault(require("moment"));

var _color = require("../util/color");

var _style = _interopRequireDefault(require("./style.scss"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _hoc = require("../util/hoc");

var _constant = require("../util/constant");

var _entity_avatar = _interopRequireDefault(require("../entity_avatar"));

var _skeleton = _interopRequireDefault(require("../skeleton"));

var _symbol = require("../dataview/symbol");

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["value"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var messages = (0, _reactIntl.defineMessages)({
  "value-true": {
    "id": "ftrack-spark-components.attribute.boolean-attribute.value-true",
    "defaultMessage": "Yes"
  },
  "value-false": {
    "id": "ftrack-spark-components.attribute.boolean-attribute.value-false",
    "defaultMessage": "No"
  }
});
var SpanWithTooltip = (0, _tooltip.default)("span");
/** Render text Formatter */

function TextFormatter(_ref) {
  var value = _ref.value;

  if (value === null) {
    return null;
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
    children: typeof value === "string" ? value : JSON.stringify(value)
  });
}

TextFormatter.propTypes = {
  value: _propTypes.default.string
};

function VersionWithStatusFormatter(_ref2) {
  var value = _ref2.value,
      onClick = _ref2.onClick;

  if (!value || !value.length) {
    return null;
  }

  var result = [];
  value.forEach(function (data, index) {
    if (index > 0 && index < value.length) {
      result.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        className: _style.default.comma,
        children: ","
      }));
    }

    result.push( /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(SpanWithTooltip, {
        tooltip: data.status.name,
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
          className: _style.default.circle,
          style: {
            backgroundColor: data.status.color
          }
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(LinkFormatter, {
        value: data.link,
        shortLabel: true,
        onClick: onClick
      })]
    }));
  });
  return result;
}

VersionWithStatusFormatter.propTypes = {
  value: _propTypes.default.array,
  // eslint-disable-line react/forbid-prop-types
  onClick: _propTypes.default.func
};

function SentenceCaseTextFormatter(_ref3) {
  var value = _ref3.value;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
    className: _style.default["sentence-case"],
    children: value
  });
}

SentenceCaseTextFormatter.propTypes = {
  value: _propTypes.default.string
};

function BooleanFormatter_(_ref4) {
  var value = _ref4.value;

  if (value === true) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["value-true"]));
  }

  if (value === false) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["value-false"]));
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {});
}

BooleanFormatter_.propTypes = {
  value: _propTypes.default.bool
};
var BooleanFormatter = (0, _safe_inject_intl.default)(BooleanFormatter_);
exports.BooleanFormatter = BooleanFormatter;

var LinkFormatter = /*#__PURE__*/function (_PureComponent) {
  _inherits(LinkFormatter, _PureComponent);

  var _super = _createSuper(LinkFormatter);

  function LinkFormatter() {
    var _this;

    _classCallCheck(this, LinkFormatter);

    _this = _super.call(this);
    _this.onClick = _this.onClick.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(LinkFormatter, [{
    key: "onClick",
    value: function onClick() {
      var value = this.props.value;
      var lastItem = value[value.length - 1];
      this.props.onClick("link", {
        id: lastItem.id,
        type: lastItem.type
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          value = _this$props.value,
          onClick = _this$props.onClick,
          shortLabel = _this$props.shortLabel;
      var classes = (0, _classnames.default)(_defineProperty({}, _style.default["link-formatter-with-click"], onClick));

      if (!value || !value.length) {
        return null;
      }

      var label = "";

      if (shortLabel) {
        label = value[value.length - 1].name;
      } else {
        label = value.map(function (item) {
          return item.name;
        }).join(" / ");
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        onClick: onClick && this.onClick,
        className: classes,
        title: label,
        children: label
      });
    }
  }]);

  return LinkFormatter;
}(_react.PureComponent);

exports.LinkFormatter = LinkFormatter;
LinkFormatter.propTypes = {
  value: _propTypes.default.array,
  // eslint-disable-line react/forbid-prop-types
  onClick: _propTypes.default.func,
  shortLabel: _propTypes.default.bool
};
LinkFormatter.defaultProps = {
  shortLabel: false
};

function FullnameFormatter(_ref5) {
  var first = _ref5.first,
      last = _ref5.last;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
    children: "".concat(first || "", " ").concat(last || "")
  });
}

FullnameFormatter.propTypes = {
  first: _propTypes.default.string.isRequired,
  last: _propTypes.default.string.isRequired
};
/** Render formatted moment date.
 *   Note that the DateFormatter only modify the passed moment if *localize* is true.
 */

function DateFormatter(_ref6) {
  var value = _ref6.value,
      displayTooltip = _ref6.displayTooltip,
      localize = _ref6.localize;
  var clonedValue = _moment.default.isMoment(value) ? (0, _moment.default)(value) : null;

  if (!clonedValue) {
    return null;
  }

  var displayValue = localize ? clonedValue.local() : clonedValue;
  var formattedDate = /*#__PURE__*/(0, _jsxRuntime.jsx)(TextFormatter, {
    value: displayValue && displayValue.format && displayValue.format("MMM Do YYYY")
  });

  if (!displayTooltip) {
    return formattedDate;
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(SpanWithTooltip, {
    floating: true,
    tooltip: displayValue && displayValue.format && displayValue.format("MMM Do YYYY HH:mm"),
    children: formattedDate
  });
}

DateFormatter.propTypes = {
  value: _propTypes.default.shape({
    format: _propTypes.default.func.isRequired
  }),
  displayTooltip: _propTypes.default.bool,
  localize: _propTypes.default.bool
};
DateFormatter.defaultProps = {
  displayTooltip: false,
  localize: false
};
/** Render formatted moment date in user's timezone. */

function LocalizedDateFormatter(_ref7) {
  var props = Object.assign({}, _ref7);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(DateFormatter, _objectSpread({
    localize: true
  }, props));
}

function ThumbnailFormatter_(_ref8) {
  var _classNames2;

  var thumbnailId = _ref8.thumbnailId,
      session = _ref8.session,
      onClick = _ref8.onClick,
      height = _ref8.height,
      disableThumbnailClick = _ref8.disableThumbnailClick;
  // Render compact image and play button if row height is small.
  var className = (0, _classnames.default)(_style.default.image, (_classNames2 = {}, _defineProperty(_classNames2, _style.default.compact, height < 50), _defineProperty(_classNames2, _style.default.large, height > 80), _classNames2));
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: className,
    style: {
      backgroundImage: "url('".concat(session.thumbnailUrl(thumbnailId, 500), "')")
    },
    children: !disableThumbnailClick && onClick && thumbnailId ? /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default["button-container"],
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_button.IconButton, {
        icon: "play_circle_filled",
        onClick: onClick,
        flat: true
      })
    }) : null
  });
}

ThumbnailFormatter_.propTypes = {
  thumbnailId: _propTypes.default.string,
  session: _propTypes.default.shape({
    thumbnailUrl: _propTypes.default.func.isRequired
  }),
  onClick: _propTypes.default.func,
  height: _propTypes.default.number,
  disableThumbnailClick: _propTypes.default.bool
};
ThumbnailFormatter_.defaultProps = {
  disableThumbnailClick: false
};
var ThumbnailFormatter = (0, _recompose.compose)((0, _recompose.withHandlers)({
  onClick: function onClick(props) {
    return function () {
      return props.onClick && props.onClick("thumbnail", props.thumbnailId);
    };
  }
}), _hoc.withSession)(ThumbnailFormatter_);
exports.ThumbnailFormatter = ThumbnailFormatter;

function AvatarThumbnailFormatter_(_ref9) {
  var user = _ref9.user,
      session = _ref9.session,
      height = _ref9.height;
  var size = "medium";

  if (height > 80) {
    size = "large";
  } else if (height < 40) {
    size = "small";
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_avatar.default, {
    entity: user,
    size: size,
    session: session
  });
}

AvatarThumbnailFormatter_.propTypes = {
  session: _propTypes.default.shape({
    thumbnailUrl: _propTypes.default.func.isRequired
  }),
  height: _propTypes.default.number,
  user: _propTypes.default.shape({
    thumnail_id: _propTypes.default.func.isRequired,
    first_name: _propTypes.default.func.isRequired,
    last_name: _propTypes.default.func.isRequired
  })
};
var AvatarThumbnailFormatter = (0, _hoc.withSession)(AvatarThumbnailFormatter_);
exports.AvatarThumbnailFormatter = AvatarThumbnailFormatter;

function ColorLabelFormatter(_ref10) {
  var name = _ref10.name,
      color = _ref10.color;

  if (!name) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {});
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    style: {
      backgroundColor: color,
      color: (0, _color.getForegroundColor)(color)
    },
    className: _style.default["attribute-priority-status"],
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      children: name
    })
  });
}

ColorLabelFormatter.propTypes = {
  name: _propTypes.default.string,
  color: _propTypes.default.string
};

function NamesFormatter(_ref11) {
  var names = _ref11.names;

  if (!names) {
    return null;
  }

  var elements = names.reduce(function (accumulator, item) {
    var element = /*#__PURE__*/(0, _jsxRuntime.jsx)(FullnameFormatter, {
      first: item.first_name,
      last: item.last_name
    });

    if (accumulator === null) {
      return [element];
    }

    accumulator.push(", ", element);
    return accumulator;
  }, null);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
    children: elements
  });
}

NamesFormatter.propTypes = {
  names: _propTypes.default.array // eslint-disable-line

};

function SecondToHoursFormatter(_ref12) {
  var value = _ref12.value;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
    children: ((value || 0) / 3600.0).toFixed(2)
  });
}

SecondToHoursFormatter.propTypes = {
  value: _propTypes.default.number.isRequired
};

function SecondToDaysFormatter_(_ref13) {
  var value = _ref13.value,
      workdayLength = _ref13.workdayLength;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
    children: ((value || 0) / workdayLength).toFixed(2)
  });
}

SecondToDaysFormatter_.propTypes = {
  value: _propTypes.default.number.isRequired,
  workdayLength: _propTypes.default.number.isRequired
};
var SecondToDaysFormatter = (0, _hoc.withSettings)({
  workdayLength: _constant.DEFAULT_WORKDAY_LENGTH
})(SecondToDaysFormatter_);
exports.SecondToDaysFormatter = SecondToDaysFormatter;

function BidTimeFormatter_(_ref14) {
  var value = _ref14.value,
      isDisplayBidAsDays = _ref14.isDisplayBidAsDays;

  if (isDisplayBidAsDays) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(SecondToDaysFormatter, {
      value: value
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(SecondToHoursFormatter, {
    value: value
  });
}

BidTimeFormatter_.propTypes = {
  value: _propTypes.default.number.isRequired,
  isDisplayBidAsDays: _propTypes.default.bool.isRequired
};
var BidTimeFormatter = (0, _hoc.withSettings)({
  isDisplayBidAsDays: _constant.DEFAULT_DISPLAY_BID_AS_DAYS
})(BidTimeFormatter_);
/** Render color */

exports.BidTimeFormatter = BidTimeFormatter;

function ColorFormatter(_ref15) {
  var value = _ref15.value,
      height = _ref15.height;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: _style.default.color,
    style: {
      backgroundColor: value,
      lineHeight: height && "".concat(height - 8, "px") || undefined
    },
    children: "\xA0"
  });
}

ColorFormatter.propTypes = {
  value: _propTypes.default.string,
  height: _propTypes.default.number
};
var SIZE_MAP = ["B", "KB", "MB", "GB", "TB", "PB"];

function convertBytes(value) {
  var iteration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

  if (value < 1024 || iteration >= SIZE_MAP.length - 1) {
    return {
      value: value,
      unit: SIZE_MAP[iteration]
    };
  }

  return convertBytes(value / 1024, iteration + 1);
}

function StorageSizeFormatter(_ref16) {
  var value = _ref16.value,
      props = _objectWithoutProperties(_ref16, _excluded);

  var _convertBytes = convertBytes(value),
      size = _convertBytes.value,
      unit = _convertBytes.unit; // Return rounded value with 1 decimal precision.


  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", _objectSpread(_objectSpread({}, props), {}, {
    children: [Math.round(size * 10) / 10, " ", unit]
  }));
}

StorageSizeFormatter.propTypes = {
  value: _propTypes.default.number.isRequired
};

function LoadingState() {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {
    className: _style.default.loadingState
  });
}

var withAttributeLoadingState = (0, _recompose.branch)(function (_ref17) {
  var value = _ref17.value;
  return value === _symbol.ATTRIBUTE_LOADING;
}, function () {
  return LoadingState;
});
exports.withAttributeLoadingState = withAttributeLoadingState;

function ComponentFormatter(_ref18) {
  var _ref18$value = _ref18.value,
      name = _ref18$value.name,
      version = _ref18$value.version,
      onClick = _ref18.onClick;
  var classes = (0, _classnames.default)(_defineProperty({}, _style.default["link-formatter-with-click"], !!version));
  var props = {};

  if (version) {
    props.title = version.link.map(function (_ref19) {
      var name = _ref19.name;
      return name;
    }).join(" / ");

    props.onClick = function () {
      return onClick("link", {
        id: version.id,
        type: "AssetVersion"
      });
    };
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", _objectSpread(_objectSpread({
    className: classes
  }, props), {}, {
    children: name
  }));
}

ComponentFormatter.propTypes = {
  value: _propTypes.default.object,
  onClick: _propTypes.default.func
};

function AssetFormatter(_ref20) {
  var _ref20$value = _ref20.value,
      name = _ref20$value.name,
      parent = _ref20$value.parent,
      version = _ref20$value.latest_version,
      onClick = _ref20.onClick;
  var classes = (0, _classnames.default)(_defineProperty({}, _style.default["link-formatter-with-click"], !!version));
  var props = {};

  if (parent) {
    props.title = [].concat(_toConsumableArray(parent.link.map(function (_ref21) {
      var name = _ref21.name;
      return name;
    })), [name]).join(" / ");

    props.onClick = function () {
      return onClick("link", {
        id: version.id,
        type: "AssetVersion"
      });
    };
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", _objectSpread(_objectSpread({
    className: classes
  }, props), {}, {
    children: name
  }));
}

AssetFormatter.propTypes = {
  value: _propTypes.default.object,
  onClick: _propTypes.default.func
};

function ListsFormatter(_ref22) {
  var _ref22$values = _ref22.values,
      values = _ref22$values === void 0 ? [] : _ref22$values,
      onClick = _ref22.onClick;
  return values.map(function (_ref23, i) {
    var name = _ref23.name,
        id = _ref23.id;
    return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_react.Fragment, {
      children: [!!i && /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        children: ", "
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(LinkFormatter, {
        value: [{
          name: name,
          id: id,
          type: "List"
        }],
        onClick: onClick
      })]
    }, id);
  });
}

ListsFormatter.propTypes = {
  lists: _propTypes.default.array,
  // eslint-disable-line
  onClick: _propTypes.default.func
};