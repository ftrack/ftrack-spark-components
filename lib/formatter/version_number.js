"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactIntl = require("react-intl");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var messages = (0, _reactIntl.defineMessages)({
  version: {
    "id": "ftrack-spark-components.attribute-formatter-version.version",
    "defaultMessage": "Version {versionNumber}"
  },
  "version-short": {
    "id": "ftrack-spark-components.attribute-formatter-version.version-short",
    "defaultMessage": "V{versionNumber}"
  }
});

function VersionNumber(_ref) {
  var version = _ref.version,
      _ref$format = _ref.format,
      format = _ref$format === void 0 ? "full" : _ref$format;

  if (format === "short") {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["version-short"]), {}, {
      values: {
        versionNumber: version
      }
    }));
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages.version), {}, {
    values: {
      versionNumber: version
    }
  }));
}

VersionNumber.propTypes = {
  version: _propTypes.default.number,
  format: _propTypes.default.oneOf(["short", "full"])
};
var _default = VersionNumber;
exports.default = _default;