"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactSelect = _interopRequireDefault(require("react-select"));

var _dropdown = _interopRequireDefault(require("react-toolbox/lib/dropdown"));

var _reactIntl = require("react-intl");

var _recompose = require("recompose");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _dropdown_theme = _interopRequireDefault(require("./dropdown_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2016 ftrack
function formatOptions(intl, items) {
  var options = (items || []).map(function (item) {
    return {
      label: item.i18n_label ? intl.formatMessage(item.i18n_label) : item.label,
      value: item.value
    };
  });
  return options;
}

function DropdownFilter(_ref) {
  var intl = _ref.intl,
      value = _ref.value,
      items = _ref.items,
      multiSelect = _ref.multiSelect,
      _onChange = _ref.onChange;
  var options = formatOptions(intl, items);

  if (multiSelect) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactSelect.default, {
      multi: true,
      options: options,
      value: value,
      onChange: function onChange(selected) {
        var values = selected.map(function (item) {
          return item.value;
        });

        _onChange(values);
      }
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_dropdown.default, {
    auto: true,
    theme: _dropdown_theme.default,
    onChange: _onChange,
    source: options,
    value: value
  });
}

DropdownFilter.propTypes = {
  value: _propTypes.default.string,
  items: _propTypes.default.array.isRequired,
  multiSelect: _propTypes.default.bool,
  onChange: _propTypes.default.func,
  intl: _reactIntl.intlShape.isRequired
};

var _default = (0, _recompose.compose)(_safe_inject_intl.default, (0, _recompose.withHandlers)({
  onChange: function onChange(props) {
    return function (value) {
      return props.onChange(value && value.length ? value : null);
    };
  }
}))(DropdownFilter);

exports.default = _default;