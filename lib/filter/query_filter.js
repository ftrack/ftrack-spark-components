"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.replaceVariables = replaceVariables;
exports.default = void 0;

require("core-js/modules/es6.regexp.match.js");

require("core-js/modules/es6.regexp.constructor.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.regexp.replace.js");

require("core-js/modules/es6.string.starts-with.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _debounce = _interopRequireDefault(require("lodash/debounce"));

var _input = require("react-toolbox/lib/input");

var _recompose = require("recompose");

var _reactIntl = require("react-intl");

var _progress_bar = _interopRequireDefault(require("react-toolbox/lib/progress_bar"));

var _moment = _interopRequireDefault(require("moment"));

var _loglevel = _interopRequireDefault(require("loglevel"));

var _hoc = require("../util/hoc");

var _constant = require("../util/constant");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _style = _interopRequireDefault(require("./style.scss"));

var _query_filter_progress_theme = _interopRequireDefault(require("./query_filter_progress_theme.scss"));

var _query_filter_input_theme = _interopRequireDefault(require("./query_filter_input_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var logger = _loglevel.default.getLogger("query-filter");
/** Return new expression with repalced variables in *expression*.
 *
 * A configuration must be passed userId, username. Optionally a `safeNow`
 * variable can be passed. This allows for "stable" expression generation.
 *
 */


function replaceVariables(expression, configuration) {
  var userId = configuration.userId,
      username = configuration.username,
      _configuration$safeNo = configuration.safeNow,
      safeNow = _configuration$safeNo === void 0 ? (0, _moment.default)() : _configuration$safeNo;
  var replace = {
    "{CURRENT_USER_ID}": function CURRENT_USER_ID() {
      return userId;
    },
    "{CURRENT_USERNAME}": function CURRENT_USERNAME() {
      return username;
    },
    "{LAST_MONTH}": function LAST_MONTH() {
      return (0, _moment.default)(safeNow).startOf("month").add(-1, "month").format(_constant.ENCODE_DATE_FORMAT);
    },
    "{THIS_MONTH}": function THIS_MONTH() {
      return (0, _moment.default)(safeNow).startOf("month").format(_constant.ENCODE_DATE_FORMAT);
    },
    "{NEXT_MONTH}": function NEXT_MONTH() {
      return (0, _moment.default)(safeNow).startOf("month").add(1, "month").format(_constant.ENCODE_DATE_FORMAT);
    },
    "{LAST_WEEK}": function LAST_WEEK() {
      return (0, _moment.default)(safeNow).startOf("isoWeek").add(-1, "week").format(_constant.ENCODE_DATE_FORMAT);
    },
    "{THIS_WEEK}": function THIS_WEEK() {
      return (0, _moment.default)(safeNow).startOf("isoWeek").format(_constant.ENCODE_DATE_FORMAT);
    },
    "{NEXT_WEEK}": function NEXT_WEEK() {
      return (0, _moment.default)(safeNow).startOf("isoWeek").add(1, "week").format(_constant.ENCODE_DATE_FORMAT);
    },
    "{YESTERDAY}": function YESTERDAY() {
      return (0, _moment.default)(safeNow).startOf("day").add(-1, "days").format(_constant.ENCODE_DATE_FORMAT);
    },
    "{TODAY}": function TODAY() {
      return (0, _moment.default)(safeNow).format(_constant.ENCODE_DATE_FORMAT);
    },
    "{TOMORROW}": function TOMORROW() {
      return (0, _moment.default)(safeNow).startOf("day").add(1, "days").format(_constant.ENCODE_DATE_FORMAT);
    },
    "{NOW}": function NOW() {
      return (0, _moment.default)(safeNow).format(_constant.ENCODE_DATETIME_FORMAT);
    }
  };

  var daysExpression = function daysExpression(variable) {
    var offset = 0;

    try {
      offset = parseInt(variable.match(/(-?\d+)/g)[0], 10);
    } catch (error) {
      logger.error("Could not parse days expression", error);
    }

    return (0, _moment.default)(safeNow).add(offset, "days").format(_constant.ENCODE_DATE_FORMAT);
  };

  var replaceKeysRegexp = new RegExp(["{DAYS\\(\\-?\\d*\\)}"].concat(_toConsumableArray(Object.keys(replace))).join("|"), "g");
  return expression.replace(replaceKeysRegexp, function (matched) {
    if (matched.startsWith("{DAYS(")) {
      return daysExpression(matched);
    }

    return replace[matched]();
  });
}

var messages = (0, _reactIntl.defineMessages)({
  "api-help": {
    "id": "ftrack-spark-components.filter.query-filter.api-ehlp",
    "defaultMessage": "Filter using an API Query. For syntax and more information see the {helpLink}"
  },
  "api-help-link": {
    "id": "ftrack-spark-components.filter.query-filter.api-help-link",
    "defaultMessage": "http://help.ftrack.com/using-ftrack-studio/advanced/filtering-using-api-like-queries"
  },
  documentation: {
    "id": "ftrack-spark-components.filter.query-filter.documentation",
    "defaultMessage": "documentation"
  }
});

var QueryFilter = /*#__PURE__*/function (_Component) {
  _inherits(QueryFilter, _Component);

  var _super = _createSuper(QueryFilter);

  function QueryFilter(props) {
    var _this;

    _classCallCheck(this, QueryFilter);

    _this = _super.call(this, props);
    _this.onChange = _this.onChange.bind(_assertThisInitialized(_this));
    _this.validateQuery = (0, _debounce.default)(_this.validateQuery.bind(_assertThisInitialized(_this)), 500);
    _this.state = {
      processing: false,
      errors: false,
      value: props.value
    };
    return _this;
  }

  _createClass(QueryFilter, [{
    key: "onChange",
    value: function onChange(value) {
      this.setState({
        processing: !!value,
        errors: false,
        value: value
      });

      if (value) {
        this.validateQuery(value);
      } else if (value === "") {
        this.props.onChange(null);
      }
    }
  }, {
    key: "validateQuery",
    value: function validateQuery(value) {
      var _this2 = this;

      var _this$props = this.props,
          model = _this$props.model,
          session = _this$props.session;
      session.call([{
        action: "parse_query",
        expression: "select id from ".concat(model, " where ").concat(value)
      }]).then(function (_ref) {
        var _ref2 = _slicedToArray(_ref, 1),
            data = _ref2[0].data;

        _this2.setState({
          errors: data !== true ? data : false,
          processing: false
        });

        if (data === true) {
          _this2.props.onChange(value);
        }
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$state = this.state,
          value = _this$state.value,
          processing = _this$state.processing,
          errors = _this$state.errors;
      var intl = this.props.intl;
      var hint = null;

      if (!errors && !processing) {
        var helpLink = /*#__PURE__*/(0, _jsxRuntime.jsx)("a", {
          href: intl.formatMessage(messages["api-help-link"]),
          target: "blank",
          children: intl.formatMessage(messages.documentation)
        });
        hint = /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
          className: _style.default["filter-hint"],
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["api-help"]), {}, {
            values: {
              helpLink: helpLink
            }
          }))
        });
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: _style.default["query-filter"],
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_input.Input, {
          multiline: true,
          rows: 3,
          value: value,
          onChange: this.onChange,
          theme: _query_filter_input_theme.default,
          className: _style.default["field-field-standard-input"],
          error: errors
        }), processing && /*#__PURE__*/(0, _jsxRuntime.jsx)(_progress_bar.default, {
          theme: _query_filter_progress_theme.default,
          type: "linear",
          mode: "indeterminate"
        }), hint]
      });
    }
  }]);

  return QueryFilter;
}(_react.Component);

QueryFilter.propTypes = {
  model: _propTypes.default.string.isRequired,
  value: _propTypes.default.string.isRequired,
  onChange: _propTypes.default.func.isRequired,
  intl: _reactIntl.intlShape,
  session: _propTypes.default.shape({
    call: _propTypes.default.func.isRequired
  }).isRequired
};

var _default = (0, _recompose.compose)(_hoc.withSession, _safe_inject_intl.default)(QueryFilter);

exports.default = _default;