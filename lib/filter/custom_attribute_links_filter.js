"use strict";

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _chip = _interopRequireDefault(require("@ftrack/react-toolbox/lib/chip"));

var _font_icon = _interopRequireDefault(require("@ftrack/react-toolbox/lib/font_icon"));

var _reactIntl = require("react-intl");

var _entity_picker = _interopRequireDefault(require("../entity_picker"));

var _hoc = require("../util/hoc");

var _style = _interopRequireDefault(require("./style.scss"));

var _hooks = require("../util/hooks");

var _entityUtils = require("../formatter/entityUtils");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var messages = (0, _reactIntl.defineMessages)({
  "select-entity-type": {
    "id": "ftrack-spark-components.filter.select-entity-type",
    "defaultMessage": "Select {entityType}..."
  }
});

function CustomAttributeLinksFilterItem(_ref) {
  var children = _ref.children,
      onClick = _ref.onClick,
      placeholder = _ref.placeholder;

  function handleKeyDown(e) {
    if (e.key === "Enter" || e.key === " ") {
      e.preventDefault();
      onClick();
    }
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    onClick: onClick,
    onKeyDown: handleKeyDown,
    className: _style.default.customAttributeLinksFilterItem,
    tabIndex: "0",
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      className: _style.default.customAttributeLinksFilterItemChip,
      children: children.length === 0 ? placeholder : children
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
      value: "edit"
    })]
  });
}

CustomAttributeLinksFilterItem.propTypes = {
  children: _propTypes.default.arrayOf(_propTypes.default.object),
  onClick: _propTypes.default.func,
  placeholder: _propTypes.default.string
};

var query = function query(entityType, projection, filter) {
  return "\n        select ".concat(projection.join(","), "\n        from ").concat(entityType, "\n        ").concat(filter && "where ".concat(filter), "\n    ");
};

function useFetchLabels(session, ids, entityType) {
  var _useState = (0, _react.useState)(ids ? ids.map(function (v) {
    return {
      id: v
    };
  }) : []),
      _useState2 = _slicedToArray(_useState, 2),
      labels = _useState2[0],
      setLabels = _useState2[1];

  var _useSessionQuery = (0, _hooks.useSessionQuery)({
    session: session,
    baseQuery: query(entityType, _entityUtils.Entity[entityType].labelProjection, "id in(".concat(labels.map(function (_ref2) {
      var id = _ref2.id;
      return "'".concat(id, "'");
    }).join(","), ")")),
    config: {
      limit: 25,
      pause: labels.length === 0
    }
  }),
      isLoading = _useSessionQuery.isLoading,
      data = _useSessionQuery.data;

  (0, _react.useEffect)(function () {
    if (!isLoading && Array.isArray(data)) {
      setLabels(data.map(function (entity) {
        return {
          id: entity.id,
          label: _entityUtils.Entity[entityType].formatter(entity)
        };
      }));
    }
  }, // TODO: Feel free to fix this eslint error if you got time
  // eslint-disable-next-line react-hooks/exhaustive-deps
  [isLoading, data]);
  return [labels, setLabels];
}

function CustomAttributeLinksFilter(_ref3) {
  var value = _ref3.value,
      entityType = _ref3.entityType,
      objectTypeId = _ref3.objectTypeId,
      objectTypeName = _ref3.objectTypeName,
      contextId = _ref3.contextId,
      baseFilter = _ref3.baseFilter,
      onChange = _ref3.onChange,
      session = _ref3.session;

  var _useState3 = (0, _react.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      entityPickerIsOpen = _useState4[0],
      setEntityPickerIsOpen = _useState4[1];

  var _useFetchLabels = useFetchLabels(session, value, entityType),
      _useFetchLabels2 = _slicedToArray(_useFetchLabels, 2),
      labels = _useFetchLabels2[0],
      setLabels = _useFetchLabels2[1];

  function handleClick() {
    setEntityPickerIsOpen(true);
  }

  function handleClose() {
    setEntityPickerIsOpen(false);
  }

  function handleSelect(val) {
    setLabels(val.map(function (v) {
      return {
        id: v.id,
        label: v.returnLabel
      };
    }));
    onChange(val.length > 0 ? val.map(function (v) {
      return v.id;
    }) : null);
    setEntityPickerIsOpen(false);
  }

  var entityTypeName = objectTypeName || _entityUtils.Entity[entityType] && _entityUtils.Entity[entityType].name || entityType || "";
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_react.Fragment, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(CustomAttributeLinksFilterItem, {
      onClick: handleClick,
      placeholder: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["select-entity-type"]), {}, {
        values: {
          entityType: /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
            className: _style.default.entityTypeName,
            children: entityTypeName
          })
        }
      })),
      children: labels.map(function (v) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_chip.default, {
          children: v.label || "…"
        }, v.id);
      })
    }), entityPickerIsOpen && /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_picker.default, {
      open: true,
      mode: "MULTI",
      contextId: contextId,
      onClose: handleClose,
      entityType: entityType,
      objectTypeId: objectTypeId,
      onSelected: handleSelect,
      selected: labels.map(function (v) {
        return v.id;
      }),
      baseFilter: baseFilter,
      session: session
    })]
  });
}

CustomAttributeLinksFilter.propTypes = {
  value: _propTypes.default.arrayOf(_propTypes.default.string),
  entityType: _propTypes.default.string,
  objectTypeId: _propTypes.default.string,
  objectTypeName: _propTypes.default.string,
  onChange: _propTypes.default.func,
  baseFilter: _propTypes.default.string,
  session: _propTypes.default.func,
  contextId: _propTypes.default.string
};

var _default = (0, _hoc.withSession)(CustomAttributeLinksFilter);

exports.default = _default;