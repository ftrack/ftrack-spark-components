"use strict";

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getCondition = getCondition;
exports.isFilterActive = isFilterActive;
exports.toQueryExpression = toQueryExpression;

require("core-js/modules/es6.regexp.split.js");

require("core-js/modules/es6.string.starts-with.js");

require("core-js/modules/es6.regexp.replace.js");

require("core-js/modules/es6.regexp.constructor.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.regexp.match.js");

require("core-js/modules/es6.array.find.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es7.object.entries.js");

var _moment = _interopRequireDefault(require("moment"));

var _isString = _interopRequireDefault(require("lodash/isString"));

var _isArray = _interopRequireDefault(require("lodash/isArray"));

var _util = require("../dataview/util");

var _constant = require("../util/constant");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

/**
 * Convert *value* to a date range expression for for *configuration.attribute*.
 *
 * value.mode can be one of:
 *     - this_week
 *     - this_month
 *     - custom_range
 *
 * For `custom_range`, specify value.start and value.end as either Date objects
 * or ISO-formatted strings.
 *
 * *isDateTime* if `true`, utc date-time value will be used,
 *  if `false`, date without time will be used (default).
 *
 */
function toDateRangeExpression(value, attribute, operator, configuration, isDateTime) {
  var valueStart = null;
  var valueEnd = null;
  var inclusiveEnd = true;
  var _configuration$isSund = configuration.isSundayFirstDayOfWeek,
      isSundayFirstDayOfWeek = _configuration$isSund === void 0 ? false : _configuration$isSund;

  if (!value || !value.mode) {
    return null;
  } else if (value.mode === "custom_range") {
    valueStart = (0, _moment.default)(value.start);
    valueEnd = (0, _moment.default)(value.end);
  } else if (value.mode === "date") {
    if (!value.date) {
      return null;
    }

    valueStart = (0, _moment.default)(value.date);

    if (isDateTime) {
      valueEnd = (0, _moment.default)(value.date);
    } else {
      valueEnd = (0, _moment.default)(value.date).add(1, "days");
      inclusiveEnd = false;
    }
  } else if (value.mode === "this_week") {
    valueStart = (0, _moment.default)().startOf("week");
    valueEnd = (0, _moment.default)().endOf("week");

    if (!isSundayFirstDayOfWeek) {
      valueStart.add(1, "days");
      valueEnd.add(1, "days");
    }
  } else if (value.mode === "this_month") {
    valueStart = (0, _moment.default)().startOf("month");
    valueEnd = (0, _moment.default)().endOf("month");
  } else if (value.mode === "next_month") {
    valueStart = (0, _moment.default)().startOf("month").add(1, "months");
    valueEnd = (0, _moment.default)().endOf("month").add(1, "months");
  } else if (value.mode === "next_week") {
    valueStart = (0, _moment.default)().startOf("week").add(7, "days");
    valueEnd = (0, _moment.default)().endOf("week").add(7, "days");

    if (!isSundayFirstDayOfWeek) {
      valueStart.add(1, "days");
      valueEnd.add(1, "days");
    }
  } else if (value.mode === "today") {
    valueStart = (0, _moment.default)();

    if (isDateTime) {
      valueEnd = (0, _moment.default)();
    } else {
      valueEnd = (0, _moment.default)().add(1, "days");
      inclusiveEnd = false;
    }
  } else if (value.mode === "tomorrow") {
    valueStart = (0, _moment.default)().add(1, "days");

    if (isDateTime) {
      valueEnd = (0, _moment.default)().add(1, "days");
    } else {
      valueEnd = (0, _moment.default)().add(2, "days");
      inclusiveEnd = false;
    }
  } else if (value.mode === "yesterday") {
    valueStart = (0, _moment.default)().add(-1, "days");

    if (isDateTime) {
      valueEnd = (0, _moment.default)().add(-1, "days");
    } else {
      valueEnd = (0, _moment.default)();
      inclusiveEnd = false;
    }
  }

  if (!valueStart && !valueEnd) {
    return null;
  }

  if ((0, _isString.default)(valueStart)) {
    valueStart = (0, _moment.default)(valueStart);
  }

  if ((0, _isString.default)(valueEnd)) {
    valueEnd = (0, _moment.default)(valueEnd);
  }

  var startExpression;

  if (valueStart) {
    if (isDateTime) {
      var utcStart = (0, _moment.default)(valueStart.startOf("day")).utc();
      startExpression = "".concat(attribute, " >= \"").concat(utcStart.format(_constant.ENCODE_DATETIME_FORMAT), "\"");
    } else {
      startExpression = "".concat(attribute, " >= \"").concat(valueStart.format(_constant.ENCODE_DATE_FORMAT), "\"");
    }
  }

  var endExpression;

  if (valueEnd) {
    if (isDateTime) {
      var utcEnd = (0, _moment.default)(valueEnd.endOf("day")).utc();
      endExpression = "".concat(attribute, " <= \"").concat(utcEnd.format(_constant.ENCODE_DATETIME_FORMAT), "\"");
    } else {
      endExpression = "".concat(attribute, " <").concat(inclusiveEnd ? "=" : "", " \"").concat(valueEnd.format(_constant.ENCODE_DATE_FORMAT), "\"");
    }
  }

  var expression;

  if (valueStart && valueEnd) {
    expression = "".concat(startExpression, " and ").concat(endExpression);
  } else if (valueStart) {
    expression = startExpression;
  } else {
    expression = endExpression;
  }

  if (operator === "is_not") {
    expression = "not (".concat(expression, ")");
  }

  return expression;
}

function getCondition(attribute, value, operator, invert, configuration, isFirstInBranch) {
  var key = attribute.key,
      type = attribute.type,
      objectTypeId = attribute.objectTypeId;
  var attributeName = key.split(".").pop();
  var apiOperator = {
    is: "is",
    is_not: "is_not",
    is_greater: ">",
    is_less: "<",
    is_greater_or_equal: ">=",
    is_less_or_equal: "<=",
    in: "in",
    not_in: "not_in",
    contains: "like",
    not_contains: "not_like",
    starts_with: "like",
    ends_with: "like",
    set: "set",
    not_set: "not_set"
  }[operator];

  if (attributeName.startsWith(_util.CUSTOM_ATTRIBUTE_KEY_PREFIX)) {
    // Logically, a  custom attribute link filter with `not_in` operator would look like this (notice the `not_in` operator after `to_id`):
    // select name, id from Task
    //    where (project.status is \"active\")
    //       and (custom_attribute_links any (configuration_id is \"b06cb185-0745-4252-9c92-4ae4f59165ad\"
    //             and to_id not_in (\"459e5ab6-4265-11df-80c8-002219661452\")))
    // Unfortunately, due to how custom attribute links are stored (sparsely),
    // this will not include tasks which have no links at all for the given configuration_id
    // Instead, we have to inverse the entire `in` query, as such:
    // select name, id from Task
    //    where (project.status is \"active\")
    //       and not (custom_attribute_links any (configuration_id is \"b06cb185-0745-4252-9c92-4ae4f59165ad\"
    //                 and to_id in (\"459e5ab6-4265-11df-80c8-002219661452\")))
    var linkOperator = attribute.type === "link" && apiOperator === "not_in" ? "in" : operator;

    var _condition = getCondition({
      type: type,
      key: "value",
      filterCondition: attribute.filterCondition
    }, value, linkOperator, invert, configuration);

    var configurationId = attributeName.replace(new RegExp("".concat(_util.CUSTOM_ATTRIBUTE_LINK_FROM_KEY_PREFIX, "|").concat(_util.CUSTOM_ATTRIBUTE_LINK_TO_KEY_PREFIX)), "");
    var linkPropertyName = attribute.options && attribute.options.isIncomingLink ? "custom_attribute_links_from" : "custom_attribute_links";
    var customAttributePropertyName = attribute.type === "link" ? linkPropertyName : "custom_attributes";
    var returnValue = "".concat(customAttributePropertyName, " any (configuration_id is \"").concat(configurationId, "\" and ").concat(_condition, ")");

    if (attribute.type === "link" && apiOperator === "not_in") {
      return "not (".concat(returnValue, ")");
    }

    return returnValue;
  }

  var condition;

  if (attribute.filterCondition) {
    condition = attribute.filterCondition(apiOperator || operator, value, configuration);
  } else if (type === "string") {
    var apiValue = value;

    if (operator === "contains" || operator === "not_contains") {
      apiValue = "%".concat(apiValue, "%");
    } else if (operator === "starts_with") {
      apiValue = "".concat(apiValue, "%");
    } else if (operator === "ends_with") {
      apiValue = "%".concat(apiValue);
    }

    condition = "".concat(attributeName, " ").concat(apiOperator, " \"").concat(apiValue, "\"");
  } else if (type === "number" || type === "integer") {
    condition = "".concat(attributeName, " ").concat(apiOperator, " \"").concat(value, "\"");
  } else if (type === "date-time") {
    condition = toDateRangeExpression(value, attributeName, operator, configuration, true);
  } else if (type === "enumerator") {
    var _apiValue = value;

    if ((0, _isArray.default)(_apiValue)) {
      _apiValue = "(\"".concat(_apiValue.join('", "'), "\")");
    } else if (_apiValue === null) {
      _apiValue = '""';
    }

    condition = "".concat(attributeName, " ").concat(apiOperator, " ").concat(_apiValue);
  } else if (type === "boolean") {
    var _apiValue2 = value === true ? "True" : "False";

    condition = "".concat(attributeName, " ").concat(apiOperator, " ").concat(_apiValue2);
  } else {
    condition = "".concat(attributeName, " ").concat(apiOperator, " ").concat(value);
  }

  if (objectTypeId && isFirstInBranch) {
    condition = "".concat(condition, " and object_type_id is \"").concat(objectTypeId, "\"");
  }

  if (invert) {
    return "not (".concat(condition, ")");
  }

  return condition;
}

function isFilterActive(_ref, type) {
  var disable = _ref.disable,
      operator = _ref.operator,
      value = _ref.value;

  if (type === "date-time" && value && value.mode === "date" && !value.date) {
    // If mode is date the date must be set for the filter to be active.
    return false;
  }

  if (disable) {
    return false;
  }

  return value !== null || operator === "set" || operator === "not_set";
}

function toQueryExpression(data, attributes, schemaId, schemas) {
  var configuration = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};
  var activeFilters = data.items.filter(function (item) {
    var attribute = attributes[item.key];
    return isFilterActive(item, attribute.type);
  });
  var matchKeyword = data.match === "all" ? "and" : "or";
  var tree = activeFilters.reduce(function (accumulator, item) {
    var key = item.key,
        value = item.value,
        operator = item.operator,
        invert = item.invert;
    var attribute = attributes[key];
    var parts = attribute.relation ? attribute.relation.split(".") : [];
    var fromSchema = schemas.find(function (schema) {
      return schema.id === schemaId;
    });

    if (parts.length) {
      var traverseItem = accumulator;
      parts.forEach(function (part, index) {
        var details = (0, _util.getPolymorphicRelationDetails)(part);
        var property;

        if (details) {
          property = fromSchema.properties[details.name];
        } else {
          property = fromSchema.properties[part];
        }

        var relationshipCondition;
        var ref;

        if (property.type === "array") {
          ref = property.items.$ref;
          relationshipCondition = "any";
        } else {
          ref = property.$ref;
          relationshipCondition = "has";
        }

        var traverseBranchKey = attribute.objectTypeSchemaId && details ? "".concat(details.name, "[").concat(attribute.objectTypeSchemaId, "]") : part;

        if (traverseItem.branch[traverseBranchKey] === undefined) {
          traverseItem.branch[traverseBranchKey] = {
            relationshipCondition: relationshipCondition,
            branch: {},
            condition: []
          };
        }

        traverseItem = traverseItem.branch[traverseBranchKey];

        if (index === parts.length - 1) {
          traverseItem.condition.push(getCondition(attribute, value, operator, invert, configuration, traverseItem.condition.length === 0));
        }

        if (details) {
          fromSchema = schemas.find(function (schema) {
            return schema.id === details.targetSchemaId;
          });
        } else {
          fromSchema = schemas.find(function (schema) {
            return schema.id === ref;
          });
        }
      });
    } else {
      accumulator.condition.push(getCondition(attribute, value, operator, invert, configuration));
    }

    return accumulator;
  }, {
    branch: {},
    condition: []
  });

  function getConditionFromBranch(_ref2) {
    var condition = _ref2.condition,
        branch = _ref2.branch;
    var expression = "";

    if (condition.length) {
      expression += "".concat(condition.join(" ".concat(matchKeyword, " ")));
    }

    var relationshipConditions = Object.entries(branch).map(function (_ref3) {
      var _ref4 = _slicedToArray(_ref3, 2),
          branchKey = _ref4[0],
          branchValue = _ref4[1];

      return "".concat(branchKey, " ").concat(branchValue.relationshipCondition, " (").concat(getConditionFromBranch(branchValue), ")");
    });

    if (relationshipConditions.length && expression.length) {
      return "".concat(expression, " ").concat(matchKeyword, " (").concat(relationshipConditions.join(" ".concat(matchKeyword, " ")), ")");
    } else if (relationshipConditions.length) {
      return relationshipConditions.join(" ".concat(matchKeyword, " "));
    } else if (expression.length) {
      return expression;
    }

    return [];
  }

  var query = getConditionFromBranch(tree);
  return query;
}