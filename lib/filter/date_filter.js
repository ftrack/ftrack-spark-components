"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.object.assign.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.array.find.js");

require("core-js/modules/es6.array.map.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _checkbox = _interopRequireDefault(require("react-toolbox/lib/checkbox"));

var _reactIntl = require("react-intl");

var _picker = require("../picker");

var _moment = _interopRequireDefault(require("moment"));

var _isString = _interopRequireDefault(require("lodash/isString"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _style = _interopRequireDefault(require("./style"));

var _recompose = require("recompose");

var _hoc = require("../util/hoc");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var messages = (0, _reactIntl.defineMessages)({
  "mode-this-week": {
    "id": "overview.date-range-filter.mode-this-week",
    "defaultMessage": "This week"
  },
  "mode-this-month": {
    "id": "overview.date-range-filter.mode-this-month",
    "defaultMessage": "This month"
  },
  "mode-custom-range": {
    "id": "overview.date-range-filter.mode-custom-range",
    "defaultMessage": "Date range"
  },
  start: {
    "id": "overview.date-range-input.start",
    "defaultMessage": "Start"
  },
  end: {
    "id": "overview.date-range-input.end",
    "defaultMessage": "End"
  },
  date: {
    "id": "overview.date-range-input.date",
    "defaultMessage": "Date"
  },
  "mode-today": {
    "id": "overview.date-range-input.mode-today",
    "defaultMessage": "Today"
  },
  "mode-yesterday": {
    "id": "overview.date-range-input.mode-yesterday",
    "defaultMessage": "Yesterday"
  },
  "mode-date": {
    "id": "overview.date-range-input.mode-date",
    "defaultMessage": "Date"
  },
  "mode-tomorrow": {
    "id": "overview.date-range-input.mode-tomorrow",
    "defaultMessage": "Tomorrow"
  },
  "mode-next-week": {
    "id": "overview.date-range-filter.mode-next-week",
    "defaultMessage": "Next week"
  },
  "mode-next-month": {
    "id": "overview.date-range-filter.mode-next-month",
    "defaultMessage": "Next month"
  }
});

function formatDate(date) {
  return (0, _moment.default)(date).format("MMM Do, YYYY");
}

var DateRangeInput_ = /*#__PURE__*/function (_Component) {
  _inherits(DateRangeInput_, _Component);

  var _super = _createSuper(DateRangeInput_);

  function DateRangeInput_() {
    _classCallCheck(this, DateRangeInput_);

    return _super.apply(this, arguments);
  }

  _createClass(DateRangeInput_, [{
    key: "handleDateChange",
    value: function handleDateChange(field, value) {
      this.props.onChange(Object.assign({}, this.props.value, _defineProperty({}, field, value)));
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          _this$props$value = _this$props.value,
          value = _this$props$value === void 0 ? {} : _this$props$value,
          isSundayFirstDayOfWeek = _this$props.isSundayFirstDayOfWeek;
      var valueStart = null;
      var valueEnd = null;

      if (value.start) {
        valueStart = (0, _isString.default)(value.start) ? new Date(value.start) : value.start;
      }

      if (value.end) {
        valueEnd = (0, _isString.default)(value.end) ? new Date(value.end) : value.end;
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: _style.default["custom-date-range-wrapper"],
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_picker.DatePicker, {
          label: this.props.intl.formatMessage(messages.start),
          inputFormat: formatDate,
          onChange: this.handleDateChange.bind(this, "start"),
          value: valueStart,
          sundayFirstDayOfWeek: isSundayFirstDayOfWeek,
          autoOk: true
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_picker.DatePicker, {
          label: this.props.intl.formatMessage(messages.end),
          inputFormat: formatDate,
          onChange: this.handleDateChange.bind(this, "end"),
          value: valueEnd,
          sundayFirstDayOfWeek: isSundayFirstDayOfWeek,
          autoOk: true
        })]
      });
    }
  }]);

  return DateRangeInput_;
}(_react.Component);

DateRangeInput_.propTypes = {
  intl: _reactIntl.intlShape.isRequired,
  value: _propTypes.default.string,
  onChange: _propTypes.default.func,
  isSundayFirstDayOfWeek: _propTypes.default.bool
};
DateRangeInput_.defaultProps = {
  isSundayFirstDayOfWeek: false
};
var DateRangeInput = (0, _safe_inject_intl.default)(DateRangeInput_);

var DateInput_ = /*#__PURE__*/function (_Component2) {
  _inherits(DateInput_, _Component2);

  var _super2 = _createSuper(DateInput_);

  function DateInput_() {
    var _this;

    _classCallCheck(this, DateInput_);

    _this = _super2.call(this);
    _this.handleDateChange = _this.handleDateChange.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(DateInput_, [{
    key: "handleDateChange",
    value: function handleDateChange(value) {
      this.props.onChange(Object.assign({}, this.props.value, {
        date: value
      }));
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          _this$props2$value = _this$props2.value,
          value = _this$props2$value === void 0 ? {} : _this$props2$value,
          isSundayFirstDayOfWeek = _this$props2.isSundayFirstDayOfWeek;
      var valueDate = null;

      if (value.date) {
        valueDate = (0, _isString.default)(value.date) ? new Date(value.date) : value.date;
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _style.default["custom-date-range-wrapper"],
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_picker.DatePicker, {
          label: this.props.intl.formatMessage(messages.date),
          inputFormat: formatDate,
          onChange: this.handleDateChange,
          value: valueDate,
          sundayFirstDayOfWeek: isSundayFirstDayOfWeek,
          autoOk: true
        })
      });
    }
  }]);

  return DateInput_;
}(_react.Component);

DateInput_.propTypes = {
  intl: _reactIntl.intlShape.isRequired,
  value: _propTypes.default.string,
  onChange: _propTypes.default.func,
  isSundayFirstDayOfWeek: _propTypes.default.bool
};
DateInput_.defaultProps = {
  isSundayFirstDayOfWeek: false
};
var DateInput = (0, _safe_inject_intl.default)(DateInput_);

var DateRangeFilter = /*#__PURE__*/function (_Component3) {
  _inherits(DateRangeFilter, _Component3);

  var _super3 = _createSuper(DateRangeFilter);

  function DateRangeFilter(props) {
    var _this2;

    _classCallCheck(this, DateRangeFilter);

    _this2 = _super3.call(this, props);
    _this2.modes = [{
      id: "this_month",
      i18n_label: messages["mode-this-month"],
      value: {
        mode: "this_month"
      }
    }, {
      id: "this_week",
      i18n_label: messages["mode-this-week"],
      value: {
        mode: "this_week"
      }
    }, {
      id: "yesterday",
      i18n_label: messages["mode-yesterday"],
      value: {
        mode: "yesterday"
      }
    }, {
      id: "today",
      i18n_label: messages["mode-today"],
      value: {
        mode: "today"
      }
    }, {
      id: "tomorrow",
      i18n_label: messages["mode-tomorrow"],
      value: {
        mode: "tomorrow"
      }
    }, {
      id: "next_week",
      i18n_label: messages["mode-next-week"],
      value: {
        mode: "next_week"
      }
    }, {
      id: "next_month",
      i18n_label: messages["mode-next-month"],
      value: {
        mode: "next_month"
      }
    }, {
      id: "date",
      i18n_label: messages["mode-date"],
      component: DateInput,
      value: {
        mode: "date"
      }
    }, {
      id: "custom_range",
      i18n_label: messages["mode-custom-range"],
      component: DateRangeInput,
      value: {
        mode: "custom_range"
      }
    }];

    if (props.modes) {
      _this2.modes = _this2.modes.filter(function (mode) {
        return props.modes.indexOf(mode.id) !== -1;
      });
    }

    _this2.state = {};
    return _this2;
  }

  _createClass(DateRangeFilter, [{
    key: "handleModeChange",
    value: function handleModeChange(modeId, modeValue) {
      var nextModeId = null;
      var nextValue = null;
      var currentModeId = this.props.value && this.props.value.mode || null;

      if (currentModeId !== modeId && modeValue) {
        nextModeId = modeId;
        nextValue = this.modes.find(function (item) {
          return item.id === nextModeId;
        }).value;
      }

      this.setState({
        value: nextValue
      });
      this.props.onChange(nextValue);
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var _this$props3 = this.props,
          isSundayFirstDayOfWeek = _this$props3.isSundayFirstDayOfWeek,
          _this$props3$value = _this$props3.value,
          value = _this$props3$value === void 0 ? {} : _this$props3$value,
          onChange = _this$props3.onChange;
      return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        children: this.modes.map(function (mode) {
          var id = mode.id;
          var currentMode = id === (value && value.mode);
          return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_checkbox.default, {
              className: _style.default["filter-checkbox"],
              checked: currentMode,
              label: _this3.props.intl.formatMessage(mode.i18n_label),
              onChange: _this3.handleModeChange.bind(_this3, id)
            }), currentMode && mode.component ? /*#__PURE__*/(0, _react.createElement)(mode.component, {
              value: value,
              onChange: onChange,
              isSundayFirstDayOfWeek: isSundayFirstDayOfWeek
            }) : null]
          }, id);
        })
      });
    }
  }]);

  return DateRangeFilter;
}(_react.Component);

DateRangeFilter.propTypes = {
  intl: _reactIntl.intlShape.isRequired,
  value: _propTypes.default.object,
  onChange: _propTypes.default.func,
  modes: _propTypes.default.arrayOf(_propTypes.default.string),
  isSundayFirstDayOfWeek: _propTypes.default.bool
};

var _default = (0, _recompose.compose)(_safe_inject_intl.default, (0, _hoc.withSettings)({
  isSundayFirstDayOfWeek: false
}))(DateRangeFilter);

exports.default = _default;