"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.array.slice.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.string.ends-with.js");

require("core-js/modules/es6.object.assign.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.regexp.match.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _input = _interopRequireDefault(require("react-toolbox/lib/input"));

var _dropdown = _interopRequireDefault(require("react-toolbox/lib/dropdown"));

var _switch = _interopRequireDefault(require("react-toolbox/lib/switch"));

var _material = require("@mui/material");

var _reactIntl = require("react-intl");

var _recompose = require("recompose");

var _classnames = _interopRequireDefault(require("classnames"));

var _heading = _interopRequireDefault(require("../heading"));

var _button_menu = _interopRequireDefault(require("../button_menu"));

var _date_filter = _interopRequireDefault(require("./date_filter"));

var _dropdown_filter = _interopRequireDefault(require("./dropdown_filter"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _icon_button = _interopRequireDefault(require("../icon_button"));

var _custom_attribute_links_filter = _interopRequireDefault(require("./custom_attribute_links_filter"));

var _attribute = require("../dataview/attribute");

var _util = require("../dataview/util");

var _style = _interopRequireDefault(require("./style.scss"));

var _switch_theme = _interopRequireDefault(require("./switch_theme.scss"));

var _dropdown_theme = _interopRequireDefault(require("./dropdown_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var messages = (0, _reactIntl.defineMessages)({
  "value-true": {
    "id": "ftrack-spark-components.filter.boolean-filter.value-true",
    "defaultMessage": "Yes"
  },
  "value-false": {
    "id": "ftrack-spark-components.filter.boolean-filter.value-false",
    "defaultMessage": "No"
  },
  "operator-is_less": {
    "id": "ftrack-spark-components.filter.operator-less-than",
    "defaultMessage": "Less than"
  },
  "operator-is_greater": {
    "id": "ftrack-spark-components.filter.operator-greater-than",
    "defaultMessage": "Greater than"
  },
  "operator-is_less_or_equal": {
    "id": "ftrack-spark-components.filter.operator-less-than-or-equal",
    "defaultMessage": "Less than or equal"
  },
  "operator-is_greater_or_equal": {
    "id": "ftrack-spark-components.filter.operator-greater-than-or-equal",
    "defaultMessage": "Greater than or equal"
  },
  "operator-is": {
    "id": "ftrack-spark-components.filter.operator-is",
    "defaultMessage": "Is"
  },
  "operator-is_not": {
    "id": "ftrack-spark-components.filter.operator-is-not",
    "defaultMessage": "Is not"
  },
  "operator-in": {
    "id": "ftrack-spark-components.filter.operator-in",
    "defaultMessage": "Includes"
  },
  "operator-not_in": {
    "id": "ftrack-spark-components.filter.operator-not-in",
    "defaultMessage": "Does not include"
  },
  "operator-set": {
    "id": "ftrack-spark-components.filter.operator-set",
    "defaultMessage": "Is set"
  },
  "operator-not_set": {
    "id": "ftrack-spark-components.filter.operator-not-set",
    "defaultMessage": "Is not set"
  },
  "operator-contains": {
    "id": "ftrack-spark-components.filter.operator-contains",
    "defaultMessage": "Contains"
  },
  "operator-not_contains": {
    "id": "ftrack-spark-components.filter.operator-not-contains",
    "defaultMessage": "Does not contain"
  },
  "operator-starts_with": {
    "id": "ftrack-spark-components.filter.operator-starts-with",
    "defaultMessage": "Starts with"
  },
  "operator-ends_with": {
    "id": "ftrack-spark-components.filter.operator-ends-with",
    "defaultMessage": "Ends with"
  },
  "filter-match-any-label": {
    "id": "ftrack-spark-components.filter.filter-match-any-label",
    "defaultMessage": "Match any filter"
  },
  "remove-filter": {
    "id": "ftrack-spark-components.filter.remove-filter",
    "defaultMessage": "Remove"
  },
  "invert-filter": {
    "id": "ftrack-spark-components.filter.invert-filter",
    "defaultMessage": "Invert"
  },
  "disable-filter": {
    "id": "ftrack-spark-components.filter.disable-filter",
    "defaultMessage": "Disable"
  },
  "unknown-attribute": {
    "id": "ftrack-spark-components.filter.unknown-attribute",
    "defaultMessage": "Unknown filter"
  }
});

function UnknownFilterField_(_ref) {
  var onRemoveFilter = _ref.onRemoveFilter,
      intl = _ref.intl;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: _style.default["filter-field"],
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default["filter-field-top"],
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _style.default["filter-header-operator"],
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_heading.default, {
          variant: "label",
          color: "secondary",
          children: intl.formatMessage(messages["unknown-attribute"])
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _style.default["filter-field-control-buttons"],
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_icon_button.default, {
          tooltip: intl.formatMessage(messages["remove-filter"]),
          className: _style.default["visible-on-hover"],
          icon: "clear",
          onClick: onRemoveFilter
        })
      })]
    }, "filter-top")
  });
}

UnknownFilterField_.propTypes = {
  intl: _reactIntl.intlShape.isRequired,
  onRemoveFilter: _propTypes.default.func.isRequired
};
var UnknownFilterField = (0, _recompose.compose)(_safe_inject_intl.default, (0, _recompose.withHandlers)({
  onRemoveFilter: function onRemoveFilter(props) {
    return function () {
      return props.onRemoveFilter(props.index);
    };
  }
}))(UnknownFilterField_);

function FilterField_(_ref2) {
  var attribute = _ref2.attribute,
      value = _ref2.value,
      operator = _ref2.operator,
      index = _ref2.index,
      invert = _ref2.invert,
      disable = _ref2.disable,
      intl = _ref2.intl,
      onValueChange = _ref2.onValueChange,
      onOperatorChange = _ref2.onOperatorChange,
      onRemoveFilter = _ref2.onRemoveFilter,
      onDisableChange = _ref2.onDisableChange,
      onInvertChange = _ref2.onInvertChange,
      enableInvert = _ref2.enableInvert,
      disableRemove = _ref2.disableRemove;
  var type = attribute.type,
      operators = attribute.filterOperators;
  var FilterComponent = UnknownFilterField;
  var filterProps;

  if (attribute.filterComponent) {
    FilterComponent = attribute.filterComponent;
  } else if (type === "string") {
    FilterComponent = _input.default;
    filterProps = {
      className: _style.default["field-field-standard-input"]
    };
  } else if (type === "number" || type === "integer") {
    FilterComponent = _input.default;
    filterProps = {
      type: "number",
      className: _style.default["field-field-standard-input"]
    };
  } else if (type === "boolean") {
    FilterComponent = _dropdown.default;
    filterProps = {
      theme: _dropdown_theme.default,
      auto: true,
      source: [{
        label: intl.formatMessage(messages["value-true"]),
        value: true
      }, {
        label: intl.formatMessage(messages["value-false"]),
        value: false
      }]
    };
  } else if (type === "date-time") {
    FilterComponent = _date_filter.default;
  } else if (type === "enumerator") {
    var options;

    try {
      options = JSON.parse(attribute.config.data);
    } catch (err) {
      options = [];
    }

    FilterComponent = _dropdown_filter.default;
    filterProps = {
      theme: _dropdown_theme.default,
      items: options.map(function (item) {
        return {
          value: item.value,
          label: item.menu
        };
      }),
      multiSelect: true
    };
  } else if (type === "link") {
    FilterComponent = _custom_attribute_links_filter.default;
    filterProps = {
      className: _style.default["field-field-standard-input"],
      entityType: attribute.options.entityType,
      objectTypeId: attribute.options.objectTypeId,
      objectTypeName: attribute.options.objectTypeName,
      baseFilter: attribute.options.baseFilter
    };
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: _style.default["filter-field"],
    children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default["filter-field-top"],
      children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: _style.default["filter-header-operator"],
        children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(_heading.default, {
          variant: "label",
          color: "secondary",
          noWrap: true,
          children: [attribute.path && attribute.path.length || attribute.groupLabel || attribute.key.endsWith("$entities") ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.Box, {
            component: "span",
            sx: {
              mr: !attribute.key.endsWith("$entities") ? 0.5 : 0,
              textTransform: "capitalize"
            },
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_attribute.AttributeGroupLabel, {
              path: attribute.path || [],
              label: attribute.groupLabel
            })
          }) : null, !attribute.key.endsWith("$entities") && /*#__PURE__*/(0, _jsxRuntime.jsx)(_attribute.AttributeLabel, {
            attribute: attribute
          })]
        }), operators && operators.length ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_button_menu.default, {
          button: /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.Button, {
            sx: {
              minWidth: 0,
              fontSize: "fontSizeSmall",
              textTransform: "lowercase",
              fontWeight: "fontWeightThin",
              color: "text.secondary",
              height: function height(_ref3) {
                var spacing = _ref3.spacing;
                return spacing(2);
              },
              lineHeight: function lineHeight(_ref4) {
                var spacing = _ref4.spacing;
                return spacing(2);
              },
              px: 0.5
            },
            children: messages["operator-".concat(operator)] && intl.formatMessage(messages["operator-".concat(operator)]) || operator
          }),
          onClick: onOperatorChange,
          children: operators.map(function (item) {
            return /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.MenuItem, {
              children: messages["operator-".concat(item)] && intl.formatMessage(messages["operator-".concat(item)]) || item
            }, item);
          })
        }) : null, attribute.filterMessage ? /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
          className: (_style.default["filter-field-group-label"], _style.default.colorAccent2),
          children: "*"
        }) : null]
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: _style.default["filter-field-control-buttons"],
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_icon_button.default, {
          tooltip: intl.formatMessage(messages["disable-filter"]),
          className: disable ? "" : _style.default["visible-on-hover"],
          icon: "pause",
          onClick: onDisableChange
        }), enableInvert ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_icon_button.default, {
          tooltip: intl.formatMessage(messages["invert-filter"]),
          className: invert ? "" : _style.default["visible-on-hover"],
          icon: "exposure",
          onClick: onInvertChange
        }) : null, !disableRemove && /*#__PURE__*/(0, _jsxRuntime.jsx)(_icon_button.default, {
          tooltip: intl.formatMessage(messages["remove-filter"]),
          className: _style.default["visible-on-hover"],
          icon: "clear",
          onClick: onRemoveFilter
        })]
      })]
    }, "filter-top"), operator !== "set" && operator !== "not_set" ? /*#__PURE__*/(0, _jsxRuntime.jsx)(FilterComponent, _objectSpread({
      onChange: onValueChange,
      value: value
    }, filterProps), "filter-".concat(attribute.key, "-").concat(index)) : null, attribute.filterMessage ? /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: (0, _classnames.default)(_style.default["filter-field-group-label"], _style.default.colorAccent2),
      children: ["* ", /*#__PURE__*/(0, _jsxRuntime.jsx)(_attribute.AttributeGroupLabel, {
        label: intl.formatMessage(attribute.filterMessage)
      })]
    }) : null]
  });
}

FilterField_.propTypes = {
  attribute: _util.attributeShape.isRequired,
  value: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.number, _propTypes.default.bool, _propTypes.default.arrayOf(_propTypes.default.string)]),
  enableInvert: _propTypes.default.bool,
  disableRemove: _propTypes.default.bool,
  operator: _propTypes.default.string,
  index: _propTypes.default.number,
  invert: _propTypes.default.bool,
  disable: _propTypes.default.bool,
  intl: _reactIntl.intlShape.isRequired,
  onValueChange: _propTypes.default.func.isRequired,
  onOperatorChange: _propTypes.default.func.isRequired,
  onRemoveFilter: _propTypes.default.func.isRequired,
  onDisableChange: _propTypes.default.func.isRequired,
  onInvertChange: _propTypes.default.func.isRequired
};
FilterField_.defaultProps = {
  enableInvert: false
};
var FilterField = (0, _recompose.compose)(_safe_inject_intl.default, (0, _recompose.withHandlers)({
  onValueChange: function onValueChange(props) {
    return function (value) {
      return props.onValueChange(props.index, value);
    };
  },
  onOperatorChange: function onOperatorChange(props) {
    return function (operator) {
      return props.onOperatorChange(props.index, operator);
    };
  },
  onDisableChange: function onDisableChange(props) {
    return function () {
      return props.onDisableChange(props.index, !props.disable);
    };
  },
  onInvertChange: function onInvertChange(props) {
    return function () {
      return props.onInvertChange(props.index, !props.invert);
    };
  },
  onRemoveFilter: function onRemoveFilter(props) {
    return function () {
      return props.onRemoveFilter(props.index);
    };
  }
}))(FilterField_);

var FilterPanel = /*#__PURE__*/function (_Component) {
  _inherits(FilterPanel, _Component);

  var _super = _createSuper(FilterPanel);

  function FilterPanel(props) {
    var _this;

    _classCallCheck(this, FilterPanel);

    _this = _super.call(this, props);
    _this.onOperatorChange = _this.onOperatorChange.bind(_assertThisInitialized(_this));
    _this.onInvertChange = _this.onInvertChange.bind(_assertThisInitialized(_this));
    _this.onDisableChange = _this.onDisableChange.bind(_assertThisInitialized(_this));
    _this.onValueChange = _this.onValueChange.bind(_assertThisInitialized(_this));
    _this.onRemoveFilter = _this.onRemoveFilter.bind(_assertThisInitialized(_this));
    _this.onFilterMatchChange = _this.onFilterMatchChange.bind(_assertThisInitialized(_this));
    _this.onItemsUpdate = _this.onItemsUpdate.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(FilterPanel, [{
    key: "onValueChange",
    value: function onValueChange(index, value) {
      var items = this.props.data.items;

      var nextItems = _toConsumableArray(items);

      nextItems[index] = Object.assign({}, items[index], {
        value: value
      });
      this.onItemsUpdate(nextItems);
    }
  }, {
    key: "onOperatorChange",
    value: function onOperatorChange(index, operator) {
      var items = this.props.data.items;

      var nextItems = _toConsumableArray(items);

      nextItems[index] = Object.assign({}, items[index], {
        operator: operator
      });
      this.onItemsUpdate(nextItems);
    }
  }, {
    key: "onDisableChange",
    value: function onDisableChange(index, disable) {
      var items = this.props.data.items;

      var nextItems = _toConsumableArray(items);

      nextItems[index] = Object.assign({}, items[index], {
        disable: disable || undefined
      });
      this.onItemsUpdate(nextItems);
    }
  }, {
    key: "onInvertChange",
    value: function onInvertChange(index, invert) {
      var items = this.props.data.items;

      var nextItems = _toConsumableArray(items);

      nextItems[index] = Object.assign({}, items[index], {
        invert: invert || undefined
      });
      this.onItemsUpdate(nextItems);
    }
  }, {
    key: "onRemoveFilter",
    value: function onRemoveFilter(removeAtIndex) {
      var items = this.props.data.items;
      this.onItemsUpdate(items.filter(function (item, index) {
        return index !== removeAtIndex;
      }));
    }
  }, {
    key: "onItemsUpdate",
    value: function onItemsUpdate(items) {
      var _this$props = this.props,
          data = _this$props.data,
          onChange = _this$props.onChange;
      onChange(Object.assign({}, data, {
        items: items
      }));
    }
  }, {
    key: "onFilterMatchChange",
    value: function onFilterMatchChange(match) {
      var _this$props2 = this.props,
          data = _this$props2.data,
          onChange = _this$props2.onChange;
      onChange(Object.assign({}, data, {
        match: match
      }));
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props3 = this.props,
          title = _this$props3.title,
          _this$props3$data = _this$props3.data,
          items = _this$props3$data.items,
          match = _this$props3$data.match,
          attributes = _this$props3.attributes,
          className = _this$props3.className,
          disableRemove = _this$props3.disableRemove;
      var fields = items.map(function (item, index) {
        var attribute = attributes[item.key];

        if (!attribute) {
          return /*#__PURE__*/(0, _jsxRuntime.jsx)(UnknownFilterField, {
            onRemoveFilter: _this2.onRemoveFilter,
            index: index
          }, "".concat(item.key, "-").concat(index));
        }

        return /*#__PURE__*/(0, _jsxRuntime.jsx)(FilterField, {
          disableRemove: disableRemove,
          value: item.value,
          operator: item.operator,
          invert: !!item.invert,
          disable: !!item.disable,
          attribute: attribute,
          index: index,
          onOperatorChange: _this2.onOperatorChange,
          onDisableChange: _this2.onDisableChange,
          onInvertChange: _this2.onInvertChange,
          onValueChange: _this2.onValueChange,
          onRemoveFilter: _this2.onRemoveFilter
        }, "".concat(item.key, "-").concat(index));
      });
      var showAnySwitch = fields.length > 1;

      if (title || showAnySwitch) {
        return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
          className: className,
          children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("hgroup", {
            className: _style.default.heading,
            children: [showAnySwitch ? /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
              className: _style.default["filter-any-switch"],
              children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
                className: _style.default["filter-any-switch-label"],
                children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["filter-match-any-label"]))
              }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_switch.default, {
                theme: _switch_theme.default,
                checked: match === "any",
                onChange: function onChange() {
                  return _this2.onFilterMatchChange(match === "any" ? "all" : "any");
                }
              })]
            }) : null, /*#__PURE__*/(0, _jsxRuntime.jsx)(_heading.default, {
              variant: "subheading",
              color: "default",
              children: title
            })]
          }), fields]
        });
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: className,
        children: fields
      });
    }
  }]);

  return FilterPanel;
}(_react.Component);

FilterPanel.propTypes = {
  attributes: _propTypes.default.arrayOf(_util.attributeShape),
  data: _propTypes.default.shape({
    match: _propTypes.default.string,
    items: _propTypes.default.array
  }).isRequired,
  onChange: _propTypes.default.func,
  className: _propTypes.default.string,
  title: _propTypes.default.string,
  intl: _reactIntl.intlShape.isRequired,
  disableRemove: _propTypes.default.bool
};
FilterPanel.defaultProps = {
  title: "",
  disableRemove: false
};

var _default = (0, _safe_inject_intl.default)(FilterPanel);

exports.default = _default;