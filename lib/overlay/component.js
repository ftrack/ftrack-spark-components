"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _progress_bar = _interopRequireDefault(require("react-toolbox/lib/progress_bar"));

var _button = _interopRequireDefault(require("react-toolbox/lib/button"));

var _classnames = _interopRequireDefault(require("classnames"));

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _reveal = _interopRequireDefault(require("../reveal"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Overlay component.
 *
 * Displays a full-screen overlay which can be customized with various props.
 */
function Overlay(props) {
  var _classNames2;

  var className = props.className,
      active = props.active,
      fixed = props.fixed,
      icon = props.icon,
      loader = props.loader,
      progress = props.progress,
      title = props.title,
      message = props.message,
      details = props.details,
      dismissable = props.dismissable,
      onDismss = props.onDismss,
      dismissLabel = props.dismissLabel;

  var _classNames = (0, _classnames.default)(_style.default.outer, (_classNames2 = {}, _defineProperty(_classNames2, _style.default.active, active), _defineProperty(_classNames2, _style.default.fixed, fixed), _classNames2), className);

  var children = [];

  if (icon) {
    var iconElement = typeof icon === "string" ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
      value: icon,
      className: _style.default.icon
    }) : icon;
    children.push(iconElement);
  }

  if (loader) {
    var mode = progress === null ? "indeterminate" : "determinate";
    children.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_progress_bar.default, {
      className: _style.default.loader,
      type: "circular",
      mode: mode,
      value: progress
    }, "loader"));
  }

  if (title) {
    children.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("h3", {
      children: title
    }, "title"));
  }

  if (message) {
    children.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("h6", {
      className: _style.default.message,
      children: message
    }, "message"));
  }

  if (details) {
    children.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default.details,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reveal.default, {
        label: "Details",
        children: details
      })
    }, "details"));
  }

  if (dismissable) {
    children.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_button.default, {
      label: dismissLabel,
      onClick: onDismss,
      raised: true
    }, "dismiss-button"));
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: _classNames,
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default.inner,
      children: [children, props.children]
    })
  });
}

Overlay.propTypes = {
  active: _propTypes.default.bool,
  children: _propTypes.default.node,
  className: _propTypes.default.string,
  details: _propTypes.default.node,
  dismissable: _propTypes.default.bool,
  dismissLabel: _propTypes.default.node,
  fixed: _propTypes.default.bool,
  icon: _propTypes.default.node,
  loader: _propTypes.default.bool,
  message: _propTypes.default.node,
  onDismss: _propTypes.default.func,
  progress: _propTypes.default.number,
  title: _propTypes.default.node
};
Overlay.defaultProps = {
  className: "",
  active: false,
  fixed: false,
  loader: false,
  progress: null,
  title: null,
  message: null,
  details: null,
  dismissable: false,
  onDismss: null,
  dismissLabel: "Close"
};
var _default = Overlay;
exports.default = _default;