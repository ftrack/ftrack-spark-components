"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.array.slice.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["link", "size", "parent", "ancestors", "className", "color"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/** Return *entity* name. */
function _getName(entity) {
  return entity.full_name || entity.name;
}
/** Return joined names from *entities*. */


function _joinNames(entities) {
  return entities.map(_getName).join(" / ");
}
/**
 * EntityLink Component - display entity and parent names on multiple lines.
 */


function EntityLink(props) {
  var link = props.link,
      size = props.size,
      parent = props.parent,
      ancestors = props.ancestors,
      className = props.className,
      color = props.color,
      other = _objectWithoutProperties(props, _excluded);

  var _classNames = (0, _classnames.default)(_style.default["entity-link"], _defineProperty({}, _style.default.colorLight, color === "light"), className);

  var result = [];

  if (link && link.length) {
    var entity = link[link.length - 1];
    var entityAncestors = link.slice(0, -1);

    if (size === "large") {
      result.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("h5", {
        className: _style.default.entity,
        children: _getName(entity)
      }, entity.id));
    } else {
      result.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
        className: _style.default.entity,
        children: _getName(entity)
      }, entity.id));
    }

    var entityParent = entityAncestors[entityAncestors.length - 1];

    if (parent && entityParent) {
      result.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
        className: _style.default["entity-parent"],
        children: _getName(entityParent)
      }, entityParent.id));

      if (ancestors) {
        var parentAncestors = entityAncestors.slice(0, -1);
        result.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
          className: _style.default["entity-ancestors"],
          children: _joinNames(parentAncestors)
        }, "".concat(entityParent.id, "-ancestors")));
      }
    } else if (ancestors && entityAncestors.length) {
      result.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
        className: _style.default["entity-ancestors"],
        children: _joinNames(entityAncestors)
      }, "".concat(entity.id, "-ancestors")));
    }
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", _objectSpread(_objectSpread({
    className: _classNames
  }, other), {}, {
    children: result
  }));
}

EntityLink.propTypes = {
  link: _propTypes.default.arrayOf(_propTypes.default.shape({
    id: _propTypes.default.string.isRequired,
    name: _propTypes.default.string.isRequired
  })).isRequired,
  size: _propTypes.default.oneOf(["medium", "large"]),
  parent: _propTypes.default.bool,
  ancestors: _propTypes.default.bool,
  className: _propTypes.default.string,
  color: _propTypes.default.string
};
EntityLink.defaultProps = {
  className: "",
  size: "medium",
  parent: true,
  ancestors: true
};
var _default = EntityLink;
exports.default = _default;