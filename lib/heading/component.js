"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["children", "className", "variant", "color", "noWrap"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function Header(props) {
  var _classNames;

  var children = props.children,
      className = props.className,
      variant = props.variant,
      color = props.color,
      noWrap = props.noWrap,
      other = _objectWithoutProperties(props, _excluded);

  var classes = (0, _classnames.default)(_style.default.header, className, (_classNames = {}, _defineProperty(_classNames, _style.default.variantHeadline, variant === "headline"), _defineProperty(_classNames, _style.default.variantDisplay1, variant === "display1"), _defineProperty(_classNames, _style.default.variantTitle, variant === "title"), _defineProperty(_classNames, _style.default.variantSubheading, variant === "subheading"), _defineProperty(_classNames, _style.default.variantLabel, variant === "label"), _defineProperty(_classNames, _style.default.colorInherit, color === "inherit"), _defineProperty(_classNames, _style.default.colorPrimary, color === "primary"), _defineProperty(_classNames, _style.default.colorLight, color === "light"), _defineProperty(_classNames, _style.default.colorDefault, color === "default"), _defineProperty(_classNames, _style.default.colorSecondary, color === "secondary"), _defineProperty(_classNames, _style.default.colorLightSecondary, color === "lightSecondary"), _defineProperty(_classNames, _style.default.noWrap, noWrap), _classNames));
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("h2", _objectSpread(_objectSpread({
    className: classes
  }, other), {}, {
    children: children
  }));
}

Header.propTypes = {
  className: _propTypes.default.string,
  children: _propTypes.default.node,
  variant: _propTypes.default.oneOf(["display1", "headline", "title", "subheading", "label"]),
  color: _propTypes.default.oneOf(["inherit", "default", "primary", "light", "secondary"]),
  noWrap: _propTypes.default.bool
};
Header.defaultProps = {
  variant: "headline",
  color: "inherit",
  noWrap: false
};
var _default = Header;
exports.default = _default;