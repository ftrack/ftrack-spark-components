"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _moment = _interopRequireDefault(require("moment"));

var _button = _interopRequireDefault(require("react-toolbox/lib/button"));

var _reactIntl = require("react-intl");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _style = _interopRequireDefault(require("./style.scss"));

var _dark_button_theme = _interopRequireDefault(require("./dark_button_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["variant", "expiresAt"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var messages = (0, _reactIntl.defineMessages)({
  "free-period-expired": {
    "id": "ftrack-spark-components.trial-expiry-button.free-period-expired",
    "defaultMessage": "Free period expired"
  },
  "free-period-ends-in": {
    "id": "ftrack-spark-components.trial-expiry-button.free-period-ends-in",
    "defaultMessage": "Free period ends in {numberOfDays} days"
  },
  "free-period-ends-today": {
    "id": "ftrack-spark-components.trial-expiry-button.free-period-ends-today",
    "defaultMessage": "Free period ends today"
  },
  "free-period-ends-tomorrow": {
    "id": "ftrack-spark-components.trial-expiry-button.free-period-ends-tomorrow",
    "defaultMessage": "Free period ends tomorrow"
  },
  "upgrade-now": {
    "id": "ftrack-spark-components.trial-expiry-button.upgrade-now",
    "defaultMessage": "Upgrade now"
  }
});
/*
 * Trial expiry button with text to indicate how much time is left of trial.
 *
 * Takes a *expiresAt* to indicate when trial expires. If *expiresAt* is:
 *
 *   -   In the past the button will state "Trial expired".
 *   -   Today's date it will state "Today".
 *   -   Tomorrow's date it will state "Tomorrow".
 *   -   For other durations it will state "duration" days where duration is rounded to
 *       the closest integer number.
 *
 */

function TrialExpiryButton(_ref) {
  var variant = _ref.variant,
      expiresAt = _ref.expiresAt,
      props = _objectWithoutProperties(_ref, _excluded);

  var buttonTheme = variant === "dark" ? _dark_button_theme.default : undefined;
  var message = null;
  var msUntilExpiry = (0, _moment.default)(expiresAt).diff(_moment.default.utc());
  var localExpireAt = (0, _moment.default)(expiresAt).local();
  var daysUntilExpiry = Math.round((0, _moment.default)(expiresAt).diff(_moment.default.utc(), "hours") / 24.0);
  var isToday = localExpireAt.startOf("day").isSame((0, _moment.default)().startOf("day"));
  var isTomorrow = localExpireAt.startOf("day").isSame((0, _moment.default)().startOf("day").add(1, "days"));

  if (msUntilExpiry >= 0) {
    if (isToday) {
      message = /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["free-period-ends-today"]));
    } else if (isTomorrow) {
      message = /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["free-period-ends-tomorrow"]));
    } else {
      message = /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["free-period-ends-in"]), {}, {
        values: {
          numberOfDays: daysUntilExpiry
        }
      }));
    }
  } else {
    message = /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["free-period-expired"]));
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_button.default, _objectSpread(_objectSpread({
    theme: buttonTheme
  }, props), {}, {
    children: [message, /*#__PURE__*/(0, _jsxRuntime.jsx)("label", {
      className: _style.default.label,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["upgrade-now"]))
    })]
  }));
}

TrialExpiryButton.propTypes = {
  expiresAt: _propTypes.default.string,
  className: _propTypes.default.string,
  variant: _propTypes.default.string
};
TrialExpiryButton.defaultProps = {};

var _default = (0, _safe_inject_intl.default)(TrialExpiryButton);

exports.default = _default;