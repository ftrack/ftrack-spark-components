"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es7.array.includes.js");

var _react = require("react");

var _material = require("@mui/material");

var _ArrowDropDownOutlined = _interopRequireDefault(require("@mui/icons-material/ArrowDropDownOutlined"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["children", "size", "sx"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var DropdownButton = /*#__PURE__*/(0, _react.forwardRef)(function DropdownButton(_ref, ref) {
  var children = _ref.children,
      _ref$size = _ref.size,
      size = _ref$size === void 0 ? "medium" : _ref$size,
      _ref$sx = _ref.sx,
      sx = _ref$sx === void 0 ? {} : _ref$sx,
      restProps = _objectWithoutProperties(_ref, _excluded);

  var fontSizeIcon = (0, _react.useMemo)(function () {
    return ["small", "medium"].includes(size) ? "fontSizeRegular" : undefined;
  }, [size]);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.Button, _objectSpread(_objectSpread({
    ref: ref,
    variant: "outlined",
    size: size,
    sx: _objectSpread({
      pr: 0.5,
      textTransform: "unset",
      fontWeight: "fontWeightRegular",
      "& .MuiButton-endIcon": {
        m: 0
      },
      "& .MuiButton-endIcon>*:nth-of-type(1)": {
        fontSize: fontSizeIcon
      }
    }, sx)
  }, restProps), {}, {
    endIcon: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ArrowDropDownOutlined.default, {}),
    children: children
  }));
});
var _default = DropdownButton;
exports.default = _default;