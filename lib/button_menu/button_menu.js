"use strict";

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

var _react = require("react");

var _material = require("@mui/material");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ButtonMenu(_ref) {
  var button = _ref.button,
      children = _ref.children,
      onClick = _ref.onClick,
      dense = _ref.dense,
      _ref$anchorOrigin = _ref.anchorOrigin,
      anchorOrigin = _ref$anchorOrigin === void 0 ? {
    horizontal: "left",
    vertical: "bottom"
  } : _ref$anchorOrigin,
      transformOrigin = _ref.transformOrigin,
      _ref$sx = _ref.sx,
      sx = _ref$sx === void 0 ? {} : _ref$sx;

  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      anchorEl = _useState2[0],
      setAnchorEl = _useState2[1];

  var handleButtonClick = function handleButtonClick(event) {
    event.stopPropagation();

    if (Boolean(anchorEl)) {
      setAnchorEl(null);
    } else {
      setAnchorEl(event.currentTarget);
    }
  };

  var handleClose = function handleClose(event) {
    if (event !== null && event !== void 0 && event.stopPropagation) {
      event.stopPropagation();
    }

    setAnchorEl(null);
  };

  var handleItemClick = function handleItemClick(key, callback, event) {
    handleClose(event);

    if (callback) {
      callback();
    } else if (onClick) {
      onClick(key);
    }
  };

  var Button = /*#__PURE__*/(0, _react.cloneElement)(button, {
    onClick: handleButtonClick
  });
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
    children: [Button, /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.Menu, {
      sx: sx,
      anchorEl: anchorEl,
      open: Boolean(anchorEl),
      onClose: handleClose,
      MenuListProps: {
        dense: dense
      },
      anchorOrigin: anchorOrigin,
      transformOrigin: transformOrigin,
      children: _react.Children.map(children, function (child) {
        if ( /*#__PURE__*/(0, _react.isValidElement)(child)) {
          return /*#__PURE__*/(0, _react.cloneElement)(child, {
            onClick: child.props.onClick ? function (e) {
              return handleItemClick(null, child.props.onClick, e);
            } : function (e) {
              return handleItemClick(child.key, null, e);
            }
          });
        }

        return child;
      })
    })]
  });
}

ButtonMenu.propTypes = {
  button: _propTypes.default.element.isRequired,
  dense: _propTypes.default.bool,
  children: _propTypes.default.node,
  onClick: _propTypes.default.func,
  anchorOrigin: _propTypes.default.shape({
    horizontal: _propTypes.default.oneOf(["center", "left", "right"]),
    vertical: _propTypes.default.oneOf(["center", "bottom", "top"])
  }),
  transformOrigin: _propTypes.default.shape({
    horizontal: _propTypes.default.oneOf(["center", "left", "right"]),
    vertical: _propTypes.default.oneOf(["center", "bottom", "top"])
  }),
  sx: _propTypes.default.object
};
var _default = ButtonMenu;
exports.default = _default;