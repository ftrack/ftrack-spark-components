"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _button = require("react-toolbox/lib/button");

var _tooltip = _interopRequireDefault(require("react-toolbox/lib/tooltip"));

var _classnames = _interopRequireDefault(require("classnames"));

var _large_positive_theme = _interopRequireDefault(require("./large_positive_theme.scss"));

var _large_negative_theme = _interopRequireDefault(require("./large_negative_theme.scss"));

var _default_theme = _interopRequireDefault(require("./default_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["variant", "active", "className"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var RTIconButtonWithTooltip = (0, _tooltip.default)(_button.IconButton);
var THEMES = {
  default: _default_theme.default,
  "large-icon-positive": _large_positive_theme.default,
  "large-icon-negative": _large_negative_theme.default
};

function IconButton(_ref) {
  var variant = _ref.variant,
      active = _ref.active,
      className = _ref.className,
      props = _objectWithoutProperties(_ref, _excluded);

  var Element = props.tooltip ? RTIconButtonWithTooltip : _button.IconButton;
  var theme = THEMES[variant] || _default_theme.default;
  var classes = (0, _classnames.default)(className, _defineProperty({}, theme.active, active));
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Element, _objectSpread({
    theme: theme,
    className: classes
  }, props));
}

IconButton.propTypes = {
  tooltip: _propTypes.default.node,
  active: _propTypes.default.bool,
  variant: _propTypes.default.oneOf(["default", "large-icon-positive", "large-icon-negative"]),
  className: _propTypes.default.string
};
IconButton.defaultProps = {
  color: "default"
};
var _default = IconButton;
exports.default = _default;