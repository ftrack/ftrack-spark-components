"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _withHandlers = _interopRequireDefault(require("recompose/withHandlers"));

var _tooltip = _interopRequireDefault(require("react-toolbox/lib/tooltip"));

var _button = require("react-toolbox/lib/button");

var _reactIntl = require("react-intl");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["first", "current", "total", "onPreviousClick", "onNextClick", "onChangePage", "intl"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var TooltipIconButton = (0, _tooltip.default)(_button.IconButton);
var messages = (0, _reactIntl.defineMessages)({
  "current-text": {
    "id": "ftrack-spark-components.page-indicator.current-text",
    "defaultMessage": "Page {current} of {total}"
  },
  "previous-page-tooltip": {
    "id": "ftrack-spark-components.page-indicator.previous-page-tooltip",
    "defaultMessage": "Previous"
  },
  "next-page-tooltip": {
    "id": "ftrack-spark-components.page-indicator.next-page-tooltip",
    "defaultMessage": "Next"
  }
});

function PageIndicatorBase(_ref) {
  var first = _ref.first,
      current = _ref.current,
      total = _ref.total,
      onPreviousClick = _ref.onPreviousClick,
      onNextClick = _ref.onNextClick,
      onChangePage = _ref.onChangePage,
      intl = _ref.intl,
      props = _objectWithoutProperties(_ref, _excluded);

  var lastPage = total - 1;
  var currentPage = Math.max(Math.min(current, lastPage), first);
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", _objectSpread(_objectSpread({}, props), {}, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
      icon: "skip_previous",
      onClick: onPreviousClick,
      disabled: currentPage <= first,
      tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["previous-page-tooltip"])),
      tooltipPosition: "bottom"
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      className: _style.default.pageIndicatorText,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["current-text"]), {}, {
        values: {
          current: currentPage + 1,
          total: total
        }
      }))
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
      icon: "skip_next",
      onClick: onNextClick,
      disabled: currentPage >= lastPage,
      tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["next-page-tooltip"])),
      tooltipPosition: "bottom"
    })]
  }));
}

PageIndicatorBase.propTypes = {
  current: _propTypes.default.number,
  first: _propTypes.default.number,
  total: _propTypes.default.number,
  onChangePage: _propTypes.default.func,
  onPreviousClick: _propTypes.default.func,
  onNextClick: _propTypes.default.func,
  intl: _reactIntl.intlShape
};
PageIndicatorBase.defaultProps = {
  current: 0,
  first: 0,
  total: 1
};
var PageIndicator = (0, _compose.default)(function (BaseComponent) {
  return (0, _safe_inject_intl.default)(BaseComponent);
}, (0, _withHandlers.default)({
  onPreviousClick: function onPreviousClick(_ref2) {
    var current = _ref2.current,
        onChangePage = _ref2.onChangePage;
    return function (e) {
      e.stopPropagation();
      onChangePage(current > 0 ? current - 1 : current);
    };
  },
  onNextClick: function onNextClick(_ref3) {
    var current = _ref3.current,
        total = _ref3.total,
        onChangePage = _ref3.onChangePage;
    return function (e) {
      e.stopPropagation();
      onChangePage(current < total - 1 ? current + 1 : current);
    };
  }
}))(PageIndicatorBase);
var _default = PageIndicator;
exports.default = _default;