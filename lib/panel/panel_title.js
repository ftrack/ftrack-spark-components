"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _heading = _interopRequireDefault(require("../heading"));

var _classnames = _interopRequireDefault(require("classnames"));

var _panel_title = _interopRequireDefault(require("./panel_title.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["children", "rightAction", "rightActionClassName", "className"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function PanelTitle(_ref) {
  var children = _ref.children,
      rightAction = _ref.rightAction,
      rightActionClassName = _ref.rightActionClassName,
      className = _ref.className,
      props = _objectWithoutProperties(_ref, _excluded);

  var classes = (0, _classnames.default)(_panel_title.default.heading, className);
  var rightActionClasses = (0, _classnames.default)(_panel_title.default.rightAction, rightActionClassName);
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_heading.default, _objectSpread(_objectSpread({
    variant: "title",
    className: classes
  }, props), {}, {
    children: [children, /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: rightActionClasses,
      children: rightAction
    })]
  }));
}

PanelTitle.propTypes = {
  children: _propTypes.default.node,
  rightAction: _propTypes.default.func,
  className: _propTypes.default.string,
  rightActionClassName: _propTypes.default.string
};
var _default = PanelTitle;
exports.default = _default;