"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _panel = _interopRequireDefault(require("./panel"));

var _icon_button = _interopRequireDefault(require("../icon_button"));

var _classnames = _interopRequireDefault(require("classnames"));

var _expansion_panel = _interopRequireDefault(require("./expansion_panel.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["active", "title", "children", "onChange", "className", "titleProps"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ExpansionPanel(_ref) {
  var active = _ref.active,
      title = _ref.title,
      children = _ref.children,
      onChange = _ref.onChange,
      className = _ref.className,
      titleProps = _ref.titleProps,
      props = _objectWithoutProperties(_ref, _excluded);

  var classes = (0, _classnames.default)(_expansion_panel.default.root, className);
  var rightAction = /*#__PURE__*/(0, _jsxRuntime.jsx)(_icon_button.default, {
    variant: "default",
    icon: "expand_more",
    className: active ? _expansion_panel.default.iconActive : _expansion_panel.default.icon
  });
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_panel.default, _objectSpread(_objectSpread({
    title: title,
    rightAction: rightAction
  }, props), {}, {
    className: classes,
    titleProps: _objectSpread(_objectSpread({}, titleProps), {}, {
      onClick: function onClick() {
        onChange(!active);
      }
    }),
    children: active ? children : null
  }));
}

ExpansionPanel.propTypes = {
  active: _propTypes.default.bool,
  title: _propTypes.default.string,
  children: _propTypes.default.node,
  onChange: _propTypes.default.func,
  className: _propTypes.default.string
};
var _default = ExpansionPanel;
exports.default = _default;