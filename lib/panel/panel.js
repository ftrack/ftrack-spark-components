"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Panel;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _panel_title = _interopRequireDefault(require("./panel_title"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["description", "title", "children", "rightAction", "titleProps", "className", "spacing", "background", "contentClassName"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function Panel(_ref) {
  var _classNames;

  var description = _ref.description,
      title = _ref.title,
      children = _ref.children,
      rightAction = _ref.rightAction,
      titleProps = _ref.titleProps,
      className = _ref.className,
      _ref$spacing = _ref.spacing,
      spacing = _ref$spacing === void 0 ? 2 : _ref$spacing,
      _ref$background = _ref.background,
      background = _ref$background === void 0 ? "contained" : _ref$background,
      contentClassName = _ref.contentClassName,
      props = _objectWithoutProperties(_ref, _excluded);

  var classes = (0, _classnames.default)(_style.default.container, (_classNames = {}, _defineProperty(_classNames, _style.default.spacing0, spacing === 0), _defineProperty(_classNames, _style.default.spacing1, spacing === 1), _defineProperty(_classNames, _style.default.spacing2, spacing === 2), _defineProperty(_classNames, _style.default.backgroundContained, background === "contained"), _classNames), className);
  var contentClasses = (0, _classnames.default)(_style.default.content, contentClassName);
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", _objectSpread(_objectSpread({
    className: classes
  }, props), {}, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_panel_title.default, _objectSpread(_objectSpread({
      rightAction: rightAction
    }, titleProps), {}, {
      children: title
    })), description ? /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
      className: _style.default.description,
      children: description
    }) : null, children ? /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: contentClasses,
      children: children
    }) : null]
  }));
}

Panel.propTypes = {
  background: _propTypes.default.oneOf([null, "contained"]),
  spacing: _propTypes.default.oneOf([0, 1, 2]),
  title: _propTypes.default.node,
  description: _propTypes.default.string,
  children: _propTypes.default.node,
  rightAction: _propTypes.default.func,
  className: _propTypes.default.string,
  contentClassName: _propTypes.default.string,
  titleProps: _propTypes.default.node
};