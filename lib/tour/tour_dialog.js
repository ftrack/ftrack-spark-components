"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _react = require("react");

var _dialog = _interopRequireDefault(require("react-toolbox/lib/dialog"));

var _reactSwipeableViews = _interopRequireDefault(require("react-swipeable-views"));

var _reactSwipeableViewsUtils = require("react-swipeable-views-utils");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _tour_navigation_button = _interopRequireDefault(require("../tour/tour_navigation_button"));

var _tour_stepper = _interopRequireDefault(require("../tour/tour_stepper"));

var _tour_item = _interopRequireDefault(require("../tour/tour_item"));

var _style = _interopRequireDefault(require("./style.scss"));

var _dialog_theme = _interopRequireDefault(require("./dialog_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["steps", "onAction"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var BindKeyboardSwipeableViews = (0, _reactSwipeableViewsUtils.bindKeyboard)(_reactSwipeableViews.default);

var TourDialog = /*#__PURE__*/function (_Component) {
  _inherits(TourDialog, _Component);

  var _super = _createSuper(TourDialog);

  function TourDialog(props) {
    var _this;

    _classCallCheck(this, TourDialog);

    _this = _super.call(this, props);
    _this.state = {
      currentIndex: 0
    };
    _this.handleChange = _this.handleChange.bind(_assertThisInitialized(_this));
    _this.handlePrevious = _this.handlePrevious.bind(_assertThisInitialized(_this));
    _this.handleNext = _this.handleNext.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(TourDialog, [{
    key: "handleChange",
    value: function handleChange(step) {
      this.setState({
        currentIndex: step
      });
    }
  }, {
    key: "handlePrevious",
    value: function handlePrevious() {
      if (this.state.currentIndex === 0) {
        return;
      }

      this.handleChange(this.state.currentIndex - 1);
    }
  }, {
    key: "handleNext",
    value: function handleNext() {
      if (this.state.currentIndex >= this.props.steps.length - 1) {
        return;
      }

      this.handleChange(this.state.currentIndex + 1);
    }
  }, {
    key: "render",
    value: function render() {
      var currentIndex = this.state.currentIndex;

      var _this$props = this.props,
          steps = _this$props.steps,
          onAction = _this$props.onAction,
          props = _objectWithoutProperties(_this$props, _excluded);

      var classes = (0, _classnames.default)(_style.default.swipableWrapper, _defineProperty({}, _style.default.singleStep, steps.length === 1));
      return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_dialog.default, _objectSpread(_objectSpread({
        theme: _dialog_theme.default
      }, props), {}, {
        children: [steps.length > 1 ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_tour_navigation_button.default, {
          onClick: this.handlePrevious,
          hidden: this.state.currentIndex === 0,
          direction: "left"
        }) : null, /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            className: classes,
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(BindKeyboardSwipeableViews, {
              disabled: steps.length === 1,
              resistance: true,
              enableMouseEvents: true,
              index: currentIndex,
              onChangeIndex: this.handleChange,
              children: steps.map(function (step, index) {
                return /*#__PURE__*/(0, _jsxRuntime.jsx)(_tour_item.default, _objectSpread({
                  onAction: onAction
                }, step), index);
              })
            })
          }), steps.length > 1 ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_tour_stepper.default, {
            steps: steps,
            currentIndex: this.state.currentIndex,
            onClick: this.handleChange
          }) : null]
        }), steps.length > 1 ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_tour_navigation_button.default, {
          onClick: this.handleNext,
          hidden: !steps[currentIndex + 1],
          direction: "right"
        }) : null]
      }));
    }
  }]);

  return TourDialog;
}(_react.Component);

TourDialog.propTypes = {
  steps: _propTypes.default.arrayOf(_propTypes.default.shape({
    title: _propTypes.default.node,
    text: _propTypes.default.node,
    image: _propTypes.default.string,
    layout: _propTypes.default.string
  })),
  onAction: _propTypes.default.func
};
var _default = TourDialog;
exports.default = _default;