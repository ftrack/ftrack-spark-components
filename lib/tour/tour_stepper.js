"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function TourStepper(_ref) {
  var _onClick = _ref.onClick,
      currentIndex = _ref.currentIndex,
      steps = _ref.steps;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("ol", {
    className: _style.default.stepWrapper,
    children: steps.map(function (step, dotIndex) {
      var classes = (0, _classnames.default)(_style.default.stepper, _defineProperty({}, _style.default.stepperActive, currentIndex === dotIndex));
      return /*#__PURE__*/(0, _jsxRuntime.jsx)("li", {
        className: classes,
        onClick: function onClick() {
          _onClick(dotIndex);
        }
      }, dotIndex);
    })
  });
}

TourStepper.propTypes = {
  currentIndex: _propTypes.default.number,
  onClick: _propTypes.default.func,
  steps: _propTypes.default.arrayOf(_propTypes.default.shape({
    title: _propTypes.default.node,
    text: _propTypes.default.node,
    image: _propTypes.default.string
  }))
};
var _default = TourStepper;
exports.default = _default;