"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _button = require("react-toolbox/lib/button");

var _markdown = _interopRequireDefault(require("../markdown"));

var _heading = _interopRequireDefault(require("../heading"));

var _style = _interopRequireDefault(require("./style.scss"));

var _action_button_theme = _interopRequireDefault(require("./action_button_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function TourItem(_ref) {
  var _classNames;

  var title = _ref.title,
      text = _ref.text,
      image = _ref.image,
      layout = _ref.layout,
      actions = _ref.actions,
      onAction = _ref.onAction;
  var classes = (0, _classnames.default)(_style.default.row, (_classNames = {}, _defineProperty(_classNames, _style.default.imageLeft, layout === "image_left"), _defineProperty(_classNames, _style.default.imageRight, layout === "image_right"), _defineProperty(_classNames, _style.default.noImage, layout === "center"), _classNames));
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: classes,
    children: [image ? /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default.pictureWrapper,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _style.default.image,
        style: {
          backgroundImage: "url('".concat(image, "')")
        }
      })
    }) : null, /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default.contentWrapper,
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_heading.default, {
        color: "light",
        variant: "display1",
        className: _style.default.heading,
        children: title
      }), typeof text === "string" ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_markdown.default, {
        source: text,
        size: "large",
        className: _style.default.text
      }) : /*#__PURE__*/(0, _jsxRuntime.jsx)(_heading.default, {
        variant: "headline",
        className: _style.default.text,
        children: text
      }), actions ? /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _style.default.buttonContainer,
        children: actions.map(function (action) {
          return /*#__PURE__*/(0, _jsxRuntime.jsx)(_button.Button, {
            theme: _action_button_theme.default,
            className: action.className,
            label: action.text,
            primary: action.primary,
            onClick: function onClick() {
              onAction(action.value);
            },
            href: action.href,
            target: action.value === "link_external" ? "_blank" : "",
            rel: action.value === "link_external" ? "noopener noreferrer" : ""
          });
        })
      }) : null]
    })]
  });
}

TourItem.propTypes = {
  title: _propTypes.default.node,
  text: _propTypes.default.node,
  image: _propTypes.default.string,
  layout: _propTypes.default.string,
  onAction: _propTypes.default.func,
  actions: _propTypes.default.arrayOf(_propTypes.default.shape({
    text: _propTypes.default.string,
    value: _propTypes.default.string,
    primary: _propTypes.default.bool,
    href: _propTypes.default.node,
    className: _propTypes.default.string
  }))
};
TourItem.defaultProps = {
  layout: "image_right"
};
var _default = TourItem;
exports.default = _default;