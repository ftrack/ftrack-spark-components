"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.promise.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _loglevel = _interopRequireDefault(require("loglevel"));

var _moment = _interopRequireDefault(require("moment"));

var _uuid = require("uuid");

var _dialog = _interopRequireDefault(require("react-toolbox/lib/dialog"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactIntl = require("react-intl");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _overlay = _interopRequireDefault(require("../overlay"));

var _constant = require("../util/constant");

var _form = _interopRequireDefault(require("./form"));

var _create_project_success = _interopRequireDefault(require("./create_project_success"));

var _create_project_success_message = _interopRequireDefault(require("./create_project_success_message"));

var _style = _interopRequireDefault(require("./style.scss"));

var _slugify = _interopRequireDefault(require("./slugify"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var messages = (0, _reactIntl.defineMessages)({
  errorTitle: {
    "id": "ftrack-spark-components.create-project-dialog.error-title",
    "defaultMessage": "Failed to create project"
  },
  closeLabel: {
    "id": "ftrack-spark-components.create-project-dialog.close-label",
    "defaultMessage": "Close"
  },
  gotoLabel: {
    "id": "ftrack-spark-components.create-project-dialog.goto-label",
    "defaultMessage": "Go to project"
  },
  cancelLabel: {
    "id": "ftrack-spark-components.create-project-dialog.cancel-label",
    "defaultMessage": "Cancel"
  },
  createProjectLabel: {
    "id": "ftrack-spark-components.create-project-dialog.create-project-label",
    "defaultMessage": "Create project"
  }
});
/**
 * Create Project Dialog.
 */

var CreateProjectDialog = /*#__PURE__*/function (_Component) {
  _inherits(CreateProjectDialog, _Component);

  var _super = _createSuper(CreateProjectDialog);

  function CreateProjectDialog(props) {
    var _this;

    _classCallCheck(this, CreateProjectDialog);

    _this = _super.call(this, props);
    _this.state = {
      loading: false,
      success: false,
      projectId: null
    };
    _this.submitForm = _this.submitForm.bind(_assertThisInitialized(_this));
    _this.handleSubmit = _this.handleSubmit.bind(_assertThisInitialized(_this));
    _this.handleKeys = _this.handleKeys.bind(_assertThisInitialized(_this));
    _this.handlePrimaryAction = _this.handlePrimaryAction.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(CreateProjectDialog, [{
    key: "componentWillMount",
    value: function componentWillMount() {
      window.document.body.addEventListener("keydown", this.handleKeys);
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      if (this.state.success && !this.props.renderSuccess) {
        this.props.onProjectCreatedGoto(this.state.projectId, null);
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.document.body.removeEventListener("keydown", this.handleKeys);
    }
  }, {
    key: "handleKeys",
    value: function handleKeys(e) {
      // Submit form on command/ctrl + return.
      if ((e.ctrlKey || e.metaKey) && e.which === 13) {
        e.preventDefault();
        this.handlePrimaryAction();
      }
    }
  }, {
    key: "handlePrimaryAction",
    value: function handlePrimaryAction() {
      if (this.state.success) {
        this.props.onProjectCreatedGoto(this.state.projectId, null);
      } else {
        this.submitForm();
      }
    }
    /** Trigger form submission. */

  }, {
    key: "submitForm",
    value: function submitForm() {
      this.form.submit();
    }
  }, {
    key: "handleSubmit",
    value: function handleSubmit(_ref) {
      var _this2 = this;

      var formData = _ref.formData;
      this.setState({
        loading: true
      });
      var name = formData.name,
          full_name = formData.full_name;
      var code = name || (full_name ? (0, _slugify.default)(full_name) : null);

      var ensureUniqueProjectCode = function ensureUniqueProjectCode() {
        return new Promise(function (resolve) {
          if (code === (0, _slugify.default)(full_name)) {
            _this2.props.session.query("select id from Project where name is \"".concat(code, "\" limit 1")).then(function (response) {
              if (response.data.length) {
                code = "".concat(code, "_").concat((0, _uuid.v4)().substr(0, 5));
              }

              resolve();
            });
          } else {
            resolve();
          }
        });
      };

      ensureUniqueProjectCode().then(function () {
        _loglevel.default.debug("Submitting", formData);

        return _this2.props.session.create("Project", {
          color: formData.color.toUpperCase(),
          end_date: (0, _moment.default)(formData.end_date).format(_constant.ENCODE_DATETIME_FORMAT),
          full_name: formData.full_name,
          name: code,
          project_schema_id: formData.project_schema,
          start_date: (0, _moment.default)(formData.start_date).format(_constant.ENCODE_DATETIME_FORMAT),
          status: "active",
          thumbnail_id: formData.thumbnail_id,
          is_private: formData.is_private
        });
      }).then(function (response) {
        var projectId = response.data.id;

        _this2.setState({
          loading: false,
          success: true,
          projectId: projectId
        });

        if (_this2.props.onSuccess) {
          _this2.props.onSuccess(response.data.id);
        }
      }).catch(function (error) {
        _this2.setState({
          loading: false,
          error: error
        });
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var _this$props = this.props,
          intl = _this$props.intl,
          title = _this$props.title;
      var overlayProps = {};

      if (this.state.loading) {
        overlayProps = {
          active: true,
          loader: true
        };
      }

      if (this.state.error) {
        overlayProps = {
          active: true,
          title: intl.formatMessage(messages.errorTitle),
          details: this.state.error.message,
          dismissable: true,
          onDismss: function onDismss() {
            _this3.setState({
              error: null
            });
          }
        };
      }

      var actions = [];

      if (this.state.success) {
        actions = [{
          label: intl.formatMessage(messages.closeLabel),
          onClick: this.props.onProjectCreatedClose
        }, {
          label: intl.formatMessage(messages.gotoLabel),
          primary: true,
          onClick: this.handlePrimaryAction
        }];
      } else {
        actions = [{
          label: intl.formatMessage(messages.cancelLabel),
          onClick: this.props.onCancel
        }, {
          label: intl.formatMessage(messages.createProjectLabel),
          primary: true,
          onClick: this.handlePrimaryAction
        }];
      }

      var dialogClassNames = (0, _classnames.default)(_style.default.dialog, this.props.className);
      return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_dialog.default, {
        active: true,
        actions: actions,
        title: title,
        className: dialogClassNames,
        children: [this.state.success && this.props.renderSuccess ? this.props.renderSuccess({
          projectId: this.state.projectId,
          onGoto: this.props.onProjectCreatedGoto
        }) : /*#__PURE__*/(0, _jsxRuntime.jsx)(_form.default, {
          formRef: function formRef(node) {
            _this3.form = node;
          },
          session: this.props.session,
          attributes: this.props.attributes,
          onSubmit: this.handleSubmit,
          intlMessages: {}
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_overlay.default, _objectSpread({}, overlayProps))]
      });
    }
  }]);

  return CreateProjectDialog;
}(_react.Component);

CreateProjectDialog.propTypes = {
  onCancel: _propTypes.default.func.isRequired,
  onProjectCreatedClose: _propTypes.default.func,
  onProjectCreatedGoto: _propTypes.default.func.isRequired,
  session: _propTypes.default.shape({
    create: _propTypes.default.func.isRequired,
    query: _propTypes.default.func
  }).isRequired,
  className: _propTypes.default.string,
  attributes: _propTypes.default.arrayOf(_propTypes.default.string),
  renderSuccess: _propTypes.default.func,
  onSuccess: _propTypes.default.func,
  intl: _reactIntl.intlShape,
  title: _propTypes.default.string
};
CreateProjectDialog.defaultProps = {
  className: "",
  intlMessages: {
    errorTitle: "Failed to create project",
    closeLabel: "Close",
    gotoLabel: "Go to project",
    cancelLabel: "Cancel",
    createProjectLabel: "Create project"
  },
  renderSuccess: function renderSuccess(props) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_create_project_success.default, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_create_project_success_message.default, _objectSpread({}, props))
    });
  }
};

var _default = (0, _safe_inject_intl.default)(CreateProjectDialog);

exports.default = _default;