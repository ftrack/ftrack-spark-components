"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactIntl = require("react-intl");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var HELP_CENTER_URL = "https://www.ftrack.com/help";
var messages = (0, _reactIntl.defineMessages)({
  message: {
    "id": "ftrack-spark-components.create-project-dialog-success.message",
    "defaultMessage": "Your new project has been created."
  },
  "message-detail": {
    "id": "ftrack-spark-components.create-project-dialog-success.message-detail",
    "defaultMessage": "Go to project to manage tasks and users or click here to {inviteTeamMembersLink} to the project. Need help getting started? Check out this {gettingStartedLink}."
  },
  "invite-team-members": {
    "id": "ftrack-spark-components.create-project-dialog-success.invite-team-members",
    "defaultMessage": "invite team members"
  },
  "help-center": {
    "id": "ftrack-spark-components.create-project-dialog-success.help-center",
    "defaultMessage": "help center"
  }
});

function CreateProjectSuccessMessage(props) {
  var onInviteClick = function onInviteClick(event) {
    props.onGoto(props.projectId, "team");
    event.preventDefault();
  };

  var inviteTeamMembersLink = /*#__PURE__*/(0, _jsxRuntime.jsx)("a", {
    href: "#invite-team-members",
    onClick: onInviteClick,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["invite-team-members"]))
  });
  var gettingStartedLink = /*#__PURE__*/(0, _jsxRuntime.jsx)("a", {
    href: HELP_CENTER_URL,
    target: "_blank",
    rel: "noopener noreferrer",
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["help-center"]))
  });
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("p", {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.message)), /*#__PURE__*/(0, _jsxRuntime.jsx)("br", {}), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["message-detail"]), {}, {
      values: {
        inviteTeamMembersLink: inviteTeamMembersLink,
        gettingStartedLink: gettingStartedLink
      }
    }))]
  });
}

CreateProjectSuccessMessage.propTypes = {
  onGoto: _propTypes.default.func.isRequired,
  projectId: _propTypes.default.string.isRequired
};

var _default = (0, _safe_inject_intl.default)(CreateProjectSuccessMessage);

exports.default = _default;