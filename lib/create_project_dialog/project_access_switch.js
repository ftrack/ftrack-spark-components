"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactIntl = require("react-intl");

var _switch = require("../form/field/switch");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2019 ftrack
var messages = (0, _reactIntl.defineMessages)({
  "switch-label-public": {
    "id": "ftrack-spark-components.create-project-dialog.switch-label-public",
    "defaultMessage": "Private access off"
  },
  "switch-label-private": {
    "id": "ftrack-spark-components.create-project-dialog.switch-label-private",
    "defaultMessage": "Private access on"
  },
  "switch-description-open-review": {
    "id": "ftrack-spark-components.create-project-dialog.switch-description-open-review",
    "defaultMessage": "All users in the workspace have access"
  },
  "switch-description-open-studio": {
    "id": "ftrack-spark-components.create-project-dialog.switch-description-open-studio",
    "defaultMessage": "Users with roles set for open access projects can view this project"
  },
  "switch-description-private": {
    "id": "ftrack-spark-components.create-project-dialog.switch-description-private",
    "defaultMessage": "Users must be invited to view this project"
  }
});

function ProjectAccessSwitch(_ref) {
  var intl = _ref.intl,
      product = _ref.product,
      value = _ref.value,
      onChange = _ref.onChange;
  var label;
  var description;

  if (!value) {
    label = intl.formatMessage(messages["switch-label-public"]);
    description = intl.formatMessage(messages["switch-description-open-".concat(product)]);
  } else {
    label = intl.formatMessage(messages["switch-label-private"]);
    description = intl.formatMessage(messages["switch-description-private"]);
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_switch.SwitchInput, {
    label: label,
    description: description,
    onChange: onChange,
    value: value
  });
}

ProjectAccessSwitch.propTypes = {
  value: _propTypes.default.bool,
  intl: _reactIntl.intlShape.isRequired,
  product: _propTypes.default.oneOf(["review", "studio"]),
  onChange: _propTypes.default.func
};
ProjectAccessSwitch.defaultProps = {
  product: "review"
};

var _default = (0, _safe_inject_intl.default)(ProjectAccessSwitch);

exports.default = _default;