"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.regexp.replace.js");

var slugify = function slugify(string) {
  return (string || "").trim().toLowerCase().replace(/ /g, "_").replace(/([^a-zA-Z0-9._]+)/g, "");
};

var _default = slugify;
exports.default = _default;