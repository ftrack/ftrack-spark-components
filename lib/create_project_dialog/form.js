"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.string.includes.js");

require("core-js/modules/es7.array.includes.js");

require("core-js/modules/es6.object.assign.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _moment = _interopRequireDefault(require("moment"));

var _loglevel = _interopRequireDefault(require("loglevel"));

var _recompose = require("recompose");

var _debounce = _interopRequireDefault(require("lodash/debounce"));

var _ftrackJavascriptApi = require("ftrack-javascript-api");

var _reactIntl = require("react-intl");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _get_rjsf_selector = _interopRequireDefault(require("../form/widget/get_rjsf_selector"));

var _get_custom_field = _interopRequireDefault(require("../form/form_field/get_custom_field"));

var _remote_selector = _interopRequireDefault(require("../selector/remote_selector"));

var _form = _interopRequireDefault(require("../form"));

var _color = _interopRequireDefault(require("../form/field/color"));

var _thumbnail = _interopRequireDefault(require("../form/field/thumbnail"));

var _project_access_switch_field = _interopRequireDefault(require("./project_access_switch_field"));

var _constant = require("../util/constant");

var _style = _interopRequireDefault(require("./style.scss"));

var _slugify = _interopRequireDefault(require("./slugify"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var withRjsfSelector = function withRjsfSelector(BaseComponent) {
  return (0, _get_rjsf_selector.default)(BaseComponent);
};

var withCustomField = function withCustomField(BaseComponent) {
  return (0, _get_custom_field.default)(BaseComponent);
};

var ProjectSchemaField = (0, _recompose.compose)(withCustomField, withRjsfSelector, (0, _recompose.withProps)(function () {
  return {
    entityType: "ProjectSchema"
  };
}))(_remote_selector.default);
var messages = (0, _reactIntl.defineMessages)({
  "attribute-thumbnail-message": {
    "id": "ftrack-spark-components.create-project-form.attribute-thumbnail-message",
    "defaultMessage": "Drop image here or click to browse"
  },
  "attribute-title-full-name": {
    "id": "ftrack-spark-components.create-project-form.attribute-title-full-name",
    "defaultMessage": "Name"
  },
  "attribute-title-name": {
    "id": "ftrack-spark-components.create-project-form.attribute-title-name",
    "defaultMessage": "Code"
  },
  "attribute-title-project-schema": {
    "id": "ftrack-spark-components.create-project-form.attribute-title-project-schema",
    "defaultMessage": "Workflow"
  },
  "attribute-title-start-date": {
    "id": "ftrack-spark-components.create-project-form.attribute-title-start-date",
    "defaultMessage": "Start"
  },
  "attribute-title-end-date": {
    "id": "ftrack-spark-components.create-project-form.attribute-title-end-date",
    "defaultMessage": "End"
  },
  "attribute-help-name": {
    "id": "ftrack-spark-components.create-project-form.attribute-help-name",
    "defaultMessage": "A unique name used by the API and integrations."
  },
  "attribute-help-project-schema": {
    "id": "ftrack-spark-components.create-project-form.attribute-help-project-schema",
    "defaultMessage": "The project workflow defines what objects, types and statuses will be used for this project and can be configured in system settings."
  },
  "error-project-code-taken": {
    "id": "ftrack-spark-components.create-project-form.error-project-code-taken",
    "defaultMessage": "Project code must be unique."
  },
  "error-start-date": {
    "id": "ftrack-spark-components.create-project-form.error-start-date",
    "defaultMessage": "Start date must be before end."
  }
});
var fields = {
  color: _color.default,
  thumbnail: _thumbnail.default,
  project_schema: ProjectSchemaField,
  is_private_review: (0, _recompose.withProps)({
    product: "review"
  })(_project_access_switch_field.default),
  is_private_studio: (0, _recompose.withProps)({
    product: "studio"
  })(_project_access_switch_field.default)
};

function randomChoice(choices) {
  var index = Math.floor(Math.random() * choices.length);
  return choices[index];
}

var CreateProjectForm = /*#__PURE__*/function (_Component) {
  _inherits(CreateProjectForm, _Component);

  var _super = _createSuper(CreateProjectForm);

  function CreateProjectForm(props) {
    var _this;

    _classCallCheck(this, CreateProjectForm);

    _this = _super.call(this, props);
    _this.handleChange = _this.handleChange.bind(_assertThisInitialized(_this));
    _this.validate = _this.validate.bind(_assertThisInitialized(_this));
    _this.getSettings = _this.getSettings.bind(_assertThisInitialized(_this));
    _this.validateProjectCode = _this.validateProjectCode.bind(_assertThisInitialized(_this));
    var VALIDATE_PROJECT_CODE_DELAY = 400;
    _this.validateProjectCodeDebounced = (0, _debounce.default)(_this.validateProjectCode, VALIDATE_PROJECT_CODE_DELAY, {
      leading: false,
      trailing: true
    });
    var formatMessage = _this.props.intl.formatMessage;
    var formData = {
      full_name: "",
      start_date: (0, _moment.default)().startOf("day").format(_constant.DATETIME_ISO_FORMAT),
      end_date: (0, _moment.default)().startOf("day").add("month", "1").format(_constant.DATETIME_ISO_FORMAT),
      color: randomChoice(_constant.PROJECT_COLORS)
    };
    var formContext = {
      session: props.session,
      sundayFirstDayOfWeek: false
    };
    _this.state = {
      formData: formData,
      formContext: formContext
    };
    _this.schema = {
      type: "object",
      required: ["full_name", "name", "project_schema", "start_date", "end_date"],
      properties: {
        thumbnail_id: {
          type: "string"
        },
        color: {
          type: "string"
        },
        full_name: {
          type: "string",
          title: formatMessage(messages["attribute-title-full-name"]),
          default: ""
        },
        name: {
          type: "string",
          title: formatMessage(messages["attribute-title-name"])
        },
        project_schema: {
          type: "string",
          title: formatMessage(messages["attribute-title-project-schema"])
        },
        start_date: {
          type: "string",
          format: "date-time",
          title: formatMessage(messages["attribute-title-start-date"])
        },
        end_date: {
          type: "string",
          format: "date-time",
          title: formatMessage(messages["attribute-title-end-date"])
        },
        is_private: {
          type: "boolean"
        }
      }
    };
    _this.schema.required = _this.schema.required.filter(function (attribute) {
      return _this.props.attributes.includes(attribute);
    });
    _this.schema.properties = _this.props.attributes.reduce(function (result, key) {
      result[key] = _this.schema.properties[key];
      return result;
    }, {});

    var isStudio = _this.props.attributes.includes("project_schema");

    var uiSchema = {
      name: {
        reveal: true,
        "ui:help": formatMessage(messages["attribute-help-name"])
      },
      full_name: {
        "ui:autofocus": true
      },
      project_schema: {
        "ui:field": "project_schema",
        "ui:help": formatMessage(messages["attribute-help-project-schema"])
      },
      thumbnail_id: {
        classNames: _style.default.thumbnailField,
        "ui:field": "thumbnail",
        thumbnailSize: "xlarge",
        message: formatMessage(messages["attribute-thumbnail-message"])
      },
      start_date: {
        "ui:widget": "date",
        classNames: _style.default.dateField
      },
      end_date: {
        "ui:widget": "date",
        classNames: _style.default.dateField
      },
      is_private: {
        "ui:field": isStudio ? "is_private_studio" : "is_private_review",
        classNames: _style.default.privateField
      },
      color: {
        "ui:displayLabel": false,
        "ui:field": "color",
        classNames: _style.default.colorField
      }
    };
    _this.uiSchema = _this.props.attributes.reduce(function (result, key) {
      result[key] = uiSchema[key];
      return result;
    }, {});
    return _this;
  }

  _createClass(CreateProjectForm, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.getSettings();
    }
  }, {
    key: "getSettings",
    value: function getSettings() {
      var _this2 = this;

      this.props.session.call([_ftrackJavascriptApi.operation.query('select value from Setting where group is "DEFAULT" and name is "workflowid" limit 1'), _ftrackJavascriptApi.operation.query('select value from Setting where group is "TIME" and name is "week_startday" limit 1')]).then(function (responses) {
        var defaultSchemaId = responses[0].data[0] ? responses[0].data[0].value : undefined;

        _loglevel.default.debug("Retrieved default project schema", defaultSchemaId);

        var weekStartDay = responses[1].data[0] && responses[1].data[0].value;

        _loglevel.default.debug("Retrieved week startday", weekStartDay);

        var sundayFirstDayOfWeek = weekStartDay === "0";

        _this2.setState({
          formContext: Object.assign({}, _this2.state.formContext, {
            sundayFirstDayOfWeek: sundayFirstDayOfWeek
          }),
          formData: Object.assign({}, _this2.state.formData, {
            project_schema: defaultSchemaId
          })
        });
      });
    }
  }, {
    key: "transformFormData",
    value: function transformFormData(formData) {
      var nextFormData = formData;

      if (nextFormData.full_name !== this.state.formData.full_name) {
        if (!this.state.formData.name || this.state.formData.name === (0, _slugify.default)(this.state.formData.full_name)) {
          nextFormData = Object.assign({}, formData, {
            name: (0, _slugify.default)(nextFormData.full_name)
          });
        }
      }

      return nextFormData;
    }
  }, {
    key: "validateProjectCode",
    value: function validateProjectCode() {
      var _this3 = this;

      var name = this.state.formData.name;

      if (name === this._lastValidatedName) {
        return;
      }

      this._lastValidatedName = name;
      this.setState({
        projectCodeTaken: false
      });
      this.props.session.query("select id from Project where name is \"".concat(name, "\" limit 1")).then(function (response) {
        if (response.data.length) {
          _this3.setState({
            projectCodeTaken: true
          });
        }
      });
    }
  }, {
    key: "validate",
    value: function validate(formData, errors) {
      var formatMessage = this.props.intl.formatMessage;

      if (this.state.projectCodeTaken) {
        errors.name.addError(formatMessage(messages["error-project-code-taken"]));
      }

      if (formData.end_date && formData.end_date < formData.start_date) {
        errors.start_date.addError(formatMessage(messages["error-start-date"]));
      }

      return errors;
    }
  }, {
    key: "handleChange",
    value: function handleChange(_ref) {
      var formData = _ref.formData;

      if (this.props.attributes.indexOf("name") !== -1) {
        this.setState({
          formData: this.transformFormData(formData)
        });
        this.validateProjectCodeDebounced();
      }
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_form.default, _objectSpread(_objectSpread({
        ref: this.props.formRef,
        schema: this.schema,
        uiSchema: this.uiSchema
      }, this.props), {}, {
        validate: this.validate,
        fields: fields,
        formContext: this.state.formContext,
        formData: this.state.formData,
        onChange: this.handleChange
      }));
    }
  }]);

  return CreateProjectForm;
}(_react.Component);

CreateProjectForm.propTypes = {
  intl: _reactIntl.intlShape.isRequired,
  onSubmit: _propTypes.default.func.isRequired,
  // eslint-disable-line
  session: _propTypes.default.object.isRequired,
  // eslint-disable-line
  intlMessages: _propTypes.default.object.isRequired,
  // eslint-disable-line
  attributes: _propTypes.default.arrayOf(_propTypes.default.string),
  formRef: _propTypes.default.string
};
CreateProjectForm.defaultProps = {
  attributes: ["thumbnail_id", "color", "full_name", "name", "project_schema", "start_date", "end_date", "is_private"]
};

var _default = (0, _safe_inject_intl.default)(CreateProjectForm, {
  withRef: true
});

exports.default = _default;