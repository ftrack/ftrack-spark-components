"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _reactIntl = require("react-intl");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var messages = (0, _reactIntl.defineMessages)({
  title: {
    "id": "ftrack-spark-components.create-project-dialog-success.title",
    "defaultMessage": "Congratulations"
  }
});

function CreateProjectSuccess(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: _style.default.success,
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
      className: _style.default.icon,
      value: "done"
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("h1", {
      className: _style.default.successHeader,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.title))
    }), props.children]
  });
}

var _default = (0, _safe_inject_intl.default)(CreateProjectSuccess);

exports.default = _default;