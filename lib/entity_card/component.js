"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.find.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _material = require("@mui/material");

var _AccountCircle = _interopRequireDefault(require("@mui/icons-material/AccountCircle"));

var _checkbox = _interopRequireDefault(require("react-toolbox/lib/checkbox"));

var _button = require("react-toolbox/lib/button");

var _recompose = require("recompose");

var _classnames = _interopRequireDefault(require("classnames"));

var _isFunction = _interopRequireDefault(require("lodash/isFunction"));

var _attribute2 = require("../dataview/attribute");

var _hoc = require("../util/hoc");

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var ThumbnailPlayIcon = (0, _recompose.compose)((0, _recompose.withHandlers)({
  onClick: function onClick(props) {
    return function () {
      return props.onClick("thumbnail", props.thumbnailId);
    };
  }
}), (0, _recompose.withProps)({
  icon: "play_circle_filled",
  flat: true
}))(_button.IconButton);

function MediumHeadingLink_(_ref) {
  var className = _ref.className,
      onClick = _ref.onClick,
      children = _ref.children;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("h5", {
    onClick: onClick,
    className: className,
    children: children
  });
}

MediumHeadingLink_.propTypes = {
  className: _propTypes.default.string,
  onClick: _propTypes.default.func.isRequired,
  entityId: _propTypes.default.arrayOf(_propTypes.default.string),
  entityType: _propTypes.default.string,
  children: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.string])
};
var MediumHeadingLink = (0, _recompose.withHandlers)({
  onClick: function onClick(props) {
    return function () {
      return props.onClick("link", {
        id: props.entityId,
        type: props.entityType
      });
    };
  }
})(MediumHeadingLink_);

var CustomCardMedia = function CustomCardMedia(_ref2) {
  var entityType = _ref2.entityType,
      image = _ref2.image,
      thumbnailId = _ref2.thumbnailId;

  if (entityType !== "User" && image) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.CardMedia, {
      image: image,
      alt: "",
      sx: {
        height: "14.4rem"
      }
    });
  } else if (entityType === "User" && image && thumbnailId) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.CardMedia, {
      sx: {
        m: "1.6rem auto 0",
        position: "relative",
        height: "14.4rem",
        backgroundColor: "background.paper"
      },
      image: image,
      variant: "round",
      alt: ""
    });
  } else {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.Avatar, {
      sx: {
        m: "1.6rem auto 0",
        position: "relative",
        height: "12rem",
        width: "12rem"
      },
      src: /*#__PURE__*/(0, _jsxRuntime.jsx)(_AccountCircle.default, {})
    });
  }
};
/** Card component with thumbnail, name and attributes. */


var CustomizableCard = /*#__PURE__*/function (_PureComponent) {
  _inherits(CustomizableCard, _PureComponent);

  var _super = _createSuper(CustomizableCard);

  function CustomizableCard() {
    var _this;

    _classCallCheck(this, CustomizableCard);

    _this = _super.call(this);
    _this.onSelectionChanged = _this.onSelectionChanged.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(CustomizableCard, [{
    key: "onSelectionChanged",
    value: function onSelectionChanged(state) {
      var _this$props = this.props,
          onSelectionChanged = _this$props.onSelectionChanged,
          index = _this$props.index;
      onSelectionChanged(state, index);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props2 = this.props,
          attributes = _this$props2.attributes,
          data = _this$props2.data,
          thumbnail = _this$props2.thumbnail,
          footer = _this$props2.footer,
          selected = _this$props2.selected,
          selectable = _this$props2.selectable,
          session = _this$props2.session,
          onItemClicked = _this$props2.onItemClicked,
          disableThumbnailClick = _this$props2.disableThumbnailClick,
          oneAttributePerLine = _this$props2.oneAttributePerLine;
      var ignoreAttributes = [];
      var thumbnailUrl;

      if ((0, _isFunction.default)(thumbnail)) {
        thumbnailUrl = thumbnail(data);
      } else {
        var thumbnailAttribute = attributes.find(function (candidate) {
          return candidate.key === thumbnail;
        });
        thumbnailUrl = session.thumbnailUrl(data[thumbnailAttribute.key]);
        ignoreAttributes.push(thumbnailAttribute.key);
      }

      var footerElement = null;

      if (footer) {
        footerElement = /*#__PURE__*/(0, _react.createElement)(footer, {
          data: data
        });
      }

      var titleElement;

      if ((0, _isFunction.default)(this.props.titleAttribute)) {
        titleElement = this.props.titleAttribute(data);
      } else {
        var attribute = attributes.find(function (candidate) {
          return candidate.key === _this2.props.titleAttribute;
        });
        titleElement = data[attribute.key];

        if (attribute.formatter) {
          titleElement = /*#__PURE__*/(0, _react.createElement)(attribute.formatter, {
            value: titleElement
          });
        }

        ignoreAttributes.push(this.props.titleAttribute);
      }

      var subtitleElements = [];
      this.props.subtitleAttributes.forEach(function (subtitleAttribute) {
        if ((0, _isFunction.default)(subtitleAttribute)) {
          subtitleElements.push(subtitleAttribute(data));
        } else {
          var _attribute = attributes.find(function (candidate) {
            return candidate.key === subtitleAttribute;
          });

          var value = data[_attribute.key];

          if (_attribute.formatter) {
            value = /*#__PURE__*/(0, _react.createElement)(_attribute.formatter, {
              value: value
            });
          }

          subtitleElements.push(value);
          ignoreAttributes.push(subtitleAttribute);
        }
      });

      if (this.props.colorAttribute) {
        ignoreAttributes.push(this.props.colorAttribute);
      }

      var cardTextElement = null;

      if (attributes.length) {
        cardTextElement = /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default["card-text"],
          children: attributes.map(function (attribute) {
            var valueElement = data[attribute.key];

            if (ignoreAttributes.indexOf(attribute.key) !== -1) {
              return null;
            }

            if (attribute.formatter) {
              valueElement = /*#__PURE__*/(0, _react.createElement)(attribute.formatter, {
                value: data[attribute.key],
                onClick: _this2.props.onItemClicked
              });
            }

            return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
              className: (0, _classnames.default)(_style.default["attribute-item"], _defineProperty({}, _style.default["full-width"], oneAttributePerLine)),
              children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("h6", {
                className: _style.default["attribute-title"],
                children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_attribute2.AttributeLabel, {
                  attribute: attribute,
                  enableGroup: true,
                  className: _style.default["attribute-upper-case"]
                })
              }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
                className: _style.default["attribute-value"],
                children: valueElement
              })]
            }, attribute.key);
          })
        });
      }

      var cardMenuClasses = (0, _classnames.default)(_style.default["card-menu"], selected && _style.default["card-selected"]);
      var cardClasses = (0, _classnames.default)(this.props.className, _style.default.card, selected && _style.default["card-selected"]);
      var cardOverlayClasses = (0, _classnames.default)(_style.default["card-thumb-overlay"], selected && _style.default["card-thumb-overlay-highlighted"]);
      var cardMenu = this.props.cardMenu ? /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: cardMenuClasses,
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(this.props.cardMenu, {
          entityId: data.__entity_id__,
          entityType: data.__entity_type__,
          name: data.name ? data.name : this.props.model
        })
      }) : /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {});
      return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_material.Card, {
        elevation: 0,
        className: cardClasses,
        sx: {
          width: "26rem",
          minHeight: "26rem"
        },
        children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(_material.Box, {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(_material.Box, {
            className: cardOverlayClasses,
            children: [cardMenu, selectable && /*#__PURE__*/(0, _jsxRuntime.jsx)(_checkbox.default, {
              className: _style.default["card-checkbox"],
              checked: selected,
              onChange: this.onSelectionChanged
            }), !disableThumbnailClick && data.thumbnail_id ? /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
              className: _style.default["button-container"],
              children: /*#__PURE__*/(0, _jsxRuntime.jsx)(ThumbnailPlayIcon, {
                thumbnailId: data.thumbnail_id,
                onClick: onItemClicked
              })
            }) : null]
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(CustomCardMedia, {
            entityType: data.__entity_type__,
            image: thumbnailUrl,
            thumbnailId: data.thumbnail_id
          })]
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_material.CardContent, {
          sx: {
            padding: 0,
            ":last-child": {
              paddingBottom: 0
            }
          },
          children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("hgroup", {
            className: _style.default["card-title-group"],
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(MediumHeadingLink, {
              className: _style.default["card-title"],
              entityId: data.__entity_id__,
              entityType: data.__entity_type__,
              onClick: onItemClicked,
              children: titleElement
            }), subtitleElements.map(function (subtitleElement, index) {
              return /*#__PURE__*/(0, _jsxRuntime.jsx)("h6", {
                className: _style.default["card-title-subtitle"],
                children: subtitleElement
              }, "subtitle-".concat(index));
            }), this.props.colorAttribute ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_button.Button, {
              icon: "",
              floating: true,
              primary: true,
              mini: true,
              className: _style.default["card-color-fab"],
              style: {
                backgroundColor: data[this.props.colorAttribute]
              }
            }) : null]
          }), cardTextElement, /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            children: footerElement
          })]
        })]
      });
    }
  }]);

  return CustomizableCard;
}(_react.PureComponent);

CustomizableCard.propTypes = {
  data: _propTypes.default.object.isRequired,
  cardTheme: _propTypes.default.object,
  attributes: _propTypes.default.array.isRequired,
  className: _propTypes.default.string,
  thumbnail: _propTypes.default.oneOfType([_propTypes.default.func, _propTypes.default.string]),
  titleAttribute: _propTypes.default.oneOfType([_propTypes.default.func, _propTypes.default.string]),
  subtitleAttributes: _propTypes.default.array,
  colorAttribute: _propTypes.default.string,
  footer: _propTypes.default.func,
  cardMenu: _propTypes.default.func,
  onItemClicked: _propTypes.default.func,
  onSelectionChanged: _propTypes.default.func,
  selected: _propTypes.default.bool,
  selectable: _propTypes.default.bool,
  disableThumbnailClick: _propTypes.default.bool,
  index: _propTypes.default.number,
  oneAttributePerLine: _propTypes.default.bool,
  session: _propTypes.default.shape({
    thumbnailUrl: _propTypes.default.func.isRequired
  })
};
CustomizableCard.defaultProps = {
  className: "",
  disableThumbnailClick: false,
  selectable: false,
  oneAttributePerLine: false
};

var _default = (0, _hoc.withSession)(CustomizableCard);

exports.default = _default;