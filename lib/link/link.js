"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _styles = _interopRequireDefault(require("./styles.scss"));

var _skeleton = _interopRequireDefault(require("../skeleton"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function Link(_ref) {
  var className = _ref.className,
      to = _ref.to,
      title = _ref.title,
      onClick = _ref.onClick,
      disabled = _ref.disabled,
      isLoading = _ref.isLoading;

  if (isLoading) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      className: (0, _classnames.default)(_styles.default.root, className),
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {
        style: {
          width: "100px"
        }
      })
    });
  }

  if (onClick) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      className: (0, _classnames.default)(_styles.default.root, className, _defineProperty({}, "".concat(_styles.default.disabled), disabled)),
      onClick: onClick,
      children: title
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("a", {
    className: (0, _classnames.default)(_styles.default.root, className, _defineProperty({}, "".concat(_styles.default.disabled), disabled)),
    href: to,
    target: "_blank",
    rel: "noopener noreferrer",
    children: title
  });
}

Link.propTypes = {
  to: _propTypes.default.string,
  title: _propTypes.default.string,
  className: _propTypes.default.string,
  onClick: _propTypes.default.func,
  disabled: _propTypes.default.bool,
  isLoading: _propTypes.default.bool
};
var _default = Link;
exports.default = _default;