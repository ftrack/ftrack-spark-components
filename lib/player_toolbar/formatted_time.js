"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.weak-map.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactIntl = require("react-intl");

var _classnames = _interopRequireDefault(require("classnames"));

var _button_menu_old = _interopRequireWildcard(require("../button_menu_old"));

var _get_formatted_time = _interopRequireWildcard(require("../util/get_formatted_time"));

var _messages = _interopRequireDefault(require("./messages"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function FormattedTime(_ref) {
  var className = _ref.className,
      currentFrame = _ref.currentFrame,
      format = _ref.format,
      frameRate = _ref.frameRate,
      onChangeFormat = _ref.onChangeFormat,
      tooltipPosition = _ref.tooltipPosition,
      totalFrames = _ref.totalFrames;
  var timeFormats = [{
    title: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["time-format-title"])),
    options: [{
      value: _get_formatted_time.FORMAT_STANDARD,
      caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["time-format-standard"])),
      selectedIcon: "check",
      selected: format === "standard"
    }, {
      value: _get_formatted_time.FORMAT_FRAMES,
      caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["time-format-frames"])),
      selectedIcon: "check",
      selected: format === "frames"
    }, {
      value: _get_formatted_time.FORMAT_TIMECODE,
      caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["time-format-timecode"])),
      selectedIcon: "check",
      selected: format === "timecode"
    }]
  }];
  var formattedTime = (0, _get_formatted_time.default)(format, currentFrame, totalFrames, frameRate);
  var buttonLabel = /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
    children: [formattedTime[0], format !== _get_formatted_time.FORMAT_TIMECODE && /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
      className: _style.default.formattedTimeTotal,
      children: [" / ", formattedTime[1]]
    })]
  });
  var classes = (0, _classnames.default)(_style.default.formattedTime, className);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_button_menu_old.default, {
    button: /*#__PURE__*/(0, _jsxRuntime.jsx)(_button_menu_old.DropdownButton, {
      tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["time-format-tooltip"])),
      tooltipPosition: tooltipPosition,
      children: buttonLabel
    }),
    className: classes,
    position: "bottomLeft",
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_button_menu_old.SectionMenu, {
      sections: timeFormats,
      onSelectionChange: function onSelectionChange(value) {
        onChangeFormat(value);
      }
    }, format)
  });
}

FormattedTime.propTypes = {
  className: _propTypes.default.string,
  currentFrame: _propTypes.default.number,
  format: _propTypes.default.string,
  frameRate: _propTypes.default.number,
  onChangeFormat: _propTypes.default.func,
  tooltipPosition: _propTypes.default.string,
  totalFrames: _propTypes.default.number
};
var _default = FormattedTime;
exports.default = _default;