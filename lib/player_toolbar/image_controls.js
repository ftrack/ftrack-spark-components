"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactIntl = require("react-intl");

var _tooltip = _interopRequireDefault(require("react-toolbox/lib/tooltip"));

var _classnames = _interopRequireDefault(require("classnames"));

var _page_indicator = _interopRequireDefault(require("../page_indicator"));

var _download_button = _interopRequireDefault(require("../player_toolbar/download_button"));

var _icon_button = _interopRequireDefault(require("../icon_button"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _style = _interopRequireDefault(require("./style.scss"));

var _active_button_theme = _interopRequireDefault(require("./active_button_theme.scss"));

var _clear_annotation_button_theme = _interopRequireDefault(require("./clear_annotation_button_theme.scss"));

var _messages = _interopRequireDefault(require("./messages"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["className", "currentFrame", "totalFrames", "onChangeFrame", "activeTool", "onToolClicked", "downloadableComponents", "onClearAnnotation", "tooltipPosition", "onZoomToolChanged", "zoomValue", "bigZoomIsEnabled", "bigZoom", "isComparing", "canCompare"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var TooltipIconButton = (0, _tooltip.default)(_icon_button.default);
var TooltipSpan = (0, _tooltip.default)("span");

function ImageControls(_ref) {
  var className = _ref.className,
      currentFrame = _ref.currentFrame,
      totalFrames = _ref.totalFrames,
      onChangeFrame = _ref.onChangeFrame,
      activeTool = _ref.activeTool,
      onToolClicked = _ref.onToolClicked,
      downloadableComponents = _ref.downloadableComponents,
      onClearAnnotation = _ref.onClearAnnotation,
      tooltipPosition = _ref.tooltipPosition,
      onZoomToolChanged = _ref.onZoomToolChanged,
      zoomValue = _ref.zoomValue,
      bigZoomIsEnabled = _ref.bigZoomIsEnabled,
      bigZoom = _ref.bigZoom,
      isComparing = _ref.isComparing,
      canCompare = _ref.canCompare,
      props = _objectWithoutProperties(_ref, _excluded);

  var classes = (0, _classnames.default)(_style.default.controls, className);
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", _objectSpread(_objectSpread({
    className: classes
  }, props), {}, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      children: totalFrames > 1 ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_page_indicator.default, {
        current: currentFrame,
        total: totalFrames,
        onChangePage: onChangeFrame
      }) : null
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipSpan, {
        tooltipPosition: tooltipPosition,
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["compare-mode"])),
        className: _style.default.compare,
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_icon_button.default, {
          icon: "compare",
          value: "COMPARE",
          disabled: !canCompare,
          onClick: function onClick() {
            return onToolClicked("COMPARE");
          },
          theme: isComparing ? _active_button_theme.default : undefined
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        icon: "wb_sunny",
        value: "LASER",
        onClick: function onClick() {
          return onToolClicked("LASER");
        },
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["laser-tool"])),
        theme: activeTool === "LASER" ? _active_button_theme.default : undefined,
        tooltipPosition: tooltipPosition
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        icon: "brush",
        onClick: onClearAnnotation,
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["annotation-button-tooltip"])),
        theme: _clear_annotation_button_theme.default,
        tooltipPosition: tooltipPosition
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        theme: activeTool === "KEYBOARD" ? _active_button_theme.default : undefined,
        value: "KEYBOARD",
        onClick: function onClick() {
          return onToolClicked("KEYBOARD");
        },
        icon: "keyboard",
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["keyboard-shortcuts-tooltip"])),
        tooltipPosition: tooltipPosition
      }), bigZoomIsEnabled ? /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        theme: bigZoom && zoomValue !== "ZOOM" ? _active_button_theme.default : undefined,
        value: "ZOOM_100",
        onClick: function onClick() {
          if (zoomValue === "ZOOM_100") {
            onZoomToolChanged("ZOOM_RESET");
          } else {
            onZoomToolChanged("ZOOM_100");
          }
        },
        icon: bigZoom ? "youtube_searched_for" : "crop_original",
        tooltip: zoomValue === "ZOOM_100" ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["reset-zoom"])) : /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["zoom-100"])),
        tooltipPosition: tooltipPosition
      }) : null, /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        theme: activeTool === "ZOOM" ? _active_button_theme.default : undefined,
        value: "ZOOM",
        onClick: function onClick() {
          onZoomToolChanged("ZOOM");
          onToolClicked("ZOOM");
        },
        icon: "zoom_in",
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["zoom-tool-tooltip"])),
        tooltipPosition: tooltipPosition
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        value: "PAN",
        theme: activeTool === "PAN" ? _active_button_theme.default : undefined,
        onClick: function onClick() {
          return onToolClicked("PAN");
        },
        icon: "pan_tool",
        className: _style.default.smallIcon,
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["pan-tool-tooltip"])),
        tooltipPosition: tooltipPosition
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        value: "fullscreen",
        onClick: function onClick() {
          return onToolClicked("fullscreen");
        },
        icon: "fullscreen",
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["fullscreen-mode-tooltip"])),
        tooltipPosition: tooltipPosition
      }), downloadableComponents && downloadableComponents.length ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_download_button.default, {
        downloadableComponents: downloadableComponents,
        tooltipPosition: tooltipPosition
      }) : null]
    })]
  }));
}

ImageControls.propTypes = {
  className: _propTypes.default.string,
  onToolClicked: _propTypes.default.func,
  activeTool: _propTypes.default.string,
  currentFrame: _propTypes.default.number,
  totalFrames: _propTypes.default.number,
  onChangeFrame: _propTypes.default.func,
  downloadableComponents: _propTypes.default.arrayOf(_propTypes.default.shape({
    name: _propTypes.default.node.isRequired,
    url: _propTypes.default.string.isRequired
  })),
  onClearAnnotation: _propTypes.default.func,
  tooltipPosition: _propTypes.default.string,
  onZoomToolChanged: _propTypes.default.func,
  zoomValue: _propTypes.default.string,
  bigZoomIsEnabled: _propTypes.default.bool,
  bigZoom: _propTypes.default.bool,
  isComparing: _propTypes.default.bool,
  canCompare: _propTypes.default.bool
};

var _default = (0, _safe_inject_intl.default)(ImageControls);

exports.default = _default;