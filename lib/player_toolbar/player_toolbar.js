"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.math.trunc.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _progress_bar = _interopRequireDefault(require("react-toolbox/lib/progress_bar"));

var _style = _interopRequireDefault(require("./style.scss"));

var _progress_bar_theme = _interopRequireDefault(require("./progress_bar_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["buffer", "children", "className", "currentFrame", "firstFrame", "onChangeFrame", "totalFrames", "loopIn", "loopOut", "sequenceMemberFrameOuts"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var PlayerToolbarBase = /*#__PURE__*/function (_Component) {
  _inherits(PlayerToolbarBase, _Component);

  var _super = _createSuper(PlayerToolbarBase);

  function PlayerToolbarBase(props) {
    var _this;

    _classCallCheck(this, PlayerToolbarBase);

    _this = _super.call(this, props);
    _this.state = {};
    _this.handleMouseDown = _this.handleMouseDown.bind(_assertThisInitialized(_this));
    _this.handleMouseMove = _this.handleMouseMove.bind(_assertThisInitialized(_this));
    _this.handleMouseUp = _this.handleMouseUp.bind(_assertThisInitialized(_this));
    _this.handleTouchStart = _this.handleTouchStart.bind(_assertThisInitialized(_this));
    _this.handleTouchMove = _this.handleTouchMove.bind(_assertThisInitialized(_this));
    _this.handleTouchEnd = _this.handleTouchEnd.bind(_assertThisInitialized(_this));
    _this.handleSeekMove = _this.handleSeekMove.bind(_assertThisInitialized(_this));
    _this.removeSeekerListeners = _this.removeSeekerListeners.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(PlayerToolbarBase, [{
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.removeSeekerListeners();
    }
  }, {
    key: "handleTouchStart",
    value: function handleTouchStart(event) {
      event.preventDefault();
      event.stopPropagation();

      if (this.props.onIsSeekingChanged) {
        this.props.onIsSeekingChanged(true);
      }

      document.body.addEventListener("touchmove", this.handleTouchMove);
      document.body.addEventListener("touchend", this.handleTouchEnd);
      var firstTouch = event.changedTouches[0];
      this.handleSeekMove(firstTouch);
    }
  }, {
    key: "handleTouchMove",
    value: function handleTouchMove(event) {
      event.preventDefault();
      event.stopPropagation();
      var firstTouch = event.changedTouches[0];
      this.handleSeekMove(firstTouch);
    }
  }, {
    key: "handleTouchEnd",
    value: function handleTouchEnd(event) {
      event.preventDefault();
      event.stopPropagation();
      this.removeSeekerListeners();
    }
  }, {
    key: "handleMouseDown",
    value: function handleMouseDown(event) {
      if (this.props.onIsSeekingChanged) {
        this.props.onIsSeekingChanged(true);
      }

      document.body.addEventListener("mousemove", this.handleMouseMove);
      document.body.addEventListener("mouseup", this.handleMouseUp);
      this.handleSeekMove(event);
    }
  }, {
    key: "handleMouseMove",
    value: function handleMouseMove(event) {
      event.stopPropagation();
      event.preventDefault();
      this.handleSeekMove(event);
    }
  }, {
    key: "handleSeekMove",
    value: function handleSeekMove(event) {
      var clientX = event.clientX;
      var bounds = this.seekerNode.getBoundingClientRect(); // Get normalized frame [firstFrame, totalFrames]

      var progress = (clientX - bounds.left) / bounds.width;
      var frame = progress * this.props.totalFrames + this.props.firstFrame;
      frame = Math.trunc(Math.min(Math.max(frame, this.props.firstFrame), this.props.totalFrames));
      this.props.onChangeFrame(frame);
    }
  }, {
    key: "handleMouseUp",
    value: function handleMouseUp(event) {
      event.stopPropagation();
      event.preventDefault();
      this.removeSeekerListeners();
    }
  }, {
    key: "removeSeekerListeners",
    value: function removeSeekerListeners() {
      if (this.props.onIsSeekingChanged) {
        this.props.onIsSeekingChanged(false);
      }

      document.body.removeEventListener("mousemove", this.handleMouseMove);
      document.body.removeEventListener("mouseup", this.handleMouseUp);
      document.body.removeEventListener("touchmove", this.handleTouchMove);
      document.body.removeEventListener("touchend", this.handleTouchEnd);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          buffer = _this$props.buffer,
          children = _this$props.children,
          className = _this$props.className,
          currentFrame = _this$props.currentFrame,
          firstFrame = _this$props.firstFrame,
          onChangeFrame = _this$props.onChangeFrame,
          totalFrames = _this$props.totalFrames,
          loopIn = _this$props.loopIn,
          loopOut = _this$props.loopOut,
          sequenceMemberFrameOuts = _this$props.sequenceMemberFrameOuts,
          props = _objectWithoutProperties(_this$props, _excluded);

      var showProgressBar = this.props.totalFrames > 1;
      var classes = (0, _classnames.default)(_style.default.root, className);
      return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", _objectSpread(_objectSpread({
        className: classes
      }, props), {}, {
        children: [showProgressBar ? /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
          className: _style.default.progressBarWrapper,
          ref: function ref(node) {
            _this2.seekerNode = node;
          },
          onMouseDown: onChangeFrame && this.handleMouseDown,
          onTouchStart: onChangeFrame && this.handleTouchStart,
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_progress_bar.default, {
            className: _style.default.progressBar,
            theme: _progress_bar_theme.default,
            type: "linear",
            mode: "determinate",
            value: currentFrame,
            buffer: buffer,
            min: 0,
            max: totalFrames - 1
          }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
            className: _style.default["pointers-wrapper"],
            children: [loopIn >= 0 ? /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
              style: {
                left: "".concat(loopIn / totalFrames * 100, "%")
              },
              className: _style.default["point-in-out-mark"]
            }) : null, loopOut >= 0 ? /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
              style: {
                left: "".concat(loopOut / totalFrames * 100, "%")
              },
              className: _style.default["point-in-out-mark"]
            }) : null, sequenceMemberFrameOuts && sequenceMemberFrameOuts.length >= 1 ? sequenceMemberFrameOuts.map(function (item) {
              return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
                style: {
                  left: "".concat(item / totalFrames * 100, "%")
                },
                className: _style.default["frame-out-marker"]
              });
            }) : null]
          })]
        }) : null, children]
      }));
    }
  }]);

  return PlayerToolbarBase;
}(_react.Component);

PlayerToolbarBase.propTypes = {
  buffer: _propTypes.default.number,
  children: _propTypes.default.node,
  className: _propTypes.default.string,
  currentFrame: _propTypes.default.number,
  firstFrame: _propTypes.default.number,
  onChangeFrame: _propTypes.default.func,
  totalFrames: _propTypes.default.number,
  loopIn: _propTypes.default.number,
  loopOut: _propTypes.default.number,
  sequenceMemberFrameOuts: _propTypes.default.arrayOf(_propTypes.default.number),
  onIsSeekingChanged: _propTypes.default.func
};
PlayerToolbarBase.defaultProps = {
  buffer: 0,
  currentFrame: 0,
  firstFrame: 0,
  totalFrames: 1
};
var PlayerToolbar = PlayerToolbarBase;
var _default = PlayerToolbar;
exports.default = _default;