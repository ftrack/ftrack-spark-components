"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Slider = _interopRequireDefault(require("@mui/material/Slider"));

var _tooltip = _interopRequireDefault(require("react-toolbox/lib/tooltip"));

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactIntl = require("react-intl");

var _download_button = _interopRequireDefault(require("../player_toolbar/download_button"));

var _playback_menu = _interopRequireDefault(require("../player_toolbar/playback_menu.js"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _icon_button = _interopRequireDefault(require("../icon_button"));

var _style = _interopRequireDefault(require("./style.scss"));

var _active_button_theme = _interopRequireDefault(require("./active_button_theme.scss"));

var _clear_annotation_button_theme = _interopRequireDefault(require("./clear_annotation_button_theme.scss"));

var _messages = _interopRequireDefault(require("./messages"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["className", "isPlaying", "onTogglePlay", "onToolClicked", "onVolumeChanged", "volume", "activeTool", "downloadableComponents", "loop", "playbackMode", "onPlaybackModeChanged", "playbackRate", "onPlaybackRateChanged", "onLoopToggle", "playAllSequenceMediaIds", "children", "onClearAnnotation", "tooltipPosition", "onZoomToolChanged", "zoomValue", "isComparing", "canCompare"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var TooltipIconButton = (0, _tooltip.default)(_icon_button.default);
var TooltipSpan = (0, _tooltip.default)("span");

function PlayButton(_ref) {
  var isPlaying = _ref.isPlaying,
      onTogglePlay = _ref.onTogglePlay,
      tooltipPosition = _ref.tooltipPosition;
  var playIcon = isPlaying ? "pause" : "play_arrow";
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
    className: _style.default.largeIcon,
    icon: playIcon,
    onClick: function onClick() {
      onTogglePlay(!isPlaying);
    },
    tooltip: isPlaying ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["pause-tooltip"])) : /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["play-tooltip"])),
    tooltipPosition: tooltipPosition
  });
}

PlayButton.propTypes = {
  onTogglePlay: _propTypes.default.func,
  isPlaying: _propTypes.default.bool,
  tooltipPosition: _propTypes.default.string
};

function VideoControls(_ref2) {
  var className = _ref2.className,
      isPlaying = _ref2.isPlaying,
      onTogglePlay = _ref2.onTogglePlay,
      onToolClicked = _ref2.onToolClicked,
      onVolumeChanged = _ref2.onVolumeChanged,
      volume = _ref2.volume,
      activeTool = _ref2.activeTool,
      downloadableComponents = _ref2.downloadableComponents,
      loop = _ref2.loop,
      playbackMode = _ref2.playbackMode,
      onPlaybackModeChanged = _ref2.onPlaybackModeChanged,
      playbackRate = _ref2.playbackRate,
      onPlaybackRateChanged = _ref2.onPlaybackRateChanged,
      onLoopToggle = _ref2.onLoopToggle,
      playAllSequenceMediaIds = _ref2.playAllSequenceMediaIds,
      children = _ref2.children,
      onClearAnnotation = _ref2.onClearAnnotation,
      tooltipPosition = _ref2.tooltipPosition,
      onZoomToolChanged = _ref2.onZoomToolChanged,
      zoomValue = _ref2.zoomValue,
      isComparing = _ref2.isComparing,
      canCompare = _ref2.canCompare,
      props = _objectWithoutProperties(_ref2, _excluded);

  var volumeIcon = "volume_off";

  if (volume) {
    volumeIcon = /*#__PURE__*/(0, _jsxRuntime.jsxs)(_react.Fragment, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
        className: _style.default.volumeDown,
        value: "volume_down"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
        className: _style.default.volumeUp,
        style: {
          opacity: volume
        },
        value: "volume_up"
      })]
    });
  }

  var classes = (0, _classnames.default)(_style.default.controls, className);
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", _objectSpread(_objectSpread({
    className: classes
  }, props), {}, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(PlayButton, {
        isPlaying: isPlaying,
        onTogglePlay: onTogglePlay,
        tooltipPosition: tooltipPosition
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        value: "volume",
        onClick: function onClick() {
          return onToolClicked("volume");
        },
        icon: volumeIcon,
        tooltip: volume === 0 ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["unmute-volume-tooltip"])) : /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["mute-volume-tooltip"])),
        tooltipPosition: tooltipPosition
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Slider.default, {
        min: 0,
        max: 1,
        step: 0.01,
        onChange: function onChange(event, selectedValue) {
          onVolumeChanged(selectedValue);
        },
        value: volume,
        color: "neutral",
        sx: {
          flex: 1,
          my: 0,
          mr: 2,
          ml: 1,
          zIndex: 0,
          display: "inline-block",
          verticalAlign: "middle",
          width: 90
        }
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_playback_menu.default // Force menu to close on value changes.
      , {
        className: _style.default.playbackButton,
        loop: loop,
        onLoopToggle: onLoopToggle,
        onPlaybackModeChanged: onPlaybackModeChanged,
        playbackMode: playbackMode,
        onPlaybackRateChanged: onPlaybackRateChanged,
        playbackRate: playbackRate,
        playAllSequenceMediaIds: playAllSequenceMediaIds,
        tooltipPosition: tooltipPosition
      }, "menu-".concat(loop, "-").concat(playbackMode, "-").concat(playbackRate)), children]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default.tools,
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipSpan, {
        tooltipPosition: tooltipPosition,
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["compare-mode"])),
        className: _style.default.compare,
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_icon_button.default, {
          icon: "compare",
          value: "COMPARE",
          disabled: !canCompare,
          onClick: function onClick() {
            return onToolClicked("COMPARE");
          },
          theme: isComparing ? _active_button_theme.default : undefined
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        icon: "wb_sunny",
        value: "LASER",
        onClick: function onClick() {
          return onToolClicked("LASER");
        },
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["laser-tool"])),
        theme: activeTool === "LASER" ? _active_button_theme.default : undefined,
        tooltipPosition: tooltipPosition
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        icon: "brush",
        onClick: onClearAnnotation,
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["annotation-button-tooltip"])),
        theme: _clear_annotation_button_theme.default,
        tooltipPosition: tooltipPosition
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        theme: activeTool === "KEYBOARD" ? _active_button_theme.default : undefined,
        value: "KEYBOARD",
        onClick: function onClick() {
          return onToolClicked("KEYBOARD");
        },
        icon: "keyboard",
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["keyboard-shortcuts-tooltip"])),
        tooltipPosition: tooltipPosition
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        theme: activeTool === "ZOOM" ? _active_button_theme.default : undefined,
        value: "ZOOM",
        onClick: function onClick() {
          onZoomToolChanged("ZOOM");
          onToolClicked("ZOOM");
        },
        icon: "zoom_in",
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["zoom-tool-tooltip"])),
        tooltipPosition: tooltipPosition
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        value: "PAN",
        theme: activeTool === "PAN" ? _active_button_theme.default : undefined,
        onClick: function onClick() {
          return onToolClicked("PAN");
        },
        icon: "pan_tool",
        className: _style.default.smallIcon,
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["pan-tool-tooltip"])),
        tooltipPosition: tooltipPosition
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        value: "fullscreen",
        onClick: function onClick() {
          return onToolClicked("fullscreen");
        },
        icon: "fullscreen",
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["fullscreen-mode-tooltip"])),
        tooltipPosition: tooltipPosition
      }), downloadableComponents && downloadableComponents.length ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_download_button.default, {
        downloadableComponents: downloadableComponents,
        tooltipPosition: tooltipPosition
      }) : null]
    })]
  }));
}

VideoControls.propTypes = {
  className: _propTypes.default.string,
  isPlaying: _propTypes.default.bool,
  onTogglePlay: _propTypes.default.func,
  onToolClicked: _propTypes.default.func,
  onVolumeChanged: _propTypes.default.func,
  volume: _propTypes.default.number,
  activeTool: _propTypes.default.string,
  downloadableComponents: _propTypes.default.arrayOf(_propTypes.default.shape({
    name: _propTypes.default.node.isRequired,
    value: _propTypes.default.string.isRequired
  })),
  loop: _propTypes.default.bool,
  onLoopToggle: _propTypes.default.func,
  onPlaybackModeChanged: _propTypes.default.func,
  onPlaybackRateChanged: _propTypes.default.func,
  onClick: _propTypes.default.func,
  playbackMode: _propTypes.default.string,
  playbackRate: _propTypes.default.number,
  mediaType: _propTypes.default.string,
  playAllSequenceMediaIds: _propTypes.default.arrayOf(_propTypes.default.string),
  children: _propTypes.default.node,
  onClearAnnotation: _propTypes.default.func,
  tooltipPosition: _propTypes.default.string,
  onZoomToolChanged: _propTypes.default.func,
  zoomValue: _propTypes.default.string,
  isComparing: _propTypes.default.bool,
  canCompare: _propTypes.default.bool
};
VideoControls.defaultProps = {
  isPlaying: false
};

var _default = (0, _safe_inject_intl.default)(VideoControls);

exports.default = _default;