"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

var _button = require("react-toolbox/lib/button");

var _menu = require("react-toolbox/lib/menu");

var _tooltip = _interopRequireDefault(require("react-toolbox/lib/tooltip"));

var _reactIntl = require("react-intl");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _button_menu_old = _interopRequireDefault(require("../button_menu_old"));

var _messages = _interopRequireDefault(require("./messages"));

var _style = _interopRequireDefault(require("./style.scss"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TooltipDownloadButton = (0, _tooltip.default)(_button.IconButton);

function DownloadButton(_ref) {
  var downloadableComponents = _ref.downloadableComponents,
      tooltipPosition = _ref.tooltipPosition,
      intl = _ref.intl,
      label = _ref.label;
  var button = label ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_button.Button, {
    icon: "get_app",
    label: label
  }) : /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipDownloadButton, {
    icon: "get_app",
    tooltip: intl.formatMessage(_messages.default["download-button-tooltip"]),
    tooltipPosition: tooltipPosition
  });
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_button_menu_old.default, {
    display: "inline-block",
    button: button,
    position: "bottomRight",
    onSelect: function onSelect(value) {
      // eslint-disable-next-line no-undef
      window.open(value);
    },
    children: downloadableComponents.map(function (comp) {
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_menu.MenuItem, {
        value: comp.value,
        icon: "insert_drive_file",
        caption: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            children: comp.name
          }), comp.description ? /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            className: _style.default.downloadButtonDescription,
            children: comp.description
          }) : null]
        })
      }, comp.value);
    })
  });
}

DownloadButton.propTypes = {
  downloadableComponents: _propTypes.default.arrayOf(_propTypes.default.shape({
    name: _propTypes.default.node.isRequired,
    description: _propTypes.default.string,
    value: _propTypes.default.string.isRequired
  })),
  tooltipPosition: _propTypes.default.string,
  intl: _reactIntl.intlShape,
  label: _propTypes.default.string
};
DownloadButton.defaultProps = {
  downloadableComponents: [],
  tooltipPosition: "bottom",
  label: ""
};

var _default = (0, _safe_inject_intl.default)(DownloadButton);

exports.default = _default;