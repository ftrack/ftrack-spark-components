"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reactIntl = require("react-intl");

// :copyright: Copyright (c) 2020 ftrack
var messages = (0, _reactIntl.defineMessages)({
  "annotation-button-tooltip": {
    "id": "ftrack-spark-components.player_toolbar.annotation-button-tooltip",
    "defaultMessage": "Clear annotations"
  },
  "pan-tool-tooltip": {
    "id": "ftrack-spark-components.player_toolbar.pan-tool-tooltip",
    "defaultMessage": "Pan tool"
  },
  "zoom-tool-tooltip": {
    "id": "ftrack-spark-components.player_toolbar.zoom-tool-tooltip",
    "defaultMessage": "Zoom tool"
  },
  "keyboard-shortcuts-tooltip": {
    "id": "ftrack-spark-components.player_toolbar.keyboard-shortcuts-tooltip",
    "defaultMessage": "Keyboard shortcuts"
  },
  "unmute-volume-tooltip": {
    "id": "ftrack-spark-components.player_toolbar.unmute-volume-tooltip",
    "defaultMessage": "Unmute"
  },
  "mute-volume-tooltip": {
    "id": "ftrack-spark-components.player_toolbar.mute-volume-tooltip",
    "defaultMessage": "Mute"
  },
  "fullscreen-mode-tooltip": {
    "id": "ftrack-spark-components.player_toolbar.fullscreen-mode-tooltip",
    "defaultMessage": "Full screen"
  },
  "download-button-tooltip": {
    "id": "ftrack-spark-components.player_toolbar.download-button-tooltip",
    "defaultMessage": "Download media"
  },
  "pause-tooltip": {
    "id": "ftrack-spark-components.player_toolbar.pause-tooltip",
    "defaultMessage": "Pause"
  },
  "play-tooltip": {
    "id": "ftrack-spark-components.player_toolbar.play-tooltip",
    "defaultMessage": "Play"
  },
  "time-format-tooltip": {
    "id": "ftrack-spark-components.player_toolbar.time-format-tooltip",
    "defaultMessage": "Time format"
  },
  "time-format-standard": {
    "id": "ftrack-spark-components.player_toolbar.time-format-standard",
    "defaultMessage": "Standard"
  },
  "time-format-frames": {
    "id": "ftrack-spark-components.player_toolbar.time-format-frames",
    "defaultMessage": "Frames"
  },
  "time-format-timecode": {
    "id": "ftrack-spark-components.player_toolbar.time-format-timecode",
    "defaultMessage": "Timecode"
  },
  "time-format-title": {
    "id": "ftrack-spark-components.player_toolbar.time-format-title",
    "defaultMessage": "Time format"
  },
  "zoom-100": {
    "id": "ftrack-spark-components.player_toolbar.zoom-100",
    "defaultMessage": "Zoom to 100%"
  },
  "reset-zoom": {
    "id": "ftrack-spark-components.player_toolbar.reset-zoom",
    "defaultMessage": "Reset zoom"
  },
  "laser-tool": {
    "id": "ftrack-spark-components.player_toolbar.laser-tool",
    "defaultMessage": "Laser tool"
  },
  "compare-mode": {
    "id": "ftrack-spark-components.player_toolbar.compare-mode",
    "defaultMessage": "Compare"
  }
});
var _default = messages;
exports.default = _default;