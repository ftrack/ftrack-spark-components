"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.weak-map.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.number.constructor.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactIntl = require("react-intl");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _button_menu_old = _interopRequireWildcard(require("../button_menu_old"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["playbackRate", "onPlaybackRateChanged", "loop", "onLoopToggle", "onPlaybackModeChanged", "playbackMode", "playAllSequenceMediaIds", "tooltipPosition"];

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var messages = (0, _reactIntl.defineMessages)({
  "rate-heading": {
    "id": "ftrack-spark-overview.playback-menu.rate-heading",
    "defaultMessage": "Speed"
  },
  rate: {
    "id": "ftrack-spark-overview.playback-menu.rate",
    "defaultMessage": "{rate, number}x"
  },
  "rate-1x": {
    "id": "ftrack-spark-overview.playback-menu.rate-1x",
    "defaultMessage": "Normal"
  },
  "first-heading": {
    "id": "ftrack-spark-overview.playback-menu.first-heading",
    "defaultMessage": "Loop"
  },
  "loop-on": {
    "id": "ftrack-spark-overview.playback-menu.loop-on",
    "defaultMessage": "Loop on"
  },
  "loop-off": {
    "id": "ftrack-spark-overview.playback-menu.loop-off",
    "defaultMessage": "Loop off"
  },
  "second-heading": {
    "id": "ftrack-spark-overview.playback-menu.second-heading",
    "defaultMessage": "Playback"
  },
  "play-one": {
    "id": "ftrack-spark-overview.playback-menu.play-one",
    "defaultMessage": "Play one"
  },
  "play-all": {
    "id": "ftrack-spark-overview.playback-menu.play-all",
    "defaultMessage": "Play all"
  },
  "play-selection": {
    "id": "ftrack-spark-components.playback-menu.play-selection",
    "defaultMessage": "Play selection"
  },
  "playback-mode-tooltip": {
    "id": "ftrack-spark-components.playback-menu.playback-mode-tooltip",
    "defaultMessage": "Playback options"
  }
});

function PlaybackMenu(props) {
  var playbackRate = props.playbackRate,
      onPlaybackRateChanged = props.onPlaybackRateChanged,
      loop = props.loop,
      onLoopToggle = props.onLoopToggle,
      onPlaybackModeChanged = props.onPlaybackModeChanged,
      playbackMode = props.playbackMode,
      playAllSequenceMediaIds = props.playAllSequenceMediaIds,
      tooltipPosition = props.tooltipPosition,
      rest = _objectWithoutProperties(props, _excluded);

  var list = [{
    title: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["rate-heading"])),
    options: [{
      caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages.rate), {}, {
        values: {
          rate: 0.25
        }
      })),
      selectedIcon: "check",
      value: 0.25,
      selected: playbackRate === 0.25
    }, {
      caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages.rate), {}, {
        values: {
          rate: 0.5
        }
      })),
      selectedIcon: "check",
      value: 0.5,
      selected: playbackRate === 0.5
    }, {
      caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages.rate), {}, {
        values: {
          rate: 0.75
        }
      })),
      selectedIcon: "check",
      value: 0.75,
      selected: playbackRate === 0.75
    }, {
      caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["rate-1x"])),
      selectedIcon: "check",
      value: 1,
      selected: playbackRate === 1
    }, {
      caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages.rate), {}, {
        values: {
          rate: 1.25
        }
      })),
      selectedIcon: "check",
      value: 1.25,
      selected: playbackRate === 1.25
    }, {
      caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages.rate), {}, {
        values: {
          rate: 1.5
        }
      })),
      selectedIcon: "check",
      value: 1.5,
      selected: playbackRate === 1.5
    }, {
      caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages.rate), {}, {
        values: {
          rate: 1.75
        }
      })),
      selectedIcon: "check",
      value: 1.75,
      selected: playbackRate === 1.75
    }, {
      caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages.rate), {}, {
        values: {
          rate: 2
        }
      })),
      selectedIcon: "check",
      value: 2,
      selected: playbackRate === 2
    }]
  }, {
    title: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["first-heading"])),
    options: [{
      caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["loop-on"])),
      selectedIcon: "check",
      value: "loop-on",
      selected: loop
    }, {
      caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["loop-off"])),
      selectedIcon: "check",
      value: "loop-off",
      selected: !loop
    }]
  }];

  if (playAllSequenceMediaIds && playAllSequenceMediaIds.length > 1) {
    list.push({
      title: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["second-heading"])),
      options: [{
        caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["play-one"])),
        selectedIcon: "check",
        value: "PlayOne",
        selected: playbackMode === "PlayOne"
      }, {
        caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["play-all"])),
        selectedIcon: "check",
        value: "PlayAll",
        selected: playbackMode === "PlayAll"
      }, {
        caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["play-selection"])),
        selectedIcon: "check",
        value: "PlaySelection",
        selected: playbackMode === "PlaySelection"
      }]
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_button_menu_old.default, _objectSpread(_objectSpread({
    display: "inline-block",
    position: "bottomLeft",
    button: /*#__PURE__*/(0, _jsxRuntime.jsx)(_button_menu_old.DropdownButton, {
      icon: "settings",
      tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["playback-mode-tooltip"])),
      tooltipPosition: tooltipPosition
    })
  }, rest), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_button_menu_old.SectionMenu, {
      sections: list,
      onSelectionChange: function onSelectionChange(value, selected) {
        if (Number.isFinite(value)) {
          onPlaybackRateChanged(value);
        } else if (value === "PlayOne" || value === "PlayAll" || value === "PlaySelection") {
          onPlaybackModeChanged(value);
        } else if (value === "loop-on") {
          onLoopToggle(selected);
        } else if (value === "loop-off") {
          onLoopToggle(!selected);
        }
      }
    })
  }));
}

PlaybackMenu.propTypes = {
  playbackRate: _propTypes.default.number,
  onPlaybackRateChanged: _propTypes.default.func,
  loop: _propTypes.default.bool,
  onLoopToggle: _propTypes.default.func,
  onPlaybackModeChanged: _propTypes.default.func,
  playbackMode: _propTypes.default.string,
  playAllSequenceMediaIds: _propTypes.default.arrayOf(_propTypes.default.string),
  tooltipPosition: _propTypes.default.string
};

var _default = (0, _safe_inject_intl.default)(PlaybackMenu);

exports.default = _default;