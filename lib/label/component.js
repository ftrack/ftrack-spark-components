"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es7.array.includes.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _color = require("../util/color");

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Theme colors.
var THEME_COLORS = ["positive", "negative", "feedback"];

function Label(_ref) {
  var _classNames;

  var color = _ref.color,
      children = _ref.children,
      className = _ref.className;
  var backgroundColor;
  var foregroundColor;
  var colorClasses = (0, _classnames.default)(_style.default.colorContainer, className, (_classNames = {}, _defineProperty(_classNames, _style.default.positiveColor, color === "positive"), _defineProperty(_classNames, _style.default.negativeColor, color === "negative"), _defineProperty(_classNames, _style.default.feedbackColor, color === "feedback"), _classNames));

  if (color && !THEME_COLORS.includes(color)) {
    backgroundColor = color;
    foregroundColor = (0, _color.getForegroundColor)(color);
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
    style: {
      backgroundColor: backgroundColor,
      color: foregroundColor
    },
    className: colorClasses,
    children: children
  });
}

Label.propTypes = {
  color: _propTypes.default.oneOfType([_propTypes.default.oneOf(THEME_COLORS), _propTypes.default.string]),
  children: _propTypes.default.node,
  className: _propTypes.default.string
};
var _default = Label;
exports.default = _default;