"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _dark_theme_palette = require("./dark_theme_palette");

var _create_theme = require("./create_theme");

// :copyright: Copyright (c) 2021 ftrack
var darkTheme = (0, _create_theme.createTheme)(_dark_theme_palette.darkThemePalette);
var _default = darkTheme;
exports.default = _default;