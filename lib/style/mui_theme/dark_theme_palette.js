"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.weak-map.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es6.symbol.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.darkThemePalette = void 0;

var data = _interopRequireWildcard(require("../token/design_tokens.json"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

// :copyright: Copyright (c) 2021 ftrack
var _data$default$colors = data.default.colors,
    red = _data$default$colors.red,
    teal = _data$default$colors.teal,
    orange = _data$default$colors.orange,
    yellow = _data$default$colors.yellow,
    purple = _data$default$colors.purple,
    green = _data$default$colors.green,
    darkContrastColor = _data$default$colors.darkContrastColor,
    lightContrastColor = _data$default$colors.lightContrastColor,
    neutral = _data$default$colors.neutral,
    backgroundDark = _data$default$colors.backgroundDark;
var darkThemePalette = {
  mode: "dark",
  background: {
    paper: backgroundDark.paper,
    default: backgroundDark.default,
    imageOverlay: darkContrastColor.disabled,
    raised: backgroundDark.raised,
    backdrop: darkContrastColor.disabled
  },
  text: {
    primary: lightContrastColor.highEmphasis,
    secondary: lightContrastColor.mediumEmphasis,
    disabled: lightContrastColor.disabled,
    icon: lightContrastColor.highEmphasis
  },
  primary: {
    main: purple[200],
    light: purple[100],
    dark: purple[400],
    contrastText: darkContrastColor.highEmphasis
  },
  neutral: {
    main: neutral[100],
    light: neutral[50],
    dark: neutral[400]
  },
  secondary: {
    main: yellow[200],
    light: yellow[100],
    dark: yellow[300],
    contrastText: darkContrastColor.highEmphasis
  },
  error: {
    main: red[300],
    dark: red[400],
    light: red[100],
    contrastText: darkContrastColor.highEmphasis
  },
  info: {
    main: teal[200],
    dark: teal[400],
    light: teal[100],
    contrastText: darkContrastColor.highEmphasis
  },
  warning: {
    main: orange[300],
    dark: orange[400],
    light: orange[100],
    contrastText: darkContrastColor.highEmphasis
  },
  success: {
    main: green[200],
    dark: green[400],
    light: green[100],
    contrastText: darkContrastColor.highEmphasis
  },
  divider: lightContrastColor.faded,
  other: {
    outlineBorder: lightContrastColor.faded,
    standardFieldLine: darkContrastColor.mediumEmphasis,
    snackbar: lightContrastColor.highEmphasis,
    avatarBackground: neutral[600]
  },
  action: {
    active: lightContrastColor.highEmphasis,
    hover: lightContrastColor.highlighted,
    selected: lightContrastColor.highlighted,
    disabled: lightContrastColor.highlighted,
    disabledBackground: lightContrastColor.highlighted,
    focus: lightContrastColor.faded
  }
};
exports.darkThemePalette = darkThemePalette;