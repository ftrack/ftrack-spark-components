"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "lightTheme", {
  enumerable: true,
  get: function get() {
    return _light_theme.default;
  }
});
Object.defineProperty(exports, "darkTheme", {
  enumerable: true,
  get: function get() {
    return _dark_theme.default;
  }
});

var _light_theme = _interopRequireDefault(require("./light_theme.js"));

var _dark_theme = _interopRequireDefault(require("./dark_theme.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }