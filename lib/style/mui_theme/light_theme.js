"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _light_theme_palette = require("./light_theme_palette");

var _create_theme = require("./create_theme");

// :copyright: Copyright (c) 2021 ftrack
var lightTheme = (0, _create_theme.createTheme)(_light_theme_palette.lightThemePalette);
var _default = lightTheme;
exports.default = _default;