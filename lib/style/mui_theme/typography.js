"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.weak-map.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es6.symbol.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.muiTypography = void 0;

var data = _interopRequireWildcard(require("../token/design_tokens.json"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

// :copyright: Copyright (c) 2021 ftrack
var _data$default$typogra = data.default.typography,
    fontSize = _data$default$typogra.fontSize,
    fontWeight = _data$default$typogra.fontWeight;
var muiTypography = {
  htmlFontSize: 10,
  letterSpacing: "normal",
  fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  h1: {
    fontSize: fontSize.h1,
    letterSpacing: "normal",
    fontWeight: fontWeight.thin
  },
  h2: {
    fontSize: fontSize.h2,
    letterSpacing: "normal",
    fontWeight: fontWeight.thin
  },
  h3: {
    fontSize: fontSize.h3,
    letterSpacing: "normal",
    fontWeight: fontWeight.regular
  },
  h4: {
    fontSize: fontSize.h4,
    letterSpacing: "normal",
    fontWeight: fontWeight.regular
  },
  h5: {
    fontSize: fontSize.h5,
    letterSpacing: "normal",
    fontWeight: fontWeight.medium
  },
  h6: {
    fontSize: fontSize.h5,
    letterSpacing: "normal",
    fontWeight: fontWeight.medium
  },
  subtitle1: {
    fontSize: fontSize.regular,
    letterSpacing: "normal"
  },
  subtitle2: {
    fontSize: fontSize.small,
    letterSpacing: "normal"
  },
  body1: {
    fontSize: fontSize.regular,
    letterSpacing: "normal"
  },
  body2: {
    fontSize: fontSize.small,
    letterSpacing: "normal"
  },
  button: {
    fontSize: fontSize.regular
  },
  caption: {
    fontSize: fontSize.xsmall,
    letterSpacing: "normal"
  },
  overline: {
    fontSize: fontSize.xsmall
  },
  fontSizeLarge: fontSize.large,
  fontSizeRegular: fontSize.regular,
  fontSizeSmall: fontSize.small,
  fontSizeXsmall: fontSize.xsmall,
  fontWeightThin: fontWeight.thin,
  fontWeightRegular: fontWeight.regular,
  fontWeightMedium: fontWeight.medium,
  fontWeightBold: fontWeight.bold
};
exports.muiTypography = muiTypography;