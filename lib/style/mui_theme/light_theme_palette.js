"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.weak-map.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es6.symbol.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.lightThemePalette = void 0;

var data = _interopRequireWildcard(require("../token/design_tokens.json"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

// :copyright: Copyright (c) 2021 ftrack
var _data$default$colors = data.default.colors,
    red = _data$default$colors.red,
    teal = _data$default$colors.teal,
    orange = _data$default$colors.orange,
    yellow = _data$default$colors.yellow,
    purple = _data$default$colors.purple,
    green = _data$default$colors.green,
    darkContrastColor = _data$default$colors.darkContrastColor,
    lightContrastColor = _data$default$colors.lightContrastColor,
    neutral = _data$default$colors.neutral,
    backgroundLight = _data$default$colors.backgroundLight;
var lightThemePalette = {
  mode: "light",
  background: {
    paper: backgroundLight.paper,
    default: backgroundLight.default,
    imageOverlay: darkContrastColor.disabled,
    raised: backgroundLight.raised,
    backdrop: darkContrastColor.disabled
  },
  text: {
    primary: darkContrastColor.highEmphasis,
    secondary: darkContrastColor.mediumEmphasis,
    disabled: darkContrastColor.disabled,
    icon: darkContrastColor.highEmphasis
  },
  primary: {
    main: purple[400],
    light: purple[300],
    dark: purple[700],
    contrastText: lightContrastColor.highEmphasis
  },
  neutral: {
    main: neutral[800],
    light: neutral[300],
    dark: neutral[900]
  },
  secondary: {
    main: yellow[400],
    light: yellow[300],
    dark: yellow[500],
    contrastText: lightContrastColor.highEmphasis
  },
  error: {
    main: red[400],
    dark: red[700],
    light: red[300],
    contrastText: lightContrastColor.highEmphasis
  },
  info: {
    main: teal[600],
    dark: teal[800],
    light: teal[400],
    contrastText: lightContrastColor.highEmphasis
  },
  warning: {
    main: orange[500],
    dark: orange[700],
    light: orange[400],
    contrastText: lightContrastColor.highEmphasis
  },
  success: {
    main: green[600],
    dark: green[800],
    light: green[400],
    contrastText: lightContrastColor.highEmphasis
  },
  divider: darkContrastColor.faded,
  other: {
    outlineBorder: darkContrastColor.faded,
    standardFieldLine: lightContrastColor.mediumEmphasis,
    snackbar: darkContrastColor.highEmphasis,
    avatarBackground: neutral[300]
  },
  action: {
    active: darkContrastColor.highEmphasis,
    hover: darkContrastColor.highlighted,
    selected: darkContrastColor.highlighted,
    disabled: darkContrastColor.highlighted,
    disabledBackground: darkContrastColor.highlighted,
    focus: darkContrastColor.faded
  }
};
exports.lightThemePalette = lightThemePalette;