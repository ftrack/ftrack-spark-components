"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _tooltip = _interopRequireDefault(require("react-toolbox/lib/tooltip"));

var _classnames = _interopRequireDefault(require("classnames"));

var _style = _interopRequireDefault(require("./style.scss"));

var _theme = _interopRequireDefault(require("./theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["tooltipPosition", "className", "text", "icon"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var TooltipFontIcon = (0, _tooltip.default)(_font_icon.default);
/**
 * HelpIcon component.
 */

function HelpIcon(props) {
  var tooltipPosition = props.tooltipPosition,
      className = props.className,
      text = props.text,
      icon = props.icon,
      otherProps = _objectWithoutProperties(props, _excluded);

  var iconClassNames = (0, _classnames.default)(_style.default.icon, className);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipFontIcon, _objectSpread({
    tooltipPosition: tooltipPosition,
    theme: _theme.default,
    className: iconClassNames,
    tooltip: text,
    value: icon
  }, otherProps));
}

HelpIcon.propTypes = {
  className: _propTypes.default.string,
  text: _propTypes.default.string,
  icon: _propTypes.default.string,
  tooltipPosition: _propTypes.default.string
};
HelpIcon.defaultProps = {
  className: "",
  text: null,
  icon: "help_outline",
  tooltipPosition: "top"
};
var _default = HelpIcon;
exports.default = _default;