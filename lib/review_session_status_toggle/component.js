"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactIntl = require("react-intl");

var _recompose = require("recompose");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _icon_button = _interopRequireDefault(require("../icon_button"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["status", "onChange"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var IconButtonWithValue = (0, _recompose.withHandlers)({
  onClick: function onClick(props) {
    return function () {
      return props.onChange(props.value);
    };
  }
})(_icon_button.default);
var messages = (0, _reactIntl.defineMessages)({
  "require-changes": {
    "id": "ftrack-spark-components.status-toggle.require-changes",
    "defaultMessage": "Require changes"
  },
  approve: {
    "id": "ftrack-spark-components.status-toggle.approve",
    "defaultMessage": "Approve"
  }
});

function StatusToggle(_ref) {
  var status = _ref.status,
      onChange = _ref.onChange,
      props = _objectWithoutProperties(_ref, _excluded);

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", _objectSpread(_objectSpread({}, props), {}, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(IconButtonWithValue, {
      variant: "large-icon-negative",
      active: status === "require_changes",
      icon: "clear",
      onChange: onChange,
      value: "require_changes",
      className: _style.default.spacing,
      tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["require-changes"]))
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(IconButtonWithValue, {
      variant: "large-icon-positive",
      active: status === "approved",
      icon: "check",
      onChange: onChange,
      value: "approved",
      tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.approve))
    })]
  }));
}

StatusToggle.propTypes = {
  status: _propTypes.default.oneOf(["require_changes", "approved", "seen"]),
  onChange: _propTypes.default.func.isRequired,
  className: _propTypes.default.string
};

var _default = (0, _safe_inject_intl.default)(StatusToggle);

exports.default = _default;