"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.reflect.construct.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.notesLoad = notesLoad;
exports.notesLoadNextPage = notesLoadNextPage;
exports.notesReload = notesReload;
exports.noteCompletionChange = noteCompletionChange;
exports.noteCompletedChanged = noteCompletedChanged;
exports.openNoteForm = openNoteForm;
exports.hideNoteForm = hideNoteForm;
exports.removeNote = removeNote;
exports.notesLoaded = notesLoaded;
exports.noteHighlight = noteHighlight;
exports.submitNoteForm = submitNoteForm;
exports.submitNote = submitNote;
exports.noteSubmitted = noteSubmitted;
exports.noteFormSubmitted = noteFormSubmitted;
exports.noteRemoved = noteRemoved;
exports.noteSubmitError = noteSubmitError;
exports.noteRemoveError = noteRemoveError;
exports.notesLoadError = notesLoadError;
exports.notesLoadCancelled = notesLoadCancelled;
exports.notesLoadSingle = notesLoadSingle;
exports.notesLoadSingleComplete = notesLoadSingleComplete;
exports.reducer = reducer;
exports.notesLoadSaga = notesLoadSaga;
exports.notesReloadSaga = notesReloadSaga;
exports.notesLoadNextPageSaga = notesLoadNextPageSaga;
exports.noteSubmitSaga = noteSubmitSaga;
exports.noteFormSubmitSaga = noteFormSubmitSaga;
exports.noteRemoveSaga = noteRemoveSaga;
exports.noteTodoChangeSaga = noteTodoChangeSaga;
exports.noteHighlightSaga = noteHighlightSaga;
exports.notesLoadSingleSaga = notesLoadSingleSaga;
exports.default = exports.mapDispatchToProps = exports.mapStateToProps = exports.NotesList = exports.sagas = exports.actions = void 0;

require("regenerator-runtime/runtime.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.promise.js");

require("core-js/modules/es6.object.assign.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.array.find-index.js");

require("core-js/modules/es6.array.sort.js");

require("core-js/modules/es6.array.find.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.string.includes.js");

require("core-js/modules/es7.array.includes.js");

var _ftrackJavascriptApi = require("ftrack-javascript-api");

var _loglevel = _interopRequireDefault(require("loglevel"));

var _moment = _interopRequireDefault(require("moment"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _tooltip = _interopRequireDefault(require("react-toolbox/lib/tooltip"));

var _reactIntl = require("react-intl");

var _classnames = _interopRequireDefault(require("classnames"));

var _isNumber = _interopRequireDefault(require("lodash/isNumber"));

var _reactRedux = require("react-redux");

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _reactWaypoint = require("react-waypoint");

var _reduxSaga = require("redux-saga");

var _effects = require("redux-saga/effects");

var _reactFlipMove = _interopRequireDefault(require("react-flip-move"));

var _uuid = require("uuid");

var _scrollIntoViewIfNeeded = _interopRequireDefault(require("scroll-into-view-if-needed"));

var _saga = require("../util/saga");

var _color = require("../util/color");

var _empty_text = _interopRequireDefault(require("../empty_text"));

var _entity_link = _interopRequireDefault(require("../entity_link"));

var _note = _interopRequireDefault(require("../note"));

var _constant = require("../util/constant");

var _hoc = require("../util/hoc");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _skeleton = _interopRequireDefault(require("./skeleton"));

var _style = _interopRequireDefault(require("./style.scss"));

var _tooltip_theme = _interopRequireDefault(require("./tooltip_theme.scss"));

var _string = require("../util/string");

var _schema_operations = _interopRequireDefault(require("../util/schema_operations"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(loadAdditionalNotesInformation),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(removeNoteHandler),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(submitNoteFormHandler),
    _marked4 = /*#__PURE__*/regeneratorRuntime.mark(submitNoteHandler),
    _marked5 = /*#__PURE__*/regeneratorRuntime.mark(loadNotesHandler),
    _marked6 = /*#__PURE__*/regeneratorRuntime.mark(reloadNotesHandler),
    _marked7 = /*#__PURE__*/regeneratorRuntime.mark(notesLoadSingleHandler),
    _marked8 = /*#__PURE__*/regeneratorRuntime.mark(changeTodoHandler),
    _marked9 = /*#__PURE__*/regeneratorRuntime.mark(noteHighlightHandler),
    _marked10 = /*#__PURE__*/regeneratorRuntime.mark(notesLoadSaga),
    _marked11 = /*#__PURE__*/regeneratorRuntime.mark(notesReloadSaga),
    _marked12 = /*#__PURE__*/regeneratorRuntime.mark(notesLoadNextPageSaga),
    _marked13 = /*#__PURE__*/regeneratorRuntime.mark(noteSubmitSaga),
    _marked14 = /*#__PURE__*/regeneratorRuntime.mark(noteFormSubmitSaga),
    _marked15 = /*#__PURE__*/regeneratorRuntime.mark(noteRemoveSaga),
    _marked16 = /*#__PURE__*/regeneratorRuntime.mark(noteTodoChangeSaga),
    _marked17 = /*#__PURE__*/regeneratorRuntime.mark(noteHighlightSaga),
    _marked18 = /*#__PURE__*/regeneratorRuntime.mark(notesLoadSingleSaga);

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var DEFAULT_NAMESPACE = "default";
var DEFAULT_SELECT = ["id", "content", "author.first_name", "author.last_name", "author.thumbnail_id", "author.thumbnail_url", "parent_id", "parent_type", "date", "note_components.component.file_type", "note_components.component.name", "note_components.component.id", "note_components.component.metadata.key", "note_components.thumbnail_url", "note_components.url", "metadata.key", "metadata.value", "in_reply_to_id", "completed_at", "completed_by.first_name", "completed_by.last_name", "completed_by.thumbnail_id", "completed_by.thumbnail_url", "is_todo", "thread_activity", "recipients.resource_id", "recipients.text_mentioned"];
var SpanWithToolTip = (0, _tooltip.default)("span");
var ENTITY_INFORMATION_TOOLTIP_DELAY = 400;

function delay(ms) {
  return new Promise(function (resolve) {
    return setTimeout(function () {
      return resolve(true);
    }, ms);
  });
}

var EditableNote = _note.default.EditableNote,
    ReplyForm = _note.default.ReplyForm,
    NoteForm = _note.default.NoteForm;

var logger = _loglevel.default.getLogger("saga:note");

var NOTES_LOAD = "NOTES_LOAD";
var NOTES_LOAD_NEXT_PAGE = "NOTES_LOAD_NEXT_PAGE";
var NOTES_RELOAD = "NOTES_RELOAD";
var NOTES_LOADED = "NOTES_LOADED";
var NOTES_LOAD_ERROR = "NOTES_LOAD_ERROR";
var NOTES_LOAD_CANCELLED = "NOTES_LOAD_CANCELLED";
var NOTES_LOAD_SINGLE = "NOTES_LOAD_SINGLE";
var NOTES_LOAD_SINGLE_COMPLETE = "NOTES_LOAD_SINGLE_COMPLETE";
var OPEN_NOTE_FORM = "OPEN_NOTE_FORM";
var HIDE_NOTE_FORM = "HIDE_NOTE_FORM";
var SUBMIT_NOTE_FORM = "SUBMIT_NOTE_FORM";
var NOTE_FORM_SUBMITTED = "NOTE_FORM_SUBMITTED";
var SUBMIT_NOTE = "SUBMIT_NOTE";
var REMOVE_NOTE = "REMOVE_NOTE";
var NOTE_SUBMITTED = "NOTE_SUBMITTED";
var NOTE_REMOVED = "NOTE_REMOVED";
var NOTE_SUBMIT_ERROR = "NOTE_SUBMIT_ERROR";
var NOTE_REMOVE_ERROR = "NOTE_REMOVE_ERROR";
var NOTE_TODO_COMPLETED_CHANGE = "NOTE_TODO_COMPLETED_CHANGE";
var NOTE_TODO_COMPLETED_CHANGED = "NOTE_TODO_COMPLETED_CHANGED";
var NOTE_HIGHLIGHT = "NOTE_HIGHLIGHT";
var actions = {
  NOTES_LOAD: NOTES_LOAD,
  NOTES_LOAD_NEXT_PAGE: NOTES_LOAD_NEXT_PAGE,
  NOTES_RELOAD: NOTES_RELOAD,
  NOTES_LOADED: NOTES_LOADED,
  NOTES_LOAD_ERROR: NOTES_LOAD_ERROR,
  NOTES_LOAD_CANCELLED: NOTES_LOAD_CANCELLED,
  NOTES_LOAD_SINGLE: NOTES_LOAD_SINGLE,
  NOTES_LOAD_SINGLE_COMPLETE: NOTES_LOAD_SINGLE_COMPLETE,
  OPEN_NOTE_FORM: OPEN_NOTE_FORM,
  HIDE_NOTE_FORM: HIDE_NOTE_FORM,
  SUBMIT_NOTE_FORM: SUBMIT_NOTE_FORM,
  SUBMIT_NOTE: SUBMIT_NOTE,
  REMOVE_NOTE: REMOVE_NOTE,
  NOTE_FORM_SUBMITTED: NOTE_FORM_SUBMITTED,
  NOTE_SUBMITTED: NOTE_SUBMITTED,
  NOTE_SUBMIT_ERROR: NOTE_SUBMIT_ERROR,
  NOTE_REMOVED: NOTE_REMOVED,
  NOTE_TODO_COMPLETED_CHANGE: NOTE_TODO_COMPLETED_CHANGE,
  NOTE_TODO_COMPLETED_CHANGED: NOTE_TODO_COMPLETED_CHANGED,
  NOTE_HIGHLIGHT: NOTE_HIGHLIGHT
};
exports.actions = actions;
var messages = (0, _reactIntl.defineMessages)({
  "attribute-message-no-notes-found": {
    "id": "ftrack-spark-components-notes-container-attribute-message-no-notes-found",
    "defaultMessage": "There is no feedback yet"
  },
  "top-entity-information": {
    "id": "ftrack-spark-components-notes-container-top-entity-information",
    "defaultMessage": "on"
  },
  "top-entity-information-version-number": {
    "id": "ftrack-spark-components-notes-container-top-entity-information-version",
    "defaultMessage": "Version {versionNumber}"
  }
});
/** Load notes action creator. */

function notesLoad(namespace, id, type, filters, sort, limit, includeAllVersions, noteSelect) {
  return {
    type: NOTES_LOAD,
    meta: {
      namespace: namespace
    },
    payload: {
      entity: {
        id: id,
        type: type
      },
      filters: filters,
      sort: sort,
      limit: limit,
      includeAllVersions: includeAllVersions,
      noteSelect: noteSelect
    }
  };
}
/** Load next page of notes action creator. */


function notesLoadNextPage(namespace, id, type, filters, sort, limit, nextOffset) {
  return {
    type: NOTES_LOAD_NEXT_PAGE,
    meta: {
      namespace: namespace
    },
    payload: {
      entity: {
        id: id,
        type: type
      },
      filters: filters,
      sort: sort,
      limit: limit,
      nextOffset: nextOffset
    }
  };
}
/** Load notes action creator. */


function notesReload(namespace) {
  return {
    type: NOTES_RELOAD,
    meta: {
      namespace: namespace
    },
    payload: {}
  };
}
/** Change note todo completion. */


function noteCompletionChange(namespace, id, isCompleted) {
  return {
    type: NOTE_TODO_COMPLETED_CHANGE,
    meta: {
      namespace: namespace
    },
    payload: {
      id: id,
      isCompleted: isCompleted
    }
  };
}
/** Change note todo completion. */


function noteCompletedChanged(namespace, note) {
  return {
    type: NOTE_TODO_COMPLETED_CHANGED,
    meta: {
      namespace: namespace
    },
    payload: {
      note: note
    }
  };
}
/** Open notes form action creator. */


function openNoteForm(namespace, formKey, data) {
  return {
    type: OPEN_NOTE_FORM,
    meta: {
      namespace: namespace
    },
    payload: {
      formKey: formKey,
      data: data
    }
  };
}
/** Hide notes form action creator. */


function hideNoteForm(namespace, formKey, content) {
  return {
    type: HIDE_NOTE_FORM,
    meta: {
      namespace: namespace
    },
    payload: {
      formKey: formKey,
      content: content
    }
  };
}
/** Remove note action creator. */


function removeNote(namespace, id) {
  return {
    type: REMOVE_NOTE,
    meta: {
      namespace: namespace
    },
    payload: {
      id: id
    }
  };
}
/** Notes loaded action creator. */


function notesLoaded(namespace, entity, notes, nextOffset) {
  return {
    type: NOTES_LOADED,
    meta: {
      namespace: namespace
    },
    payload: {
      entity: entity,
      items: notes,
      nextOffset: nextOffset
    }
  };
}
/** Notes highlight action creator. */


function noteHighlight(namespace, noteId) {
  return {
    type: NOTE_HIGHLIGHT,
    meta: {
      namespace: namespace
    },
    payload: {
      noteId: noteId
    }
  };
}
/** Submit note form action creator. */


function submitNoteForm(namespace, formKey, data, author) {
  return {
    type: SUBMIT_NOTE_FORM,
    meta: {
      namespace: namespace
    },
    payload: {
      formKey: formKey,
      author: author,
      data: data
    }
  };
}
/** Submit note form action creator. */


function submitNote(namespace, data, author) {
  var _ref = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {},
      metadata = _ref.metadata,
      componentIds = _ref.componentIds,
      labelIds = _ref.labelIds;

  return {
    type: SUBMIT_NOTE,
    meta: {
      namespace: namespace
    },
    payload: {
      data: data,
      metadata: metadata,
      componentIds: componentIds,
      labelIds: labelIds,
      author: author
    }
  };
}
/** Notes submitted action creator. */


function noteSubmitted(namespace, note, isUpdate) {
  return {
    type: NOTE_SUBMITTED,
    meta: {
      namespace: namespace
    },
    payload: {
      note: note,
      isUpdate: isUpdate
    }
  };
}
/** Notes submitted action creator. */


function noteFormSubmitted(namespace, formKey, note, isUpdate) {
  return {
    type: NOTE_FORM_SUBMITTED,
    meta: {
      namespace: namespace
    },
    payload: {
      formKey: formKey,
      note: note,
      isUpdate: isUpdate
    }
  };
}
/** Note removed action creator. */


function noteRemoved(namespace, id) {
  return {
    type: NOTE_REMOVED,
    meta: {
      namespace: namespace
    },
    payload: {
      id: id
    }
  };
}
/** Notes submitted action creator. */


function noteSubmitError(namespace, error) {
  return {
    type: NOTE_SUBMIT_ERROR,
    meta: {
      namespace: namespace
    },
    payload: {
      error: error
    }
  };
}
/** Note removed action creator. */


function noteRemoveError(namespace, error) {
  return {
    type: NOTE_REMOVE_ERROR,
    meta: {
      namespace: namespace
    },
    payload: {
      error: error
    }
  };
}
/** Note load error action creator. */


function notesLoadError(namespace, error) {
  return {
    type: NOTES_LOAD_ERROR,
    meta: {
      namespace: namespace
    },
    payload: {
      error: error
    }
  };
}
/** Note load error action creator. */


function notesLoadCancelled(namespace) {
  return {
    type: NOTES_LOAD_CANCELLED,
    meta: {
      namespace: namespace
    },
    payload: {}
  };
}
/** Load note action creator. */


function notesLoadSingle(namespace, noteId) {
  return {
    type: NOTES_LOAD_SINGLE,
    meta: {
      namespace: namespace
    },
    payload: {
      noteId: noteId
    }
  };
}
/** Load note action creator. */


function notesLoadSingleComplete(namespace, note) {
  return {
    type: NOTES_LOAD_SINGLE_COMPLETE,
    meta: {
      namespace: namespace
    },
    payload: {
      note: note
    }
  };
}

var sortNotesCreator = function sortNotesCreator(field) {
  return function (a, b) {
    if (a[field].isSame(b[field])) {
      if (a.id > b.id) {
        return -1;
      }

      if (a.id < b.id) {
        return 1;
      }

      return 0;
    }

    if (a[field].toDate() < b[field].toDate()) {
      return 1;
    }

    return -1;
  };
};

var sortNotesOnDate = sortNotesCreator("date");
var sortNotesOnThreadActivity = sortNotesCreator("thread_activity");

function sortNotesOnFrame(noteA, noteB) {
  var _noteA$frame = noteA.frame,
      frameA = _noteA$frame === void 0 ? {} : _noteA$frame;
  var _noteB$frame = noteB.frame,
      frameB = _noteB$frame === void 0 ? {} : _noteB$frame;

  if ((0, _isNumber.default)(frameB.number) && (0, _isNumber.default)(frameA.number)) {
    return frameA.number - frameB.number;
  }

  if ((0, _isNumber.default)(frameA.number)) {
    return -1;
  }

  if ((0, _isNumber.default)(frameB.number)) {
    return 1;
  }

  return 0;
}
/**
 * Reduce state for application-wide notes.
 */


function reducer() {
  var multiState = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var action = arguments.length > 1 ? arguments[1] : undefined;
  var namespace = action.meta && action.meta.namespace;

  if (!namespace) {
    // Ignore all actions where namespace is not defined.
    return multiState;
  }

  var state = multiState[namespace] || {
    highlight: null
  };
  var nextState = state;

  if (action.type === actions.NOTES_LOAD) {
    nextState = Object.assign({}, state, {
      entity: action.payload.entity,
      noteSelect: action.payload.noteSelect,
      items: [],
      nextOffset: null,
      loading: true,
      // Record latest payload so that it can be used for reload.
      latestPayload: _objectSpread({}, action.payload)
    });
  }

  if (action.type === actions.NOTES_LOAD_NEXT_PAGE) {
    nextState = Object.assign({}, state, {
      loading: true
    });
  } else if (action.type === actions.NOTES_LOADED) {
    var items = [].concat(_toConsumableArray(state.items), _toConsumableArray(action.payload.items));
    nextState = Object.assign({}, state, {
      items: items,
      nextOffset: action.payload.nextOffset,
      loading: false
    });
  } else if (action.type === actions.OPEN_NOTE_FORM) {
    var forms = Object.assign({}, state.forms);
    var exists = forms[action.payload.formKey] !== undefined;
    var initialState = exists ? {} : action.payload.data;
    forms[action.payload.formKey] = Object.assign({}, forms[action.payload.formKey], {
      state: "visible"
    }, initialState);
    nextState = Object.assign({}, state, {
      forms: forms
    });
  } else if (action.type === actions.HIDE_NOTE_FORM) {
    var _forms = Object.assign({}, state.forms);

    _forms[action.payload.formKey] = Object.assign({}, state.forms[action.payload.formKey], {
      state: "hidden",
      content: action.payload.content
    });
    nextState = Object.assign({}, state, {
      forms: _forms
    });
  } else if (action.type === actions.SUBMIT_NOTE_FORM) {
    var _forms2 = Object.assign({}, state.forms);

    _forms2[action.payload.formKey] = Object.assign({}, state.forms[action.payload.formKey], {
      state: "pending"
    });
    nextState = Object.assign({}, state, {
      forms: _forms2
    });
  } else if (action.type === actions.NOTE_SUBMITTED) {
    var _items;

    var nextOffset = state.nextOffset;

    if (action.payload.isUpdate) {
      // Find and update the note if the operation is an update.
      _items = state.items.map(function (note) {
        if (note.id === action.payload.note.id) {
          return action.payload.note;
        } else if (note.id === action.payload.note.in_reply_to_id) {
          var parentNote = Object.assign({}, note);
          parentNote.replies = note.replies.map(function (reply) {
            if (reply.id === action.payload.note.id) {
              return action.payload.note;
            }

            return reply;
          });
          return parentNote;
        }

        return note;
      });
    } else if (action.payload.note.in_reply_to_id) {
      _items = state.items.map(function (note) {
        if (note.id === action.payload.note.in_reply_to_id) {
          var replies = [].concat(_toConsumableArray(note.replies), [action.payload.note]);
          return Object.assign({}, note, {
            replies: replies
          });
        }

        return note;
      });
    } else {
      _items = [action.payload.note].concat(_toConsumableArray(state.items));
    }

    nextState = Object.assign({}, state, {
      items: _items,
      nextOffset: nextOffset
    });
  } else if (action.type === actions.NOTE_FORM_SUBMITTED) {
    var _forms3 = Object.assign({}, state.forms);

    delete _forms3[action.payload.formKey];
    nextState = Object.assign({}, state, {
      forms: _forms3
    });
  } else if (action.type === actions.NOTE_REMOVED) {
    // Find and remove the note if the action is a note being removed.
    var _items2 = [];
    var _nextOffset = state.nextOffset;
    state.items.forEach(function (note) {
      if (note.id !== action.payload.id) {
        var nextNote = note;
        var replies = [];
        note.replies.forEach(function (reply) {
          if (reply.id !== action.payload.id) {
            replies.push(reply);
          }
        }); // If length differ a matching note was removed and the
        // replies array must be updated.

        if (nextNote.replies.length !== replies.length) {
          nextNote = _objectSpread(_objectSpread({}, note), {}, {
            replies: replies
          });
        }

        _items2.push(nextNote);
      }
    });
    nextState = Object.assign({}, state, {
      items: _items2,
      nextOffset: _nextOffset
    });
  } else if (action.type === actions.NOTE_TODO_COMPLETED_CHANGE) {
    var pendingTodos = Object.assign({}, state.pendingTodos);
    pendingTodos[action.payload.id] = true;
    nextState = Object.assign({}, state, {
      pendingTodos: pendingTodos
    });
  } else if (action.type === actions.NOTE_TODO_COMPLETED_CHANGED) {
    // Find and update the note.
    var _items3 = state.items.map(function (note) {
      if (note.id === action.payload.note.id) {
        return action.payload.note;
      } else if (note.id === action.payload.note.in_reply_to_id) {
        var parentNote = Object.assign({}, note);
        parentNote.replies = note.replies.map(function (reply) {
          if (reply.id === action.payload.note.id) {
            return action.payload.note;
          }

          return reply;
        });
        return parentNote;
      }

      return note;
    });

    var _pendingTodos = Object.assign({}, state.pendingTodos);

    delete _pendingTodos[action.payload.note.id];
    nextState = Object.assign({}, state, {
      pendingTodos: _pendingTodos,
      items: _items3
    });
  } else if (action.type === actions.NOTES_LOAD_SINGLE_COMPLETE) {
    var nextItems = state.items;
    var note = action.payload.note;
    var inReplyToId = note.in_reply_to_id;

    if (inReplyToId) {
      var parentIndex = state.items.findIndex(function (candidate) {
        return candidate.id === inReplyToId;
      });

      if (parentIndex !== -1) {
        var parentNote = state.items[parentIndex];
        var replies = parentNote.replies;
        var replyIndex = replies.findIndex(function (candidate) {
          return candidate.id === note.id;
        });
        var nextReplies;

        if (replyIndex === -1) {
          nextReplies = [].concat(_toConsumableArray(replies), [note]);
        } else {
          nextReplies = _toConsumableArray(replies);
          nextReplies[replyIndex] = note;
        }

        nextItems = _toConsumableArray(state.items);
        nextItems[parentIndex] = _objectSpread(_objectSpread({}, parentNote), {}, {
          replies: nextReplies
        });
      }
    } else {
      var noteIndex = state.items.findIndex(function (candidate) {
        return candidate.id === note.id;
      }); // Add or update note

      if (noteIndex === -1) {
        nextItems = [note].concat(_toConsumableArray(state.items));
      } else {
        nextItems = _toConsumableArray(state.items);
        nextItems[noteIndex] = note;
      } // Resort items


      var sort = state.latestPayload.sort;

      if (sort === "frame") {
        nextItems.sort(sortNotesOnFrame);
      } else {
        nextItems.sort(sortNotesOnThreadActivity);
      }
    }

    nextState = _objectSpread(_objectSpread({}, state), {}, {
      items: nextItems
    });
  } else if (action.type === actions.NOTE_HIGHLIGHT) {
    nextState = Object.assign({}, state, {
      highlight: action.payload.noteId
    });
  }

  if (state !== nextState) {
    return _objectSpread(_objectSpread({}, multiState), {}, _defineProperty({}, namespace, nextState));
  }

  return multiState;
}
/** Load additional information on *notes* */


function loadAdditionalNotesInformation(notes, session) {
  var relatedEntitiesSelect, reviewObjectAttributes, schema, hasLinkAttribute, extraInformationQuery, processNoteMeta, noteParentIds, relatedEntitiesResponse, extraInformation;
  return regeneratorRuntime.wrap(function loadAdditionalNotesInformation$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          processNoteMeta = function _processNoteMeta(note) {
            note.metadata.forEach(function (item) {
              if (item.key === "reviewFrame") {
                try {
                  note.frame = JSON.parse(item.value);
                } catch (error) {// Frame number has not been set correctly, do
                  // nothing.
                }
              }

              if (item.key === "inviteeStatus") {
                note.inviteeStatus = item.value;
              }
            });
          };

          relatedEntitiesSelect = "id, link, status.color, thumbnail_url";
          reviewObjectAttributes = ["name", "description", "version", "asset_version.thumbnail_url", "review_session.name", "review_session_id", "review_session.project_id", "review_session.project.full_name"]; // TODO: This can be removed once merging works as expected.
          // It is required to ensure that AssetVersion contains link when
          // returned both as part of ReviewSessionObject and separately.

          schema = session.schemas.find(function (candidate) {
            return candidate.id === "AssetVersion";
          });
          hasLinkAttribute = schema && schema.properties.link || false;

          if (hasLinkAttribute) {
            reviewObjectAttributes.push("asset_version.link");
          } // Gather all version ids of assets publish on entity.


          extraInformationQuery = {
            AssetVersion: "select ".concat(relatedEntitiesSelect, " from AssetVersion"),
            TypedContext: "select ".concat(relatedEntitiesSelect, " from TypedContext"),
            Project: "select id, link, thumbnail_url from Project",
            ReviewSessionObject: "select ".concat(reviewObjectAttributes.join(","), " from ReviewSessionObject")
          };
          noteParentIds = notes.reduce(function (accumulator, note) {
            if (!accumulator[note.parent_type]) {
              accumulator[note.parent_type] = [];
            }

            accumulator[note.parent_type].push(note.parent_id);
            return accumulator;
          }, {});
          _context.next = 10;
          return (0, _effects.call)([session, session.call], Object.keys(noteParentIds).map(function (model) {
            var targetModel = model;
            var targetSchema = session.schemas.find(function (schema) {
              return schema.id === model;
            });

            if (targetSchema.alias_for && targetSchema.alias_for.id === "Task") {
              targetModel = "TypedContext";
            }

            return _ftrackJavascriptApi.operation.query("".concat(extraInformationQuery[targetModel], " where id in (").concat(noteParentIds[model], ")"));
          }));

        case 10:
          relatedEntitiesResponse = _context.sent;
          extraInformation = {};
          relatedEntitiesResponse.forEach(function (item) {
            item.data.forEach(function (partial) {
              return extraInformation[partial.id] = partial;
            });
          });
          notes.forEach(function (note) {
            if (extraInformation[note.parent_id]) {
              note.extraInformation = extraInformation[note.parent_id];
            }

            processNoteMeta(note);

            if (note.replies) {
              // Note replies has a category in the model that is not visible to
              // the end-user.
              // TODO: Change this when displaying frame numbers as tags.
              note.replies.forEach(function (reply) {
                processNoteMeta(reply);
                delete reply.category;
              }); // Order replies since there is garantuee that the they are ordered.

              note.replies.sort(sortNotesOnDate);
              note.replies.reverse();
            }
          });

        case 14:
        case "end":
          return _context.stop();
      }
    }
  }, _marked);
}
/** Handle remove note *action*. */


function removeNoteHandler(action) {
  var deleteOperation, session;
  return regeneratorRuntime.wrap(function removeNoteHandler$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          deleteOperation = _ftrackJavascriptApi.operation.delete("Note", [action.payload.id]);
          _context2.next = 3;
          return (0, _effects.getContext)("ftrackSession");

        case 3:
          session = _context2.sent;
          _context2.prev = 4;
          _context2.next = 7;
          return (0, _effects.call)([session, session.call], [deleteOperation]);

        case 7:
          _context2.next = 14;
          break;

        case 9:
          _context2.prev = 9;
          _context2.t0 = _context2["catch"](4);
          _context2.next = 13;
          return (0, _effects.put)(noteRemoveError(action.meta.namespace, _context2.t0));

        case 13:
          return _context2.abrupt("return");

        case 14:
          _context2.next = 16;
          return (0, _effects.put)(noteRemoved(action.meta.namespace, action.payload.id));

        case 16:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, null, [[4, 9]]);
}

function submitNoteFormHandler(action) {
  return regeneratorRuntime.wrap(function submitNoteFormHandler$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return (0, _effects.put)(submitNote(action.meta.namespace, action.payload.data, action.payload.author));

        case 2:
          _context3.next = 4;
          return (0, _effects.put)(noteFormSubmitted(action.meta.namespace, action.payload.formKey));

        case 4:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3);
}
/** Handle submit note *action*. */


function submitNoteHandler(action) {
  var submitNoteOperations, isUpdate, componentIds, author, labelIds, recipients, session, _noteId, existingNoteLabelsResponse, existingNoteLabels, deletableNoteLabels, newNoteLabelIds, data, submitResponse, noteId, noteSelect, query, response, _response$data, note;

  return regeneratorRuntime.wrap(function submitNoteHandler$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          submitNoteOperations = [];
          isUpdate = !!action.payload.data.id;
          componentIds = action.payload.componentIds;
          author = action.payload.author;
          labelIds = action.payload.data.labelIds || [];
          recipients = action.payload.data.recipients;
          _context4.next = 8;
          return (0, _effects.getContext)("ftrackSession");

        case 8:
          session = _context4.sent;

          if (!isUpdate) {
            _context4.next = 22;
            break;
          }

          _noteId = action.payload.data.id;
          _context4.next = 13;
          return session.query("select label_id from NoteLabelLink where note_id is \"".concat(_noteId, "\""));

        case 13:
          existingNoteLabelsResponse = _context4.sent;
          existingNoteLabels = existingNoteLabelsResponse.data || [];
          deletableNoteLabels = existingNoteLabels.filter(function (noteLabel) {
            return !labelIds.includes(noteLabel.label_id);
          });
          newNoteLabelIds = labelIds.filter(function (labelId) {
            return !existingNoteLabels.some(function (noteLabel) {
              return noteLabel.label_id === labelId;
            });
          });
          submitNoteOperations.push(_ftrackJavascriptApi.operation.update("Note", [_noteId], {
            content: action.payload.data.content,
            is_todo: action.payload.data.is_todo === true
          }));
          submitNoteOperations.push.apply(submitNoteOperations, _toConsumableArray(deletableNoteLabels.map(function (deletableNoteLabel) {
            return _ftrackJavascriptApi.operation.delete("NoteLabelLink", [_noteId, deletableNoteLabel.label_id]);
          })));
          submitNoteOperations.push.apply(submitNoteOperations, _toConsumableArray(newNoteLabelIds.map(function (labelId) {
            return _ftrackJavascriptApi.operation.create("NoteLabelLink", {
              label_id: labelId,
              note_id: _noteId
            });
          })));
          _context4.next = 28;
          break;

        case 22:
          data = {
            content: action.payload.data.content,
            parent_id: action.payload.data.parent_id,
            parent_type: action.payload.data.parent_type,
            in_reply_to_id: action.payload.data.in_reply_to_id,
            is_todo: action.payload.data.is_todo === true,
            user_id: author.id,
            id: (0, _uuid.v4)()
          };
          submitNoteOperations.push(_ftrackJavascriptApi.operation.create("Note", data));

          if (action.payload.metadata) {
            submitNoteOperations.push.apply(submitNoteOperations, _toConsumableArray(action.payload.metadata.map(function (item) {
              return _ftrackJavascriptApi.operation.create("Metadata", _objectSpread(_objectSpread({}, item), {}, {
                parent_id: data.id,
                parent_type: "Note"
              }));
            })));
          }

          if (componentIds) {
            submitNoteOperations.push.apply(submitNoteOperations, _toConsumableArray(componentIds.map(function (componentId) {
              return _ftrackJavascriptApi.operation.create("NoteComponent", {
                component_id: componentId,
                note_id: data.id
              });
            })));
          }

          if (labelIds) {
            submitNoteOperations.push.apply(submitNoteOperations, _toConsumableArray(labelIds.map(function (labelId) {
              return _ftrackJavascriptApi.operation.create("NoteLabelLink", {
                label_id: labelId,
                note_id: data.id
              });
            })));
          }

          if (recipients) {
            submitNoteOperations.push.apply(submitNoteOperations, _toConsumableArray(recipients.map(function (recipient) {
              return _ftrackJavascriptApi.operation.create("Recipient", {
                note_id: data.id,
                resource_id: recipient.resource_id,
                text_mentioned: recipient.text_mentioned
              });
            })));
          }

        case 28:
          _context4.prev = 28;
          _context4.next = 31;
          return (0, _effects.call)([session, session.call], submitNoteOperations);

        case 31:
          submitResponse = _context4.sent;
          _context4.next = 39;
          break;

        case 34:
          _context4.prev = 34;
          _context4.t0 = _context4["catch"](28);
          _context4.next = 38;
          return (0, _effects.put)(noteSubmitError(action.meta.namespace, _context4.t0));

        case 38:
          throw _context4.t0;

        case 39:
          noteId = submitResponse[0].data.id;

          if (!isUpdate) {
            _context4.next = 51;
            break;
          }

          _context4.prev = 41;
          _context4.next = 44;
          return recipients.map(function (recipient) {
            return session.ensure("Recipient", {
              note_id: noteId,
              resource_id: recipient.resource_id,
              text_mentioned: recipient.text_mentioned
            }, ["resource_id", "note_id"]);
          });

        case 44:
          _context4.next = 51;
          break;

        case 46:
          _context4.prev = 46;
          _context4.t1 = _context4["catch"](41);
          _context4.next = 50;
          return (0, _effects.put)(noteSubmitError(action.meta.namespace, _context4.t1));

        case 50:
          throw _context4.t1;

        case 51:
          _context4.next = 53;
          return (0, _effects.select)(function (state) {
            return state.notes[action.meta.namespace].noteSelect;
          });

        case 53:
          noteSelect = _context4.sent;
          query = "".concat(noteSelect, " where id is \"").concat(noteId, "\"");
          _context4.next = 57;
          return (0, _effects.call)([session, session.query], query);

        case 57:
          response = _context4.sent;
          _response$data = _slicedToArray(response.data, 1), note = _response$data[0];
          _context4.next = 61;
          return loadAdditionalNotesInformation([note], session);

        case 61:
          _context4.next = 63;
          return (0, _effects.put)(noteSubmitted(action.meta.namespace, note, isUpdate));

        case 63:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4, null, [[28, 34], [41, 46]]);
}
/** Handle load notes and load next page *action*. */


function loadNotesHandler(action) {
  var offset, limit, sort, includeAllVersions, session, entity, baseFilters, filters, pageFilter, baseFiltersExpression, noteSelect, queries, response, notes, nextOffset, lastNote, isCancelled;
  return regeneratorRuntime.wrap(function loadNotesHandler$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          offset = 0;
          limit = action.payload.limit;
          sort = action.payload.sort;
          includeAllVersions = action.payload.includeAllVersions;

          if (action.type === actions.NOTES_LOAD_NEXT_PAGE) {
            offset = action.payload.nextOffset;
          }

          _context5.next = 7;
          return (0, _effects.getContext)("ftrackSession");

        case 7:
          session = _context5.sent;
          entity = action.payload.entity;
          baseFilters = action.payload.filters;
          filters = [];

          if (!(entity.type === "Project")) {
            _context5.next = 18;
            break;
          }

          filters.push("parent_id is \"".concat(entity.id, "\""));
          filters.push("parent_id in (select id from TypedContext where project_id is \"".concat(entity.id, "\")"));
          filters.push("parent_id in (select id from AssetVersion where asset.context_id is \"".concat(entity.id, "\"") + "or asset.parent.project_id is \"".concat(entity.id, "\")"));
          filters.push("parent_id in (select id from ReviewSessionObject where review_session.project_id " + " is \"".concat(entity.id, "\" and review_session.is_moderated is False)"));
          _context5.next = 38;
          break;

        case 18:
          if (!((0, _schema_operations.default)(entity.type, session.schemas) || entity.type === "TypedContext")) {
            _context5.next = 25;
            break;
          }

          filters.push("parent_id is \"".concat(entity.id, "\""));
          filters.push("parent_id in (select id from TypedContext where ancestors.id is \"".concat(entity.id, "\")"));
          filters.push("parent_id in (select id from AssetVersion where asset.context_id is \"".concat(entity.id, "\"") + " or asset.parent.ancestors.id is \"".concat(entity.id, "\"") + " or task_id is \"".concat(entity.id, "\")"));
          filters.push("parent_id in (select id from ReviewSessionObject where " + "(asset_version.asset.context_id is \"".concat(entity.id, "\" or ") + "asset_version.asset.parent.ancestors.id is \"".concat(entity.id, "\") and ") + "review_session.is_moderated is False)");
          _context5.next = 38;
          break;

        case 25:
          if (!(entity.type === "ReviewSessionObject")) {
            _context5.next = 29;
            break;
          }

          if (includeAllVersions) {
            filters.push('parent_type is "review_session_object"');
          } else {
            filters.push("parent_id is \"".concat(entity.id, "\""));
          }

          _context5.next = 38;
          break;

        case 29:
          if (!(entity.type === "ReviewSession")) {
            _context5.next = 33;
            break;
          }

          filters.push("parent_id in (select id from ReviewSessionObject where " + "review_session_id is \"".concat(entity.id, "\")"));
          _context5.next = 38;
          break;

        case 33:
          if (!(entity.type === "AssetVersion")) {
            _context5.next = 37;
            break;
          }

          if (includeAllVersions) {
            filters.push("parent_id in (select id from AssetVersion where asset.versions.id is \"".concat(entity.id, "\")"));
            filters.push("parent_id in (select id from ReviewSessionObject where " + "(asset_version.asset.versions.id is \"".concat(entity.id, "\" and ") + "review_session.is_moderated is False))");
          } else {
            filters.push("parent_id is \"".concat(entity.id, "\""));
            filters.push("parent_id in (select id from ReviewSessionObject where " + "(asset_version.id is \"".concat(entity.id, "\" and ") + "review_session.is_moderated is False))");
          }

          _context5.next = 38;
          break;

        case 37:
          throw new Error("Notes cannot be fetched for type: ".concat(entity.type, "."));

        case 38:
          pageFilter = "";

          if (offset) {
            pageFilter = " and (thread_activity < \"".concat(offset.date.format("YYYY-MM-DDTHH:mm:ss"), "\" or ") + "(thread_activity = \"".concat(offset.date.format("YYYY-MM-DDTHH:mm:ss"), "\" and id < \"").concat(offset.id, "\"))");
          }

          baseFiltersExpression = baseFilters ? "and (".concat(baseFilters, ")") : "";
          _context5.next = 43;
          return (0, _effects.select)(function (state) {
            return state.notes[action.meta.namespace].noteSelect;
          });

        case 43:
          noteSelect = _context5.sent;
          queries = filters.map(function (filter) {
            return "".concat(noteSelect, " where ") + "(".concat(filter, ") and not in_reply_to has () ").concat(baseFiltersExpression, " ").concat(pageFilter, " ") + "order by thread_activity desc, id desc limit ".concat(limit);
          });
          _context5.prev = 45;
          logger.debug('Loading notes with "', queries, '" from action', action);
          _context5.next = 49;
          return (0, _effects.call)([session, session.call], queries.map(function (query) {
            return _ftrackJavascriptApi.operation.query(query);
          }));

        case 49:
          response = _context5.sent;
          logger.debug("Notes query result: ", response);
          notes = response.reduce(function (accumulator, result) {
            return accumulator.concat(result.data);
          }, []);
          notes.sort(sortNotesOnThreadActivity);
          notes = notes.splice(0, limit);
          _context5.next = 56;
          return loadAdditionalNotesInformation(notes, session);

        case 56:
          nextOffset = null;

          if (notes.length && notes.length >= limit) {
            lastNote = notes[notes.length - 1];
            nextOffset = {
              date: lastNote.thread_activity,
              id: lastNote.id
            };
          }

          if (sort === "frame") {
            notes.sort(sortNotesOnFrame);

            if (nextOffset !== null) {
              logger.warn("Notes sort on frame does not support paged results.");
            }
          }

          _context5.next = 61;
          return (0, _effects.put)(notesLoaded(action.meta.namespace, {
            id: action.payload.entity.id,
            type: action.payload.entity.type
          }, notes, nextOffset));

        case 61:
          _context5.next = 67;
          break;

        case 63:
          _context5.prev = 63;
          _context5.t0 = _context5["catch"](45);
          _context5.next = 67;
          return (0, _effects.put)(notesLoadError(action.meta.namespace, _context5.t0));

        case 67:
          _context5.prev = 67;
          _context5.next = 70;
          return (0, _effects.cancelled)();

        case 70:
          isCancelled = _context5.sent;

          if (!isCancelled) {
            _context5.next = 74;
            break;
          }

          _context5.next = 74;
          return (0, _effects.put)(notesLoadCancelled(action.meta.namespace));

        case 74:
          return _context5.finish(67);

        case 75:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5, null, [[45, 63, 67, 75]]);
}

function reloadNotesHandler(action) {
  var _yield$select, entity, filters, sort, limit, includeAllVersions, noteSelect;

  return regeneratorRuntime.wrap(function reloadNotesHandler$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.next = 2;
          return (0, _effects.select)(function (state) {
            return state.notes[action.meta.namespace].latestPayload;
          });

        case 2:
          _yield$select = _context6.sent;
          entity = _yield$select.entity;
          filters = _yield$select.filters;
          sort = _yield$select.sort;
          limit = _yield$select.limit;
          includeAllVersions = _yield$select.includeAllVersions;
          noteSelect = _yield$select.noteSelect;
          _context6.next = 11;
          return (0, _effects.put)(notesLoad(action.meta.namespace, entity.id, entity.type, filters, sort, limit, includeAllVersions, noteSelect));

        case 11:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6);
}

function notesLoadSingleHandler(_ref2) {
  var meta, payload, namespace, noteId, session, noteState, noteSelect, noteResponse, data;
  return regeneratorRuntime.wrap(function notesLoadSingleHandler$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          meta = _ref2.meta, payload = _ref2.payload;
          namespace = meta.namespace;
          noteId = payload.noteId;
          _context7.next = 5;
          return (0, _effects.getContext)("ftrackSession");

        case 5:
          session = _context7.sent;
          _context7.next = 8;
          return (0, _effects.select)(function (state) {
            return state.notes[namespace];
          });

        case 8:
          noteState = _context7.sent;

          if (!noteState) {
            _context7.next = 20;
            break;
          }

          noteSelect = noteState.noteSelect;
          _context7.next = 13;
          return session.query("".concat(noteSelect, " where id is \"").concat(noteId, "\""));

        case 13:
          noteResponse = _context7.sent;
          data = noteResponse.data[0];

          if (!data) {
            _context7.next = 20;
            break;
          }

          _context7.next = 18;
          return loadAdditionalNotesInformation([data], session);

        case 18:
          _context7.next = 20;
          return (0, _effects.put)(notesLoadSingleComplete(namespace, data));

        case 20:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7);
}
/** Handle todo change *action* */


function changeTodoHandler(action) {
  var session, _action$payload, id, isCompleted, userId, completedAt, response, noteSelect, updatedNoteResponse;

  return regeneratorRuntime.wrap(function changeTodoHandler$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.next = 2;
          return (0, _effects.getContext)("ftrackSession");

        case 2:
          session = _context8.sent;
          _action$payload = action.payload, id = _action$payload.id, isCompleted = _action$payload.isCompleted;
          userId = null;
          completedAt = null;

          if (!isCompleted) {
            _context8.next = 12;
            break;
          }

          _context8.next = 9;
          return session.query("select id from User where username is \"".concat(session.apiUser, "\""));

        case 9:
          response = _context8.sent;
          userId = response.data[0].id;
          completedAt = (0, _moment.default)().format(_constant.DATETIME_ISO_FORMAT);

        case 12:
          _context8.next = 14;
          return session.update("Note", [id], {
            completed_by_id: userId,
            completed_at: completedAt
          });

        case 14:
          _context8.next = 16;
          return (0, _effects.select)(function (state) {
            return state.notes[action.meta.namespace].noteSelect;
          });

        case 16:
          noteSelect = _context8.sent;
          _context8.next = 19;
          return session.query("".concat(noteSelect, " where id is \"").concat(id, "\""));

        case 19:
          updatedNoteResponse = _context8.sent;
          _context8.next = 22;
          return (0, _effects.put)(noteCompletedChanged(action.meta.namespace, updatedNoteResponse.data[0]));

        case 22:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8);
}

function noteHighlightHandler(action) {
  return regeneratorRuntime.wrap(function noteHighlightHandler$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          if (!action.payload.noteId) {
            _context9.next = 5;
            break;
          }

          _context9.next = 3;
          return (0, _effects.call)(delay, 1000);

        case 3:
          _context9.next = 5;
          return (0, _effects.put)(noteHighlight(action.meta.namespace, null));

        case 5:
        case "end":
          return _context9.stop();
      }
    }
  }, _marked9);
}
/** Handle NOTES_LOAD action.
 *
 * Cancel concurrent note load tasks if they are in the same namespace.
 */


function notesLoadSaga() {
  var tasks, action, task;
  return regeneratorRuntime.wrap(function notesLoadSaga$(_context10) {
    while (1) {
      switch (_context10.prev = _context10.next) {
        case 0:
          tasks = {}; // eslint-disable-next-line no-constant-condition

        case 1:
          if (!true) {
            _context10.next = 14;
            break;
          }

          _context10.next = 4;
          return (0, _effects.take)(actions.NOTES_LOAD);

        case 4:
          action = _context10.sent;
          task = tasks[action.meta.namespace];

          if (!task) {
            _context10.next = 9;
            break;
          }

          _context10.next = 9;
          return (0, _effects.cancel)(task);

        case 9:
          _context10.next = 11;
          return (0, _effects.fork)(loadNotesHandler, action);

        case 11:
          tasks[action.meta.namespace] = _context10.sent;
          _context10.next = 1;
          break;

        case 14:
        case "end":
          return _context10.stop();
      }
    }
  }, _marked10);
}
/** Handle NOTES_RELOAD action. */


function notesReloadSaga() {
  return regeneratorRuntime.wrap(function notesReloadSaga$(_context11) {
    while (1) {
      switch (_context11.prev = _context11.next) {
        case 0:
          _context11.next = 2;
          return (0, _reduxSaga.takeEvery)(actions.NOTES_RELOAD, (0, _saga.catchAllErrors)(reloadNotesHandler));

        case 2:
        case "end":
          return _context11.stop();
      }
    }
  }, _marked11);
}
/** Handle NOTES_LOAD action.
 *
 * Cancel concurrent note load tasks if they are in the same namespace.
 */


function notesLoadNextPageSaga() {
  var tasks, action, task;
  return regeneratorRuntime.wrap(function notesLoadNextPageSaga$(_context12) {
    while (1) {
      switch (_context12.prev = _context12.next) {
        case 0:
          tasks = {}; // eslint-disable-next-line no-constant-condition

        case 1:
          if (!true) {
            _context12.next = 14;
            break;
          }

          _context12.next = 4;
          return (0, _effects.take)(actions.NOTES_LOAD_NEXT_PAGE);

        case 4:
          action = _context12.sent;
          task = tasks[action.meta.namespace];

          if (!task) {
            _context12.next = 9;
            break;
          }

          _context12.next = 9;
          return (0, _effects.cancel)(task);

        case 9:
          _context12.next = 11;
          return (0, _effects.fork)(loadNotesHandler, action);

        case 11:
          tasks[action.meta.namespace] = _context12.sent;
          _context12.next = 1;
          break;

        case 14:
        case "end":
          return _context12.stop();
      }
    }
  }, _marked12);
}
/** Handle SUBMIT_NOTE_FORM action. */


function noteSubmitSaga() {
  return regeneratorRuntime.wrap(function noteSubmitSaga$(_context13) {
    while (1) {
      switch (_context13.prev = _context13.next) {
        case 0:
          _context13.next = 2;
          return (0, _reduxSaga.takeEvery)(actions.SUBMIT_NOTE, (0, _saga.catchAllErrors)(submitNoteHandler));

        case 2:
        case "end":
          return _context13.stop();
      }
    }
  }, _marked13);
}
/** Handle SUBMIT_NOTE_FORM action. */


function noteFormSubmitSaga() {
  return regeneratorRuntime.wrap(function noteFormSubmitSaga$(_context14) {
    while (1) {
      switch (_context14.prev = _context14.next) {
        case 0:
          _context14.next = 2;
          return (0, _reduxSaga.takeEvery)(actions.SUBMIT_NOTE_FORM, submitNoteFormHandler);

        case 2:
        case "end":
          return _context14.stop();
      }
    }
  }, _marked14);
}
/** Handle REMOVE_NOTE action. */


function noteRemoveSaga() {
  return regeneratorRuntime.wrap(function noteRemoveSaga$(_context15) {
    while (1) {
      switch (_context15.prev = _context15.next) {
        case 0:
          _context15.next = 2;
          return (0, _reduxSaga.takeEvery)(actions.REMOVE_NOTE, removeNoteHandler);

        case 2:
        case "end":
          return _context15.stop();
      }
    }
  }, _marked15);
}
/** Handle REMOVE_NOTE action. */


function noteTodoChangeSaga() {
  return regeneratorRuntime.wrap(function noteTodoChangeSaga$(_context16) {
    while (1) {
      switch (_context16.prev = _context16.next) {
        case 0:
          _context16.next = 2;
          return (0, _reduxSaga.takeEvery)(actions.NOTE_TODO_COMPLETED_CHANGE, changeTodoHandler);

        case 2:
        case "end":
          return _context16.stop();
      }
    }
  }, _marked16);
}
/** Handle NOTE_HIGHLIGHT action. */


function noteHighlightSaga() {
  return regeneratorRuntime.wrap(function noteHighlightSaga$(_context17) {
    while (1) {
      switch (_context17.prev = _context17.next) {
        case 0:
          _context17.next = 2;
          return (0, _reduxSaga.takeEvery)(actions.NOTE_HIGHLIGHT, noteHighlightHandler);

        case 2:
        case "end":
          return _context17.stop();
      }
    }
  }, _marked17);
}

function notesLoadSingleSaga() {
  return regeneratorRuntime.wrap(function notesLoadSingleSaga$(_context18) {
    while (1) {
      switch (_context18.prev = _context18.next) {
        case 0:
          _context18.next = 2;
          return (0, _reduxSaga.takeEvery)(actions.NOTES_LOAD_SINGLE, notesLoadSingleHandler);

        case 2:
        case "end":
          return _context18.stop();
      }
    }
  }, _marked18);
}

var sagas = [notesLoadSaga, notesReloadSaga, notesLoadNextPageSaga, noteSubmitSaga, noteFormSubmitSaga, noteRemoveSaga, noteTodoChangeSaga, noteHighlightSaga, notesLoadSingleSaga];
exports.sagas = sagas;

function editableNoteStateToProps() {
  return function (multiState, props) {
    var state = multiState.notes[props.namespace];
    var forms = state && state.forms || {};
    var pendingTodos = state && state.pendingTodos || {};
    var form = forms["edit-".concat(props.note.id)] || {};
    var pendingTodo = pendingTodos[props.note.id] || false;
    return {
      content: form.content || props.note.content,
      recipients: form.recipients || props.note.recipients,
      pending: form.state === "pending",
      pendingTodo: pendingTodo,
      collapsed: form.state === undefined || form.state === "hidden"
    };
  };
}

function editableNoteDispatchToProps() {
  return function (dispatch, props) {
    var formKey = "edit-".concat(props.note.id);
    return {
      onShowForm: function onShowForm() {
        return dispatch(openNoteForm(props.namespace, formKey, {}));
      },
      onHideForm: function onHideForm(_ref3) {
        var content = _ref3.content;
        return dispatch(hideNoteForm(props.namespace, formKey, content));
      },
      onSubmitForm: function onSubmitForm(_ref4) {
        var content = _ref4.content,
            recipients = _ref4.recipients,
            is_todo = _ref4.is_todo,
            labelIds = _ref4.labelIds;
        var data = {
          content: content,
          recipients: recipients,
          labelIds: labelIds,
          is_todo: is_todo,
          id: props.note.id
        };
        dispatch(submitNoteForm(props.namespace, formKey, data));
      },
      onRemove: function onRemove() {
        return dispatch(removeNote(props.namespace, props.note.id));
      },
      onTodoChange: function onTodoChange() {
        return dispatch(noteCompletionChange(props.namespace, props.note.id, !props.note.completed_at));
      }
    };
  };
}

var EditableNoteContainer = (0, _hoc.withSession)((0, _reactRedux.connect)(editableNoteStateToProps, editableNoteDispatchToProps)(EditableNote));

function replyDispatchToProps() {
  return function (dispatch, props) {
    var author = props.author,
        parentNote = props.parentNote;
    var formKey = "reply-".concat(parentNote.id);
    return {
      onHideForm: function onHideForm(_ref5) {
        var content = _ref5.content;
        return dispatch(hideNoteForm(props.namespace, formKey, content));
      },
      onShowForm: function onShowForm() {
        return dispatch(openNoteForm(props.namespace, formKey, {}));
      },
      onSubmitForm: function onSubmitForm(_ref6) {
        var content = _ref6.content,
            recipients = _ref6.recipients;
        var data = {
          content: content,
          recipients: recipients,
          in_reply_to_id: parentNote.id,
          parent_id: parentNote.parent_id,
          parent_type: parentNote.parent_type
        };
        dispatch(submitNoteForm(props.namespace, formKey, data, author));
      }
    };
  };
}

function replyStateToProps() {
  return function (multiState, props) {
    var state = multiState.notes[props.namespace];
    var forms = state && state.forms || {};
    var form = forms["reply-".concat(props.parentNote.id)] || {};
    return {
      content: form.content || "",
      recipients: form.recipients || [],
      pending: form.state === "pending",
      collapsed: form.state === undefined || form.state === "hidden"
    };
  };
}

var ReplyFormContainer = (0, _reactRedux.connect)(replyStateToProps, replyDispatchToProps)(ReplyForm);

function newNoteStateToProps() {
  return function (multiState, props) {
    var state = multiState.notes[props.namespace];
    var forms = state && state.forms || {};
    var form = forms["new-".concat(props.entity.id)] || {};
    var collapsed = !form.state || form.state === "hidden";
    return {
      autoFocus: !collapsed,
      content: form.content,
      recipients: form.recipients,
      pending: form.state === "pending",
      collapsed: collapsed
    };
  };
}

function newNoteDispatchToProps() {
  return function (dispatch, props) {
    var formKey = "new-".concat(props.entity.id);
    var author = props.author;
    return {
      onExpand: function onExpand() {
        return dispatch(openNoteForm(props.namespace, formKey, {}));
      },
      onClickOutside: function onClickOutside(_ref7) {
        var content = _ref7.content,
            collapsed = _ref7.collapsed;

        if (!collapsed) {
          dispatch(hideNoteForm(props.namespace, formKey, content));
        }
      },
      onSubmit: function onSubmit(_ref8) {
        var content = _ref8.content,
            entity = _ref8.entity,
            recipients = _ref8.recipients;
        var data = {
          content: content,
          recipients: recipients,
          parent_id: entity.id,
          parent_type: entity.type
        };
        dispatch(submitNoteForm(props.namespace, formKey, data, author));
      }
    };
  };
}

var NewNoteFormContainer = (0, _reactRedux.connect)(newNoteStateToProps, newNoteDispatchToProps)(NoteForm);

function _NotesListEmptyText(_ref9) {
  var onActionButtonClick = _ref9.onActionButtonClick,
      hide = _ref9.hide,
      intl = _ref9.intl;
  var formatMessage = intl.formatMessage;

  if (hide) {
    return null;
  }

  var message = /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
    children: formatMessage(messages["attribute-message-no-notes-found"])
  });
  var iconElement = /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
    value: "message"
  });
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_empty_text.default, {
    className: _style.default.empty,
    icon: iconElement,
    label: message,
    cover: false
  });
}

_NotesListEmptyText.propTypes = {
  onActionButtonClick: _propTypes.default.func,
  entity: _propTypes.default.shape({
    // eslint-disable-line
    id: _propTypes.default.string.isRequired,
    type: _propTypes.default.string.isRequired
  }).isRequired,
  hide: _propTypes.default.bool,
  intl: _reactIntl.intlShape.isRequired
};

function mapEmptyTextStateToProps(multiState, props) {
  var state = multiState.notes[props.namespace];
  var forms = state && state.forms || {};
  var form = forms["new-".concat(props.entity.id)] || {};
  var collapsed = !form.state || form.state === "hidden";
  return {
    hide: !collapsed
  };
}

function mapEmptyTextDispatchToProps(dispatch, props) {
  var formKey = "new-".concat(props.entity.id);
  return {
    onActionButtonClick: function onActionButtonClick() {
      return dispatch(openNoteForm(props.namespace, formKey, {}));
    }
  };
}

var NotesListEmptyText = (0, _reactRedux.connect)(mapEmptyTextStateToProps, mapEmptyTextDispatchToProps)((0, _safe_inject_intl.default)(_NotesListEmptyText));

var _EntityInformation = /*#__PURE__*/function (_Component) {
  _inherits(_EntityInformation, _Component);

  var _super = _createSuper(_EntityInformation);

  function _EntityInformation() {
    var _this;

    _classCallCheck(this, _EntityInformation);

    _this = _super.call(this);
    _this.handleClick = _this.handleClick.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(_EntityInformation, [{
    key: "handleClick",
    value: function handleClick() {
      var _this$props = this.props,
          data = _this$props.data,
          onClick = _this$props.onClick;
      var type = data.__entity_type__,
          id = data.id;

      if (onClick) {
        onClick([{
          id: id,
          type: type
        }]);
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          data = _this$props2.data,
          session = _this$props2.session,
          onClick = _this$props2.onClick,
          entityInformationTooltip = _this$props2.entityInformationTooltip,
          entity = _this$props2.entity;
      var thumbnailId;
      var thumbnailUrl;
      var link = [];
      var versionNumber = null;
      var type = data.__entity_type__;

      if (type === "ReviewSessionObject") {
        thumbnailId = data.asset_version.thumbnail_id;
        thumbnailUrl = data.asset_version.thumbnail_url && data.asset_version.thumbnail_url.value;
        versionNumber = (0, _string.getVersionFromEntityNameIfPossible)(data.version);
        var rsoName = versionNumber ? "".concat(data.name, " v").concat(versionNumber) : data.name;
        link = [{
          name: data.review_session.project.full_name,
          id: data.review_session.project_id
        }, {
          name: data.review_session.name,
          id: data.review_session_id
        }, {
          name: rsoName,
          id: data.id
        }];
      } else {
        thumbnailUrl = data.thumbnail_url && data.thumbnail_url.value;
        thumbnailId = data.thumbnail_id;
        link = data.link || [];
      }

      var url = thumbnailUrl;

      if (!url) {
        url = session.thumbnailUrl(thumbnailId, _constant.THUMBNAIL_SIZES.small);
      }

      var extraInformationClasses = (0, _classnames.default)(_style.default["extra-information"], onClick && _style.default.clickable, _defineProperty({}, _style.default["extra-information-inline"], !entityInformationTooltip));
      var reviewSessionName = data.review_session && data.review_session.name;
      var entityName = link[link.length - 1] && link[link.length - 1].name || "Unknown entity";

      if (entityInformationTooltip) {
        var tooltip = /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
          className: extraInformationClasses,
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_link.default, {
            color: "light",
            link: link,
            parent: true
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            className: _style.default["thumbnail-with-tooltip"],
            style: {
              backgroundImage: "url('".concat(url, "')")
            }
          })]
        });
        var entityInformationText = [];

        if (!versionNumber) {
          versionNumber = (0, _string.getVersionFromEntityNameIfPossible)(entityName);
        }

        if (type === "ReviewSessionObject") {
          if (entity.type === "ReviewSessionObject") {
            entityInformationText.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["top-entity-information-version-number"]), {}, {
              values: {
                versionNumber: versionNumber
              }
            })));
          } else if (entity.type === "ReviewSession") {
            entityInformationText.push(entityName);
          } else {
            entityInformationText.push("".concat(reviewSessionName, ", ").concat(entityName));
          }
        } else if (type === "AssetVersion") {
          if (["Asset", "AssetVersion"].includes(entity.type)) {
            entityInformationText.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["top-entity-information-version-number"]), {}, {
              values: {
                versionNumber: versionNumber
              }
            })));
          } else {
            entityInformationText.push(entityName);
          }
        } else {
          entityInformationText.push(entityName);
        }

        return /*#__PURE__*/(0, _jsxRuntime.jsxs)(SpanWithToolTip, {
          onClick: this.handleClick,
          theme: _tooltip_theme.default,
          tooltip: tooltip,
          tooltipDelay: ENTITY_INFORMATION_TOOLTIP_DELAY,
          tooltipPosition: "top",
          className: _style.default.onTooltip,
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["top-entity-information"])), " ", /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
            className: _style.default.tooltipSpan,
            children: entityInformationText
          })]
        });
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        onClick: this.handleClick,
        className: extraInformationClasses,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default["thumbnail-without-tooltip"],
          style: {
            backgroundImage: "url('".concat(url, "')")
          }
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_link.default, {
          link: link,
          parent: false
        })]
      });
    }
  }]);

  return _EntityInformation;
}(_react.Component);

_EntityInformation.propTypes = {
  data: _propTypes.default.shape({
    thumbnail_id: _propTypes.default.string,
    link: _propTypes.default.array
  }).isRequired,
  session: _propTypes.default.shape({
    thumbnailUrl: _propTypes.default.func.isRequired
  }).isRequired,
  onClick: _propTypes.default.func,
  entityInformationTooltip: _propTypes.default.bool,
  entity: _propTypes.default.shape({
    id: _propTypes.default.string,
    type: _propTypes.default.string
  })
};
var EntityInformation = (0, _hoc.withSession)(_EntityInformation); // eslint-disable-next-line

var NoteThread_ = /*#__PURE__*/function (_Component2) {
  _inherits(NoteThread_, _Component2);

  var _super2 = _createSuper(NoteThread_);

  function NoteThread_() {
    var _this2;

    _classCallCheck(this, NoteThread_);

    _this2 = _super2.call(this);
    _this2.handleRef = _this2.handleRef.bind(_assertThisInitialized(_this2));
    _this2.scrollNoteIntoViewIfNecessary = _this2.scrollNoteIntoViewIfNecessary.bind(_assertThisInitialized(_this2));
    return _this2;
  }

  _createClass(NoteThread_, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.scrollNoteIntoViewIfNecessary(this.props);
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(props) {
      if (this.props.highlightNoteId !== props.highlightNoteId) {
        this.scrollNoteIntoViewIfNecessary(this.props);
      }
    }
  }, {
    key: "handleRef",
    value: function handleRef(node) {
      this.node = node;
    }
  }, {
    key: "scrollNoteIntoViewIfNecessary",
    value: function scrollNoteIntoViewIfNecessary(_ref10) {
      var note = _ref10.note,
          highlightNoteId = _ref10.highlightNoteId;
      var shouldComponentHighlight = highlightNoteId && note.id === highlightNoteId;

      if (shouldComponentHighlight && this.node) {
        (0, _scrollIntoViewIfNeeded.default)(this.node, {
          block: "center",
          inline: "center",
          skipOverflowHiddenElements: "false",
          behavior: "smooth",
          scrollMode: "if-needed"
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props3 = this.props,
          note = _this$props3.note,
          onAttachmentClick = _this$props3.onAttachmentClick,
          onFrameClick = _this$props3.onFrameClick,
          author = _this$props3.author,
          entity = _this$props3.entity,
          onEntityInformationClick = _this$props3.onEntityInformationClick,
          highlightNoteId = _this$props3.highlightNoteId,
          disableReply = _this$props3.disableReply,
          className = _this$props3.className,
          entityInformationTooltip = _this$props3.entityInformationTooltip,
          colors = _this$props3.colors,
          namespace = _this$props3.namespace,
          lastItem = _this$props3.lastItem,
          canMention = _this$props3.canMention,
          showNoteLabelSelector = _this$props3.showNoteLabelSelector,
          showCompletableNotes = _this$props3.showCompletableNotes; // Add note components for each reply.

      var replies = (note.replies || []).map(function (reply) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(EditableNoteContainer, {
          className: _style.default.reply,
          entityInformationTooltip: entityInformationTooltip,
          onAttachmentClick: onAttachmentClick,
          onFrameClick: onFrameClick,
          note: reply,
          colors: colors,
          author: author,
          namespace: namespace,
          canMention: canMention
        }, reply.id);
      });
      var entityInformation = null;

      if (note.parent_id !== entity.id && note.extraInformation) {
        entityInformation = /*#__PURE__*/(0, _jsxRuntime.jsx)(EntityInformation, {
          data: note.extraInformation,
          onClick: onEntityInformationClick,
          entityInformationTooltip: entityInformationTooltip,
          entity: entity
        });
      }

      var parentNoteClass = (0, _classnames.default)(_style.default["parent-note-item"], _defineProperty({}, _style.default["highlight-note"], highlightNoteId && highlightNoteId === note.id));
      var authorColor = note.author ? (0, _color.pickReviewColor)(note.author, colors) : null;
      return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: className,
        ref: this.handleRef,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
          className: parentNoteClass,
          style: {
            borderColor: authorColor
          },
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(EditableNoteContainer, {
            entityInformationTooltip: entityInformationTooltip,
            entityInformation: entityInformation,
            onAttachmentClick: onAttachmentClick,
            onFrameClick: onFrameClick,
            note: note,
            author: author,
            onRef: this.onNoteRef,
            colors: colors,
            namespace: namespace,
            canMention: canMention,
            showNoteLabelSelector: showNoteLabelSelector,
            showCompletableNotes: showCompletableNotes
          }), replies.length ? /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            className: _style.default.replies,
            children: replies
          }) : null, disableReply ? null : /*#__PURE__*/(0, _jsxRuntime.jsx)(ReplyFormContainer, {
            namespace: namespace,
            parentNote: note,
            author: author,
            canMention: canMention
          }, "reply-note-form-".concat(note.id))]
        }), lastItem ? null : /*#__PURE__*/(0, _jsxRuntime.jsx)("hr", {
          className: _style.default.noteSeparator
        })]
      });
    }
  }]);

  return NoteThread_;
}(_react.Component);

NoteThread_.propTypes = {
  className: _propTypes.default.string,
  note: _propTypes.default.object.isRequired,
  // eslint-disable-line
  entity: _propTypes.default.shape({
    id: _propTypes.default.string.isRequired,
    type: _propTypes.default.string.isRequired
  }).isRequired,
  entitySelector: _propTypes.default.array,
  // eslint-disable-line
  author: _propTypes.default.shape({
    id: _propTypes.default.string.isRequired,
    type: _propTypes.default.string.isRequired
  }).isRequired,
  onAttachmentClick: _propTypes.default.func,
  onFrameClick: _propTypes.default.func,
  onEntityInformationClick: _propTypes.default.func,
  disableReply: _propTypes.default.bool,
  highlightNoteId: _propTypes.default.bool,
  entityInformationTooltip: _propTypes.default.bool,
  colors: _propTypes.default.arrayOf(_propTypes.default.string),
  namespace: _propTypes.default.string,
  lastItem: _propTypes.default.bool,
  canMention: _propTypes.default.bool,
  showNoteLabelSelector: _propTypes.default.bool,
  showCompletableNotes: _propTypes.default.bool
};
var NoteThread = (0, _reactRedux.connect)(function (multiState, props) {
  return {
    highlightNoteId: multiState.notes[props.namespace].highlight
  };
})(NoteThread_);
/** List an array of note *items* with support for editing and creating notes.
 *
 * The *entity* is the currently loaded entity, e.g. a task or a version. This
 * is used as parent when creating new notes.
 *
 * The *user* object is the active user and is used as author for any new notes.
 */
// eslint-disable-next-line

var NotesList = /*#__PURE__*/function (_Component3) {
  _inherits(NotesList, _Component3);

  var _super3 = _createSuper(NotesList);

  function NotesList() {
    _classCallCheck(this, NotesList);

    return _super3.apply(this, arguments);
  }

  _createClass(NotesList, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props4 = this.props,
          entity = _this$props4.entity,
          filters = _this$props4.filters,
          sort = _this$props4.sort,
          onLoad = _this$props4.onLoad,
          onRef = _this$props4.onRef,
          pageSize = _this$props4.pageSize,
          includeAllVersions = _this$props4.includeAllVersions,
          noteSelect = _this$props4.noteSelect;
      onLoad(entity, filters, sort, pageSize, includeAllVersions, noteSelect(this.props.session));

      if (onRef) {
        onRef(this);
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(_ref11) {
      var entity = _ref11.entity,
          sort = _ref11.sort,
          pageSize = _ref11.pageSize,
          filters = _ref11.filters,
          includeAllVersions = _ref11.includeAllVersions,
          noteSelect = _ref11.noteSelect;

      if (entity.id !== this.props.entity.id || sort !== this.props.sort || filters !== this.props.filters || includeAllVersions !== this.props.includeAllVersions) {
        this.props.onLoad(entity, filters, sort, pageSize, includeAllVersions, noteSelect(this.props.session));
      }
    }
  }, {
    key: "reload",
    value: function reload() {
      var _this$props5 = this.props,
          entity = _this$props5.entity,
          sort = _this$props5.sort,
          pageSize = _this$props5.pageSize,
          filters = _this$props5.filters,
          includeAllVersions = _this$props5.includeAllVersions,
          noteSelect = _this$props5.noteSelect;
      this.props.onLoad(entity, filters, sort, pageSize, includeAllVersions, noteSelect(this.props.session));
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props6 = this.props,
          items = _this$props6.items,
          entity = _this$props6.entity,
          user = _this$props6.user,
          loading = _this$props6.loading,
          nextOffset = _this$props6.nextOffset,
          onFetchMore = _this$props6.onFetchMore,
          onAttachmentClick = _this$props6.onAttachmentClick,
          disableReply = _this$props6.disableReply,
          disableCreate = _this$props6.disableCreate,
          entitySelector = _this$props6.entitySelector,
          onEntityInformationClick = _this$props6.onEntityInformationClick,
          onFrameClick = _this$props6.onFrameClick,
          pageSize = _this$props6.pageSize,
          itemClassName = _this$props6.itemClassName,
          sort = _this$props6.sort,
          colors = _this$props6.colors,
          entityInformationTooltip = _this$props6.entityInformationTooltip,
          namespace = _this$props6.namespace,
          filters = _this$props6.filters,
          noteSelect = _this$props6.noteSelect,
          includeAllVersions = _this$props6.includeAllVersions,
          canMention = _this$props6.canMention,
          showNoteLabelSelector = _this$props6.showNoteLabelSelector,
          showCompletableNotes = _this$props6.showCompletableNotes;
      logger.debug("Rendering notes", entity, user);
      var notes = [];
      items.forEach(function (note, index) {
        // Add primary note component.
        notes.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(NoteThread, {
          onAttachmentClick: onAttachmentClick,
          onEntityInformationClick: onEntityInformationClick,
          onFrameClick: onFrameClick,
          note: note,
          entity: entity,
          author: user,
          disableReply: disableReply,
          className: itemClassName,
          entityInformationTooltip: entityInformationTooltip,
          colors: colors,
          namespace: namespace,
          includeAllVersions: includeAllVersions,
          lastItem: index === items.length - 1,
          canMention: canMention,
          showCompletableNotes: showCompletableNotes,
          showNoteLabelSelector: showNoteLabelSelector
        }, note.id));
      });
      var content = [];

      if (!disableCreate) {
        content.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(NewNoteFormContainer, {
          className: _style.default["new-note-form"],
          entity: entity,
          entitySelector: entitySelector,
          author: user,
          namespace: namespace,
          canMention: canMention
        }, "new-note-form-".concat(entity.id)));
      }

      if (notes.length) {
        content.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default["note-list-inner"],
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactFlipMove.default, {
            duration: _constant.TRANSITION_DURATION,
            leaveAnimation: "fade",
            children: notes
          })
        }, "note-list-inner"));
      }

      if (loading) {
        // Add loading indicator.
        content.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {}, "loading-skeleton"));
      }

      if (!loading && !items.length) {
        content.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(NotesListEmptyText, {
          entity: entity,
          disableCallToAction: disableCreate
        }, "notes-list-empty-state"));
      }

      if (loading === false && nextOffset !== null && items.length) {
        // Only add way point if not loading and there are more items to load.
        notes.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactWaypoint.Waypoint, {
          onEnter: function onEnter() {
            return onFetchMore(entity, filters, sort, pageSize, nextOffset, noteSelect);
          },
          bottomOffset: -100
        }, "notes-list-waypoint"));
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _style.default["note-list"],
        children: content
      });
    }
  }]);

  return NotesList;
}(_react.Component);

exports.NotesList = NotesList;
NotesList.propTypes = {
  items: _propTypes.default.array.isRequired,
  // eslint-disable-line
  entity: _propTypes.default.shape({
    id: _propTypes.default.string.isRequired,
    type: _propTypes.default.string.isRequired
  }).isRequired,
  entitySelector: _propTypes.default.array,
  // eslint-disable-line
  user: _propTypes.default.shape({
    id: _propTypes.default.string.isRequired,
    type: _propTypes.default.string.isRequired
  }).isRequired,
  loading: _propTypes.default.bool,
  nextOffset: _propTypes.default.shape({
    date: _propTypes.default.object.isRequired,
    // eslint-disable-line
    id: _propTypes.default.string.isRequired
  }),
  onFetchMore: _propTypes.default.func,
  session: _propTypes.default.object,
  onLoad: _propTypes.default.func,
  onAttachmentClick: _propTypes.default.func,
  onFrameClick: _propTypes.default.func,
  onEntityInformationClick: _propTypes.default.func,
  disableReply: _propTypes.default.bool,
  disableCreate: _propTypes.default.bool,
  canMention: _propTypes.default.bool,
  showNoteLabelSelector: _propTypes.default.bool,
  showCompletableNotes: _propTypes.default.bool,
  pageSize: _propTypes.default.number.isRequired,
  itemClassName: _propTypes.default.string,
  namespace: _propTypes.default.string,
  sort: _propTypes.default.string,
  filters: _propTypes.default.string,
  onRef: _propTypes.default.func,
  noteSelect: _propTypes.default.func,
  entityInformationTooltip: _propTypes.default.bool,
  colors: _propTypes.default.arrayOf(_propTypes.default.string),
  includeAllVersions: _propTypes.default.bool
};
NotesList.defaultProps = {
  disableReply: false,
  disableCreate: false,
  canMention: false,
  showNoteLabelSelector: false,
  showCompletableNotes: false,
  noteSelect: function noteSelect(session) {
    var projections = [].concat(DEFAULT_SELECT);
    var userSchema = session.schemas.find(function (schema) {
      return schema.id === "User";
    });

    if (userSchema.properties.email) {
      // Backend support for User.email in collaborator API is added in
      // 4.2.
      projections.push("author.email");
    }

    if (session.schemas.some(function (schema) {
      return schema.id === "NoteLabel";
    })) {
      // Backend support for NoteLabel in collaborator API is added in
      // 4.3.X
      projections.push("note_label_links.label.name", "note_label_links.label.color");
    } else {
      projections.push("category.name", "category.color");
    } // Add same attributes but with replies prefix to load the same data on
    // replies.


    projections.push.apply(projections, _toConsumableArray(projections.map(function (attribute) {
      return "replies.".concat(attribute);
    })));
    return "select ".concat(projections.join(", "), " from Note");
  },
  entitySelector: null,
  highlight: null,
  sort: "date",
  pageSize: 10,
  namespace: DEFAULT_NAMESPACE,
  includeAllVersions: false
};

var mapStateToProps = function mapStateToProps(multiState, props) {
  var state = multiState.notes[props.namespace || DEFAULT_NAMESPACE];
  var items = state && state.items || [];
  var loading = state && state.loading || false;
  var nextOffset = state && state.nextOffset;
  return {
    items: items,
    nextOffset: nextOffset,
    loading: loading
  };
};

exports.mapStateToProps = mapStateToProps;

var mapDispatchToProps = function mapDispatchToProps(dispatch, ownProps) {
  return {
    onFetchMore: function onFetchMore(entity, filters, sort, limit, nextOffset) {
      dispatch(notesLoadNextPage(ownProps.namespace || DEFAULT_NAMESPACE, entity.id, entity.type, filters, sort, limit, nextOffset));
    },
    onLoad: function onLoad(entity, filters, sort, limit, includeAllVersions, noteSelect) {
      dispatch(notesLoad(ownProps.namespace || DEFAULT_NAMESPACE, entity.id, entity.type, filters, sort, limit, includeAllVersions, noteSelect));
    }
  };
};

exports.mapDispatchToProps = mapDispatchToProps;

var _default = (0, _hoc.withSession)((0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(NotesList));

exports.default = _default;