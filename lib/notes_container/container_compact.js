"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.mapDispatchToProps = exports.NotesListCompact = void 0;

require("core-js/modules/es6.array.sort.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _loglevel = _interopRequireDefault(require("loglevel"));

var _react = require("react");

var _reactIntl = require("react-intl");

var _reactRedux = require("react-redux");

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _reactFlipMove = _interopRequireDefault(require("react-flip-move"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _empty_text = _interopRequireDefault(require("../empty_text"));

var _constant = require("../util/constant");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _skeleton = _interopRequireDefault(require("./skeleton"));

var _style = _interopRequireDefault(require("./style.scss"));

var _note_compact = _interopRequireDefault(require("../note/note_compact"));

var _container = require("./container");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var DEFAULT_NAMESPACE = "default";
var COMPACT_SELECT = ["id", "content", "author.first_name", "author.last_name", "parent_id", "parent_type", "date", "metadata.key", "metadata.value", "completed_at", "is_todo", "thread_activity"];

var logger = _loglevel.default.getLogger("saga:note");

var messages = (0, _reactIntl.defineMessages)({
  "attribute-message-no-notes-found": {
    "id": "ftrack-spark-components-notes-container-attribute-message-no-notes-found",
    "defaultMessage": "There is no feedback yet"
  }
});

function _NotesListEmptyText(_ref) {
  var hide = _ref.hide,
      intl = _ref.intl;
  var formatMessage = intl.formatMessage;

  if (hide) {
    return null;
  }

  var message = /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
    children: formatMessage(messages["attribute-message-no-notes-found"])
  });
  var iconElement = /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
    value: "message"
  });
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_empty_text.default, {
    className: (0, _classnames.default)(_style.default.empty),
    icon: iconElement,
    label: message,
    cover: false
  });
}

_NotesListEmptyText.propTypes = {
  entity: _propTypes.default.shape({
    // eslint-disable-line
    id: _propTypes.default.string.isRequired,
    type: _propTypes.default.string.isRequired
  }).isRequired,
  hide: _propTypes.default.bool,
  intl: _reactIntl.intlShape.isRequired
};

function mapEmptyTextStateToProps(multiState, props) {
  var state = multiState.notes[props.namespace];
  var forms = state && state.forms || {};
  var form = forms["new-".concat(props.entity.id)] || {};
  var collapsed = !form.state || form.state === "hidden";
  return {
    hide: !collapsed
  };
}

var NotesListEmptyText = (0, _reactRedux.connect)(mapEmptyTextStateToProps)((0, _safe_inject_intl.default)(_NotesListEmptyText)); // eslint-disable-next-line

var NotesListCompact = /*#__PURE__*/function (_Component) {
  _inherits(NotesListCompact, _Component);

  var _super = _createSuper(NotesListCompact);

  function NotesListCompact() {
    _classCallCheck(this, NotesListCompact);

    return _super.apply(this, arguments);
  }

  _createClass(NotesListCompact, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props = this.props,
          entity = _this$props.entity,
          filters = _this$props.filters,
          sort = _this$props.sort,
          onLoad = _this$props.onLoad,
          onRef = _this$props.onRef,
          pageSize = _this$props.pageSize,
          noteSelect = _this$props.noteSelect;
      onLoad(entity, filters, sort, pageSize, noteSelect());

      if (onRef) {
        onRef(this);
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(_ref2) {
      var entity = _ref2.entity,
          sort = _ref2.sort,
          pageSize = _ref2.pageSize,
          filters = _ref2.filters,
          noteSelect = _ref2.noteSelect;

      if (entity.id !== this.props.entity.id || sort !== this.props.sort || filters !== this.props.filters) {
        this.props.onLoad(entity, filters, sort, pageSize, noteSelect());
      }
    }
  }, {
    key: "reload",
    value: function reload() {
      var _this$props2 = this.props,
          entity = _this$props2.entity,
          sort = _this$props2.sort,
          pageSize = _this$props2.pageSize,
          filters = _this$props2.filters,
          noteSelect = _this$props2.noteSelect;
      this.props.onLoad(entity, filters, sort, pageSize, noteSelect());
    }
  }, {
    key: "render",
    value: function render() {
      var _this = this;

      var _this$props3 = this.props,
          items = _this$props3.items,
          entity = _this$props3.entity,
          user = _this$props3.user,
          loading = _this$props3.loading,
          disableCreate = _this$props3.disableCreate;
      logger.debug("Rendering notes", entity, user);
      var notes = [];
      items.forEach(function (note) {
        // Add primary note component.
        notes.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_note_compact.default, {
          completedAt: note.completed_at,
          onTodoChange: function onTodoChange() {
            return _this.props.onTodoChange(note.id, note.completed_at);
          },
          data: _objectSpread(_objectSpread({}, note), user)
        }, note.id));
      });
      var content = [];

      if (notes.length) {
        content.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactFlipMove.default, {
            duration: _constant.TRANSITION_DURATION,
            leaveAnimation: "fade",
            children: notes
          })
        }, "note-list-inner"));
      }

      if (loading) {
        // Add loading indicator.
        content.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {}, "loading-skeleton"));
      }

      if (!loading && !items.length) {
        content.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(NotesListEmptyText, {
          entity: entity,
          disableCallToAction: disableCreate
        }, "notes-list-empty-state"));
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _style.default["note-list-compact"],
        children: content
      });
    }
  }]);

  return NotesListCompact;
}(_react.Component);

exports.NotesListCompact = NotesListCompact;
NotesListCompact.propTypes = {
  items: _propTypes.default.array.isRequired,
  // eslint-disable-line
  entity: _propTypes.default.shape({
    id: _propTypes.default.string.isRequired,
    type: _propTypes.default.string.isRequired
  }).isRequired,
  entitySelector: _propTypes.default.array,
  // eslint-disable-line
  user: _propTypes.default.shape({
    id: _propTypes.default.string.isRequired,
    type: _propTypes.default.string
  }).isRequired,
  loading: _propTypes.default.bool,
  nextOffset: _propTypes.default.shape({
    date: _propTypes.default.object.isRequired,
    // eslint-disable-line
    id: _propTypes.default.string.isRequired
  }),
  onLoad: _propTypes.default.func,
  disableCreate: _propTypes.default.bool,
  includeReplies: _propTypes.default.bool,
  pageSize: _propTypes.default.number.isRequired,
  namespace: _propTypes.default.string,
  sort: _propTypes.default.string,
  filters: _propTypes.default.string,
  onRef: _propTypes.default.func,
  noteSelect: _propTypes.default.func,
  onTodoChange: _propTypes.default.func.isRequired,
  intl: _reactIntl.intlShape.isRequired
};
NotesListCompact.defaultProps = {
  disableCreate: false,
  includeReplies: false,
  entitySelector: null,
  highlight: null,
  sort: "date",
  noteSelect: function noteSelect() {
    var projections = [].concat(COMPACT_SELECT);
    return "select ".concat(projections.join(", "), " from Note");
  },
  pageSize: 5,
  namespace: DEFAULT_NAMESPACE
};

var mapDispatchToProps = function mapDispatchToProps(dispatch, ownProps) {
  return {
    onLoad: function onLoad(entity, filters, sort, limit, noteSelect) {
      dispatch((0, _container.notesLoad)(ownProps.namespace || DEFAULT_NAMESPACE, entity.id, entity.type, filters, sort, limit, false, noteSelect));
    },
    onTodoChange: function onTodoChange(noteId, completedAt) {
      return dispatch((0, _container.noteCompletionChange)(ownProps.namespace, noteId, !completedAt));
    }
  };
};

exports.mapDispatchToProps = mapDispatchToProps;

var _default = (0, _reactRedux.connect)(_container.mapStateToProps, mapDispatchToProps)((0, _safe_inject_intl.default)(NotesListCompact));

exports.default = _default;