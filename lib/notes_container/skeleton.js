"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _skeleton = _interopRequireDefault(require("../skeleton"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NoteSkeleton = function NoteSkeleton() {
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: _style.default["loading-skeleton"],
    children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default["skeleton-top-row"],
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {
        className: _style.default.avatar
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: _style.default["skeleton-header-text"],
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {
          className: _style.default["skeleton-username"]
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {
          className: _style.default["skeleton-date"]
        })]
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {
      className: _style.default["skeleton-content"]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {
      className: _style.default["skeleton-content"]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {
      className: _style.default["skeleton-content"]
    })]
  });
};

var _default = NoteSkeleton;
exports.default = _default;