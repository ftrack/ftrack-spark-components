"use strict";

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.regexp.split.js");

var _constant = require("../util/constant");

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function getQueryFilter(queryString, attributes, baseFilter) {
  var words = queryString.split(" ").map(function (word) {
    return word.trim();
  }).filter(function (word) {
    return word;
  });
  var joinedFilters = [];

  var _iterator = _createForOfIteratorHelper(words),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var word = _step.value;
      var attributeFilters = [];

      var _iterator2 = _createForOfIteratorHelper(attributes),
          _step2;

      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var attribute = _step2.value;

          if (attribute === "id" && _constant.UUID_REGEX.test(word)) {
            attributeFilters.push("id is \"".concat(word, "\""));
          } else if (attribute !== "id") {
            attributeFilters.push("".concat(attribute, " like \"%").concat(word, "%\""));
          }
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }

      joinedFilters.push("(".concat(attributeFilters.join(" or "), ")"));
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  if (joinedFilters.length && baseFilter) {
    return "(".concat(baseFilter, ") and (").concat(joinedFilters.join(" and "), ")");
  } else if (joinedFilters.length) {
    return joinedFilters.join(" and ");
  } else if (baseFilter) {
    return baseFilter;
  }

  return "";
}

var _default = getQueryFilter;
exports.default = _default;