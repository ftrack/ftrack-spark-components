"use strict";

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ensureSettings = ensureSettings;
exports.getSetting = getSetting;

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.promise.js");

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

// :copyright: Copyright (c) 2017 ftrack
var allSettings = null;
var settingsLoading = null;
/**
 * Return promise resolved with all settings.
 * Will cache settings indefinitely.
 */

function ensureSettings(session) {
  if (allSettings) {
    return Promise.resolve(allSettings);
  } else if (!settingsLoading) {
    settingsLoading = session.query("select name, group, value from Setting").then(function (response) {
      allSettings = response.data;
      return allSettings;
    });
  }

  return settingsLoading;
}
/** Return promise resolved with setting value or *defaultValue*. */


function getSetting(session, group, name) {
  var defaultValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
  return ensureSettings(session).then(function (settings) {
    var _iterator = _createForOfIteratorHelper(settings),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var setting = _step.value;

        if (setting.name === name && setting.group === group) {
          return setting.value;
        }
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }

    return defaultValue;
  });
}