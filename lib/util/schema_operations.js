"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isTypedContextSubclass = isTypedContextSubclass;
exports.getObjectTypeSchemaId = getObjectTypeSchemaId;
exports.default = void 0;

require("core-js/modules/es6.array.find.js");

// :copyright: Copyright (c) 2019 ftrack
function isTypedContextSubclass(model, schemas) {
  var schema = schemas.find(function (candidate) {
    return candidate.id === model;
  });

  if (schema && schema.alias_for && schema.alias_for.id === "Task") {
    return true;
  }

  return false;
}
/** Get schema ID for *objectTypeId* */


function getObjectTypeSchemaId(schemas, objectTypeId) {
  var schema = schemas.find(function (candidate) {
    var _candidate$alias_for, _candidate$alias_for2, _candidate$alias_for3;

    return ((_candidate$alias_for = candidate.alias_for) === null || _candidate$alias_for === void 0 ? void 0 : _candidate$alias_for.id) === "Task" && ((_candidate$alias_for2 = candidate.alias_for) === null || _candidate$alias_for2 === void 0 ? void 0 : (_candidate$alias_for3 = _candidate$alias_for2.classifiers) === null || _candidate$alias_for3 === void 0 ? void 0 : _candidate$alias_for3.object_typeid) === objectTypeId;
  });

  if (!schema) {
    throw new Error("Failed to get schema id for object type ".concat(objectTypeId));
  }

  return schema.id;
}

var _default = isTypedContextSubclass;
exports.default = _default;