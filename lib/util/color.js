"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.hexToRgb = hexToRgb;
exports.getLuminance = getLuminance;
exports.getForegroundColor = getForegroundColor;
exports.pickColorFromString = pickColorFromString;
exports.pickReviewColor = pickReviewColor;

require("core-js/modules/es6.regexp.constructor.js");

require("core-js/modules/es6.regexp.match.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.number.constructor.js");

require("core-js/modules/es6.regexp.split.js");

// :copyright: Copyright (c) 2018 ftrack

/** Return array of 8-bit RGB values for #NNN or #NNNNNN hex color. */
function hexToRgb(hexColor) {
  var color = hexColor;

  if (color.indexOf("#") === 0) {
    color = color.substr(1);
  } // Split color to channels and expand 3-digit hex colors to 6-digit.


  var cssHexColorRe = new RegExp(".{1,".concat(color.length / 3, "}"), "g");
  var channels = color.match(cssHexColorRe);

  if (channels && channels[0].length === 1) {
    channels = channels.map(function (channel) {
      return channel + channel;
    });
  } // Convert HEX channels to decimal


  return channels.map(function (channel) {
    return parseInt(channel, 16);
  });
}
/**
 * Get luminance of *color* in the range [0, 1]
 *
 * *color* should be either an array of 8-bit decimal rgb values or a hex string.
 *
 * Formula from: https://www.w3.org/TR/2008/REC-WCAG20-20081211/#relativeluminancedef
 */


function getLuminance(color) {
  var rgbColor = color;

  if (!Array.isArray(rgbColor)) {
    rgbColor = hexToRgb(color);
  } // Convert 8-bit RGB color to relative luminance


  var rgb = rgbColor.map(function (channel) {
    var c = channel / 255;
    return c <= 0.03928 ? c / 12.92 : Math.pow((c + 0.055) / 1.055, 2.4);
  });
  return Number((0.2126 * rgb[0] + 0.7152 * rgb[1] + 0.0722 * rgb[2]).toFixed(3)); // clamp to 3 digits
}
/** Return light or dark text color for use on *backgroundColor*. */


function getForegroundColor(backgroundColor) {
  var lightColor = "white";
  var darkColor = "black";
  var luminanceLimit = 0.6;

  try {
    var luminance = getLuminance(backgroundColor);

    if (luminance < luminanceLimit) {
      return lightColor;
    }
  } catch (err) {// Ignore errors and return default
  }

  return darkColor;
}
/** Return a HEX color based on *text*.
 *
 * The result of this function is meant to produce stable colors based on
 * *text* and has an equivalent in Python. These two implementations
 * are meant to produce identical results. This is not the case for double
 * byte strings and thus this implementation is recommended for
 * email addresses and other strings that cannot contain such characters.
 */


function pickColorFromString() {
  var text = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var colors = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var value = text.split("").reduce(function (accumulator, character, index) {
    return accumulator + character.charCodeAt(0) + index;
  }, 0);
  return colors[value % colors.length];
}

function pickReviewColor(syncUser) {
  var colors = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

  if (!syncUser) {
    return null;
  }

  var name = syncUser.name,
      email = syncUser.email;
  return pickColorFromString(email || name || "", colors);
}