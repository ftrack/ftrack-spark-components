"use strict";

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getEntityThumbnail = getEntityThumbnail;
exports.sortMediaComponent = sortMediaComponent;
exports.getReviewMeta = getReviewMeta;
exports.getMediaTypeForComponent = getMediaTypeForComponent;
exports.getMediaComponentsForComponents = getMediaComponentsForComponents;
exports.default = exports.ASSET_VERSION_MEDIA_ATTRIBUTES = void 0;

require("core-js/modules/es6.array.find.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.string.includes.js");

require("core-js/modules/es7.array.includes.js");

require("core-js/modules/es6.array.sort.js");

var _constant = require("./constant");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var REVIEW_META_KEY = "ftr_meta"; // Attributes to load for media components on AssetVersion's.

var ASSET_VERSION_MEDIA_ATTRIBUTES = ["components.component_locations.location_id", "components.component_locations.url", "components.metadata", "components.name", "components.file_type", "thumbnail_id", "thumbnail_url"];
exports.ASSET_VERSION_MEDIA_ATTRIBUTES = ASSET_VERSION_MEDIA_ATTRIBUTES;

function getEntityThumbnail(_ref) {
  var id = _ref.thumbnail_id,
      url = _ref.thumbnail_url;
  return id && url ? url.value : null;
}
/** Sort function for prioritizing media components. */


function sortMediaComponent(a, b) {
  // Sort based on if media is playable.
  if (a.playable && !b.playable) {
    return -1;
  } else if (b.playable && !a.playable) {
    return 1;
  }

  if (!b.playable && !a.playable && b.encoding !== a.encoding) {
    // One of them is currently encoding.
    if (a.encoding) {
      return -1;
    }

    if (b.encoding) {
      return 1;
    }
  }

  var sortOrder = [].concat(_toConsumableArray(_constant.MEDIA_SORT_ORDER), ["unknown"]); // Sort based on media sort order.

  var typeSortOrderA = sortOrder.indexOf(a.type);
  var typeSortOrderB = sortOrder.indexOf(b.type);

  if (typeSortOrderA !== typeSortOrderB) {
    return typeSortOrderA - typeSortOrderB;
  } // Sort based on resolution.


  var resolutionSortOrderA = (a.width || 1) * (a.height || 1);
  var resolutionSortOrderB = (b.width || 1) * (b.height || 1);

  if (resolutionSortOrderA !== resolutionSortOrderB) {
    return resolutionSortOrderB - resolutionSortOrderA;
  }

  return a.name.localeCompare(b.name);
}
/** Return { encoding, frameRate?, frameIn?, frameOut?, format? } for component. */


function getReviewMeta(component) {
  var meta = component.metadata.find(function (candidate) {
    return candidate.key === REVIEW_META_KEY;
  });

  try {
    if (meta && meta.value === "encoding") {
      return {
        encoding: true
      };
    } else if (meta) {
      // Pluck values of interest from review meta.
      var metaValue = JSON.parse(meta.value);
      return ["frameRate", "frameIn", "frameOut", "format", "height", "width"].reduce(function (accumulator, property) {
        if (metaValue[property]) {
          accumulator[property] = metaValue[property];
        }

        return accumulator;
      }, {
        encoding: false
      });
    }
  } catch (err) {// Empty
  } // Review meta does not exist or could not be extracted.


  if (component.name === "ftrackreview-image") {
    return {
      encoding: false
    };
  }

  return null;
}
/** Return first *componentLocations* matching *locationId* */


function findComponentLocation() {
  var componentLocations = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var locationId = arguments.length > 1 ? arguments[1] : undefined;
  return componentLocations.find(function (candidate) {
    return candidate.location_id === locationId;
  });
}

function getComponentLocation(component) {
  // Get first component location in review or server locations.
  var componentLocation = findComponentLocation(component.component_locations, _constant.REVIEW_LOCATION_ID);

  if (!componentLocation) {
    componentLocation = findComponentLocation(component.component_locations, _constant.SERVER_LOCATION_ID);
  }

  return componentLocation;
} // Valid file types or component names for each media type.


var MEDIA_TYPE_FILE_TYPES = {
  video: _constant.SUPPORTED_VIDEO_FILE_TYPES,
  pdf: _constant.SUPPORTED_PDF_FILE_TYPES,
  image: _constant.SUPPORTED_IMG_FILE_TYPES
};
/** Return one of one of *MEDIA_TYPE_FILE_TYPES* or unknown for *component*. */

function getMediaTypeForComponent(component) {
  if (_constant.LEGACY_MEDIA_COMPONENTS[component.name]) {
    return _constant.LEGACY_MEDIA_COMPONENTS[component.name];
  }

  var extension = component.file_type && component.file_type.substr(1, component.file_type.length);
  var type = Object.keys(MEDIA_TYPE_FILE_TYPES).find(function (candidate) {
    return MEDIA_TYPE_FILE_TYPES[candidate].includes(extension);
  });

  if (!type) {
    return "unknown";
  }

  return type;
}

var defaultMediaComponent = {
  id: null,
  name: "unknown",
  type: "unknown",
  frameRate: 1,
  frameIn: 0,
  frameOut: 0,
  encoding: false,
  playable: false,
  value: null
};
/**
 * Return media component information for *components*.
 *
 * Media components are marked as playable or not and will provide other
 * information.
 *
 * The result is an array sorted by Playable, Media type (Video, PDF, Image),
 * Resolution (width x height) and name.
 *
 */

function getMediaComponentsForComponents(components) {
  var mediaInfos = components.reduce(function (accumulator, component) {
    var reviewMeta = getReviewMeta(component);
    var type = getMediaTypeForComponent(component);
    var location = getComponentLocation(component);
    var mediaMaxSize = _constant.MEDIA_MAX_SIZE[type]; // Mark components as non-playable if they are not in a supported
    // review format, have a URL, have too high resolution, is encoding,
    // or does not have required metadata.

    var problems = [];

    if (!_constant.MEDIA_SORT_ORDER.includes(type)) {
      problems.push("type-not-supported");
    }

    var url = null;

    if (location) {
      url = location.url;
    } else {
      problems.push("no-location");
    }

    if (reviewMeta === null) {
      problems.push("no-ftr-meta");
    } else if (reviewMeta.format === "unknown") {
      problems.push("no-ftr-meta");
    } else {
      if (reviewMeta.encoding) {
        problems.push("encoding");
      }

      if (type === "video" && !reviewMeta.frameRate) {
        problems.push("no-frame-rate");
      }

      if (mediaMaxSize && reviewMeta.height && reviewMeta.width && Math.max(reviewMeta.height, reviewMeta.width) > mediaMaxSize) {
        problems.push("resolution-too-high");
      }
    }

    accumulator.push(_objectSpread(_objectSpread(_objectSpread(_objectSpread({}, defaultMediaComponent), reviewMeta || {}), url || {}), {}, {
      playable: problems.length === 0,
      playableProblems: problems,
      type: type,
      fileType: component.file_type,
      name: component.name,
      id: component.id
    }));
    return accumulator;
  }, []); // Sort media by priority.

  mediaInfos.sort(sortMediaComponent);
  return mediaInfos;
}
/**
 * Return media info for *component*.
 */


function getMediaComponent(components) {
  var selected = getMediaComponentsForComponents(components)[0];

  if (!components || !components.length) {
    return defaultMediaComponent;
  }

  return selected;
}

var _default = getMediaComponent;
exports.default = _default;