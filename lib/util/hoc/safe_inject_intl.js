"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = safeInjectIntl;

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _react = require("react");

var _reactIntl = require("react-intl");

var _invariant = _interopRequireDefault(require("invariant"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function getDisplayName(Component) {
  return Component.displayName || Component.name || "Component";
}

function safeInjectIntl(WrappedComponent) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var _options$intlPropName = options.intlPropName,
      intlPropName = _options$intlPropName === void 0 ? "intl" : _options$intlPropName,
      _options$withRef = options.withRef,
      withRef = _options$withRef === void 0 ? false : _options$withRef;

  var InjectIntl = /*#__PURE__*/function (_Component) {
    _inherits(InjectIntl, _Component);

    var _super = _createSuper(InjectIntl);

    function InjectIntl(props, context) {
      var _this;

      _classCallCheck(this, InjectIntl);

      _this = _super.call(this, props, context);
      _this.DeepWrappedComponent = null;
      return _this;
    }

    _createClass(InjectIntl, [{
      key: "getWrappedInstance",
      value: function getWrappedInstance() {
        (0, _invariant.default)(withRef, "[React Intl] To access the wrapped instance, " + "the `{withRef: true}` option must be set when calling: " + "`safeInjectIntl()`");
        return this.refs.wrappedInstance;
      }
    }, {
      key: "render",
      value: function render() {
        if (!this.context.intl) {
          // Only create the deep wrapped component once.
          if (!this.DeepWrappedComponent) {
            this.DeepWrappedComponent = safeInjectIntl(WrappedComponent);
          }

          var DeepWrappedComponent = this.DeepWrappedComponent;
          return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.IntlProvider, {
            locale: "en",
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(DeepWrappedComponent, _objectSpread(_objectSpread(_objectSpread({}, this.props), _defineProperty({}, intlPropName, this.context.intl)), {}, {
              ref: withRef ? "wrappedInstance" : null
            }))
          });
        }

        return /*#__PURE__*/(0, _jsxRuntime.jsx)(WrappedComponent, _objectSpread(_objectSpread(_objectSpread({}, this.props), _defineProperty({}, intlPropName, this.context.intl)), {}, {
          ref: withRef ? "wrappedInstance" : null
        }));
      }
    }]);

    return InjectIntl;
  }(_react.Component);

  InjectIntl.displayName = "InjectIntl(".concat(getDisplayName(WrappedComponent), ")");
  InjectIntl.contextTypes = {
    intl: _reactIntl.intlShape
  };
  InjectIntl.WrappedComponent = WrappedComponent;
  return InjectIntl;
}