"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.withInjectSagas = withInjectSagas;
exports.withSubspace = withSubspace;
Object.defineProperty(exports, "withSession", {
  enumerable: true,
  get: function get() {
    return _with_session.default;
  }
});
Object.defineProperty(exports, "SparkProvider", {
  enumerable: true,
  get: function get() {
    return _spark_provider.default;
  }
});
exports.SettingsProvider = exports.withSettings = exports.withAutoSize = exports.withIntl = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.object.assign.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactReduxDynamicReducer = require("react-redux-dynamic-reducer");

var _reduxSubspaceSaga = require("redux-subspace-saga");

var _reduxSagasDynamicInjector = _interopRequireDefault(require("redux-sagas-dynamic-injector"));

var _recompose = require("recompose");

var _safe_inject_intl = _interopRequireDefault(require("./safe_inject_intl"));

var _reactSizeme = _interopRequireDefault(require("react-sizeme"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _with_session = _interopRequireDefault(require("./with_session"));

var _spark_provider = _interopRequireDefault(require("./spark_provider"));

var _excluded = ["size"],
    _excluded2 = ["settings"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function withInjectSagas(sagas) {
  function wrapWithSagas(InnerComponentWithReducer) {
    var WrapperComponent = /*#__PURE__*/function (_Component) {
      _inherits(WrapperComponent, _Component);

      var _super = _createSuper(WrapperComponent);

      function WrapperComponent() {
        _classCallCheck(this, WrapperComponent);

        return _super.apply(this, arguments);
      }

      _createClass(WrapperComponent, [{
        key: "componentWillMount",
        value: function componentWillMount() {
          var store = this.context.store;
          var injectSagasAsync = (0, _reduxSagasDynamicInjector.default)(store);
          injectSagasAsync(sagas);
        }
      }, {
        key: "render",
        value: function render() {
          return /*#__PURE__*/(0, _jsxRuntime.jsx)(InnerComponentWithReducer, _objectSpread({}, this.props));
        }
      }]);

      return WrapperComponent;
    }(_react.Component);

    WrapperComponent.contextTypes = {
      store: _propTypes.default.object.isRequired
    };
    return WrapperComponent;
  }

  return wrapWithSagas;
}
/** Return a function that wraps a component in a redux subspace. */


function withSubspace(inputReducer, inputSagas, defaultSubspace) {
  return function wrapComponent(InnerComponent) {
    var InnerComponentWithReducer = (0, _reactReduxDynamicReducer.withReducer)(inputReducer, defaultSubspace)(InnerComponent);
    var InnerComponentWithSagas = withInjectSagas(inputSagas.map(function (saga) {
      return (0, _reduxSubspaceSaga.subspaced)(function (state) {
        return state[defaultSubspace];
      }, defaultSubspace)(saga);
    }), defaultSubspace)(InnerComponentWithReducer);

    InnerComponentWithSagas.createInstance = function (subspace) {
      return withInjectSagas(inputSagas.map(function (saga) {
        return (0, _reduxSubspaceSaga.subspaced)(function (state) {
          return state[subspace];
        }, subspace)(saga);
      }), subspace)(InnerComponentWithReducer.createInstance(subspace));
    };

    return InnerComponentWithSagas;
  };
}
/** withIntl HOC for use by compose. */


var withIntl = function withIntl(BaseComponent) {
  return (0, _safe_inject_intl.default)(BaseComponent);
};

exports.withIntl = withIntl;

var withAutoSize = function withAutoSize() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var sizeMeOptions = Object.assign({
    refreshRate: 1000,
    monitorHeight: true,
    monitorWidth: true
  }, options);
  return function (BaseComponent) {
    var AutoSize = (0, _reactSizeme.default)(sizeMeOptions)(function (_ref) {
      var size = _ref.size,
          props = _objectWithoutProperties(_ref, _excluded);

      return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _style.default["auto-size"],
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(BaseComponent, _objectSpread({
          width: size.width,
          height: size.height
        }, props))
      });
    });

    if (process.env.NODE_ENV !== "production") {
      return (0, _recompose.setDisplayName)((0, _recompose.wrapDisplayName)(BaseComponent, "withAutoSize"))(AutoSize);
    }

    return AutoSize;
  };
};

exports.withAutoSize = withAutoSize;

var withSettings = function withSettings(settings) {
  return (0, _recompose.compose)((0, _recompose.getContext)({
    settings: _propTypes.default.object
  }), (0, _recompose.mapProps)(function (_ref2) {
    var contextSettings = _ref2.settings,
        props = _objectWithoutProperties(_ref2, _excluded2);

    var settingsProps = {};
    Object.keys(settings).forEach(function (selected) {
      settingsProps[selected] = contextSettings && contextSettings[selected] || settings[selected];
    });
    return _objectSpread(_objectSpread({}, settingsProps), props);
  }));
};

exports.withSettings = withSettings;

var SettingsProvider = /*#__PURE__*/function (_Component2) {
  _inherits(SettingsProvider, _Component2);

  var _super2 = _createSuper(SettingsProvider);

  function SettingsProvider() {
    _classCallCheck(this, SettingsProvider);

    return _super2.apply(this, arguments);
  }

  _createClass(SettingsProvider, [{
    key: "getChildContext",
    value: function getChildContext() {
      return {
        settings: this.props.settings
      };
    }
  }, {
    key: "render",
    value: function render() {
      return this.props.children;
    }
  }]);

  return SettingsProvider;
}(_react.Component);

exports.SettingsProvider = SettingsProvider;
SettingsProvider.childContextTypes = {
  settings: _propTypes.default.object
};
SettingsProvider.propTypes = {
  settings: _propTypes.default.object,
  children: _propTypes.default.node
};