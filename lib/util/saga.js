"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.catchAllErrors = catchAllErrors;

require("regenerator-runtime/runtime.js");

require("core-js/modules/es6.array.slice.js");

var _effects = require("redux-saga/effects");

var _loglevel = _interopRequireDefault(require("loglevel"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2019 ftrack

/** Function for error handling for Sagas. */
// eslint-disable-next-line import/prefer-default-export
function catchAllErrors(saga) {
  var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      errorActionCreator = _ref.errorActionCreator,
      onError = _ref.onError;

  var wrapped = /*#__PURE__*/regeneratorRuntime.mark(function wrappedTryCatch() {
    var _args = arguments;
    return regeneratorRuntime.wrap(function wrappedTryCatch$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _effects.call.apply(void 0, [saga].concat(Array.prototype.slice.call(_args)));

          case 3:
            _context.next = 14;
            break;

          case 5:
            _context.prev = 5;
            _context.t0 = _context["catch"](0);

            _loglevel.default.error("Error in saga", saga, _context.t0);

            if (!onError) {
              _context.next = 11;
              break;
            }

            _context.next = 11;
            return onError(_context.t0);

          case 11:
            if (!errorActionCreator) {
              _context.next = 14;
              break;
            }

            _context.next = 14;
            return (0, _effects.put)(errorActionCreator({
              error: _context.t0
            }));

          case 14:
          case "end":
            return _context.stop();
        }
      }
    }, wrappedTryCatch, null, [[0, 5]]);
  });
  wrapped._original = saga;
  return wrapped;
}