"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.string.includes.js");

require("core-js/modules/es7.array.includes.js");

var _constant = require("./constant");

// :copyright: Copyright (c) 2018 ftrack

/** Return true if *component* is assumed to be consumable media. */
function isMedia() {
  var fileType = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var lowerCasedFileType = fileType.toLowerCase();
  return _constant.SUPPORTED_IMG_FILE_TYPES.includes(lowerCasedFileType);
}

var _default = isMedia;
exports.default = _default;