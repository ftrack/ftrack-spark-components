"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.durationToTimeParts = durationToTimeParts;
exports.framesToTimeParts = framesToTimeParts;
exports.formatNumber = formatNumber;
exports.formatTimecode = formatTimecode;
exports.formatStandard = formatStandard;
exports.default = exports.FORMAT_TIMECODE = exports.FORMAT_STANDARD = exports.FORMAT_FRAMES = void 0;

require("core-js/modules/es7.string.pad-start.js");

require("core-js/modules/es6.array.map.js");

// :copyright: Copyright (c) 2018 ftrack
var FORMAT_FRAMES = "frames";
exports.FORMAT_FRAMES = FORMAT_FRAMES;
var FORMAT_STANDARD = "standard";
exports.FORMAT_STANDARD = FORMAT_STANDARD;
var FORMAT_TIMECODE = "timecode";
/** Return time parts based on *duration* in seconds */

exports.FORMAT_TIMECODE = FORMAT_TIMECODE;

function durationToTimeParts() {
  var duration = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
  var totalSeconds = duration;
  var hours = Math.floor(totalSeconds / (60.0 * 60.0));
  totalSeconds %= 60.0 * 60.0;
  var minutes = Math.floor(totalSeconds / 60.0);
  totalSeconds %= 60.0;
  var seconds = Math.round(totalSeconds);
  return {
    hours: hours,
    minutes: minutes,
    seconds: seconds,
    frames: 0
  };
}
/** Return time parts based on *frame* and *frameRate* */


function framesToTimeParts() {
  var frame = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
  var frameRate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
  var duration = Math.floor(frame / frameRate);
  var result = durationToTimeParts(duration);
  result.frames = Math.floor(frame % frameRate);
  return result;
}
/** Return *number* as a string, zero-padded to *digits*. */


function formatNumber(number) {
  var digits = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;
  return number.toFixed(0).padStart(digits, "0");
}
/** Return formatted time on the format HH:MM:SS:FF from *timeParts*. */


function formatTimecode(_ref) {
  var hours = _ref.hours,
      minutes = _ref.minutes,
      seconds = _ref.seconds,
      frames = _ref.frames;
  return [hours, minutes, seconds, frames].map(function (num) {
    return formatNumber(num);
  }).join(":");
}
/** Return formatted time on the format [HH:]MM:SS from *timeParts*. */


function formatStandard(_ref2) {
  var hours = _ref2.hours,
      minutes = _ref2.minutes,
      seconds = _ref2.seconds;
  var formattedTime = "".concat(formatNumber(minutes), ":").concat(formatNumber(seconds));

  if (hours) {
    formattedTime = "".concat(formatNumber(hours), ":").concat(formattedTime);
  }

  return formattedTime;
}
/**
 * Return *currentFrame*, *totalFrames*  formatted according to *format*.
 *
 * format can be one of:
 *
 *  * FORMAT_FRAMES: FF
 *  * FORMAT_TIMECODE: HH:MM:SS:FF
 *  * FORMAT_STANDARD: [HH:]MM:SS
 */


function getFormattedTime(format, currentFrame, totalFrames, frameRate) {
  if (format === FORMAT_FRAMES) {
    var total = totalFrames.toFixed(0); // Offset current frame display so that it starts at 1.

    var current = formatNumber(currentFrame + 1, total.length);
    return [current, total];
  } else if (format === FORMAT_TIMECODE) {
    var _currentTime = framesToTimeParts(currentFrame, frameRate);

    var _totalTime = framesToTimeParts(totalFrames, frameRate);

    return [formatTimecode(_currentTime), formatTimecode(_totalTime)];
  } // format === FORMAT_STANDARD


  var currentTime = framesToTimeParts(currentFrame, frameRate);
  var totalTime = framesToTimeParts(totalFrames, frameRate);
  return [formatStandard(currentTime), formatStandard(totalTime)];
}

var _default = getFormattedTime;
exports.default = _default;