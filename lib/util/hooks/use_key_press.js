"use strict";

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = useKeyPress;

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.string.includes.js");

require("core-js/modules/es7.array.includes.js");

require("core-js/modules/es6.regexp.split.js");

require("core-js/modules/es7.object.values.js");

var _react = require("react");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

/**
 * Listens to specific key strokes for a DOM item.
 *
 * *ref* ref to node to listen to keystrokes
 *
 * *targetKey* array of keynames to keep track of, example ["ArrowUp", "Enter", "Control+Enter"]
 *
 * *callback* function called when key is released.
 */
function useKeyPress(ref, targetKeys, callback) {
  var _useState = (0, _react.useState)({}),
      _useState2 = _slicedToArray(_useState, 2),
      map = _useState2[0],
      setMap = _useState2[1];

  var upHandler = (0, _react.useCallback)(function (_ref) {
    var key = _ref.key;
    setMap(function (currMap) {
      return _objectSpread(_objectSpread({}, currMap), {}, _defineProperty({}, key, false));
    });
  }, // TODO: Feel free to fix this eslint error if you got time
  // eslint-disable-next-line react-hooks/exhaustive-deps
  [callback, setMap, targetKeys]);
  var downHandler = (0, _react.useCallback)(function (_ref2) {
    var key = _ref2.key;
    var possibleKeys = targetKeys.filter(function (k) {
      return k.split("+").includes(key);
    });
    var otherKeysPressed = Object.values(_objectSpread(_objectSpread({}, map), {}, _defineProperty({}, key, false))).includes(true);

    if (possibleKeys.length) {
      if (possibleKeys.length === 1 && possibleKeys[0].split("+").length === 1 && !otherKeysPressed) {
        callback(key);
      } else {
        var matchedCombination;
        var possibleNotCurrent = possibleKeys.filter(function (p) {
          return p !== key;
        });
        possibleNotCurrent.forEach(function (possibleKey) {
          var keyArr = possibleKey.split("+").filter(function (k) {
            return k !== key;
          });
          var combinationLocked = keyArr.reduce(function (prev, curr) {
            if (!prev) {
              return prev;
            }

            if (map[curr]) {
              return true;
            }

            return false;
          }, true);

          if (combinationLocked) {
            matchedCombination = possibleKey;
          }
        });

        if (matchedCombination) {
          callback(matchedCombination);
        } else if (!otherKeysPressed) {
          callback(key);
        }
      }
    }

    setMap(function (currMap) {
      return _objectSpread(_objectSpread({}, currMap), {}, _defineProperty({}, key, true));
    });
  }, // TODO: Feel free to fix this eslint error if you got time
  // eslint-disable-next-line react-hooks/exhaustive-deps
  [callback, setMap, targetKeys]);
  (0, _react.useEffect)(function () {
    if (ref.current) {
      var keepRef = ref.current;
      keepRef.addEventListener("keydown", downHandler);
      keepRef.addEventListener("keyup", upHandler);
      return function () {
        keepRef.removeEventListener("keydown", downHandler);
        keepRef.removeEventListener("keyup", upHandler);
      };
    }

    return function () {};
  }, [ref, upHandler, downHandler]);
}