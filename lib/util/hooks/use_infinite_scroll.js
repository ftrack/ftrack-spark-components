"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = useInfiniteScroll;

var _react = require("react");

// :copyright: Copyright (c) 2021 ftrack

/**
 * Used to keep track of when an object is scrolled into view.
 *
 * *scrollRef* ref to node to observe that will fire callback.
 *
 * *callback* function called when tracked node is scrolled into view.
 *
 */
function useInfiniteScroll(scrollRef, callback) {
  var scrollObserver = (0, _react.useCallback)(function (node) {
    // eslint-disable-next-line
    new IntersectionObserver(function (entries) {
      entries.forEach(function (en) {
        if (en.intersectionRatio > 0) {
          callback();
        }
      });
    }).observe(node);
  }, [callback]);
  (0, _react.useEffect)(function () {
    if (scrollRef.current) {
      scrollObserver(scrollRef.current);
    }
  }, [scrollObserver, scrollRef]);
}