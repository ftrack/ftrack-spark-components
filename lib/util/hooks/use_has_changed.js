"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = useHasChanged;

var _lodash = require("lodash");

var _use_previous = _interopRequireDefault(require("./use_previous"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2021 ftrack

/**
 * Check if value have changed between rendering cycles.
 *
 * *value* can be anything that needs to do deep comparison.
 *
 * Example use do deep equal between complex objects / arrays.
 */
function useHasChanged(obj) {
  var prevVal = (0, _use_previous.default)(obj);
  return !(0, _lodash.isEqual)(prevVal, obj);
}