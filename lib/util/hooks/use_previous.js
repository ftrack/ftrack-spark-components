"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = usePrevious;

var _react = require("react");

// :copyright: Copyright (c) 2021 ftrack

/**
 * Get previous value in react render cycle.
 *
 * *value* can be anything that needs to keep track of previous value.
 *
 * Example use do deep equal between complex objects / arrays.
 */
function usePrevious(value) {
  var ref = (0, _react.useRef)();
  (0, _react.useEffect)(function () {
    ref.current = value;
  });
  return ref.current;
}