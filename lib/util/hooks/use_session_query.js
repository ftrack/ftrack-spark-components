"use strict";

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useSessionQuery = useSessionQuery;
exports.useSessionSearch = useSessionSearch;

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.regexp.split.js");

require("core-js/modules/es6.regexp.search.js");

var _react = require("react");

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var buildQuery = function buildQuery(baseQuery, _ref) {
  var _ref$limit = _ref.limit,
      limit = _ref$limit === void 0 ? 10 : _ref$limit,
      _ref$offset = _ref.offset,
      offset = _ref$offset === void 0 ? 0 : _ref$offset;
  var queryString = baseQuery;

  if (offset >= 0) {
    queryString += " offset ".concat(offset);
  }

  if (limit >= 0) {
    queryString += " limit ".concat(limit);
  }

  return queryString;
};

function useCallbackQuery(queryFn, baseQuery, _ref2) {
  var _ref2$limit = _ref2.limit,
      limit = _ref2$limit === void 0 ? 30 : _ref2$limit,
      _ref2$offset = _ref2.offset,
      offset = _ref2$offset === void 0 ? 0 : _ref2$offset,
      pause = _ref2.pause;

  var _useState = (0, _react.useState)([]),
      _useState2 = _slicedToArray(_useState, 2),
      data = _useState2[0],
      setData = _useState2[1];

  var _useState3 = (0, _react.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      isLoading = _useState4[0],
      setIsLoading = _useState4[1];

  var _useState5 = (0, _react.useState)(true),
      _useState6 = _slicedToArray(_useState5, 2),
      canFetchMore = _useState6[0],
      setCanFetchMore = _useState6[1];

  var _useState7 = (0, _react.useState)(),
      _useState8 = _slicedToArray(_useState7, 2),
      error = _useState8[0],
      setError = _useState8[1];

  var fetchData = function fetchData(query, abortController) {
    var append = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    setIsLoading(true);
    setError();

    if (!append) {
      setData([]);
      setCanFetchMore(true);
    }

    queryFn(query, abortController).then(function (response) {
      var items = response.data;

      if (items.length < limit) {
        setCanFetchMore(false);
      }

      if (append) {
        setData(function (currentValues) {
          return [].concat(_toConsumableArray(currentValues), _toConsumableArray(items));
        });
      } else if (Array.isArray(items)) {
        setData(items);
      }

      setIsLoading(false);
    }).catch(function (e) {
      if (e.name !== "AbortError") {
        setError(e);
        setIsLoading(false);
      }
    });
  };

  var fetchMore = (0, _react.useCallback)(function () {
    if (!isLoading && canFetchMore) {
      var appendQuery = buildQuery(baseQuery, {
        limit: limit,
        offset: data.length
      });
      fetchData(appendQuery, undefined, true);
    }
  }, // TODO: Feel free to fix this eslint error if you got time
  // eslint-disable-next-line react-hooks/exhaustive-deps
  [isLoading, canFetchMore, baseQuery]);
  (0, _react.useEffect)(function () {
    if (!pause) {
      var query = buildQuery(baseQuery, {
        limit: limit,
        offset: offset
      });
      var abortController = new AbortController();

      if (query) {
        fetchData(query, abortController);
      }

      return function () {
        abortController.abort();
      };
    }
  }, // TODO: Feel free to fix this eslint error if you got time
  // eslint-disable-next-line react-hooks/exhaustive-deps
  [queryFn, baseQuery]);
  return {
    data: data,
    isLoading: isLoading,
    fetchMore: fetchMore,
    canFetchMore: canFetchMore,
    error: error
  };
}
/**
 * Used to query data with a hook.
 *
 * *session* ftrack-session object.
 *
 * *baseQuery* query to be run, offset and limit will be added if config with limit is used.
 *
 * *config* object with property limit to handle fetch more data.
 *
 */


function useSessionQuery(_ref3) {
  var session = _ref3.session,
      baseQuery = _ref3.baseQuery,
      _ref3$config = _ref3.config,
      config = _ref3$config === void 0 ? {} : _ref3$config;
  var queryFn = (0, _react.useCallback)(function (query, abortController) {
    return session.query(query, {
      abortController: abortController
    });
  }, // TODO: Feel free to fix this eslint error if you got time
  // eslint-disable-next-line react-hooks/exhaustive-deps
  [baseQuery]);
  return useCallbackQuery(queryFn, baseQuery, config);
}

function useSessionSearch(_ref4) {
  var session = _ref4.session,
      entityType = _ref4.entityType,
      _ref4$baseExpression = _ref4.baseExpression,
      baseExpression = _ref4$baseExpression === void 0 ? "" : _ref4$baseExpression,
      _ref4$objectTypeIds = _ref4.objectTypeIds,
      objectTypeIds = _ref4$objectTypeIds === void 0 ? [] : _ref4$objectTypeIds,
      _ref4$value = _ref4.value,
      value = _ref4$value === void 0 ? "" : _ref4$value,
      contextId = _ref4.contextId;
  var terms = value.split(" ").filter(function (w) {
    return w;
  });
  var searchFn = (0, _react.useCallback)(function (query, abortController) {
    return session.search({
      expression: query,
      entityType: entityType,
      terms: terms,
      contextId: contextId,
      objectTypeIds: objectTypeIds
    }, {
      abortController: abortController
    });
  }, // TODO: Feel free to fix this eslint error if you got time
  // eslint-disable-next-line react-hooks/exhaustive-deps
  [baseExpression, value, contextId, objectTypeIds.join(" "), entityType]);
  return useCallbackQuery(searchFn, baseExpression, {
    limit: 30
  });
}