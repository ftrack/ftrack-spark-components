"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = useScrollElementVisible;

require("core-js/modules/es6.array.slice.js");

var _react = require("react");

var _use_previous = _interopRequireDefault(require("./use_previous"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2021 ftrack

/**
 * Used to scroll a specfic list item index into view.
 *
 * *ref* ref to parent container for list.
 *
 * *currentIndex* index of item that should be scrolled into view.
 *
 *  *canScroll* if scroll should be active (optional) default true
 */
function useScrollElementVisible(ref, currentIndex) {
  var canScroll = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
  var prevIndex = (0, _use_previous.default)(currentIndex);
  (0, _react.useEffect)(function () {
    if (ref && canScroll) {
      var children = [].slice.call(ref.current.children);
      var selectedChild = children[currentIndex];

      if (selectedChild) {
        var containerHeight = ref.current.offsetHeight;
        var viewTop = ref.current.scrollTop;
        var viewBottom = viewTop + containerHeight;
        var itemHeight = selectedChild.offsetHeight;
        var itemTop = selectedChild.offsetTop;
        var itemBottom = itemTop + itemHeight;

        if (!prevIndex || prevIndex < currentIndex) {
          if (itemBottom > viewBottom) {
            ref.current.scrollTo({
              top: itemTop - (ref.current.offsetHeight - itemHeight),
              behavior: "smooth"
            });
          }
        } else if (prevIndex > currentIndex) {
          if (itemTop - itemHeight * 2 < viewTop) {
            ref.current.scrollTo({
              top: viewTop - itemHeight,
              behavior: "smooth"
            });
          }
        }
      }
    }
  }, // TODO: Feel free to fix this eslint error if you got time
  // eslint-disable-next-line react-hooks/exhaustive-deps
  [ref, prevIndex, canScroll]);
}