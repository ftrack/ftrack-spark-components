"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.guessName = guessName;
exports.getVersionFromEntityNameIfPossible = getVersionFromEntityNameIfPossible;
exports.getInitialsFromString = getInitialsFromString;

require("core-js/modules/es6.regexp.split.js");

require("core-js/modules/es6.regexp.replace.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.map.js");

// :copyright: Copyright (c) 2017 ftrack

/* eslint-disable import/prefer-default-export */

/**
 * Return guessed name from *email*
 *
 * Retrieves the part before the at sign, replaces separators with space
 * and transform to title case.
 */
function guessName(email) {
  var name = email.split("@")[0];
  name = name.replace(/[._-]/g, " ").replace(/\s\s+/g, " ");
  name = name.replace(/\w\S*/g, function (word) {
    return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
  });
  return name;
}
/** Extract version number from *entityName* if ending with "Version 1" or " v1".*/


function getVersionFromEntityNameIfPossible(entityName) {
  var versionLookup = (/[(?: v)|(?:Version )](\d+)$/.exec(entityName) || [false, false])[1]; // return false if not found

  if (versionLookup) {
    return versionLookup;
  }

  return false;
}
/**
 * Return initials for *inputString*.
 *
 * Can be used to generate initials for an user name or group name.
 * Eg. "Carl Claesson" -> "CC"
 */


function getInitialsFromString() {
  var inputString = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  return inputString.split(" ").map(function (word) {
    return word.charAt(0);
  }).join("").slice(0, 2).toUpperCase();
}