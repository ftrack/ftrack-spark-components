"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _progress_bar = _interopRequireDefault(require("react-toolbox/lib/progress_bar"));

var _component = _interopRequireDefault(require("../heading/component"));

var _classnames = _interopRequireDefault(require("classnames"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _style = _interopRequireDefault(require("./style.scss"));

var _progressbar_theme = _interopRequireDefault(require("./progressbar_theme.scss"));

var _progressbar_theme_approved = _interopRequireDefault(require("./progressbar_theme_approved.scss"));

var _progressbar_theme_requires_changes = _interopRequireDefault(require("./progressbar_theme_requires_changes.scss"));

var _progressbar_theme_seen = _interopRequireDefault(require("./progressbar_theme_seen.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["variant", "className", "label", "value"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ProgressIndicator(_ref) {
  var _classNames, _classNames2, _classNames3;

  var variant = _ref.variant,
      className = _ref.className,
      label = _ref.label,
      value = _ref.value,
      props = _objectWithoutProperties(_ref, _excluded);

  var progressBarClasses = (0, _classnames.default)(_style.default.progressBar, className, (_classNames = {}, _defineProperty(_classNames, _style.default.primaryBar, variant === "primary"), _defineProperty(_classNames, _style.default.secondaryBar, variant === "secondary"), _classNames));
  var textClasses = (0, _classnames.default)(_style.default.textStyles, className, (_classNames2 = {}, _defineProperty(_classNames2, _style.default.primaryText, variant === "primary"), _defineProperty(_classNames2, _style.default.secondaryText, variant === "secondary"), _defineProperty(_classNames2, _style.default.approvedText, variant === "approved"), _defineProperty(_classNames2, _style.default.requireChangesText, variant === "require_changes"), _defineProperty(_classNames2, _style.default.seenText, variant === "seen"), _classNames2));
  var percentageClasses = (0, _classnames.default)(_style.default.percentage, className, (_classNames3 = {}, _defineProperty(_classNames3, _style.default.primaryPercentage, variant === "primary"), _defineProperty(_classNames3, _style.default.secondaryPercentage, variant === "secondary"), _defineProperty(_classNames3, _style.default.approvedPercentage, variant === "approved"), _defineProperty(_classNames3, _style.default.requireChangesPercentage, variant === "require_changes"), _defineProperty(_classNames3, _style.default.seenPercentage, variant === "seen"), _classNames3));

  var theme = function theme() {
    switch (variant) {
      case "secondary":
        return _progressbar_theme.default;

      case "approved":
        return _progressbar_theme_approved.default;

      case "require_changes":
        return _progressbar_theme_requires_changes.default;

      case "seen":
        return _progressbar_theme_seen.default;

      default:
        return undefined;
    }
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", _objectSpread(_objectSpread({}, props), {}, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_component.default, {
      variant: "subheading",
      className: textClasses,
      children: label
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_component.default, {
      variant: "subheading",
      className: percentageClasses,
      children: [value, "%"]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_progress_bar.default, {
      className: progressBarClasses,
      theme: theme(),
      mode: "determinate",
      value: value
    })]
  }));
}

ProgressIndicator.propTypes = {
  variant: _propTypes.default.oneOf(["primary", "secondary"]),
  className: _propTypes.default.string,
  label: _propTypes.default.string,
  value: _propTypes.default.number
};

var _default = (0, _safe_inject_intl.default)(ProgressIndicator);

exports.default = _default;