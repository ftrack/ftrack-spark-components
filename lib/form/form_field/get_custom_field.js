"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _revealable_field = _interopRequireDefault(require("./revealable_field"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function getCustomField(Widget) {
  var CustomField = function CustomField(props) {
    var label = props.schema.title || props.schema.name;
    var help = props.uiSchema["ui:help"];
    var reveal = props.uiSchema.reveal;
    var classNames = props.uiSchema.classNames;
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_revealable_field.default, {
      className: classNames,
      label: label,
      reveal: reveal,
      errors: props.rawErrors,
      help: help,
      required: props.required,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(Widget, _objectSpread(_objectSpread({}, props), {}, {
        value: props.formData
      }))
    });
  };

  CustomField.propTypes = {
    classNames: _propTypes.default.string,
    schema: _propTypes.default.shape({
      title: _propTypes.default.string,
      name: _propTypes.default.string
    }),
    uiSchema: _propTypes.default.shape({
      reveal: _propTypes.default.bool,
      classNames: _propTypes.default.string
    }),
    rawErrors: _propTypes.default.arrayOf(_propTypes.default.string),
    rawHelp: _propTypes.default.string,
    required: _propTypes.default.bool,
    formData: _propTypes.default.any // eslint-disable-line react/forbid-prop-types

  };
  return CustomField;
}

var _default = getCustomField;
exports.default = _default;