"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reveal = _interopRequireDefault(require("../../reveal"));

var _help_icon = _interopRequireDefault(require("../../help_icon"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2017 ftrack
function RevealableField(props) {
  var reveal = props.reveal;
  var help = props.help;
  var hasErrors = props.errors && props.errors.length;
  var required = props.required;
  var label = props.label;
  var fieldClassName = (0, _classnames.default)(_style.default.field, props.className);
  var labelContent = label ? /*#__PURE__*/(0, _jsxRuntime.jsxs)("label", {
    htmlFor: props.id,
    className: _style.default.label,
    children: [label, required ? /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      className: _style.default.required,
      children: "*"
    }) : null, help ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_help_icon.default, {
      text: help,
      className: _style.default.helpIcon
    }) : null]
  }) : null;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reveal.default, {
    className: _style.default.reveal,
    label: labelContent,
    active: !reveal || hasErrors,
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: fieldClassName,
      children: [labelContent, props.children, hasErrors ? /*#__PURE__*/(0, _jsxRuntime.jsx)("ul", {
        className: _style.default.errors,
        children: props.errors.map(function (errorMessage) {
          return /*#__PURE__*/(0, _jsxRuntime.jsx)("li", {
            children: errorMessage
          });
        })
      }) : null]
    })
  });
}

RevealableField.propTypes = {
  id: _propTypes.default.string,
  className: _propTypes.default.string,
  children: _propTypes.default.node,
  reveal: _propTypes.default.bool,
  help: _propTypes.default.string,
  errors: _propTypes.default.arrayOf(_propTypes.default.string),
  required: _propTypes.default.bool,
  label: _propTypes.default.string
};
RevealableField.defaultProps = {
  id: null,
  children: null,
  reveal: false,
  help: null,
  errors: [],
  required: false,
  label: null
};
var _default = RevealableField;
exports.default = _default;