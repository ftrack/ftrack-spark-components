"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.object.assign.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _revealable_field = _interopRequireDefault(require("./revealable_field"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2017 ftrack
function FormField(props) {
  if (props.hidden) {
    return props.children;
  }

  if (props.schema && props.schema.type === "array") {
    return /*#__PURE__*/(0, _react.cloneElement)(props.children, Object.assign({}, props));
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_revealable_field.default, {
    className: props.classNames,
    id: props.id,
    label: props.displayLabel ? props.label : null,
    reveal: props.uiSchema.reveal,
    errors: props.rawErrors,
    help: props.rawHelp,
    required: props.required,
    children: [props.children, props.displayLabel && props.description ? props.description : null, props.help && !props.rawHelp ? props.help : null]
  });
}
/* eslint-disable react/no-unused-prop-types, react/forbid-prop-types */


FormField.propTypes = {
  id: _propTypes.default.string,
  classNames: _propTypes.default.string,
  label: _propTypes.default.string,
  children: _propTypes.default.node.isRequired,
  errors: _propTypes.default.element,
  rawErrors: _propTypes.default.arrayOf(_propTypes.default.string),
  help: _propTypes.default.element,
  rawHelp: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.element]),
  description: _propTypes.default.element,
  rawDescription: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.element]),
  hidden: _propTypes.default.bool,
  required: _propTypes.default.bool,
  readonly: _propTypes.default.bool,
  displayLabel: _propTypes.default.bool,
  fields: _propTypes.default.object,
  formContext: _propTypes.default.object,
  uiSchema: _propTypes.default.object
};
FormField.defaultProps = {
  classNames: "",
  hidden: false,
  readonly: false,
  required: false,
  displayLabel: true
};
var _default = FormField;
exports.default = _default;