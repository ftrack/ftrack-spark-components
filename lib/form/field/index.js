"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _switch = _interopRequireDefault(require("./switch"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2017 ftrack
// TODO: Feel free to fix this eslint error if you got time
// eslint-disable-next-line import/no-anonymous-default-export
var _default = {
  switch: _switch.default
};
exports.default = _default;