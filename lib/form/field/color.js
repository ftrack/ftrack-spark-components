"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _color_picker = _interopRequireDefault(require("../../color_picker"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2017 ftrack

/** Color field */
function ColorField(_ref) {
  var classNames = _ref.classNames,
      formData = _ref.formData,
      onChange = _ref.onChange;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_color_picker.default, {
    className: classNames,
    position: "right",
    border: true,
    value: formData,
    onChange: onChange
  });
}

ColorField.propTypes = {
  classNames: _propTypes.default.string,
  formData: _propTypes.default.string,
  onChange: _propTypes.default.func.isRequired
};
var _default = ColorField;
exports.default = _default;