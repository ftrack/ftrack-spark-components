"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.SwitchInput = void 0;

var _classnames = _interopRequireDefault(require("classnames"));

var _switch = _interopRequireDefault(require("react-toolbox/lib/switch"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _switch2 = _interopRequireDefault(require("./switch.scss"));

var _help_icon = _interopRequireDefault(require("../../help_icon"));

var _switch_theme = _interopRequireDefault(require("./switch_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2017 ftrack
var SwitchInput = function SwitchInput(_ref) {
  var className = _ref.className,
      onChange = _ref.onChange,
      label = _ref.label,
      name = _ref.name,
      value = _ref.value,
      description = _ref.description,
      help = _ref.help,
      theme = _ref.theme,
      labelClassName = _ref.labelClassName,
      descriptionClassName = _ref.descriptionClassName;
  var twoLines = label && description;
  var classes = (0, _classnames.default)(_switch2.default["switch-field"], className);
  var labelClass = twoLines ? _switch2.default["switch-field-title"] : _switch2.default["switch-field-description"];
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: classes,
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_switch.default, {
      name: name,
      className: twoLines && _switch_theme.default.fieldTwoLines,
      checked: value,
      onChange: onChange,
      theme: theme || _switch_theme.default
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      onClick: function onClick() {
        onChange(!value);
      },
      children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("p", {
        className: labelClassName || labelClass,
        children: [label, help ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_help_icon.default, {
          text: help,
          className: _switch2.default.helpIcon
        }) : null]
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
        className: descriptionClassName || _switch2.default["switch-field-description"],
        children: description
      })]
    })]
  });
};

exports.SwitchInput = SwitchInput;

function SwitchField(_ref2) {
  var onChange = _ref2.onChange,
      _ref2$schema = _ref2.schema,
      schema = _ref2$schema === void 0 ? {} : _ref2$schema,
      id = _ref2.id,
      formData = _ref2.formData,
      uiSchema = _ref2.uiSchema;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(SwitchInput, {
    name: id,
    value: formData,
    onChange: onChange,
    label: schema.title || schema.name,
    description: uiSchema.description,
    help: uiSchema["ui:help"]
  });
}

SwitchInput.propTypes = {
  className: _propTypes.default.string,
  label: _propTypes.default.string,
  description: _propTypes.default.string,
  help: _propTypes.default.string,
  name: _propTypes.default.string,
  onChange: _propTypes.default.func,
  value: _propTypes.default.bool,
  // eslint-disable-next-line react/forbid-prop-types
  theme: _propTypes.default.object,
  labelClassName: _propTypes.default.string,
  descriptionClassName: _propTypes.default.string
};
SwitchField.propTypes = {
  schema: _propTypes.default.object,
  id: _propTypes.default.string,
  formData: _propTypes.default.bool,
  onChange: _propTypes.default.func,
  uiSchema: _propTypes.default.object
};
var _default = SwitchField;
exports.default = _default;