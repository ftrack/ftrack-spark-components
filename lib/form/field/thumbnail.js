"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _thumbnail = _interopRequireDefault(require("../../droppable_area/thumbnail"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2017 ftrack
function ThumbnailField(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_thumbnail.default, {
    thumbnailSize: props.uiSchema.thumbnailSize,
    session: props.formContext.session,
    value: props.formData,
    onChange: props.onChange
  });
}

ThumbnailField.propTypes = {
  uiSchema: _propTypes.default.object,
  // eslint-disable-line
  formContext: _propTypes.default.object,
  // eslint-disable-line
  formData: _propTypes.default.string,
  onChange: _propTypes.default.func.isRequired
};
var _default = ThumbnailField;
exports.default = _default;