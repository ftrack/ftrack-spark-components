"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function get() {
    return _form.default;
  }
});

var _form = _interopRequireDefault(require("./form.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }