"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.dateToString = dateToString;
exports.stringToDate = stringToDate;
exports.default = void 0;

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _picker = require("../../picker");

var _isEqual = _interopRequireDefault(require("lodash/isEqual"));

var _moment = _interopRequireDefault(require("moment"));

var _loglevel = _interopRequireDefault(require("loglevel"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

/**
 * Convert date value to to string for JSON Schema.
 */
function dateToString(date, _ref) {
  var dateOnly = _ref.dateOnly,
      displayOffsetUnit = _ref.displayOffsetUnit,
      displayOffsetValue = _ref.displayOffsetValue;
  var dateValue = (0, _moment.default)(date);

  if (displayOffsetUnit && displayOffsetValue) {
    dateValue.add(displayOffsetValue, displayOffsetUnit);
  }

  if (dateOnly) {
    dateValue = dateValue.startOf("day");
  }

  return dateValue.format();
}
/**
 * Convert string value to date object for display.
 */


function stringToDate(stringValue, _ref2) {
  var dateOnly = _ref2.dateOnly,
      displayOffsetUnit = _ref2.displayOffsetUnit,
      displayOffsetValue = _ref2.displayOffsetValue;
  var dateValue = (0, _moment.default)(stringValue, _moment.default.ISO_8601, true);

  if (!dateValue.isValid()) {
    dateValue = (0, _moment.default)();
  }

  if (dateOnly) {
    dateValue = dateValue.startOf("day");
  }

  if (displayOffsetUnit && displayOffsetValue) {
    dateValue.subtract(displayOffsetValue, displayOffsetUnit);
  }

  return dateValue.toDate();
}

var DateTimeWidget = /*#__PURE__*/function (_Component) {
  _inherits(DateTimeWidget, _Component);

  var _super = _createSuper(DateTimeWidget);

  function DateTimeWidget(props) {
    var _this;

    _classCallCheck(this, DateTimeWidget);

    _this = _super.call(this, props);
    _this.handleChange = _this.handleChange.bind(_assertThisInitialized(_this));
    _this.state = {
      value: stringToDate(props.value, props)
    };
    return _this;
  }

  _createClass(DateTimeWidget, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (!(0, _isEqual.default)(nextProps.value, this.props.value) || nextProps.dateOnly !== this.props.dateOnly || nextProps.displayOffsetUnit !== this.props.displayOffsetUnit || nextProps.displayOffsetValue !== this.props.displayOffsetValue) {
        this.setState({
          value: stringToDate(nextProps.value, nextProps)
        });
      }
    }
  }, {
    key: "handleChange",
    value: function handleChange(value) {
      this.setState({
        value: value
      });
      this.props.onChange(dateToString(value, this.props));
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          _this$props$dateOnly = _this$props.dateOnly,
          dateOnly = _this$props$dateOnly === void 0 ? false : _this$props$dateOnly,
          _this$props$formConte = _this$props.formContext,
          formContext = _this$props$formConte === void 0 ? {} : _this$props$formConte;
      var sundayFirstDayOfWeek = formContext.sundayFirstDayOfWeek;

      if (sundayFirstDayOfWeek == null) {
        _loglevel.default.warn("sundayFirstDayOfWeek not specified in form context.");
      }

      var DatePickerComponent = this.props.datePickerComponent || _picker.DatePicker;
      var TimePickerComponent = this.props.timePickerComponent || _picker.TimePicker;
      return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: _style.default.datetime,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(DatePickerComponent, {
          inputClassName: _style.default.input,
          onChange: this.handleChange,
          value: this.state.value,
          autoOk: true,
          sundayFirstDayOfWeek: sundayFirstDayOfWeek
        }), dateOnly === true ? null : /*#__PURE__*/(0, _jsxRuntime.jsx)(TimePickerComponent, {
          inputClassName: _style.default.input,
          onChange: this.handleChange,
          value: this.state.value
        })]
      });
    }
  }]);

  return DateTimeWidget;
}(_react.Component);

DateTimeWidget.propTypes = {
  onChange: _propTypes.default.func,
  dateOnly: _propTypes.default.bool,
  displayOffsetUnit: _propTypes.default.number,
  displayOffsetValue: _propTypes.default.string,
  value: _propTypes.default.string,
  formContext: _propTypes.default.object,
  // eslint-disable-line
  datePickerComponent: _propTypes.default.element,
  // eslint-disable-line
  timePickerComponent: _propTypes.default.element // eslint-disable-line

};
DateTimeWidget.defaultProps = {
  datePickerComponent: null,
  timePickerComponent: null
};
var _default = DateTimeWidget;
exports.default = _default;