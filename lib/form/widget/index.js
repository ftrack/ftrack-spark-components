"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _input = _interopRequireDefault(require("./input"));

var _date_time = _interopRequireDefault(require("./date_time"));

var _unit_number = _interopRequireDefault(require("./unit_number"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var DateWidget = function DateWidget(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_date_time.default, _objectSpread(_objectSpread({}, props), {}, {
    dateOnly: true
  }));
};

var EndDateWidget = function EndDateWidget(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_date_time.default, _objectSpread(_objectSpread({}, props), {}, {
    dateOnly: true,
    displayOffsetUnit: "day",
    displayOffsetValue: 1
  }));
};

var HourUnitNumberWidget = function HourUnitNumberWidget(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_unit_number.default, _objectSpread(_objectSpread({}, props), {}, {
    unit: "hour"
  }));
};

var WorkDayUnitNumberWidget = function WorkDayUnitNumberWidget(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_unit_number.default, _objectSpread(_objectSpread({}, props), {}, {
    unit: "workday"
  }));
};

var DayUnitNumberWidget = function DayUnitNumberWidget(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_unit_number.default, _objectSpread(_objectSpread({}, props), {}, {
    unit: "day"
  }));
}; // TODO: Feel free to fix this eslint error if you got time
// eslint-disable-next-line import/no-anonymous-default-export


var _default = {
  TextWidget: _input.default,
  date: DateWidget,
  enddate: EndDateWidget,
  "date-time": _date_time.default,
  hour: HourUnitNumberWidget,
  workday: WorkDayUnitNumberWidget,
  day: DayUnitNumberWidget
};
exports.default = _default;