"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.number.constructor.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _input = _interopRequireDefault(require("react-toolbox/lib/input"));

var _loglevel = _interopRequireDefault(require("loglevel"));

var _classnames = _interopRequireDefault(require("classnames"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function asNumber(value) {
  var result = parseFloat(value);

  if (Number.isFinite(result)) {
    return result;
  }

  return undefined;
}
/** Unit number widget. */


var UnitNumberWidget = /*#__PURE__*/function (_Component) {
  _inherits(UnitNumberWidget, _Component);

  var _super = _createSuper(UnitNumberWidget);

  function UnitNumberWidget(props) {
    var _this;

    _classCallCheck(this, UnitNumberWidget);

    _this = _super.call(this, props);
    _this.handleChange = _this.handleChange.bind(_assertThisInitialized(_this));
    var value = asNumber(props.value);

    var formattedValue = _this.format(value);

    _this.state = {
      value: value,
      formattedValue: formattedValue
    };
    return _this;
  }

  _createClass(UnitNumberWidget, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.value !== this.props.value) {
        var value = asNumber(nextProps.value);
        var formattedValue = this.format(value);
        this.setState({
          value: value,
          formattedValue: formattedValue
        });
      }
    }
  }, {
    key: "secondsPerUnit",
    value: function secondsPerUnit() {
      var unit = this.props.unit;

      if (unit === "hour") {
        return 60 * 60;
      }

      if (unit === "workday") {
        if (!this.props.formContext || !this.props.formContext.workDayLength) {
          _loglevel.default.warn("workDayLength not set in form context.");

          return 8 * 60 * 60;
        }

        return this.props.formContext.workDayLength;
      }

      return 24 * 60 * 60;
    }
  }, {
    key: "parseInput",
    value: function parseInput(_value) {
      var value = asNumber(_value);
      return value * this.secondsPerUnit();
    }
  }, {
    key: "format",
    value: function format(value) {
      if (!Number.isFinite(value)) {
        return undefined;
      }

      return Math.round(100 * (value / this.secondsPerUnit())) / 100;
    }
  }, {
    key: "handleChange",
    value: function handleChange(value) {
      var seconds = this.parseInput(value);
      this.props.onChange(seconds);
    }
  }, {
    key: "render",
    value: function render() {
      var inputClassName = (0, _classnames.default)(_style.default.input, _defineProperty({}, _style.default.readonly, this.props.readonly));
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_input.default, {
        className: inputClassName,
        value: this.state.formattedValue,
        onChange: this.handleChange,
        type: "number",
        placeholder: "1",
        readOnly: this.props.readonly
      });
    }
  }]);

  return UnitNumberWidget;
}(_react.Component);

UnitNumberWidget.propTypes = {
  formContext: _propTypes.default.shape({
    workDayLength: _propTypes.default.number
  }),
  onChange: _propTypes.default.func,
  readonly: _propTypes.default.bool,
  value: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.number]),
  unit: _propTypes.default.oneOf(["hour", "workday", "day"])
};
var _default = UnitNumberWidget;
exports.default = _default;