"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.regexp.split.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.string.starts-with.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _react = require("react");

var _initial = _interopRequireDefault(require("lodash/initial"));

var _uniq = _interopRequireDefault(require("lodash/uniq"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _progress_bar = _interopRequireDefault(require("react-toolbox/lib/progress_bar"));

var _details = _interopRequireDefault(require("./details"));

var _util = _interopRequireDefault(require("../dataview/util"));

var _hoc = require("../util/hoc");

var _container = require("../dataview/container");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var DataDetailsPanel = (0, _container.withDataLoader)(_details.default);
var DataDetailsPanelDefault = (0, _hoc.withSubspace)(_container.reducer, _container.sagas, "grid-view-default")(DataDetailsPanel);

var DetailsContainerBase = /*#__PURE__*/function (_Component) {
  _inherits(DetailsContainerBase, _Component);

  var _super = _createSuper(DetailsContainerBase);

  function DetailsContainerBase(props) {
    var _this;

    _classCallCheck(this, DetailsContainerBase);

    _this = _super.call(this, props);
    _this.state = {
      attributeSource: null
    };
    return _this;
  }

  _createClass(DetailsContainerBase, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var _this$props = this.props,
          entity = _this$props.entity,
          session = _this$props.session,
          selectedAttributes = _this$props.selectedAttributes;
      var relations = (0, _uniq.default)(selectedAttributes.reduce(function (accumulator, item) {
        var fragments = item.split(".");

        if (fragments.length > 1) {
          accumulator.push((0, _initial.default)(fragments).join("."));
        }

        return accumulator;
      }, []));
      session.initializing.then(function () {
        return (0, _util.default)(entity.type, relations, session);
      }).then(function (attributeSource) {
        _this2.setState({
          attributeSource: attributeSource
        });
      });
    }
  }, {
    key: "render",
    value: function render() {
      var attributeSource = this.state.attributeSource;
      var _this$props2 = this.props,
          selectedAttributes = _this$props2.selectedAttributes,
          entity = _this$props2.entity,
          className = _this$props2.className,
          onItemClicked = _this$props2.onItemClicked;
      var element;

      if (!attributeSource) {
        element = /*#__PURE__*/(0, _jsxRuntime.jsx)(_progress_bar.default, {
          mode: "indeterminate"
        });
      } else {
        var attributes = selectedAttributes.map(function (item) {
          return attributeSource.attributes[item];
        });
        Object.keys(attributeSource.attributes).forEach(function (key) {
          if (key.startsWith("custom_attribute-")) {
            attributes.push(attributeSource.attributes[key]);
          }
        });
        element = /*#__PURE__*/(0, _jsxRuntime.jsx)(DataDetailsPanelDefault, {
          attributes: attributes,
          className: className,
          onItemClicked: onItemClicked,
          loader: {
            model: entity.type,
            limit: 1,
            filters: "id is \"".concat(entity.id, "\"")
          }
        });
      }

      return element;
    }
  }]);

  return DetailsContainerBase;
}(_react.Component);

DetailsContainerBase.propTypes = {
  entity: _propTypes.default.shape({
    id: _propTypes.default.string.isRequired,
    type: _propTypes.default.string.isRequired
  }).isRequired,
  className: _propTypes.default.string,
  onItemClicked: _propTypes.default.func,
  session: _propTypes.default.object.isRequired,
  // eslint-disable-line react/forbid-prop-types
  selectedAttributes: _propTypes.default.arrayOf(_propTypes.default.string.isRequired).isRequired
};

var _default = (0, _hoc.withSession)(DetailsContainerBase);

exports.default = _default;