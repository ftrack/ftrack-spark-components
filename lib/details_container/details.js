"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _attribute = require("../dataview/attribute");

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2020 ftrack
function DetailsPanel(_ref) {
  var data = _ref.data,
      attributes = _ref.attributes,
      className = _ref.className,
      onItemClicked = _ref.onItemClicked;
  var item = data[0];

  if (!item) {
    return null;
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: className,
    children: attributes.map(function (attribute) {
      var valueElement = item[attribute.key];

      if (attribute.formatter) {
        var AttributeValueComponentType = attribute.formatter;
        valueElement = /*#__PURE__*/(0, _jsxRuntime.jsx)(AttributeValueComponentType, {
          value: item[attribute.key],
          onClick: onItemClicked
        });
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: _style.default.attribute,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_attribute.AttributeLabel, {
          attribute: attribute,
          className: _style.default.attributeLabel,
          enableGroup: true
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default.attributeValue,
          children: valueElement
        })]
      }, attribute.key);
    })
  });
}

DetailsPanel.propTypes = {
  data: _propTypes.default.arrayOf(_propTypes.default.objects),
  attributes: _propTypes.default.arrayOf(_propTypes.default.objects),
  className: _propTypes.default.string,
  onItemClicked: _propTypes.default.func
};
var _default = DetailsPanel;
exports.default = _default;