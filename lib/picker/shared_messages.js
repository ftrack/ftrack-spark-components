"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.okCancelMessages = void 0;

var _reactIntl = require("react-intl");

// :copyright: Copyright (c) 2018 ftrack
var okCancelMessages = (0, _reactIntl.defineMessages)({
  okLabel: {
    "id": "ftrack-spark-components.i18n-date-picker.okLabel",
    "defaultMessage": "Ok"
  },
  cancelLabel: {
    "id": "ftrack-spark-components.i18n-date-picker.cancelLabel",
    "defaultMessage": "Cancel"
  }
});
exports.okCancelMessages = okCancelMessages;