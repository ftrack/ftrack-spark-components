"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es7.object.entries.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.regexp.split.js");

var _reactIntl = require("react-intl");

var _date_picker = _interopRequireDefault(require("react-toolbox/lib/date_picker"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _shared_messages = require("./shared_messages");

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["intl"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var datePickerLocaleMessages = (0, _reactIntl.defineMessages)({
  months: {
    "id": "ftrack-spark-components.i18n-date-picker.months",
    "defaultMessage": "January_February_March_April_May_June_July_August_September_October_November_December"
  },
  monthsShort: {
    "id": "ftrack-spark-components.i18n-date-picker.months-short",
    "defaultMessage": "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec"
  },
  weekdays: {
    "id": "ftrack-spark-components.i18n-date-picker.weekdays",
    "defaultMessage": "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday"
  },
  weekdaysShort: {
    "id": "ftrack-spark-components.i18n-date-picker.weekdays-short",
    "defaultMessage": "Sun_Mon_Tue_Wed_Thu_Fri_Sat"
  },
  weekdaysLetter: {
    "id": "ftrack-spark-components.i18n-date-picker.weekdays-letter",
    "defaultMessage": "_"
  }
});

function LocalizedDatePicker(_ref) {
  var intl = _ref.intl,
      props = _objectWithoutProperties(_ref, _excluded);

  var locale = {};

  for (var _i = 0, _Object$entries = Object.entries(datePickerLocaleMessages); _i < _Object$entries.length; _i++) {
    var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
        key = _Object$entries$_i[0],
        message = _Object$entries$_i[1];

    locale[key] = intl.formatMessage(message).split("_").filter(String);
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_date_picker.default, _objectSpread({
    cancelLabel: intl.formatMessage(_shared_messages.okCancelMessages.cancelLabel),
    okLabel: intl.formatMessage(_shared_messages.okCancelMessages.okLabel),
    locale: locale,
    inputFormat: function inputFormat(date) {
      return intl.formatDate(date, {
        day: "numeric",
        month: "short",
        year: "numeric"
      });
    }
  }, props));
}

LocalizedDatePicker.propTypes = {
  intl: _reactIntl.intlShape.isRequired
};

var _default = (0, _safe_inject_intl.default)(LocalizedDatePicker);

exports.default = _default;