"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.weak-map.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.removeLastItem = exports.iconHelper = void 0;

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.regexp.replace.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactIntl = require("react-intl");

var _breadcrumbs = _interopRequireDefault(require("../breadcrumbs"));

var _entity_list_item = _interopRequireWildcard(require("../entity_list_item"));

var _formatter = require("../formatter");

var _style = _interopRequireDefault(require("./style.scss"));

var _entityUtils = require("../formatter/entityUtils");

var _jsxRuntime = require("react/jsx-runtime");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var messages = (0, _reactIntl.defineMessages)({
  "open-review": {
    "id": "ftrack-spark-components.entity-picker-list-item.open-review",
    "defaultMessage": "Open"
  },
  "closed-review": {
    "id": "ftrack-spark-components.entity-picker-list-item.closed-review",
    "defaultMessage": "Closed"
  },
  "list-asset-version": {
    "id": "ftrack-spark-components.entity-picker-list-item.list-asset-version",
    "defaultMessage": "Asset version"
  },
  "list-task": {
    "id": "ftrack-spark-components.entity-picker-list-item.list-task",
    "defaultMessage": "Task"
  }
});

var iconHelper = function iconHelper(entity, value) {
  switch (entity) {
    case "Project":
      if (value === "active") return "toggle_on";
      if (value === "inactive") return "toggle_off";
      if (value === "hidden") return "visibility_off";
      return undefined;

    case "ReviewSession":
      if (value) return "lock_open";
      return "lock";

    case "TypedContext":
      if (value === "Shot") return "movie";
      if (value === "Sequence") return "folder";
      if (value === "Episode") return "video_library";
      if (value === "Task") return "assignment";
      if (value === "Asset Version") return "layers";
      if (value === "Asset") return "layers";
      if (value === "Asset Build") return "folder";
      if (value === "Folder") return "folder";
      if (value === "Milestone") return "flag";
      return undefined;

    default:
      return undefined;
  }
};

exports.iconHelper = iconHelper;

var removeLastItem = function removeLastItem() {
  var items = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  return items.slice(0, -1);
};

exports.removeLastItem = removeLastItem;
var entityFormatter = {
  User: function User(_ref) {
    var first_name = _ref.first_name,
        last_name = _ref.last_name,
        thumbnail_url = _ref.thumbnail_url,
        thumbnail_id = _ref.thumbnail_id,
        memberships = _ref.memberships;
    return {
      firstRow: _entityUtils.Entity.User.formatter({
        first_name: first_name,
        last_name: last_name
      }),
      secondRow: /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.ListItemChips, {
        items: memberships.map(function (m) {
          return {
            id: m.id,
            name: m.group.name,
            icon: "group"
          };
        })
      }),
      media: thumbnail_url && thumbnail_url.url && {
        thumbnail_id: thumbnail_id,
        thumbnail_url: thumbnail_url
      }
    };
  },
  Group: function Group(_ref2) {
    var name = _ref2.name,
        parent = _ref2.parent;
    return {
      firstRow: _entityUtils.Entity.Group.formatter({
        name: name
      }),
      secondRow: parent && parent.link ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.ListItemChips, {
        items: parent.link.map(function (m) {
          return {
            id: m.id,
            name: m.name,
            icon: "group"
          };
        })
      }) : null
    };
  },
  List: function List(_ref3) {
    var name = _ref3.name,
        project = _ref3.project,
        category = _ref3.category,
        system_type = _ref3.system_type;
    return {
      firstRow: _entityUtils.Entity.List.formatter({
        name: name
      }),
      secondRow: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_react.Fragment, {
        children: [project && project.full_name && /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.ListItemChip, {
          name: project.full_name,
          color: project.color
        }), category && /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.ListItemChip, {
          name: category.name,
          icon: "folder"
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.ListItemChip, {
          icon: system_type === "assetversion" ? "layers" : "assignment",
          name: system_type === "assetversion" ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["list-asset-version"])) : /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["list-task"]))
        })]
      })
    };
  },
  Context: function Context(_ref4) {
    var name = _ref4.name,
        context_type = _ref4.context_type,
        link = _ref4.link;
    return {
      firstRow: _entityUtils.Entity.TypedContext.formatter({
        name: name
      }),
      secondRow: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_react.Fragment, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_breadcrumbs.default, {
          truncation: "tooltip",
          items: removeLastItem(link).map(function (_ref5) {
            var id = _ref5.id,
                title = _ref5.name;
            return {
              id: id,
              title: title,
              disabled: true
            };
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.ListItemChip, {
          sentenceCase: true,
          icon: iconHelper("TypedContext", context_type),
          name: context_type,
          truncation: false
        })]
      })
    };
  },
  TypedContext: function TypedContext(_ref6) {
    var name = _ref6.name,
        object_type = _ref6.object_type,
        thumbnail_url = _ref6.thumbnail_url,
        status = _ref6.status,
        thumbnail_id = _ref6.thumbnail_id,
        link = _ref6.link;
    return {
      firstRow: _entityUtils.Entity.TypedContext.formatter({
        name: name
      }),
      secondRow: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_react.Fragment, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_breadcrumbs.default, {
          truncation: "tooltip",
          items: removeLastItem(link).map(function (_ref7) {
            var id = _ref7.id,
                title = _ref7.name;
            return {
              id: id,
              title: title,
              disabled: true
            };
          })
        }), object_type && object_type.name && /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.ListItemChip, {
          sentenceCase: true,
          icon: iconHelper("TypedContext", object_type.name),
          name: object_type.name,
          truncation: false
        })]
      }),
      media: thumbnail_url && thumbnail_url.url && {
        thumbnail_id: thumbnail_id,
        thumbnail_url: thumbnail_url
      },
      status: status
    };
  },
  Project: function Project(_ref8) {
    var full_name = _ref8.full_name,
        start_date = _ref8.start_date,
        end_date = _ref8.end_date,
        thumbnail_url = _ref8.thumbnail_url,
        thumbnail_id = _ref8.thumbnail_id,
        status = _ref8.status;
    return {
      firstRow: _entityUtils.Entity.Project.formatter({
        full_name: full_name
      }),
      secondRow: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_react.Fragment, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.ListItemChip, {
          sentenceCase: true,
          name: status,
          icon: iconHelper("Project", status)
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.ListItemChip, {
          icon: "date_range",
          name: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_react.Fragment, {
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_formatter.DateFormatter, {
              value: start_date
            }), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
              children: " - "
            }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_formatter.DateFormatter, {
              value: end_date
            })]
          })
        })]
      }),
      media: thumbnail_url && thumbnail_url.url && {
        thumbnail_id: thumbnail_id,
        thumbnail_url: thumbnail_url
      }
    };
  },
  Component: function Component(_ref9) {
    var name = _ref9.name,
        version = _ref9.version,
        _ref9$file_type = _ref9.file_type,
        file_type = _ref9$file_type === void 0 ? "" : _ref9$file_type;
    return {
      firstRow: _entityUtils.Entity.Component.formatter({
        name: name
      }),
      secondRow: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_react.Fragment, {
        children: [version && version.link && /*#__PURE__*/(0, _jsxRuntime.jsx)(_breadcrumbs.default, {
          truncation: "tooltip",
          items: removeLastItem(version.link).map(function (_ref10) {
            var id = _ref10.id,
                title = _ref10.name;
            return {
              id: id,
              title: title,
              disabled: true
            };
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.ListItemChip, {
          icon: "insert_drive_file",
          name: file_type ? file_type.replace(".", "").toUpperCase() : "",
          truncation: false
        })]
      })
    };
  },
  Asset: function Asset(_ref11) {
    var name = _ref11.name,
        latest_version = _ref11.latest_version;
    return {
      firstRow: _entityUtils.Entity.Asset.formatter({
        name: name
      }),
      secondRow: latest_version && latest_version.link && /*#__PURE__*/(0, _jsxRuntime.jsx)(_breadcrumbs.default, {
        truncation: "tooltip",
        items: removeLastItem(latest_version.link).map(function (_ref12) {
          var id = _ref12.id,
              title = _ref12.name;
          return {
            id: id,
            title: title,
            disabled: true
          };
        })
      }),
      media: latest_version && latest_version.thumbnail_url && latest_version.thumbnail_url.url && {
        thumbnail_id: latest_version.thumbnail_id,
        thumbnail_url: latest_version.thumbnail_url
      },
      status: latest_version && latest_version.status
    };
  },
  AssetVersion: function AssetVersion(_ref13) {
    var asset = _ref13.asset,
        version = _ref13.version,
        thumbnail_url = _ref13.thumbnail_url,
        thumbnail_id = _ref13.thumbnail_id,
        link = _ref13.link,
        status = _ref13.status,
        task = _ref13.task;
    return {
      firstRow:
      /*#__PURE__*/
      // Set specific style to show version number
      // when asset name gets truncated.
      (0, _jsxRuntime.jsxs)("span", {
        className: _style.default.assetVersionContainer,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
          className: _style.default.assetVersionName,
          children: [asset.name, "\xA0-\xA0"]
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
          className: _style.default.assetVersionNumber,
          children: ["v", version]
        })]
      }),
      secondRow: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_react.Fragment, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_breadcrumbs.default, {
          truncation: "tooltip",
          items: removeLastItem(link).map(function (_ref14) {
            var id = _ref14.id,
                title = _ref14.name;
            return {
              id: id,
              title: title,
              disabled: true
            };
          })
        }), task && task.type && /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.ListItemChip, {
          name: task.name,
          color: task.type.color,
          icon: "assignment",
          truncation: false
        })]
      }),
      additional: task && task.name,
      media: thumbnail_url && thumbnail_url.url && {
        thumbnail_id: thumbnail_id,
        thumbnail_url: thumbnail_url
      },
      status: status
    };
  },
  ReviewSession: function ReviewSession(_ref15) {
    var name = _ref15.name,
        project = _ref15.project,
        is_open = _ref15.is_open,
        thumbnail_id = _ref15.thumbnail_id,
        thumbnail_url = _ref15.thumbnail_url;
    return {
      firstRow: _entityUtils.Entity.ReviewSession.formatter({
        name: name
      }),
      secondRow: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_react.Fragment, {
        children: [project && project.full_name && project.color && /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.ListItemChip, {
          name: project.full_name,
          color: project.color
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.ListItemChip, {
          icon: iconHelper("ReviewSession", is_open),
          name: is_open ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["open-review"])) : /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["closed-review"]))
        })]
      }),
      media: thumbnail_url && thumbnail_url.url && {
        thumbnail_id: thumbnail_id,
        thumbnail_url: thumbnail_url
      }
    };
  }
};

function getDefaultFormatter(entityType) {
  return entityFormatter[entityType];
}

function EntityPickerListItem(_ref16) {
  var entity = _ref16.entity,
      entityType = _ref16.entityType,
      onSelected = _ref16.onSelected,
      active = _ref16.active,
      selected = _ref16.selected,
      disabled = _ref16.disabled,
      showToggle = _ref16.showToggle,
      onMouseEnter = _ref16.onMouseEnter,
      onMouseLeave = _ref16.onMouseLeave,
      _ref16$formatter = _ref16.formatter,
      formatter = _ref16$formatter === void 0 ? getDefaultFormatter(entityType) : _ref16$formatter,
      disabledTooltip = _ref16.disabledTooltip;
  var item = (0, _react.useMemo)(function () {
    return entity && formatter ? formatter(entity) : {};
  }, // dependency array below should also include formatter, but this causes issues when switching entityType,
  // since the entity prop and entityType might be mismatched for a render frame, causing errors.
  // once React 18 is released, I believe this will have been resolved since they will group state updates
  // in a more efficient way. try to readd formatter here and see if switching entityType in the storybook causes any issues.
  // JC
  // eslint-disable-next-line react-hooks/exhaustive-deps
  [entity]);

  var handleSelect = function handleSelect() {
    if (!disabled) {
      onSelected(entity);
    }
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_entity_list_item.default, {
    disableShadows: true,
    disableActiveIndicator: true,
    disabled: disabled,
    active: active,
    onClick: handleSelect,
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave,
    children: [item.media && /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.EntityThumbnail, {
      selectable: showToggle,
      selected: selected,
      disabled: disabled,
      disabledTooltip: disabledTooltip,
      onClick: handleSelect,
      media: {
        assetVersion: {
          thumbnail_id: item.media.thumbnail_id
        },
        thumbnailUrl: item.media.thumbnail_url.url
      }
    }), !item.media && showToggle && /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.EntityToggle, {
      selected: selected,
      disabled: disabled,
      disabledTooltip: disabledTooltip,
      onClick: handleSelect
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_entity_list_item.EntityListItemContent, {
      children: [item.firstRow && /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.EntityListItemRow, {
        className: _style.default.firstRow,
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
          className: _style.default.truncateText,
          children: item.firstRow
        })
      }), item.secondRow && /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.EntityListItemRow, {
        className: _style.default.secondRow,
        children: item.secondRow
      })]
    }), item.status && /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_list_item.EntityStatusBar, {
      status: item.status
    })]
  });
}

EntityPickerListItem.propTypes = {
  entity: _propTypes.default.shape({
    name: _propTypes.default.string
  }),
  onSelected: _propTypes.default.func,
  active: _propTypes.default.bool,
  onMouseEnter: _propTypes.default.func,
  onMouseLeave: _propTypes.default.func,
  selected: _propTypes.default.bool,
  disabled: _propTypes.default.bool,
  disabledTooltip: _propTypes.default.node,
  showToggle: _propTypes.default.bool,
  formatter: _propTypes.default.func,
  entityType: _propTypes.default.string
};
var _default = EntityPickerListItem;
exports.default = _default;