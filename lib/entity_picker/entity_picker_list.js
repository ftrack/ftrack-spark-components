"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.weak-map.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.string.includes.js");

require("core-js/modules/es7.array.includes.js");

var _react = require("react");

var _propTypes = _interopRequireWildcard(require("prop-types"));

var _reactIntl = require("react-intl");

var _progress_bar = _interopRequireDefault(require("react-toolbox/lib/progress_bar"));

var _entity_picker_list_item = _interopRequireDefault(require("./entity_picker_list_item"));

var _hooks = require("../util/hooks");

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var messages = (0, _reactIntl.defineMessages)({
  "no-matches-text": {
    "id": "ftrack-spark-components.entity-picker.no-matches-text",
    "defaultMessage": "Couldn't find a match. Try again."
  },
  "no-items-text": {
    "id": "ftrack-spark-components.entity-picker.no-items-text",
    "defaultMessage": "We could not find anything to display."
  }
});

function EntityPickerList(_ref) {
  var items = _ref.items,
      isLoading = _ref.isLoading,
      onScrollToBottom = _ref.onScrollToBottom,
      selectedIndex = _ref.selectedIndex,
      canFetchMore = _ref.canFetchMore,
      formatter = _ref.formatter,
      onHover = _ref.onHover,
      multiSelect = _ref.multiSelect,
      onSelected = _ref.onSelected,
      error = _ref.error,
      _ref$selectedIds = _ref.selectedIds,
      selectedIds = _ref$selectedIds === void 0 ? [] : _ref$selectedIds,
      _ref$disabledIds = _ref.disabledIds,
      disabledIds = _ref$disabledIds === void 0 ? [] : _ref$disabledIds,
      disabledTooltip = _ref.disabledTooltip,
      entityType = _ref.entityType,
      emptyStateText = _ref.emptyStateText;
  var scrollRef = (0, _react.useRef)(null);
  var listRef = (0, _react.useRef)(null);

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      isHovering = _useState2[0],
      setIsHovering = _useState2[1];

  (0, _hooks.useInfiniteScroll)(scrollRef, onScrollToBottom);
  (0, _hooks.useScrollElementVisible)(listRef, selectedIndex, !isHovering);
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: _style.default.listContainer,
    ref: listRef,
    onMouseEnter: function onMouseEnter() {
      return setIsHovering(true);
    },
    onMouseLeave: function onMouseLeave() {
      return setIsHovering(false);
    },
    children: [items && items.map(function (e, i) {
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_picker_list_item.default, {
        entity: e,
        active: i === selectedIndex,
        selected: selectedIds.includes(e.id),
        onSelected: onSelected,
        disabled: disabledIds.includes(e.id),
        onMouseEnter: function onMouseEnter() {
          return onHover(i);
        },
        showToggle: multiSelect,
        formatter: formatter,
        entityType: entityType,
        disabledTooltip: disabledTooltip
      }, e.id);
    }), !items && !isLoading && /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
      className: _style.default.noMatch,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["no-items-text"]))
    }), items && items.length === 0 && !isLoading && /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
      className: _style.default.noMatch,
      children: emptyStateText || /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["no-matches-text"]))
    }), isLoading && /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default.loadingContainer,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_progress_bar.default, {
        type: "circular",
        mode: "indeterminate",
        theme: {
          circular: _style.default.circular,
          circle: _style.default.circle
        }
      })
    }), error && !isLoading && /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      children: error.message
    }), items && !isLoading && canFetchMore && /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      id: "scroll-ref",
      style: {
        height: "1px"
      },
      ref: scrollRef
    })]
  });
}

EntityPickerList.propTypes = {
  items: _propTypes.default.array,
  // eslint-disable-line
  isLoading: _propTypes.default.bool,
  selectedIndex: _propTypes.default.number,
  onScrollToBottom: _propTypes.default.func,
  onSelected: _propTypes.default.func,
  onHover: _propTypes.default.func,
  canFetchMore: _propTypes.default.bool,
  multiSelect: _propTypes.default.bool,
  formatter: _propTypes.default.func,
  selectedIds: _propTypes.default.arrayOf(_propTypes.string),
  disabledIds: _propTypes.default.arrayOf(_propTypes.string),
  disabledTooltip: _propTypes.default.node,
  error: _propTypes.default.object,
  // eslint-disable-line
  entityType: _propTypes.default.string,
  emptyStateText: _propTypes.default.string
};
var _default = EntityPickerList;
exports.default = _default;