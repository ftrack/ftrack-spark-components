"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.weak-map.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.string.includes.js");

require("core-js/modules/es7.array.includes.js");

var _react = require("react");

var _propTypes = _interopRequireWildcard(require("prop-types"));

var _reactIntl = require("react-intl");

var _classnames = _interopRequireDefault(require("classnames"));

var _redux = require("redux");

var _dialog = _interopRequireDefault(require("react-toolbox/lib/dialog"));

var _button = _interopRequireDefault(require("react-toolbox/lib/button"));

var _chip = _interopRequireDefault(require("react-toolbox/lib/chip"));

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _search_field = _interopRequireDefault(require("../search_field"));

var _icon_button = _interopRequireDefault(require("../icon_button"));

var _entity_picker_list = _interopRequireDefault(require("./entity_picker_list"));

var _hooks = require("../util/hooks");

var _entityUtils = require("../formatter/entityUtils");

var _button_theme = _interopRequireDefault(require("./button_theme.scss"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var messages = (0, _reactIntl.defineMessages)({
  "cancel-button-text": {
    "id": "ftrack-spark-components.entity-picker-search.cancel-button-text",
    "defaultMessage": "Cancel"
  },
  "done-button-text": {
    "id": "ftrack-spark-components.entity-picker-search.done-button-text",
    "defaultMessage": "Done"
  },
  "chip-text": {
    "id": "ftrack-spark-components.entity-picker.chip-text",
    "defaultMessage": "{numberOfIds, plural, one {# selected item} other {# selected items}}"
  },
  "user-placeholder": {
    "id": "ftrack-spark-components.entity-picker.user-placeholder",
    "defaultMessage": "Search user by name or group name"
  },
  "group-placeholder": {
    "id": "ftrack-spark-components.entity-picker.group-placeholder",
    "defaultMessage": "Search group by name or parent group name"
  },
  "list-placeholder": {
    "id": "ftrack-spark-components.entity-picker.list-placeholder",
    "defaultMessage": "Search list by name or category name"
  },
  "context-placeholder": {
    "id": "ftrack-spark-components.entity-picker.context-placeholder",
    "defaultMessage": "Search by name or project name"
  },
  "project-placeholder": {
    "id": "ftrack-spark-components.entity-picker.project-placeholder",
    "defaultMessage": "Search project by name, date or status"
  },
  "component-placeholder": {
    "id": "ftrack-spark-components.entity-picker.component-placeholder",
    "defaultMessage": "Search component by name or file type"
  },
  "asset-placeholder": {
    "id": "ftrack-spark-components.entity-picker.asset-placeholder",
    "defaultMessage": "Search asset by name"
  },
  "asset-version-placeholder": {
    "id": "ftrack-spark-components.entity-picker.asset-version-placeholder",
    "defaultMessage": "Search asset version by name or parent names"
  },
  "review-placeholder": {
    "id": "ftrack-spark-components.entity-picker.review-placeholder",
    "defaultMessage": "Search review session by name"
  },
  "unknown-context": {
    "id": "ftrack-spark-components.entity-picker.unknown-context",
    "defaultMessage": "Unknown context"
  }
});
var DEFAULTS = {
  User: {
    projection: [].concat(_toConsumableArray(_entityUtils.Entity.User.labelProjection), ["thumbnail_url", "thumbnail_id", "memberships.group.name"]),
    order: _entityUtils.Entity.User.order,
    getPlaceholder: function getPlaceholder(intl) {
      return intl.formatMessage(messages["user-placeholder"]);
    }
  },
  Group: {
    projection: [].concat(_toConsumableArray(_entityUtils.Entity.Group.labelProjection), ["parent.memberships.group.name"]),
    order: _entityUtils.Entity.Group.order,
    getPlaceholder: function getPlaceholder(intl) {
      return intl.formatMessage(messages["group-placeholder"]);
    },
    filter: _entityUtils.Entity.Group.filter
  },
  List: {
    projection: [].concat(_toConsumableArray(_entityUtils.Entity.List.labelProjection), ["category.name", "system_type", "project.full_name", "project.color"]),
    getPlaceholder: function getPlaceholder(intl) {
      return intl.formatMessage(messages["list-placeholder"]);
    },
    projectFilter: function projectFilter(projectId) {
      return "project_id = '".concat(projectId, "'");
    },
    order: _entityUtils.Entity.List.order
  },

  get TypedContextList() {
    return this.List;
  },

  get AssetVersionList() {
    return this.List;
  },

  Context: {
    projection: [].concat(_toConsumableArray(_entityUtils.Entity.Context.labelProjection), ["link", "context_type"]),
    getPlaceholder: function getPlaceholder(intl) {
      return intl.formatMessage(messages["context-placeholder"]);
    },
    projectFilter: function projectFilter(projectId) {
      return "project_id = '".concat(projectId, "'");
    },
    order: _entityUtils.Entity.Context.order
  },
  TypedContext: {
    projection: [].concat(_toConsumableArray(_entityUtils.Entity.TypedContext.labelProjection), ["link", "type.name", "object_type.name", "status.color", "thumbnail_url", "thumbnail_id"]),
    getPlaceholder: function getPlaceholder(intl) {
      return intl.formatMessage(messages["context-placeholder"]);
    },
    projectFilter: function projectFilter(projectId) {
      return "project_id = '".concat(projectId, "'");
    },
    order: _entityUtils.Entity.TypedContext.order
  },
  Project: {
    projection: [].concat(_toConsumableArray(_entityUtils.Entity.Project.labelProjection), ["start_date", "end_date", "thumbnail_url", "thumbnail_id", "status"]),
    order: _entityUtils.Entity.Project.order,
    getPlaceholder: function getPlaceholder(intl) {
      return intl.formatMessage(messages["project-placeholder"]);
    }
  },
  Asset: {
    projection: [].concat(_toConsumableArray(_entityUtils.Entity.Asset.labelProjection), ["latest_version.link", "latest_version.thumbnail_url", "latest_version.thumbnail_id", "latest_version.status.color"]),
    order: _entityUtils.Entity.Asset.order,
    getPlaceholder: function getPlaceholder(intl) {
      return intl.formatMessage(messages["asset-placeholder"]);
    },
    projectFilter: function projectFilter(projectId) {
      return "parent.project.id = '".concat(projectId, "'");
    }
  },
  AssetVersion: {
    projection: [].concat(_toConsumableArray(_entityUtils.Entity.AssetVersion.labelProjection), ["link", "thumbnail_url", "thumbnail_id", "status.color", "task.name", "task.type.id", "task.type.color", "task.type.name", "asset.parent.name", "components.name"]),
    order: _entityUtils.Entity.AssetVersion.order,
    getPlaceholder: function getPlaceholder(intl) {
      return intl.formatMessage(messages["asset-version-placeholder"]);
    },
    projectFilter: function projectFilter(projectId) {
      return "asset.parent.project_id is '".concat(projectId, "'");
    }
  },
  ReviewSession: {
    projection: [].concat(_toConsumableArray(_entityUtils.Entity.ReviewSession.labelProjection), ["project.full_name", "project.color", "is_open", "thumbnail_url", "thumbnail_id"]),
    getPlaceholder: function getPlaceholder(intl) {
      return intl.formatMessage(messages["review-placeholder"]);
    },
    projectFilter: function projectFilter(projectId) {
      return "project_id = '".concat(projectId, "'");
    },
    order: _entityUtils.Entity.ReviewSession.order
  }
};

var buildQuery = function buildQuery(_ref) {
  var entityType = _ref.entityType,
      projection = _ref.projection,
      filter = _ref.filter,
      order = _ref.order;
  return "\n        select ".concat(projection.join(","), "\n        from ").concat(entityType, "\n        ").concat(filter ? "where ".concat(filter) : "", "\n        ").concat(order ? "order by ".concat(order) : "", "\n    ");
};

var buildSearchExpression = function buildSearchExpression(_ref2) {
  var entityType = _ref2.entityType,
      projection = _ref2.projection,
      _ref2$order = _ref2.order,
      order = _ref2$order === void 0 ? "" : _ref2$order,
      _ref2$filter = _ref2.filter,
      filter = _ref2$filter === void 0 ? "" : _ref2$filter;
  var projectionsLabel = projection.join(", ");
  return "select ".concat(projectionsLabel, "\n            from ").concat(entityType, "\n            ").concat(filter && "where ".concat(filter), "\n            ").concat(order && "order by ".concat(order));
};

var QUICK_SELECT = "QUICK";
var SINGLE_SELECT = "SINGLE";
var MULTI_SELECT = "MULTI";

function ContextToggle(_ref3) {
  var label = _ref3.label,
      onToggle = _ref3.onToggle,
      enabled = _ref3.enabled,
      canToggle = _ref3.canToggle;
  var chipTheme = _style.default.projectChip;

  if (enabled) {
    if (canToggle) {
      chipTheme = _style.default.projectChipSelected;
    } else {
      chipTheme = _style.default.projectChipSelectedAndCannotBeDisabled;
    }
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_chip.default, {
    theme: {
      chip: chipTheme
    },
    onClick: canToggle ? onToggle : function () {},
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
      className: (0, _classnames.default)(_style.default.icon, _defineProperty({}, _style.default.iconFade, enabled)),
      value: "check"
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      children: label
    })]
  });
}

ContextToggle.propTypes = {
  label: _propTypes.default.string,
  onToggle: _propTypes.default.func,
  enabled: _propTypes.default.bool,
  canToggle: _propTypes.default.bool
};

function EntityPicker(_ref4) {
  var open = _ref4.open,
      entityType = _ref4.entityType,
      objectTypeId = _ref4.objectTypeId,
      projection = _ref4.projection,
      order = _ref4.order,
      formatter = _ref4.formatter,
      placeholder = _ref4.placeholder,
      contextId = _ref4.contextId,
      disableContextFilterToggle = _ref4.disableContextFilterToggle,
      _ref4$canClose = _ref4.canClose,
      canClose = _ref4$canClose === void 0 ? true : _ref4$canClose,
      doneText = _ref4.doneText,
      onClose = _ref4.onClose,
      onSelected = _ref4.onSelected,
      session = _ref4.session,
      _ref4$baseFilter = _ref4.baseFilter,
      baseFilter = _ref4$baseFilter === void 0 ? "" : _ref4$baseFilter,
      _ref4$selected = _ref4.selected,
      selected = _ref4$selected === void 0 ? [] : _ref4$selected,
      _ref4$disabledIds = _ref4.disabledIds,
      disabledIds = _ref4$disabledIds === void 0 ? [] : _ref4$disabledIds,
      disabledTooltip = _ref4.disabledTooltip,
      _ref4$mode = _ref4.mode,
      mode = _ref4$mode === void 0 ? QUICK_SELECT : _ref4$mode,
      intl = _ref4.intl,
      emptyStateText = _ref4.emptyStateText;
  var hasConfiguredContextFilter = DEFAULTS[entityType].projectFilter !== undefined;

  var _useState = (0, _react.useState)(!!contextId && hasConfiguredContextFilter),
      _useState2 = _slicedToArray(_useState, 2),
      contextFilterEnabled = _useState2[0],
      setContextFilterEnabled = _useState2[1];

  var _useState3 = (0, _react.useState)(""),
      _useState4 = _slicedToArray(_useState3, 2),
      searchValue = _useState4[0],
      setSearchValue = _useState4[1];

  var _useSessionSearch = (0, _hooks.useSessionSearch)({
    session: session,
    baseExpression: buildSearchExpression({
      entityType: entityType,
      projection: projection || DEFAULTS[entityType].projection,
      order: order || DEFAULTS[entityType].order,
      filter: baseFilter || DEFAULTS[entityType].filter
    }),
    entityType: entityType,
    value: searchValue,
    contextId: contextFilterEnabled && hasConfiguredContextFilter && contextId,
    objectTypeIds: objectTypeId ? [objectTypeId] : []
  }),
      isLoading = _useSessionSearch.isLoading,
      data = _useSessionSearch.data,
      error = _useSessionSearch.error,
      fetchMore = _useSessionSearch.fetchMore,
      canFetchMore = _useSessionSearch.canFetchMore;

  var _useSessionQuery = (0, _hooks.useSessionQuery)({
    session: session,
    baseQuery: buildQuery({
      entityType: entityType,
      projection: projection || DEFAULTS[entityType].projection,
      order: order || DEFAULTS[entityType].order,
      filter: "id in (".concat(selected.map(function (id) {
        return "'".concat(id, "'");
      }).join(","), ")")
    }),
    config: {
      limit: 25,
      pause: !selected || selected.length === 0
    }
  }),
      isLoadingSelected = _useSessionQuery.isLoading,
      selectedData = _useSessionQuery.data,
      fetchMoreSelected = _useSessionQuery.fetchMore,
      canFetchMoreSelected = _useSessionQuery.canFetchMore,
      errorSelected = _useSessionQuery.error;

  var _useSessionQuery2 = (0, _hooks.useSessionQuery)({
    session: session,
    baseQuery: "select link from Context where id = '".concat(contextId, "'"),
    config: {
      limit: 1,
      pause: !contextId
    }
  }),
      isLoadingContextName = _useSessionQuery2.isLoading,
      contextData = _useSessionQuery2.data;

  var contextName = contextData && contextData.length && Array.isArray(contextData[0].link) && contextData[0].link.length && contextData[0].link[0].name ? contextData[0].link[0].name : /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["unknown-context"]));

  var _useState5 = (0, _react.useState)(0),
      _useState6 = _slicedToArray(_useState5, 2),
      selectedIndex = _useState6[0],
      setSelectedIndex = _useState6[1];

  var _useState7 = (0, _react.useState)([]),
      _useState8 = _slicedToArray(_useState7, 2),
      selectedIds = _useState8[0],
      setSelectedIds = _useState8[1];

  var _useState9 = (0, _react.useState)([]),
      _useState10 = _slicedToArray(_useState9, 2),
      selectedItems = _useState10[0],
      setSelectedItems = _useState10[1];

  var _useState11 = (0, _react.useState)(false),
      _useState12 = _slicedToArray(_useState11, 2),
      showSelected = _useState12[0],
      setShowSelected = _useState12[1];

  (0, _react.useEffect)(function () {
    setSelectedIds([]);
    setSelectedItems([]);
  }, [entityType]);
  (0, _react.useEffect)(function () {
    if (selected && selected.length) {
      setSelectedIds(selected);
    }
  }, [selected]);
  (0, _react.useEffect)(function () {
    if (selectedData) {
      setSelectedItems(function (prevItems) {
        return [].concat(_toConsumableArray(prevItems), _toConsumableArray(selectedData));
      });
    }
  }, [selectedData]);
  (0, _react.useEffect)(function () {
    if (!selectedIds.length) {
      setShowSelected(false);
    }
  }, [selectedIds]);

  var handleSearchChange = function handleSearchChange(searchStr) {
    setSearchValue(searchStr);
    setSelectedIndex(0);
    setShowSelected(false);
  };

  var handleSelected = function handleSelected(item) {
    if (disabledIds.includes(item.id)) {
      return;
    }

    if (mode === MULTI_SELECT) {
      var isSelected = selectedIds.includes(item.id);

      if (isSelected) {
        // If it's already selected, the item should be toggled off
        setSelectedIds(function (ids) {
          return ids.filter(function (i) {
            return i !== item.id;
          });
        });
        setSelectedItems(function (items) {
          return items.filter(function (i) {
            return i.id !== item.id;
          });
        });
      } else {
        setSelectedIds(function (ids) {
          return [].concat(_toConsumableArray(ids), [item.id]);
        });
        setSelectedItems(function (items) {
          return [].concat(_toConsumableArray(items), [item]);
        });
      }
    } else if (mode === SINGLE_SELECT) {
      var _isSelected = selectedIds.includes(item.id);

      if (_isSelected) {
        setSelectedIds([]);
        setSelectedItems([]);
      } else {
        setSelectedIds([item.id]);
        setSelectedItems([item]);
      }
    } else if (mode === QUICK_SELECT) {
      onSelected(_objectSpread(_objectSpread({}, item), {}, {
        returnLabel: _entityUtils.Entity[entityType].formatter(item)
      }));
    }
  };

  var handleOnHover = function handleOnHover(index) {
    setSelectedIndex(index);
  };

  var handleFetchMoreData = (0, _react.useCallback)(function () {
    if (showSelected) {
      if (selected.length > 0) {
        fetchMoreSelected();
      }
    } else {
      fetchMore();
    }
  }, [fetchMore, fetchMoreSelected, showSelected, selected.length]);

  var toggleShowSelected = function toggleShowSelected() {
    setShowSelected(function (prev) {
      return !prev;
    });
    setSelectedIndex(0);
  };

  var handleToggleContext = function handleToggleContext() {
    setShowSelected(false);
    setContextFilterEnabled(function (curr) {
      return !curr;
    });
  };

  var handleKeyPress = function handleKeyPress(key) {
    if (["Control+Enter", "Meta+Enter"].includes(key)) {
      if (mode === MULTI_SELECT) {
        onSelected(selectedItems.map(function (item) {
          return _objectSpread(_objectSpread({}, item), {}, {
            returnLabel: _entityUtils.Entity[entityType].formatter(item)
          });
        }));
      } else if (mode === SINGLE_SELECT) {
        var item = selectedItems[0];

        if (item && !disabledIds.includes(item.id)) {
          onSelected(_objectSpread(_objectSpread({}, selectedItems[0]), {}, {
            returnLabel: _entityUtils.Entity[entityType].formatter(selectedItems[0])
          }));
        } else if (!disabledIds.includes(item.id)) {
          onSelected(null);
        }
      } else if (mode === QUICK_SELECT) {
        var _item = data[selectedIndex];
        handleSelected(_item);
      }
    } else if (key === "ArrowUp" && data && data.length > 0 && selectedIndex > 0) {
      setSelectedIndex(function (currIndex) {
        return currIndex - 1;
      });
    } else if (key === "ArrowDown" && data && data.length - 1 > selectedIndex) {
      setSelectedIndex(function (currIndex) {
        return currIndex + 1;
      });
    } else if (key === "Enter" && data && data.length > 0) {
      var _item2 = data[selectedIndex];

      if (_item2) {
        handleSelected(_item2);
      }
    }
  };

  var chipTheme = showSelected ? _style.default.chipButtonSelected : _style.default.chipButton;
  var searchPlaceholder = DEFAULTS && DEFAULTS[entityType] && DEFAULTS[entityType].getPlaceholder(intl) || placeholder;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_dialog.default, {
    className: _style.default.dialog,
    active: open,
    onEscKeyDown: onClose,
    onOverlayClick: onClose,
    theme: {
      body: _style.default.dialogBody
    },
    children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default.container,
      children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: _style.default.topContainer,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_search_field.default, {
          large: true,
          disableClose: true,
          debounce: 600,
          resetIcon: "backspace",
          onChange: handleSearchChange,
          onKeyPress: handleKeyPress,
          keys: ["ArrowUp", "ArrowDown", "Enter", "Control+Enter", "Meta+Enter"],
          placeholder: searchPlaceholder,
          additionalMatchesAvailable: canFetchMore,
          matches: data && !isLoading && Boolean(searchValue) ? data.length : null
        }), mode === QUICK_SELECT && canClose && /*#__PURE__*/(0, _jsxRuntime.jsx)(_icon_button.default, {
          className: _style.default.closeButton,
          icon: "close",
          onClick: onClose
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: _style.default.chipContainer,
        children: [contextId && hasConfiguredContextFilter && !isLoadingContextName ? /*#__PURE__*/(0, _jsxRuntime.jsx)(ContextToggle, {
          label: contextName,
          onToggle: handleToggleContext,
          enabled: contextFilterEnabled,
          canToggle: !disableContextFilterToggle
        }) : /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {}), mode !== QUICK_SELECT && /*#__PURE__*/(0, _jsxRuntime.jsxs)(_chip.default, {
          theme: {
            chip: !selectedIds.length ? _style.default.chipDisabled : chipTheme
          },
          onClick: toggleShowSelected,
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
            className: (0, _classnames.default)(_style.default.icon, _defineProperty({}, _style.default.iconFade, showSelected)),
            value: "check"
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["chip-text"]), {}, {
              values: {
                selectordinal: selectedIds.length,
                numberOfIds: selectedIds.length
              }
            }))
          })]
        })]
      }), !error && /*#__PURE__*/(0, _jsxRuntime.jsxs)(_react.Fragment, {
        children: [!showSelected && /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_picker_list.default, {
          items: data,
          isLoading: isLoading,
          selectedIndex: selectedIndex,
          onScrollToBottom: handleFetchMoreData,
          onSelected: handleSelected,
          onHover: handleOnHover,
          canFetchMore: canFetchMore,
          multiSelect: mode !== QUICK_SELECT,
          selectedIds: selectedIds,
          disabledIds: disabledIds,
          disabledTooltip: disabledTooltip,
          error: error,
          formatter: formatter,
          entityType: entityType,
          emptyStateText: emptyStateText
        }), showSelected && /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_picker_list.default, {
          items: selectedItems,
          isLoading: isLoadingSelected,
          selectedIndex: selectedIndex,
          onScrollToBottom: handleFetchMoreData,
          onSelected: handleSelected,
          onHover: handleOnHover,
          canFetchMore: canFetchMoreSelected,
          multiSelect: mode !== QUICK_SELECT,
          selectedIds: selectedIds,
          disabledIds: disabledIds,
          disabledTooltip: disabledTooltip,
          error: errorSelected,
          formatter: formatter,
          entityType: entityType,
          emptyStateText: emptyStateText
        })]
      })]
    }), mode !== QUICK_SELECT && /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default.buttonContainer,
      children: [canClose ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_button.default, {
        theme: _button_theme.default,
        label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["cancel-button-text"])),
        onClick: function onClick() {
          return onClose();
        }
      }) : /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {}), /*#__PURE__*/(0, _jsxRuntime.jsx)(_button.default, {
        primary: true,
        theme: _button_theme.default,
        label: doneText || /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["done-button-text"])),
        onClick: function onClick() {
          if (mode === MULTI_SELECT) {
            onSelected(selectedItems.map(function (item) {
              return _objectSpread(_objectSpread({}, item), {}, {
                returnLabel: _entityUtils.Entity[entityType].formatter(item)
              });
            }));
          } else if (mode === SINGLE_SELECT) {
            var item = selectedItems[0];

            if (item) {
              onSelected(_objectSpread(_objectSpread({}, selectedItems[0]), {}, {
                returnLabel: _entityUtils.Entity[entityType].formatter(selectedItems[0])
              }));
            } else {
              onSelected(null);
            }
          }
        }
      })]
    })]
  });
}

EntityPicker.propTypes = {
  session: _propTypes.default.object.isRequired,
  // eslint-disable-line
  baseFilter: _propTypes.default.string,
  entityType: _propTypes.default.string.isRequired,
  objectTypeId: _propTypes.default.string,
  projection: _propTypes.default.arrayOf(_propTypes.string),
  order: _propTypes.default.string,
  formatter: _propTypes.default.func,
  // eslint-disable-line
  placeholder: _propTypes.default.string,
  open: _propTypes.default.bool,
  contextId: _propTypes.default.string,
  disableContextFilterToggle: _propTypes.default.bool,
  onClose: _propTypes.default.func,
  onSelected: _propTypes.default.func,
  mode: _propTypes.default.oneOf(["QUICK", "SINGLE", "MULTI"]),
  selected: _propTypes.default.arrayOf(_propTypes.string),
  disabledIds: _propTypes.default.arrayOf(_propTypes.string),
  disabledTooltip: _propTypes.default.node,
  canClose: _propTypes.default.bool,
  doneText: _propTypes.default.string,
  emptyStateText: _propTypes.default.string,
  intl: _reactIntl.intlShape
};

var _default = (0, _redux.compose)(_safe_inject_intl.default)(EntityPicker);

exports.default = _default;