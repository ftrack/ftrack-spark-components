"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.regexp.replace.js");

require("core-js/modules/es6.array.slice.js");

var _constant = require("../util/constant");

var replacer = function replacer(md) {
  var arrayReplaceAt = md.utils.arrayReplaceAt;

  var splitTextToken = function splitTextToken(text, level, Token) {
    var token;
    var lastPos = 0;
    var nodes = [];
    text.replace(_constant.MENTION_REGEX, function (match, p1, p2, offset) {
      if (offset > lastPos) {
        token = new Token("text", "", 0);
        token.content = text.slice(lastPos, offset);
        nodes.push(token);
      }

      token = new Token("mention", "span", 0);
      token.attrs = [["id", p2], ["label", p1]];
      token.content = "@".concat(p2);
      nodes.push(token);
      lastPos = offset + match.length;
    });

    if (lastPos < text.length) {
      token = new Token("text", "", 0);
      token.content = text.slice(lastPos);
      nodes.push(token);
    }

    return nodes;
  };

  return function mentionReplace(state) {
    var tokens;
    var token;
    var autolinkLevel = 0;
    var blockTokens = state.tokens;
    var i;
    var j;
    var l;

    for (j = 0, l = blockTokens.length; j < l; j++) {
      if (blockTokens[j].type === "inline") {
        tokens = blockTokens[j].children;

        for (i = tokens.length - 1; i >= 0; i--) {
          token = tokens[i];

          if (token.type === "link_open" || token.type === "link_close") {
            if (token.info === "auto") {
              autolinkLevel -= token.nesting;
            }
          }

          if (token.type === "text" && autolinkLevel === 0 && _constant.MENTION_REGEX.test(token.content)) {
            blockTokens[j].children = tokens = arrayReplaceAt(tokens, i, splitTextToken(token.content, token.level, state.Token));
          }
        }
      }
    }
  };
};

var renderer = function renderer(tokens, idx) {
  var token = tokens[idx];
  return "<MentionTooltip id=\"".concat(token.attrGet("id"), "\" label=\"").concat(token.attrGet("label"), "\" session={session} />");
};
/**
 * MentionPlugin parses and renders inline mentions of format @{text_mentioned:resource_id}
 */


var MentionPlugin = function MentionPlugin(md) {
  md.renderer.rules.mention = renderer;
  md.core.ruler.push("mention", replacer(md));
};

var _default = MentionPlugin;
exports.default = _default;