"use strict";

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.replaceHtmlBreaks = exports.markdown = exports.default = void 0;

require("core-js/modules/es6.regexp.replace.js");

require("core-js/modules/es6.regexp.constructor.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _markdownIt = _interopRequireDefault(require("markdown-it"));

var _markdownItLinkAttributes = _interopRequireDefault(require("markdown-it-link-attributes"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactJsxParser = _interopRequireDefault(require("react-jsx-parser"));

var _tooltip = _interopRequireDefault(require("react-toolbox/lib/tooltip"));

var _hoc = require("../util/hoc");

var _entity_avatar = _interopRequireDefault(require("../entity_avatar"));

var _mention_plugin = _interopRequireDefault(require("./mention_plugin"));

var _style = _interopRequireDefault(require("./style.scss"));

var _tooltip_theme = _interopRequireDefault(require("./tooltip_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var SpanWithToolTip = (0, _tooltip.default)("span");
var markdown = new _markdownIt.default({
  linkify: true,
  breaks: true
});
exports.markdown = markdown;
markdown.use(_markdownItLinkAttributes.default, {
  attrs: {
    target: "_blank",
    rel: "noopener"
  }
});
markdown.use(_mention_plugin.default);

var MentionTooltip = function MentionTooltip(_ref) {
  var id = _ref.id,
      label = _ref.label,
      session = _ref.session;

  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      user = _useState2[0],
      setUser = _useState2[1];

  var loadUser = function loadUser() {
    if (user === null) {
      session.query("select first_name, last_name, thumbnail_id, thumbnail_url, id from User where id is ".concat(id)).then(function (response) {
        if (response.data) {
          setUser(response.data[0]);
        }
      });
    }
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(SpanWithToolTip, {
    theme: _tooltip_theme.default,
    tooltip: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_avatar.default, {
        session: session,
        size: "small",
        entity: user || {
          name: label
        }
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
        className: _style.default["tooltip-label"],
        children: [" ", user ? "".concat(user.first_name, " ").concat(user.last_name) : label]
      })]
    }),
    tooltipPosition: "top",
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
      className: "mention",
      onMouseOver: loadUser,
      children: ["@", label]
    })
  });
};

MentionTooltip.propTypes = {
  id: _propTypes.default.string,
  label: _propTypes.default.string,
  session: _propTypes.default.object
};

var replaceHtmlBreaks = function replaceHtmlBreaks(text) {
  return text.replace(new RegExp("<br>", "g"), "\n");
};
/** Render markdown from *source*. */


exports.replaceHtmlBreaks = replaceHtmlBreaks;

var Markdown = function Markdown(_ref2) {
  var _classNames;

  var source = _ref2.source,
      size = _ref2.size,
      className = _ref2.className,
      session = _ref2.session;
  var classes = (0, _classnames.default)(_style.default.markdown, (_classNames = {}, _defineProperty(_classNames, _style.default["size-small"], size === "small"), _defineProperty(_classNames, _style.default["size-large"], size === "large"), _classNames), className);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: classes,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactJsxParser.default, {
      bindings: {
        session: session
      },
      autoCloseVoidElements: true,
      components: {
        MentionTooltip: MentionTooltip
      },
      jsx: markdown.render(replaceHtmlBreaks(source) || "")
    })
  });
};

Markdown.propTypes = {
  source: _propTypes.default.string.isRequired,
  size: _propTypes.default.oneOf(["small", "medium", "large"]),
  className: _propTypes.default.string,
  session: _propTypes.default.object
};
Markdown.defaultProps = {
  size: "small"
};

var _default = (0, _hoc.withSession)(Markdown);

exports.default = _default;