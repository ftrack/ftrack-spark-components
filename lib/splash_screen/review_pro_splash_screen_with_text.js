"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.object.assign.js");

var _reactIntl = require("react-intl");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _tour = _interopRequireDefault(require("../tour"));

var _messages = _interopRequireDefault(require("./messages"));

var _uiSync = _interopRequireDefault(require("./images/ui-sync.png"));

var _review_pro_splash_screen_with_text = _interopRequireDefault(require("./review_pro_splash_screen_with_text.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function ReviewProSplashScreenWithText(_ref) {
  var props = Object.assign({}, _ref);
  var steps = [{
    title: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default.title)),
    text: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["text-p1"])), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        style: {
          marginBottom: "16px"
        }
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["text-p2"])), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        style: {
          marginBottom: "8px"
        }
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("ul", {
        className: _review_pro_splash_screen_with_text.default.list,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("li", {
          className: _review_pro_splash_screen_with_text.default["list-item"],
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["text-li-1"]))
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("li", {
          className: _review_pro_splash_screen_with_text.default["list-item"],
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["text-li-2"]))
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("li", {
          className: _review_pro_splash_screen_with_text.default["list-item"],
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["text-li-3"]))
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["action-text"]))]
    }),
    image: _uiSync.default,
    layout: "image_right"
  }];
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_tour.default, _objectSpread({
    active: true,
    steps: steps
  }, props));
}

var _default = (0, _safe_inject_intl.default)(ReviewProSplashScreenWithText);

exports.default = _default;