"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reactIntl = require("react-intl");

// :copyright: Copyright (c) 2020 ftrack
var messages = (0, _reactIntl.defineMessages)({
  title: {
    "id": "ftrack-spark-components.splash_screen.title",
    "defaultMessage": "Upgrade to Pro \u2728"
  },
  "text-p1": {
    "id": "ftrack-spark-components.splash_screen.text-p1",
    "defaultMessage": "Keep everyone in the frame with synced reviews and approve projects in real-time."
  },
  "text-p2": {
    "id": "ftrack-spark-components.splash_screen.text-p2",
    "defaultMessage": "What\u2019s included in the Review Pro add-on:"
  },
  "text-li-1": {
    "id": "ftrack-spark-components.splash_screen.text-li-1",
    "defaultMessage": "Up to 10 people in live sync review"
  },
  "text-li-2": {
    "id": "ftrack-spark-components.splash_screen.text-li-2",
    "defaultMessage": "Additional 250 GB storage"
  },
  "text-li-3": {
    "id": "ftrack-spark-components.splash_screen.text-li-3",
    "defaultMessage": "Increased video quality up to 4K"
  },
  "action-button": {
    "id": "ftrack-spark-components.splash_screen.action-button",
    "defaultMessage": "Upgrade"
  },
  "action-text": {
    "id": "ftrack-spark-components.splash_screen.action-text",
    "defaultMessage": "Go to your subscription and billing page to upgrade to Review Pro."
  }
});
var _default = messages;
exports.default = _default;