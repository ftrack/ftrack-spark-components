"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reactIntl = require("react-intl");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _tour = _interopRequireDefault(require("../tour"));

var _messages = _interopRequireDefault(require("./messages"));

var _uiSync = _interopRequireDefault(require("./images/ui-sync.png"));

var _review_pro_splash_screen_with_link = _interopRequireDefault(require("./review_pro_splash_screen_with_link.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["onAction"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ReviewProSplashScreenWithLink(_ref) {
  var onAction = _ref.onAction,
      props = _objectWithoutProperties(_ref, _excluded);

  var steps = [{
    title: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default.title)),
    text: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["text-p1"])), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        style: {
          marginBottom: "16px"
        }
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["text-p2"])), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        style: {
          marginBottom: "8px"
        }
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("ul", {
        className: _review_pro_splash_screen_with_link.default.list,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("li", {
          className: _review_pro_splash_screen_with_link.default["list-item"],
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["text-li-1"]))
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("li", {
          className: _review_pro_splash_screen_with_link.default["list-item"],
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["text-li-2"]))
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("li", {
          className: _review_pro_splash_screen_with_link.default["list-item"],
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["text-li-3"]))
        })]
      })]
    }),
    image: _uiSync.default,
    layout: "image_right",
    actions: [{
      text: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, _messages.default["action-button"])),
      value: "close",
      primary: true,
      href: null,
      className: _review_pro_splash_screen_with_link.default.ctaButton
    }]
  }];
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_tour.default, _objectSpread({
    active: true,
    steps: steps,
    onAction: onAction
  }, props));
}

ReviewProSplashScreenWithLink.propTypes = {
  onAction: _propTypes.default.func,
  active: _propTypes.default.bool
};

var _default = (0, _safe_inject_intl.default)(ReviewProSplashScreenWithLink);

exports.default = _default;