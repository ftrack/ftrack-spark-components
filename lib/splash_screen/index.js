"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ReviewProSplashScreenWithLink", {
  enumerable: true,
  get: function get() {
    return _review_pro_splash_screen_with_link.default;
  }
});
Object.defineProperty(exports, "ReviewProSplashScreenWithText", {
  enumerable: true,
  get: function get() {
    return _review_pro_splash_screen_with_text.default;
  }
});

var _review_pro_splash_screen_with_link = _interopRequireDefault(require("./review_pro_splash_screen_with_link.js"));

var _review_pro_splash_screen_with_text = _interopRequireDefault(require("./review_pro_splash_screen_with_text.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }