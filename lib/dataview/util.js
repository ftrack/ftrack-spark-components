"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.weak-map.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isCustomAttribute = isCustomAttribute;
exports.getPolymorphicRelationDetails = getPolymorphicRelationDetails;
exports.getFilterOperators = getFilterOperators;
exports.getCustomAttributeLinksProperties = getCustomAttributeLinksProperties;
exports.buildCustomAttributes = buildCustomAttributes;
exports.getProperties = getProperties;
exports.getFilters = getFilters;
exports.default = getAttributes;
exports.ATTRIBUTE_OVERRIDES = exports.FILTER_OVERRIDES = exports.CUSTOM_ATTRIBUTE_LINK_FROM_KEY_PREFIX = exports.CUSTOM_ATTRIBUTE_LINK_TO_KEY_PREFIX = exports.CUSTOM_ATTRIBUTE_KEY_PREFIX = exports.attributeGroupShape = exports.attributeShape = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.regexp.split.js");

require("core-js/modules/es6.string.starts-with.js");

require("core-js/modules/es6.regexp.replace.js");

require("core-js/modules/es6.array.find.js");

require("core-js/modules/es6.string.ends-with.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.sort.js");

require("core-js/modules/es6.object.assign.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.string.includes.js");

require("core-js/modules/es7.array.includes.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.promise.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _recompose = require("recompose");

var _ftrackJavascriptApi = require("ftrack-javascript-api");

var _isArray = _interopRequireDefault(require("lodash/isArray"));

var _reactIntl = require("react-intl");

var _hoc = require("../util/hoc");

var _selector = _interopRequireDefault(require("../selector"));

var _project_selector = _interopRequireDefault(require("../selector/project_selector"));

var _resource_selector = _interopRequireDefault(require("../selector/resource_selector"));

var _date_filter = _interopRequireDefault(require("../filter/date_filter"));

var _query_filter = _interopRequireWildcard(require("../filter/query_filter"));

var _dropdown_filter = _interopRequireDefault(require("../filter/dropdown_filter"));

var _entityUtils = require("../formatter/entityUtils");

var _schema_operations = _interopRequireWildcard(require("../util/schema_operations"));

var _formatter = require("../formatter");

var _constant = require("../util/constant");

var _jsxRuntime = require("react/jsx-runtime");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var attributeShape = _propTypes.default.shape({
  icon: _propTypes.default.string,
  key: _propTypes.default.string.isRequired,
  description: _propTypes.default.string,
  label: _propTypes.default.string
});

exports.attributeShape = attributeShape;

var attributeGroupShape = _propTypes.default.shape({
  key: _propTypes.default.string.isRequired,
  label: _propTypes.default.string,
  attributes: _propTypes.default.arrayOf(attributeShape).isRequired
});

exports.attributeGroupShape = attributeGroupShape;
var CUSTOM_ATTRIBUTE_KEY_PREFIX = "custom_attribute-"; // We would prefer CUSTOM_ATTRIBUTE_LINK_TO_KEY_PREFIX to include `-to-`, but
// unfortunately this will break filters that have already added links while from support
// wasn't shipped.

exports.CUSTOM_ATTRIBUTE_KEY_PREFIX = CUSTOM_ATTRIBUTE_KEY_PREFIX;
var CUSTOM_ATTRIBUTE_LINK_TO_KEY_PREFIX = "custom_attribute-";
exports.CUSTOM_ATTRIBUTE_LINK_TO_KEY_PREFIX = CUSTOM_ATTRIBUTE_LINK_TO_KEY_PREFIX;
var CUSTOM_ATTRIBUTE_LINK_FROM_KEY_PREFIX = "custom_attribute-from-";
exports.CUSTOM_ATTRIBUTE_LINK_FROM_KEY_PREFIX = CUSTOM_ATTRIBUTE_LINK_FROM_KEY_PREFIX;

function encodeArrayCondition(items) {
  return items.map(function (item) {
    return "\"".concat(item, "\"");
  }).join(", ");
}
/** Return true if *attribute* is a custom attribute */


function isCustomAttribute(attribute) {
  var attributeName = attribute.key.split(".").pop();
  return attributeName.startsWith(CUSTOM_ATTRIBUTE_KEY_PREFIX);
}
/** Return object details from *relation* or null.
 *
 * Passing parent[TypedContext] as an argument will return an object with:
 *
 * * `name`: parent
 * * `targetSchemaId`: TypedContext
 *
 * Passing parent will return null.
 */


function getPolymorphicRelationDetails(relation) {
  var regExp = /.*\[([^\]]*)\]/;
  var result = regExp.exec(relation);

  if (!result) {
    return null;
  }

  var targetSchemaId = result[1];
  return {
    name: relation.replace("[".concat(targetSchemaId, "]"), ""),
    targetSchemaId: targetSchemaId
  };
}

var FILTER_MESSAGES = (0, _reactIntl.defineMessages)({
  "ancestors-hint": {
    "id": "ftrack-spark-components.dataview.utils.ancestors-hint",
    "defaultMessage": "Matches objects above in project hierarchy"
  }
});

function getFilterOperators(type) {
  var operators = {
    number: ["is", "is_not", "is_greater", "is_greater_or_equal", "is_less", "is_less_or_equal"],
    integer: ["is", "is_not", "is_greater", "is_greater_or_equal", "is_less", "is_less_or_equal"],
    string: ["is", "is_not", "is_greater", "is_greater_or_equal", "is_less", "is_less_or_equal", "contains", "not_contains", "starts_with", "ends_with"],
    boolean: ["is"],
    enumerator: ["in", "not_in", "set", "not_set"],
    link: ["in", "not_in"]
  };
  return operators[type] || ["is", "is_not"];
}

function CustomAttributeFormatWrapper(id, type, config, _ref) {
  var value = _ref.value,
      onClick = _ref.onClick;

  // Return correct formatter based on *value* and *configuration*.
  if (type === "link") {
    if (value && value.length > 0) {
      return value.map(function (v, i) {
        var entityValue = config.getDisplayData(v);

        if (!entityValue) {
          // The entity value might be undefined if pruned by a
          // server side permission engine. I.e. the link exist,
          // but the user doesn't have the permission to see the value.
          return null;
        }

        return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_react.Fragment, {
          children: [!!i && /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
            children: ", "
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(config.formatter, {
            value: entityValue,
            onClick: onClick
          })]
        }, entityValue.id);
      });
    }

    return null;
  }

  var customAttributeItem = (value || []).find(function (item) {
    return item.configuration_id === id;
  });
  var customAttributeValue = customAttributeItem && customAttributeItem.value;

  if (customAttributeValue === null || customAttributeValue === undefined) {
    return null;
  }

  if (type === "number") {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      children: customAttributeValue.toFixed(config.isdecimal ? 2 : 0)
    });
  }

  if (type === "date-time") {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_formatter.DateFormatter, {
      value: customAttributeValue
    });
  }

  if (type === "boolean") {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_formatter.BooleanFormatter, {
      value: customAttributeValue
    });
  }

  if (type === "enumerator") {
    var data;

    try {
      data = JSON.parse(config.data);
    } catch (error) {
      data = [];
    } // Build list of enumerator labels. Use raw value if label does not
    // exist.


    var enumeratorLabels = customAttributeValue.map(function (item) {
      var candidate = data.find(function (option) {
        return option.value === item;
      });
      return candidate && candidate.menu ? candidate.menu : item;
    });
    return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      children: enumeratorLabels.join(", ")
    });
  }

  if (type === "dynamic enumerator") {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      children: customAttributeValue.join(", ")
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
    children: customAttributeValue
  });
}

var remoteSelectorHandler = (0, _recompose.withHandlers)({
  onChange: function onChange(props) {
    return function (value) {
      props.onChange(value && value.length ? value.map(function (item) {
        return item.value ? item.value : item;
      }) : null);
    };
  }
});

function bidFilterToSeconds(value, isDisplayBidAsDays, workdayLength) {
  if (isDisplayBidAsDays) {
    return (value || 0) * workdayLength;
  }

  return (value || 0) * _constant.SECONDS_IN_HOUR;
}

function getBidFilterCondition(attributeName) {
  return function (operator, value) {
    var configuration = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
    var workdayLength = configuration.workdayLength || _constant.DEFAULT_WORKDAY_LENGTH;
    var isDisplayBidAsDays = configuration.isDisplayBidAsDays || _constant.DEFAULT_DISPLAY_BID_AS_DAYS;
    return "".concat(attributeName, " ").concat(operator, " \"").concat(bidFilterToSeconds(value, isDisplayBidAsDays, workdayLength), "\"");
  };
}

function addObjectTypeInformationToAddtribute(attribute, objectType, schemas) {
  // Name filters according to the object type name instead of relationship (ancestors -> Episode)
  attribute.groupLabel = objectType.name; // Add hint to filters about how it is applied

  attribute.filterMessage = FILTER_MESSAGES["ancestors-hint"]; // Used to apply filter to group (... and object_type_id is ${objectTypeId})

  attribute.objectTypeId = objectType.id; // Used as an identifier for the filter, `ancestors[${objectTypeSchemaId}]`

  attribute.objectTypeSchemaId = (0, _schema_operations.getObjectTypeSchemaId)(schemas, objectType.id);

  if (attribute.key.endsWith("$entities")) {
    // When selecting entity we need to add more information to entity picker
    attribute.options = _objectSpread(_objectSpread({}, attribute.options), {}, {
      objectTypeId: objectType.id,
      objectTypeName: objectType.name,
      objectTypeSchemaId: (0, _schema_operations.getObjectTypeSchemaId)(schemas, objectType.id)
    });
  }
}

function buildRelationalGroup(relation, schemas, schema, model, purpose, objectType) {
  var parts = [];
  var targetSchema = relation.split(".").reduce(function (fromSchema, relationFragment) {
    var details = getPolymorphicRelationDetails(relationFragment);
    var nextSchema;

    if (details) {
      // The relation is polymorphic and the schema and key
      // must be changed.
      nextSchema = schemas.find(function (item) {
        return item.id === details.targetSchemaId;
      });
    } else {
      var property = fromSchema.properties[relationFragment];
      var ref;

      if (property.type === "array") {
        ref = property.items.$ref;
      } else {
        ref = property.$ref;
      }

      nextSchema = schemas.find(function (item) {
        return item.id === ref;
      });
    }

    parts.push({
      key: relationFragment,
      targetModel: nextSchema.id,
      model: fromSchema.id
    });
    return nextSchema;
  }, schema);
  var relationalAttributes = getPropertiesFromSchema(targetSchema, purpose, parts, objectType).scalar;

  if (objectType) {
    // When object type is set, we need to add additonal information about relations
    // for each attribute to be used on filters in query, entity picker and label
    relationalAttributes.forEach(function (attribute) {
      addObjectTypeInformationToAddtribute(attribute, objectType, schemas);
    });
  }

  return {
    path: parts,
    groupLabel: objectType === null || objectType === void 0 ? void 0 : objectType.name,
    key: "".concat(model, ".").concat(relation),
    model: targetSchema.id,
    relation: relation,
    attributes: relationalAttributes
  };
}

var FILTER_OVERRIDES = {
  TypedContext: {
    bid: {
      filterCondition: getBidFilterCondition("bid")
    },
    bid_time_logged_difference: {
      filterCondition: getBidFilterCondition("bid_time_logged_difference")
    },
    time_logged: {
      filterCondition: getBidFilterCondition("time_logged")
    },
    $query: {
      filterComponent: (0, _recompose.withProps)({
        model: "TypedContext"
      })(_query_filter.default),
      filterCondition: function filterCondition(operator, value, configuration) {
        return (0, _query_filter.replaceVariables)(value, configuration);
      },
      filterOperators: []
    },
    $project: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        multi: true
      }))(_project_selector.default),
      filterCondition: function filterCondition(operator, value) {
        return "project_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in"]
    },
    $status: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        entityType: "Status",
        multi: true,
        extraFields: ["color"],
        orderByField: "sort",
        orderByDirection: "DESC"
      }))(_selector.default),
      filterCondition: function filterCondition(operator, value) {
        return "status_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in"]
    },
    $type: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        entityType: "Type",
        multi: true,
        extraFields: ["color"],
        orderByField: "sort"
      }))(_selector.default),
      filterCondition: function filterCondition(operator, value) {
        return "type_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in"]
    },
    $priority: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        entityType: "Priority",
        multi: true,
        extraFields: ["color"],
        orderByField: "sort"
      }))(_selector.default),
      filterCondition: function filterCondition(operator, value) {
        return "priority_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in"]
    },
    $assignees: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        users: true,
        multi: true
      }))(_resource_selector.default),
      filterCondition: function filterCondition(operator, value) {
        if (operator === "set") {
          return "assignments any ()";
        }

        if (operator === "not_set") {
          return "not assignments any ()";
        }

        return "assignments.resource_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in", "set", "not_set"]
    },
    $objectType: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        entityType: "ObjectType",
        multi: true,
        orderByField: "sort"
      }))(_selector.default),
      filterCondition: function filterCondition(operator, value) {
        return "object_type_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in"]
    },
    $scope: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        entityType: "Scope",
        multi: true
      }))(_selector.default),
      filterCondition: function filterCondition(operator, value) {
        if (operator === "set") {
          return "scopes any ()";
        }

        if (operator === "not_set") {
          return "not scopes any ()";
        }

        return "scopes.id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in", "set", "not_set"]
    },
    created_at: {
      filterComponent: (0, _recompose.withProps)({
        modes: ["this_week", "this_month", "today", "yesterday", "date", "custom_range"]
      })(_date_filter.default)
    },
    $entities: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "TypedContext"
      },
      filterCondition: function filterCondition(operator, value) {
        return "id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      }
    },
    $lists: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "List",
        baseFilter: "system_type is 'task'"
      },
      filterCondition: function filterCondition(operator, value) {
        return "lists.id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      }
    },
    $outgoing_links: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "TypedContext"
      },
      filterCondition: function filterCondition(operator, value) {
        return "outgoing_links.to_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      }
    },
    $incoming_links: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "TypedContext"
      },
      filterCondition: function filterCondition(operator, value) {
        return "incoming_links.from_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      }
    }
  },
  ReviewSession: {},
  AssetVersion: {
    $isLatestVersion: {
      type: "boolean",
      filterCondition: function filterCondition(operator, value) {
        return value ? "is_latest_version is True" : "is_latest_version is False";
      }
    },
    date: {
      filterComponent: (0, _recompose.withProps)({
        modes: ["this_week", "this_month", "today", "yesterday", "date", "custom_range"]
      })(_date_filter.default)
    },
    $query: {
      filterComponent: (0, _recompose.withProps)({
        model: "AssetVersion"
      })(_query_filter.default),
      filterCondition: function filterCondition(operator, value, configuration) {
        return (0, _query_filter.replaceVariables)(value, configuration);
      },
      filterOperators: []
    },
    $project: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        multi: true
      }))(_project_selector.default),
      filterCondition: function filterCondition(operator, value) {
        var values = encodeArrayCondition(value);
        var condition = "(asset.parent.project_id in (".concat(values, "))");

        if (operator === "not_in") {
          return "not ".concat(condition);
        }

        return condition;
      },
      filterOperators: ["in", "not_in"]
    },
    $status: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        entityType: "Status",
        multi: true,
        extraFields: ["color"],
        orderByField: "sort",
        orderByDirection: "DESC"
      }))(_selector.default),
      filterCondition: function filterCondition(operator, value) {
        return "status_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in"]
    },
    $assetType: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        entityType: "AssetType",
        multi: true
      }))(_selector.default),
      filterCondition: function filterCondition(operator, value) {
        return "asset.type_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in"]
    },
    $publishedBy: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        users: true,
        multi: true
      }))(_resource_selector.default),
      filterCondition: function filterCondition(operator, value) {
        return "user_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in"]
    },
    $asset_parent: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "Context"
      },
      filterCondition: function filterCondition(operator, value) {
        return "asset.parent.id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      }
    },
    $lists: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "List",
        baseFilter: "system_type is 'assetversion'"
      },
      filterCondition: function filterCondition(operator, value) {
        return "lists.id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      }
    },
    $outgoing_links: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "AssetVersion"
      },
      filterCondition: function filterCondition(operator, value) {
        return "outgoing_links.to_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      }
    },
    $incoming_links: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "AssetVersion"
      },
      filterCondition: function filterCondition(operator, value) {
        return "incoming_links.from_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      }
    },
    $review_sessions: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "ReviewSession"
      },
      filterCondition: function filterCondition(operator, value) {
        return "review_session_objects.review_session.id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      }
    }
  },
  Type: {
    $type: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        entityType: "Type",
        multi: true,
        extraFields: ["color"],
        orderByField: "sort"
      }))(_selector.default),
      filterCondition: function filterCondition(operator, value) {
        return "id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in"]
    }
  },
  Note: {
    $author: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        users: true,
        multi: true
      }))(_resource_selector.default),
      filterCondition: function filterCondition(operator, value) {
        return "user_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in"]
    },
    date: {
      filterComponent: (0, _recompose.withProps)({
        modes: ["this_week", "this_month", "today", "yesterday", "date", "custom_range"]
      })(_date_filter.default)
    },
    thread_activity: {
      filterComponent: (0, _recompose.withProps)({
        modes: ["this_week", "this_month", "today", "yesterday", "date", "custom_range"]
      })(_date_filter.default)
    },
    completed_at: {
      filterComponent: (0, _recompose.withProps)({
        modes: ["this_week", "this_month", "today", "yesterday", "date", "custom_range"]
      })(_date_filter.default)
    },
    $is_completed: {
      type: "boolean",
      filterCondition: function filterCondition(operator, value) {
        if (value) {
          return "completed_at is_not None";
        }

        return "completed_at is None";
      }
    },
    $recipients: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        users: true,
        multi: true,
        groups: true
      }))(_resource_selector.default),
      filterCondition: function filterCondition(operator, value) {
        return "recipients.resource_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in"]
    },
    $note_labels: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        entityType: "NoteLabel",
        multi: true,
        extraFields: ["name"],
        orderByField: "sort"
      }))(_selector.default),
      filterCondition: function filterCondition(operator, value) {
        return "note_label_links.label_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in"]
    },
    $query: {
      filterComponent: (0, _recompose.withProps)({
        model: "Note"
      })(_query_filter.default),
      filterCondition: function filterCondition(operator, value, configuration) {
        return (0, _query_filter.replaceVariables)(value, configuration);
      },
      filterOperators: []
    }
  },
  Project: {
    $project: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        multi: true
      }))(_project_selector.default),
      filterCondition: function filterCondition(operator, value) {
        var values = encodeArrayCondition(value);

        if (operator === "not_in") {
          return "id not_in (".concat(values, ")");
        }

        return "id in (".concat(values, ")");
      },
      filterOperators: ["in", "not_in"]
    },
    $scope: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        entityType: "Scope",
        multi: true
      }))(_selector.default),
      filterCondition: function filterCondition(operator, value) {
        if (operator === "set") {
          return "scopes any ()";
        }

        if (operator === "not_set") {
          return "not scopes any ()";
        }

        return "scopes.id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in", "set", "not_set"]
    },
    status: {
      filterComponent: (0, _recompose.withProps)({
        multiSelect: false,
        items: [{
          value: null,
          label: "Any"
        }, {
          value: "active",
          label: "Active"
        }, {
          value: "hidden",
          label: "Hidden"
        }]
      })(_dropdown_filter.default),
      filterOperators: ["is", "is_not"]
    },
    created_at: {
      filterComponent: (0, _recompose.withProps)({
        modes: ["this_week", "this_month", "today", "yesterday", "date", "custom_range"]
      })(_date_filter.default)
    }
  },
  User: {
    $user: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        users: true,
        multi: true
      }))(_resource_selector.default),
      filterCondition: function filterCondition(operator, value) {
        return "id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in"]
    },
    $group_membership: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        users: false,
        groups: true,
        multi: true
      }))(_resource_selector.default),
      filterCondition: function filterCondition(operator, value) {
        return "memberships.group_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in"]
    }
  },
  Group: {
    $group: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        users: false,
        groups: true,
        multi: true
      }))(_resource_selector.default),
      filterCondition: function filterCondition(operator, value) {
        return "id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in"]
    }
  },
  CalendarEvent: {
    $project: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        multi: true
      }))(_project_selector.default),
      filterCondition: function filterCondition(operator, value) {
        return "project_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in"]
    },
    $type: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        entityType: "Type",
        multi: true,
        extraFields: ["color"],
        orderByField: "sort"
      }))(_selector.default),
      filterCondition: function filterCondition(operator, value) {
        return "type_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in"]
    },
    $users: {
      filterComponent: (0, _recompose.compose)(_hoc.withSession, remoteSelectorHandler, (0, _recompose.withProps)({
        users: true,
        multi: true
      }))(_resource_selector.default),
      filterCondition: function filterCondition(operator, value) {
        if (operator === "set") {
          return "calendar_event_resources any ()";
        }

        if (operator === "not_set") {
          return "not calendar_event_resources any ()";
        }

        return "calendar_event_resources.resource_id ".concat(operator, " (").concat(encodeArrayCondition(value), ")");
      },
      filterOperators: ["in", "not_in", "set", "not_set"]
    },
    estimate: {
      filterCondition: function filterCondition(operator, value) {
        var configuration = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
        var workdayLength = configuration.workdayLength || _constant.DEFAULT_WORKDAY_LENGTH;
        return "estimate ".concat(operator, " \"").concat((value || 0) * workdayLength, "\"");
      }
    },
    effort: {
      filterCondition: function filterCondition(operator, value) {
        return "effort ".concat(operator, " \"").concat((value || 0) * 3600, "\"");
      }
    }
  }
};
exports.FILTER_OVERRIDES = FILTER_OVERRIDES;
var ATTRIBUTE_OVERRIDES = {
  Asset: {
    $type: {
      formatter: (0, _recompose.mapProps)(function (_ref2) {
        var value = _ref2.value;
        return {
          value: value && value.type ? value.type.name : null
        };
      })(_formatter.TextFormatter),
      projection: ["type.name"]
    }
  },
  AssetVersion: {
    date: {
      formatter: (0, _recompose.mapProps)(function (_ref3) {
        var value = _ref3.value;
        return {
          value: value,
          displayTooltip: true,
          localize: true
        };
      })(_formatter.DateFormatter)
    },
    $name: {
      formatter: (0, _recompose.mapProps)(function (_ref4) {
        var value = _ref4.value,
            onClick = _ref4.onClick;
        return {
          value: value && value.link && [value.link[value.link.length - 1]] || [],
          onClick: onClick
        };
      })(_formatter.LinkFormatter),
      projection: ["link"]
    },
    link: {
      formatter: _formatter.LinkFormatter
    },
    _link: {
      formatter: _formatter.LinkFormatter
    },
    $linkedTo: {
      formatter: (0, _recompose.mapProps)(function (_ref5) {
        var value = _ref5.value,
            onClick = _ref5.onClick;
        return {
          value: value && value.link && value.link.slice(0, value.link.length - 1) || [],
          onClick: onClick
        };
      })(_formatter.LinkFormatter),
      projection: ["link"]
    },
    thumbnail_: {
      formatter: (0, _recompose.mapProps)(function (_ref6) {
        var value = _ref6.value,
            onClick = _ref6.onClick,
            height = _ref6.height;
        return {
          thumbnailId: value && value.thumbnail_id,
          onClick: onClick,
          height: height
        };
      })(_formatter.ThumbnailFormatter),
      projection: ["thumbnail_id"]
    },
    $status: {
      formatter: (0, _recompose.mapProps)(function (_ref7) {
        var value = _ref7.value;
        return {
          name: value && value.status ? value.status.name : null,
          color: value && value.status ? value.status.color : null
        };
      })(_formatter.ColorLabelFormatter),
      projection: ["status.color", "status.name"],
      sortOn: ["status.sort"]
    },
    $user: {
      formatter: (0, _recompose.mapProps)(function (_ref8) {
        var value = _ref8.value;
        return {
          first: value && value.user ? value.user.first_name : null,
          last: value && value.user ? value.user.last_name : null
        };
      })(_formatter.FullnameFormatter),
      projection: ["user.first_name", "user.last_name"]
    },
    $lists: {
      formatter: (0, _recompose.mapProps)(function (_ref9) {
        var _ref9$value = _ref9.value,
            value = _ref9$value === void 0 ? {} : _ref9$value,
            onClick = _ref9.onClick;
        return {
          values: value.lists,
          onClick: onClick
        };
      })(_formatter.ListsFormatter),
      projection: ["lists.name", "lists.id"]
    }
  },
  Context: {
    link: {
      formatter: _formatter.LinkFormatter
    },
    name: {
      formatter: (0, _recompose.mapProps)(function (_ref10) {
        var value = _ref10.value,
            onClick = _ref10.onClick;
        return {
          value: value && [{
            name: value.name,
            id: value.id,
            type: "TypedContext"
          }] || [],
          onClick: onClick
        };
      })(_formatter.LinkFormatter),
      projection: ["name", "id"]
    },
    $createdBy: {
      formatter: (0, _recompose.mapProps)(function (_ref11) {
        var value = _ref11.value;
        return {
          first: value && value.created_by ? value.created_by.first_name : null,
          last: value && value.created_by ? value.created_by.last_name : null
        };
      })(_formatter.FullnameFormatter),
      projection: ["created_by.first_name", "created_by.last_name"]
    },
    created_at: {
      formatter: _formatter.LocalizedDateFormatter
    }
  },
  TypedContext: {
    name: {
      formatter: (0, _recompose.mapProps)(function (_ref12) {
        var value = _ref12.value,
            onClick = _ref12.onClick;
        return {
          value: value && [{
            name: value.name,
            id: value.id,
            type: "TypedContext"
          }] || [],
          onClick: onClick
        };
      })(_formatter.LinkFormatter),
      projection: ["name", "id"]
    },
    $latestVersions: {
      formatter: (0, _formatter.withAttributeLoadingState)(_formatter.VersionWithStatusFormatter),
      options: {
        loading: "rollup"
      }
    },
    $linkedTo: {
      formatter: (0, _recompose.mapProps)(function (_ref13) {
        var value = _ref13.value,
            onClick = _ref13.onClick;
        return {
          value: value && value.link && value.link.slice(0, value.link.length - 1) || [],
          onClick: onClick
        };
      })(_formatter.LinkFormatter),
      projection: ["link"]
    },
    link: {
      formatter: _formatter.LinkFormatter
    },
    _link: {
      formatter: _formatter.LinkFormatter
    },
    $assignees: {
      projection: ["assignments.resource.first_name", "assignments.resource.last_name"],
      formatter: (0, _recompose.mapProps)(function (_ref14) {
        var value = _ref14.value;
        return {
          names: value && value.assignments ? value.assignments.map(function (item) {
            return item.resource;
          }) : null
        };
      })(_formatter.NamesFormatter)
    },
    bid: {
      formatter: _formatter.BidTimeFormatter
    },
    time_logged: {
      formatter: _formatter.BidTimeFormatter
    },
    bid_time_logged_difference: {
      formatter: _formatter.BidTimeFormatter
    },
    start_date: {
      formatter: _formatter.LocalizedDateFormatter
    },
    end_date: {
      formatter: _formatter.LocalizedDateFormatter
    },
    $scope: {
      projection: ["scopes.name"],
      formatter: (0, _recompose.mapProps)(function (_ref15) {
        var value = _ref15.value;
        return {
          value: value && value.scopes ? value.scopes.map(function (scope) {
            return scope.name;
          }).sort().join(", ") : null
        };
      })(_formatter.TextFormatter)
    },
    thumbnail_: {
      formatter: (0, _recompose.mapProps)(function (_ref16) {
        var value = _ref16.value,
            onClick = _ref16.onClick,
            height = _ref16.height;
        return {
          thumbnailId: value && value.thumbnail_id,
          onClick: onClick,
          height: height
        };
      })(_formatter.ThumbnailFormatter),
      projection: ["thumbnail_id"]
    },
    $type: {
      formatter: (0, _recompose.mapProps)(function (_ref17) {
        var value = _ref17.value;
        return {
          value: value && value.type ? value.type.name : null
        };
      })(_formatter.TextFormatter),
      projection: ["type.name"],
      sortOn: ["type.sort"]
    },
    $status: {
      formatter: (0, _recompose.mapProps)(function (_ref18) {
        var value = _ref18.value;
        return {
          name: value && value.status ? value.status.name : null,
          color: value && value.status ? value.status.color : null
        };
      })(_formatter.ColorLabelFormatter),
      projection: ["status.color", "status.name"],
      sortOn: ["status.sort"]
    },
    $priority: {
      formatter: (0, _recompose.mapProps)(function (_ref19) {
        var value = _ref19.value;
        return {
          name: value && value.priority ? value.priority.name : null,
          color: value && value.priority ? value.priority.color : null
        };
      })(_formatter.ColorLabelFormatter),
      projection: ["priority.color", "priority.name"],
      sortOn: ["priority.sort"]
    },
    $project: {
      formatter: (0, _recompose.mapProps)(function (_ref20) {
        var value = _ref20.value,
            onClick = _ref20.onClick;
        return {
          value: value && [{
            name: value.project.full_name,
            id: value.project.id,
            type: "Project"
          }] || [],
          onClick: onClick
        };
      })(_formatter.LinkFormatter),
      projection: ["project.full_name", "project.id"],
      sortOn: ["project.full_name"]
    },
    $objectType: {
      formatter: (0, _recompose.mapProps)(function (_ref21) {
        var value = _ref21.value;
        return {
          value: value && value.object_type ? value.object_type.name : null
        };
      })(_formatter.TextFormatter),
      projection: ["object_type.name"],
      sortOn: ["object_type.sort"]
    },
    $createdBy: {
      formatter: (0, _recompose.mapProps)(function (_ref22) {
        var value = _ref22.value;
        return {
          first: value && value.created_by ? value.created_by.first_name : null,
          last: value && value.created_by ? value.created_by.last_name : null
        };
      })(_formatter.FullnameFormatter),
      projection: ["created_by.first_name", "created_by.last_name"]
    },
    $lists: {
      formatter: (0, _recompose.mapProps)(function (_ref23) {
        var value = _ref23.value,
            onClick = _ref23.onClick;
        return {
          values: (value === null || value === void 0 ? void 0 : value.lists) || [],
          onClick: onClick
        };
      })(_formatter.ListsFormatter),
      projection: ["lists.name", "lists.id"]
    }
  },
  User: {
    thumbnail_: {
      formatter: (0, _recompose.mapProps)(function (_ref24) {
        var value = _ref24.value,
            height = _ref24.height;
        return {
          user: {
            thumbnail_id: value.thumbnail_id,
            first_name: value.first_name,
            last_name: value.last_name
          },
          height: height
        };
      })(_formatter.AvatarThumbnailFormatter),
      projection: ["thumbnail_id", "first_name", "last_name"]
    },
    $user: {
      formatter: (0, _recompose.mapProps)(function (_ref25) {
        var value = _ref25.value,
            onClick = _ref25.onClick;
        return {
          value: value && [{
            name: "".concat(value.first_name, " ").concat(value.last_name),
            id: value.id,
            type: "User"
          }] || [],
          onClick: onClick
        };
      })(_formatter.LinkFormatter),
      projection: ["first_name", "last_name", "id"]
    }
  },
  Project: {
    link: {
      formatter: _formatter.LinkFormatter
    },
    $project_schema_name: {
      formatter: (0, _recompose.mapProps)(function (_ref26) {
        var value = _ref26.value;
        return {
          value: value && value.project_schema ? value.project_schema.name : null
        };
      })(_formatter.TextFormatter),
      projection: ["project_schema.name"]
    },
    $managers: {
      projection: ["managers.user.first_name", "managers.user.last_name"],
      formatter: (0, _recompose.mapProps)(function (_ref27) {
        var value = _ref27.value;
        return {
          names: value && value.managers ? value.managers.map(function (item) {
            return item.user;
          }) : null
        };
      })(_formatter.NamesFormatter)
    },
    $scope: {
      projection: ["scopes.name"],
      formatter: (0, _recompose.mapProps)(function (_ref28) {
        var value = _ref28.value;
        return {
          value: value && value.scopes ? value.scopes.map(function (scope) {
            return scope.name;
          }).sort().join(", ") : null
        };
      })(_formatter.TextFormatter)
    },
    thumbnail_: {
      formatter: (0, _recompose.mapProps)(function (_ref29) {
        var value = _ref29.value,
            onClick = _ref29.onClick,
            height = _ref29.height;
        return {
          thumbnailId: value && value.thumbnail_id,
          onClick: onClick,
          height: height
        };
      })(_formatter.ThumbnailFormatter),
      projection: ["thumbnail_id"]
    },
    color: {
      formatter: _formatter.ColorFormatter
    },
    name: {
      formatter: (0, _recompose.mapProps)(function (_ref30) {
        var value = _ref30.value,
            onClick = _ref30.onClick;
        return {
          value: value && [{
            name: value.name,
            id: value.id,
            type: "Project"
          }] || [],
          onClick: onClick
        };
      })(_formatter.LinkFormatter),
      projection: ["name", "id"]
    },
    status: {
      formatter: _formatter.SentenceCaseTextFormatter
    },
    $createdBy: {
      formatter: (0, _recompose.mapProps)(function (_ref31) {
        var value = _ref31.value;
        return {
          first: value && value.created_by ? value.created_by.first_name : null,
          last: value && value.created_by ? value.created_by.last_name : null
        };
      })(_formatter.FullnameFormatter),
      projection: ["created_by.first_name", "created_by.last_name"]
    }
  },
  Timelog: {
    duration: {
      formatter: _formatter.SecondToHoursFormatter
    }
  },
  ReviewSession: {
    $reviewSession: {
      formatter: (0, _recompose.mapProps)(function (_ref32) {
        var value = _ref32.value,
            onClick = _ref32.onClick;
        return {
          value: value && [{
            name: value.name,
            id: value.id,
            type: "ReviewSession"
          }] || [],
          onClick: onClick
        };
      })(_formatter.LinkFormatter),
      projection: ["name", "id"]
    },
    thumbnail_: {
      formatter: (0, _recompose.compose)((0, _recompose.mapProps)(function (_ref33) {
        var value = _ref33.value,
            onClick = _ref33.onClick,
            height = _ref33.height;
        return {
          thumbnailId: value && value.thumbnail_id,
          onClick: onClick,
          height: height
        };
      }), (0, _recompose.withProps)({
        disableThumbnailClick: true
      }))(_formatter.ThumbnailFormatter),
      projection: ["thumbnail_id"]
    },
    $project: {
      formatter: (0, _recompose.mapProps)(function (_ref34) {
        var value = _ref34.value,
            onClick = _ref34.onClick;
        return {
          value: value && [{
            name: value.project.full_name,
            id: value.project.id,
            type: "Project"
          }] || [],
          onClick: onClick
        };
      })(_formatter.LinkFormatter),
      projection: ["project.full_name", "project.id"],
      sortOn: ["project.full_name"]
    },
    $createdBy: {
      formatter: (0, _recompose.mapProps)(function (_ref35) {
        var value = _ref35.value;
        return {
          first: value && value.created_by ? value.created_by.first_name : null,
          last: value && value.created_by ? value.created_by.last_name : null
        };
      })(_formatter.FullnameFormatter),
      projection: ["created_by.first_name", "created_by.last_name"]
    },
    created_at: {
      formatter: _formatter.LocalizedDateFormatter
    }
  }
};
exports.ATTRIBUTE_OVERRIDES = ATTRIBUTE_OVERRIDES;

function getCustomAttributeLinksProperties(entityTypeTo) {
  var properties = {
    projections: [],
    entityType: ""
  }; // TODO: Combine this with formatter/entityUtils.js

  switch (entityTypeTo) {
    case "user":
      properties.entityType = "User";
      properties.projections = _entityUtils.Entity.User.linkProjection;
      properties.linkRelation = _entityUtils.Entity.User.linkRelation;
      properties.formatter = ATTRIBUTE_OVERRIDES.User.$user.formatter;

      properties.getDisplayData = function (value) {
        return value.user && {
          type: value.user.__entity_type__,
          id: value.user.id,
          first_name: value.user.first_name,
          last_name: value.user.last_name
        };
      };

      break;

    case "asset_version":
      properties.entityType = "AssetVersion";
      properties.projections = _entityUtils.Entity.AssetVersion.linkProjection;
      properties.linkRelation = _entityUtils.Entity.AssetVersion.linkRelation;
      properties.formatter = (0, _recompose.withProps)({
        shortLabel: true
      })(_formatter.VersionWithStatusFormatter); // Refactor the VersionWithStatusFormatter, currently it operates on an array but
      // perhaps we should instead have it work on a single version?

      properties.getDisplayData = function (value) {
        return [value.asset_version];
      };

      break;

    case "list":
      properties.entityType = "List";
      properties.projections = _entityUtils.Entity.List.linkProjection;
      properties.linkRelation = _entityUtils.Entity.List.linkRelation;
      properties.formatter = (0, _recompose.mapProps)(function (_ref36) {
        var value = _ref36.value,
            onClick = _ref36.onClick;
        return {
          value: value.project && value && [{
            name: value.project.full_name,
            id: value.project.id,
            type: "Project"
          }, {
            name: value.name,
            id: value.id,
            type: "List"
          }] || [],
          onClick: onClick,
          shortLabel: true
        };
      })(_formatter.LinkFormatter);

      properties.getDisplayData = function (value) {
        return value.list;
      };

      break;

    case "group":
      properties.entityType = "Group";
      properties.projections = _entityUtils.Entity.Group.linkProjection;
      properties.linkRelation = _entityUtils.Entity.Group.linkRelation;
      properties.formatter = (0, _recompose.withProps)({
        shortLabel: true,
        onClick: null
      })(_formatter.LinkFormatter);

      properties.getDisplayData = function (value) {
        return value.group && value.group.link;
      };

      break;

    case "asset":
      properties.entityType = "Asset";
      properties.projections = _entityUtils.Entity.Asset.linkProjection;
      properties.linkRelation = _entityUtils.Entity.Asset.linkRelation;
      properties.formatter = _formatter.AssetFormatter;

      properties.getDisplayData = function (value) {
        return value.asset;
      };

      break;

    case "task":
      properties.entityType = "TypedContext";
      properties.projections = _entityUtils.Entity.TypedContext.linkProjection;
      properties.linkRelation = _entityUtils.Entity.TypedContext.linkRelation;
      properties.formatter = (0, _recompose.withProps)({
        shortLabel: true
      })(_formatter.LinkFormatter);

      properties.getDisplayData = function (value) {
        return value.context && value.context.link;
      };

      break;

    case "show":
      properties.entityType = "Project";
      properties.projections = _entityUtils.Entity.Project.linkProjection;
      properties.linkRelation = _entityUtils.Entity.Project.linkRelation;
      properties.formatter = (0, _recompose.withProps)({
        shortLabel: true
      })(_formatter.LinkFormatter);

      properties.getDisplayData = function (value) {
        return value.context && value.context.link;
      };

      break;

    case "context":
      properties.entityType = "Context";
      properties.projections = _entityUtils.Entity.Context.linkProjection;
      properties.linkRelation = _entityUtils.Entity.Context.linkRelation;
      properties.formatter = (0, _recompose.withProps)({
        shortLabel: true
      })(_formatter.LinkFormatter);

      properties.getDisplayData = function (value) {
        return value.context && value.context.link;
      };

      break;

    default:
      properties.projections = _entityUtils.Entity.Context.linkProjection;
      properties.linkRelation = _entityUtils.Entity.Context.linkRelation;
      properties.formatter = (0, _recompose.withProps)({
        shortLabel: true
      })(_formatter.LinkFormatter);

      properties.getDisplayData = function (value) {
        return value.context && value.context.link;
      };

      break;
  }

  return properties;
}

function attachCustomAttributeLinkData(customLinkResult, model, objectTypeId) {
  return customLinkResult.data.map(function (link) {
    var currentType = getCustomAttributeLinksProperties(link.entity_type_to).entityType;
    var isIncomingLink = currentType === model; // if there is a subtype, check that it matches too

    if (link.object_type_id_to) {
      isIncomingLink = link.object_type_id_to === objectTypeId;
    }

    var objectTypeName = isIncomingLink ? link.object_type && link.object_type.name : link.object_type_to && link.object_type_to.name;
    var customAttributeLinkProperties = getCustomAttributeLinksProperties(isIncomingLink ? link.entity_type : link.entity_type_to);

    if (isIncomingLink) {
      customAttributeLinkProperties.linkRelation = "".concat(customAttributeLinkProperties.linkRelation, "From");
    }

    function buildCustomAttributeLinkProperty(isIncoming) {
      return _objectSpread(_objectSpread({}, link), {}, {
        label: isIncoming ? "".concat(objectTypeName || customAttributeLinkProperties.entityType, " (from ").concat(link.label, ")") : link.label,
        type: {
          name: "link"
        },
        options: _objectSpread({
          isIncomingLink: isIncoming,
          loading: "custom_attribute_links",
          configuration_id: link.id,
          entity_type: link.entity_type,
          entity_type_to: link.entity_type_to,
          object_type_id: link.object_type_id,
          object_type_id_to: link.object_type_id_to,
          objectTypeId: isIncoming ? link.object_type_id : link.object_type_id_to,
          objectTypeName: objectTypeName
        }, customAttributeLinkProperties)
      });
    }

    var incomingAndOutgoingIsSameEntity = link.entity_type === link.entity_type_to;

    if (link.entity_type_to === "task") {
      incomingAndOutgoingIsSameEntity = link.object_type_id_to === link.object_type_id;
    } // Since we want to include both the from and to direction
    // in the case where incoming and outgoing are the same entity,
    // but only the incoming link when they are not the same, we are
    // returning an array that we later flatten.


    return incomingAndOutgoingIsSameEntity ? [buildCustomAttributeLinkProperty(true), buildCustomAttributeLinkProperty(false)] : [buildCustomAttributeLinkProperty(isIncomingLink)];
  }).flat();
}

function buildCustomAttributes(relation, path, configurations, purpose) {
  var _this = this;

  var attributes = [];
  configurations.forEach(function (configuration) {
    var config;

    try {
      config = JSON.parse(configuration.config);
    } catch (error) {
      config = {};
    }

    var type = {
      date: "date-time",
      text: "string"
    }[configuration.type.name] || configuration.type.name;
    var keyPrefix = CUSTOM_ATTRIBUTE_KEY_PREFIX;

    if (type === "link") {
      keyPrefix = configuration.options.isIncomingLink ? CUSTOM_ATTRIBUTE_LINK_FROM_KEY_PREFIX : CUSTOM_ATTRIBUTE_LINK_TO_KEY_PREFIX;
    }

    var attribute = {
      label: configuration.label,
      key: relation ? "".concat(relation, ".").concat(keyPrefix).concat(configuration.id) : "".concat(keyPrefix).concat(configuration.id),
      path: path,
      type: type,
      relation: relation,
      customAttributeGroupName: configuration.group && configuration.group.name || null
    };

    if (purpose === "filters") {
      attribute = _objectSpread(_objectSpread({}, attribute), {}, {
        config: config,
        filterOperators: getFilterOperators(type)
      });

      if (type === "enumerator") {
        attribute.filterCondition = function (operator, value) {
          if (operator === "set") {
            return 'value is_not ""';
          }

          if (operator === "not_set") {
            return 'value is ""';
          }

          var encodedValue = (0, _isArray.default)(value) ? encodeArrayCondition(value) : value;
          return "value ".concat(operator, " (").concat(encodedValue, ")");
        };
      }

      if (type === "link") {
        attribute.options = configuration.options;
        var direction = configuration.options.isIncomingLink ? "from_id" : "to_id";

        attribute.filterCondition = function (operator, value) {
          var values = encodeArrayCondition(value);

          if (operator === "not_in") {
            return "".concat(direction, " not_in (").concat(values, ")");
          }

          return "".concat(direction, " in (").concat(values, ")");
        };
      }
    } else {
      var defaultProjections = type === "link" ? configuration.options.projections : "custom_attributes";
      var defaultConfig = type === "link" ? configuration.options : config;
      attribute = _objectSpread(_objectSpread({}, attribute), {}, {
        projection: relation ? "".concat(relation, ".custom_attributes") : defaultProjections,
        width: 150,
        formatter: CustomAttributeFormatWrapper.bind(_this, configuration.id, type, defaultConfig),
        options: configuration.options
      });
    }

    attributes.push(attribute);
  });
  return attributes;
}

function getFormatter(property) {
  if (property.formatter) {
    return property.formatter;
  }

  if (property.type === "string" && property.format === "date-time") {
    return _formatter.DateFormatter;
  }

  if (property.type === "boolean") {
    return _formatter.BooleanFormatter;
  }

  return null;
}

function getPropertiesFromSchema(schema, purpose) {
  var path = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
  var objectType = arguments.length > 3 ? arguments[3] : undefined;
  var schemaOverrides;

  if (purpose === "filters") {
    schemaOverrides = FILTER_OVERRIDES;
  } else if (purpose === "attributes") {
    schemaOverrides = ATTRIBUTE_OVERRIDES;
  } else {
    throw new Error("Purpose must be either filters or attributes. Got: ".concat(purpose, "."));
  }

  var properties = Object.assign({}, schema.properties);
  var override = schemaOverrides[schema.id];

  if (schema.alias_for && schema.alias_for.id === "Task") {
    // Pick overrides from typed context.
    override = schemaOverrides.TypedContext;
  }

  Object.keys(override || []).forEach(function (key) {
    properties[key] = Object.assign({}, properties[key] || {}, override[key]);
  });
  var attributes = Object.keys(properties).reduce(function (accumulator, key) {
    var property = properties[key];
    var pathKeys = path.map(function (item) {
      return item.key;
    });
    var fullKey = [].concat(_toConsumableArray(pathKeys), [objectType === null || objectType === void 0 ? void 0 : objectType.id, key]).filter(function (keyPart) {
      return keyPart;
    }).join(".");
    var projection = fullKey;

    if (property.projection) {
      projection = property.projection.map(function (item) {
        return [].concat(_toConsumableArray(pathKeys), [item]).join(".");
      });
    }

    var type = property.format === "date-time" ? property.format : property.type;
    var relation = pathKeys.join(".");
    var sortOn = property.sortOn;

    if (sortOn && relation.length) {
      // The attribute should be sorted on something else than the
      // default projection.
      sortOn = property.sortOn.map(function (item) {
        return "".concat(relation, ".").concat(item);
      });
    }

    var attribute = {
      key: fullKey,
      model: schema.id,
      path: path,
      relation: relation,
      icon: property.icon,
      sortOn: sortOn,
      type: type,
      options: property.options
    };

    if (purpose === "filters") {
      Object.assign(attribute, {
        filterOperators: property.filterOperators || getFilterOperators(type),
        filterComponent: property.filterComponent,
        filterCondition: property.filterCondition,
        groupLabel: property.groupLabel
      });
    } else {
      Object.assign(attribute, {
        projection: projection,
        width: property.width,
        options: property.options,
        formatter: getFormatter(property)
      });
    }

    if (property.$ref) {
      accumulator.single.push(attribute);
    } else if (property.items) {
      accumulator.collection.push(attribute);
    } else {
      accumulator.scalar.push(attribute);
    }

    return accumulator;
  }, {
    scalar: [],
    single: [],
    collection: []
  });
  return attributes;
}

function getProperties(model, relations, session, purpose) {
  var schemas = session.schemas;
  var schema = schemas.find(function (item) {
    return item.id === model;
  });
  var supportedCustomAttributeTypes = ["text", "date", "enumerator", "number", "boolean", "link"];

  if (purpose === "attributes") {
    supportedCustomAttributeTypes.push("dynamic enumerator");
  }

  var operations = [_ftrackJavascriptApi.operation.query("select id, name, is_prioritizable, is_time_reportable, is_taskable, is_typeable, " + "is_statusable, is_schedulable, is_leaf from ObjectType")];

  if (schema.properties.custom_attributes) {
    operations.push(_ftrackJavascriptApi.operation.query("select label, key, type.name, entity_type, config, object_type_id, group.name " + "from CustomAttributeConfiguration where project_id is None and " + "is_hierarchical is False order by label"));
  }

  if (schema.properties.custom_attribute_links) {
    operations.push(_ftrackJavascriptApi.operation.query("select label, key, object_type_id, group.name, entity_type, entity_type_to, object_type_id_to, object_type.name, object_type_to.name " + "from CustomAttributeLinkConfiguration where project_id is None " + "order by label"));
  }

  return session.call(operations).then(function (_ref37) {
    var _ref38 = _slicedToArray(_ref37, 3),
        objectTypesResult = _ref38[0],
        customAttributesResult = _ref38[1],
        _ref38$ = _ref38[2],
        customLinkResult = _ref38$ === void 0 ? {
      data: []
    } : _ref38$;

    var ownObjectType = objectTypesResult.data.find(function (object) {
      return (0, _schema_operations.getObjectTypeSchemaId)(schemas, object.id) === model;
    });
    var ownAttributes = getPropertiesFromSchema(schema, purpose).scalar;
    ownAttributes.forEach(function (attribute) {
      // Rename top-level "select objects" to e.g. "Select shot"
      // and add object type information to entity-picker filter.
      if (attribute.key.endsWith("$entities")) {
        attribute.groupLabel = ownObjectType === null || ownObjectType === void 0 ? void 0 : ownObjectType.name;
        attribute.options = attribute.options || {};
        attribute.options = _objectSpread(_objectSpread({}, attribute.options), {}, {
          objectTypeId: ownObjectType === null || ownObjectType === void 0 ? void 0 : ownObjectType.id,
          objectTypeName: ownObjectType === null || ownObjectType === void 0 ? void 0 : ownObjectType.name,
          objectTypeSchemaId: model
        });
      }
    });
    var groups = [{
      label: model,
      key: model,
      model: model,
      path: [],
      relation: null,
      attributes: ownAttributes
    }];
    var relationGroups = relations.map(function (relation) {
      return buildRelationalGroup(relation, schemas, schema, model, purpose);
    });

    if (purpose === "filters" && (model === "AssetVersion" || (0, _schema_operations.default)(model, schemas))) {
      objectTypesResult.data.filter(function (objectType) {
        return !objectType.is_leaf && (0, _schema_operations.getObjectTypeSchemaId)(schemas, objectType.id) !== model;
      }).forEach(function (objectType) {
        var relation = model === "AssetVersion" ? "asset.ancestors" : "ancestors";
        var genericGroup = buildRelationalGroup("".concat(relation, "[TypedContext]"), schemas, schema, model, purpose, objectType);
        var ObjectTypeSchemaId = (0, _schema_operations.getObjectTypeSchemaId)(schemas, objectType.id);
        genericGroup.key = "".concat(model, ".").concat(relation, "[").concat(ObjectTypeSchemaId, "]");
        genericGroup.objectType = _objectSpread({}, objectType);
        relationGroups.push(genericGroup);
      });
    }

    groups.push.apply(groups, _toConsumableArray(relationGroups));
    var FILTER_CONSTRAINTS = {
      status_id: "is_statusable",
      $status: "is_statusable",
      type_id: "is_typeable",
      $type: "is_typeable",
      $assignees: "is_leaf",
      bid: "is_leaf",
      time_logged: "is_leaf",
      bid_time_logged_difference: "is_leaf",
      start_date: "is_schedulable",
      end_date: "is_schedulable",
      $priority: "is_prioritizable",
      priority_id: "is_prioritizable"
    };
    groups.forEach(function (group) {
      var targetSchema = schemas.find(function (item) {
        return item.id === group.model;
      });
      var objectTypeId = targetSchema.alias_for && targetSchema.alias_for.classifiers && targetSchema.alias_for.classifiers.object_typeid;

      if (objectTypeId || group.objectType) {
        var objectTypeConfig = group.objectType ? group.objectType : objectTypesResult.data.find(function (candidate) {
          return candidate.id === objectTypeId;
        });

        if (objectTypeConfig) {
          group.attributes = group.attributes.filter(function (attribute) {
            // For related objects key is not same as attribute
            var candidateKey = group.path.length ? attribute.key.split(".").pop() : attribute.key; // Remove object type filter when there is an implicit filter.

            if (candidateKey === "$objectType" || candidateKey === "object_type_id") {
              return false;
            }

            var constraint = FILTER_CONSTRAINTS[candidateKey];

            if (constraint && objectTypeConfig[constraint] === false) {
              // The object type does not allow for the attribute.
              return false;
            }

            return true;
          });
        }
      } else if (group.path.length && targetSchema.id === "TypedContext") {
        // Handle attribute filter for related TypedContext
        group.attributes = group.attributes.filter(function (attribute) {
          var candidateKey = attribute.key.split(".").pop();
          var constraint = FILTER_CONSTRAINTS[candidateKey];

          if (objectTypesResult.data.filter(function (objectType) {
            return !objectType.is_leaf;
          }).some(function (objectType) {
            return objectType[constraint];
          }) || !constraint) {
            return true;
          }

          return false;
        });
      }

      if (schema.properties.custom_attributes || schema.properties.custom_attribute_links) {
        var combinedResult = [].concat(_toConsumableArray(attachCustomAttributeLinkData(customLinkResult, model, objectTypeId)), _toConsumableArray(customAttributesResult.data));
        var items = combinedResult.filter(function (item) {
          var entityTypes = [item.entity_type, item.entity_type_to];

          if (supportedCustomAttributeTypes.indexOf(item.type.name) === -1) {
            return false;
          }

          if (objectTypeId) {
            return [item.object_type_id, item.object_type_id_to].includes(targetSchema.alias_for.classifiers.object_typeid);
          }

          if (group.objectType) {
            return [item.object_type_id, item.object_type_id_to].includes(group.objectType.id);
          }

          if (group.model === "Asset" && entityTypes.includes("asset")) {
            return true;
          }

          if (group.model === "Project" && entityTypes.includes("show")) {
            return true;
          }

          if (group.model === "AssetVersion" && (entityTypes.includes("assetversion") || entityTypes.includes("asset_version"))) {
            return true;
          }

          if (group.model === "User" && entityTypes.includes("user")) {
            return true;
          }

          if (group.model === "List" && entityTypes.includes("list")) {
            return true;
          }

          return false;
        });
        var customAttributes = buildCustomAttributes(group.relation, group.path, items, purpose);
        group.attributes = [].concat(_toConsumableArray(group.attributes), _toConsumableArray(customAttributes));
      }
    });
    return Promise.resolve({
      groups: groups,
      attributes: groups.reduce(function (accumulator, group) {
        group.attributes.forEach(function (attribute) {
          // accumulator[attribute.key] and attribute needs to point to the
          // same object beacuse we modify group attribute in other places
          // and expect then changes to be carried over to attributes.
          accumulator[attribute.key] = attribute;
        });
        return accumulator;
      }, {})
    });
  });
}

function getFilters(model, relations, session) {
  return getProperties(model, relations, session, "filters");
}

function getAttributes(model, relations, session) {
  return getProperties(model, relations, session, "attributes");
}