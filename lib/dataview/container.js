"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.reflect.construct.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.reloadData = reloadData;
exports.loadData = loadData;
exports.loadDataDone = loadDataDone;
exports.loadDataFailed = loadDataFailed;
exports.loadDataCancelled = loadDataCancelled;
exports.sortData = sortData;
exports.loadDataItem = loadDataItem;
exports.loadDataItemDone = loadDataItemDone;
exports.loadDelayedAttributeDone = loadDelayedAttributeDone;
exports.loadDelayedDataHandler = loadDelayedDataHandler;
exports.loadDelayedItemDataHandler = loadDelayedItemDataHandler;
exports.loadDataSaga = loadDataSaga;
exports.reloadDataSaga = reloadDataSaga;
exports.loadDataItemSaga = loadDataItemSaga;
exports.loadDelayedDataSaga = loadDelayedDataSaga;
exports.loadDelayedItemDataSaga = loadDelayedItemDataSaga;
exports.reducer = reducer;
exports.withDataLoader = withDataLoader;
exports.sagas = exports.actions = void 0;

require("regenerator-runtime/runtime.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.string.starts-with.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.regexp.split.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.string.includes.js");

require("core-js/modules/es7.array.includes.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.sort.js");

require("core-js/modules/es6.array.find-index.js");

require("core-js/modules/es6.object.assign.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactRedux = require("react-redux");

var _reselect = require("reselect");

var _debounce = _interopRequireDefault(require("lodash/debounce"));

var _effects = require("redux-saga/effects");

var _reduxSaga = require("redux-saga");

var _loglevel = _interopRequireDefault(require("loglevel"));

var _isArray = _interopRequireDefault(require("lodash/isArray"));

var _isEqual = _interopRequireDefault(require("lodash/isEqual"));

var _uniq = _interopRequireDefault(require("lodash/uniq"));

var _flatten = _interopRequireDefault(require("lodash/flatten"));

var _cloneDeep = _interopRequireDefault(require("lodash/cloneDeep"));

var _zip = _interopRequireDefault(require("lodash/zip"));

var _ftrackJavascriptApi = require("ftrack-javascript-api");

var _util = require("./util");

var _symbol = require("./symbol");

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["wrapComponent", "data"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(reloadDataHandler),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(resolveRollup),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(resolveCustomAttributeLinks),
    _marked4 = /*#__PURE__*/regeneratorRuntime.mark(fetchData),
    _marked5 = /*#__PURE__*/regeneratorRuntime.mark(loadDataItemHandler),
    _marked6 = /*#__PURE__*/regeneratorRuntime.mark(loadDataHandler),
    _marked7 = /*#__PURE__*/regeneratorRuntime.mark(loadDelayedData),
    _marked8 = /*#__PURE__*/regeneratorRuntime.mark(loadDelayedDataHandler),
    _marked9 = /*#__PURE__*/regeneratorRuntime.mark(loadDelayedItemDataHandler),
    _marked10 = /*#__PURE__*/regeneratorRuntime.mark(loadDataSaga),
    _marked11 = /*#__PURE__*/regeneratorRuntime.mark(reloadDataSaga),
    _marked12 = /*#__PURE__*/regeneratorRuntime.mark(loadDataItemSaga),
    _marked13 = /*#__PURE__*/regeneratorRuntime.mark(loadDelayedDataSaga),
    _marked14 = /*#__PURE__*/regeneratorRuntime.mark(loadDelayedItemDataSaga);

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var DELAY_FILTER_CHANGED_LOAD_DATA = 500;
var LOAD_DATA = "LOAD_DATA";
var RELOAD_DATA = "RELOAD_DATA";
var LOAD_DATA_DONE = "LOAD_DATA_DONE";
var LOAD_DATA_FAILED = "LOAD_DATA_FAILED";
var LOAD_DATA_CANCELLED = "LOAD_DATA_CANCELLED";
var SORT_DATA = "SORT_DATA";
var LOAD_DATA_ITEM = "LOAD_DATA_ITEM";
var LOAD_DATA_ITEM_DONE = "LOAD_DATA_ITEM_DONE";
var LOAD_DELAYED_ATTRIBUTE_DONE = "LOAD_DELAYED_ATTRIBUTE_DONE";

function reloadData(payload) {
  return {
    type: RELOAD_DATA,
    payload: payload
  };
}

function loadData(payload) {
  return {
    type: LOAD_DATA,
    payload: payload
  };
}

function loadDataDone(payload) {
  return {
    type: LOAD_DATA_DONE,
    payload: payload
  };
}

function loadDataFailed(payload) {
  return {
    type: LOAD_DATA_FAILED,
    payload: payload
  };
}

function loadDataCancelled(payload) {
  return {
    type: LOAD_DATA_CANCELLED,
    payload: payload
  };
}

function sortData(payload) {
  return {
    type: SORT_DATA,
    payload: payload
  };
}

function loadDataItem(payload) {
  return {
    type: LOAD_DATA_ITEM,
    payload: payload
  };
}

function loadDataItemDone(payload) {
  return {
    type: LOAD_DATA_ITEM_DONE,
    payload: payload
  };
}

function loadDelayedAttributeDone(payload) {
  return {
    type: LOAD_DELAYED_ATTRIBUTE_DONE,
    payload: payload
  };
}

var actions = {
  LOAD_DATA: LOAD_DATA,
  LOAD_DELAYED_ATTRIBUTE_DONE: LOAD_DELAYED_ATTRIBUTE_DONE,
  LOAD_DATA_DONE: LOAD_DATA_DONE,
  LOAD_DATA_CANCELLED: LOAD_DATA_CANCELLED,
  LOAD_DATA_FAILED: LOAD_DATA_FAILED,
  RELOAD_DATA: RELOAD_DATA,
  SORT_DATA: SORT_DATA,
  LOAD_DATA_ITEM: LOAD_DATA_ITEM,
  LOAD_DATA_ITEM_DONE: LOAD_DATA_ITEM_DONE
}; // eslint-disable-next-line

exports.actions = actions;

function processData(data, projections) {
  var path = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";

  if (!data) {
    return data;
  }

  Object.keys(data).forEach(function (key) {
    var fullPath = "".concat(path).concat(key);

    if (projections.some(function (projection) {
      return projection.startsWith("".concat(fullPath, "."));
    })) {
      // Partial path found, continue traverse.
      if ((0, _isArray.default)(data[key])) {
        data[key].forEach(function (item) {
          return processData(item, projections, "".concat(fullPath, "."));
        });
      } else {
        processData(data[key], projections, "".concat(fullPath, "."));
      }
    } else if (!projections.some(function (projection) {
      return projection === fullPath;
    })) {
      delete data[key];
    }
  });
}

function buildQuery(model, attributes, sortAttribute, sortDirection, filters, offset, limit) {
  var query = "select ".concat(attributes.join(", "), " from ").concat(model);

  if (filters) {
    query += " where ".concat(filters);
  }

  if (sortAttribute) {
    if ((0, _isArray.default)(sortAttribute)) {
      query += " order by ".concat(sortAttribute.map(function (item) {
        return "".concat(item, " ").concat(sortDirection);
      }).join(", "));
    } else {
      query += " order by ".concat(sortAttribute, " ").concat(sortDirection);
    }
  }

  query += " offset ".concat(offset, " limit ").concat(limit);
  return query;
}

function reloadDataHandler(action) {
  var params;
  return regeneratorRuntime.wrap(function reloadDataHandler$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return (0, _effects.select)(function (state) {
            return state.params;
          });

        case 2:
          params = _context.sent;

          if (!params) {
            _context.next = 6;
            break;
          }

          _context.next = 6;
          return (0, _effects.put)(loadData(_objectSpread({
            model: params.model,
            sortAttribute: params.sortAttribute,
            sortDirection: params.sortDirection,
            attributes: params.attributes,
            filters: params.filters,
            loading: true,
            nextOffset: 0
          }, action.payload)));

        case 6:
        case "end":
          return _context.stop();
      }
    }
  }, _marked);
}

var processAttribute = function processAttribute(attribute, data) {
  var value;

  if (!(0, _isArray.default)(attribute.projection)) {
    var attributeFragments = attribute.projection.split(".");
    value = attributeFragments.reduce(function (accumulator, fragment) {
      if (accumulator !== undefined && accumulator !== null) {
        return accumulator[fragment];
      }

      return undefined;
    }, data);
  } else {
    // Process a multi projection attribute.
    // Inspect the key to find the relation.
    var relation = attribute.key.split(".");
    relation.pop(); // Traverse the relation to find the target value.

    value = relation.reduce(function (accumulator, fragment) {
      if (accumulator !== undefined && accumulator !== null) {
        return accumulator[fragment];
      }

      return undefined;
    }, data); // Loop projections and remove relation up to the attributes'
    // projection.

    var projections = [];
    attribute.projection.forEach(function (item) {
      var projectionFragments = item.split(".");
      projectionFragments.splice(0, relation.length);
      projections.push(projectionFragments.join("."));
    }); // Clone the value before removing all attributes that are not part
    // of the attribute.

    value = (0, _cloneDeep.default)(value); // Now clean the data to garantuee that no data is passed to the
    // formatter that was not asked for.

    processData(value, projections);
  }

  if (attribute.transform) {
    value = attribute.transform(value);
  }

  return value;
};
/** Resolve rollup on *attribute* for *entities* */


function resolveRollup(attribute, entities) {
  var session, response, data;
  return regeneratorRuntime.wrap(function resolveRollup$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return (0, _effects.getContext)("ftrackSession");

        case 2:
          session = _context2.sent;
          _context2.next = 5;
          return session.call([{
            action: "compute_rollups",
            data: entities,
            rollup: "latest_version"
          }]);

        case 5:
          response = _context2.sent;
          data = (0, _zip.default)(response[0].data, entities).map(function (_ref) {
            var _ref2 = _slicedToArray(_ref, 2),
                attributeData = _ref2[0],
                entity = _ref2[1];

            return _objectSpread(_defineProperty({}, attribute.key, attributeData), entity);
          });
          _context2.next = 9;
          return (0, _effects.put)(loadDelayedAttributeDone({
            rows: data
          }));

        case 9:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2);
}

function resolveCustomAttributeLinks(attributes, entities) {
  var session, linkedEntities, operations, response, rows;
  return regeneratorRuntime.wrap(function resolveCustomAttributeLinks$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return (0, _effects.getContext)("ftrackSession");

        case 2:
          session = _context3.sent;
          linkedEntities = entities.map(function (e) {
            return "'".concat(e.__entity_id__, "'");
          }).join(",");

          if (!(linkedEntities && linkedEntities.length > 0)) {
            _context3.next = 12;
            break;
          }

          operations = attributes.map(function (_ref3) {
            var options = _ref3.options,
                projection = _ref3.projection;
            var isIncomingLink = options.isIncomingLink;
            var linkDirection = isIncomingLink ? "to_id" : "from_id";
            return _ftrackJavascriptApi.operation.query("\n                select from_id, to_id, configuration_id, ".concat(projection.join(","), "\n                from ").concat(options.linkRelation, "\n                where (").concat(linkDirection, " in (").concat(linkedEntities, "))\n                and configuration_id is '").concat(options.configuration_id, "'"));
          });
          _context3.next = 8;
          return session.call(operations);

        case 8:
          response = _context3.sent;
          // TODO: this is O(entities * attributes^2) and might explode CPU wise
          // when entities and attributes are too many, find a way to reduce the complexity
          rows = entities.map(function (entity) {
            return attributes.reduce(function (acc, _ref4, i) {
              var options = _ref4.options,
                  key = _ref4.key;
              var attributeResponse = response[i].data;
              var result = attributeResponse.filter(function (attribute) {
                var isMatchingConfigurationId = attribute.configuration_id === options.configuration_id;
                var isMatchingAttributeId = attribute.from_id === entity.__entity_id__;

                if (options.isIncomingLink) {
                  isMatchingAttributeId = attribute.to_id === entity.__entity_id__;
                }

                return isMatchingAttributeId && isMatchingConfigurationId;
              });
              return _objectSpread(_objectSpread({}, acc), {}, _defineProperty({}, key, result));
            }, entity);
          });
          _context3.next = 12;
          return (0, _effects.put)(loadDelayedAttributeDone({
            rows: rows
          }));

        case 12:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3);
}
/** Fetch data from server. */


function fetchData(attributes, model, sortAttribute, sortDirection, filters) {
  var offset,
      limit,
      session,
      batches,
      mainBatch,
      delayedAttributes,
      operations,
      primaryKeyNames,
      fetchResponse,
      responses,
      processedData,
      _args4 = arguments;
  return regeneratorRuntime.wrap(function fetchData$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          offset = _args4.length > 5 && _args4[5] !== undefined ? _args4[5] : 0;
          limit = _args4.length > 6 && _args4[6] !== undefined ? _args4[6] : 25;
          _context4.next = 4;
          return (0, _effects.getContext)("ftrackSession");

        case 4:
          session = _context4.sent;
          batches = [];
          mainBatch = {
            keys: [],
            projection: []
          };
          delayedAttributes = [];
          attributes.forEach(function (attribute) {
            if (attribute.options && attribute.options.loading === "split-batch") {
              batches.push({
                keys: [attribute.key],
                projection: (0, _isArray.default)(attribute.projection) ? attribute.projection : [attribute.projection]
              });
            } else if (attribute.options && (attribute.options.loading === "rollup" || attribute.options.loading === "custom_attribute_links")) {
              // Do nothing, rollups are fetched separately.
              delayedAttributes.push(attribute.key);
            } else {
              var _mainBatch$projection;

              mainBatch.keys.push(attribute.key);

              (_mainBatch$projection = mainBatch.projection).push.apply(_mainBatch$projection, _toConsumableArray((0, _isArray.default)(attribute.projection) ? attribute.projection : [attribute.projection]));
            }
          });

          if (mainBatch.keys.length) {
            batches.push(mainBatch);
          }

          operations = batches.map(function (batch) {
            return _ftrackJavascriptApi.operation.query(buildQuery(model, batch.projection, sortAttribute, sortDirection, filters, offset, limit));
          });
          primaryKeyNames = session.getPrimaryKeyAttributes(model);

          _loglevel.default.debug("Query operations", operations);

          fetchResponse = session.call(operations);
          _context4.next = 16;
          return fetchResponse;

        case 16:
          responses = _context4.sent;

          _loglevel.default.debug("Query response", responses);

          processedData = responses[0].data.reduce(function (accumulator, row) {
            var data = {};
            data.__entity_id__ = primaryKeyNames.map(function (key) {
              return row[key];
            }).join(",");
            data.__entity_type__ = row.__entity_type__;
            accumulator.map[data.__entity_id__] = data;
            accumulator.rows.push(data);
            return accumulator;
          }, {
            rows: [],
            map: {}
          });
          responses.forEach(function (response, index) {
            response.data.forEach(function (row) {
              var entityId = primaryKeyNames.map(function (key) {
                return row[key];
              }).join(",");
              var processed = processedData.map[entityId];
              attributes.forEach(function (attribute) {
                if (batches[index].keys.includes(attribute.key)) {
                  processed[attribute.key] = processAttribute(attribute, row);
                }
              });
            });
          });
          delayedAttributes.forEach(function (key) {
            return processedData.rows.forEach(function (row) {
              row[key] = _symbol.ATTRIBUTE_LOADING;
            });
          });
          return _context4.abrupt("return", {
            rows: processedData.rows,
            nextOffset: responses[0].metadata.next.offset
          });

        case 22:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4);
}

function loadDataItemHandler(action) {
  var id, _yield$select, attributes, model, _yield$fetchData, _yield$fetchData$rows, row;

  return regeneratorRuntime.wrap(function loadDataItemHandler$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          id = action.payload.id;
          _context5.next = 3;
          return (0, _effects.select)(function (state) {
            return state.params;
          });

        case 3:
          _yield$select = _context5.sent;
          attributes = _yield$select.attributes;
          model = _yield$select.model;
          _context5.next = 8;
          return fetchData(attributes, model, null, null, "id is \"".concat(id, "\""), 0);

        case 8:
          _yield$fetchData = _context5.sent;
          _yield$fetchData$rows = _slicedToArray(_yield$fetchData.rows, 1);
          row = _yield$fetchData$rows[0];

          if (!row) {
            _context5.next = 16;
            break;
          }

          _context5.next = 14;
          return (0, _effects.put)(loadDataItemDone({
            row: row,
            attributes: attributes
          }));

        case 14:
          _context5.next = 17;
          break;

        case 16:
          _loglevel.default.debug("Could not load data item for ", action);

        case 17:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5);
}

function loadDataHandler(action) {
  var model, sortAttribute, sortDirection, attributes, offset, limit, discreet, isAppend, filters, _yield$fetchData2, rows, nextOffset, isCancelled;

  return regeneratorRuntime.wrap(function loadDataHandler$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          model = action.payload.model;
          sortAttribute = action.payload.sortAttribute;
          sortDirection = action.payload.sortDirection;
          attributes = action.payload.attributes;
          offset = action.payload.nextOffset;
          limit = action.payload.limit;
          discreet = action.payload.discreet;
          isAppend = offset > 0;
          filters = action.payload.filters;
          _context6.prev = 9;
          _context6.next = 12;
          return fetchData(attributes, model, sortAttribute, sortDirection, filters, offset, limit);

        case 12:
          _yield$fetchData2 = _context6.sent;
          rows = _yield$fetchData2.rows;
          nextOffset = _yield$fetchData2.nextOffset;
          _context6.next = 17;
          return (0, _effects.put)(loadDataDone({
            attributes: attributes,
            rows: rows,
            nextOffset: nextOffset,
            append: isAppend,
            discreet: discreet
          }));

        case 17:
          _context6.next = 24;
          break;

        case 19:
          _context6.prev = 19;
          _context6.t0 = _context6["catch"](9);

          _loglevel.default.error("Error when loading data", _context6.t0);

          _context6.next = 24;
          return (0, _effects.put)(loadDataFailed({
            append: isAppend,
            discreet: discreet,
            error: _context6.t0
          }));

        case 24:
          _context6.prev = 24;
          _context6.next = 27;
          return (0, _effects.cancelled)();

        case 27:
          isCancelled = _context6.sent;

          if (!isCancelled) {
            _context6.next = 31;
            break;
          }

          _context6.next = 31;
          return (0, _effects.put)(loadDataCancelled({
            append: isAppend,
            discreet: discreet
          }));

        case 31:
          return _context6.finish(24);

        case 32:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6, null, [[9, 19, 24, 32]]);
}

function loadDelayedData(attributes, rows) {
  var entities, delayed, delayedLinkAttributes;
  return regeneratorRuntime.wrap(function loadDelayedData$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _loglevel.default.debug("Handling action for delayed load of data", attributes, rows);

          entities = rows.map(function (_ref5) {
            var __entity_id__ = _ref5.__entity_id__,
                __entity_type__ = _ref5.__entity_type__;
            return {
              __entity_id__: __entity_id__,
              __entity_type__: __entity_type__
            };
          });
          delayed = attributes.filter(function (attribute) {
            return attribute.options && attribute.options.loading === "rollup";
          }).map(function (attribute) {
            return (0, _effects.call)(resolveRollup, attribute, entities);
          });
          delayedLinkAttributes = attributes.filter(function (attribute) {
            return attribute.options && attribute.options.loading === "custom_attribute_links";
          });

          if (delayedLinkAttributes.length > 0 && entities.length > 0) {
            delayed.push((0, _effects.call)(resolveCustomAttributeLinks, delayedLinkAttributes, entities));
          }

          _loglevel.default.debug("Delayed data identified", delayed);

          _context7.next = 8;
          return (0, _effects.race)({
            // The delayed load of data should cancel if the dataview reloads. This
            // is indicated by the nextOffset being set to 0.
            dataviewReloaded: (0, _effects.take)(function (loadDataAction) {
              return loadDataAction.type === actions.LOAD_DATA_DONE && loadDataAction.payload.nextOffset === 0;
            }),
            delayedDataDone: (0, _effects.fork)(_effects.all, delayed)
          });

        case 8:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7);
}

function loadDelayedDataHandler(action) {
  var _action$payload, attributes, rows;

  return regeneratorRuntime.wrap(function loadDelayedDataHandler$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _action$payload = action.payload, attributes = _action$payload.attributes, rows = _action$payload.rows;
          _context8.next = 3;
          return loadDelayedData(attributes, rows);

        case 3:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8);
}

function loadDelayedItemDataHandler(action) {
  var _action$payload2, attributes, row;

  return regeneratorRuntime.wrap(function loadDelayedItemDataHandler$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _action$payload2 = action.payload, attributes = _action$payload2.attributes, row = _action$payload2.row;

          if (!attributes) {
            _context9.next = 4;
            break;
          }

          _context9.next = 4;
          return loadDelayedData(attributes, [row]);

        case 4:
        case "end":
          return _context9.stop();
      }
    }
  }, _marked9);
}
/** Handle LOAD_DATA action. */


function loadDataSaga() {
  return regeneratorRuntime.wrap(function loadDataSaga$(_context10) {
    while (1) {
      switch (_context10.prev = _context10.next) {
        case 0:
          _context10.next = 2;
          return (0, _reduxSaga.takeLatest)(actions.LOAD_DATA, loadDataHandler);

        case 2:
        case "end":
          return _context10.stop();
      }
    }
  }, _marked10);
}
/** Handle RELOAD_DATA action. */


function reloadDataSaga() {
  return regeneratorRuntime.wrap(function reloadDataSaga$(_context11) {
    while (1) {
      switch (_context11.prev = _context11.next) {
        case 0:
          _context11.next = 2;
          return (0, _reduxSaga.takeLatest)(actions.RELOAD_DATA, reloadDataHandler);

        case 2:
        case "end":
          return _context11.stop();
      }
    }
  }, _marked11);
}
/** Handle LOAD_DATA_ITEM action. */


function loadDataItemSaga() {
  return regeneratorRuntime.wrap(function loadDataItemSaga$(_context12) {
    while (1) {
      switch (_context12.prev = _context12.next) {
        case 0:
          _context12.next = 2;
          return (0, _reduxSaga.takeEvery)(actions.LOAD_DATA_ITEM, loadDataItemHandler);

        case 2:
        case "end":
          return _context12.stop();
      }
    }
  }, _marked12);
}
/** Handle LOAD_DATA_DONE action and load delayed data. */


function loadDelayedDataSaga() {
  return regeneratorRuntime.wrap(function loadDelayedDataSaga$(_context13) {
    while (1) {
      switch (_context13.prev = _context13.next) {
        case 0:
          _context13.next = 2;
          return (0, _reduxSaga.takeEvery)(actions.LOAD_DATA_DONE, loadDelayedDataHandler);

        case 2:
        case "end":
          return _context13.stop();
      }
    }
  }, _marked13);
}
/** Handle LOAD_DATA_ITEM_DONE action and load delayed data. */


function loadDelayedItemDataSaga() {
  return regeneratorRuntime.wrap(function loadDelayedItemDataSaga$(_context14) {
    while (1) {
      switch (_context14.prev = _context14.next) {
        case 0:
          _context14.next = 2;
          return (0, _reduxSaga.takeEvery)(actions.LOAD_DATA_ITEM_DONE, loadDelayedItemDataHandler);

        case 2:
        case "end":
          return _context14.stop();
      }
    }
  }, _marked14);
}

var sagas = [loadDataSaga, reloadDataSaga, loadDataItemSaga, loadDelayedDataSaga, loadDelayedItemDataSaga];
exports.sagas = sagas;

function reducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var action = arguments.length > 1 ? arguments[1] : undefined;
  var nextState = state;

  if (action.type === actions.LOAD_DATA_DONE) {
    var rows = [];

    if (action.payload.append === true) {
      rows = [].concat(_toConsumableArray(state.rows), _toConsumableArray(action.payload.rows));
    } else {
      rows = action.payload.rows;
    }

    nextState = _objectSpread(_objectSpread({}, state), {}, {
      loading: false,
      rows: rows,
      nextOffset: action.payload.nextOffset
    });
  } else if (action.type === actions.LOAD_DATA) {
    nextState = _objectSpread(_objectSpread({}, state), {}, {
      params: {
        // Params are used to reload data.
        sortAttribute: action.payload.sortAttribute,
        sortDirection: action.payload.sortDirection,
        attributes: action.payload.attributes,
        filters: action.payload.filters,
        model: action.payload.model
      },
      model: action.payload.model,
      loading: action.payload.loading,
      rows: action.payload.nextOffset > 0 || action.payload.discreet ? state.rows : []
    });
  } else if (action.type === actions.SORT_DATA) {
    var sortOrder = action.payload.sortOrder;

    var _rows = _toConsumableArray(state.rows).sort(function (a, b) {
      return sortOrder.indexOf(a.__entity_id__) - sortOrder.indexOf(b.__entity_id__);
    });

    nextState = _objectSpread(_objectSpread({}, state), {}, {
      rows: _rows
    });
  } else if (action.type === actions.LOAD_DATA_ITEM_DONE) {
    var _action$payload3 = action.payload,
        newRow = _action$payload3.row,
        _action$payload3$refr = _action$payload3.refreshOnly,
        refreshOnly = _action$payload3$refr === void 0 ? false : _action$payload3$refr,
        _action$payload3$merg = _action$payload3.mergeChanges,
        mergeChanges = _action$payload3$merg === void 0 ? false : _action$payload3$merg;

    var _rows2 = _toConsumableArray(state.rows);

    var rowIndexToReplace = _rows2.findIndex(function (row) {
      return row.__entity_id__ === newRow.__entity_id__;
    });

    if (rowIndexToReplace !== -1 && mergeChanges) {
      _rows2[rowIndexToReplace] = Object.assign({}, _rows2[rowIndexToReplace], newRow);
    } else if (rowIndexToReplace !== -1) {
      _rows2[rowIndexToReplace] = newRow;
    } else if (!refreshOnly) {
      // Add new entity at the start of the list.
      _rows2.unshift(newRow);
    }

    nextState = _objectSpread(_objectSpread({}, state), {}, {
      rows: _rows2
    });
  } else if (action.type === actions.LOAD_DELAYED_ATTRIBUTE_DONE) {
    var updateRows = action.payload.rows;

    var _rows3 = _toConsumableArray(state.rows);

    updateRows.forEach(function (updateRow) {
      var index = _rows3.findIndex(function (row) {
        return row.__entity_id__ === updateRow.__entity_id__;
      }); // Prepare data and update the row.


      var newData = _objectSpread({}, updateRow);

      delete newData.__entity_id__;
      delete newData.__entity_type__;

      if (index !== -1) {
        _rows3[index] = _objectSpread(_objectSpread({}, _rows3[index]), updateRow);
      }
    });
    nextState = _objectSpread(_objectSpread({}, state), {}, {
      rows: _rows3
    });
  }

  return nextState;
}

var DataProvider = /*#__PURE__*/function (_Component) {
  _inherits(DataProvider, _Component);

  var _super = _createSuper(DataProvider);

  function DataProvider() {
    var _this;

    _classCallCheck(this, DataProvider);

    _this = _super.call(this);
    _this.onFetchData = _this.onFetchData.bind(_assertThisInitialized(_this));
    _this.onFetchMoreData = _this.onFetchMoreData.bind(_assertThisInitialized(_this));
    _this.handleSignificantPropsChanged = (0, _debounce.default)(_this.handleSignificantPropsChanged.bind(_assertThisInitialized(_this)), DELAY_FILTER_CHANGED_LOAD_DATA);
    return _this;
  }

  _createClass(DataProvider, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.handleComponentMount();
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(props) {
      var shouldReload = false;

      if (!(0, _isEqual.default)((0, _uniq.default)((0, _flatten.default)(this.props.attributes.map(function (attribute) {
        return attribute.key;
      }))).sort(), (0, _uniq.default)((0, _flatten.default)(props.attributes.map(function (attribute) {
        return attribute.key;
      }))).sort())) {
        shouldReload = true;
      } // TODO: Remove the use of the loader attributes as they are not used.
      // Current implementation only uses the attributes from props and not
      // props.loader.


      var oldAttributes = (0, _uniq.default)((0, _flatten.default)(this.props.loader.attributes)).sort();
      var newAttributes = (0, _uniq.default)((0, _flatten.default)(props.loader.attributes)).sort();

      if (!(0, _isEqual.default)(oldAttributes, newAttributes)) {
        shouldReload = true;
      }

      if (!(0, _isEqual.default)(props.loader.sortAttribute, this.props.loader.sortAttribute) || props.loader.sortDirection !== this.props.loader.sortDirection) {
        shouldReload = true;
      }

      if (props.loader.filters !== this.props.loader.filters) {
        shouldReload = true;
      }

      if (shouldReload) {
        this.handleSignificantPropsChanged(props.loader);
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.props.onClearData();
    }
  }, {
    key: "onFetchMoreData",
    value: function onFetchMoreData() {
      if (!this.props.loading) {
        this.onFetchData();
      }
    }
  }, {
    key: "onFetchData",
    value: function onFetchData(params) {
      var _ref6 = params || {},
          newModel = _ref6.model,
          newSortAttribute = _ref6.sortAttribute,
          newSortDirection = _ref6.sortDirection,
          newNextOffset = _ref6.nextOffset,
          discreet = _ref6.discreet;

      var _this$props$loader = this.props.loader,
          model = _this$props$loader.model,
          sortAttribute = _this$props$loader.sortAttribute,
          sortDirection = _this$props$loader.sortDirection,
          nextOffset = _this$props$loader.nextOffset,
          filters = _this$props$loader.filters,
          pageSize = _this$props$loader.pageSize;
      var offset = newNextOffset !== undefined ? newNextOffset : nextOffset;

      if (offset === null) {
        return;
      }

      this.props.onFetchData(newModel !== undefined ? newModel : model, newSortAttribute !== undefined ? newSortAttribute : sortAttribute, newSortDirection !== undefined ? newSortDirection : sortDirection, filters, discreet || false, offset, pageSize);
    }
  }, {
    key: "handleComponentMount",
    value: function handleComponentMount() {
      var loader = this.props.loader;

      if (loader.onRef) {
        loader.onRef(this);
      }

      var params = Object.assign({}, this.props.loader, {
        nextOffset: 0
      });
      this.onFetchData(params);
    }
  }, {
    key: "reload",
    value: function reload() {
      var discreet = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      var params = Object.assign({}, this.props.loader, {
        nextOffset: 0,
        discreet: discreet
      });
      this.onFetchData(params);
    }
  }, {
    key: "handleSignificantPropsChanged",
    value: function handleSignificantPropsChanged(props) {
      var params = Object.assign({}, props.loader, {
        nextOffset: 0
      });
      this.onFetchData(params);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          wrapComponent = _this$props.wrapComponent,
          data = _this$props.data,
          props = _objectWithoutProperties(_this$props, _excluded);

      delete props.loader;
      var passProps = Object.assign({}, props, {
        data: data,
        onFetchMoreData: this.onFetchMoreData
      });
      return /*#__PURE__*/(0, _react.createElement)(wrapComponent, passProps);
    }
  }]);

  return DataProvider;
}(_react.Component);

DataProvider.propTypes = {
  wrapComponent: _propTypes.default.element,
  onFetchData: _propTypes.default.func,
  onRef: _propTypes.default.func,
  onClearData: _propTypes.default.func,
  data: _propTypes.default.array,
  attributes: _propTypes.default.arrayOf(_util.attributeShape),
  loader: _propTypes.default.object,
  loading: _propTypes.default.bool
};
DataProvider.defaultProps = {};
var getPagedLoader = (0, _reselect.createSelector)([function (props) {
  return props.loader;
}, function (props, state) {
  return state.nextOffset;
}], function (loader, nextOffset) {
  return Object.assign({
    nextOffset: nextOffset
  }, loader);
});

function mapStateToProps(state, ownProps) {
  var loader = getPagedLoader(ownProps, state);
  return {
    data: state.rows || [],
    loader: loader,
    loading: state.loading
  };
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    onFetchData: function onFetchData(model, sortAttribute, sortDirection, filters, discreet) {
      var nextOffset = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 0;
      var pageSize = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : 25;
      dispatch(loadData({
        model: model,
        sortAttribute: sortAttribute,
        sortDirection: sortDirection,
        filters: filters,
        loading: true,
        nextOffset: nextOffset,
        discreet: discreet,
        limit: pageSize,
        attributes: ownProps.attributes
      }));
    },
    onClearData: function onClearData() {
      dispatch(loadDataDone({
        rows: [],
        attributes: [],
        nextOffset: 0,
        append: false,
        reset: true
      }));
    }
  };
}

var ConnectedDataProvider = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(DataProvider);

function withDataLoader(wrapComponent) {
  return function WrappedComponent(props) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(ConnectedDataProvider, _objectSpread(_objectSpread({}, props), {}, {
      wrapComponent: wrapComponent
    }));
  };
}