"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ATTRIBUTE_LOADING = void 0;

require("core-js/modules/es6.symbol.js");

// :copyright: Copyright (c) 2020 ftrack
// eslint-disable-next-line import/prefer-default-export
var ATTRIBUTE_LOADING = Symbol("LOADING");
exports.ATTRIBUTE_LOADING = ATTRIBUTE_LOADING;