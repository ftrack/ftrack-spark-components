"use strict";

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.array.slice.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sortLocalisedAttributes = sortLocalisedAttributes;
exports.sortLocalisedGroups = sortLocalisedGroups;
exports.AttributeGroupLabel = AttributeGroupLabel;
exports.AttributeLabel_ = AttributeLabel_;
exports.AttributeLabel = void 0;

require("core-js/modules/es6.regexp.split.js");

require("core-js/modules/es6.array.sort.js");

require("core-js/modules/es6.string.ends-with.js");

require("core-js/modules/es6.array.find.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactIntl = require("react-intl");

var _classnames = _interopRequireDefault(require("classnames"));

var _style = _interopRequireDefault(require("./style.scss"));

var _util = require("../dataview/util");

var _attribute = require("../attribute");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var messages = (0, _reactIntl.defineMessages)({
  "unknown-attribute": {
    "id": "ftrack-spark-components.attribute.unknown-attribute",
    "defaultMessage": "Unknown attribute"
  }
});

function sortLocalisedAttributes(attributes, schema, intl) {
  var localisedStrings = attributes.reduce(function (localisedAccumulator, _ref) {
    var key = _ref.key,
        label = _ref.label;

    if (label) {
      localisedAccumulator[key] = label;
    } else {
      var keyPath = key.split(".");
      var attributeKey = keyPath[keyPath.length - 1];
      var i18nMessage = (0, _attribute.getLocalisedLabel)(schema, attributeKey);
      localisedAccumulator[key] = i18nMessage ? intl.formatMessage(i18nMessage) : key;
    }

    return localisedAccumulator;
  }, {});
  return _toConsumableArray(attributes).sort(function (a, b) {
    if ((0, _util.isCustomAttribute)(a) && !(0, _util.isCustomAttribute)(b) || b.key.endsWith("$entities")) {
      return 1;
    }

    if (!(0, _util.isCustomAttribute)(a) && (0, _util.isCustomAttribute)(b) || a.key.endsWith("$entities")) {
      return -1;
    }

    return localisedStrings[a.key].localeCompare(localisedStrings[b.key]);
  });
}

function sortLocalisedGroups(groups, session, intl) {
  var localisedStrings = groups.reduce(function (accumumulator, _ref2) {
    var key = _ref2.key,
        path = _ref2.path,
        groupLabel = _ref2.groupLabel;
    if (!path.length) return accumumulator;

    if (groupLabel) {
      accumumulator[key] = groupLabel;
    } else {
      var combinedLabel = path.reduce(function (combinedAccumulator, _ref3) {
        var pathKey = _ref3.key,
            model = _ref3.model;
        var candidateKey = pathKey;
        var details = (0, _util.getPolymorphicRelationDetails)(candidateKey);

        if (details) {
          candidateKey = details.name;
        }

        var keys = candidateKey.split(".");
        var schema = session.schemas.find(function (candidate) {
          return candidate.id === model;
        });
        var i18nMessage = (0, _attribute.getLocalisedLabel)(schema, keys[keys.length - 1]);
        combinedAccumulator.push(i18nMessage ? intl.formatMessage(i18nMessage) : key);
        return combinedAccumulator;
      }, []);
      accumumulator[key] = combinedLabel.join(" ");
    }

    return accumumulator;
  }, {});
  return _toConsumableArray(groups).sort(function (a, b) {
    if (!localisedStrings[b.key]) {
      return 1;
    }

    if (!localisedStrings[a.key]) {
      return -1;
    }

    return localisedStrings[a.key].localeCompare(localisedStrings[b.key]);
  });
}

function AttributeGroupLabel(_ref4) {
  var path = _ref4.path,
      label = _ref4.label;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
    children: label || path.reduce(function (accumulator, _ref5, index) {
      var candidateKey = _ref5.key,
          model = _ref5.model,
          label = _ref5.label;
      var key = candidateKey;
      var details = (0, _util.getPolymorphicRelationDetails)(key);

      if (details) {
        key = details.name;
      }

      var keys = key.split(".");
      accumulator.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_attribute.AttributeLabel, {
        attribute: label || keys[keys.length - 1],
        entityType: model
      }, "".concat(model, "-").concat(key)));

      if (index < path.length - 1) {
        accumulator.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
          children: " "
        }, "".concat(model, "-").concat(key, "-separator")));
      }

      return accumulator;
    }, [])
  });
}

AttributeGroupLabel.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  path: _propTypes.default.array.isRequired,
  label: _propTypes.default.string
};
AttributeGroupLabel.defaultProps = {
  path: []
};

function AttributeLabel_(_ref6) {
  var _classNames;

  var attribute = _ref6.attribute,
      enableGroup = _ref6.enableGroup,
      intl = _ref6.intl,
      transform = _ref6.transform,
      className = _ref6.className;

  if (!attribute) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      children: intl.formatMessage(messages["unknown-attribute"])
    });
  }

  var path = attribute.path,
      key = attribute.key,
      model = attribute.model,
      label = attribute.label;
  var fragments = key.split(".");
  var groupElements = null;

  if (enableGroup && path && path.length) {
    groupElements = [/*#__PURE__*/(0, _jsxRuntime.jsx)(AttributeGroupLabel, {
      path: path
    }, "group"), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      children: " "
    }, "space")];
  }

  var labelElement;

  if (label) {
    labelElement = /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      children: label
    });
  } else {
    labelElement = /*#__PURE__*/(0, _jsxRuntime.jsx)(_attribute.AttributeLabel, {
      attribute: fragments[fragments.length - 1],
      entityType: model
    }, "".concat(model, "-").concat(key));
  }

  var className_ = (0, _classnames.default)(_style.default["attribute-label"], (_classNames = {}, _defineProperty(_classNames, _style.default["attribute-label-capitalize"], transform === "capitalize"), _defineProperty(_classNames, _style.default["attribute-label-uppercase"], transform === "uppercase"), _classNames), className);
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: className_,
    children: [groupElements, labelElement]
  });
}

AttributeLabel_.propTypes = {
  intl: _reactIntl.intlShape.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  attribute: _util.attributeShape.isRequired,
  className: _propTypes.default.string,
  enableGroup: _propTypes.default.bool,
  transform: _propTypes.default.oneOf(["capitalize", "uppercase"])
};
AttributeLabel_.defaultProps = {
  enableGroup: false,
  transform: "capitalize"
};
var AttributeLabel = (0, _safe_inject_intl.default)(AttributeLabel_);
exports.AttributeLabel = AttributeLabel;