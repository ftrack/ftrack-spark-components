"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.string.includes.js");

require("core-js/modules/es7.array.includes.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _fixedDataTable = require("fixed-data-table-2");

var _checkbox = _interopRequireDefault(require("react-toolbox/lib/checkbox"));

var _style = _interopRequireDefault(require("./style"));

var _selection_checkbox_theme = _interopRequireDefault(require("./selection_checkbox_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var SelectionCell = /*#__PURE__*/function (_PureComponent) {
  _inherits(SelectionCell, _PureComponent);

  var _super = _createSuper(SelectionCell);

  function SelectionCell(props) {
    var _this;

    _classCallCheck(this, SelectionCell);

    _this = _super.call(this, props);
    _this.handleChange = _this.handleChange.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(SelectionCell, [{
    key: "handleChange",
    value: function handleChange(state) {
      this.props.onSelectionChanged(state, this.props.rowIndex);
    }
  }, {
    key: "render",
    value: function render() {
      var isChecked = this.props.selectedIndexes.includes(this.props.rowIndex);

      if (this.props.rowIndex === this.props.lastRowIndex) {
        this.props.onLastRowRendered();
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_fixedDataTable.Cell, {
        className: _style.default["selection-cell"],
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_checkbox.default, {
          theme: _selection_checkbox_theme.default,
          checked: isChecked,
          onChange: this.handleChange
        })
      });
    }
  }]);

  return SelectionCell;
}(_react.PureComponent);

SelectionCell.propTypes = {
  lastRowIndex: _propTypes.default.number,
  onLastRowRendered: _propTypes.default.func,
  onSelectionChanged: _propTypes.default.func,
  rowIndex: _propTypes.default.number,
  selectedIndexes: _propTypes.default.array
};
var _default = SelectionCell;
exports.default = _default;