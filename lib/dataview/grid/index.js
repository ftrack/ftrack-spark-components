"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.array.slice.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _debounce = _interopRequireDefault(require("lodash/debounce"));

var _isUndefined = _interopRequireDefault(require("lodash/isUndefined"));

var _fixedDataTable = require("fixed-data-table-2");

var _loglevel = _interopRequireDefault(require("loglevel"));

var _header_cell = _interopRequireDefault(require("./header_cell"));

var _selection_cell = _interopRequireDefault(require("./selection_cell"));

var _formatted_cell = _interopRequireDefault(require("./formatted_cell"));

var _selection_handler = require("../selection_handler");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

/** Grid component */
var Grid = /*#__PURE__*/function (_Component) {
  _inherits(Grid, _Component);

  var _super = _createSuper(Grid);

  function Grid(props) {
    var _this;

    _classCallCheck(this, Grid);

    _this = _super.call(this);
    _this.state = {
      nextOffset: 0,
      sortAttribute: props.sortAttribute,
      sortDirection: props.sortDirection
    };
    _this.handleAllRowsRendered = (0, _debounce.default)(_this.handleAllRowsRendered.bind(_assertThisInitialized(_this)), 25);
    _this.handleGridSort = _this.handleGridSort.bind(_assertThisInitialized(_this));
    _this.handleRowSelectionChanged = _this.handleRowSelectionChanged.bind(_assertThisInitialized(_this));
    _this.handleColumnReorder = _this.handleColumnReorder.bind(_assertThisInitialized(_this));
    _this.handleColumnResize = _this.handleColumnResize.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(Grid, [{
    key: "getGridColumns",
    value: function getGridColumns() {
      var _this2 = this;

      var _this$props = this.props,
          attributes = _this$props.attributes,
          data = _this$props.data,
          sortAttribute = _this$props.sortAttribute,
          sortDirection = _this$props.sortDirection,
          selected = _this$props.selected;
      var dataIds = data.map(function (item) {
        return item.__entity_id__;
      });
      var selectedIndexes = selected.reduce(function (accumulator, identifier) {
        var index = dataIds.indexOf(identifier);

        if (index !== -1) {
          accumulator.push(index);
        }

        return accumulator;
      }, []);
      var checkboxColumn = /*#__PURE__*/(0, _jsxRuntime.jsx)(_fixedDataTable.Column, {
        width: 34,
        cell: function cell(props) {
          return /*#__PURE__*/(0, _jsxRuntime.jsx)(_selection_cell.default, {
            lastRowIndex: data.length - 1,
            onLastRowRendered: _this2.handleAllRowsRendered,
            onSelectionChanged: _this2.handleRowSelectionChanged,
            rowIndex: props.rowIndex,
            selectedIndexes: selectedIndexes
          });
        }
      }, "checkbox-column");
      var columns = attributes.map(function (attribute, index) {
        var columnSortDirection = null;

        if (attribute.sortable && attribute.key === sortAttribute) {
          columnSortDirection = sortDirection;
        } // Force last column to flex.


        var lastColumn = index === attributes.length - 1;
        var flexGrow = attribute.width ? null : 1;
        var attributeWidth = attribute.width || null;

        if (lastColumn) {
          flexGrow = 1;
        } // Min width should be set as width if column is flex.
        // https://github.com/schrodinger/fixed-data-table-2/issues/23


        if (flexGrow && attribute.minWidth) {
          attributeWidth = attribute.minWidth;
        }

        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_fixedDataTable.Column, {
          width: attributeWidth,
          minWidth: attribute.minWidth,
          maxWidth: attribute.maxWidth,
          flexGrow: flexGrow,
          columnKey: attribute.key,
          isResizable: (0, _isUndefined.default)(attribute.resizable) ? true : attribute.resizable,
          isReorderable: (0, _isUndefined.default)(attribute.reorderable) ? true : attribute.reorderable,
          allowCellsRecycling: true,
          header: function header(headerProps) {
            return /*#__PURE__*/(0, _jsxRuntime.jsx)(_header_cell.default, _objectSpread({
              attribute: attribute,
              sortable: attribute.sortable,
              sortDirection: columnSortDirection,
              onSortChanged: _this2.handleGridSort
            }, headerProps));
          },
          cell: function cell(cellProps) {
            return /*#__PURE__*/(0, _jsxRuntime.jsx)(_formatted_cell.default, _objectSpread({
              attribute: attribute,
              row: data[cellProps.rowIndex],
              onCellDoubleClick: _this2.props.onCellDoubleClick,
              onClick: _this2.props.onItemClicked
            }, cellProps));
          }
        }, "column-".concat(attribute.key));
      });
      return [checkboxColumn].concat(_toConsumableArray(columns));
    }
  }, {
    key: "handleAllRowsRendered",
    value: function handleAllRowsRendered() {
      if (this.props.onFetchMoreData) {
        this.props.onFetchMoreData();
      }
    }
  }, {
    key: "handleRowSelectionChanged",
    value: function handleRowSelectionChanged(state, selectedIndex) {
      if (this.props.onCheckboxClicked) {
        this.props.onCheckboxClicked(state, selectedIndex);
      }
    }
  }, {
    key: "handleGridSort",
    value: function handleGridSort(sortAttribute, sortDirection) {
      this.props.onSortChanged(sortAttribute, sortDirection);
    }
  }, {
    key: "handleColumnReorder",
    value: function handleColumnReorder(event) {
      if (!event.reorderColumn) {
        _loglevel.default.warn("Reordering column without key defined.", event);

        return;
      }

      var nextColumnOrder = this.props.attributes.map(function (attribute) {
        return attribute.key;
      }).filter(function (columnKey) {
        return columnKey !== event.reorderColumn;
      });

      if (event.columnAfter) {
        var insertAt = nextColumnOrder.indexOf(event.columnAfter);
        nextColumnOrder.splice(insertAt, 0, event.reorderColumn);
      } else {
        nextColumnOrder.push(event.reorderColumn);
      }

      this.props.onColumnReorder(nextColumnOrder);
    }
  }, {
    key: "handleColumnResize",
    value: function handleColumnResize(nextWidth, columnKey) {
      this.props.onColumnResize(columnKey, nextWidth);
    }
  }, {
    key: "render",
    value: function render() {
      var columns = this.getGridColumns();
      return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: this.props.className,
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_fixedDataTable.Table, {
          rowHeight: this.props.rowHeight,
          headerHeight: 56,
          rowsCount: this.props.data.length,
          width: this.props.width,
          height: this.props.height,
          onColumnReorderEndCallback: this.handleColumnReorder,
          onColumnResizeEndCallback: this.handleColumnResize,
          isColumnReordering: false,
          isColumnResizing: false,
          children: columns
        })
      });
    }
  }]);

  return Grid;
}(_react.Component);

Grid.propTypes = {
  attributes: _propTypes.default.array,
  selected: _propTypes.default.array,
  height: _propTypes.default.number,
  loading: _propTypes.default.bool,
  onFetchMoreData: _propTypes.default.func,
  onCellDoubleClick: _propTypes.default.func,
  onColumnReorder: _propTypes.default.func,
  onColumnResize: _propTypes.default.func,
  onComponentDidMount: _propTypes.default.func,
  onCheckboxClicked: _propTypes.default.func,
  onItemClicked: _propTypes.default.func,
  onSortChanged: _propTypes.default.func,
  rowHeight: _propTypes.default.number,
  data: _propTypes.default.array,
  sortAttribute: _propTypes.default.string,
  sortDirection: _propTypes.default.string,
  className: _propTypes.default.string,
  width: _propTypes.default.number
};
Grid.defaultProps = {
  selected: []
};

var _default = (0, _selection_handler.withSelectionHandler)()(Grid);

exports.default = _default;