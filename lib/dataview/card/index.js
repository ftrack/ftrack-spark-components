"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactWaypoint = require("react-waypoint");

var _classnames = _interopRequireDefault(require("classnames"));

var _isArray = _interopRequireDefault(require("lodash/isArray"));

var _style = _interopRequireDefault(require("./style.scss"));

var _entity_card = _interopRequireDefault(require("../../entity_card"));

var _util = require("../util");

var _selection_handler = require("../selection_handler");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var CardViewContainer = /*#__PURE__*/function (_Component) {
  _inherits(CardViewContainer, _Component);

  var _super = _createSuper(CardViewContainer);

  function CardViewContainer() {
    var _this;

    _classCallCheck(this, CardViewContainer);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));
    _this.renderItems = _this.renderItems.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(CardViewContainer, [{
    key: "renderItems",
    value: function renderItems(renderProps) {
      var _this2 = this;

      var _this$props = this.props,
          thumbnail = _this$props.thumbnail,
          titleAttribute = _this$props.titleAttribute,
          subtitleAttributes = _this$props.subtitleAttributes,
          attributes = _this$props.attributes,
          colorAttribute = _this$props.colorAttribute,
          footer = _this$props.footer,
          model = _this$props.model,
          selectable = _this$props.selectable,
          rows = _this$props.data,
          heading = _this$props.heading,
          showHeadingOnEmpty = _this$props.showHeadingOnEmpty,
          viewId = _this$props.viewId,
          loading = _this$props.loading,
          CardComponent = _this$props.card,
          onItemClicked = _this$props.onItemClicked,
          selected = _this$props.selected,
          className = _this$props.className,
          disableThumbnailClick = _this$props.disableThumbnailClick;
      var waypoint = !loading ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactWaypoint.Waypoint, {
        onEnter: function onEnter() {
          _this2.props.onFetchMoreData();
        },
        bottomOffset: -400
      }, "list-waypoint") : null;
      var children = [];

      if (this.props.children) {
        children = (0, _isArray.default)(this.props.children) ? this.props.children : [this.props.children];
      }

      var showHeading = showHeadingOnEmpty || heading && rows.length > 0;
      var cardViewClasses = (0, _classnames.default)(className, _defineProperty({}, _style.default["cards-view"], children.length || rows.length || showHeading));
      return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        children: [showHeading && /*#__PURE__*/(0, _jsxRuntime.jsx)("h4", {
          children: heading
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
          className: cardViewClasses,
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            className: _style.default["extra-children"],
            children: children.map(function (child) {
              return /*#__PURE__*/(0, _react.cloneElement)(child, _objectSpread({}, renderProps));
            })
          }), rows.map(function (data, rowIndex) {
            var selectionProps = selectable ? {
              selected: selected.indexOf(data.__entity_id__) !== -1,
              onSelectionChanged: _this2.props.onCheckboxClicked
            } : {};
            return /*#__PURE__*/(0, _jsxRuntime.jsx)(CardComponent, _objectSpread(_objectSpread({
              thumbnail: thumbnail,
              className: _style.default["card-item"],
              data: data,
              id: data.id,
              attributes: attributes,
              titleAttribute: titleAttribute,
              subtitleAttributes: subtitleAttributes,
              colorAttribute: colorAttribute,
              footer: footer,
              model: model,
              selectable: selectable
            }, selectionProps), {}, {
              index: rowIndex,
              onItemClicked: onItemClicked,
              disableThumbnailClick: disableThumbnailClick,
              viewId: viewId
            }, renderProps), "card-".concat(rowIndex));
          })]
        }), waypoint]
      });
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        children: this.renderItems()
      });
    }
  }, {
    key: "render",
    value: function render() {
      var renderProps = this.props.renderProps;

      if (renderProps) {
        return this.renderItems(renderProps);
      }

      return this.renderContent();
    }
  }]);

  return CardViewContainer;
}(_react.Component);

CardViewContainer.propTypes = {
  card: _propTypes.default.node,
  children: _propTypes.default.arrayOf(_propTypes.default.node),
  titleAttribute: _propTypes.default.func,
  subtitleAttributes: _propTypes.default.arrayOf(_propTypes.default.oneOfType([_propTypes.default.func, _propTypes.default.string]).isRequired).isRequired,
  attributes: _propTypes.default.arrayOf(_util.attributeShape),
  data: _propTypes.default.array,
  colorAttribute: _propTypes.default.string,
  footer: _propTypes.default.func,
  renderProps: _propTypes.default.shape({
    onDrop: _propTypes.default.func
  }),
  onCheckboxClicked: _propTypes.default.func,
  onFetchMoreData: _propTypes.default.func,
  onItemClicked: _propTypes.default.func,
  model: _propTypes.default.string,
  thumbnail: _propTypes.default.func,
  heading: _propTypes.default.string,
  className: _propTypes.default.string,
  showHeadingOnEmpty: _propTypes.default.bool,
  selectable: _propTypes.default.bool,
  viewId: _propTypes.default.string,
  loading: _propTypes.default.bool,
  disableThumbnailClick: _propTypes.default.bool,
  selected: _propTypes.default.array.isRequired
};
CardViewContainer.defaultProps = {
  card: _entity_card.default,
  selected: []
};

var _default = (0, _selection_handler.withSelectionHandler)()(CardViewContainer);

exports.default = _default;