"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.uniqueAndSortedArray = uniqueAndSortedArray;
exports.withSelectionHandler = withSelectionHandler;

require("core-js/modules/es6.set.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.sort.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.from.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _classnames = _interopRequireDefault(require("classnames"));

var _with_monitor_key_events = _interopRequireDefault(require("../util/hoc/with_monitor_key_events"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["shiftIsPressed", "className"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function uniqueAndSortedArray(arrayToSort) {
  return _toConsumableArray(new Set(arrayToSort.sort(function (a, b) {
    return a - b;
  })));
}

function withSelectionHandler() {
  return function wrapComponent(InnerComponent) {
    var WrappedComponent = /*#__PURE__*/function (_Component) {
      _inherits(WrappedComponent, _Component);

      var _super = _createSuper(WrappedComponent);

      function WrappedComponent(props) {
        var _this;

        _classCallCheck(this, WrappedComponent);

        _this = _super.call(this, props);
        _this.state = {
          lastSelectedIndex: null
        };
        _this.handleCheckboxClicked = _this.handleCheckboxClicked.bind(_assertThisInitialized(_this));
        return _this;
      }

      _createClass(WrappedComponent, [{
        key: "getShiftSelection",
        value: function getShiftSelection(checked, clickedIndex, previouslySelected) {
          var selectedIndexes = [];
          var lastSelectedIndex = this.state.lastSelectedIndex;
          var max = Math.max(clickedIndex, lastSelectedIndex);
          var min = Math.min(clickedIndex, lastSelectedIndex);
          var length = max - min + 1;
          var shiftSelectedIndexes = uniqueAndSortedArray(_toConsumableArray(Array(length).keys()).map(function (element, arrayIndex) {
            return min + arrayIndex;
          }));

          if (checked) {
            selectedIndexes = [].concat(_toConsumableArray(previouslySelected.filter(function (item) {
              return item !== clickedIndex;
            })), _toConsumableArray(shiftSelectedIndexes));
          } else {
            selectedIndexes = Array.from(shiftSelectedIndexes);
            /**
             * Get indexes that are previously selected and not included in new shift-click.
             */

            var shiftSelectedIndexesSet = new Set(_toConsumableArray(shiftSelectedIndexes));
            var difference = previouslySelected.filter(function (cardIndex) {
              return !shiftSelectedIndexesSet.has(cardIndex);
            });
            /**
             * Get sequences of numbers
             */

            var sequences = [];
            var sequence = [];
            difference.forEach(function (cardIndex, differenceArrayIndex) {
              /**
               * If index is +1 from previous index or the first element, it should be
               * a part of an ongoing sequence.
               */
              if (differenceArrayIndex === 0 || cardIndex === Math.max.apply(Math, _toConsumableArray(sequence)) + 1) {
                sequence.push(cardIndex);
              } else {
                /**
                 * If not part of a sequence with the previous index, count it
                 * as start of a new sequence.
                 */
                sequences.push(Array.from(sequence));
                sequence = [cardIndex];
              }
            });
            /**
             * Push the last ongoing sequence to the sequences array.
             */

            sequences.push(Array.from(sequence));
            /**
             * For every sequence, check if they are neighbors to the last shift selection.
             * If they are, the should not be selected.
             */

            sequences.forEach(function (indexSequence) {
              var sequenceMax = Math.max.apply(Math, _toConsumableArray(indexSequence));
              var sequenceMin = Math.min.apply(Math, _toConsumableArray(indexSequence));

              if (Math.abs(sequenceMax - max) !== 1 && Math.abs(sequenceMax - min) !== 1 && Math.abs(sequenceMin - max) !== 1 && Math.abs(sequenceMin - min) !== 1) {
                selectedIndexes = selectedIndexes.concat(indexSequence);
              }
            });
          }

          return selectedIndexes;
        }
      }, {
        key: "handleCheckboxClicked",
        value: function handleCheckboxClicked(checked, clickedIndex) {
          var _this$props = this.props,
              onSelectionChanged = _this$props.onSelectionChanged,
              shiftIsPressed = _this$props.shiftIsPressed,
              selected = _this$props.selected,
              data = _this$props.data;
          var dataIds = data.map(function (item) {
            return item.__entity_id__;
          });
          var previouslySelected = selected.reduce(function (accumulator, identifier) {
            var index = dataIds.indexOf(identifier);

            if (index !== -1) {
              accumulator.push(index);
            }

            return accumulator;
          }, []);
          var selectedIndexes = [];
          var lastSelectedIndex = this.state.lastSelectedIndex;

          if (shiftIsPressed) {
            selectedIndexes = this.getShiftSelection(checked, clickedIndex, previouslySelected);
          } else {
            selectedIndexes = checked ? [].concat(_toConsumableArray(previouslySelected), [clickedIndex]) : previouslySelected.filter(function (item) {
              return item !== clickedIndex;
            });
            /**
             * Keep track of the last item that was checked/unchecked without pressing
             * shift at the same time to be able to decide what should be selected on the next
             * Shift-Click.
             */

            lastSelectedIndex = clickedIndex;
          }

          selectedIndexes = uniqueAndSortedArray(selectedIndexes);
          this.setState({
            lastSelectedIndex: lastSelectedIndex
          });
          onSelectionChanged(selectedIndexes.map(function (index) {
            return data[index].__entity_id__;
          }));
        }
      }, {
        key: "render",
        value: function render() {
          var _this$props2 = this.props,
              shiftIsPressed = _this$props2.shiftIsPressed,
              className = _this$props2.className,
              props = _objectWithoutProperties(_this$props2, _excluded);

          return /*#__PURE__*/(0, _jsxRuntime.jsx)(InnerComponent, _objectSpread(_objectSpread({}, props), {}, {
            className: (0, _classnames.default)(className, _defineProperty({}, _style.default["disable-text-selection"], shiftIsPressed)),
            onCheckboxClicked: this.handleCheckboxClicked
          }));
        }
      }]);

      return WrappedComponent;
    }(_react.Component);

    WrappedComponent.propTypes = {
      onSelectionChanged: _propTypes.default.func.isRequired,
      shiftIsPressed: _propTypes.default.bool.isRequired,
      selected: _propTypes.default.array.isRequired,
      className: _propTypes.default.string,
      data: _propTypes.default.array.isRequired
    };
    WrappedComponent.defaultTyps = {
      selected: []
    };
    return (0, _with_monitor_key_events.default)({
      16: "shiftIsPressed"
    })(WrappedComponent);
  };
}