"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _heading = _interopRequireDefault(require("../heading"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2018 ftrack
function EmptyText(_ref) {
  var label = _ref.label,
      header = _ref.header,
      icon = _ref.icon,
      children = _ref.children,
      className = _ref.className,
      cover = _ref.cover;
  var classNames = [_style.default.emptyText];

  if (cover) {
    classNames.push(_style.default.cover);
  }

  if (className) {
    classNames.push(className);
  }

  var iconElement = null;

  if (icon) {
    if (typeof icon === "string") {
      iconElement = /*#__PURE__*/(0, _jsxRuntime.jsx)("img", {
        className: _style.default.icon,
        alt: "",
        role: "presentation",
        src: icon
      });
    } else {
      iconElement = /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _style.default["font-icon"],
        children: icon
      });
    }
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: classNames.join(" "),
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default.inner,
      children: [iconElement, header ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_heading.default, {
        variant: "title",
        className: _style.default.header,
        color: "primary",
        children: header
      }) : null, label ? /*#__PURE__*/(0, _jsxRuntime.jsx)("h5", {
        className: _style.default.label,
        children: label
      }) : null, children]
    })
  });
}

EmptyText.propTypes = {
  icon: _propTypes.default.node,
  label: _propTypes.default.node,
  header: _propTypes.default.node,
  children: _propTypes.default.node,
  cover: _propTypes.default.bool,
  className: _propTypes.default.string
};
EmptyText.defaultProps = {
  cover: true
};
var _default = EmptyText;
exports.default = _default;