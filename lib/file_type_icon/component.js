"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.regexp.replace.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _progress_bar = _interopRequireDefault(require("react-toolbox/lib/progress_bar"));

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _style = _interopRequireDefault(require("./style.scss"));

var _spinner_theme = _interopRequireDefault(require("./spinner_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function FileTypeIcon(_ref) {
  var _classNames;

  var className = _ref.className,
      variant = _ref.variant,
      _ref$label = _ref.label,
      label = _ref$label === void 0 ? "" : _ref$label,
      loading = _ref.loading;
  var longLabel = label.length > 5; // Adjust font-size depending on label length

  var labelClasses = (0, _classnames.default)((_classNames = {}, _defineProperty(_classNames, _style.default["label-tiny"], variant === "tiny"), _defineProperty(_classNames, _style.default["label-tiny-long-text"], variant === "tiny" && longLabel), _defineProperty(_classNames, _style.default["label-small"], variant === "small"), _defineProperty(_classNames, _style.default["label-small-long-text"], variant === "small" && longLabel), _defineProperty(_classNames, _style.default["label-medium"], variant === "medium"), _defineProperty(_classNames, _style.default["label-medium-long-text"], variant === "medium" && longLabel), _defineProperty(_classNames, _style.default["label-large"], variant === "large"), _defineProperty(_classNames, _style.default["label-large-long-text"], variant === "large" && longLabel), _defineProperty(_classNames, _style.default["label-playlist"], variant === "playlist"), _defineProperty(_classNames, _style.default["label-playlist-long-text"], variant === "playlist" && longLabel), _classNames));
  var newLabel = label ? label.replace(".", "") : "";
  var content = loading ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_progress_bar.default, {
    type: "circular",
    mode: "indeterminate",
    className: _style.default["spinner-".concat(variant)],
    theme: _spinner_theme.default
  }) : /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
    className: labelClasses,
    children: longLabel ? "".concat(newLabel.substr(0, 5), "...") : newLabel
  });
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: (0, _classnames.default)(_style.default.iconWrapper, className),
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
      value: "insert_drive_file",
      variant: variant,
      className: _style.default["icon-".concat(variant)]
    }), content]
  });
}

FileTypeIcon.propTypes = {
  variant: _propTypes.default.string,
  label: _propTypes.default.string,
  className: _propTypes.default.string,
  loading: _propTypes.default.bool
};
var _default = FileTypeIcon;
exports.default = _default;