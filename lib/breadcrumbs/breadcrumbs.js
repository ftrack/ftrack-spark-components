"use strict";

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _material = require("@mui/material");

var _MoreHoriz = _interopRequireDefault(require("@mui/icons-material/MoreHoriz"));

var _link = _interopRequireDefault(require("../link"));

var _button_menu = _interopRequireDefault(require("../button_menu"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var styles = {
  breadcrumbRoot: {
    display: "flex",
    alignItems: "center",
    minWidth: 0
  },
  breadcrumbContainer: {
    textAlign: "right",
    overflow: "hidden",
    whiteSpace: "nowrap",
    position: "relative"
  },
  disabledBreadcrumbContainer: {
    textAlign: "left",
    textOverflow: "ellipsis"
  },
  breadcrumb: {
    float: "right"
  },
  disabledBreadcrumb: {
    float: "unset"
  },
  breadcrumbButton: {
    padding: 0,
    borderRadius: "1.5rem",
    mr: 0.5,
    bgcolor: "lightContrastColor.highEmphasis"
  },
  buttonIcon: {
    height: "1.5rem",
    width: "2.4rem",
    fontSize: "fontSizeLarge"
  },
  truncatedItemsIcon: {
    marginRight: "1px",
    paddingTop: "0.6rem",
    width: "1.6rem",
    height: "2.2rem",
    lineHeight: 1,
    userSelect: "none"
  }
};
var DEFAULT_SEPARATOR = " / ";

function Breadcrumbs(_ref) {
  var _ref$items = _ref.items,
      items = _ref$items === void 0 ? [] : _ref$items,
      sx = _ref.sx,
      sxBreadcrumb = _ref.sxBreadcrumb,
      _ref$separator = _ref.separator,
      separator = _ref$separator === void 0 ? DEFAULT_SEPARATOR : _ref$separator,
      _ref$truncation = _ref.truncation,
      truncation = _ref$truncation === void 0 ? "menu" : _ref$truncation;
  var refContainer = /*#__PURE__*/(0, _react.createRef)();
  var refContent = /*#__PURE__*/(0, _react.createRef)();

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      truncationEnabled = _useState2[0],
      setTruncationEnabled = _useState2[1];

  (0, _react.useLayoutEffect)(function () {
    if (truncation !== "simple") {
      if (refContainer.current.clientWidth < refContent.current.clientWidth) {
        setTruncationEnabled(true);
      } else {
        setTruncationEnabled(false);
      }
    }
  }, // TODO: Feel free to fix this eslint error if you got time
  // eslint-disable-next-line react-hooks/exhaustive-deps
  [refContainer, refContent]);

  var handleSelect = function handleSelect(selectedIndex) {
    var _ref2 = items[selectedIndex] || {},
        to = _ref2.to,
        onClick = _ref2.onClick,
        disabled = _ref2.disabled;

    if (!disabled) {
      if (onClick) {
        onClick();
      } else {
        // eslint-disable-next-line
        var newWindow = window.open(to, "_blank");
        newWindow.focus();
      }
    }
  };

  var truncatedItems = null;

  if (truncationEnabled && truncation === "menu") {
    truncatedItems = /*#__PURE__*/(0, _jsxRuntime.jsx)(_button_menu.default, {
      dense: true,
      button: /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.IconButton, {
        sx: _objectSpread({}, styles.breadcrumbButton),
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_MoreHoriz.default, {
          sx: styles.buttonIcon
        })
      }),
      onSelect: handleSelect,
      children: items.map(function (_ref3, i) {
        var title = _ref3.title,
            disabled = _ref3.disabled;
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.MenuItem, {
          disabled: disabled,
          children: title
        }, i);
      })
    });
  } else if (truncationEnabled && truncation === "tooltip") {
    truncatedItems = /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.Tooltip, {
      title: items.map(function (item) {
        return item.title;
      }).join(" / "),
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_MoreHoriz.default, {
        sx: styles.truncatedItemsIcon
      })
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_material.Box, {
    sx: _objectSpread(_objectSpread({}, styles.breadcrumbRoot), sx),
    children: [truncatedItems, /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.Box, {
      ref: refContainer,
      sx: _objectSpread(_objectSpread({}, styles.breadcrumbContainer), truncation === "simple" ? styles.disabledBreadcrumbContainer : {}),
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.Box, {
        ref: refContent,
        component: "span",
        sx: _objectSpread(_objectSpread({}, styles.breadcrumb), truncation === "simple" ? styles.disabledBreadcrumb : {}),
        children: items.map(function (_ref4, i) {
          var to = _ref4.to,
              title = _ref4.title,
              id = _ref4.id,
              onClick = _ref4.onClick,
              disabled = _ref4.disabled,
              isLoading = _ref4.isLoading;
          return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_react.Fragment, {
            children: [!!i && separator, /*#__PURE__*/(0, _jsxRuntime.jsx)(_link.default, {
              disabled: disabled,
              to: to,
              title: title,
              onClick: onClick,
              isLoading: isLoading
            })]
          }, id);
        })
      })
    })]
  });
}

Breadcrumbs.propTypes = {
  items: _propTypes.default.arrayOf(_propTypes.default.shape({
    id: _propTypes.default.string,
    to: _propTypes.default.string,
    title: _propTypes.default.string,
    onClick: _propTypes.default.func,
    disabled: _propTypes.default.bool,
    isLoading: _propTypes.default.bool
  })).isRequired,
  className: _propTypes.default.string,
  classNameBreadcrumb: _propTypes.default.string,
  separator: _propTypes.default.node,
  truncation: _propTypes.default.oneOf(["menu", "tooltip", "simple"])
};
var _default = Breadcrumbs;
exports.default = _default;