"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MilestoneIcon = MilestoneIcon;
exports.AssetBuildIcon = AssetBuildIcon;
exports.ICONS = void 0;

var _iconsMaterial = require("@mui/icons-material");

var _material = require("@mui/material");

var _jsxRuntime = require("react/jsx-runtime");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/** https://icons.getbootstrap.com/icons/diamond-fill/ */
function MilestoneIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.SvgIcon, _objectSpread(_objectSpread({
    viewBox: "-4 -4 24 24"
  }, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
      d: "M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.482 1.482 0 0 1 0-2.098L6.95.435z"
    })
  }));
}
/** https://icons.getbootstrap.com/icons/box2-fill/ */


function AssetBuildIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.SvgIcon, _objectSpread(_objectSpread({
    viewBox: "-4 -4 24 24"
  }, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
      d: "M3.75 0a1 1 0 0 0-.8.4L.1 4.2a.5.5 0 0 0-.1.3V15a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V4.5a.5.5 0 0 0-.1-.3L13.05.4a1 1 0 0 0-.8-.4h-8.5ZM15 4.667V5H1v-.333L1.5 4h6V1h1v3h6l.5.667Z"
    })
  }));
}

var ICONS = {
  default: _iconsMaterial.Folder,
  folder: _iconsMaterial.Folder,
  episode: _iconsMaterial.Tv,
  shot: _iconsMaterial.Movie,
  folder_shared: _iconsMaterial.FolderShared,
  folder_special: _iconsMaterial.FolderSpecial,
  widgets: _iconsMaterial.Widgets,
  extension: _iconsMaterial.Extension,
  audio: _iconsMaterial.Audiotrack,
  image: _iconsMaterial.Image,
  tv: _iconsMaterial.Tv,
  mic: _iconsMaterial.Mic,
  camera: _iconsMaterial.CameraAlt,
  task: _iconsMaterial.TaskOutlined,
  star: _iconsMaterial.Star,
  "star-outline": _iconsMaterial.StarOutline,
  user: _iconsMaterial.Person,
  close: _iconsMaterial.Close,
  refresh: _iconsMaterial.Refresh,
  check: _iconsMaterial.Check,
  apps: _iconsMaterial.Apps,
  book: _iconsMaterial.LocalLibrary,
  cloud: _iconsMaterial.Cloud,
  "cloud-upload": _iconsMaterial.CloudUpload,
  file: _iconsMaterial.InsertDriveFile,
  "format-size": _iconsMaterial.FormatSize,
  "attach-file": _iconsMaterial.AttachFile,
  time: _iconsMaterial.AccessTime,
  minus: _iconsMaterial.Remove,
  add: _iconsMaterial.Add,
  "add-filled": _iconsMaterial.AddCircle,
  email: _iconsMaterial.Email,
  "play-circle-outline": _iconsMaterial.PlayCircleOutline,
  pause: _iconsMaterial.Pause,
  loop: _iconsMaterial.Loop,
  warning: _iconsMaterial.Warning,
  "view-rows": _iconsMaterial.Reorder,
  "view-blocks": _iconsMaterial.ViewStream,
  search: _iconsMaterial.Search,
  settings: _iconsMaterial.Settings,
  "help-filled": _iconsMaterial.Help,
  remove: _iconsMaterial.HighlightOff,
  favorite: _iconsMaterial.Favorite,
  "favorite-outline": _iconsMaterial.FavoriteBorder,
  "3d-rotation": _iconsMaterial.ThreeDRotation,
  accessibility: _iconsMaterial.Accessibility,
  "account-balance": _iconsMaterial.AccountBalance,
  "account-box": _iconsMaterial.AccountBox,
  "account-circle": _iconsMaterial.AccountCircle,
  "add-shopping-cart": _iconsMaterial.AddShoppingCart,
  album: _iconsMaterial.Album,
  archive: _iconsMaterial.Archive,
  assessment: _iconsMaterial.Assessment,
  assignment: _iconsMaterial.Assignment,
  "assignment-ind": _iconsMaterial.AssignmentInd,
  "assignment-late": _iconsMaterial.AssignmentLate,
  "assignment-return": _iconsMaterial.AssignmentReturn,
  "assignment-returned": _iconsMaterial.AssignmentReturned,
  "assignment-turned-in": _iconsMaterial.AssignmentTurnedIn,
  "attach-money": _iconsMaterial.AttachMoney,
  book2: _iconsMaterial.Book,
  bookmark: _iconsMaterial.Bookmark,
  "bookmark-outline": _iconsMaterial.BookmarkBorder,
  brush: _iconsMaterial.Brush,
  "bug-report": _iconsMaterial.BugReport,
  cake: _iconsMaterial.Cake,
  "camera-roll": _iconsMaterial.CameraRoll,
  camera2: _iconsMaterial.Camera,
  cancel: _iconsMaterial.Cancel,
  "check-box-outline-blank": _iconsMaterial.CheckBoxOutlineBlank,
  "closed-caption": _iconsMaterial.ClosedCaption,
  "cloud-circle": _iconsMaterial.CloudCircle,
  "cloud-done": _iconsMaterial.CloudDone,
  "cloud-download": _iconsMaterial.CloudDownload,
  "cloud-off": _iconsMaterial.CloudOff,
  "cloud-queue": _iconsMaterial.CloudQueue,
  "color-lens": _iconsMaterial.ColorLens,
  computer: _iconsMaterial.Computer,
  "content-copy": _iconsMaterial.ContentCopy,
  "content-cut": _iconsMaterial.ContentCut,
  "content-paste": _iconsMaterial.ContentPaste,
  "control-point": _iconsMaterial.ControlPoint,
  create: _iconsMaterial.Create,
  "credit-card": _iconsMaterial.CreditCard,
  "crop-original": _iconsMaterial.CropOriginal,
  dashboard: _iconsMaterial.Dashboard,
  description: _iconsMaterial.Description,
  devices: _iconsMaterial.Devices,
  directions: _iconsMaterial.Directions,
  "directions-bike": _iconsMaterial.DirectionsBike,
  "directions-bus": _iconsMaterial.DirectionsBus,
  "directions-car": _iconsMaterial.DirectionsCar,
  "directions-ferry": _iconsMaterial.DirectionsBoat,
  "directions-subway": _iconsMaterial.DirectionsSubway,
  "directions-walk": _iconsMaterial.DirectionsWalk,
  dns: _iconsMaterial.Dns,
  domain: _iconsMaterial.Domain,
  "done-all": _iconsMaterial.DoneAll,
  drafts: _iconsMaterial.Drafts,
  dvr: _iconsMaterial.Dvr,
  event: _iconsMaterial.Event,
  explicit: _iconsMaterial.Explicit,
  explore: _iconsMaterial.Explore,
  "face-unlock": _iconsMaterial.Face,
  "filter-hdr": _iconsMaterial.FilterHdr,
  "filter-none": _iconsMaterial.FilterNone,
  "filter-vintage": _iconsMaterial.FilterVintage,
  "find-in-page": _iconsMaterial.FindInPage,
  flag: _iconsMaterial.Flag,
  flight: _iconsMaterial.Flight,
  "folder-open": _iconsMaterial.FolderOpen,
  "format-paint": _iconsMaterial.FormatPaint,
  "group-work": _iconsMaterial.GroupWork,
  headset: _iconsMaterial.Headset,
  healing: _iconsMaterial.Healing,
  home: _iconsMaterial.Home,
  hotel: _iconsMaterial.Hotel,
  https: _iconsMaterial.Https,
  inbox2: _iconsMaterial.MoveToInbox,
  info: _iconsMaterial.Info,
  "info-outline": _iconsMaterial.InfoOutlined,
  "insert-chart": _iconsMaterial.InsertChart,
  "keyboard-alt": _iconsMaterial.TagFacesOutlined,
  label: _iconsMaterial.Label,
  "label-outline": _iconsMaterial.LabelOutlined,
  language: _iconsMaterial.Language,
  lens: _iconsMaterial.Lens,
  link: _iconsMaterial.Link,
  "live-help": _iconsMaterial.LiveHelp,
  "local-atm": _iconsMaterial.LocalAtm,
  "local-attraction": _iconsMaterial.LocalActivity,
  "local-bar": _iconsMaterial.LocalBar,
  "local-cafe": _iconsMaterial.LocalCafe,
  "local-drink": _iconsMaterial.LocalDrink,
  "local-florist": _iconsMaterial.LocalFlorist,
  "local-gas-station": _iconsMaterial.LocalGasStation,
  "local-laundry-service": _iconsMaterial.LocalLaundryService,
  "local-offer": _iconsMaterial.LocalOffer,
  "local-restaurant": _iconsMaterial.LocalDining,
  "local-shipping": _iconsMaterial.LocalShipping,
  "lock-open": _iconsMaterial.LockOpen,
  "lock-outline": _iconsMaterial.LockOutlined,
  loupe: _iconsMaterial.Loupe,
  loyalty: _iconsMaterial.Loyalty,
  mail: _iconsMaterial.Mail,
  map: _iconsMaterial.Map,
  "markunread-mailbox": _iconsMaterial.MarkunreadMailbox,
  mouse: _iconsMaterial.Mouse,
  "my-library-add": _iconsMaterial.LibraryAdd,
  "my-library-books": _iconsMaterial.LibraryBooks,
  "my-library-mus": _iconsMaterial.LibraryMusic,
  nature: _iconsMaterial.Nature,
  "nature-people": _iconsMaterial.NaturePeople,
  "note-add": _iconsMaterial.NoteAdd,
  notifications: _iconsMaterial.Notifications,
  "notifications-none": _iconsMaterial.NotificationsNone,
  "open-with": _iconsMaterial.OpenWith,
  pageview: _iconsMaterial.Pageview,
  "party-mode": _iconsMaterial.PartyMode,
  "pause-circle-fill": _iconsMaterial.PauseCircle,
  "perm-camera-m": _iconsMaterial.PermCameraMic,
  "perm-contact-cal": _iconsMaterial.PermContactCalendar,
  "perm-device-info": _iconsMaterial.PermDeviceInformation,
  "perm-media": _iconsMaterial.PermMedia,
  "picture-in-picture": _iconsMaterial.PictureInPicture,
  "play-circle-fill": _iconsMaterial.PlayCircle,
  "play-download": _iconsMaterial.Download,
  "play-install": _iconsMaterial.DownloadDone,
  print: _iconsMaterial.Print,
  publ: _iconsMaterial.Public,
  radio: _iconsMaterial.Radio,
  "radio-button-off": _iconsMaterial.RadioButtonUnchecked,
  "radio-button-on": _iconsMaterial.RadioButtonChecked,
  receipt: _iconsMaterial.Receipt,
  "recent-actors": _iconsMaterial.RecentActors,
  "remove-circle": _iconsMaterial.RemoveCircle,
  "remove-circle-outline": _iconsMaterial.RemoveCircleOutline,
  report: _iconsMaterial.Report,
  room: _iconsMaterial.Room,
  school: _iconsMaterial.School,
  "settings-applications": _iconsMaterial.SettingsApplications,
  share: _iconsMaterial.Share,
  shop: _iconsMaterial.Shop,
  "shop-two": _iconsMaterial.ShopTwo,
  "shopping-basket": _iconsMaterial.ShoppingBasket,
  "shopping-cart": _iconsMaterial.ShoppingCart,
  speaker: _iconsMaterial.Speaker,
  stars: _iconsMaterial.Stars,
  "stay-current-landscape": _iconsMaterial.StayCurrentLandscape,
  "stay-current-portrait": _iconsMaterial.StayCurrentPortrait,
  straighten: _iconsMaterial.Straighten,
  style: _iconsMaterial.Style,
  subtitles: _iconsMaterial.Subtitles,
  "supervisor-account": _iconsMaterial.SupervisedUserCircle,
  "swap-vert-circle": _iconsMaterial.SwapVerticalCircle,
  tab: _iconsMaterial.Tab,
  "tab-unselected": _iconsMaterial.TabUnselected,
  "tablet-mac": _iconsMaterial.TabletMac,
  theaters: _iconsMaterial.Theaters,
  "thumb-down": _iconsMaterial.ThumbDown,
  "thumb-up": _iconsMaterial.ThumbUp,
  traff: _iconsMaterial.Traffic,
  "verified-user": _iconsMaterial.VerifiedUser,
  "video-collection": _iconsMaterial.VideoLibrary,
  videocam: _iconsMaterial.Videocam,
  "view-array": _iconsMaterial.ViewArray,
  "view-carousel": _iconsMaterial.ViewCarousel,
  "view-column": _iconsMaterial.ViewColumn,
  "view-day": _iconsMaterial.ViewDay,
  "view-thumbnails": _iconsMaterial.ViewComfy,
  "view-week": _iconsMaterial.ViewWeek,
  visibility: _iconsMaterial.Visibility,
  "visibility-off": _iconsMaterial.VisibilityOff,
  "volume-down": _iconsMaterial.VolumeDown,
  watch: _iconsMaterial.Watch,
  "wb-incandescent": _iconsMaterial.WbIncandescent,
  "wb-sunny": _iconsMaterial.WbSunny,
  whatshot: _iconsMaterial.Whatshot,
  work: _iconsMaterial.Work,
  //Entity type Icons
  project: _iconsMaterial.Lens,
  list: _iconsMaterial.List,
  group: _iconsMaterial.Group,
  asset: _iconsMaterial.Layers,
  //Custom Icons
  milestone: MilestoneIcon,
  asset_build: AssetBuildIcon
};
exports.ICONS = ICONS;