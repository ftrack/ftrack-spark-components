"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ObjectTypeIcon", {
  enumerable: true,
  get: function get() {
    return _object_type_icon.default;
  }
});
Object.defineProperty(exports, "EntityTypeIcon", {
  enumerable: true,
  get: function get() {
    return _entity_type_icon.default;
  }
});

var _object_type_icon = _interopRequireDefault(require("./object_type_icon"));

var _entity_type_icon = _interopRequireDefault(require("./entity_type_icon"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }