"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.ErrorMessage = exports.ClusterCreateAssetVersion = exports.ClusterManageReviewSession = exports.Asset = exports.ReviewSessionObject = exports.ReviewSession = exports.Note = exports.AssetVersion = exports.ObjectStatus = exports.Project = exports.EventActivityItem = void 0;

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.object.assign.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _classnames = _interopRequireDefault(require("classnames"));

var _recompose = require("recompose");

var _button = _interopRequireDefault(require("react-toolbox/lib/button"));

var _reactIntl = require("react-intl");

var _reactSizeme = _interopRequireDefault(require("react-sizeme"));

var _style = _interopRequireDefault(require("./style.scss"));

var _hoc = require("../util/hoc");

var _entity_avatar = _interopRequireDefault(require("../entity_avatar"));

var _markdown = _interopRequireDefault(require("../markdown"));

var _ftrack_icon = _interopRequireDefault(require("../ftrack_icon"));

var _thumbnail = _interopRequireDefault(require("../image/thumbnail"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _get_media_component = require("../util/get_media_component");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var messages = (0, _reactIntl.defineMessages)({
  "activity-message-project-create": {
    "id": "ftrack-spark-components-activity-message-project-create",
    "defaultMessage": "{user} created the project {link}"
  },
  "activity-message-project-first-entry": {
    "id": "ftrack-spark-components-activity-message-project-first-entry",
    "defaultMessage": "This is the beginning of your project. While the project develops, this view will give you a historical overview of what has happened and when."
  },
  "activity-message-added-note-to-review": {
    "id": "ftrack-spark-components-activity-message-added-note-to-review",
    "defaultMessage": "{user} gave feedback on {link} in {reviewSession}"
  },
  "activity-message-added-note-to": {
    "id": "ftrack-spark-components-activity-message-added-note-to",
    "defaultMessage": "{user} gave feedback on {link}"
  },
  "activity-message-added-new-version": {
    "id": "ftrack-spark-components-activity-message-added-new-version",
    "defaultMessage": "{user} added a new version of {link}"
  },
  "activity-message-added-new-first-asset": {
    "id": "ftrack-spark-components-activity-message-added-new-first-asset",
    "defaultMessage": "{user} added an asset {link}"
  },
  "activity-unknown-user": {
    "id": "ftrack-spark-components-activity-unknown-user",
    "defaultMessage": "Unknown"
  },
  "activity-message-review-session-object-status-approved": {
    "id": "ftrack-spark-components-activity-message-review-session-object-status-approved",
    "defaultMessage": "{user} approved {link}"
  },
  "activity-message-review-session-object-status-require_changes": {
    "id": "ftrack-spark-components-activity-message-review-session-object-status-require_changes",
    "defaultMessage": "{user} require changes on {link}"
  },
  "activity-message-review-session-object-status-seen": {
    "id": "ftrack-spark-components-activity-message-review-session-object-status-seen",
    "defaultMessage": "{user} has seen {link}"
  },
  "activity-message-review-session-object-status-removed": {
    "id": "ftrack-spark-components-activity-message-review-session-object-status-removed",
    "defaultMessage": "{user} changed the status on {link}"
  },
  "activity-message-created-review-session": {
    "id": "ftrack-spark-components-activity-message-created-review-session",
    "defaultMessage": "{user} created {link}"
  },
  "activity-message-created-review-session-object": {
    "id": "ftrack-spark-components-activity-message-created-review-session-object",
    "defaultMessage": "{user} added {reviewSessionObjectLink} to {reviewSessionLink}"
  },
  "activity-message-asset-changed": {
    "id": "ftrack-spark-components-activity-message-asset-changed",
    "defaultMessage": "{user} changed {link}"
  },
  "activity-message-asset-renamed": {
    "id": "ftrack-spark-components-activity-message-asset-renamed",
    "defaultMessage": "{user} renamed {link}"
  },
  "activity-message-asset-moved": {
    "id": "ftrack-spark-components-activity-message-asset-moved",
    "defaultMessage": "{user} moved {link}"
  },
  "activity-message-asset-moved-and-renamed": {
    "id": "ftrack-spark-components-activity-moved-and-renamed",
    "defaultMessage": "{user} moved and renamed {link}"
  },
  "activity-message-cluster-manage-session-created": {
    "id": "ftrack-spark-components-activity-message-cluster-manage-session-created",
    "defaultMessage": "{user} created {reviewSessionLink} with {assetsLink}"
  },
  "activity-message-cluster-manage-session-added": {
    "id": "ftrack-spark-components-activity-message-cluster-manage-session-added",
    "defaultMessage": "{user} added {assetsLink} to {reviewSessionLink}"
  },
  "activity-message-cluster-created-asset-version": {
    "id": "ftrack-spark-components-activity-message-cluster-created-asset-version",
    "defaultMessage": "{user} added {assetsLink}"
  },
  "activity-message-cluster-manage-session-number-of-assets": {
    "id": "ftrack-spark-components-activity-message-cluster-manage-session-number-of-assets",
    "defaultMessage": "{numberOfAssets, plural, =0 {no assets} one {one asset} other {# assets}}"
  },
  "activity-message-error-not-interpret": {
    "id": "ftrack-spark-components-activity-message-error-not-interpret",
    "defaultMessage": "We found an activity that we could not interpret."
  },
  "activity-message-error-contact-support": {
    "id": "ftrack-spark-components-activity-contact-support",
    "defaultMessage": "Contact our support for more information."
  },
  "activity-see-more-button": {
    "id": "ftrack-spark-components.activity-see-more-button",
    "defaultMessage": "See more"
  }
});

var projectShape = _propTypes.default.shape({
  name: _propTypes.default.string.isRequired,
  type: _propTypes.default.string.isRequired,
  id: _propTypes.default.string.isRequired
});

var reviewSessionObjectShape = _propTypes.default.shape({
  name: _propTypes.default.string.isRequired,
  type: _propTypes.default.string.isRequired,
  id: _propTypes.default.string.isRequired
});

var reviewSessionShape = _propTypes.default.shape({
  name: _propTypes.default.string.isRequired,
  id: _propTypes.default.string.isRequired
});

var assetVersionShape = _propTypes.default.shape({
  name: _propTypes.default.string.isRequired,
  type: _propTypes.default.string.isRequired,
  id: _propTypes.default.string.isRequired,
  thumbnail_id: _propTypes.default.string
});

var assetShape = _propTypes.default.shape({
  name: _propTypes.default.string.isRequired,
  id: _propTypes.default.string.isRequired
});

var noteEntityShape = _propTypes.default.oneOfType([projectShape, reviewSessionObjectShape, assetVersionShape]);

var EntityAvatarWithSession = (0, _hoc.withSession)(_entity_avatar.default);

var PreviewImage = /*#__PURE__*/function (_Component) {
  _inherits(PreviewImage, _Component);

  var _super = _createSuper(PreviewImage);

  function PreviewImage(props) {
    var _this;

    _classCallCheck(this, PreviewImage);

    _this = _super.call(this, props);
    _this.onClick = _this.onClick.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(PreviewImage, [{
    key: "onClick",
    value: function onClick() {
      this.props.onClick([this.props.entity], "preview");
    }
  }, {
    key: "render",
    value: function render() {
      var entity = this.props.entity;
      var url = (0, _get_media_component.getEntityThumbnail)(entity);
      return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        onClick: this.onClick,
        className: _style.default["image-container"],
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_thumbnail.default, {
          rounded: true,
          src: url,
          size: "tiny",
          className: _style.default.thumbnail
        })
      });
    }
  }]);

  return PreviewImage;
}(_react.Component);

PreviewImage.propTypes = {
  entity: _propTypes.default.shape({
    thumbnail_url: _propTypes.default.shape({
      url: _propTypes.default.string
    }),
    thumbnail_id: _propTypes.default.string,
    name: _propTypes.default.string,
    id: _propTypes.default.string,
    type: _propTypes.default.string
  }).isRequired,
  onClick: _propTypes.default.func.isRequired
};

function User_(_ref) {
  var user = _ref.user,
      intl = _ref.intl;

  if (!user) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      children: intl.formatMessage(messages["activity-unknown-user"])
    });
  }

  if (user.name) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      children: user.name
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
    children: "".concat(user.first_name, " ").concat(user.last_name)
  });
}

User_.propTypes = {
  user: _propTypes.default.oneOfType([_propTypes.default.shape({
    first_name: _propTypes.default.string.isRequired,
    last_name: _propTypes.default.string.isRequired
  }), _propTypes.default.shape({
    name: _propTypes.default.string.isRequired
  })]),
  intl: _reactIntl.intlShape.isRequired
};
var User = (0, _safe_inject_intl.default)(User_);

function EventActivityItem_(_ref2) {
  var children = _ref2.children,
      createdAt = _ref2.createdAt,
      icon = _ref2.icon,
      createdBy = _ref2.createdBy,
      previews = _ref2.previews,
      intl = _ref2.intl,
      onClick = _ref2.onClick,
      small = _ref2.small,
      firstMessage = _ref2.firstMessage;
  var avatar;

  if (createdBy) {
    avatar = /*#__PURE__*/(0, _jsxRuntime.jsx)(EntityAvatarWithSession, {
      entity: createdBy
    });
  } else if (firstMessage) {
    avatar = /*#__PURE__*/(0, _jsxRuntime.jsx)(_ftrack_icon.default, {
      className: _style.default["icon-logo"]
    });
  }

  var date;
  var dateTime;

  if (createdBy) {
    date = createdAt.toDate();
    dateTime = "".concat(intl.formatTime(date), " ").concat(intl.formatDate(date));
  }

  var formattedDate = /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedDate, {
      value: date,
      year: "numeric",
      day: "2-digit",
      month: "2-digit"
    }), " - ", /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedTime, {
      value: date,
      hour: "numeric",
      minute: "2-digit"
    })]
  });
  var previewElements = previews.map(function (entity, index) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(PreviewImage, {
      entity: entity,
      onClick: onClick
    }, "".concat(index, "-entity-").concat(entity.id));
  });
  var bodyClasses = (0, _classnames.default)(_style.default["activity-item-body"], small && _style.default.small);
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: _style.default["activity-item"],
    children: [!small ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
      className: _style.default.icon,
      value: icon
    }) : null, avatar, /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: bodyClasses,
      children: [children, previewElements.length ? /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _style.default.images,
        children: previewElements
      }) : null, createdBy ? /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _style.default.info,
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
          title: dateTime,
          className: _style.default.date,
          children: formattedDate
        })
      }) : null]
    })]
  });
}

EventActivityItem_.propTypes = {
  children: _propTypes.default.oneOfType([_propTypes.default.arrayOf(_propTypes.default.node), _propTypes.default.node]).isRequired,
  createdAt: _propTypes.default.object.isRequired,
  // eslint-disable-line
  icon: _propTypes.default.string.isRequired,
  createdBy: _propTypes.default.object.isRequired,
  // eslint-disable-line
  previews: _propTypes.default.arrayOf(_propTypes.default.shape({
    thumbnail_id: _propTypes.default.string,
    name: _propTypes.default.string.isRequired,
    id: _propTypes.default.string.isRequired,
    type: _propTypes.default.string.isRequired
  })),
  onClick: _propTypes.default.func.isRequired,
  intl: _reactIntl.intlShape.isRequired,
  small: _propTypes.default.bool,
  firstMessage: _propTypes.default.bool
};
EventActivityItem_.defaultProps = {
  previews: []
};
var EventActivityItem = (0, _safe_inject_intl.default)((0, _hoc.withSession)(EventActivityItem_));
exports.EventActivityItem = EventActivityItem;

var LinkRelated = /*#__PURE__*/function (_Component2) {
  _inherits(LinkRelated, _Component2);

  var _super2 = _createSuper(LinkRelated);

  // eslint-disable-line
  function LinkRelated(props) {
    var _this2;

    _classCallCheck(this, LinkRelated);

    _this2 = _super2.call(this, props);
    _this2.state = {};
    _this2.onClick = _this2.onClick.bind(_assertThisInitialized(_this2));
    return _this2;
  }

  _createClass(LinkRelated, [{
    key: "onClick",
    value: function onClick() {
      this.props.onClick(this.props.items, "link");
    }
  }, {
    key: "render",
    value: function render() {
      var label = this.props.label;
      return /*#__PURE__*/(0, _jsxRuntime.jsx)("button", {
        role: "link",
        onClick: this.onClick,
        className: _style.default.link,
        children: label
      });
    }
  }]);

  return LinkRelated;
}(_react.Component);

LinkRelated.propTypes = {
  items: _propTypes.default.arrayOf(_propTypes.default.shape({
    id: _propTypes.default.string,
    type: _propTypes.default.string
  })).isRequired,
  label: _propTypes.default.string.isRequired,
  onClick: _propTypes.default.func.isRequired
};

function Project_(_ref3) {
  var onClick = _ref3.onClick,
      createdAt = _ref3.createdAt,
      user = _ref3.user,
      project = _ref3.project,
      small = _ref3.small;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(EventActivityItem, {
      onClick: onClick,
      icon: "lens",
      createdAt: createdAt,
      createdBy: user,
      small: small,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["activity-message-project-create"]), {}, {
        values: {
          user: /*#__PURE__*/(0, _jsxRuntime.jsx)(User, {
            user: user
          }),
          link: /*#__PURE__*/(0, _jsxRuntime.jsx)(LinkRelated, {
            onClick: onClick,
            label: project.name,
            items: [project]
          })
        }
      }))
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("br", {}), /*#__PURE__*/(0, _jsxRuntime.jsx)(EventActivityItem, {
      onClick: onClick,
      icon: "lens",
      firstMessage: true,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["activity-message-project-first-entry"]))
    })]
  });
}

Project_.propTypes = {
  createdAt: _propTypes.default.object.isRequired,
  // eslint-disable-line
  user: _propTypes.default.shape({
    first_name: _propTypes.default.string.isRequired,
    last_name: _propTypes.default.string.isRequired,
    thumbnail_id: _propTypes.default.string
  }),
  project: projectShape.isRequired,
  onClick: _propTypes.default.func.isRequired,
  small: _propTypes.default.bool
};
var Project = (0, _safe_inject_intl.default)(Project_);
exports.Project = Project;

function ObjectStatus_(_ref4) {
  var onClick = _ref4.onClick,
      createdAt = _ref4.createdAt,
      user = _ref4.user,
      status = _ref4.status,
      reviewSessionObject = _ref4.reviewSessionObject,
      small = _ref4.small;
  var intlString = "activity-message-review-session-object-status-".concat(status || "removed");
  var seenStatus = "activity-message-review-session-object-status-seen";
  var assetVersion = reviewSessionObject.asset_version ? "v".concat(reviewSessionObject.asset_version.version) : "";
  var entityInformationLink = "".concat(reviewSessionObject.name, " ").concat(assetVersion);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(EventActivityItem, {
    onClick: onClick,
    icon: seenStatus ? "people" : "ondemand_video",
    createdAt: createdAt,
    createdBy: user,
    small: small,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages[intlString]), {}, {
      values: {
        user: /*#__PURE__*/(0, _jsxRuntime.jsx)(User, {
          user: user
        }),
        link: /*#__PURE__*/(0, _jsxRuntime.jsx)(LinkRelated, {
          onClick: onClick,
          label: entityInformationLink,
          items: [reviewSessionObject]
        })
      }
    }))
  });
}

ObjectStatus_.propTypes = {
  createdAt: _propTypes.default.object.isRequired,
  // eslint-disable-line
  user: _propTypes.default.shape({
    name: _propTypes.default.isRequired
  }),
  status: _propTypes.default.string.isRequired,
  reviewSessionObject: reviewSessionObjectShape.isRequired,
  onClick: _propTypes.default.func.isRequired,
  small: _propTypes.default.bool
};
var ObjectStatus = (0, _safe_inject_intl.default)(ObjectStatus_);
exports.ObjectStatus = ObjectStatus;

function AssetVersion_(_ref5) {
  var onClick = _ref5.onClick,
      user = _ref5.user,
      createdAt = _ref5.createdAt,
      assetVersion = _ref5.assetVersion,
      small = _ref5.small;
  var entityInformationLink = "";

  if (assetVersion.version && assetVersion.parent) {
    entityInformationLink = "".concat(assetVersion.parent, " / ").concat(assetVersion.name, " v").concat(assetVersion.version);
  } else if (assetVersion.version) {
    entityInformationLink = "".concat(assetVersion.name, " v").concat(assetVersion.version);
  } else {
    entityInformationLink = "".concat(assetVersion.name);
  }

  var firstAsset = assetVersion.version === 1;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(EventActivityItem, {
    onClick: onClick,
    icon: "layers",
    createdAt: createdAt,
    createdBy: user,
    small: small,
    previews: [Object.assign({}, {
      thumbnail_url: assetVersion.thumbnail_url,
      thumbnail_id: assetVersion.thumbnail_id,
      name: assetVersion.name,
      id: assetVersion.id,
      type: assetVersion.type
    })],
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages[firstAsset ? "activity-message-added-new-first-asset" : "activity-message-added-new-version"]), {}, {
      values: {
        user: /*#__PURE__*/(0, _jsxRuntime.jsx)(User, {
          user: user
        }),
        link: /*#__PURE__*/(0, _jsxRuntime.jsx)(LinkRelated, {
          onClick: onClick,
          label: entityInformationLink,
          items: [assetVersion]
        })
      }
    }))
  });
}

AssetVersion_.propTypes = {
  createdAt: _propTypes.default.object.isRequired,
  // eslint-disable-line
  user: _propTypes.default.shape({
    first_name: _propTypes.default.string.isRequired,
    last_name: _propTypes.default.string.isRequired,
    thumbnail_id: _propTypes.default.string
  }),
  assetVersion: assetVersionShape.isRequired,
  onClick: _propTypes.default.func.isRequired,
  small: _propTypes.default.bool
};
var AssetVersion = (0, _safe_inject_intl.default)(AssetVersion_);
exports.AssetVersion = AssetVersion;

function Note_(_ref6) {
  var _classNames;

  var onClick = _ref6.onClick,
      user = _ref6.user,
      createdAt = _ref6.createdAt,
      entity = _ref6.entity,
      small = _ref6.small,
      noteText = _ref6.noteText,
      clicked = _ref6.clicked,
      onButtonClick = _ref6.onButtonClick,
      className = _ref6.className,
      size = _ref6.size,
      onSize = _ref6.onSize;
  var height = size.height;
  var canExpand = height && height > 150;
  var fadeOutText = !clicked && canExpand;
  var classes = (0, _classnames.default)(_style.default["notes-activity"], className, (_classNames = {}, _defineProperty(_classNames, _style.default["note-bottom-fade"], fadeOutText), _defineProperty(_classNames, _style.default["note-bottom-fade-raised"], fadeOutText && small), _defineProperty(_classNames, _style.default["show-entire-note"], clicked), _classNames));
  var version = entity.version ? "v".concat(entity.version) : "";
  var entityInformationLink = "".concat(entity.name, " ").concat(version);
  var reviewSessionName = entity.review_session ? entity.review_session.name : "";
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(EventActivityItem, {
    onClick: onClick,
    icon: "feedback",
    createdAt: createdAt,
    createdBy: user,
    small: small,
    children: [reviewSessionName ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["activity-message-added-note-to-review"]), {}, {
      values: {
        user: /*#__PURE__*/(0, _jsxRuntime.jsx)(User, {
          user: user
        }),
        link: /*#__PURE__*/(0, _jsxRuntime.jsx)(LinkRelated, {
          onClick: onClick,
          label: entityInformationLink,
          items: [entity]
        }),
        reviewSession: /*#__PURE__*/(0, _jsxRuntime.jsx)(LinkRelated, {
          label: reviewSessionName,
          onClick: onClick,
          items: [{
            type: entity.review_session.__entity_type__,
            id: entity.review_session.id
          }]
        })
      }
    })) : /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["activity-message-added-note-to"]), {}, {
      values: {
        user: /*#__PURE__*/(0, _jsxRuntime.jsx)(User, {
          user: user
        }),
        link: /*#__PURE__*/(0, _jsxRuntime.jsx)(LinkRelated, {
          onClick: onClick,
          label: entityInformationLink,
          items: [entity]
        })
      }
    })), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: classes,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(MeasuredMarkdown, {
        source: noteText || "",
        onSize: onSize
      })
    }), !clicked && canExpand ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_button.default, {
      className: _style.default["note-button"],
      onClick: onButtonClick,
      label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["activity-see-more-button"]))
    }) : null]
  });
}

Note_.propTypes = {
  createdAt: _propTypes.default.object.isRequired,
  // eslint-disable-line
  user: _propTypes.default.shape({
    name: _propTypes.default.isRequired
  }),
  entity: noteEntityShape.isRequired,
  onClick: _propTypes.default.func.isRequired,
  small: _propTypes.default.bool,
  clicked: _propTypes.default.bool,
  noteText: _propTypes.default.string,
  className: _propTypes.default.string,
  onButtonClick: _propTypes.default.func,
  onSize: _propTypes.default.func,
  // eslint-disable-next-line react/forbid-prop-types
  size: _propTypes.default.object
};
var MeasuredMarkdown = (0, _reactSizeme.default)({
  monitorHeight: true
})(_markdown.default);
var Note = (0, _recompose.compose)((0, _recompose.withState)("clicked", "showFullText", false), (0, _recompose.withState)("size", "onSize", {}), (0, _recompose.withHandlers)({
  onButtonClick: function onButtonClick(props) {
    return function () {
      return props.showFullText(!props.clicked);
    };
  }
}), _safe_inject_intl.default)(Note_);
exports.Note = Note;

function ReviewSession_(_ref7) {
  var onClick = _ref7.onClick,
      user = _ref7.user,
      createdAt = _ref7.createdAt,
      reviewSession = _ref7.reviewSession,
      small = _ref7.small;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(EventActivityItem, {
    onClick: onClick,
    icon: "ondemand_video",
    createdAt: createdAt,
    createdBy: user,
    small: small,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["activity-message-created-review-session"]), {}, {
      values: {
        user: /*#__PURE__*/(0, _jsxRuntime.jsx)(User, {
          user: user
        }),
        link: /*#__PURE__*/(0, _jsxRuntime.jsx)(LinkRelated, {
          onClick: onClick,
          label: reviewSession.name,
          items: [reviewSession]
        })
      }
    }))
  });
}

ReviewSession_.propTypes = {
  createdAt: _propTypes.default.object.isRequired,
  // eslint-disable-line
  user: _propTypes.default.shape({
    first_name: _propTypes.default.string.isRequired,
    last_name: _propTypes.default.string.isRequired,
    thumbnail_id: _propTypes.default.string
  }),
  reviewSession: reviewSessionShape.isRequired,
  onClick: _propTypes.default.func.isRequired,
  small: _propTypes.default.bool
};
var ReviewSession = (0, _safe_inject_intl.default)(ReviewSession_);
exports.ReviewSession = ReviewSession;

function ReviewSessionObject_(_ref8) {
  var onClick = _ref8.onClick,
      user = _ref8.user,
      createdAt = _ref8.createdAt,
      reviewSessionObject = _ref8.reviewSessionObject,
      reviewSession = _ref8.reviewSession,
      small = _ref8.small;
  var assetVersion = reviewSessionObject.version ? "v".concat(reviewSessionObject.version) : "";
  var entityInformationLink = reviewSessionObject.parent ? "".concat(reviewSessionObject.parent, " / ").concat(reviewSessionObject.name, " ").concat(assetVersion) : "".concat(reviewSessionObject.name, " ").concat(assetVersion);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(EventActivityItem, {
    onClick: onClick,
    icon: "ondemand_video",
    createdAt: createdAt,
    createdBy: user,
    small: small,
    previews: [Object.assign({}, {
      thumbnail_url: reviewSessionObject.thumbnail_url,
      thumbnail_id: reviewSessionObject.thumbnail_id,
      name: reviewSessionObject.name,
      id: reviewSessionObject.id,
      type: reviewSessionObject.type
    })],
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["activity-message-created-review-session-object"]), {}, {
      values: {
        user: /*#__PURE__*/(0, _jsxRuntime.jsx)(User, {
          user: user
        }),
        reviewSessionObjectLink: /*#__PURE__*/(0, _jsxRuntime.jsx)(LinkRelated, {
          onClick: onClick,
          label: entityInformationLink,
          items: [reviewSessionObject]
        }),
        reviewSessionLink: /*#__PURE__*/(0, _jsxRuntime.jsx)(LinkRelated, {
          onClick: onClick,
          label: reviewSession.name,
          items: [reviewSession]
        })
      }
    }))
  });
}

ReviewSessionObject_.propTypes = {
  createdAt: _propTypes.default.object.isRequired,
  // eslint-disable-line
  user: _propTypes.default.shape({
    first_name: _propTypes.default.string.isRequired,
    last_name: _propTypes.default.string.isRequired,
    thumbnail_id: _propTypes.default.string
  }),
  reviewSession: reviewSessionShape.isRequired,
  reviewSessionObject: reviewSessionObjectShape.isRequired,
  onClick: _propTypes.default.func.isRequired,
  small: _propTypes.default.bool
};
var ReviewSessionObject = (0, _safe_inject_intl.default)(ReviewSessionObject_);
exports.ReviewSessionObject = ReviewSessionObject;

function Asset_(_ref9) {
  var onClick = _ref9.onClick,
      user = _ref9.user,
      createdAt = _ref9.createdAt,
      asset = _ref9.asset,
      isMove = _ref9.isMove,
      isRename = _ref9.isRename,
      small = _ref9.small;
  var action = "changed";

  if (isMove) {
    action = "moved";
  }

  if (isRename) {
    action = "renamed";
  }

  if (isMove && isRename) {
    action = "moved-and-renamed";
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(EventActivityItem, {
    onClick: onClick,
    icon: "layers",
    createdAt: createdAt,
    createdBy: user,
    small: small,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["activity-message-asset-".concat(action)]), {}, {
      values: {
        user: /*#__PURE__*/(0, _jsxRuntime.jsx)(User, {
          user: user
        }),
        link: /*#__PURE__*/(0, _jsxRuntime.jsx)(LinkRelated, {
          onClick: onClick,
          label: asset.name,
          items: [asset]
        })
      }
    }))
  });
}

Asset_.propTypes = {
  createdAt: _propTypes.default.object.isRequired,
  // eslint-disable-line
  user: _propTypes.default.shape({
    first_name: _propTypes.default.string.isRequired,
    last_name: _propTypes.default.string.isRequired,
    thumbnail_id: _propTypes.default.string
  }),
  asset: assetShape.isRequired,
  isMove: _propTypes.default.bool,
  isRename: _propTypes.default.bool,
  onClick: _propTypes.default.func.isRequired,
  small: _propTypes.default.bool
};
var Asset = (0, _safe_inject_intl.default)(Asset_);
exports.Asset = Asset;

function ClusterManageReviewSession_(_ref10) {
  var onClick = _ref10.onClick,
      user = _ref10.user,
      createdAt = _ref10.createdAt,
      items = _ref10.items,
      intl = _ref10.intl,
      small = _ref10.small;
  var mapOnType = items.reduce(function (accumulator, _ref11) {
    var type = _ref11.type,
        data = _ref11.data;

    if (!accumulator[type]) {
      accumulator[type] = [];
    }

    accumulator[type].push(data);
    return accumulator;
  }, {});
  var previews = mapOnType.review_session_object.map(function (data) {
    return Object.assign({}, {
      thumbnail_url: data.reviewSessionObject.thumbnail_url,
      thumbnail_id: data.reviewSessionObject.thumbnail_id,
      name: data.reviewSessionObject.name,
      id: data.reviewSessionObject.id,
      type: data.reviewSessionObject.type
    });
  });
  var content;

  if (mapOnType.review_session && mapOnType.review_session.length) {
    // Review session has been created in this cluster.
    var _mapOnType$review_ses = _slicedToArray(mapOnType.review_session, 1),
        reviewSession = _mapOnType$review_ses[0].reviewSession;

    content = /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["activity-message-cluster-manage-session-created"]), {}, {
      values: {
        user: /*#__PURE__*/(0, _jsxRuntime.jsx)(User, {
          user: user
        }),
        reviewSessionLink: /*#__PURE__*/(0, _jsxRuntime.jsx)(LinkRelated, {
          onClick: onClick,
          items: [reviewSession],
          label: reviewSession.name
        }),
        assetsLink: /*#__PURE__*/(0, _jsxRuntime.jsx)(LinkRelated, {
          onClick: onClick,
          items: [reviewSession],
          label: intl.formatMessage(messages["activity-message-cluster-manage-session-number-of-assets"], {
            numberOfAssets: mapOnType.review_session_object && mapOnType.review_session_object.length || 0
          })
        })
      }
    }));
  } else {
    // No review session has been created in this cluster.
    var _mapOnType$review_ses2 = _slicedToArray(mapOnType.review_session_object, 1),
        _reviewSession = _mapOnType$review_ses2[0].reviewSession;

    content = /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["activity-message-cluster-manage-session-added"]), {}, {
      values: {
        user: /*#__PURE__*/(0, _jsxRuntime.jsx)(User, {
          user: user
        }),
        assetsLink: /*#__PURE__*/(0, _jsxRuntime.jsx)(LinkRelated, {
          onClick: onClick,
          items: [_reviewSession],
          label: intl.formatMessage(messages["activity-message-cluster-manage-session-number-of-assets"], {
            numberOfAssets: mapOnType.review_session_object && mapOnType.review_session_object.length || 0
          })
        }),
        reviewSessionLink: /*#__PURE__*/(0, _jsxRuntime.jsx)(LinkRelated, {
          onClick: onClick,
          items: [_reviewSession],
          label: _reviewSession.name
        })
      }
    }));
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(EventActivityItem, {
    onClick: onClick,
    icon: "ondemand_video",
    createdAt: createdAt,
    createdBy: user,
    previews: previews,
    small: small,
    children: content
  });
}

ClusterManageReviewSession_.propTypes = {
  createdAt: _propTypes.default.object.isRequired,
  // eslint-disable-line
  user: _propTypes.default.shape({
    first_name: _propTypes.default.string.isRequired,
    last_name: _propTypes.default.string.isRequired,
    thumbnail_id: _propTypes.default.string.isRequired
  }),
  items: _propTypes.default.array,
  // eslint-disable-line
  intl: _reactIntl.intlShape.isRequired,
  onClick: _propTypes.default.func.isRequired,
  small: _propTypes.default.bool
};
var ClusterManageReviewSession = (0, _safe_inject_intl.default)(ClusterManageReviewSession_);
exports.ClusterManageReviewSession = ClusterManageReviewSession;

function ClusterCreateAssetVersion_(_ref12) {
  var onClick = _ref12.onClick,
      user = _ref12.user,
      createdAt = _ref12.createdAt,
      items = _ref12.items,
      intl = _ref12.intl,
      small = _ref12.small;
  var previews = items.map(function (item) {
    return Object.assign({}, {
      thumbnail_url: item.data.assetVersion.thumbnail_url,
      thumbnail_id: item.data.assetVersion.thumbnail_id,
      name: item.data.assetVersion.name,
      id: item.data.assetVersion.id,
      type: item.data.assetVersion.type
    });
  });
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(EventActivityItem, {
    onClick: onClick,
    icon: "ondemand_video",
    createdAt: createdAt,
    createdBy: user,
    small: small,
    previews: previews,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["activity-message-cluster-created-asset-version"]), {}, {
      values: {
        user: /*#__PURE__*/(0, _jsxRuntime.jsx)(User, {
          user: user
        }),
        assetsLink: /*#__PURE__*/(0, _jsxRuntime.jsx)(LinkRelated, {
          onClick: onClick,
          items: items,
          label: intl.formatMessage(messages["activity-message-cluster-manage-session-number-of-assets"], {
            numberOfAssets: items && items.length || 0
          })
        })
      }
    }))
  });
}

ClusterCreateAssetVersion_.propTypes = {
  createdAt: _propTypes.default.object.isRequired,
  // eslint-disable-line
  user: _propTypes.default.shape({
    first_name: _propTypes.default.string.isRequired,
    last_name: _propTypes.default.string.isRequired,
    thumbnail_id: _propTypes.default.string.isRequired
  }),
  items: _propTypes.default.array,
  // eslint-disable-line
  intl: _reactIntl.intlShape.isRequired,
  onClick: _propTypes.default.func.isRequired,
  small: _propTypes.default.bool
};
var ClusterCreateAssetVersion = (0, _safe_inject_intl.default)(ClusterCreateAssetVersion_);
exports.ClusterCreateAssetVersion = ClusterCreateAssetVersion;

function ErrorMessage_(_ref13) {
  var small = _ref13.small;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(EventActivityItem, {
    icon: "error_outline",
    small: small,
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default["error-message"],
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["activity-message-error-not-interpret"]))
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["activity-message-error-contact-support"]))
    })]
  });
}

ErrorMessage_.propTypes = {
  small: _propTypes.default.bool
};
var ErrorMessage = (0, _safe_inject_intl.default)(ErrorMessage_); // TODO: Feel free to fix this eslint error if you got time
// eslint-disable-next-line import/no-anonymous-default-export

exports.ErrorMessage = ErrorMessage;
var _default = {
  object_status: ObjectStatus,
  asset_version: AssetVersion,
  note: Note,
  review_session: ReviewSession,
  review_session_object: ReviewSessionObject,
  cluster_manage_review_session: ClusterManageReviewSession,
  cluster_add_asset_version: ClusterCreateAssetVersion,
  project: Project,
  asset: Asset,
  error: ErrorMessage
};
exports.default = _default;