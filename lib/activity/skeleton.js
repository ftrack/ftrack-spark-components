"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _skeleton = _interopRequireDefault(require("../skeleton"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2018 ftrack
var ActivitySkeleton = function ActivitySkeleton() {
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: _style.default["activity-skeleton"],
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {
      className: _style.default["avatar-skeleton"]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default["activity-skeleton-body"],
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {}), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: _style.default["skeleton-row"],
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {
          className: _style.default["icon-skeleton"]
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default["date-skeleton-container"],
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {})
        })]
      })]
    })]
  });
};

var _default = ActivitySkeleton;
exports.default = _default;