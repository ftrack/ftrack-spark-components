"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.weak-map.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.EntityToggle = exports.EntityThumbnail = exports.VersionsList = exports.EntityVersionSelector = exports.ListItemChips = exports.ListItemChip = exports.EntityStatusBar = exports.EntityListItemRow = exports.EntityListItemContent = exports.ReviewEntityListItem = exports.StudioEntityListItem = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.array.slice.js");

var React = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _useOnclickoutside = _interopRequireDefault(require("use-onclickoutside"));

var _material = require("@mui/material");

var _MoreVert = _interopRequireDefault(require("@mui/icons-material/MoreVert"));

var _Add = _interopRequireDefault(require("@mui/icons-material/Add"));

var _Delete = _interopRequireDefault(require("@mui/icons-material/Delete"));

var _Layers = _interopRequireDefault(require("@mui/icons-material/Layers"));

var _reactIntl = require("react-intl");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _checkbox = _interopRequireDefault(require("react-toolbox/lib/checkbox"));

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _tooltip = _interopRequireDefault(require("react-toolbox/lib/tooltip"));

var _image = require("../image");

var _file_type_icon = _interopRequireDefault(require("../file_type_icon"));

var _breadcrumbs = _interopRequireDefault(require("../breadcrumbs"));

var _button_menu = _interopRequireDefault(require("../button_menu"));

var _style = _interopRequireDefault(require("./style.scss"));

var _checkbox_theme = _interopRequireDefault(require("./checkbox_theme.scss"));

var _tooltip_theme = _interopRequireDefault(require("./tooltip_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["active", "disabled", "disableActiveIndicator", "dragging", "innerRef", "children", "onClick", "disableShadows"],
    _excluded2 = ["active", "selected", "selectable", "media", "canViewInStudio", "onViewInStudio", "canViewInLibrary", "onViewInLibrary", "onRemove", "selectVersion", "versionSelectorChildren", "hideVersionSelector", "canAddVersions", "onAddVersion"],
    _excluded3 = ["active", "selected", "media", "onViewInLibrary", "onDelete"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var DivWithTooltip = (0, _tooltip.default)("div");
var messages = (0, _reactIntl.defineMessages)({
  "menuitem-view-in-studio": {
    "id": "ftrack-spark-components.entity-list-item.menuitem-view-in-studio",
    "defaultMessage": "View in Studio"
  },
  "menuitem-view-in-library": {
    "id": "ftrack-spark-components.entity-list-item.menuitem-view-in-library",
    "defaultMessage": "View in library"
  },
  "menuitem-remove": {
    "id": "ftrack-spark-components.entity-list-item.menuitem-remove",
    "defaultMessage": "Remove"
  },
  "menuitem-addrelatedversions": {
    "id": "ftrack-spark-components.entity-list-item.menuitem-addrelatedversions",
    "defaultMessage": "Add related versions"
  }
});

var ListItemChip = function ListItemChip(_ref) {
  var _classNames2;

  var name = _ref.name,
      color = _ref.color,
      icon = _ref.icon,
      tooltip = _ref.tooltip,
      sentenceCase = _ref.sentenceCase,
      _ref$truncation = _ref.truncation,
      truncation = _ref$truncation === void 0 ? true : _ref$truncation;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: (0, _classnames.default)(_style.default.taskChip, _defineProperty({}, _style.default.taskChipTruncate, truncation)),
    children: [!icon && color ? /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default.taskChipColor,
      style: {
        backgroundColor: color
      }
    }) : /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
      className: _style.default.taskChipIcon,
      value: icon,
      style: {
        color: color
      }
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      title: tooltip || (typeof name === "string" ? name : null),
      className: (0, _classnames.default)(_style.default.taskChipText, (_classNames2 = {}, _defineProperty(_classNames2, _style.default.taskChipTruncate, truncation), _defineProperty(_classNames2, _style.default.sentenceCase, sentenceCase), _classNames2)),
      children: name
    })]
  });
};

exports.ListItemChip = ListItemChip;
ListItemChip.propTypes = {
  name: _propTypes.default.string.isRequired,
  color: _propTypes.default.string.isRequired,
  icon: _propTypes.default.string,
  tooltip: _propTypes.default.string,
  truncation: _propTypes.default.bool,
  sentenceCase: _propTypes.default.bool
};

var ListItemChips = function ListItemChips(_ref2) {
  var _ref2$items = _ref2.items,
      items = _ref2$items === void 0 ? [] : _ref2$items;
  var chips = [];

  if (items.length > 3) {
    chips = items.splice(0, 3);
    chips.push({
      id: items.map(function (r) {
        return r.id;
      }).join("-"),
      name: "+ ".concat(items.length),
      tooltip: items.map(function (r) {
        return r.name;
      }).join(", "),
      truncation: false
    });
  } else {
    chips = items;
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(React.Fragment, {
    children: chips.map(function (_ref3) {
      var id = _ref3.id,
          name = _ref3.name,
          color = _ref3.color,
          icon = _ref3.icon,
          tooltip = _ref3.tooltip,
          truncation = _ref3.truncation;
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(ListItemChip, {
        name: name,
        color: color,
        icon: icon,
        tooltip: tooltip,
        truncation: truncation
      }, id);
    })
  });
};

exports.ListItemChips = ListItemChips;
ListItemChips.propTypes = {
  items: _propTypes.default.arrayOf(_propTypes.default.shape({
    name: _propTypes.default.string.isRequired,
    color: _propTypes.default.string,
    icon: _propTypes.default.string,
    tooltip: _propTypes.default.string
  }))
};

var VersionsList = function VersionsList(_ref4) {
  var versionMenuOpen = _ref4.versionMenuOpen,
      setVersionMenuOpen = _ref4.setVersionMenuOpen,
      versionMenuRef = _ref4.versionMenuRef,
      versions = _ref4.versions,
      selectVersion = _ref4.selectVersion,
      extraChildren = _ref4.extraChildren,
      assetId = _ref4.assetId,
      disableClickOutside = _ref4.disableClickOutside;
  var versionsListRef = (0, React.useRef)(null);
  (0, _useOnclickoutside.default)(versionsListRef, function (e) {
    if (disableClickOutside) {
      return;
    }

    var closestIgnore = e.target.closest("[data-ignoreonclick]");
    var ignoreonclick = closestIgnore && closestIgnore.getAttribute("data-ignoreonclick") === assetId;

    if (!ignoreonclick) {
      setVersionMenuOpen(false);
    }
  });
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.Popper, {
    open: versionMenuOpen,
    anchorEl: versionMenuRef.current,
    placement: "bottom-start",
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      ref: versionsListRef,
      className: _style.default.entityVersionSelectorPopperWrapper,
      style: {
        overflowY: versions.length > 10 ? "scroll" : "inherit"
      },
      children: [versions.map(function (v) {
        return /*#__PURE__*/(0, _jsxRuntime.jsxs)(EntityListItem, {
          onClick: function onClick(e) {
            e.stopPropagation();
            setVersionMenuOpen(!versionMenuOpen);
            selectVersion(v);
          },
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(EntityListItemContent, {
            children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(EntityListItemRow, {
              children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
                className: _style.default.versionTitleContainer,
                children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
                  title: v.name,
                  className: _style.default.entityVersionName,
                  children: v.name
                }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
                  className: _style.default.entityVersionVersion,
                  children: ["v", v.version]
                })]
              }), v.assetVersion && v.assetVersion.task && v.assetVersion.task.type && /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
                className: _style.default.taskChipContainer,
                children: /*#__PURE__*/(0, _jsxRuntime.jsx)(ListItemChip, {
                  name: v.assetVersion.task.name,
                  color: v.assetVersion.task.type.color,
                  icon: "assignment"
                })
              })]
            })
          }), v.assetVersion && v.assetVersion.status && /*#__PURE__*/(0, _jsxRuntime.jsx)(EntityStatusBar, {
            status: v.assetVersion.status
          })]
        }, v.id);
      }), extraChildren]
    })
  });
};

exports.VersionsList = VersionsList;
VersionsList.propTypes = {
  versionMenuOpen: _propTypes.default.bool,
  versionMenuRef: _propTypes.default.any,
  versions: _propTypes.default.array,
  setVersionMenuOpen: _propTypes.default.func,
  selectVersion: _propTypes.default.func,
  extraChildren: _propTypes.default.node,
  assetId: _propTypes.default.string,
  disableClickOutside: _propTypes.default.bool
};

var EntityVersionSelector = function EntityVersionSelector(_ref5) {
  var media = _ref5.media,
      selectVersion = _ref5.selectVersion,
      extraChildren = _ref5.extraChildren,
      disabled = _ref5.disabled,
      largeButton = _ref5.largeButton,
      hideCount = _ref5.hideCount;
  var name = media.name,
      numberOfVersions = media.numberOfVersions,
      version = media.version,
      versions = media.versions;
  var versionMenuRef = (0, React.useRef)(null);

  var _useState = (0, React.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      versionMenuOpen = _useState2[0],
      setVersionMenuOpen = _useState2[1];

  var versionSelectorButtonClasses = (0, _classnames.default)(_style.default.entityVersionSelectorDecoration, _defineProperty({}, _style.default.entityVersionSelectorOpen, versionMenuOpen));
  var versionSelectorClasses = (0, _classnames.default)(_style.default.entityVersionSelector, _defineProperty({}, _style.default.entityVersionSelectorHover, largeButton && !disabled));
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.ClickAwayListener, {
    onClickAway: function onClickAway() {
      if (largeButton && !disabled) {
        setVersionMenuOpen(false);
      }
    },
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: versionSelectorClasses,
      onClick: function onClick(e) {
        e.stopPropagation();

        if (largeButton && !disabled) {
          setVersionMenuOpen(!versionMenuOpen);
        }
      },
      children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        ref: versionMenuRef,
        className: _style.default.entityVersionSelectorInner,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
          title: name,
          className: _style.default.entityVersionName,
          children: name
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
          className: _style.default.entityVersionVersion,
          children: ["v", version]
        }), " ", /*#__PURE__*/(0, _jsxRuntime.jsxs)(React.Fragment, {
          children: [!hideCount && /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
            children: ["(", numberOfVersions, ")"]
          }), !disabled && /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            "data-ignoreonclick": media.id,
            className: versionSelectorButtonClasses,
            onClick: function onClick(e) {
              e.stopPropagation();
              setVersionMenuOpen(!versionMenuOpen);
            },
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
              className: _style.default.entityVersionSelectorDecorationIcon,
              value: !versionMenuOpen ? "expand_more" : "expand_less"
            })
          })]
        })]
      }), versions && /*#__PURE__*/(0, _jsxRuntime.jsx)(VersionsList, {
        disableClickOutside: largeButton,
        versionMenuOpen: versionMenuOpen,
        setVersionMenuOpen: setVersionMenuOpen,
        versionMenuRef: versionMenuRef,
        versions: versions,
        selectVersion: selectVersion,
        assetId: media.id,
        extraChildren: extraChildren
      })]
    })
  });
};

exports.EntityVersionSelector = EntityVersionSelector;
EntityVersionSelector.propTypes = {
  media: _propTypes.default.object,
  selectVersion: _propTypes.default.func,
  extraChildren: _propTypes.default.node,
  disabled: _propTypes.default.bool,
  largeButton: _propTypes.default.bool,
  hideCount: _propTypes.default.bool
};

var EntityListThumbnailWrapper = function EntityListThumbnailWrapper(_ref6) {
  var children = _ref6.children,
      disabled = _ref6.disabled,
      disabledTooltip = _ref6.disabledTooltip;
  return disabled && disabledTooltip ? /*#__PURE__*/(0, _jsxRuntime.jsx)(DivWithTooltip, {
    theme: _tooltip_theme.default,
    className: _style.default.entityListItemThumbnailWrapper,
    tooltipPosition: "top",
    tooltip: disabledTooltip,
    children: children
  }) : /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    children: children
  });
};

EntityListThumbnailWrapper.propTypes = {
  children: _propTypes.default.node,
  disabled: _propTypes.default.bool,
  disabledTooltip: _propTypes.default.node
};

var EntityThumbnail = function EntityThumbnail(_ref7) {
  var media = _ref7.media,
      selectable = _ref7.selectable,
      selected = _ref7.selected,
      disabled = _ref7.disabled,
      disabledTooltip = _ref7.disabledTooltip,
      onClick = _ref7.onClick;
  var thumbnailUrl = media.thumbnailUrl,
      fileType = media.fileType,
      encoding = media.encoding;
  var thumbnailId = media.assetVersion && media.assetVersion.thumbnail_id;
  var selectionStateClasses = (0, _classnames.default)(_style.default.selectionOverlay, _defineProperty({}, _style.default.selectionOverlayShown, selected || disabled));

  var handleClick = function handleClick(_, event) {
    event.preventDefault();
    event.stopPropagation();

    if (onClick) {
      onClick();
    }
  };

  return thumbnailId ? /*#__PURE__*/(0, _jsxRuntime.jsxs)(EntityListThumbnailWrapper, {
    disabled: disabled,
    disabledTooltip: disabledTooltip,
    children: [selectable && /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default.selectionOverlayWrapper,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: selectionStateClasses,
        onClick: function onClick(event) {
          return handleClick(null, event);
        },
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_checkbox.default, {
          theme: _checkbox_theme.default,
          checked: selected || disabled,
          disabled: disabled,
          onChange: handleClick
        })
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default.thumbnailOverlay
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_image.Thumbnail, {
      size: "extra-tiny",
      className: _style.default.thumbnail,
      loading: encoding,
      src: thumbnailId && thumbnailUrl
    })]
  }) : /*#__PURE__*/(0, _jsxRuntime.jsxs)(EntityListThumbnailWrapper, {
    disabled: disabled,
    disabledTooltip: disabledTooltip,
    children: [selectable && /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default.selectionOverlayWrapper,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: selectionStateClasses,
        onClick: handleClick,
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_checkbox.default, {
          theme: _checkbox_theme.default,
          checked: selected || disabled,
          disabled: disabled,
          onClick: handleClick
        })
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default.thumbnailOverlay
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default.thumbnail,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_file_type_icon.default, {
        variant: "tiny",
        label: fileType,
        loading: encoding
      })
    })]
  });
};

exports.EntityThumbnail = EntityThumbnail;
EntityThumbnail.propTypes = {
  media: _propTypes.default.object.isRequired,
  selectable: _propTypes.default.bool,
  selected: _propTypes.default.bool,
  disabled: _propTypes.default.bool,
  disabledTooltip: _propTypes.default.node,
  onClick: _propTypes.default.func
};

var EntityToggle = function EntityToggle(_ref8) {
  var selected = _ref8.selected,
      disabled = _ref8.disabled,
      onClick = _ref8.onClick;

  var handleClick = function handleClick(_, event) {
    event.preventDefault();
    event.stopPropagation();

    if (onClick) {
      onClick();
    }
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: _style.default.entityToggle,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_checkbox.default, {
      theme: _checkbox_theme.default,
      checked: selected,
      disabled: disabled,
      onChange: handleClick
    })
  });
};

exports.EntityToggle = EntityToggle;
EntityToggle.propTypes = {
  selected: _propTypes.default.bool,
  disabled: _propTypes.default.bool,
  onClick: _propTypes.default.func
};

var EntityStatusBar = function EntityStatusBar(_ref9) {
  var status = _ref9.status;
  var StatusBarWithToolTip = (0, _tooltip.default)("div");
  var colorStyle = {
    backgroundColor: status ? status.color : "transparent"
  };
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(StatusBarWithToolTip, {
    className: _style.default.entityStatusBar,
    style: colorStyle,
    tooltip: status.name,
    tooltipPosition: "left"
  });
};

exports.EntityStatusBar = EntityStatusBar;
EntityStatusBar.propTypes = {
  status: _propTypes.default.shape({
    color: _propTypes.default.string.isRequired
  })
};

var EntityListItemContent = function EntityListItemContent(_ref10) {
  var children = _ref10.children;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: _style.default.entityListItemContent,
    children: children
  });
};

exports.EntityListItemContent = EntityListItemContent;
EntityListItemContent.propTypes = {
  children: _propTypes.default.node
};

var EntityListItemRow = function EntityListItemRow(_ref11) {
  var children = _ref11.children,
      className = _ref11.className;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: (0, _classnames.default)(_style.default.entityListItemRow, className),
    children: children
  });
};

exports.EntityListItemRow = EntityListItemRow;
EntityListItemRow.propTypes = {
  children: _propTypes.default.node,
  className: _propTypes.default.string
};

var EntityListItemContextMenu = function EntityListItemContextMenu(_ref12) {
  var children = _ref12.children;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: _style.default.entityListItemMenuButtonWrapper,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_button_menu.default, {
      button: /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.IconButton, {
        size: "small",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_MoreVert.default, {})
      }),
      children: children
    })
  });
};

EntityListItemContextMenu.propTypes = {
  children: _propTypes.default.node
};

var EntityListItem = function EntityListItem(_ref13) {
  var active = _ref13.active,
      disabled = _ref13.disabled,
      disableActiveIndicator = _ref13.disableActiveIndicator,
      dragging = _ref13.dragging,
      innerRef = _ref13.innerRef,
      children = _ref13.children,
      onClick = _ref13.onClick,
      disableShadows = _ref13.disableShadows,
      props = _objectWithoutProperties(_ref13, _excluded);

  var wrapperClasses = (0, _classnames.default)(_style.default.entityListItem, _defineProperty({}, _style.default.active, active), _defineProperty({}, _style.default.disabled, disabled), _defineProperty({}, _style.default.dragging, dragging), _defineProperty({}, _style.default.entityListItemShadow, !disableShadows));

  var handleClick = function handleClick(event) {
    event.preventDefault();
    event.stopPropagation();

    if (onClick) {
      onClick(event);
    }
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("li", _objectSpread(_objectSpread({
    ref: innerRef,
    className: wrapperClasses,
    onClick: handleClick
  }, props), {}, {
    children: [!disableActiveIndicator && /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default.activeIndicator
    }), children]
  }));
};

EntityListItem.propTypes = {
  active: _propTypes.default.bool,
  disabled: _propTypes.default.bool,
  disableActiveIndicator: _propTypes.default.bool,
  dragging: _propTypes.default.bool,
  children: _propTypes.default.node,
  onClick: _propTypes.default.func,
  disableShadows: _propTypes.default.bool
};

var StudioEntityListItem = function StudioEntityListItem(_ref14) {
  var active = _ref14.active,
      selected = _ref14.selected,
      selectable = _ref14.selectable,
      media = _ref14.media,
      canViewInStudio = _ref14.canViewInStudio,
      onViewInStudio = _ref14.onViewInStudio,
      canViewInLibrary = _ref14.canViewInLibrary,
      onViewInLibrary = _ref14.onViewInLibrary,
      onRemove = _ref14.onRemove,
      selectVersion = _ref14.selectVersion,
      versionSelectorChildren = _ref14.versionSelectorChildren,
      hideVersionSelector = _ref14.hideVersionSelector,
      canAddVersions = _ref14.canAddVersions,
      onAddVersion = _ref14.onAddVersion,
      props = _objectWithoutProperties(_ref14, _excluded2);

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(EntityListItem, _objectSpread(_objectSpread({
    active: active,
    selected: selected
  }, props), {}, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(EntityThumbnail, {
      selectable: selectable,
      selected: selected,
      media: media
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(EntityListItemContent, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(EntityListItemRow, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(EntityVersionSelector, {
          media: media,
          selectVersion: selectVersion,
          extraChildren: versionSelectorChildren,
          disabled: hideVersionSelector
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(EntityListItemRow, {
        children: [media.link && /*#__PURE__*/(0, _jsxRuntime.jsx)(_breadcrumbs.default, {
          truncation: "tooltip",
          items: media.link.slice(1).map(function (_ref15) {
            var id = _ref15.id,
                title = _ref15.name;
            return {
              id: id,
              title: title,
              disabled: true
            };
          })
        }), media.assetVersion && media.assetVersion.task && media.assetVersion.task.type && /*#__PURE__*/(0, _jsxRuntime.jsx)(ListItemChip, {
          name: media.assetVersion.task.name,
          color: media.assetVersion.task.type.color,
          icon: "assignment"
        })]
      })]
    }), (canViewInStudio || canViewInLibrary || canAddVersions) && /*#__PURE__*/(0, _jsxRuntime.jsxs)(EntityListItemContextMenu, {
      children: [canViewInStudio && /*#__PURE__*/(0, _jsxRuntime.jsxs)(_material.MenuItem, {
        onClick: onViewInStudio,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_material.ListItemIcon, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Layers.default, {})
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["menuitem-view-in-studio"]))]
      }), canViewInLibrary && /*#__PURE__*/(0, _jsxRuntime.jsxs)(_material.MenuItem, {
        onClick: onViewInLibrary,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_material.ListItemIcon, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Layers.default, {})
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["menuitem-view-in-library"]))]
      }), canAddVersions && /*#__PURE__*/(0, _jsxRuntime.jsxs)(_material.MenuItem, {
        onClick: onRemove,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_material.ListItemIcon, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Delete.default, {})
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["menuitem-remove"]))]
      }), canAddVersions && /*#__PURE__*/(0, _jsxRuntime.jsxs)(_material.MenuItem, {
        onClick: onAddVersion,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_material.ListItemIcon, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Add.default, {})
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["menuitem-addrelatedversions"]))]
      })]
    }), media.assetVersion && media.assetVersion.status && /*#__PURE__*/(0, _jsxRuntime.jsx)(EntityStatusBar, {
      status: media.assetVersion.status
    })]
  }));
};

exports.StudioEntityListItem = StudioEntityListItem;
StudioEntityListItem.propTypes = {
  active: _propTypes.default.bool,
  selected: _propTypes.default.bool,
  selectable: _propTypes.default.bool,
  media: _propTypes.default.object.isRequired,
  canViewInStudio: _propTypes.default.bool,
  canViewInLibrary: _propTypes.default.bool,
  onViewInStudio: _propTypes.default.func,
  onViewInLibrary: _propTypes.default.func,
  onRemove: _propTypes.default.func,
  onAddVersion: _propTypes.default.func,
  selectVersion: _propTypes.default.func,
  hideVersionSelector: _propTypes.default.bool,
  canAddVersions: _propTypes.default.bool,
  versionSelectorChildren: _propTypes.default.node
};
StudioEntityListItem.defaultProps = {
  active: false,
  selected: false,
  selectable: false,
  selectVersion: function selectVersion() {}
};

var ReviewEntityListItem = function ReviewEntityListItem(_ref16) {
  var active = _ref16.active,
      selected = _ref16.selected,
      media = _ref16.media,
      onViewInLibrary = _ref16.onViewInLibrary,
      onDelete = _ref16.onDelete,
      props = _objectWithoutProperties(_ref16, _excluded3);

  var name = media.name,
      version = media.version;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(EntityListItem, _objectSpread(_objectSpread({
    active: active,
    selected: selected
  }, props), {}, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(EntityThumbnail, {
      selected: selected,
      media: media
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(EntityListItemContent, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(EntityListItemRow, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
          className: _style.default.reviewItemName,
          children: name
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(EntityListItemRow, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
          className: _style.default.reviewItemVersion,
          children: ["Version ", version]
        })
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(EntityListItemContextMenu, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(_material.MenuItem, {
        onClick: onViewInLibrary,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_material.ListItemIcon, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Layers.default, {})
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["menuitem-view-in-library"]))]
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_material.MenuItem, {
        onClick: onDelete,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_material.ListItemIcon, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Delete.default, {})
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["menuitem-remove"]))]
      })]
    })]
  }));
};

exports.ReviewEntityListItem = ReviewEntityListItem;
ReviewEntityListItem.propTypes = {
  active: _propTypes.default.bool,
  selected: _propTypes.default.bool,
  media: _propTypes.default.object.isRequired,
  onViewInLibrary: _propTypes.default.func,
  onDelete: _propTypes.default.func
};
ReviewEntityListItem.defaultProps = {
  active: false,
  selected: false
};
(0, _safe_inject_intl.default)(ReviewEntityListItem);
(0, _safe_inject_intl.default)(StudioEntityListItem);
(0, _safe_inject_intl.default)(EntityListItem);
var _default = EntityListItem;
exports.default = _default;