"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _progress_bar = _interopRequireDefault(require("react-toolbox/lib/progress_bar"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["className", "thumbnailUrl", "children", "progress"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function AttachmentItem(_ref) {
  var className = _ref.className,
      thumbnailUrl = _ref.thumbnailUrl,
      children = _ref.children,
      progress = _ref.progress,
      props = _objectWithoutProperties(_ref, _excluded);

  var classes = (0, _classnames.default)(_style.default.attachment, className);
  var imageStyle = thumbnailUrl ? {
    backgroundImage: "url('".concat(thumbnailUrl, "')")
  } : null;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("li", _objectSpread(_objectSpread({
    className: classes,
    style: imageStyle
  }, props), {}, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
      className: _style.default.name,
      children: children
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      className: _style.default.icon,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
        value: "delete",
        style: {
          fontSize: "16px"
        }
      })
    }), progress != null && progress < 100 ? /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      className: _style.default.spinnerWrapper,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_progress_bar.default, {
        className: _style.default.spinner,
        type: "circular",
        mode: progress > 0 ? "determinate" : "indeterminate",
        value: progress
      })
    }) : null]
  }));
}

AttachmentItem.propTypes = {
  onClick: _propTypes.default.func,
  className: _propTypes.default.string,
  thumbnailUrl: _propTypes.default.string,
  children: _propTypes.default.node,
  progress: _propTypes.default.number
};
var _default = AttachmentItem;
exports.default = _default;