"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _attachment_area = _interopRequireDefault(require("./attachment_area"));

var _editable = _interopRequireDefault(require("./editable"));

var _form = _interopRequireDefault(require("./form"));

var _reply = _interopRequireDefault(require("./reply"));

var _note = _interopRequireDefault(require("./note"));

var _note_compact = _interopRequireDefault(require("./note_compact"));

var _note_label_selector = _interopRequireDefault(require("./note_label_selector"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2016 ftrack
// TODO: Feel free to fix this eslint error if you got time
// eslint-disable-next-line import/no-anonymous-default-export
var _default = {
  AttachmentArea: _attachment_area.default,
  EditableNote: _editable.default,
  NoteForm: _form.default,
  ReplyForm: _reply.default,
  Note: _note.default,
  NoteCompact: _note_compact.default,
  NoteLabelSelector: _note_label_selector.default
};
exports.default = _default;