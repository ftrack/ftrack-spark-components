"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactToolbox = require("react-toolbox");

var _reactIntl = require("react-intl");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _style = _interopRequireDefault(require("./style.scss"));

var _form = _interopRequireDefault(require("./form"));

var _reply_theme = _interopRequireDefault(require("./reply_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2016 ftrack

/** Display a reply button or a note form if *collapsed* is false.
 *
 * The *pending* and *content* props are passed to the note form.
 *
 */
var messages = (0, _reactIntl.defineMessages)({
  "attribute-label-reply": {
    "id": "ftrack-spark-components.note-reply.attribute-label-reply",
    "defaultMessage": "Reply"
  }
});

function ReplyForm(_ref) {
  var content = _ref.content,
      recipients = _ref.recipients,
      pending = _ref.pending,
      collapsed = _ref.collapsed,
      onSubmitForm = _ref.onSubmitForm,
      onHideForm = _ref.onHideForm,
      onShowForm = _ref.onShowForm,
      intl = _ref.intl,
      canMention = _ref.canMention;
  var formatMessage = intl.formatMessage;

  if (!collapsed) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_form.default, {
      className: _style.default["reply-form"],
      content: content,
      recipients: recipients,
      pending: pending,
      onClickOutside: onHideForm,
      onSubmit: onSubmitForm,
      canMention: canMention,
      autoFocus: true
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: _style.default["reply-button-wrapper"],
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactToolbox.Button, {
      className: _style.default["reply-button"],
      theme: _reply_theme.default,
      label: formatMessage(messages["attribute-label-reply"]),
      mini: true,
      onClick: onShowForm
    })
  });
}

ReplyForm.propTypes = {
  collapsed: _propTypes.default.bool,
  content: _propTypes.default.string,
  recipients: _propTypes.default.arrayOf(_propTypes.default.shape({
    resource_id: _propTypes.default.string,
    text_mentioned: _propTypes.default.string
  })),
  pending: _propTypes.default.bool,
  onShowForm: _propTypes.default.func,
  onHideForm: _propTypes.default.func,
  onSubmitForm: _propTypes.default.func,
  intl: _reactIntl.intlShape.isRequired,
  canMention: _propTypes.default.bool
};

var _default = (0, _safe_inject_intl.default)(ReplyForm);

exports.default = _default;