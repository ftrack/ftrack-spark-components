"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _react = require("react");

var _moment = _interopRequireDefault(require("moment"));

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _tooltip = _interopRequireDefault(require("react-toolbox/lib/tooltip"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactIntl = require("react-intl");

var _note_compact = _interopRequireDefault(require("./note_compact.scss"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _skeleton = _interopRequireDefault(require("../skeleton"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var SpanWithTooltip = (0, _tooltip.default)("span");
var messages = (0, _reactIntl.defineMessages)({
  "attribute-unknown": {
    "id": "ftrack-spark-components.compact-note.attribute-unknown",
    "defaultMessage": "Unknown"
  },
  "todo-tooltip-done": {
    "id": "ftrack-spark-components.compact-note.todo-tooltip-done",
    "defaultMessage": "Click to unmark todo as completed."
  },
  "todo-tooltip-not-done": {
    "id": "ftrack-spark-components.compact-note.todo-tooltip-not-done",
    "defaultMessage": "Click to mark todo as completed."
  }
});

function Bottom(_ref) {
  var data = _ref.data,
      date = _ref.date,
      formatMessage = _ref.formatMessage;

  if ((data === null || data === void 0 ? void 0 : data.__entity_type__) === "User" || (data === null || data === void 0 ? void 0 : data.__entity_type__) === "Collaborator") {
    return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _note_compact.default["note-compact-bottom"],
      children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
        children: [data.first_name, " ", data.last_name]
      }), " ", "-", " ", /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedDate, {
          value: date
        })
      })]
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: _note_compact.default["note-compact-bottom"],
    children: formatMessage(messages["attribute-unknown"])
  });
}

Bottom.propTypes = {
  data: _propTypes.default.object,
  date: _propTypes.default.string,
  formatMessage: _propTypes.default.func.isRequired
};

var Todo = /*#__PURE__*/function (_Component) {
  _inherits(Todo, _Component);

  var _super = _createSuper(Todo);

  function Todo() {
    var _this;

    _classCallCheck(this, Todo);

    _this = _super.call(this);
    _this.state = {
      pendingTodo: false
    };
    _this.onClick = _this.onClick.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(Todo, [{
    key: "onClick",
    value: function onClick() {
      this.props.onTodoChange();
      this.setState({
        pendingTodo: true
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          completedAt = _this$props.completedAt,
          formatMessage = _this$props.formatMessage;

      if (this.state.pendingTodo) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _note_compact.default["pending-todo"],
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {
            className: _note_compact.default["icon-skeleton"]
          })
        });
      }

      var completed = !!completedAt;
      return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: "".concat(_note_compact.default.todo),
        onClick: this.onClick,
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(SpanWithTooltip, {
          tooltip: formatMessage(messages[completed === true ? "todo-tooltip-done" : "todo-tooltip-not-done"]),
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
            className: _note_compact.default["todo-icon"],
            value: completed === true ? "check_circle" : "radio_button_unchecked"
          })
        })
      });
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps, prevState) {
      if (nextProps.completedAt && prevState.pendingTodo) {
        return {
          pendingTodo: false
        };
      }

      return null;
    }
  }]);

  return Todo;
}(_react.Component);

Todo.propTypes = {
  completedAt: _propTypes.default.object,
  onTodoChange: _propTypes.default.func.isRequired,
  formatMessage: _propTypes.default.func.isRequired
};
/** Note component to display note data. */

function NoteCompact(_ref2) {
  var data = _ref2.data,
      onTodoChange = _ref2.onTodoChange,
      completedAt = _ref2.completedAt,
      intl = _ref2.intl;
  var localDate = (0, _moment.default)(data.date).format("MM/DD/YYYY");
  var formatMessage = intl.formatMessage;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: _note_compact.default["note-compact-row"],
    children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _note_compact.default["note-compact-row-inner"],
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _note_compact.default["note-compact-text"],
        children: data.content
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(Bottom, {
        data: data.author,
        date: localDate,
        formatMessage: formatMessage
      })]
    }), data.is_todo ? /*#__PURE__*/(0, _jsxRuntime.jsx)(Todo, {
      formatMessage: formatMessage,
      onTodoChange: onTodoChange,
      completedAt: completedAt
    }) : null]
  });
}

NoteCompact.propTypes = {
  data: _propTypes.default.object.isRequired,
  onTodoChange: _propTypes.default.func,
  completedAt: _propTypes.default.object,
  intl: _reactIntl.intlShape.isRequired
};
NoteCompact.defaultProps = {
  completedAt: null
};

var _default = (0, _safe_inject_intl.default)(NoteCompact);

exports.default = _default;