"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.array.slice.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.number.constructor.js");

require("core-js/modules/es6.array.find.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.array.filter.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _moment = _interopRequireDefault(require("moment"));

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _tooltip = _interopRequireDefault(require("react-toolbox/lib/tooltip"));

var _reactIntl = require("react-intl");

var _recompose = require("recompose");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _get_formatted_time = require("../util/get_formatted_time");

var _style = _interopRequireDefault(require("./style.scss"));

var _attachment_area = _interopRequireDefault(require("./attachment_area"));

var _skeleton = _interopRequireDefault(require("../skeleton"));

var _entity_avatar = _interopRequireDefault(require("../entity_avatar"));

var _markdown = _interopRequireDefault(require("../markdown"));

var _label = _interopRequireDefault(require("../label"));

var _utils = require("../editor/utils");

var _constant = require("../util/constant");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SpanWithTooltip = (0, _tooltip.default)("span");
var messages = (0, _reactIntl.defineMessages)({
  page: {
    "id": "ftrack-spark-components.note-note.page",
    "defaultMessage": "Page {number}"
  },
  frame: {
    "id": "ftrack-spark-components.note-note.frame",
    "defaultMessage": "Frame {number}"
  },
  "attribute-message-click-to-complete": {
    "id": "ftrack-spark-components.note-note.attribute-message-to-complete",
    "defaultMessage": "Click to mark as completed."
  },
  "attribute-message-marked-as-completed-by": {
    "id": "ftrack-spark-components.note-note.attribute-message-marked-as-completed-by",
    "defaultMessage": "Marked as completed by"
  },
  "attribute-unknown": {
    "id": "ftrack-spark-components.note-note.attribute-unknown",
    "defaultMessage": "Unknown"
  },
  "attribute-change": {
    "id": "ftrack-spark-components.note-note.attribute-change",
    "defaultMessage": "(Change)"
  },
  "attribute-message-posted-by-unknown-user": {
    "id": "ftrack-spark-components.note-note.attribute-message-posted-by-unknown-user",
    "defaultMessage": "This note is posted by an unknown user. Most likely this is a user or invitee that has been removed from ftrack."
  },
  "review-session-object-status-approved": {
    "id": "ftrack-spark-components.note-note.review-session-object-status-approved",
    "defaultMessage": "Approved"
  },
  "review-session-object-status-require_changes": {
    "id": "ftrack-spark-components.note-note.review-session-object-status-require_changes",
    "defaultMessage": "Require changes"
  }
});
/** Display author information. */

function Author(_ref) {
  var data = _ref.data;

  if ((data === null || data === void 0 ? void 0 : data.__entity_type__) === "User" || (data === null || data === void 0 ? void 0 : data.__entity_type__) === "Collaborator") {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      className: _style.default.authorName,
      children: "".concat(data.first_name, " ").concat(data.last_name)
    });
  }

  var title = /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["attribute-message-posted-by-unknown-user"]));
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
    className: _style.default.authorName,
    title: title,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["attribute-unknown"]))
  });
}

Author.propTypes = {
  data: _propTypes.default.object,
  intl: _reactIntl.intlShape.isRequired
};

function Todo_(_ref2) {
  var completedBy = _ref2.completedBy,
      completedAt = _ref2.completedAt,
      onTodoChange = _ref2.onTodoChange,
      pendingTodo = _ref2.pendingTodo,
      intl = _ref2.intl;

  if (pendingTodo) {
    return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default["pending-todo"],
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {
        className: _style.default["icon-skeleton"]
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_skeleton.default, {
        className: _style.default["todo-text-skeleton"]
      })]
    });
  }

  var formatMessage = intl.formatMessage;
  var content = formatMessage(messages["attribute-message-click-to-complete"]);
  var completed = !!completedAt;

  if (completed) {
    var localDate = (0, _moment.default)(completedAt).local(true);
    content = /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
      children: ["".concat(formatMessage(messages["attribute-message-marked-as-completed-by"]), " "), completedBy ? "".concat(completedBy.first_name, " ").concat(completedBy.last_name) : formatMessage(messages["attribute-unknown"]), " ", /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedRelative, {
        value: localDate.toDate()
      }), ".", " ", /*#__PURE__*/(0, _jsxRuntime.jsx)("button", {
        onClick: onTodoChange,
        className: _style.default["change-todo"],
        children: formatMessage(messages["attribute-change"])
      })]
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: "".concat(_style.default.todo, " ").concat(completed ? _style.default.completed : "") // role="button"
    ,
    onClick: completed ? undefined : onTodoChange,
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
        className: _style.default["todo-icon"],
        value: "check_circle"
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default["todo-content"],
      children: content
    })]
  });
}

Todo_.propTypes = {
  completedAt: _propTypes.default.object,
  completedBy: _propTypes.default.shape({
    first_name: _propTypes.default.string,
    last_name: _propTypes.default.string
  }),
  onTodoChange: _propTypes.default.func.isRequired,
  pendingTodo: _propTypes.default.bool,
  intl: _reactIntl.intlShape.isRequired
};
var Todo = (0, _safe_inject_intl.default)(Todo_);
/**
 * Render note frame annotation in the following order:
 *
 * 1. If type is PDF or Image, return page number
 * 2. If number and frameRate, or time, is defined. Format as standard.
 * 3. If only number is set, return frame number
 */

function FrameTagBase(_ref3) {
  var onClick = _ref3.onClick,
      type = _ref3.type,
      time = _ref3.time,
      number = _ref3.number,
      frameRate = _ref3.frameRate,
      annotationComponent = _ref3.annotationComponent;
  var tooltip;
  var content = [];
  var frameNumber = Number.isFinite(number) ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages.frame), {}, {
    values: {
      number: number + 1
    }
  })) : null;

  if (annotationComponent) {
    content.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
      className: _style.default.annotationIcon,
      value: "brush"
    }));
  }

  if (type === "pdf" || type === "image") {
    content.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages.page), {}, {
      values: {
        number: number + 1
      }
    })));
  } else if (Number.isFinite(number) && Number.isFinite(frameRate)) {
    content.push((0, _get_formatted_time.formatStandard)((0, _get_formatted_time.framesToTimeParts)(number, frameRate)));
    tooltip = frameNumber;
  } else if (Number.isFinite(time)) {
    content.push((0, _get_formatted_time.formatStandard)((0, _get_formatted_time.durationToTimeParts)(time)));
    tooltip = frameNumber;
  } else if (Number.isFinite(number)) {
    content.push(frameNumber);
    tooltip = frameNumber;
  }

  if (tooltip) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(SpanWithTooltip, {
      className: _style.default["frame-info"],
      onClick: onClick,
      tooltip: tooltip,
      children: content
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
    className: _style.default["frame-info"],
    onClick: onClick,
    children: content
  });
}

FrameTagBase.propTypes = {
  type: _propTypes.default.oneOf(["video", "image", "pdf"]),
  time: _propTypes.default.number,
  number: _propTypes.default.number,
  frameRate: _propTypes.default.number,
  onClick: _propTypes.default.func.isRequired,
  annotationComponent: _propTypes.default.object
};
var FrameTag = (0, _recompose.compose)((0, _recompose.withHandlers)({
  onClick: function onClick(props) {
    return function () {
      return props.onClick(props.number, props.annotationComponent);
    };
  }
}), _safe_inject_intl.default)(FrameTagBase);
/** Top part of a note */

function TopOfNote_(_ref4) {
  var data = _ref4.data,
      session = _ref4.session,
      entityInformationTooltip = _ref4.entityInformationTooltip,
      entityInformation = _ref4.entityInformation,
      intl = _ref4.intl,
      onFrameClick = _ref4.onFrameClick;
  var tags = [];
  var localDate = (0, _moment.default)(data.date).local();

  if (data.frame) {
    var annotationComponent = data.note_components.find(function (noteComponent) {
      return noteComponent.component.metadata.some(function (meta) {
        return meta.key === "annotationData" && meta.value && meta.value !== "[]";
      });
    });

    if (tags.length > 0) {
      tags.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        children: ", "
      }));
    }

    tags.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(FrameTag, _objectSpread(_objectSpread({}, data.frame), {}, {
      annotationComponent: annotationComponent,
      onClick: onFrameClick
    })));
  }

  var tagsElement = tags.length ? /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
    className: _style.default.tags,
    children: [tags.map(function (tag, index) {
      return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        children: tag
      }, "tag-".concat(index));
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      children: " - "
    })]
  }) : null;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: _style.default["top-wrapper"],
    children: [data.author ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_avatar.default, {
      className: _style.default["avatar-column"],
      session: session,
      entity: data.author
    }) : null, /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default.top,
      children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: _style.default.author,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Author, {
          data: data.author,
          intl: intl
        }), " ", entityInformationTooltip && entityInformation ? entityInformation : null]
      }), tagsElement, /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        className: _style.default.date,
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedRelative, {
          value: localDate.toDate(),
          className: _style.default.datetime
        })
      })]
    })]
  });
}

TopOfNote_.propTypes = {
  session: _propTypes.default.object,
  entityInformation: _propTypes.default.element,
  entityInformationTooltip: _propTypes.default.bool,
  data: _propTypes.default.object,
  onFrameClick: _propTypes.default.func,
  intl: _reactIntl.intlShape.isRequired
};
var TopOfNote = (0, _safe_inject_intl.default)(TopOfNote_);
/** Note component to display note data. */

var Note = /*#__PURE__*/function (_Component) {
  _inherits(Note, _Component);

  var _super = _createSuper(Note);

  function Note() {
    var _this;

    _classCallCheck(this, Note);

    _this = _super.call(this);
    _this.handleAttachmentClick = _this.handleAttachmentClick.bind(_assertThisInitialized(_this));
    _this.handleFrameClick = _this.handleFrameClick.bind(_assertThisInitialized(_this));
    _this.getAttachmentMeta = _this.getAttachmentMeta.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(Note, [{
    key: "getAttachmentMeta",
    value: function getAttachmentMeta() {
      var _this$props = this.props,
          data = _this$props.data,
          entityInformation = _this$props.entityInformation,
          entityInformationTooltip = _this$props.entityInformationTooltip,
          onFrameClick = _this$props.onFrameClick;
      var attachmentMeta = data.note_components ? /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: _style.default["note-item"],
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(TopOfNote, {
          data: data,
          entityInformationTooltip: entityInformationTooltip,
          entityInformation: entityInformation,
          onFrameClick: onFrameClick
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_markdown.default, {
          source: data.content || ""
        })]
      }) : null;
      return attachmentMeta;
    }
  }, {
    key: "handleAttachmentClick",
    value: function handleAttachmentClick(index, noteComponents) {
      var attachmentMeta = this.getAttachmentMeta();
      this.props.onAttachmentClick(index, noteComponents, attachmentMeta);
    }
  }, {
    key: "handleFrameClick",
    value: function handleFrameClick(frameNumber, annotationComponent) {
      var data = this.props.data;
      var attachmentMeta = this.getAttachmentMeta();
      this.props.onFrameClick(frameNumber, annotationComponent, attachmentMeta, data);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          data = _this$props2.data,
          category = _this$props2.category,
          session = _this$props2.session,
          todo = _this$props2.todo,
          onTodoChange = _this$props2.onTodoChange,
          completedAt = _this$props2.completedAt,
          completedBy = _this$props2.completedBy,
          pendingTodo = _this$props2.pendingTodo,
          entityInformation = _this$props2.entityInformation,
          intl = _this$props2.intl,
          entityInformationTooltip = _this$props2.entityInformationTooltip;
      var labels = [];
      var formatMessage = intl.formatMessage;
      var noteCategories = [];

      if (data.note_label_links) {
        noteCategories.push.apply(noteCategories, _toConsumableArray(data.note_label_links.map(function (_ref5) {
          var label = _ref5.label;
          return label;
        })));
      } else if (data.category) {
        noteCategories.push(data.category);
      }

      if (category && noteCategories.length) {
        labels.push.apply(labels, _toConsumableArray(noteCategories.map(function (item) {
          var color = item.color;

          if (item.id === _constant.REVIEW_SESSION_NOTE_CATEGORY) {
            color = "feedback";
          }

          return /*#__PURE__*/(0, _jsxRuntime.jsx)(_label.default, {
            color: color,
            className: _style.default["note-label"],
            children: item.name
          }, item.id);
        })));
      }

      if (data.inviteeStatus === "require_changes") {
        labels.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_label.default, {
          color: "negative",
          children: formatMessage(messages["review-session-object-status-".concat(data.inviteeStatus)])
        }, "require_changes"));
      } else if (data.inviteeStatus === "approved") {
        labels.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_label.default, {
          color: "positive",
          children: formatMessage(messages["review-session-object-status-".concat(data.inviteeStatus)])
        }, "approved"));
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: _style.default["note-item"],
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(TopOfNote, {
          data: data,
          entityInformationTooltip: entityInformationTooltip,
          entityInformation: entityInformation,
          onFrameClick: this.handleFrameClick
        }), todo ? /*#__PURE__*/(0, _jsxRuntime.jsx)(Todo, {
          onTodoChange: onTodoChange,
          completedAt: completedAt,
          completedBy: completedBy,
          pendingTodo: pendingTodo,
          intl: intl
        }) : null, labels.length ? /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default.tagLabels,
          children: labels
        }) : null, /*#__PURE__*/(0, _jsxRuntime.jsx)(_markdown.default, {
          source: (0, _utils.injectMentions)(data.content, data.recipients) || ""
        }), !entityInformationTooltip && entityInformation ? entityInformation : null, /*#__PURE__*/(0, _jsxRuntime.jsx)(_attachment_area.default, {
          session: session,
          onAttachmentClick: this.handleAttachmentClick,
          noteComponents: data.note_components.filter(function (noteComponent) {
            return !noteComponent.component.metadata.some(function (meta) {
              return meta.key === "annotationData";
            });
          })
        })]
      });
    }
  }]);

  return Note;
}(_react.Component);

Note.propTypes = {
  session: _propTypes.default.object,
  entityInformation: _propTypes.default.element,
  entityInformationTooltip: _propTypes.default.bool,
  data: _propTypes.default.object.isRequired,
  category: _propTypes.default.bool,
  onAttachmentClick: _propTypes.default.func,
  onFrameClick: _propTypes.default.func,
  onTodoChange: _propTypes.default.func,
  todo: _propTypes.default.bool,
  completedBy: _propTypes.default.object,
  completedAt: _propTypes.default.object,
  pendingTodo: _propTypes.default.bool,
  intl: _reactIntl.intlShape.isRequired
};
Note.defaultProps = {
  todo: false,
  completedBy: null,
  completedAt: null,
  category: false
};

var _default = (0, _safe_inject_intl.default)(Note);

exports.default = _default;