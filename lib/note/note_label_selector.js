"use strict";

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.sort.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.array.filter.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactIntl = require("react-intl");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _material = require("@mui/material");

var _LocalOffer = _interopRequireDefault(require("@mui/icons-material/LocalOffer"));

var _tooltip = _interopRequireDefault(require("react-toolbox/lib/tooltip"));

var _label = _interopRequireDefault(require("../label"));

var _button_menu = _interopRequireDefault(require("../button_menu"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var TooltipDiv = (0, _tooltip.default)("div");
var messages = (0, _reactIntl.defineMessages)({
  "note-label-remove-tooltip": {
    "id": "overview.feedback.note-label-remove-tooltip",
    "defaultMessage": "Click to remove"
  }
});

var NoteLabelSelector = function NoteLabelSelector(_ref) {
  var selectedNoteLabels = _ref.selectedNoteLabels,
      attachNoteLabel = _ref.attachNoteLabel,
      removeNoteLabel = _ref.removeNoteLabel,
      session = _ref.session,
      position = _ref.position;

  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      noteLabels = _useState2[0],
      setNoteLabels = _useState2[1];

  (0, _react.useEffect)(function () {
    session.query("select id, color, name, sort from NoteLabel").then(function (response) {
      var labels = response.data;
      labels.sort(function (l1, l2) {
        return l1.sort - l2.sort;
      });
      setNoteLabels(labels);
    });
  }, [session]);
  return [/*#__PURE__*/(0, _jsxRuntime.jsx)(_button_menu.default, {
    button: /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.IconButton, {
      size: "small",
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_LocalOffer.default, {
        sx: {
          fontSize: "18px",
          color: "text.secondary"
        }
      })
    }),
    children: noteLabels && noteLabels.filter(function (nl) {
      return !selectedNoteLabels.some(function (selectedNoteLabel) {
        return selectedNoteLabel.id === nl.id;
      });
    }).map(function (nl) {
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.MenuItem, {
        onClick: function onClick() {
          setTimeout(function () {
            return attachNoteLabel(nl);
          }, 200);
        },
        children: "".concat(nl.name)
      }, nl.id);
    })
  }), selectedNoteLabels.map(function (l) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipDiv, {
      className: _style.default["selected-note-label"],
      tooltipPosition: "top",
      tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["note-label-remove-tooltip"])),
      onClick: function onClick() {
        removeNoteLabel(l);
      },
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_label.default, {
        color: l.color,
        children: l.name
      })
    });
  })];
};

NoteLabelSelector.propTypes = {
  session: _propTypes.default.object.isRequired,
  selectedNoteLabels: _propTypes.default.array,
  attachNoteLabel: _propTypes.default.func.isRequired,
  removeNoteLabel: _propTypes.default.func.isRequired,
  position: _propTypes.default.string
};
NoteLabelSelector.defaultProps = {
  selectedNoteLabels: [],
  position: "bottomLeft"
};

var _default = (0, _safe_inject_intl.default)(NoteLabelSelector);

exports.default = _default;