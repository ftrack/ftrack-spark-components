"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.find.js");

require("core-js/modules/es6.array.map.js");

var _material = require("@mui/material");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _button_menu = _interopRequireDefault(require("../button_menu"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2018 ftrack
function EntitySelector(_ref) {
  var data = _ref.data,
      selected = _ref.selected,
      onSelect = _ref.onSelect;

  var handleSelect = function handleSelect(value) {
    var item = data.find(function (option) {
      return value === option.entity.id;
    });
    onSelect(item.entity);
  };

  var active = data.find(function (option) {
    return selected.id === option.entity.id;
  });
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_button_menu.default, {
    button: /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.Button, {
      startIcon: active.icon,
      sx: {
        textTransform: "none",
        maxWidth: "20rem",
        overflow: "hidden",
        textOverflow: "ellipsis",
        color: "text.primary",
        ".MuiButton-startIcon": {
          color: "primary.main"
        }
      },
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.Typography, {
        variant: "inherit",
        noWrap: true,
        fontWeight: "fontWeightMedium",
        children: active.label
      })
    }),
    sx: {
      maxWidth: "40rem"
    },
    children: data.map(function (item) {
      return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_material.MenuItem, {
        onClick: function onClick() {
          return handleSelect(item.entity.id);
        },
        sx: {
          ".MuiListItemIcon-root": {
            color: "primary.main"
          }
        },
        children: [item.icon && /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.ListItemIcon, {
          children: item.icon
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.ListItemText, {
          primary: /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.Typography, {
            variant: "inherit",
            noWrap: true,
            children: item.label
          }),
          secondary: /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.Typography, {
            variant: "inherit",
            noWrap: true,
            children: item.description
          })
        })]
      }, "menu-item-".concat(item.entity.id));
    })
  });
}

EntitySelector.propTypes = {
  data: _propTypes.default.array.isRequired,
  selected: _propTypes.default.object.isRequired,
  onSelect: _propTypes.default.func.isRequired
};
var _default = EntitySelector;
exports.default = _default;