"use strict";

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es7.array.includes.js");

require("core-js/modules/es6.array.filter.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactToolbox = require("react-toolbox");

var _reactOnclickoutside = _interopRequireDefault(require("react-onclickoutside"));

var _reactIntl = require("react-intl");

var _classnames = _interopRequireDefault(require("classnames"));

var _debounce = _interopRequireDefault(require("lodash/debounce"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _editor = _interopRequireDefault(require("../editor"));

var _icon_button = _interopRequireDefault(require("../icon_button"));

var _entity_selector = _interopRequireDefault(require("./entity_selector"));

var _note_label_selector = _interopRequireDefault(require("./note_label_selector"));

var _style = _interopRequireDefault(require("./style.scss"));

var _active_button_theme = _interopRequireDefault(require("./active_button_theme.scss"));

var _inactive_button_theme = _interopRequireDefault(require("./inactive_button_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var messages = (0, _reactIntl.defineMessages)({
  "attribute-label-update-note": {
    "id": "ftrack-spark-components.note-form.label-update-note",
    "defaultMessage": "Update your feedback..."
  },
  "attribute-label-write-comment": {
    "id": "ftrack-spark-components.note-form.label-write-comment",
    "defaultMessage": "Write something..."
  },
  "attribute-label-update": {
    "id": "ftrack-spark-components.note-form.label-update",
    "defaultMessage": "Update"
  },
  "attribute-label-comment": {
    "id": "ftrack-spark-components.note-form.label-comment",
    "defaultMessage": "Post"
  },
  "completable-note-tooltip": {
    "id": "ftrack-spark-components.note-form.completable-note-tooltip",
    "defaultMessage": "Select if this note can be marked as completed"
  }
});
var FORM_INPUT_DELAY = 250;
/** Note form use to create or edit a note. */

function NoteForm(_ref) {
  var session = _ref.session,
      content = _ref.content,
      recipients = _ref.recipients,
      isTodo = _ref.isTodo,
      labels = _ref.labels,
      pending = _ref.pending,
      collapsed = _ref.collapsed,
      edit = _ref.edit,
      autoFocus = _ref.autoFocus,
      className = _ref.className,
      onSubmit = _ref.onSubmit,
      onExpand = _ref.onExpand,
      onClickOutside = _ref.onClickOutside,
      entitySelector = _ref.entitySelector,
      entity = _ref.entity,
      intl = _ref.intl,
      canMention = _ref.canMention,
      showCompletableNotes = _ref.showCompletableNotes,
      showNoteLabelSelector = _ref.showNoteLabelSelector;
  var formatMessage = intl.formatMessage;

  var _useState = (0, _react.useState)(0),
      _useState2 = _slicedToArray(_useState, 2),
      editorKey = _useState2[0],
      setEditorKey = _useState2[1];

  var _useState3 = (0, _react.useState)(content),
      _useState4 = _slicedToArray(_useState3, 2),
      editorContent = _useState4[0],
      setEditorContent = _useState4[1];

  var _useState5 = (0, _react.useState)(isTodo),
      _useState6 = _slicedToArray(_useState5, 2),
      editorIsCompletable = _useState6[0],
      setEditorIsCompletable = _useState6[1];

  var _useState7 = (0, _react.useState)(labels || []),
      _useState8 = _slicedToArray(_useState7, 2),
      editorSelectedLabels = _useState8[0],
      setEditorSelectedLabels = _useState8[1];

  var _useState9 = (0, _react.useState)([]),
      _useState10 = _slicedToArray(_useState9, 2),
      selectedRecipients = _useState10[0],
      setSelectedRecipients = _useState10[1];

  var _useState11 = (0, _react.useState)(entity),
      _useState12 = _slicedToArray(_useState11, 2),
      selectedEntity = _useState12[0],
      setSelectedEntity = _useState12[1];

  var classes = (0, _classnames.default)(_style.default["note-form"], className);

  var handleSubmit = function handleSubmit() {
    var labelIds = editorSelectedLabels.map(function (label) {
      return label.id;
    });
    onSubmit({
      content: editorContent,
      entity: selectedEntity,
      recipients: selectedRecipients,
      is_todo: editorIsCompletable,
      labelIds: labelIds
    });
    setEditorKey(editorKey + 1);
  };

  var onContentChangeDebounced = (0, _debounce.default)(function (_ref2) {
    var markdown = _ref2.markdown,
        newRecipients = _ref2.recipients;
    setEditorContent(markdown);
    setSelectedRecipients(newRecipients);
  }, FORM_INPUT_DELAY);

  var handleUserKeyPress = function handleUserKeyPress(e) {
    if ((e.ctrlKey || e.metaKey) && e.key === "Enter" && collapsed !== true) {
      handleSubmit();
    }
  };

  (0, _react.useEffect)(function () {
    window.addEventListener("keydown", handleUserKeyPress);
    return function () {
      window.removeEventListener("keydown", handleUserKeyPress);
    };
  }, // TODO: Feel free to fix this eslint error if you got time
  // eslint-disable-next-line react-hooks/exhaustive-deps
  [editorContent, editorKey]);

  NoteForm.handleClickOutside = function (e) {
    var formatToolSelected = ["format_bold", "format_italic", "format_strikethrough", "insert_link", "format_list_bulleted"].includes(e.target.innerText);
    var ancestor = e.target.closest("[data-mention]");
    var mentionSelected = ancestor && ancestor.hasAttribute("data-mention");

    if (onClickOutside && !formatToolSelected && !mentionSelected) {
      onClickOutside({
        content: editorContent,
        collapsed: collapsed
      });
    }
  };

  var tools = [];

  if (pending) {
    tools.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {}, "filler"), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default.progressbar,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactToolbox.ProgressBar, {
        type: "circular",
        mode: "indeterminate"
      })
    }, "progress-wrapper"));
  } else {
    if (entitySelector) {
      tools.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_selector.default, {
        onSelect: setSelectedEntity,
        data: entitySelector,
        selected: selectedEntity
      }, "entity-selector"));
    } else {
      tools.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {}, "filler"));
    }

    tools.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactToolbox.Button, {
      className: _style.default.submitButton,
      onClick: handleSubmit,
      disabled: !editorContent,
      label: edit ? formatMessage(messages["attribute-label-update"]) : formatMessage(messages["attribute-label-comment"])
    }, "submit-button"));
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: classes,
    children: [showCompletableNotes && edit && /*#__PURE__*/(0, _jsxRuntime.jsx)(_icon_button.default, {
      icon: "check_circle",
      onClick: function onClick() {
        return setEditorIsCompletable(!editorIsCompletable);
      },
      tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["completable-note-tooltip"])),
      theme: !editorIsCompletable ? _inactive_button_theme.default : _active_button_theme.default
    }), showNoteLabelSelector && edit && /*#__PURE__*/(0, _jsxRuntime.jsx)(_note_label_selector.default, {
      session: session,
      selectedNoteLabels: editorSelectedLabels,
      attachNoteLabel: function attachNoteLabel(noteLabel) {
        var noteLabels = _toConsumableArray(editorSelectedLabels);

        if (!noteLabels.some(function (candidate) {
          return candidate.id === noteLabel.id;
        })) {
          noteLabels.push(noteLabel);
        }

        setEditorSelectedLabels(noteLabels);
      },
      removeNoteLabel: function removeNoteLabel(noteLabel) {
        var noteLabels = editorSelectedLabels.filter(function (selectedNoteLabel) {
          return noteLabel.id !== selectedNoteLabel.id;
        });
        setEditorSelectedLabels(noteLabels);
      },
      position: "topLeft"
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_editor.default, {
      placeholder: edit ? formatMessage(messages["attribute-label-update-note"]) : formatMessage(messages["attribute-label-write-comment"]),
      submitTitle: edit ? formatMessage(messages["attribute-label-update"]) : formatMessage(messages["attribute-label-comment"]),
      content: content,
      recipients: recipients,
      disabled: pending,
      autoFocus: autoFocus,
      canMention: canMention,
      onChange: onContentChangeDebounced,
      onFocus: function onFocus() {
        if (collapsed) onExpand();
      },
      legacyStyle: true
    }, editorKey), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _style.default.toolbar,
      children: tools
    })]
  });
}

NoteForm.propTypes = {
  session: _propTypes.default.object,
  isTodo: _propTypes.default.object,
  labels: _propTypes.default.array,
  content: _propTypes.default.string,
  recipients: _propTypes.default.arrayOf(_propTypes.default.shape({
    resource_id: _propTypes.default.string,
    text_mentioned: _propTypes.default.string
  })),
  className: _propTypes.default.string,
  onSubmit: _propTypes.default.func,
  onExpand: _propTypes.default.func,
  onClickOutside: _propTypes.default.func,
  edit: _propTypes.default.bool,
  autoFocus: _propTypes.default.bool,
  collapsed: _propTypes.default.bool,
  pending: _propTypes.default.bool,
  entity: _propTypes.default.object,
  entitySelector: _propTypes.default.array,
  intl: _reactIntl.intlShape.isRequired,
  canMention: _propTypes.default.bool,
  showNoteLabelSelector: _propTypes.default.bool,
  showCompletableNotes: _propTypes.default.bool
};
NoteForm.defaultProps = {
  onSubmit: function onSubmit() {},
  onExpand: function onExpand() {},
  showNoteLabelSelector: false,
  showCompletableNotes: false
};
var clickOutsideConfig = {
  handleClickOutside: function handleClickOutside() {
    return NoteForm.handleClickOutside;
  }
};

var _default = (0, _safe_inject_intl.default)((0, _reactOnclickoutside.default)(NoteForm, clickOutsideConfig));

exports.default = _default;