"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _classnames = _interopRequireDefault(require("classnames"));

var _dialog = _interopRequireDefault(require("react-toolbox/lib/dialog"));

var _reactIntl = require("react-intl");

var _material = require("@mui/material");

var _Delete = _interopRequireDefault(require("@mui/icons-material/Delete"));

var _MoreVert = _interopRequireDefault(require("@mui/icons-material/MoreVert"));

var _Edit = _interopRequireDefault(require("@mui/icons-material/Edit"));

var _button_menu = _interopRequireDefault(require("../button_menu"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _note = _interopRequireDefault(require("./note"));

var _form = _interopRequireDefault(require("./form"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["note", "collapsed", "pending", "content", "recipients", "author", "onShowForm", "onHideForm", "onSubmitForm", "session", "intl", "colors", "className", "canMention", "showCompletableNotes", "showNoteLabelSelector"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var messages = (0, _reactIntl.defineMessages)({
  "attribute-label-cancel": {
    "id": "ftrack-spark-components.note-editable.label-cancel",
    "defaultMessage": "Cancel"
  },
  "attribute-label-delete": {
    "id": "ftrack-spark-components.note-editable.label-delete",
    "defaultMessage": "Delete"
  },
  "attribute-label-edit": {
    "id": "ftrack-spark-components.note-editable.label-edit",
    "defaultMessage": "Edit"
  },
  "attribute-message-confirm-delete": {
    "id": "ftrack-spark-components.note-editable.message-confirm-delete",
    "defaultMessage": "Are you sure you want to delete the comment?"
  }
});
/** Editable note component that displays either a note or a note form.
 *
 * Options to edit or delete are displayed if the *note* author is matching the
 * *author*.
 *
 * Display the note form if *collpased* is false. The *pending* and *content*
 * props are passed ot the note form.
 *
 */

var EditableNote = /*#__PURE__*/function (_Component) {
  _inherits(EditableNote, _Component);

  var _super = _createSuper(EditableNote);

  function EditableNote() {
    var _this;

    _classCallCheck(this, EditableNote);

    _this = _super.call(this);
    _this.state = {
      confirmDelete: false
    };
    return _this;
  }
  /** Handle dialog interaction and remove note if *remove* is true. */


  _createClass(EditableNote, [{
    key: "handleDialog",
    value: function handleDialog(remove) {
      if (remove === true) {
        this.props.onRemove();
      }

      this.setState({
        confirmDelete: false
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          note = _this$props.note,
          collapsed = _this$props.collapsed,
          pending = _this$props.pending,
          content = _this$props.content,
          recipients = _this$props.recipients,
          author = _this$props.author,
          onShowForm = _this$props.onShowForm,
          onHideForm = _this$props.onHideForm,
          onSubmitForm = _this$props.onSubmitForm,
          session = _this$props.session,
          intl = _this$props.intl,
          colors = _this$props.colors,
          className = _this$props.className,
          canMention = _this$props.canMention,
          showCompletableNotes = _this$props.showCompletableNotes,
          showNoteLabelSelector = _this$props.showNoteLabelSelector,
          noteProps = _objectWithoutProperties(_this$props, _excluded);

      var classes = (0, _classnames.default)(_style.default["editable-note-container"], className);
      var formatMessage = intl.formatMessage;
      /* eslint-disable react/jsx-no-bind */

      var actions = [{
        label: formatMessage(messages["attribute-label-cancel"]),
        onClick: this.handleDialog.bind(this, false)
      }, {
        label: formatMessage(messages["attribute-label-delete"]),
        onClick: this.handleDialog.bind(this, true),
        className: _style.default["destructive-action"]
      }];
      var isTodo = note.is_todo;
      var noteLabels = (note.note_label_links || []).map(function (labelLink) {
        return labelLink.label;
      });
      /* eslint-enable react/jsx-no-bind */

      if (!collapsed) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_form.default, {
          session: session,
          pending: pending,
          content: content,
          recipients: recipients,
          isTodo: isTodo,
          labels: noteLabels,
          onClickOutside: onHideForm,
          onSubmit: onSubmitForm,
          canMention: canMention,
          showCompletableNotes: !note.completed_at && showCompletableNotes,
          showNoteLabelSelector: showNoteLabelSelector,
          autoFocus: true,
          edit: true
        }, "edit-note-form-".concat(note.id));
      }

      var menu = false;

      if (author && note.author && author.id === note.author.id) {
        menu = /*#__PURE__*/(0, _jsxRuntime.jsxs)(_button_menu.default, {
          button: /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.IconButton, {
            size: "small",
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_MoreVert.default, {})
          }),
          children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(_material.MenuItem, {
            onClick: onShowForm,
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_material.ListItemIcon, {
              children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Edit.default, {})
            }), formatMessage(messages["attribute-label-edit"])]
          }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_material.MenuItem, {
            onClick: function onClick() {
              return _this2.setState({
                confirmDelete: true
              });
            },
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_material.ListItemIcon, {
              children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Delete.default, {})
            }), formatMessage(messages["attribute-label-delete"])]
          })]
        });
      }

      var confirmationDialog = false;

      if (this.state.confirmDelete) {
        confirmationDialog = /*#__PURE__*/(0, _jsxRuntime.jsx)(_dialog.default, {
          type: "small",
          title: formatMessage(messages["attribute-message-confirm-delete"]),
          actions: actions,
          onEscKeyDown: function onEscKeyDown() {
            return _this2.handleDialog(false);
          },
          active: true
        });
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: classes,
        children: [confirmationDialog, /*#__PURE__*/(0, _jsxRuntime.jsx)(_note.default, _objectSpread({
          session: session,
          data: note,
          category: true,
          todo: note.is_todo,
          completedBy: note.completed_by,
          completedAt: note.completed_at,
          colors: colors
        }, noteProps), note.id), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default["icon-menu"],
          children: menu
        })]
      });
    }
  }]);

  return EditableNote;
}(_react.Component);

EditableNote.propTypes = {
  className: _propTypes.default.string,
  note: _propTypes.default.object,
  entityInformation: _propTypes.default.element,
  session: _propTypes.default.object,
  collapsed: _propTypes.default.bool,
  content: _propTypes.default.string,
  recipients: _propTypes.default.arrayOf(_propTypes.default.shape({
    resource_id: _propTypes.default.string,
    text_mentioned: _propTypes.default.string
  })),
  pending: _propTypes.default.bool,
  onShowForm: _propTypes.default.func,
  onHideForm: _propTypes.default.func,
  onSubmitForm: _propTypes.default.func,
  onRemove: _propTypes.default.func,
  author: _propTypes.default.object,
  colors: _propTypes.default.arrayOf(_propTypes.default.string),
  intl: _reactIntl.intlShape.isRequired,
  canMention: _propTypes.default.bool,
  showNoteLabelSelector: _propTypes.default.bool,
  showCompletableNotes: _propTypes.default.bool
};

var _default = (0, _safe_inject_intl.default)(EditableNote);

exports.default = _default;