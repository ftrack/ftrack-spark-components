"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.find-index.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _is_media = _interopRequireDefault(require("../util/is_media"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

/** Attachment area component to display an array of *components*.
 *
 * *onAttachmentClick* prop is called when an attachment media or document is
 * clicked.
 *
 */
var AttachmentArea = /*#__PURE__*/function (_Component) {
  _inherits(AttachmentArea, _Component);

  var _super = _createSuper(AttachmentArea);

  function AttachmentArea() {
    _classCallCheck(this, AttachmentArea);

    return _super.apply(this, arguments);
  }

  _createClass(AttachmentArea, [{
    key: "onAttachmentClick",
    value:
    /** Handle click on attachment *component*. */
    function onAttachmentClick(component) {
      var noteComponents = this.props.noteComponents;

      if (this.props.onAttachmentClick) {
        var clickedPreviewIndex = noteComponents.findIndex(function (componentToCheck) {
          return componentToCheck.component.id === component.id;
        });
        this.props.onAttachmentClick(clickedPreviewIndex, noteComponents.map(function (noteComponent) {
          return _objectSpread(_objectSpread({}, noteComponent.component), {}, {
            url: noteComponent.url && noteComponent.url.value,
            thumbnail_url: noteComponent.thumbnail_url && noteComponent.thumbnail_url.value
          });
        }));
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this = this;

      var noteComponents = this.props.noteComponents;
      var images = [];
      var other = [];
      noteComponents.forEach(function (noteComponent) {
        var component = noteComponent.component;
        var fileType = component.file_type.slice(1);

        if ((0, _is_media.default)(fileType) && noteComponent.thumbnail_url) {
          var thumbnailUrl = noteComponent.thumbnail_url.value;
          images.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            onClick: function onClick() {
              return _this.onAttachmentClick(component);
            },
            className: _style.default.image,
            style: {
              backgroundImage: "url('".concat(thumbnailUrl, "')")
            }
          }, component.id));
        } else {
          other.push( /*#__PURE__*/(0, _jsxRuntime.jsxs)("p", {
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
              className: _style.default["attachment-icon"],
              value: "attachment"
            }), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
              className: _style.default["file-name"],
              onClick: function onClick() {
                return _this.onAttachmentClick(component);
              },
              children: "".concat(component.name).concat(component.file_type)
            })]
          }, component.id));
        }
      });

      if (images.length || other.length) {
        return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
          className: _style.default["attachments-area"],
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            className: _style.default.images,
            children: images
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            className: _style.default.other,
            children: other
          })]
        });
      }

      return null;
    }
  }]);

  return AttachmentArea;
}(_react.Component);

AttachmentArea.propTypes = {
  session: _propTypes.default.object.isRequired,
  noteComponents: _propTypes.default.array.isRequired,
  onAttachmentClick: _propTypes.default.func
};
var _default = AttachmentArea;
exports.default = _default;