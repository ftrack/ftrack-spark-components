"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = require("react");

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2017 ftrack
function withEmptyState(component, emptyState, isEmptyCondition) {
  return function render(props) {
    // Always render *component* as it may have dependencies on being mounted
    // to load data correctly.
    return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default.container,
      children: [/*#__PURE__*/(0, _react.createElement)(component, props), isEmptyCondition(props) ? /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _style.default["empty-state-wrapper"],
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          children: /*#__PURE__*/(0, _react.createElement)(emptyState, {
            originalProps: props
          })
        })
      }) : null]
    });
  };
}

var _default = withEmptyState;
exports.default = _default;