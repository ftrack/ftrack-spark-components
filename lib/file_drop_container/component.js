"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactDropzone = _interopRequireDefault(require("react-dropzone"));

var _loglevel = _interopRequireDefault(require("loglevel"));

var _uuid = require("uuid");

var _classnames = _interopRequireDefault(require("classnames"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var FileDropContainer = /*#__PURE__*/function (_Component) {
  _inherits(FileDropContainer, _Component);

  var _super = _createSuper(FileDropContainer);

  function FileDropContainer(props) {
    var _this;

    _classCallCheck(this, FileDropContainer);

    _this = _super.call(this, props);
    _this.handleDropAccepted = _this.handleDropAccepted.bind(_assertThisInitialized(_this));
    _this.handleDropRejected = _this.handleDropRejected.bind(_assertThisInitialized(_this));
    _this.droppableAreaRef = null;
    return _this;
  }

  _createClass(FileDropContainer, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var _this$props = this.props,
          showFileBrowser = _this$props.showFileBrowser,
          onShowFileBrowser = _this$props.onShowFileBrowser;

      if (showFileBrowser !== prevProps.showFileBrowser && showFileBrowser && this.droppableAreaRef) {
        this.droppableAreaRef.open();

        if (onShowFileBrowser) {
          onShowFileBrowser();
        }
      }
    }
  }, {
    key: "handleDropAccepted",
    value: function handleDropAccepted(files) {
      _loglevel.default.debug("Dropped files", files);

      if (files.length) {
        this.uploadFile(files);
      }
    }
  }, {
    key: "handleDropRejected",
    value: function handleDropRejected(rejectedFiles) {
      var _this$props2 = this.props,
          maxSize = _this$props2.maxSize,
          onFileSizeTooBig = _this$props2.onFileSizeTooBig;

      _loglevel.default.debug("Files were rejected", rejectedFiles);

      if (rejectedFiles.some(function (file) {
        return file.size > maxSize;
      })) {
        onFileSizeTooBig();
      }
    }
  }, {
    key: "uploadFile",
    value: function uploadFile(files) {
      var _this2 = this;

      var _this$props3 = this.props,
          onUploadStarted = _this$props3.onUploadStarted,
          onAborted = _this$props3.onAborted,
          onChange = _this$props3.onChange,
          onUploadError = _this$props3.onUploadError;
      var uploadItems = files.map(function (file) {
        return {
          id: (0, _uuid.v4)(),
          xhr: new XMLHttpRequest(),
          file: file
        };
      });

      if (onUploadStarted) {
        var shouldContinue = onUploadStarted(uploadItems);

        if (shouldContinue === false) {
          return;
        }
      }

      var promises = files.map(function (file, index) {
        var xhr = uploadItems[index].xhr;
        return _this2.props.session.createComponent(file, {
          onAborted: onAborted,
          xhr: xhr,
          onProgress: function onProgress(progress) {
            return _this2.props.onProgress && _this2.props.onProgress(progress, uploadItems[index].id);
          },
          data: {
            id: uploadItems[index].id
          }
        });
      });
      promises.forEach(function (p) {
        p.then(function (res) {
          onChange(res[0].data.id);
        }).catch(function (err) {
          onUploadError(err);
        });
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var _this$props4 = this.props,
          disabled = _this$props4.disabled,
          multiple = _this$props4.multiple,
          maxSize = _this$props4.maxSize,
          accept = _this$props4.accept,
          dropzoneProps = _this$props4.dropzoneProps,
          rejectClassName = _this$props4.rejectClassName,
          activeClassName = _this$props4.activeClassName,
          className = _this$props4.className;
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactDropzone.default, _objectSpread(_objectSpread({
        multiple: multiple,
        maxSize: maxSize,
        accept: accept,
        onDropAccepted: this.handleDropAccepted
      }, dropzoneProps), {}, {
        disabled: disabled,
        noKeyboard: true,
        noClick: true,
        ref: function ref(node) {
          _this3.droppableAreaRef = node;
        },
        children: function children(_ref) {
          var _classNames;

          var getRootProps = _ref.getRootProps,
              getInputProps = _ref.getInputProps,
              isDragAccept = _ref.isDragAccept,
              isDragReject = _ref.isDragReject;
          var classes = (0, _classnames.default)(_style.default.container, (_classNames = {}, _defineProperty(_classNames, activeClassName, isDragAccept), _defineProperty(_classNames, rejectClassName, isDragReject), _classNames), className);
          return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", _objectSpread(_objectSpread({
            className: classes
          }, getRootProps()), {}, {
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("input", _objectSpread({}, getInputProps())), _this3.props.children]
          }));
        }
      }));
    }
  }]);

  return FileDropContainer;
}(_react.Component);

FileDropContainer.propTypes = {
  accept: _propTypes.default.string,
  dropzoneProps: _propTypes.default.object,
  // eslint-disable-line
  onChange: _propTypes.default.func,
  onAborted: _propTypes.default.func,
  onProgress: _propTypes.default.func,
  onUploadError: _propTypes.default.func,
  session: _propTypes.default.shape({
    createComponent: _propTypes.default.func.isRequired
  }).isRequired,
  multiple: _propTypes.default.bool,
  maxSize: _propTypes.default.number,
  disabled: _propTypes.default.bool,
  showFileBrowser: _propTypes.default.bool,
  onShowFileBrowser: _propTypes.default.func,
  onUploadStarted: _propTypes.default.func,
  onFileSizeTooBig: _propTypes.default.func,
  children: _propTypes.default.node,
  rejectClassName: _propTypes.default.string,
  activeClassName: _propTypes.default.string,
  className: _propTypes.default.string
};
FileDropContainer.defaultProps = {
  value: null,
  onAborted: function onAborted() {},
  onFileSizeTooBig: function onFileSizeTooBig() {},
  dropzoneProps: {},
  multiple: false,
  maxSize: 1e9,
  // 1000 MB
  accept: null,
  disabled: false,
  showFileBrowser: false
};
var _default = FileDropContainer;
exports.default = _default;