"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.weak-map.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.string.includes.js");

require("core-js/modules/es7.array.includes.js");

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireWildcard(require("prop-types"));

var _button = require("react-toolbox/lib/button");

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _progress_bar = _interopRequireDefault(require("react-toolbox/lib/progress_bar"));

var _classnames = _interopRequireDefault(require("classnames"));

var _material = require("@mui/material");

var _reactIntl = require("react-intl");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _debounce = _interopRequireDefault(require("lodash/debounce"));

var _styles = _interopRequireDefault(require("./styles.scss"));

var _hooks = require("../util/hooks");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var messages = (0, _reactIntl.defineMessages)({
  placeholder: {
    "id": "ftrack-spark-components.search-field.placeholder",
    "defaultMessage": "Search"
  },
  matches: {
    "id": "ftrack-spark-components.search-field.matches",
    "defaultMessage": "{count, plural, one {match} other {matches}}"
  }
});

var SearchField = function SearchField(_ref) {
  var _classNames, _classNames4;

  var _ref$onChange = _ref.onChange,
      onChange = _ref$onChange === void 0 ? function () {} : _ref$onChange,
      placeholder = _ref.placeholder,
      _ref$matches = _ref.matches,
      matches = _ref$matches === void 0 ? null : _ref$matches,
      onKeyPress = _ref.onKeyPress,
      _ref$onActiveChange = _ref.onActiveChange,
      onActiveChange = _ref$onActiveChange === void 0 ? function () {} : _ref$onActiveChange,
      intl = _ref.intl,
      _ref$keys = _ref.keys,
      keys = _ref$keys === void 0 ? [] : _ref$keys,
      _ref$disableClose = _ref.disableClose,
      disableClose = _ref$disableClose === void 0 ? false : _ref$disableClose,
      _ref$resetIcon = _ref.resetIcon,
      resetIcon = _ref$resetIcon === void 0 ? "close" : _ref$resetIcon,
      debounceTime = _ref.debounce,
      large = _ref.large,
      showInput = _ref.showInput,
      isLoading = _ref.isLoading,
      additionalMatchesAvailable = _ref.additionalMatchesAvailable;

  var _useState = (0, _react.useState)(disableClose),
      _useState2 = _slicedToArray(_useState, 2),
      searchActive = _useState2[0],
      setSearchActive = _useState2[1];

  var _useState3 = (0, _react.useState)(""),
      _useState4 = _slicedToArray(_useState3, 2),
      value = _useState4[0],
      setValue = _useState4[1];

  var inputRef = _react.default.useRef(null);

  var debounceChange = _react.default.useMemo(function () {
    return (0, _debounce.default)(onChange, debounceTime);
  }, [onChange, debounceTime]);

  (0, _react.useEffect)(function () {
    if (inputRef.current && searchActive) {
      inputRef.current.focus();
    }
  }, // TODO: Feel free to fix this eslint error if you got time
  // eslint-disable-next-line react-hooks/exhaustive-deps
  [inputRef.current, searchActive]);

  var handleChange = function handleChange(newValue) {
    var previousValue = value;
    setValue(newValue);

    if (!debounceTime) {
      onChange(newValue);
      return;
    } // do not debounce if leading whitespace


    if (newValue.trim().length === 0) {
      // if actually resetting the input
      if (previousValue.trim().length !== 0) {
        debounceChange.cancel();
        onChange(newValue);
      }
    } else {
      debounceChange(newValue);
    }
  };

  var handleResetSearch = function handleResetSearch() {
    if (!disableClose) {
      setSearchActive(false);
      onActiveChange(false);
    }

    setValue("");
    onChange("");

    if (inputRef.current) {
      inputRef.current.blur();
    }
  };

  var handleShowSearch = function handleShowSearch() {
    setSearchActive(true);
    onActiveChange(true);
  };

  var handleClickAway = function handleClickAway() {
    if (value === "" && !disableClose) {
      handleResetSearch();
    }
  };

  var handleKeyPress = function handleKeyPress(key) {
    if (key === "Escape") {
      if (!disableClose) {
        handleResetSearch();
      }
    } else if (onKeyPress && keys.includes(key)) {
      onKeyPress(key);
    }
  };

  var listenerKeys = _react.default.useMemo(function () {
    return [].concat(_toConsumableArray(keys), ["Escape"]);
  }, [keys]);

  (0, _hooks.useKeyPress)(inputRef, listenerKeys, handleKeyPress);
  var containerClasses = (0, _classnames.default)(_styles.default.container, (_classNames = {}, _defineProperty(_classNames, _styles.default.containerActive, searchActive), _defineProperty(_classNames, _styles.default.large, large), _defineProperty(_classNames, _styles.default.showInput, showInput), _defineProperty(_classNames, _styles.default.alwaysOpen, disableClose), _classNames));
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.ClickAwayListener, {
    onClickAway: handleClickAway,
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: containerClasses,
      children: [(searchActive || showInput) && /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: _styles.default.inputContainer,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("input", {
          ref: inputRef,
          className: (0, _classnames.default)(_styles.default.input, _defineProperty({}, _styles.default.large, large)),
          type: "text",
          value: value,
          onFocus: function onFocus() {
            return setSearchActive(true);
          },
          onChange: function onChange(e) {
            return handleChange(e.target.value);
          },
          placeholder: placeholder || intl.formatMessage(messages.placeholder),
          onKeyDown: function onKeyDown(e) {
            if (e.key === "ArrowUp" || e.key === "ArrowDown") {
              e.preventDefault();
            }
          }
        }), searchActive && /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
          className: (0, _classnames.default)(_styles.default.rightContainer, _defineProperty({}, _styles.default.large, large)),
          children: [value !== "" && !isLoading && matches !== null && /*#__PURE__*/(0, _jsxRuntime.jsx)(_react.default.Fragment, {
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
              children: "".concat(matches).concat(additionalMatchesAvailable ? "+" : "", " ").concat(intl.formatMessage(messages.matches, {
                count: matches
              }))
            })
          }), isLoading && Boolean(value) && /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_progress_bar.default, {
              type: "circular",
              mode: "indeterminate",
              theme: {
                circular: _styles.default.spinner
              }
            })
          }), (disableClose && Boolean(value) || !disableClose) && /*#__PURE__*/(0, _jsxRuntime.jsx)(_button.IconButton, {
            onClick: handleResetSearch,
            icon: resetIcon,
            theme: {
              toggle: large ? undefined : _styles.default.button,
              icon: _styles.default.icon
            }
          })]
        })]
      }), !searchActive && !disableClose && /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: _styles.default.flex,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _styles.default.flex
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_button.IconButton, {
          onClick: handleShowSearch,
          icon: "search",
          theme: {
            toggle: large ? undefined : _styles.default.button,
            icon: _styles.default.icon
          }
        })]
      }), disableClose && value === "" && /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
        value: "search",
        className: (0, _classnames.default)(_styles.default.icon, _styles.default.iconMargin, (_classNames4 = {}, _defineProperty(_classNames4, _styles.default.iconSmall, !large), _defineProperty(_classNames4, _styles.default.iconLarge, large), _classNames4))
      })]
    })
  });
};

SearchField.propTypes = {
  onChange: _propTypes.default.func.isRequired,
  onKeyPress: _propTypes.default.func,
  onActiveChange: _propTypes.default.func,
  keys: _propTypes.default.arrayOf(_propTypes.string),
  placeholder: _propTypes.default.string,
  intl: _reactIntl.intlShape,
  matches: _propTypes.default.number,
  additionalMatchesAvailable: _propTypes.default.bool,
  disableClose: _propTypes.default.bool,
  debounce: _propTypes.default.number,
  large: _propTypes.default.bool,
  isLoading: _propTypes.default.bool,
  showInput: _propTypes.default.bool,
  resetIcon: _propTypes.default.string
};

var _default = (0, _safe_inject_intl.default)(SearchField);

exports.default = _default;