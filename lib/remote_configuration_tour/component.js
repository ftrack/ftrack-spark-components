"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _loglevel = _interopRequireDefault(require("loglevel"));

var _reactErrorBoundary = _interopRequireDefault(require("react-error-boundary"));

var _tour = _interopRequireDefault(require("../tour"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["baseUrl", "steps"],
    _excluded2 = ["url"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ConfigTour(_ref) {
  var baseUrl = _ref.baseUrl,
      steps = _ref.steps,
      props = _objectWithoutProperties(_ref, _excluded);

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_tour.default, _objectSpread({
    steps: steps.map(function (step) {
      return _objectSpread(_objectSpread({}, step), {}, {
        image: step.image ? "".concat(baseUrl).concat(step.image) : null,
        actions: step.actions
      });
    })
  }, props));
}

ConfigTour.propTypes = {
  steps: _propTypes.default.arrayOf(_propTypes.default.shape({
    title: _propTypes.default.node,
    text: _propTypes.default.node,
    image: _propTypes.default.node,
    layout: _propTypes.default.string
  })),
  baseUrl: _propTypes.default.string
};

var NullComponent = function NullComponent() {
  return null;
};

function RemoteConfigurationTour(_ref2) {
  var url = _ref2.url,
      props = _objectWithoutProperties(_ref2, _excluded2);

  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      config = _useState2[0],
      setConfig = _useState2[1];

  function fetchData() {
    // eslint-disable-next-line no-undef
    fetch(url).then(function (response) {
      return response.json();
    }).then(function (response) {
      return setConfig(response);
    }).catch(function (error) {
      _loglevel.default.error("Error when loading configuration data:", error);
    });
  }

  (0, _react.useEffect)(function () {
    fetchData();
  }, // TODO: Feel free to fix this eslint error if you got time
  // eslint-disable-next-line react-hooks/exhaustive-deps
  []);

  if (!config) {
    return null;
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactErrorBoundary.default, {
    FallbackComponent: NullComponent,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(ConfigTour, _objectSpread({
      active: true,
      steps: config.steps,
      url: url
    }, props))
  });
}

RemoteConfigurationTour.propTypes = {
  url: _propTypes.default.string
};
var _default = RemoteConfigurationTour;
exports.default = _default;