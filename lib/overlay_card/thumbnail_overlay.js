"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _thumbnail_overlay = _interopRequireDefault(require("./thumbnail_overlay.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["className", "children", "visibility", "justify"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ThumbnailOverlay(_ref) {
  var _classNames;

  var className = _ref.className,
      children = _ref.children,
      _ref$visibility = _ref.visibility,
      visibility = _ref$visibility === void 0 ? "visible" : _ref$visibility,
      _ref$justify = _ref.justify,
      justify = _ref$justify === void 0 ? "start" : _ref$justify,
      props = _objectWithoutProperties(_ref, _excluded);

  var classes = (0, _classnames.default)(_thumbnail_overlay.default.root, (_classNames = {}, _defineProperty(_classNames, _thumbnail_overlay.default.justifyEnd, justify === "end"), _defineProperty(_classNames, _thumbnail_overlay.default.justifyBetween, justify === "between"), _defineProperty(_classNames, _thumbnail_overlay.default.visibilityHover, visibility === "hover"), _classNames), className);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", _objectSpread(_objectSpread({
    className: classes
  }, props), {}, {
    children: children
  }));
}

ThumbnailOverlay.propTypes = {
  className: _propTypes.default.string,
  justify: _propTypes.default.oneOf(["start", "end", "between"]),
  visibility: _propTypes.default.oneOf(["visible", "hover"]),
  children: _propTypes.default.node
};
var _default = ThumbnailOverlay;
exports.default = _default;