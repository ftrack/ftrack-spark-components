"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _checkbox = _interopRequireDefault(require("react-toolbox/lib/checkbox"));

var _classnames = _interopRequireDefault(require("classnames"));

var _style = _interopRequireDefault(require("./style.scss"));

var _checkbox_theme = _interopRequireDefault(require("./checkbox_theme.scss"));

var _thumbnail_overlay = _interopRequireDefault(require("./thumbnail_overlay"));

var _image = require("../image");

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["disabled", "selected", "thumbnailUrl", "onSelectedChange", "className", "children"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ThumbnailCard(_ref) {
  var _classNames;

  var disabled = _ref.disabled,
      selected = _ref.selected,
      thumbnailUrl = _ref.thumbnailUrl,
      onSelectedChange = _ref.onSelectedChange,
      className = _ref.className,
      children = _ref.children,
      props = _objectWithoutProperties(_ref, _excluded);

  var classes = (0, _classnames.default)(_style.default.thumbnailCard, className, (_classNames = {}, _defineProperty(_classNames, _style.default.selectedCard, selected), _defineProperty(_classNames, _style.default.disabledCard, disabled), _classNames));
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_image.Thumbnail, _objectSpread(_objectSpread({
    src: thumbnailUrl,
    onClick: function onClick() {
      onSelectedChange(!selected);
    },
    className: classes,
    rounded: true
  }, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_thumbnail_overlay.default, {
      justify: "between",
      className: _style.default.wrapper,
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _style.default.checkbox,
        children: disabled && !selected ? null : /*#__PURE__*/(0, _jsxRuntime.jsx)(_checkbox.default, {
          disabled: disabled,
          checked: selected,
          theme: _checkbox_theme.default
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _style.default.innerContent,
        children: children
      })]
    })
  }));
}

ThumbnailCard.propTypes = {
  disabled: _propTypes.default.bool,
  selected: _propTypes.default.bool,
  onClick: _propTypes.default.func,
  className: _propTypes.default.string,
  thumbnailUrl: _propTypes.default.string,
  onSelectedChange: _propTypes.default.func,
  children: _propTypes.default.node
};
var _default = ThumbnailCard;
exports.default = _default;