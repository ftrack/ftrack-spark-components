"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _button = require("react-toolbox/lib/button");

var _react = require("react");

var _classnames = _interopRequireDefault(require("classnames"));

var _reactIntl = require("react-intl");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _with_monitor_key_events = _interopRequireDefault(require("../util/hoc/with_monitor_key_events"));

var _icon_button = _interopRequireDefault(require("../icon_button"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var messages = (0, _reactIntl.defineMessages)({
  "message-x-of-y": {
    "id": "ftrack-spark-components.media_preview.message-x-of-y",
    "defaultMessage": "{x} of {y}"
  },
  "download-button": {
    "id": "ftrack-spark-components.media_preview.download",
    "defaultMessage": "Download"
  }
});
var ANIMATION_TIME = 200;

var stopPropagation = function stopPropagation(event) {
  event.stopPropagation();
  event.nativeEvent.stopImmediatePropagation();
};

var MediaPreview = /*#__PURE__*/function (_Component) {
  _inherits(MediaPreview, _Component);

  var _super = _createSuper(MediaPreview);

  function MediaPreview(props) {
    var _this;

    _classCallCheck(this, MediaPreview);

    _this = _super.call(this, props);
    _this.state = {
      visibleIndex: 0,
      mounted: false
    };
    _this.changeVisible = _this.changeVisible.bind(_assertThisInitialized(_this));
    _this.increase = _this.increase.bind(_assertThisInitialized(_this));
    _this.decrease = _this.decrease.bind(_assertThisInitialized(_this));
    _this.close = _this.close.bind(_assertThisInitialized(_this));
    _this.setMountAnimation = _this.setMountAnimation.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(MediaPreview, [{
    key: "componentWillMount",
    value: function componentWillMount() {
      this.setMountAnimation(true);

      if (this.props.visibleIndex || this.props.visibleIndex !== 0) {
        this.setState({
          visibleIndex: this.props.visibleIndex
        });
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.visible !== this.props.visible && nextProps.visible) {
        this.setMountAnimation(true);
      }

      if (nextProps.items !== this.props.items && nextProps.items.length > 0) {
        this.setMountAnimation(true);
      }

      if (nextProps.leftIsPressed) {
        this.decrease();
      }

      if (nextProps.rightIsPressed) {
        this.increase();
      }

      if (nextProps.escapeIsPressed !== this.props.escapeIsPressed && nextProps.escapeIsPressed) {
        this.close();
      }

      if (nextProps.visibleIndex !== this.props.visibleIndex) {
        this.setState({
          visibleIndex: nextProps.visibleIndex
        });
      }
    }
  }, {
    key: "setMountAnimation",
    value: function setMountAnimation(mounted) {
      var timer = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : ANIMATION_TIME;
      var that = this;
      setTimeout(function () {
        that.setState({
          mounted: mounted
        });
      }, timer);
    }
  }, {
    key: "changeVisible",
    value: function changeVisible(change) {
      var items = this.props.items;
      var min = 0;
      var max = items.length - 1;
      var oldVisibleIndex = this.state.visibleIndex !== null ? this.state.visibleIndex : this.props.visibleIndex;
      oldVisibleIndex = Math.min(Math.max(min, oldVisibleIndex), max);
      var visibleIndex = Math.min(Math.max(oldVisibleIndex + change, min), max);
      this.setState({
        visibleIndex: visibleIndex
      });
    }
  }, {
    key: "increase",
    value: function increase(event) {
      if (event) {
        stopPropagation(event);
      }

      this.changeVisible(1);
    }
  }, {
    key: "decrease",
    value: function decrease(event) {
      if (event) {
        stopPropagation(event);
      }

      this.changeVisible(-1);
    }
  }, {
    key: "close",
    value: function close() {
      var _this2 = this;

      var onClose = this.props.onClose;
      setTimeout(function () {
        onClose();

        _this2.setState({
          visibleIndex: 0
        });
      }, ANIMATION_TIME * 2);
      this.setMountAnimation(false);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          items = _this$props.items,
          visible = _this$props.visible,
          meta = _this$props.meta,
          intl = _this$props.intl,
          downloadLinks = _this$props.downloadLinks;
      var formatMessage = intl.formatMessage;
      var mounted = this.state.mounted;
      var max = items.length - 1;
      var min = 0;
      var visibleIndex = this.state.visibleIndex;

      if (visibleIndex > items.length) {
        visibleIndex = items.length - 1;
      } else if (visibleIndex < 0) {
        visibleIndex = 0;
      }

      var leftButtonClasses = (0, _classnames.default)(_style.default["left-button"], visibleIndex > min && _style.default.visible);
      var rightButtonClasses = (0, _classnames.default)(_style.default["right-button"], visibleIndex < max && _style.default.visible);

      if (items.length === 0 || !visible) {
        return null;
      }

      var containerClasses = (0, _classnames.default)(_style.default.container, mounted && _style.default.visible);
      return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        role: "button",
        className: containerClasses,
        onClick: this.close,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_icon_button.default, {
          icon: "close",
          inverse: true,
          className: _style.default["close-button"],
          onClick: this.close
        }), items.map(function (item, index) {
          if (index !== visibleIndex) {
            return null;
          }

          return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            className: _style.default["item-container"],
            children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
              onClick: stopPropagation,
              role: "presentation",
              className: _style.default["item-inner-container"],
              children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
                className: (0, _classnames.default)(_style.default.item, meta && meta[index] && _style.default["with-meta"]),
                children: item
              }), meta && meta[index] && /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
                className: _style.default.meta,
                children: [meta[index], downloadLinks && downloadLinks[index] && /*#__PURE__*/(0, _jsxRuntime.jsx)(_button.Button, {
                  icon: "get_app",
                  label: intl.formatMessage(messages["download-button"]),
                  onClick: function onClick() {
                    return window.open(downloadLinks[index]);
                  },
                  className: _style.default["download-button"]
                })]
              })]
            })
          }, "item-".concat(index));
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_button.Button, {
          icon: "keyboard_arrow_left",
          floating: true,
          className: leftButtonClasses,
          onClick: this.decrease
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_button.Button, {
          icon: "keyboard_arrow_right",
          inverse: true,
          floating: true,
          className: rightButtonClasses,
          onClick: this.increase
        }), items.length > 1 && /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default["text-container"],
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
            children: formatMessage(messages["message-x-of-y"], {
              x: visibleIndex + 1,
              y: items.length
            })
          })
        })]
      });
    }
  }]);

  return MediaPreview;
}(_react.Component);

MediaPreview.propTypes = {
  items: _propTypes.default.array,
  // eslint-disable-line
  meta: _propTypes.default.array,
  // eslint-disable-line
  downloadLinks: _propTypes.default.array,
  // eslint-disable-line
  onClose: _propTypes.default.func,
  visible: _propTypes.default.bool,
  leftIsPressed: _propTypes.default.bool,
  rightIsPressed: _propTypes.default.bool,
  escapeIsPressed: _propTypes.default.bool,
  visibleIndex: _propTypes.default.number,
  intl: _reactIntl.intlShape.isRequired
};
MediaPreview.defaultProps = {
  visible: true,
  visibleIndex: 0,
  intl: _reactIntl.intlShape.isRequired
};

var _default = (0, _with_monitor_key_events.default)({
  27: "escapeIsPressed",
  37: "leftIsPressed",
  39: "rightIsPressed"
})((0, _safe_inject_intl.default)(MediaPreview));

exports.default = _default;