"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.regexp.replace.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _loglevel = _interopRequireDefault(require("loglevel"));

var _video = _interopRequireDefault(require("./video"));

var _image = _interopRequireDefault(require("./image"));

var _constant = require("../util/constant");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var MediaFormats = {
  "ftrackreview-image": "image"
};

var browserSupports = function browserSupports(format) {
  var videoElement = document.createElement("video");
  var supported = false;

  try {
    if (videoElement.canPlayType) {
      if (format === "mp4") {
        supported = videoElement.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, "");
      } else if (format === "webm") {
        supported = videoElement.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, "");
      }
    }
  } catch (error) {
    _loglevel.default.error("Error when testing browser media support:", error);
  }

  return supported;
};

_constant.SUPPORTED_IMG_FILE_TYPES.forEach(function (fileType) {
  MediaFormats[".".concat(fileType)] = "image";
});

_constant.SUPPORTED_VIDEO_FILE_TYPES.forEach(function (fileType) {
  if (browserSupports(fileType)) {
    MediaFormats[".".concat(fileType)] = "video";
  }
});
/**
 * Add 'ftrackreview-mp4' and 'ftrackreview-webm' as fallback when
 * file_type is not set on the media component
 */


if (Object.prototype.hasOwnProperty.call(MediaFormats, ".mp4")) {
  MediaFormats["ftrackreview-mp4"] = "video";
}

if (Object.prototype.hasOwnProperty.call(MediaFormats, ".webm")) {
  MediaFormats["ftrackreview-webm"] = "video";
}

var MediaComponents = {
  image: _image.default,
  video: _video.default
};

var Media = /*#__PURE__*/function (_Component) {
  _inherits(Media, _Component);

  var _super = _createSuper(Media);

  function Media() {
    var _this;

    _classCallCheck(this, Media);

    _this = _super.call(this);
    _this.state = {
      url: null,
      format: "",
      fileType: ""
    };
    return _this;
  }

  _createClass(Media, [{
    key: "componentWillMount",
    value: function componentWillMount() {
      var component = this.props.component;

      if (component) {
        this.setComponent(component);
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.component !== this.props.component) {
        var component = nextProps.component;
        this.setComponent(component);
      }
    }
  }, {
    key: "setComponent",
    value: function setComponent(component) {
      var session = this.props.session; // Fall back to component name if file_type is not set.

      var fileType = component.file_type || component.name;
      var format = MediaFormats[fileType.trim().toLowerCase()];
      this.setState({
        url: component.url || session.getComponentUrl(component.id),
        format: format,
        fileType: fileType
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$state = this.state,
          format = _this$state.format,
          url = _this$state.url;

      if (!url || !format) {
        return null;
      }

      var MediaComponent = MediaComponents[format];
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(MediaComponent, {
        url: url
      });
    }
  }]);

  return Media;
}(_react.Component);

Media.propTypes = {
  session: _propTypes.default.object.isRequired,
  // eslint-disable-line
  component: _propTypes.default.object // eslint-disable-line

};
var _default = Media;
exports.default = _default;