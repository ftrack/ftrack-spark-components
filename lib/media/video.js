"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactToolbox = require("react-toolbox");

var _reactIntl = require("react-intl");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var messages = (0, _reactIntl.defineMessages)({
  "message-video-not-supported": {
    "id": "ftrack-spark-components.media.video.message-video-not-supported",
    "defaultMessage": "Your browser does not support the video tag."
  }
});

var Video = /*#__PURE__*/function (_Component) {
  _inherits(Video, _Component);

  var _super = _createSuper(Video);

  function Video() {
    var _this;

    _classCallCheck(this, Video);

    _this = _super.call(this);
    _this.state = {
      height: 0,
      width: 0
    };
    return _this;
  }

  _createClass(Video, [{
    key: "componentWillMount",
    value: function componentWillMount() {
      var _this2 = this;

      /**
       * Load video "off-dome" to get original width and height
       * of the clip. Remove it when done.
       */
      var url = this.props.url;
      var video = document.createElement("video");
      video.setAttribute("style", "opacity: 0;");
      video.autoplay = true;

      video.oncanplay = function (event) {
        var height = event.target.offsetHeight;
        var width = event.target.offsetWidth;

        _this2.setState({
          width: width,
          height: height
        });

        document.body.removeChild(video);
      };

      document.body.appendChild(video);
      video.src = url;
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          url = _this$props.url,
          intl = _this$props.intl;
      var _this$state = this.state,
          width = _this$state.width,
          height = _this$state.height;
      var formatMessage = intl.formatMessage;

      if (!height && !width) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default.loading,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactToolbox.ProgressBar, {
            type: "circular",
            mode: "indeterminate"
          })
        });
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _style.default["video-container"],
        style: {
          maxWidth: width,
          maxHeight: height
        },
        children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("video", {
          className: _style.default.video,
          autoPlay: true,
          controls: true,
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("source", {
            src: url,
            type: "video/mp4"
          }), formatMessage(messages["message-video-not-supported"])]
        })
      });
    }
  }]);

  return Video;
}(_react.Component);

Video.propTypes = {
  url: _propTypes.default.string.isRequired,
  intl: _reactIntl.intlShape.isRequired
};

var _default = (0, _safe_inject_intl.default)(Video);

exports.default = _default;