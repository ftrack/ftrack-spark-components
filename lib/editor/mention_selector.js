"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _material = require("@mui/material");

var _resource_selector = require("../selector/resource_selector");

var _localize_selector = require("../selector/localize_selector");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _mention_selector = _interopRequireDefault(require("./mention_selector.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["session"],
    _excluded2 = ["session", "mentionState", "setMentionState"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var MentionSelectorItem = function MentionSelectorItem(_ref) {
  var session = _ref.session,
      props = _objectWithoutProperties(_ref, _excluded);

  var focusedOption = props.focusedOption,
      focusOption = props.focusOption,
      key = props.key,
      option = props.option,
      selectValue = props.selectValue,
      userLabel = props.userLabel,
      userInactiveLabel = props.userInactiveLabel,
      groupLabel = props.groupLabel;
  var className = [_mention_selector.default["mention-option--resource"]];

  if (option === focusedOption) {
    className.push(_mention_selector.default["mention-option--focused"]);
  }

  if (option.disabled) {
    className.push(_mention_selector.default["mention-option--disabled"]);
  }

  var events = option.disabled ? {} : {
    onClick: function onClick(e) {
      return selectValue(option, e);
    },
    onMouseOver: function onMouseOver() {
      return focusOption(option);
    }
  };
  var data = option.data,
      label = option.label,
      type = option.type;

  var scrollTo = function scrollTo(ref) {
    if (ref && option === focusedOption) {
      ref.scrollIntoView({
        block: "nearest",
        inline: "nearest"
      });
    }
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", _objectSpread(_objectSpread({
    "data-mention": true,
    className: className.join(" "),
    ref: scrollTo
  }, events), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _mention_selector.default["mention-option--container"],
      children: [type !== "label" ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_resource_selector.ResourceCircle, {
        className: _mention_selector.default["mention-option--avatar"],
        session: session,
        type: type,
        data: data,
        label: label
      }) : null, /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        className: _mention_selector.default["mention-option--label"],
        children: label
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        className: _mention_selector.default["mention-option--dash"],
        children: "-"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_resource_selector.ResourceOptionSubtitle, _objectSpread({
        className: _mention_selector.default["mention-option--subtitle"],
        type: type,
        userLabel: userLabel,
        userInactiveLabel: userInactiveLabel,
        groupLabel: groupLabel
      }, data))]
    })
  }), key);
};

MentionSelectorItem.propTypes = {
  session: _propTypes.default.object,
  focusedOption: _propTypes.default.object,
  focusOption: _propTypes.default.func,
  key: _propTypes.default.string,
  option: _propTypes.default.object,
  selectValue: _propTypes.default.func,
  groupLabel: _propTypes.default.node,
  userLabel: _propTypes.default.node,
  userInactiveLabel: _propTypes.default.node
};

var MentionSelector = function MentionSelector(_ref2) {
  var session = _ref2.session,
      mentionState = _ref2.mentionState,
      setMentionState = _ref2.setMentionState,
      props = _objectWithoutProperties(_ref2, _excluded2);

  var show = mentionState.show,
      decorationNode = mentionState.decorationNode,
      _mentionState$items = mentionState.items,
      items = _mentionState$items === void 0 ? [] : _mentionState$items,
      focusedOption = mentionState.focusedOption,
      insertMention = mentionState.command,
      range = mentionState.range;
  var decorationVisible = decorationNode && decorationNode.offsetParent !== null;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.Popper, {
    open: show && decorationVisible && items.length > 0,
    anchorEl: decorationNode,
    placement: "top-start",
    style: {
      zIndex: 300
    },
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
      className: _mention_selector.default["mention-selector"],
      children: items.map(function (mention) {
        return MentionSelectorItem({
          session: session,
          option: mention,
          focusedOption: focusedOption || items[0],
          selectValue: function selectValue(option) {
            insertMention({
              range: range,
              attrs: {
                id: option.value,
                label: option.label,
                type: option.type
              }
            });
          },
          focusOption: function focusOption(option) {
            setMentionState(_objectSpread(_objectSpread({}, mentionState), {}, {
              focusedOption: option
            }));
          },
          props: props
        });
      })
    })
  });
};

MentionSelector.propTypes = {
  session: _propTypes.default.object,
  mentionState: _propTypes.default.shape({
    show: _propTypes.default.bool,
    items: _propTypes.default.array,
    range: _propTypes.default.object,
    command: _propTypes.default.func,
    focusedOption: _propTypes.default.object,
    decorationNode: _propTypes.default.object
  }),
  setMentionState: _propTypes.default.func,
  groupLabel: _propTypes.default.node,
  userLabel: _propTypes.default.node,
  userInactiveLabel: _propTypes.default.node
};
MentionSelector.defaultProps = {
  mentionState: {
    show: false,
    items: []
  }
};

var _default = (0, _safe_inject_intl.default)((0, _localize_selector.localizeResourceSelector)( /*#__PURE__*/(0, _react.memo)(MentionSelector)));

exports.default = _default;