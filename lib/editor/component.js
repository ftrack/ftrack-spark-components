"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.promise.js");

require("core-js/modules/es6.weak-map.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.regexp.replace.js");

require("core-js/modules/es7.array.includes.js");

require("regenerator-runtime/runtime.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactIntl = require("react-intl");

var _uuid = require("uuid");

var _useOnclickoutside = _interopRequireDefault(require("use-onclickoutside"));

var _recompose = require("recompose");

var _useProsemirror = require("use-prosemirror");

var _prosemirrorKeymap = require("prosemirror-keymap");

var _prosemirrorDropcursor = require("prosemirror-dropcursor");

var _prosemirrorGapcursor = require("prosemirror-gapcursor");

var _prosemirrorCommands = require("prosemirror-commands");

var _prosemirrorHistory = require("prosemirror-history");

var _prosemirrorSchemaList = require("prosemirror-schema-list");

var _prosemirrorModel = require("prosemirror-model");

var _tooltip = _interopRequireDefault(require("react-toolbox/lib/tooltip"));

var _progress_bar = _interopRequireDefault(require("react-toolbox/lib/progress_bar"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _hoc = require("../util/hoc");

var _icon_button = _interopRequireDefault(require("../icon_button"));

var _utils = require("./utils");

var _schema = _interopRequireWildcard(require("./schema"));

var _keymap = _interopRequireDefault(require("./keymap"));

var _inputrules = _interopRequireDefault(require("./inputrules"));

var _placeholder_plugin = _interopRequireDefault(require("./placeholder_plugin"));

var _styling_tooltip_plugin = _interopRequireDefault(require("./styling_tooltip_plugin"));

var _styling_tooltip = _interopRequireDefault(require("./styling_tooltip"));

var _suggestions_plugin = _interopRequireDefault(require("./suggestions_plugin"));

var _resource_selector = require("../selector/resource_selector");

var _markdown = require("../markdown");

var _style = _interopRequireDefault(require("./style.scss"));

var _link_dialog = _interopRequireDefault(require("./link_dialog"));

var _mention_selector = _interopRequireDefault(require("./mention_selector"));

var _jsxRuntime = require("react/jsx-runtime");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var TooltipIconButton = (0, _tooltip.default)(_icon_button.default);
var messages = (0, _reactIntl.defineMessages)({
  "mention-button-tooltip": {
    "id": "ftrack-spark-components.editor.mention-button-tooltip",
    "defaultMessage": "Mention someone"
  },
  "emoji-button-tooltip": {
    "id": "ftrack-spark-components.editor.emoji-button-tooltip",
    "defaultMessage": "Emoji"
  },
  "attach-button-tooltip": {
    "id": "ftrack-spark-components.editor.attach-button-tooltip",
    "defaultMessage": "Attach file"
  },
  "send-button-tooltip": {
    "id": "ftrack-spark-components.editor.send-button-tooltip",
    "defaultMessage": "Send message"
  }
});
var MENTION_INPUT_DELAY = 100;

var editorState = function editorState(_ref) {
  var content = _ref.content,
      placeholder = _ref.placeholder,
      onSelection = _ref.onSelection,
      canMention = _ref.canMention,
      onMentionEnter = _ref.onMentionEnter,
      onMentionChange = _ref.onMentionChange,
      onMentionFilter = _ref.onMentionFilter,
      onMentionExit = _ref.onMentionExit,
      onMentionKeyDown = _ref.onMentionKeyDown;
  var plugins = [(0, _inputrules.default)(_schema.default), (0, _prosemirrorDropcursor.dropCursor)(), (0, _prosemirrorGapcursor.gapCursor)(), (0, _prosemirrorHistory.history)(), (0, _placeholder_plugin.default)({
    content: placeholder
  }), (0, _styling_tooltip_plugin.default)({
    onSelection: onSelection
  })];

  if (canMention) {
    plugins.push((0, _suggestions_plugin.default)({
      matcher: {
        char: "@",
        regex: _utils.MENTION_SUGGESTION_REGEX
      },
      onEnter: onMentionEnter,
      onChange: onMentionChange,
      onFilter: onMentionFilter,
      onExit: onMentionExit,
      onKeyDown: onMentionKeyDown
    }));
  }

  plugins.push((0, _prosemirrorKeymap.keymap)((0, _keymap.default)(_schema.default)), (0, _prosemirrorKeymap.keymap)(_prosemirrorCommands.baseKeymap));
  return {
    nodes: _schema.default.nodes,
    marks: _schema.default.marks,
    doc: _schema.markdownParser.parse((0, _markdown.replaceHtmlBreaks)(content)),
    plugins: plugins
  };
};

var Editor = function Editor(_ref2) {
  var className = _ref2.className,
      session = _ref2.session,
      autoFocus = _ref2.autoFocus,
      legacyStyle = _ref2.legacyStyle,
      disabled = _ref2.disabled,
      progressing = _ref2.progressing,
      placeholder = _ref2.placeholder,
      submitTitle = _ref2.submitTitle,
      content = _ref2.content,
      recipients = _ref2.recipients,
      canAttach = _ref2.canAttach,
      onChange = _ref2.onChange,
      onAttach = _ref2.onAttach,
      onSend = _ref2.onSend,
      onFocus = _ref2.onFocus,
      onBlur = _ref2.onBlur,
      onUploadStarted = _ref2.onUploadStarted,
      onUploadProgress = _ref2.onUploadProgress,
      onUploadError = _ref2.onUploadError,
      onAbortedUpload = _ref2.onAbortedUpload,
      onFinalizedUpload = _ref2.onFinalizedUpload,
      canMention = _ref2.canMention;
  var classes = (0, _classnames.default)(legacyStyle ? _style.default["wrapper-legacy"] : _style.default.wrapper, className);
  var editorViewRef = (0, _react.useRef)();

  var _useState = (0, _react.useState)({
    selectedMarks: [],
    selectedNodes: []
  }),
      _useState2 = _slicedToArray(_useState, 2),
      selectionState = _useState2[0],
      setSelectionState = _useState2[1];

  var _useState3 = (0, _react.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      isFocused = _useState4[0],
      setIsFocused = _useState4[1];

  var _useState5 = (0, _react.useState)(false),
      _useState6 = _slicedToArray(_useState5, 2),
      showLinkDialog = _useState6[0],
      setShowLinkDialog = _useState6[1];

  var _useState7 = (0, _react.useState)({
    show: false,
    focusedOption: null
  }),
      _useState8 = _slicedToArray(_useState7, 2),
      mentionState = _useState8[0],
      setMentionState = _useState8[1]; // TODO: Feel free to fix this eslint error if you got time
  // eslint-disable-next-line react-hooks/exhaustive-deps


  var fetchMentionOptions = (0, _react.useCallback)((0, _utils.asyncDebounce)((0, _resource_selector.getLoadResourceOptions)({
    session: session,
    maxResults: 25,
    activeUsersOnly: true,
    groups: true,
    memberships: false
  }), MENTION_INPUT_DELAY), []);

  var _useProseMirror = (0, _useProsemirror.useProseMirror)(editorState({
    content: (0, _utils.injectMentions)(content, recipients),
    placeholder: placeholder,
    onSelection: setSelectionState,
    canMention: canMention,
    onMentionEnter: function onMentionEnter(props) {
      setMentionState(_objectSpread({
        show: true,
        focusedOption: null
      }, props));
    },
    onMentionFilter: function () {
      var _onMentionFilter = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(query) {
        var _yield$fetchMentionOp, options;

        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return fetchMentionOptions(query);

              case 2:
                _yield$fetchMentionOp = _context.sent;
                options = _yield$fetchMentionOp.options;
                return _context.abrupt("return", options);

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function onMentionFilter(_x) {
        return _onMentionFilter.apply(this, arguments);
      }

      return onMentionFilter;
    }(),
    onMentionChange: function onMentionChange(props) {
      setMentionState(function (prevState) {
        return _objectSpread(_objectSpread({}, prevState), {}, {
          show: true
        }, props);
      });
    },
    onMentionExit: function onMentionExit(props) {
      setMentionState(_objectSpread({
        show: false,
        focusedOption: null
      }, props));
    },
    onMentionKeyDown: function onMentionKeyDown(props) {
      var isArrowDown = props.event.keyCode === 40;
      var isArrowUp = props.event.keyCode === 38;
      var isEnterKey = props.event.keyCode === 13;
      var isEscKey = props.event.keyCode === 27;

      if (isArrowDown || isArrowUp) {
        props.event.preventDefault();
        setMentionState(function (prevState) {
          var newFocusedOptionIndex = 0;

          if (prevState.focusedOption !== null && prevState.items.length) {
            var focusedOptionIndex = prevState.items.indexOf(prevState.focusedOption);

            if (isArrowDown && focusedOptionIndex < prevState.items.length - 1) {
              newFocusedOptionIndex = focusedOptionIndex + 1;
            } else if (isArrowUp && focusedOptionIndex > 0) {
              newFocusedOptionIndex = focusedOptionIndex - 1;
            } else if (isArrowUp && focusedOptionIndex === 0) {
              newFocusedOptionIndex = prevState.items.length - 1;
            }
          }

          return _objectSpread(_objectSpread({}, prevState), {}, {
            focusedOption: prevState.items[newFocusedOptionIndex] || null
          });
        });
      } else if (isEnterKey) {
        setMentionState(function (prevState) {
          var focusedOption = prevState.focusedOption || prevState.items[0];

          if (focusedOption != null) {
            prevState.command({
              range: prevState.range,
              attrs: {
                id: focusedOption.value,
                label: focusedOption.label,
                type: focusedOption.type
              }
            });
          }

          return _objectSpread(_objectSpread({}, prevState), {}, {
            focusedOption: null
          });
        });
        return true;
      } else if (isEscKey) {
        setMentionState(_objectSpread(_objectSpread({}, props), {}, {
          show: false,
          focusedOption: null
        }));
        return true;
      }

      return false;
    }
  })),
      _useProseMirror2 = _slicedToArray(_useProseMirror, 2),
      state = _useProseMirror2[0],
      setState = _useProseMirror2[1];

  var hasContent = state.doc.childCount > 1 || state.doc.textContent.length;

  var sendAction = function sendAction() {
    var serializedMarkdown = _schema.markdownSerializer.serialize(state.doc);

    onSend((0, _utils.extractMentions)(serializedMarkdown));
  };

  var uploadFiles = function uploadFiles(files) {
    var uploadItems = files.map(function (file) {
      return {
        id: (0, _uuid.v4)(),
        xhr: new XMLHttpRequest(),
        file: file
      };
    });

    if (onUploadStarted) {
      var shouldContinue = onUploadStarted(uploadItems);

      if (shouldContinue === false) {
        return;
      }
    }

    var promises = files.map(function (file, index) {
      var xhr = uploadItems[index].xhr;
      return session.createComponent(file, {
        onAbortedUpload: onAbortedUpload,
        xhr: xhr,
        onProgress: function onProgress(progress) {
          return onUploadProgress && onUploadProgress(progress, uploadItems[index].id);
        },
        data: {
          id: uploadItems[index].id
        }
      });
    });
    promises.forEach(function (p) {
      p.then(function (res) {
        onFinalizedUpload(res[0].data.id);
      }).catch(function (err) {
        onUploadError(err);
      });
    });
  };

  var pasteTransformer = function pasteTransformer(slice) {
    var images = [];
    var children = [];
    slice.content.forEach(function (child) {
      var newChild = child;

      if (child.type.name === "image") {
        images.push(child.attrs);
        newChild = _prosemirrorModel.Fragment.empty;
      } else {
        child.descendants(function (node, pos) {
          if (node.type.name === "image") {
            images.push(child.attrs);
            newChild = newChild.replace(pos, pos + 1, new _prosemirrorModel.Slice(_prosemirrorModel.Fragment.empty, 0, 0));
          }
        });
      }

      children.push(newChild);
    });

    if (canAttach) {
      var files = (0, _utils.fetchImagesAndConvertToFiles)(images);
      uploadFiles(files);
    }

    return new _prosemirrorModel.Slice(_prosemirrorModel.Fragment.fromArray(children), slice.openStart, slice.openEnd);
  };

  (0, _react.useEffect)(function () {
    if (autoFocus) {
      editorViewRef.current.view.focus();
    }
  }, // TODO: Feel free to fix this eslint error if you got time
  // eslint-disable-next-line react-hooks/exhaustive-deps
  []);
  var wrapperRef = (0, _react.useRef)(null);
  (0, _useOnclickoutside.default)(wrapperRef, function (e) {
    var formatToolSelected = ["format_bold", "format_italic", "format_strikethrough", "insert_link", "format_list_bulleted"].includes(e.target.innerText);
    var ancestor = e.target.closest("[data-mention]");
    var mentionSelected = ancestor && ancestor.hasAttribute("data-mention");

    if (!formatToolSelected && !mentionSelected) {
      setSelectionState(_objectSpread(_objectSpread({}, selectionState), {}, {
        empty: true
      }));
    }
  });
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    ref: wrapperRef,
    className: classes,
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_useProsemirror.ProseMirror, {
      ref: editorViewRef,
      className: _style.default.editor,
      state: state,
      dispatchTransaction: function dispatchTransaction(transaction) {
        var newState = state.apply(transaction);

        if (transaction.docChanged) {
          var serializedMarkdown = _schema.markdownSerializer.serialize(newState.doc);

          onChange((0, _utils.extractMentions)(serializedMarkdown));
        }

        setState(newState);
      },
      handleDOMEvents: {
        blur: function blur() {
          onBlur();
          setIsFocused(false);
        },
        focus: function focus() {
          onFocus();
          setIsFocused(true);
        },
        beforeinput: function beforeinput(view, event) {
          switch (event.inputType) {
            case "historyUndo":
              (0, _prosemirrorHistory.undo)(view.state, view.dispatch);
              event.preventDefault();
              return true;

            case "historyRedo":
              (0, _prosemirrorHistory.redo)(view.state, view.dispatch);
              event.preventDefault();
              return true;

            default:
              return false;
          }
        }
      },
      transformPasted: pasteTransformer
    }), legacyStyle && /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default["bottom-bar"],
      children: [canMention && /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        icon: "alternate_email",
        onClick: function onClick() {
          var tr = state.tr.insertText(" @");
          editorViewRef.current.view.dispatch(tr);
          editorViewRef.current.view.focus();
        },
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["mention-button-tooltip"])),
        tooltipPosition: "top"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        className: "".concat(isFocused ? _style.default["bar-focused"] : _style.default.bar)
      })]
    }), !legacyStyle && /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default["bottom-menu"],
      style: {
        position: hasContent ? "initial" : "absolute"
      },
      children: [canAttach && /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        icon: "attach_file",
        onClick: onAttach,
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["attach-button-tooltip"])),
        tooltipPosition: "top"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: "".concat(_style.default["send-button"], " ").concat(hasContent && _style.default.active),
        children: progressing ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_progress_bar.default, {
          className: _style.default.spinner,
          type: "circular",
          mode: "indeterminate"
        }) : /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
          icon: "send",
          onClick: sendAction,
          tooltip: submitTitle || /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["send-button-tooltip"])),
          tooltipPosition: "top",
          disabled: !hasContent
        })
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styling_tooltip.default, {
      isFocused: isFocused,
      selectionState: selectionState,
      onFormatBold: function onFormatBold() {
        (0, _prosemirrorCommands.toggleMark)(_schema.default.marks.strong)(state, editorViewRef.current.view.dispatch);
        editorViewRef.current.view.focus();
      },
      onFormatItalic: function onFormatItalic() {
        (0, _prosemirrorCommands.toggleMark)(_schema.default.marks.em)(state, editorViewRef.current.view.dispatch);
        editorViewRef.current.view.focus();
      },
      onFormatStrikethrough: function onFormatStrikethrough() {
        (0, _prosemirrorCommands.toggleMark)(_schema.default.marks.strikethrough)(state, editorViewRef.current.view.dispatch);
        editorViewRef.current.view.focus();
      },
      onInsertLink: function onInsertLink() {
        setSelectionState(_objectSpread(_objectSpread({}, selectionState), {}, {
          empty: true
        }));
        setShowLinkDialog(!showLinkDialog);
      },
      onRemoveLink: function onRemoveLink() {
        (0, _prosemirrorCommands.toggleMark)(_schema.default.marks.link)(state, editorViewRef.current.view.dispatch);
        editorViewRef.current.view.focus();
      },
      onFormatList: function onFormatList() {
        (0, _prosemirrorSchemaList.wrapInList)(_schema.default.nodes.bullet_list)(state, editorViewRef.current.view.dispatch);
        editorViewRef.current.view.focus();
      },
      onLiftList: function onLiftList() {
        (0, _prosemirrorSchemaList.liftListItem)(_schema.default.nodes.list_item)(state, editorViewRef.current.view.dispatch);
        editorViewRef.current.view.focus();
      }
    }), showLinkDialog && /*#__PURE__*/(0, _jsxRuntime.jsx)(_link_dialog.default, {
      selection: state.selection.$head.parent.textContent,
      onCancel: function onCancel() {
        return setShowLinkDialog(false);
      },
      onInsertLink: function onInsertLink(link) {
        (0, _prosemirrorCommands.toggleMark)(_schema.default.marks.link, link)(state, editorViewRef.current.view.dispatch);
        editorViewRef.current.view.focus();
        setShowLinkDialog(false);
      }
    }), canMention && /*#__PURE__*/(0, _jsxRuntime.jsx)(_mention_selector.default, {
      session: session,
      mentionState: mentionState,
      setMentionState: setMentionState
    })]
  });
};

Editor.propTypes = {
  session: _propTypes.default.shape({
    createComponent: _propTypes.default.func.isRequired
  }).isRequired,
  className: _propTypes.default.string,
  legacyStyle: _propTypes.default.bool,
  autoFocus: _propTypes.default.bool,
  disabled: _propTypes.default.bool,
  progressing: _propTypes.default.bool,
  placeholder: _propTypes.default.string,
  submitTitle: _propTypes.default.string,
  content: _propTypes.default.string,
  recipients: _propTypes.default.arrayOf(_propTypes.default.shape({
    resource_id: _propTypes.default.string,
    text_mentioned: _propTypes.default.string
  })),
  canAttach: _propTypes.default.bool,
  onAttach: _propTypes.default.func,
  onChange: _propTypes.default.func,
  onSend: _propTypes.default.func,
  onFocus: _propTypes.default.func,
  onBlur: _propTypes.default.func,
  onUploadStarted: _propTypes.default.func,
  onUploadProgress: _propTypes.default.func,
  onUploadError: _propTypes.default.func,
  onAbortedUpload: _propTypes.default.func,
  onFinalizedUpload: _propTypes.default.func,
  canMention: _propTypes.default.bool
};
Editor.defaultProps = {
  legacyStyle: false,
  autoFocus: false,
  disabled: false,
  progressing: false,
  placeholder: "Write something...",
  content: "",
  recipients: [],
  canAttach: false,
  onAttach: function onAttach() {},
  onChange: function onChange() {},
  onSend: function onSend() {},
  onFocus: function onFocus() {},
  onBlur: function onBlur() {},
  onFinalizedUpload: function onFinalizedUpload() {},
  canMention: false
};

var _default = (0, _recompose.compose)(_hoc.withSession, _safe_inject_intl.default)(Editor);

exports.default = _default;