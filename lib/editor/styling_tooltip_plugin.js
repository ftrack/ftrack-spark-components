"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.filter.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _prosemirrorView = require("prosemirror-view");

var _prosemirrorState = require("prosemirror-state");

var _utils = require("./utils");

var _schema = _interopRequireDefault(require("./schema"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/** ProseMirror Plugin to show styling tooltip for text selection
 * along with selected nodes and marks. */
var StylingTooltipPlugin = /*#__PURE__*/function () {
  function StylingTooltipPlugin(_ref) {
    var editorView = _ref.editorView,
        onSelection = _ref.onSelection;

    _classCallCheck(this, StylingTooltipPlugin);

    this.onSelection = onSelection;
    this.update(editorView, null);
  }

  _createClass(StylingTooltipPlugin, [{
    key: "update",
    value: function update(view, lastState) {
      var state = view.state;
      if (lastState && lastState.doc.eq(state.doc) && lastState.selection.eq(state.selection)) return;
      var _state$selection = state.selection,
          from = _state$selection.from,
          $from = _state$selection.$from,
          to = _state$selection.to,
          empty = _state$selection.empty;
      var start = view.coordsAtPos(from);
      var end = view.coordsAtPos(to);
      var left = Math.max((start.left + end.left) / 2, start.left + 3);
      var anchorEl = {
        getBoundingClientRect: (0, _utils.generateGetBoundingClientRect)(left, start.top - 40),
        contextElement: view.dom
      };
      var selectedMarks = [_schema.default.marks.strong, _schema.default.marks.em, _schema.default.marks.strikethrough, _schema.default.marks.link].filter(function (m) {
        return state.doc.rangeHasMark(from, to, m);
      });
      var selectedNodes = [_schema.default.nodes.list_item].filter(function (n) {
        return $from.node(2) && $from.node(2).type === n;
      });
      this.onSelection({
        empty: empty,
        anchorEl: anchorEl,
        selectedMarks: selectedMarks,
        selectedNodes: selectedNodes
      });
    }
  }]);

  return StylingTooltipPlugin;
}();

StylingTooltipPlugin.propTypes = {
  editorView: _prosemirrorView.EditorView,
  onSelection: _propTypes.default.func
};

var stylingTooltipPlugin = function stylingTooltipPlugin(_ref2) {
  var onSelection = _ref2.onSelection;
  return new _prosemirrorState.Plugin({
    view: function view(editorView) {
      return new StylingTooltipPlugin({
        editorView: editorView,
        onSelection: onSelection
      });
    }
  });
};

var _default = stylingTooltipPlugin;
exports.default = _default;