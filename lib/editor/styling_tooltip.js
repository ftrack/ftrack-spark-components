"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.string.includes.js");

require("core-js/modules/es7.array.includes.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _tooltip = _interopRequireDefault(require("react-toolbox/lib/tooltip"));

var _reactIntl = require("react-intl");

var _material = require("@mui/material");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _icon_button = _interopRequireDefault(require("../icon_button"));

var _utils = require("./utils");

var _schema = _interopRequireDefault(require("./schema"));

var _style = _interopRequireDefault(require("./style.scss"));

var _tooltip_button_theme = _interopRequireDefault(require("./tooltip_button_theme.scss"));

var _tooltip_active_button_theme = _interopRequireDefault(require("./tooltip_active_button_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var messages = (0, _reactIntl.defineMessages)({
  "bold-format-tooltip": {
    "id": "ftrack-spark-components.editor.bold-format-tooltip",
    "defaultMessage": "Bold"
  },
  "italic-format-tooltip": {
    "id": "ftrack-spark-components.editor.italic-format-tooltip",
    "defaultMessage": "Italic"
  },
  "strikethrough-format-tooltip": {
    "id": "ftrack-spark-components.editor.strikethrough-format-tooltip",
    "defaultMessage": "Strikethrough"
  },
  "insert-link-format-tooltip": {
    "id": "ftrack-spark-components.editor.insert-link-format-tooltip",
    "defaultMessage": "Insert link"
  },
  "remove-link-format-tooltip": {
    "id": "ftrack-spark-components.editor.remove-link-format-tooltip",
    "defaultMessage": "Remove link"
  },
  "list-format-tooltip": {
    "id": "ftrack-spark-components.editor.list-format-tooltip",
    "defaultMessage": "List"
  }
});
var TooltipIconButton = (0, _tooltip.default)(_icon_button.default);

var StylingTooltip = function StylingTooltip(_ref) {
  var onFormatBold = _ref.onFormatBold,
      onFormatItalic = _ref.onFormatItalic,
      onFormatStrikethrough = _ref.onFormatStrikethrough,
      onFormatList = _ref.onFormatList,
      onLiftList = _ref.onLiftList,
      onInsertLink = _ref.onInsertLink,
      onRemoveLink = _ref.onRemoveLink,
      selectionState = _ref.selectionState;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_material.Popper, {
    open: !selectionState.empty,
    anchorEl: selectionState.anchorEl || null,
    placement: "top",
    style: {
      zIndex: 300
    },
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: _style.default.tooltip,
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        icon: "format_bold",
        onClick: onFormatBold,
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["bold-format-tooltip"])),
        tooltipPosition: "top",
        theme: selectionState.selectedMarks.includes(_schema.default.marks.strong) ? _tooltip_active_button_theme.default : _tooltip_button_theme.default
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        icon: "format_italic",
        onClick: onFormatItalic,
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["italic-format-tooltip"])),
        tooltipPosition: "top",
        theme: selectionState.selectedMarks.includes(_schema.default.marks.em) ? _tooltip_active_button_theme.default : _tooltip_button_theme.default
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        icon: "format_strikethrough",
        onClick: onFormatStrikethrough,
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["strikethrough-format-tooltip"])),
        tooltipPosition: "top",
        theme: selectionState.selectedMarks.includes(_schema.default.marks.strikethrough) ? _tooltip_active_button_theme.default : _tooltip_button_theme.default
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _style.default.divider
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        icon: "insert_link",
        onClick: selectionState.selectedMarks.includes(_schema.default.marks.link) ? onRemoveLink : onInsertLink,
        tooltip: selectionState.selectedMarks.includes(_schema.default.marks.link) ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["remove-link-format-tooltip"])) : /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["insert-link-format-tooltip"])),
        tooltipPosition: "top",
        theme: selectionState.selectedMarks.includes(_schema.default.marks.link) ? _tooltip_active_button_theme.default : _tooltip_button_theme.default
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: _style.default.divider
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(TooltipIconButton, {
        icon: "format_list_bulleted",
        onClick: selectionState.selectedNodes.includes(_schema.default.nodes.list_item) ? onLiftList : onFormatList,
        tooltip: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["list-format-tooltip"])),
        tooltipPosition: "top",
        theme: selectionState.selectedNodes.includes(_schema.default.nodes.list_item) ? _tooltip_active_button_theme.default : _tooltip_button_theme.default
      })]
    })
  });
};

StylingTooltip.propTypes = {
  onFormatBold: _propTypes.default.func,
  onFormatItalic: _propTypes.default.func,
  onFormatStrikethrough: _propTypes.default.func,
  onFormatList: _propTypes.default.func,
  onLiftList: _propTypes.default.func,
  onInsertLink: _propTypes.default.func,
  onRemoveLink: _propTypes.default.func,
  selectionState: _propTypes.default.shape({
    empty: _propTypes.default.bool,
    anchorEl: _propTypes.default.object,
    selectedMarks: _propTypes.default.array
  })
};
StylingTooltip.defaultProps = {
  selectionState: {
    empty: true,
    anchorEl: (0, _utils.generateGetBoundingClientRect)(0, 0),
    selectedMarks: []
  }
};

var _default = (0, _safe_inject_intl.default)( /*#__PURE__*/(0, _react.memo)(StylingTooltip));

exports.default = _default;