"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _prosemirrorState = require("prosemirror-state");

var _prosemirrorView = require("prosemirror-view");

var _style = _interopRequireDefault(require("./style.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2021 ftrack

/** ProseMirror Plugin to show placeholder on empty editor state. */
var PlaceholderPlugin = function PlaceholderPlugin(_ref) {
  var content = _ref.content;
  return new _prosemirrorState.Plugin({
    props: {
      decorations: function decorations(state) {
        var decorations = [];

        var decorate = function decorate(node, pos) {
          var hasContent = state.doc.childCount > 1 || state.doc.textContent.length;

          if (node.type.isBlock && node.childCount === 0 && !hasContent) {
            decorations.push(_prosemirrorView.Decoration.node(pos, pos + node.nodeSize, {
              class: _style.default["empty-node"],
              "data-content": content
            }));
          }
        };

        state.doc.descendants(decorate);
        return _prosemirrorView.DecorationSet.create(state.doc, decorations);
      }
    }
  });
};

var _default = PlaceholderPlugin;
exports.default = _default;