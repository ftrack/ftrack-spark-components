"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = buildKeymap;

var _prosemirrorCommands = require("prosemirror-commands");

var _prosemirrorSchemaList = require("prosemirror-schema-list");

var _prosemirrorHistory = require("prosemirror-history");

var _prosemirrorInputrules = require("prosemirror-inputrules");

// :copyright: Copyright (c) 2021 ftrack
var mac = // eslint-disable-next-line no-undef
typeof navigator !== "undefined" ? /Mac/.test(navigator.platform) : false; // Key Bindings
// * **Mod-b** for toggling [strong](#schema-basic.StrongMark)
// * **Mod-i** for toggling [emphasis](#schema-basic.EmMark)
// * **Ctrl-Shift-8** to wrap the selection in an ordered list
// * **Ctrl-Shift-9** to wrap the selection in a bullet list
// * **Ctrl->** to wrap the selection in a block quote
// * **Enter** to split a non-empty textblock in a list item while at
//   the same time splitting the list item
// * **Mod-_** to insert a horizontal rule
// * **Backspace** to undo an input rule
// * **Mod-BracketLeft** to `lift`
// * **Escape** to `selectParentNode`
// * **Mod-Shift-x** for toggling strikethough
// * **Tab** to sink list

function buildKeymap(schema) {
  var keys = {};

  function bind(key, cmd) {
    keys[key] = cmd;
  }

  bind("Mod-z", _prosemirrorHistory.undo);
  bind("Shift-Mod-z", _prosemirrorHistory.redo);
  bind("Backspace", _prosemirrorInputrules.undoInputRule);
  if (!mac) bind("Mod-y", _prosemirrorHistory.redo);
  bind("Mod-BracketLeft", _prosemirrorCommands.lift);
  bind("Escape", _prosemirrorCommands.selectParentNode);
  bind("Mod-b", (0, _prosemirrorCommands.toggleMark)(schema.marks.strong));
  bind("Mod-B", (0, _prosemirrorCommands.toggleMark)(schema.marks.strong));
  bind("Mod-i", (0, _prosemirrorCommands.toggleMark)(schema.marks.em));
  bind("Mod-I", (0, _prosemirrorCommands.toggleMark)(schema.marks.em));
  bind("Mod-Shift-x", (0, _prosemirrorCommands.toggleMark)(schema.marks.strikethrough));
  bind("Mod-Shift-X", (0, _prosemirrorCommands.toggleMark)(schema.marks.strikethrough));
  bind("Shift-Ctrl-8", (0, _prosemirrorSchemaList.wrapInList)(schema.nodes.bullet_list));
  bind("Shift-Ctrl-9", (0, _prosemirrorSchemaList.wrapInList)(schema.nodes.ordered_list));
  bind("Enter", (0, _prosemirrorSchemaList.splitListItem)(schema.nodes.list_item));
  bind("Mod-[", (0, _prosemirrorSchemaList.liftListItem)(schema.nodes.list_item));
  bind("Mod-]", (0, _prosemirrorSchemaList.sinkListItem)(schema.nodes.list_item));
  bind("Tab", (0, _prosemirrorSchemaList.sinkListItem)(schema.nodes.list_item));
  var hr = schema.nodes.horizontal_rule;
  bind("Mod-_", function (state, dispatch) {
    dispatch(state.tr.replaceSelectionWith(hr.create()).scrollIntoView());
    return true;
  });
  return keys;
}