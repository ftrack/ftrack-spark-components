"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _prosemirrorInputrules = require("prosemirror-inputrules");

var _utils = require("./utils");

// :copyright: Copyright (c) 2021 ftrack
var blockQuoteRule = function blockQuoteRule(nodeType) {
  return (0, _prosemirrorInputrules.wrappingInputRule)(/^\s*>\s$/, nodeType);
};

var orderedListRule = function orderedListRule(nodeType) {
  return (0, _prosemirrorInputrules.wrappingInputRule)(/^(\d+)\.\s$/, nodeType, function (match) {
    return {
      order: +match[1]
    };
  }, function (match, node) {
    return node.childCount + node.attrs.order === +match[1];
  });
};

var bulletListRule = function bulletListRule(nodeType) {
  return (0, _prosemirrorInputrules.wrappingInputRule)(/^\s*([-+*])\s$/, nodeType);
};

var codeBlockRule = function codeBlockRule(nodeType) {
  return (0, _prosemirrorInputrules.textblockTypeInputRule)(/^```$/, nodeType);
};

var autolinkRule = (0, _utils.createLinkInputRule)(new _utils.LinkMatcher());

var buildInputRules = function buildInputRules(schema) {
  var rules = _prosemirrorInputrules.smartQuotes.concat(_prosemirrorInputrules.ellipsis, _prosemirrorInputrules.emDash);

  rules.push(blockQuoteRule(schema.nodes.blockquote));
  rules.push(orderedListRule(schema.nodes.ordered_list));
  rules.push(bulletListRule(schema.nodes.bullet_list));
  rules.push(codeBlockRule(schema.nodes.code_block));
  rules.push(autolinkRule);
  return (0, _prosemirrorInputrules.inputRules)({
    rules: rules
  });
};

var _default = buildInputRules;
exports.default = _default;