"use strict";

require("core-js/modules/es6.promise.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = suggestionsPlugin;

require("regenerator-runtime/runtime.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.regexp.constructor.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.regexp.to-string.js");

var _prosemirrorState = require("prosemirror-state");

var _prosemirrorView = require("prosemirror-view");

var _prosemirrorModel = require("prosemirror-model");

var _utils = require("./utils");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/**
 * Adapted from
 * https://github.com/ueberdosis/tiptap/blob/main/packages/tiptap-extensions/src/plugins/Suggestions.js
 */
var getTextBetween = function getTextBetween(node, from, to, blockSeparator, inlineSeparator) {
  var leafText = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : "\0";
  var text = "";
  var blockSeparated = true;
  var inlineNode = null;
  node.content.nodesBetween(from, to, function (innerNode, pos) {
    if (innerNode.isText) {
      if (inlineNode) {
        inlineNode = null;
        return;
      }

      text += innerNode.text.slice(Math.max(from, pos) - pos, to - pos);
      blockSeparated = !blockSeparator;
    } else if (innerNode.isLeaf && leafText) {
      text += leafText;
      blockSeparated = !blockSeparator;
    } else if (innerNode.isInline && !innerNode.isLeaf) {
      text += inlineSeparator;

      if (innerNode.textContent) {
        text += innerNode.textContent;
        inlineNode = innerNode;
      }

      text += inlineSeparator;
      blockSeparated = !blockSeparated;
    } else if (!blockSeparated && innerNode.isBlock) {
      text += blockSeparator;
      blockSeparated = true;
    }
  }, 0);
  return text;
};

function triggerCharacter(_ref) {
  var _ref$char = _ref.char,
      char = _ref$char === void 0 ? "@" : _ref$char,
      _ref$regex = _ref.regex,
      regex = _ref$regex === void 0 ? _utils.MENTION_SUGGESTION_REGEX : _ref$regex;
  return function ($position) {
    // cancel if top level node
    if ($position.depth <= 0) {
      return false;
    } // Matching expressions used for later


    var escapedChar = "\\".concat(char);
    var suffix = new RegExp("\\s".concat(escapedChar, "$")); // Lookup the boundaries of the current node

    var textFrom = $position.before();
    var textTo = $position.end();
    var text = getTextBetween($position.doc, textFrom, textTo, "\0", "\0");
    var match = regex.exec(text);
    var position;

    while (match !== null) {
      // JavaScript doesn't have lookbehinds; this hacks a check that first character is " "
      // or the line beginning
      var matchPrefix = match.input.slice(Math.max(0, match.index - 1), match.index);

      if (/^[\s\0]?$/.test(matchPrefix)) {
        // The absolute position of the match in the document
        var from = match.index + $position.start();
        var to = from + match[0].length; // Edge case handling; if spaces are allowed and we're directly in between
        // two triggers

        if (suffix.test(text.slice(to - 1, to + 1))) {
          match[0] += " ";
          to += 1;
        } // If the $position is located within the matched substring, return that range


        if (from < $position.pos && to >= $position.pos) {
          position = {
            range: {
              from: from,
              to: to
            },
            query: match[0].slice(char.length),
            text: match[0]
          };
        }
      }

      match = regex.exec(text);
    }

    return position;
  };
}

function suggestionsPlugin(_ref2) {
  var _ref2$name = _ref2.name,
      name = _ref2$name === void 0 ? "mention" : _ref2$name,
      _ref2$matcher = _ref2.matcher,
      matcher = _ref2$matcher === void 0 ? {
    char: "@",
    regex: _utils.MENTION_SUGGESTION_REGEX
  } : _ref2$matcher,
      _ref2$suggestionClass = _ref2.suggestionClass,
      suggestionClass = _ref2$suggestionClass === void 0 ? "mention" : _ref2$suggestionClass,
      _ref2$onEnter = _ref2.onEnter,
      onEnter = _ref2$onEnter === void 0 ? function () {
    return false;
  } : _ref2$onEnter,
      _ref2$onChange = _ref2.onChange,
      onChange = _ref2$onChange === void 0 ? function () {
    return false;
  } : _ref2$onChange,
      _ref2$onExit = _ref2.onExit,
      onExit = _ref2$onExit === void 0 ? function () {
    return false;
  } : _ref2$onExit,
      _ref2$onKeyDown = _ref2.onKeyDown,
      onKeyDown = _ref2$onKeyDown === void 0 ? function () {
    return false;
  } : _ref2$onKeyDown,
      _ref2$onFilter = _ref2.onFilter,
      onFilter = _ref2$onFilter === void 0 ? function () {} : _ref2$onFilter;
  return new _prosemirrorState.Plugin({
    key: new _prosemirrorState.PluginKey("suggestions"),
    view: function view() {
      var _this = this;

      return {
        update: function () {
          var _update = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(view, prevState) {
            var prev, next, moved, started, stopped, changed, handleStart, handleChange, handleExit, state, decorationNode, items, props;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    prev = _this.key.getState(prevState);
                    next = _this.key.getState(view.state); // See how the state changed

                    moved = prev.active && next.active && prev.range.from !== next.range.from;
                    started = !prev.active && next.active;
                    stopped = prev.active && !next.active;
                    changed = !started && !stopped && prev.query !== next.query;
                    handleStart = started || moved;
                    handleChange = changed && !moved;
                    handleExit = stopped || moved; // Cancel when suggestion isn't active

                    if (!(!handleStart && !handleChange && !handleExit)) {
                      _context.next = 11;
                      break;
                    }

                    return _context.abrupt("return");

                  case 11:
                    state = handleExit ? prev : next;
                    decorationNode = document.querySelector("[data-decoration-id=\"".concat(state.decorationId, "\"]"));

                    if (!(handleChange || handleStart)) {
                      _context.next = 19;
                      break;
                    }

                    _context.next = 16;
                    return onFilter(state.query);

                  case 16:
                    _context.t0 = _context.sent;
                    _context.next = 20;
                    break;

                  case 19:
                    _context.t0 = [];

                  case 20:
                    items = _context.t0;
                    props = {
                      view: view,
                      range: state.range,
                      query: state.query,
                      text: state.text,
                      decorationNode: decorationNode,
                      items: items,
                      command: function command(_ref3) {
                        var range = _ref3.range,
                            attrs = _ref3.attrs;
                        var nodeType = view.state.schema.nodes[name];

                        var nodeFragment = _prosemirrorModel.Fragment.fromJSON(view.state.schema, [{
                          type: "text",
                          text: "".concat(matcher.char).concat(attrs.label)
                        }]);

                        (0, _utils.replaceText)(range, nodeType, attrs, nodeFragment)(view.state, view.dispatch);
                        view.focus();
                      }
                    }; // Trigger the hooks when necessary

                    if (handleExit) {
                      onExit(props);
                    }

                    if (handleChange) {
                      onChange(props);
                    }

                    if (handleStart) {
                      onEnter(props);
                    }

                  case 25:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee);
          }));

          function update(_x, _x2) {
            return _update.apply(this, arguments);
          }

          return update;
        }()
      };
    },
    state: {
      // Initialize the plugin's internal state.
      init: function init() {
        return {
          active: false,
          range: {},
          query: null,
          text: null
        };
      },
      // Apply changes to the plugin state from a view transaction.
      apply: function apply(tr, prev) {
        var selection = tr.selection;

        var next = _objectSpread({}, prev); // We can only be suggesting if there is no selection


        if (selection.from === selection.to) {
          // Reset active state if we just left the previous suggestion range
          if (selection.from < prev.range.from || selection.from > prev.range.to) {
            next.active = false;
          } // Try to match against where our cursor currently is


          var $position = selection.$from;
          var match = triggerCharacter(matcher)($position);
          var decorationId = (Math.random() + 1).toString(36).substr(2, 5); // If we found a match, update the current state to show it

          if (match) {
            next.active = true;
            next.decorationId = prev.decorationId ? prev.decorationId : decorationId;
            next.range = match.range;
            next.query = match.query;
            next.text = match.text;
          } else {
            next.active = false;
          }
        } else {
          next.active = false;
        } // Make sure to empty the range if suggestion is inactive


        if (!next.active) {
          next.decorationId = null;
          next.range = {};
          next.query = null;
          next.text = null;
        }

        return next;
      }
    },
    props: {
      // Call the keydown hook if suggestion is active.
      handleKeyDown: function handleKeyDown(view, event) {
        var _this$getState = this.getState(view.state),
            active = _this$getState.active,
            range = _this$getState.range;

        if (!active) return false;
        return onKeyDown({
          view: view,
          event: event,
          range: range
        });
      },
      // Setup decorator on the currently active suggestion.
      decorations: function decorations(editorState) {
        var _this$getState2 = this.getState(editorState),
            active = _this$getState2.active,
            range = _this$getState2.range,
            decorationId = _this$getState2.decorationId;

        if (!active) return null;
        return _prosemirrorView.DecorationSet.create(editorState.doc, [_prosemirrorView.Decoration.inline(range.from, range.to, {
          nodeName: "span",
          class: suggestionClass,
          "data-decoration-id": decorationId
        })]);
      }
    }
  });
}