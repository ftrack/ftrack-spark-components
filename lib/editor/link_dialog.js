"use strict";

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.regexp.replace.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _dialog = _interopRequireDefault(require("react-toolbox/lib/dialog"));

var _input = _interopRequireDefault(require("react-toolbox/lib/input"));

var _reactIntl = require("react-intl");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var messages = (0, _reactIntl.defineMessages)({
  "link-uri-placeholder": {
    "id": "ftrack-spark-components.editor.link-uri-placeholder",
    "defaultMessage": "Link"
  },
  "link-title-placeholder": {
    "id": "ftrack-spark-components.editor.link-title-placeholder",
    "defaultMessage": "Title"
  },
  "cancel-button-title": {
    "id": "ftrack-spark-components.editor.cancel-button-title",
    "defaultMessage": "Cancel"
  },
  "insert-link-button-title": {
    "id": "ftrack-spark-components.editor.insert-link-button-title",
    "defaultMessage": "Insert link"
  }
});

var LinkDialog = function LinkDialog(_ref) {
  var selection = _ref.selection,
      onInsertLink = _ref.onInsertLink,
      onCancel = _ref.onCancel;

  var _useState = (0, _react.useState)({
    title: selection,
    href: ""
  }),
      _useState2 = _slicedToArray(_useState, 2),
      link = _useState2[0],
      setLink = _useState2[1];

  var linkInputRef = (0, _react.useRef)();
  (0, _react.useEffect)(function () {
    if (linkInputRef.current && !!selection) {
      linkInputRef.current.focus();
    }
  }, // TODO: Feel free to fix this eslint error if you got time
  // eslint-disable-next-line react-hooks/exhaustive-deps
  []);

  var insertLink = function insertLink() {
    var url = link.href.trim();
    onInsertLink({
      title: link.title,
      href: url.replace(/^(?!(?:\w+:)?\/\/)/, "http://")
    });
  };

  var actions = [{
    label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["cancel-button-title"])),
    onClick: onCancel
  }, {
    label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["insert-link-button-title"])),
    onClick: insertLink,
    disabled: link.href === ""
  }];

  var handleChange = function handleChange(name, value) {
    setLink(_objectSpread(_objectSpread({}, link), {}, _defineProperty({}, name, value)));
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_dialog.default, {
    className: "ignore-react-onclickoutside",
    active: true,
    type: "small",
    actions: actions,
    onEscKeyDown: onCancel,
    onOverlayClick: onCancel,
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_input.default, {
      label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["link-title-placeholder"])),
      value: link.title,
      onChange: function onChange(value) {
        return handleChange("title", value);
      }
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_input.default, {
      innerRef: linkInputRef,
      required: true,
      type: "url",
      label: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["link-uri-placeholder"])),
      value: link.href,
      onChange: function onChange(value) {
        return handleChange("href", value);
      }
    })]
  });
};

LinkDialog.propTypes = {
  selection: _propTypes.default.string,
  onInsertLink: _propTypes.default.func,
  onCancel: _propTypes.default.func
};
LinkDialog.defaultProps = {};

var _default = (0, _safe_inject_intl.default)(LinkDialog);

exports.default = _default;