"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.markdownSerializer = exports.markdownParser = exports.default = void 0;

require("core-js/modules/es6.regexp.split.js");

var _prosemirrorMarkdown = require("prosemirror-markdown");

var _prosemirrorModel = require("prosemirror-model");

var _markdown = require("../markdown");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var newMarks = _prosemirrorMarkdown.schema.spec.marks.addToEnd("strikethrough", {
  parseDOM: [{
    tag: "s"
  }],
  toDOM: function toDOM() {
    return ["s", 0];
  }
});

var newNodes = _prosemirrorMarkdown.schema.spec.nodes.addToEnd("mention", {
  attrs: {
    id: {},
    label: {}
  },
  group: "inline",
  inline: true,
  content: "text*",
  selectable: false,
  atom: true,
  toDOM: function toDOM(node) {
    return ["span", {
      class: "mention",
      "data-mention-id": node.attrs.id
    }, "@".concat(node.attrs.label)];
  },
  parseDOM: [{
    tag: "span[data-mention-id]",
    getAttrs: function getAttrs(dom) {
      var id = dom.getAttribute("data-mention-id");
      var label = dom.innerText.split("@").join("");
      return {
        id: id,
        label: label
      };
    },
    getContent: function getContent(dom, s) {
      var label = dom.innerText.split("@").join("");
      return _prosemirrorModel.Fragment.fromJSON(s, [{
        type: "text",
        text: "@".concat(label)
      }]);
    }
  }]
});

var customSchema = new _prosemirrorModel.Schema(_objectSpread(_objectSpread({}, _prosemirrorMarkdown.schema.spec), {}, {
  marks: newMarks,
  nodes: newNodes
}));
exports.default = customSchema;
var markdownParser = new _prosemirrorMarkdown.MarkdownParser(customSchema, _markdown.markdown, _objectSpread(_objectSpread({}, _prosemirrorMarkdown.defaultMarkdownParser.tokens), {}, {
  s: {
    mark: "strikethrough"
  },
  mention: {
    node: "mention",
    getAttrs: function getAttrs(tok) {
      return {
        id: tok.attrGet("id"),
        label: tok.attrGet("label")
      };
    }
  }
}));
exports.markdownParser = markdownParser;
var markdownSerializer = new _prosemirrorMarkdown.MarkdownSerializer(_objectSpread(_objectSpread({}, _prosemirrorMarkdown.defaultMarkdownSerializer.nodes), {}, {
  mention: function mention(state, node) {
    state.write("@{".concat(node.attrs.label, ":").concat(node.attrs.id, "}"));
  }
}), _objectSpread(_objectSpread({}, _prosemirrorMarkdown.defaultMarkdownSerializer.marks), {}, {
  strikethrough: {
    open: "~~",
    close: "~~",
    mixable: true,
    expelEnclosingWhitespace: true
  }
}));
exports.markdownSerializer = markdownSerializer;