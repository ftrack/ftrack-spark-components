"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _loglevel = _interopRequireDefault(require("loglevel"));

var _component = _interopRequireDefault(require("./component"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

/** Project color picker */
var ProjectColorPicker = /*#__PURE__*/function (_Component) {
  _inherits(ProjectColorPicker, _Component);

  var _super = _createSuper(ProjectColorPicker);

  function ProjectColorPicker(props) {
    var _this;

    _classCallCheck(this, ProjectColorPicker);

    _this = _super.call(this, props);
    _this.state = {
      value: null
    };
    _this.handleChange = _this.handleChange.bind(_assertThisInitialized(_this));
    _this.handleProjectIdChanged = _this.handleProjectIdChanged.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(ProjectColorPicker, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.handleProjectIdChanged(this.props.projectId);
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.projectId !== this.props.projectId) {
        this.handleProjectIdChanged(nextProps.projectId);
      }
    }
  }, {
    key: "handleProjectIdChanged",
    value: function handleProjectIdChanged(projectId) {
      var _this2 = this;

      this.setState({
        value: null
      });

      if (!projectId) {
        return;
      }

      this.props.session.query("select color from Project where id is \"".concat(projectId, "\"")).then(function (response) {
        var color = response.data[0].color;

        _this2.setState({
          value: color
        });
      });
    }
  }, {
    key: "handleChange",
    value: function handleChange(value) {
      var _this3 = this;

      var projectId = this.props.projectId;

      _loglevel.default.debug("Changing project color", projectId, value);

      if (!projectId) {
        return;
      }

      var oldValue = this.state.value;
      this.setState({
        value: value
      });
      this.props.session.update("Project", [projectId], {
        color: value
      }).then(function (response) {
        if (_this3.props.onSuccess) {
          _this3.props.onSuccess({
            response: response,
            projectId: projectId,
            value: value
          });
        }
      }).catch(function (error) {
        _this3.setState({
          value: oldValue
        });

        if (_this3.props.onError) {
          _this3.props.onError(error);
        }
      });
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_component.default, {
        className: this.props.className,
        value: this.state.value,
        onChange: this.handleChange
      });
    }
  }]);

  return ProjectColorPicker;
}(_react.Component);

ProjectColorPicker.propTypes = {
  session: _propTypes.default.shape({
    query: _propTypes.default.func.isRequired,
    update: _propTypes.default.func.isRequired
  }).isRequired,
  projectId: _propTypes.default.string,
  className: _propTypes.default.string,
  onSuccess: _propTypes.default.func,
  onError: _propTypes.default.func
};
ProjectColorPicker.defaultProps = {};
var _default = ProjectColorPicker;
exports.default = _default;