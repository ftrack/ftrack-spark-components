"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _button = _interopRequireDefault(require("react-toolbox/lib/button"));

var _reactColor = require("react-color");

var _reactOnclickoutside = _interopRequireDefault(require("react-onclickoutside"));

var _classnames2 = _interopRequireDefault(require("classnames"));

var _font_icon = _interopRequireDefault(require("react-toolbox/lib/font_icon"));

var _constant = require("../util/constant");

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

/** Color field */
var ColorPicker = /*#__PURE__*/function (_Component) {
  _inherits(ColorPicker, _Component);

  var _super = _createSuper(ColorPicker);

  function ColorPicker(props) {
    var _this;

    _classCallCheck(this, ColorPicker);

    _this = _super.call(this, props);
    _this.state = {
      pickerActive: false
    };
    _this.handleButtonClick = _this.handleButtonClick.bind(_assertThisInitialized(_this));
    _this.handleChangeComplete = _this.handleChangeComplete.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(ColorPicker, [{
    key: "handleButtonClick",
    value: function handleButtonClick() {
      this.setState({
        pickerActive: !this.state.pickerActive
      });
    }
  }, {
    key: "handleChangeComplete",
    value: function handleChangeComplete(color) {
      this.setState({
        pickerActive: false
      });
      this.props.onChange(color.hex.toUpperCase());
    }
  }, {
    key: "handleClickOutside",
    value: function handleClickOutside() {
      this.setState({
        pickerActive: false
      });
    }
  }, {
    key: "render",
    value: function render() {
      var pickerTriangle = this.props.position === "right" ? "top-right" : "top-left";
      var className = (0, _classnames2.default)(_style.default.root, _style.default[this.props.position], this.props.className, _defineProperty({}, _style.default.withBorder, this.props.border));
      return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: className,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_button.default, {
          className: _style.default.colorButton,
          style: {
            backgroundColor: this.props.value
          },
          type: "button",
          floating: true,
          mini: true,
          theme: this.props.theme,
          onClick: this.handleButtonClick,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            className: _style.default["edit-overlay"],
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.default, {
              value: "edit",
              className: _style.default["edit-icon"]
            })
          })
        }), this.state.pickerActive ? /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default.picker,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactColor.TwitterPicker, {
            triangle: pickerTriangle,
            color: this.props.value,
            width: "240px",
            colors: _constant.PROJECT_COLORS,
            onChangeComplete: this.handleChangeComplete
          })
        }) : null]
      });
    }
  }]);

  return ColorPicker;
}(_react.Component);

ColorPicker.propTypes = {
  className: _propTypes.default.string,
  value: _propTypes.default.string,
  onChange: _propTypes.default.func.isRequired,
  position: _propTypes.default.oneOf(["left", "right"]),
  border: _propTypes.default.bool,
  theme: _propTypes.default.object
};
ColorPicker.defaultProps = {
  border: false,
  className: "",
  position: "left",
  value: "#888888"
};

var _default = (0, _reactOnclickoutside.default)(ColorPicker);

exports.default = _default;