"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _lazy_image = _interopRequireDefault(require("./lazy_image"));

var _file_type_icon = _interopRequireDefault(require("../file_type_icon"));

var _thumbnail = _interopRequireDefault(require("./thumbnail.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["src", "size", "position", "rounded", "className", "children"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function Thumbnail(_ref) {
  var _classNames;

  var src = _ref.src,
      _ref$size = _ref.size,
      size = _ref$size === void 0 ? "small" : _ref$size,
      _ref$position = _ref.position,
      position = _ref$position === void 0 ? "relative" : _ref$position,
      rounded = _ref.rounded,
      className = _ref.className,
      children = _ref.children,
      props = _objectWithoutProperties(_ref, _excluded);

  var classes = (0, _classnames.default)(_thumbnail.default.root, (_classNames = {}, _defineProperty(_classNames, _thumbnail.default.positionRelative, position === "relative"), _defineProperty(_classNames, _thumbnail.default.positionAbsolute, position === "absolute"), _defineProperty(_classNames, _thumbnail.default.rounded, rounded), _classNames), _thumbnail.default[size], className);
  var image = src ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_lazy_image.default, {
    src: src
  }) : /*#__PURE__*/(0, _jsxRuntime.jsx)(_file_type_icon.default, {
    variant: size
  });
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", _objectSpread(_objectSpread({
    className: classes
  }, props), {}, {
    children: [image, children]
  }));
}

Thumbnail.propTypes = {
  className: _propTypes.default.string,
  children: _propTypes.default.node,
  rounded: _propTypes.default.bool,
  src: _propTypes.default.string,
  size: _propTypes.default.oneOf(["tiny", "small", "extra-tiny"]),
  position: _propTypes.default.oneOf(["relative", "absolute"])
};
var _default = Thumbnail;
exports.default = _default;