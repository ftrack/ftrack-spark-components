"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SelectedAttributes = exports.AttributeItem = exports.AttributeList = exports.AttributeList_ = void 0;

require("core-js/modules/es6.object.assign.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.array.find.js");

require("core-js/modules/es6.string.ends-with.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _list = require("react-toolbox/lib/list");

var _button = require("react-toolbox/lib/button");

var _font_icon = require("react-toolbox/lib/font_icon");

var _classnames = _interopRequireDefault(require("classnames"));

var _recompose = require("recompose");

var _reactFlipMove = _interopRequireDefault(require("react-flip-move"));

var _reactIntl = require("react-intl");

var _constant = require("../util/constant");

var _attribute = require("../dataview/attribute");

var _util = require("../dataview/util");

var _list_item_theme = _interopRequireDefault(require("./list_item_theme.scss"));

var _list_item_group_theme = _interopRequireDefault(require("./list_item_group_theme.scss"));

var _list_theme = _interopRequireDefault(require("./list_theme.scss"));

var _style = _interopRequireDefault(require("./style.scss"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _hoc = require("../util/hoc");

var _entityUtils = require("../formatter/entityUtils");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var messages = (0, _reactIntl.defineMessages)({
  "unknown-attribute": {
    "id": "ftrack-spark-components.attribute-browser.unknown-attribute",
    "defaultMessage": "Unknown attribute"
  },
  attributes: {
    "id": "ftrack-spark-components.attribute-browser.attributes",
    "defaultMessage": "Attributes"
  },
  "custom-attributes": {
    "id": "ftrack-spark-components.attribute-browser.custom-attributes",
    "defaultMessage": "Custom attributes"
  },
  related: {
    "id": "ftrack-spark-components.attribute-browser.related",
    "defaultMessage": "Related"
  }
});
var ListItemWithHandlers = (0, _recompose.withHandlers)({
  onClick: function onClick(props) {
    return function () {
      return props.onClick(props.itemKey);
    };
  }
})(_list.ListItem);
var ListCheckboxWithHandlers = (0, _recompose.withHandlers)({
  onChange: function onChange(props) {
    return function (value) {
      return props.onChange(props.itemKey, value);
    };
  }
})(_list.ListCheckbox);

var AttributeList_ = /*#__PURE__*/function (_Component) {
  _inherits(AttributeList_, _Component);

  var _super = _createSuper(AttributeList_);

  function AttributeList_() {
    var _this;

    _classCallCheck(this, AttributeList_);

    _this = _super.call(this);
    _this.onExpandClick = _this.onExpandClick.bind(_assertThisInitialized(_this));
    _this.onChange = _this.onChange.bind(_assertThisInitialized(_this));
    _this.onClick = _this.onClick.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(AttributeList_, [{
    key: "onExpandClick",
    value: function onExpandClick(key) {
      var _this$props = this.props,
          setExpanded = _this$props.setExpanded,
          expanded = _this$props.expanded;
      setExpanded(Object.assign({}, expanded, _defineProperty({}, key, !expanded[key])));
    }
  }, {
    key: "onChange",
    value: function onChange(key, value) {
      var _this$props2 = this.props,
          onChange = _this$props2.onChange,
          selected = _this$props2.selected;

      if (value) {
        onChange([].concat(_toConsumableArray(selected), [key]));
      } else {
        onChange(selected.filter(function (item) {
          return item !== key;
        }));
      }
    }
  }, {
    key: "onClick",
    value: function onClick(key) {
      var onChange = this.props.onChange;
      onChange(key);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props3 = this.props,
          attributeGroups = _this$props3.attributeGroups,
          attributeTransform = _this$props3.attributeTransform,
          className = _this$props3.className,
          disabled = _this$props3.disabled,
          enableSelectionModel = _this$props3.enableSelectionModel,
          expanded = _this$props3.expanded,
          intl = _this$props3.intl,
          selected = _this$props3.selected,
          session = _this$props3.session;
      var sortedGroups = (0, _attribute.sortLocalisedGroups)(attributeGroups, session, intl);
      var items = sortedGroups.reduce(function (accumulator, group, index) {
        var isExpanded = index === 0 || expanded[group.key];
        var isInExpandedGroup = isExpanded && index !== 0;
        var listItemClassName = (0, _classnames.default)(_defineProperty({}, _style.default["list-item-indent"], isInExpandedGroup));

        if (index === 1) {
          accumulator.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_list.ListItem, {
            disabled: true,
            theme: _list_item_group_theme.default,
            caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages.related))
          }, "Related"));
        }

        if (index > 0) {
          var caption = /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            className: _style.default["attribute-group-label"],
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_attribute.AttributeGroupLabel, {
              path: group.path,
              label: group.groupLabel
            })
          }); // Do not label first group.

          accumulator.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(ListItemWithHandlers, {
            theme: _list_item_group_theme.default,
            className: _style.default["list-group-item"],
            leftIcon: isExpanded ? "keyboard_arrow_down" : "keyboard_arrow_right",
            itemKey: group.key,
            caption: caption,
            onClick: _this2.onExpandClick
          }, group.key));
        }

        var schema = session.schemas.find(function (candidate) {
          return candidate.id === group.model;
        });
        var attributes = (0, _attribute.sortLocalisedAttributes)(group.attributes, schema, intl);

        if (isExpanded) {
          accumulator.push.apply(accumulator, _toConsumableArray(attributes.reduce(function (attributeAccumulator, item, attributeIndex) {
            var _Entity$item$model;

            var caption = /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
              children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.FontIcon, {
                value: item.icon,
                className: _style.default.icon
              }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_attribute.AttributeLabel, {
                attribute: item,
                transform: attributeTransform
              }), item.key.endsWith("$entities") && /*#__PURE__*/(0, _jsxRuntime.jsx)(_attribute.AttributeLabel, {
                attribute: {
                  key: item.key,
                  label: item.groupLabel || ((_Entity$item$model = _entityUtils.Entity[item.model]) === null || _Entity$item$model === void 0 ? void 0 : _Entity$item$model.name)
                },
                transform: "uppercase",
                className: _style.default["attribute-small-margin"]
              })]
            });

            if (attributeIndex > 0 && attributes[attributeIndex - 1].key.endsWith("$entities")) {
              attributeAccumulator.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_list.ListItem, {
                disabled: true,
                theme: _list_item_group_theme.default,
                className: listItemClassName,
                caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["attributes"]))
              }, "".concat(item.key, "-attributes-").concat(attributeIndex)));
            }

            if (attributeIndex > 0 && (0, _util.isCustomAttribute)(item) && !(0, _util.isCustomAttribute)(attributes[attributeIndex - 1])) {
              // Custom attributes are at the end. Add a heading.
              attributeAccumulator.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(_list.ListItem, {
                disabled: true,
                theme: _list_item_group_theme.default,
                className: listItemClassName,
                caption: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["custom-attributes"]))
              }, "".concat(item.key, "-custom-attributes-").concat(attributeIndex)));
            }

            if (enableSelectionModel) {
              attributeAccumulator.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(ListCheckboxWithHandlers, {
                theme: _list_item_theme.default,
                className: listItemClassName,
                checked: selected.indexOf(item.key) !== -1,
                disabled: disabled.indexOf(item.key) !== -1,
                caption: caption,
                itemKey: item.key,
                onChange: _this2.onChange
              }, item.key));
            } else {
              attributeAccumulator.push( /*#__PURE__*/(0, _jsxRuntime.jsx)(ListItemWithHandlers, {
                theme: _list_item_theme.default,
                className: listItemClassName,
                disabled: disabled.indexOf(item.key) !== -1,
                caption: caption,
                legend: item.description,
                itemKey: item.key,
                onClick: _this2.onClick
              }, item.key));
            }

            return attributeAccumulator;
          }, [])));
        }

        return accumulator;
      }, []);
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_list.List, {
        selectable: true,
        className: className,
        theme: _list_theme.default,
        children: items
      });
    }
  }]);

  return AttributeList_;
}(_react.Component);

exports.AttributeList_ = AttributeList_;
AttributeList_.propTypes = {
  enableSelectionModel: _propTypes.default.bool,
  attributeGroups: _propTypes.default.arrayOf(_util.attributeGroupShape).isRequired,
  selected: _propTypes.default.arrayOf(_propTypes.default.string).isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  expanded: _propTypes.default.object.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  session: _propTypes.default.object.isRequired,
  setExpanded: _propTypes.default.func.isRequired,
  intl: _reactIntl.intlShape.isRequired,
  onChange: _propTypes.default.func.isRequired,
  className: _propTypes.default.string,
  disabled: _propTypes.default.arrayOf(_propTypes.default.string),
  attributeTransform: _propTypes.default.oneOf(["capitalize", "uppercase"])
};
AttributeList_.defaultProps = {
  enableSelectionModel: false,
  selected: [],
  disabled: []
};
var AttributeList = (0, _recompose.compose)((0, _recompose.withState)("expanded", "setExpanded", {}), _safe_inject_intl.default, _hoc.withSession)(AttributeList_);
exports.AttributeList = AttributeList;
var AttributeIconButton = (0, _recompose.withHandlers)({
  onClick: function onClick(props) {
    return function () {
      return props.onClick(props.attribute);
    };
  }
})(_button.IconButton); // Written as class to be compatible with react-flip-move
// eslint-disable-next-line react/prefer-stateless-function

var AttributeItem = /*#__PURE__*/function (_Component2) {
  _inherits(AttributeItem, _Component2);

  var _super2 = _createSuper(AttributeItem);

  function AttributeItem() {
    _classCallCheck(this, AttributeItem);

    return _super2.apply(this, arguments);
  }

  _createClass(AttributeItem, [{
    key: "render",
    value: function render() {
      var _this$props4 = this.props,
          hint = _this$props4.hint,
          attribute = _this$props4.attribute,
          onMoveUpClick = _this$props4.onMoveUpClick,
          onMoveDownClick = _this$props4.onMoveDownClick,
          onRemoveClick = _this$props4.onRemoveClick,
          disableMoveUp = _this$props4.disableMoveUp,
          disableMoveDown = _this$props4.disableMoveDown,
          attributeKey = _this$props4.attributeKey;
      var labelElement = attribute ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_attribute.AttributeLabel, {
        attribute: attribute,
        enableGroup: true
      }) : /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["unknown-attribute"]));
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_list.ListItem, {
        theme: _list_item_theme.default,
        className: _style.default["attribute-item"],
        caption: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
          className: _style.default["attribute-item-caption"],
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            className: _style.default["attribute-item-label"],
            children: labelElement
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
            className: _style.default["attribute-item-hint"],
            children: hint
          })]
        }),
        rightActions: [/*#__PURE__*/(0, _jsxRuntime.jsx)(AttributeIconButton, {
          icon: "keyboard_arrow_up",
          onClick: onMoveUpClick,
          attribute: attributeKey,
          disabled: disableMoveUp
        }, "up"), /*#__PURE__*/(0, _jsxRuntime.jsx)(AttributeIconButton, {
          down: "down",
          icon: "keyboard_arrow_down",
          onClick: onMoveDownClick,
          attribute: attributeKey,
          disabled: disableMoveDown
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(AttributeIconButton, {
          down: "remove",
          icon: "clear",
          onClick: onRemoveClick,
          attribute: attributeKey
        })]
      });
    }
  }]);

  return AttributeItem;
}(_react.Component);

exports.AttributeItem = AttributeItem;
AttributeItem.propTypes = {
  hint: _propTypes.default.string,
  onMoveUpClick: _propTypes.default.func.isRequired,
  onMoveDownClick: _propTypes.default.func.isRequired,
  onRemoveClick: _propTypes.default.func.isRequired,
  attribute: _util.attributeShape.isRequired,
  disableMoveUp: _propTypes.default.bool.isRequired,
  disableMoveDown: _propTypes.default.bool.isRequired,
  attributeKey: _propTypes.default.string.isRequired
};

var SelectedAttributes_ = /*#__PURE__*/function (_Component3) {
  _inherits(SelectedAttributes_, _Component3);

  var _super3 = _createSuper(SelectedAttributes_);

  function SelectedAttributes_() {
    var _this3;

    _classCallCheck(this, SelectedAttributes_);

    _this3 = _super3.call(this);
    _this3.onRemove = _this3.onRemove.bind(_assertThisInitialized(_this3));
    _this3.onMoveUp = _this3.onMoveUp.bind(_assertThisInitialized(_this3));
    _this3.onMoveDown = _this3.onMoveDown.bind(_assertThisInitialized(_this3));
    return _this3;
  }

  _createClass(SelectedAttributes_, [{
    key: "onRemove",
    value: function onRemove(attributeKey) {
      var selected = this.props.selected;
      this.props.onChange(_toConsumableArray(selected).filter(function (item) {
        return item !== attributeKey;
      }));
    }
  }, {
    key: "onMoveUp",
    value: function onMoveUp(attributeKey) {
      var selected = this.props.selected;
      var index = selected.indexOf(attributeKey);
      var nextSelected = selected.reduce(function (accumulator, item, itemIndex) {
        if (itemIndex === index) {
          return accumulator;
        }

        if (itemIndex === index - 1) {
          accumulator.push(attributeKey);
        }

        accumulator.push(item);
        return accumulator;
      }, []);
      this.props.onChange(nextSelected);
    }
  }, {
    key: "onMoveDown",
    value: function onMoveDown(attributeKey) {
      var selected = this.props.selected;
      var index = selected.indexOf(attributeKey);
      var nextSelected = selected.reduce(function (accumulator, item, itemIndex) {
        if (itemIndex === index) {
          return accumulator;
        }

        accumulator.push(item);

        if (itemIndex === index + 1) {
          accumulator.push(attributeKey);
        }

        return accumulator;
      }, []);
      this.props.onChange(nextSelected);
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      var _this$props5 = this.props,
          selected = _this$props5.selected,
          attributes = _this$props5.attributes,
          attributeHints = _this$props5.attributeHints;
      var items = selected.map(function (key, index) {
        var hint = attributeHints[index];
        var moveProps = {
          onMoveUpClick: _this4.onMoveUp,
          onMoveDownClick: _this4.onMoveDown,
          disableMoveUp: index === 0,
          disableMoveDown: index === selected.length - 1
        };
        var attribute = attributes[key];
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(AttributeItem, _objectSpread({
          hint: hint,
          attribute: attribute,
          attributeKey: key,
          onRemoveClick: _this4.onRemove
        }, moveProps), key);
      });
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_list.List, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactFlipMove.default, {
          duration: _constant.TRANSITION_DURATION,
          children: items
        })
      });
    }
  }]);

  return SelectedAttributes_;
}(_react.Component);

SelectedAttributes_.propTypes = {
  selected: _propTypes.default.arrayOf(_propTypes.default.string),
  onChange: _propTypes.default.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  attributes: _propTypes.default.object.isRequired,
  attributeHints: _propTypes.default.arrayOf(_propTypes.default.string)
};
SelectedAttributes_.defaultProps = {
  attributeHints: [],
  selected: []
};
var SelectedAttributes = (0, _safe_inject_intl.default)(SelectedAttributes_);
exports.SelectedAttributes = SelectedAttributes;