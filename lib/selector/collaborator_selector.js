"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.regexp.split.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.sort.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _function = require("lodash/function");

var _chip = _interopRequireDefault(require("react-toolbox/lib/chip"));

var _ftrackJavascriptApi = require("ftrack-javascript-api");

var _array = require("lodash/array");

var _reactIntl = require("react-intl");

var _classnames = _interopRequireDefault(require("classnames"));

var _input = _interopRequireDefault(require("react-toolbox/lib/input"));

var _reactToolbox = require("react-toolbox");

var _button = _interopRequireDefault(require("react-toolbox/lib/button"));

var _entity_avatar = _interopRequireDefault(require("../entity_avatar"));

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _string = require("../util/string");

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var messages = (0, _reactIntl.defineMessages)({
  "add-collaborator": {
    "id": "ftrack-spark-overview.collaborator-selector.add-collaborator",
    "defaultMessage": "Add collaborator"
  },
  "search-help-text": {
    "id": "ftrack-spark-overview.collaborator-selector.search-help-text",
    "defaultMessage": "Enter an email address to invite an external collaborator."
  },
  "invite-prompt": {
    "id": "ftrack-spark-overview.collaborator-selector.invite-prompt",
    "defaultMessage": "Invite {name} as a collaborator to this review?"
  },
  "invite-prompt-name": {
    "id": "ftrack-spark-overview.collaborator-selector.invite-prompt-name",
    "defaultMessage": "You can edit the suggested name below before saving."
  },
  "invite-empty-name": {
    "id": "ftrack-spark-overview.collaborator-selector.invite-empty-name",
    "defaultMessage": "Give {email} a name to be able to send an invite."
  },
  "add-button": {
    "id": "ftrack-spark-overview.collaborator-selector.add-button",
    "defaultMessage": "Add"
  },
  "known-collaborators": {
    "id": "ftrack-spark-components.collaborator-selector.known-collaborators",
    "defaultMessage": "Previously added collaborators or users in your workspace:"
  }
});
/** Return a LIKE query string from *keys* and *values*. */

function _getFilterString(keys, values) {
  var results = [];

  var _iterator = _createForOfIteratorHelper(values),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var value = _step.value;
      var keyResults = [];

      var _iterator2 = _createForOfIteratorHelper(keys),
          _step2;

      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var key = _step2.value;

          if (value) {
            keyResults.push("".concat(key, " like \"%").concat(value, "%\""));
          }
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }

      if (keyResults.length) {
        results.push("(".concat(keyResults.join(" or "), ")"));
      }
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  return results.join(" and ");
}
/** Render result list of collaborator search. */


function ResultList(_ref) {
  var items = _ref.items,
      onClick = _ref.onClick,
      className = _ref.className,
      session = _ref.session;

  if (items.length) {
    var result = items.map(function (item) {
      var handleClick = onClick.bind(null, item);
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactToolbox.ListItem, {
        leftActions: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_avatar.default, {
          entity: item,
          session: session,
          className: _style.default["avatar"]
        })],
        caption: item.name,
        legend: item.email,
        onClick: handleClick
      }, item.id);
    });
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactToolbox.List, {
      className: className,
      selectable: true,
      ripple: true,
      children: result
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {});
}

ResultList.propTypes = {
  className: _propTypes.default.string,
  items: _propTypes.default.shape([{
    id: _propTypes.default.string,
    name: _propTypes.default.string,
    email: _propTypes.default.string
  }]),
  onClick: _propTypes.default.func,
  session: _propTypes.default.shape({
    thumbnailUrl: _propTypes.default.func.isRequired
  })
};
/** Represent a collaborator chip that can be removed. */

var ExistingCollaboratorChip = /*#__PURE__*/function (_Component) {
  _inherits(ExistingCollaboratorChip, _Component);

  var _super = _createSuper(ExistingCollaboratorChip);

  function ExistingCollaboratorChip(props) {
    var _this;

    _classCallCheck(this, ExistingCollaboratorChip);

    _this = _super.call(this, props);
    _this.onDeleteClick = _this.onDeleteClick.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(ExistingCollaboratorChip, [{
    key: "onDeleteClick",
    value: function onDeleteClick() {
      this.props.onDeleteClick(this.props.item);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          item = _this$props.item,
          session = _this$props.session;
      return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_chip.default, {
        deletable: true,
        onDeleteClick: this.onDeleteClick,
        className: _style.default["selected-collaborator-item"],
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_entity_avatar.default, {
          entity: item,
          session: session,
          className: _style.default.avatar
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
          children: item.name
        })]
      });
    }
  }]);

  return ExistingCollaboratorChip;
}(_react.Component);

ExistingCollaboratorChip.propTypes = {
  onDeleteClick: _propTypes.default.func.isRequired,
  item: _propTypes.default.shape({
    name: _propTypes.default.string.isRequired,
    email: _propTypes.default.string.isRequired,
    id: _propTypes.default.string.isRequired,
    thumbnail_id: _propTypes.default.string
  }),
  session: _propTypes.default.shape({
    thumbnailUrl: _propTypes.default.func.isRequired
  })
};
var validateEmailRegexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
/** Quick review view */

var CollaboratorSelector = /*#__PURE__*/function (_Component2) {
  _inherits(CollaboratorSelector, _Component2);

  var _super2 = _createSuper(CollaboratorSelector);

  function CollaboratorSelector(props) {
    var _this2;

    _classCallCheck(this, CollaboratorSelector);

    _this2 = _super2.call(this, props);
    _this2.state = {
      availableCollaborators: [],
      inputActive: false,
      name: "",
      inputValue: ""
    }; // this._updateProject = this._updateProject.bind(this);

    _this2._loadCollaborators = (0, _function.debounce)(_this2._loadCollaborators.bind(_assertThisInitialized(_this2)), 500);
    _this2._onCollaboratorsChange = _this2._onCollaboratorsChange.bind(_assertThisInitialized(_this2));
    _this2.collaboratorExists = _this2.collaboratorExists.bind(_assertThisInitialized(_this2));
    _this2.addCollaborator = _this2.addCollaborator.bind(_assertThisInitialized(_this2));
    _this2.removeCollaborator = _this2.removeCollaborator.bind(_assertThisInitialized(_this2));
    _this2._onNameChange = _this2._onNameChange.bind(_assertThisInitialized(_this2));
    _this2._addNewCollaborator = _this2._addNewCollaborator.bind(_assertThisInitialized(_this2));
    _this2._onCollaboratorsKeyDown = _this2._onCollaboratorsKeyDown.bind(_assertThisInitialized(_this2));
    _this2._onInputFieldFocus = _this2._onInputFieldFocus.bind(_assertThisInitialized(_this2));
    _this2._onInputFieldBlur = _this2._onInputFieldBlur.bind(_assertThisInitialized(_this2));
    _this2.collaboratorExists = _this2.collaboratorExists.bind(_assertThisInitialized(_this2));
    return _this2;
  }
  /** Handle changes to the collaborators field. */


  _createClass(CollaboratorSelector, [{
    key: "_onCollaboratorsChange",
    value: function _onCollaboratorsChange(inputValue) {
      this._loadCollaborators(inputValue);

      var name = "";

      if (validateEmailRegexp.test(inputValue)) {
        name = (0, _string.guessName)(inputValue);
      }

      this.setState({
        name: name,
        inputValue: inputValue
      });
    }
    /** Handle input field focus. */

  }, {
    key: "_onInputFieldFocus",
    value: function _onInputFieldFocus() {
      this.setState({
        inputActive: true
      });
    }
    /** Handle input field blur. */

  }, {
    key: "_onInputFieldBlur",
    value: function _onInputFieldBlur() {
      this.setState({
        inputActive: false
      });
    }
    /** Load collaborators from server based on *value*. */

  }, {
    key: "_loadCollaborators",
    value: function _loadCollaborators(value) {
      var _this3 = this;

      // Clear state and return early if nothing to query.
      if (!value || !value.length) {
        this.setState({
          availableCollaborators: []
        });
        return;
      }

      var parts = value.split(" ");
      var userQuery = "select id, first_name, last_name, email,thumbnail_id from BaseUser where " + "".concat(_getFilterString(["first_name", "last_name", "email"], parts));
      var _this$props2 = this.props,
          session = _this$props2.session,
          alreadyIncludedCollaborators = _this$props2.alreadyIncludedCollaborators;

      var alreadyIncluded = function alreadyIncluded(email) {
        return alreadyIncludedCollaborators ? alreadyIncludedCollaborators.some(function (collaborator) {
          return collaborator.email && collaborator.email.toLowerCase() === email.toLowerCase();
        }) : false;
      };

      var promise = session.call([_ftrackJavascriptApi.operation.query(userQuery)]);
      promise.then(function (responses) {
        var results = {};
        var collaborators = [];
        var users = responses[0].data;

        if (users.length) {
          var _iterator3 = _createForOfIteratorHelper(users),
              _step3;

          try {
            for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
              var user = _step3.value;

              if (user.email) {
                if (user.email && !alreadyIncluded(user.email)) {
                  results[user.email] = {
                    id: user.id,
                    email: user.email,
                    name: "".concat(user.first_name, " ").concat(user.last_name),
                    first_name: user.first_name,
                    last_name: user.last_name,
                    thumbnail_id: user.thumbnail_id
                  };
                }
              }
            }
          } catch (err) {
            _iterator3.e(err);
          } finally {
            _iterator3.f();
          }
        }

        Object.keys(results).forEach(function (email) {
          if (!_this3.collaboratorExists(email)) {
            collaborators.push(results[email]);
          }
        });
        collaborators = collaborators.slice(0, 5);
        collaborators.sort(function (a, b) {
          if (a.name < b.name) {
            return -1;
          } else if (a.name > b.name) {
            return 1;
          }

          return 0;
        });

        _this3.setState({
          availableCollaborators: collaborators.slice(0, 5)
        });
      });
    }
    /** Return if a collaborator already exists. */

  }, {
    key: "collaboratorExists",
    value: function collaboratorExists(email) {
      var matchesEmail = function matchesEmail(collaborator) {
        return collaborator.email && collaborator.email.toLowerCase() === email.toLowerCase();
      };

      return this.props.value.some(matchesEmail) || this.props.alreadyIncludedCollaborators.some(matchesEmail);
    }
    /** Add collaborator from *item*. */

  }, {
    key: "addCollaborator",
    value: function addCollaborator(item) {
      // User already exists.
      var exists = this.collaboratorExists(item.email);
      var collaborators = this.props.value; // Clear form.

      this.setState({
        availableCollaborators: []
      });

      if (exists) {
        return;
      }

      this.setState({
        name: "",
        inputValue: ""
      });
      this.props.onChange([].concat(_toConsumableArray(collaborators), [item]));
    }
    /** Remove collaborator *item*. */

  }, {
    key: "removeCollaborator",
    value: function removeCollaborator(item) {
      this.props.onChange((0, _array.without)(this.props.value, item));
    }
    /** Add a new collaborator from the form. */

  }, {
    key: "_addNewCollaborator",
    value: function _addNewCollaborator() {
      var email = this.state.inputValue.toLowerCase();
      var name = this.state.name;
      var nameParts = name.split(" ");
      var first_name = nameParts.length === 1 ? nameParts[0] : nameParts.slice(0, -1).join(" ");
      var last_name = nameParts.length > 1 ? nameParts.slice(-1).join(" ") : "";
      this.addCollaborator({
        name: name,
        first_name: first_name,
        last_name: last_name,
        email: email,
        thumbnail_id: null
      });
    }
    /** Update the name in state. */

  }, {
    key: "_onNameChange",
    value: function _onNameChange(name) {
      this.setState({
        name: name
      });
    }
    /** Handle key press in collaborators. */

  }, {
    key: "_onCollaboratorsKeyDown",
    value: function _onCollaboratorsKeyDown(event) {
      var _this$state = this.state,
          inputValue = _this$state.inputValue,
          availableCollaborators = _this$state.availableCollaborators,
          name = _this$state.name;

      if (event.key === "Enter") {
        var emailTaken = this.collaboratorExists(inputValue);

        if (emailTaken) {
          return;
        }

        if (availableCollaborators.length) {
          this.addCollaborator(availableCollaborators[0]);
          event.stopPropagation();
          event.preventDefault();
        } else if (name !== "") {
          this._addNewCollaborator();

          event.stopPropagation();
          event.preventDefault();
        }
      }
    }
    /** Render *collaborators*. */

  }, {
    key: "renderResult",
    value: function renderResult(collaborators) {
      var result = [];
      var _this$props3 = this.props,
          session = _this$props3.session,
          footerClassName = _this$props3.footerClassName;
      var _this$state2 = this.state,
          inputValue = _this$state2.inputValue,
          name = _this$state2.name,
          inputActive = _this$state2.inputActive,
          availableCollaborators = _this$state2.availableCollaborators;
      var emailTaken = this.collaboratorExists(inputValue);

      if (collaborators.length) {
        result.push( /*#__PURE__*/(0, _jsxRuntime.jsxs)(_react.Fragment, {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
            className: _style.default["dropdown-header"],
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["known-collaborators"]))
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(ResultList, {
            session: session,
            className: _style.default["collaborator-matches"],
            items: availableCollaborators,
            onClick: this.addCollaborator
          }, "result-list")]
        }));
      } else if ((name !== "" || validateEmailRegexp.test(inputValue)) && !emailTaken) {
        result.push( /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
          className: _style.default["collaborator-add-new"],
          children: [name ? /*#__PURE__*/(0, _jsxRuntime.jsxs)(_react.Fragment, {
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
              children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["invite-prompt"]), {}, {
                values: {
                  name: name
                }
              }))
            }), /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
              className: _style.default["invite-prompt-message"],
              children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["invite-prompt-name"]))
            })]
          }) : /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread(_objectSpread({}, messages["invite-empty-name"]), {}, {
              values: {
                email: inputValue
              }
            }))
          }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_input.default, {
              value: name,
              onChange: this._onNameChange
            }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_button.default, {
              className: _style.default["add-button"],
              label: this.props.intl.formatMessage(messages["add-button"]),
              primary: true,
              disabled: !name,
              onClick: this._addNewCollaborator,
              type: "button"
            })]
          })]
        }, "collaborator-add-new"));
      } else if (inputActive) {
        result.push( /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
          className: _style.default["collaborator-info"],
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactIntl.FormattedMessage, _objectSpread({}, messages["search-help-text"]))
        }, "collaborator-info"));
      }

      if (result && result.length) {
        var footerClasses = (0, _classnames.default)(_style.default["collaborator-footer"], footerClassName);
        return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: footerClasses,
          children: result
        });
      }

      return null;
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      var inputValue = this.state.inputValue;
      var _this$props4 = this.props,
          session = _this$props4.session,
          label = _this$props4.label;
      var collaborators = this.props.value;
      var selectedCollaborators = null;

      if (collaborators && collaborators.length) {
        var items = collaborators.map(function (item, index) {
          return /*#__PURE__*/(0, _jsxRuntime.jsx)(ExistingCollaboratorChip, {
            item: item,
            session: session,
            onDeleteClick: _this4.removeCollaborator
          }, "collaborator-".concat(index));
        });
        selectedCollaborators = /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: _style.default["selected-collaborators"],
          children: items
        });
      }

      return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        children: [selectedCollaborators, /*#__PURE__*/(0, _jsxRuntime.jsx)(_input.default, {
          label: label,
          placeholder: this.props.intl.formatMessage(messages["add-collaborator"]),
          onFocus: this._onInputFieldFocus,
          onBlur: this._onInputFieldBlur,
          type: "text",
          value: inputValue,
          onChange: this._onCollaboratorsChange,
          onKeyDown: this._onCollaboratorsKeyDown,
          autoFocus: this.props.autoFocus
        }), this.renderResult(this.state.availableCollaborators)]
      });
    }
  }]);

  return CollaboratorSelector;
}(_react.Component);

CollaboratorSelector.propTypes = {
  value: _propTypes.default.arrayOf(_propTypes.default.shape([{
    name: _propTypes.default.string.isRequired,
    email: _propTypes.default.string,
    id: _propTypes.default.string.isRequired
  }]).isRequired),
  session: _propTypes.default.shape({
    query: _propTypes.default.func.isRequired,
    call: _propTypes.default.func.isRequired
  }).isRequired,
  intl: _reactIntl.intlShape.isRequired,
  onChange: _propTypes.default.func.isRequired,
  label: _propTypes.default.string,
  alreadyIncludedCollaborators: _propTypes.default.arrayOf(_propTypes.default.shape([{
    name: _propTypes.default.string.isRequired,
    email: _propTypes.default.string,
    id: _propTypes.default.string.isRequired
  }])),
  footerClassName: _propTypes.default.string,
  autoFocus: _propTypes.default.bool
};
CollaboratorSelector.defaultProps = {
  value: [],
  alreadyIncludedCollaborators: []
};

var _default = (0, _safe_inject_intl.default)(CollaboratorSelector);

exports.default = _default;