"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.localizeResourceSelector = exports.localizeSelector = exports.selectMessages = void 0;

var _reactIntl = require("react-intl");

var _recompose = require("recompose");

// :copyright: Copyright (c) 2018 ftrack
var selectMessages = (0, _reactIntl.defineMessages)({
  placeholder: {
    "id": "ftrack-spark-components.selector.placeholder",
    "defaultMessage": "Select..."
  },
  "loading-placeholder": {
    "id": "ftrack-spark-components.selector.loading-placeholder",
    "defaultMessage": "Loading..."
  },
  "search-prompt-text": {
    "id": "ftrack-spark-components.selector.search-prompt-text",
    "defaultMessage": "Type to search"
  },
  "clear-value-text": {
    "id": "ftrack-spark-components.selector.clear-value-text",
    "defaultMessage": "Clear"
  },
  "clear-all-text": {
    "id": "ftrack-spark-components.selector.clear-all-text",
    "defaultMessage": "Clear all"
  },
  "group-label": {
    "id": "ftrack-spark-components.selector.group-label",
    "defaultMessage": "Group"
  },
  "user-label": {
    "id": "ftrack-spark-components.selector.user-label",
    "defaultMessage": "User"
  },
  "user-inactive-label": {
    "id": "ftrack-spark-components.selector.user-inactive-label",
    "defaultMessage": "User (inactive)"
  },
  "more-results-label": {
    "id": "ftrack-spark-components.selector.more-results-label",
    "defaultMessage": "More results available, type to narrow down result"
  }
});
exports.selectMessages = selectMessages;
var localizeSelector = (0, _recompose.withProps)(function (_ref) {
  var intl = _ref.intl,
      searchPromptText = _ref.searchPromptText,
      loadingPlaceholder = _ref.loadingPlaceholder,
      placeholder = _ref.placeholder,
      clearValueText = _ref.clearValueText,
      clearAllText = _ref.clearAllText;
  return {
    placeholder: placeholder || intl.formatMessage(selectMessages.placeholder),
    loadingPlaceholder: loadingPlaceholder || intl.formatMessage(selectMessages["loading-placeholder"]),
    searchPromptText: searchPromptText || intl.formatMessage(selectMessages["search-prompt-text"]),
    clearValueText: clearValueText || intl.formatMessage(selectMessages["clear-value-text"]),
    clearAllText: clearAllText || intl.formatMessage(selectMessages["clear-all-text"])
  };
});
exports.localizeSelector = localizeSelector;
var localizeResourceSelector = (0, _recompose.withProps)(function (_ref2) {
  var intl = _ref2.intl,
      searchPromptText = _ref2.searchPromptText,
      loadingPlaceholder = _ref2.loadingPlaceholder,
      placeholder = _ref2.placeholder,
      clearValueText = _ref2.clearValueText,
      clearAllText = _ref2.clearAllText,
      groupLabel = _ref2.groupLabel,
      userLabel = _ref2.userLabel,
      userInactiveLabel = _ref2.userInactiveLabel;
  return {
    placeholder: placeholder || intl.formatMessage(selectMessages.placeholder),
    loadingPlaceholder: loadingPlaceholder || intl.formatMessage(selectMessages["loading-placeholder"]),
    searchPromptText: searchPromptText || intl.formatMessage(selectMessages["search-prompt-text"]),
    clearValueText: clearValueText || intl.formatMessage(selectMessages["clear-value-text"]),
    clearAllText: clearAllText || intl.formatMessage(selectMessages["clear-all-text"]),
    moreResultsLabel: intl.formatMessage(selectMessages["more-results-label"]),
    groupLabel: groupLabel || intl.formatMessage(selectMessages["group-label"]),
    userLabel: userLabel || intl.formatMessage(selectMessages["user-label"]),
    userInactiveLabel: userInactiveLabel || intl.formatMessage(selectMessages["user-inactive-label"])
  };
});
exports.localizeResourceSelector = localizeResourceSelector;