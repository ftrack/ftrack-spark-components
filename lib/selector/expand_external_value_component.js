"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.string.includes.js");

require("core-js/modules/es7.array.includes.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _castArray = _interopRequireDefault(require("lodash/castArray"));

var _isUndefined = _interopRequireDefault(require("lodash/isUndefined"));

var _isEqual = _interopRequireDefault(require("lodash/isEqual"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function getRawValue(item) {
  return item && item.value ? item.value : item;
}

function getRawValues(formattedValue) {
  return (0, _castArray.default)(formattedValue).map(getRawValue).filter(function (item) {
    return item;
  });
}
/**
 * Abstract component handling external value changes for Selector components
 *
 * When the value prop is changed, fetches new formatted values for display.
 *
 * When values are changed, the *onChange* prop is called with the raw values.
 *
 * To support loading of options when setting raw values, extend this component
 * and implement the `getValuesFromRawValues` method.
 */


var ExpandExternalValueComponent = /*#__PURE__*/function (_Component) {
  _inherits(ExpandExternalValueComponent, _Component);

  var _super = _createSuper(ExpandExternalValueComponent);

  function ExpandExternalValueComponent() {
    var _this;

    _classCallCheck(this, ExpandExternalValueComponent);

    _this = _super.call(this);
    _this.state = {
      internalValue: null,
      externalValue: null
    };
    _this.handleInternalValueChanged = _this.handleInternalValueChanged.bind(_assertThisInitialized(_this));
    _this._options = [];
    return _this;
  }
  /** Handle external values when component mounts */


  _createClass(ExpandExternalValueComponent, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.value) {
        this.handleExternalValueChanged(this.props.value);
      }
    }
    /** Handle external values when value changes */

  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (!(0, _isEqual.default)(nextProps.value, this.props.value)) {
        this.handleExternalValueChanged(nextProps.value);
      }
    }
    /**
     * Return promise resolved with formatted values suitable for selector
     * based on *rawValues*.
     */
    // eslint-disable-next-line

  }, {
    key: "getValuesFromRawValues",
    value: function getValuesFromRawValues(rawValues) {
      throw new Error("Not implemented", rawValues);
    }
    /** Return a single item if single selection or or *values* if multi. */

  }, {
    key: "getFirstItemOrArray",
    value: function getFirstItemOrArray(values) {
      return this.props.multi ? values : values[0];
    }
    /** Internal change handler */

  }, {
    key: "handleInternalValueChanged",
    value: function handleInternalValueChanged(internalValue) {
      var externalValue = this.getFirstItemOrArray(getRawValues(internalValue));

      if ((0, _isUndefined.default)(externalValue)) {
        externalValue = null;
      }

      var nextInternalValue = this.getFirstItemOrArray((0, _castArray.default)(internalValue));
      this.setState({
        internalValue: nextInternalValue,
        externalValue: externalValue
      });

      if (this.props.onFormattedValueChange) {
        this.props.onFormattedValueChange(nextInternalValue);
      }

      if (this.props.onChange) {
        this.props.onChange(externalValue);
      }
    }
    /**
     * External change handler
     *
     * Loads internal/formatted values for new values and removes any
     * internal/formatted value no longer present in *value*.
     */

  }, {
    key: "handleExternalValueChanged",
    value: function handleExternalValueChanged(value) {
      var _this2 = this;

      var _this$state = this.state,
          internalValue = _this$state.internalValue,
          externalValue = _this$state.externalValue;
      var nextExternalValues = getRawValues(value);
      var nextExternalValue = this.getFirstItemOrArray(nextExternalValues);

      if (!(0, _isEqual.default)(nextExternalValue, externalValue)) {
        var nextInternalValues = (0, _castArray.default)(internalValue);
        var missingValues = nextExternalValues.filter(function (rawValue) {
          return !nextInternalValues.includes(function (formattedValue) {
            return getRawValue(formattedValue) === rawValue;
          });
        });
        nextInternalValues = nextInternalValues.filter(function (formattedValue) {
          return nextExternalValues.some(function (rawValue) {
            return rawValue === getRawValue(formattedValue);
          });
        });
        var nextInternalValue = this.getFirstItemOrArray(nextInternalValues);
        this.setState({
          internalValue: nextInternalValue,
          externalValue: nextExternalValue
        });

        if (missingValues.length) {
          this.getValuesFromRawValues(nextExternalValues).then(function (newValues) {
            _this2.handleInternalValueChanged(newValues);
          });
        }
      }
    }
  }]);

  return ExpandExternalValueComponent;
}(_react.Component);

ExpandExternalValueComponent.propTypes = {
  onFormattedValueChange: _propTypes.default.func,
  onChange: _propTypes.default.func,
  multi: _propTypes.default.bool,
  value: _propTypes.default.oneOfType([_propTypes.default.array, _propTypes.default.object, _propTypes.default.string])
};
var _default = ExpandExternalValueComponent;
exports.default = _default;