"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _get_query_filter = _interopRequireDefault(require("../util/get_query_filter"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// :copyright: Copyright (c) 2020 ftrack
function getQueryFilters(queryString, attributes, baseFilter) {
  var queryFilter = (0, _get_query_filter.default)(queryString, attributes, baseFilter);

  if (queryFilter.length) {
    return "where ".concat(queryFilter);
  }

  return "";
}

var _default = getQueryFilters;
exports.default = _default;