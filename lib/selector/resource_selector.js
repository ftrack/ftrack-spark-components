"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.reflect.get.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getLoadResourceOptions = getLoadResourceOptions;
exports.ResourceCircle = ResourceCircle;
exports.ResourceValue = ResourceValue;
exports.ResourceOptionSubtitle = ResourceOptionSubtitle;
exports.VirtualizedResourceOption = VirtualizedResourceOption;
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.array.sort.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.promise.js");

require("core-js/modules/es6.regexp.split.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.array.find.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _reactSelect = _interopRequireDefault(require("react-select"));

var _reactVirtualizedSelect = _interopRequireDefault(require("react-virtualized-select"));

var _classnames = _interopRequireDefault(require("classnames"));

var _filter_options = _interopRequireDefault(require("./filter_options"));

var _get_query_filters = _interopRequireDefault(require("./get_query_filters"));

var _get_autoload_selector = _interopRequireDefault(require("./get_autoload_selector"));

var _expand_external_value_component = _interopRequireDefault(require("./expand_external_value_component"));

var _localize_selector = require("./localize_selector");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _style = _interopRequireDefault(require("./style.scss"));

var _constant = require("../util/constant");

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["session", "users", "groups", "memberships", "activeUsersOnly", "maxResults", "moreResultsLabel", "userLabel", "groupLabel", "userInactiveLabel", "value"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

/** Attributes to load for users */
var USER_ATTRIBUTES = ["id", "thumbnail_id", "first_name", "last_name", "is_active"];
/** Attributes to load for groups */

var GROUP_ATTRIBUTES = ["id", "name", "link"];
/** Translate User entities to values */

var optionFromUser = function optionFromUser(user) {
  return {
    label: "".concat(user.first_name, " ").concat(user.last_name),
    value: user.id,
    type: "user",
    data: {
      isActive: user.is_active,
      thumbnailId: user.thumbnail_id
    }
  };
};
/** Translate Group entities to values */


var optionFromGroup = function optionFromGroup(group) {
  return {
    label: group.name,
    value: group.id,
    type: "group",
    data: {
      groupName: group.link.slice(0, -1).map(function (item) {
        return item.name;
      }).join(" / ")
    }
  };
};
/** Translate Membership entities to values */


var optionFromMembership = function optionFromMembership(membership) {
  var user = membership.user,
      group = membership.group;
  return {
    label: "".concat(user.first_name, " ").concat(user.last_name),
    value: user.id,
    type: "membership",
    data: {
      isActive: user.is_active,
      groupName: group.link.map(function (item) {
        return item.name;
      }).join(" / "),
      thumbnailId: user.thumbnail_id
    }
  };
};
/** Translate API responses to values */


function optionsFromResponses(responses, _ref) {
  var users = _ref.users,
      groups = _ref.groups,
      memberships = _ref.memberships;
  var userResponse = users && responses.shift() || null;
  var groupResponse = groups && responses.shift() || null;
  var membershipResponse = memberships && responses.shift() || null;
  var options = [];

  if (userResponse && userResponse.data && userResponse.data.length) {
    var _iterator = _createForOfIteratorHelper(userResponse.data),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var user = _step.value;
        options.push(optionFromUser(user));
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }
  }

  if (groupResponse && groupResponse.data && groupResponse.data.length) {
    var _iterator2 = _createForOfIteratorHelper(groupResponse.data),
        _step2;

    try {
      for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
        var group = _step2.value;
        options.push(optionFromGroup(group));
      }
    } catch (err) {
      _iterator2.e(err);
    } finally {
      _iterator2.f();
    }
  }

  if (membershipResponse && membershipResponse.data && membershipResponse.data.length) {
    var membershipOptions = [];

    var _iterator3 = _createForOfIteratorHelper(membershipResponse.data),
        _step3;

    try {
      for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
        var membership = _step3.value;
        membershipOptions.push(optionFromMembership(membership));
      } // Sort memberships client-side as API does not currently
      // support ordering by relationships.
      // TODO: Address this behavior when pagination is introduced.

    } catch (err) {
      _iterator3.e(err);
    } finally {
      _iterator3.f();
    }

    membershipOptions.sort(function (a, b) {
      return a.label.localeCompare(b.label);
    });
    options = options.concat(membershipOptions);
  }

  return options;
}
/**
 * Return function to load resources based on *props*
 *
 * Query and return `User` resources if `users` is set.
 * Query and return `Group` resources if `groups` is set.
 * Query and return `User` resources based on their memberships if `users`
 * and `memberships` is set.
 * Don't include inactive users if `activeUsersOnly` is set.
 *
 * Users are searched based on username, first_name and last_name,
 * Groups are searched based on the groups name.
 */


function getLoadResourceOptions(props) {
  var session = props.session,
      maxResults = props.maxResults,
      moreResultsLabel = props.moreResultsLabel,
      _props$users = props.users,
      users = _props$users === void 0 ? true : _props$users,
      _props$activeUsersOnl = props.activeUsersOnly,
      activeUsersOnly = _props$activeUsersOnl === void 0 ? false : _props$activeUsersOnl,
      _props$groups = props.groups,
      groups = _props$groups === void 0 ? false : _props$groups,
      _props$memberships = props.memberships,
      memberships = _props$memberships === void 0 ? true : _props$memberships,
      _props$userFilters = props.userFilters,
      userFilters = _props$userFilters === void 0 ? null : _props$userFilters;
  return function loadResourceOptions(input) {
    var promises = [];

    if (users) {
      var userQueryAttributes = ["username", "first_name", "last_name"];
      var queryFilters = (0, _get_query_filters.default)(input, userQueryAttributes, userFilters);

      if (activeUsersOnly && queryFilters) {
        queryFilters += " and is_active is True";
      } else if (activeUsersOnly) {
        queryFilters = "where is_active is True";
      }

      var userQuery = "\n                select ".concat(USER_ATTRIBUTES.join(","), " from User\n                ").concat(queryFilters, "\n                order by first_name, last_name\n                limit ").concat(maxResults, "\n            ");
      promises.push(session.query(userQuery));
    }

    if (groups) {
      var _queryFilters = (0, _get_query_filters.default)(input, ["link"]);

      var projectGroupFilter = "".concat(_queryFilters ? "and" : "where", " local is False");
      var groupQuery = "\n                select ".concat(GROUP_ATTRIBUTES.join(","), " from Group\n                ").concat(_queryFilters, " ").concat(projectGroupFilter, "\n                order by link\n                limit ").concat(maxResults, "\n            ");
      promises.push(session.query(groupQuery));
    } // Only load memberships if there is any input to avoid listing
    // users twice when only expanding selector


    if (users && memberships && input) {
      var _queryFilters2 = (0, _get_query_filters.default)(input, ["group.link"], userFilters ? "user has (".concat(userFilters, ")") : null);

      if (activeUsersOnly && _queryFilters2) {
        _queryFilters2 += " and user.is_active is True";
      } else if (activeUsersOnly) {
        _queryFilters2 = "where user.is_active is True";
      }

      var membershipAttributes = ["id"].concat(_toConsumableArray(USER_ATTRIBUTES.map(function (attribute) {
        return "user.".concat(attribute);
      })), _toConsumableArray(GROUP_ATTRIBUTES.map(function (attribute) {
        return "group.".concat(attribute);
      })));

      var _projectGroupFilter = "".concat(_queryFilters2 ? "and" : "where", " group.local is False");

      var membershipQuery = "\n                select ".concat(membershipAttributes.join(","), " from Membership\n                ").concat(_queryFilters2, " ").concat(_projectGroupFilter, "\n                order by id\n                limit ").concat(maxResults, "\n            ");
      promises.push(session.query(membershipQuery));
    }

    return Promise.all(promises).then(function (responses) {
      var moreResultsAvailable = false;

      var _iterator4 = _createForOfIteratorHelper(responses),
          _step4;

      try {
        for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
          var response = _step4.value;

          if (response.data && response.data.length === maxResults) {
            moreResultsAvailable = true;
          }
        }
      } catch (err) {
        _iterator4.e(err);
      } finally {
        _iterator4.f();
      }

      var options = optionsFromResponses(responses, {
        users: users,
        groups: groups,
        memberships: memberships
      });

      if (moreResultsAvailable) {
        options.push({
          disabled: true,
          value: null,
          type: "label",
          label: moreResultsLabel,
          data: {}
        });
      }

      return {
        options: options
      };
    });
  };
}
/** Return CSS style for thumbnail. */


function getThumbnailStyle(session, thumbnailId, type) {
  var thumbnailUrl = thumbnailId && session.thumbnailUrl(thumbnailId, {
    size: _constant.THUMBNAIL_SIZES.small
  });
  var backgroundImage = thumbnailUrl && "url(".concat(thumbnailUrl, ")") || null;
  var backgroundColor = _constant.RESOURCE_TYPE_COLORS[type];
  return {
    backgroundImage: backgroundImage,
    backgroundColor: backgroundColor
  };
}
/**
 * Return initials for *inputString*.
 *
 * Can be used to generate initials for an user name or group name.
 * Eg. "Carl Claesson" -> "CC"
 */


function getInitialsFromString(inputString) {
  return inputString.split(" ").map(function (word) {
    return word.charAt(0);
  }).join("").slice(0, 2).toUpperCase();
}
/** Resource circle */


function ResourceCircle(_ref2) {
  var className = _ref2.className,
      session = _ref2.session,
      type = _ref2.type,
      label = _ref2.label,
      data = _ref2.data;
  var classes = (0, _classnames.default)(_style.default["color-circle"], className);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
    className: classes,
    style: getThumbnailStyle(session, data.thumbnailId, type),
    children: data.thumbnailId ? null : getInitialsFromString(label)
  });
}

ResourceCircle.propTypes = {
  className: _propTypes.default.string,
  session: _propTypes.default.object.isRequired,
  // eslint-disable-line
  type: _propTypes.default.oneOf(["user", "group", "membership"]),
  label: _propTypes.default.string,
  data: _propTypes.default.object // eslint-disable-line

};
/** Resource value */

function ResourceValue(session, _ref3) {
  var data = _ref3.data,
      label = _ref3.label,
      type = _ref3.type;
  var title = label;

  if (data && data.groupName) {
    title += " (".concat(data.groupName, ")");
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
    className: _style.default["select-value--circle"],
    title: title,
    children: [type !== "label" ? /*#__PURE__*/(0, _jsxRuntime.jsx)(ResourceCircle, {
      session: session,
      type: type,
      data: data,
      label: label
    }) : null, label]
  });
}
/** Return option subtitle for resource */


function ResourceOptionSubtitle(_ref4) {
  var className = _ref4.className,
      type = _ref4.type,
      groupName = _ref4.groupName,
      isActive = _ref4.isActive,
      userLabel = _ref4.userLabel,
      groupLabel = _ref4.groupLabel,
      userInactiveLabel = _ref4.userInactiveLabel;
  var classes = (0, _classnames.default)(_style.default["select-option-subtitle"], className);
  var subtitle = "";

  if (type === "user" && isActive === false) {
    subtitle = userInactiveLabel;
  } else if (type === "user") {
    subtitle = userLabel;
  } else if (type === "group") {
    subtitle = groupName || groupLabel;
  } else if (type === "membership") {
    subtitle = groupName;
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: classes,
    children: subtitle
  });
}

ResourceOptionSubtitle.propTypes = {
  className: _propTypes.default.string,
  type: _propTypes.default.oneOf(["user", "group", "membership"]).isRequired,
  groupName: _propTypes.default.string,
  groupLabel: _propTypes.default.node,
  userLabel: _propTypes.default.node,
  userInactiveLabel: _propTypes.default.node,
  isActive: _propTypes.default.bool
};
ResourceOptionSubtitle.defaultProps = {
  groupLabel: "Group",
  userLabel: "User",
  userInactiveLabel: "User (inactive)"
};
/** Resource option react-virtualized-select. */

function VirtualizedResourceOption(session, labels, props) {
  var userLabel = labels.userLabel,
      userInactiveLabel = labels.userInactiveLabel,
      groupLabel = labels.groupLabel; // eslint-disable-next-line

  var focusedOption = props.focusedOption,
      focusOption = props.focusOption,
      key = props.key,
      option = props.option,
      selectValue = props.selectValue,
      style = props.style;
  var className = [_style.default["select-option--resource"]];

  if (option === focusedOption) {
    className.push(_style.default["select-option--focused"]);
  }

  if (option.disabled) {
    className.push(_style.default["select-option--disabled"]);
  }

  var events = option.disabled ? {} : {
    onClick: function onClick() {
      return selectValue(option);
    },
    onMouseOver: function onMouseOver() {
      return focusOption(option);
    }
  };
  var data = option.data,
      label = option.label,
      type = option.type;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", _objectSpread(_objectSpread({
    className: className.join(" "),
    style: style
  }, events), {}, {
    children: [type !== "label" ? /*#__PURE__*/(0, _jsxRuntime.jsx)(ResourceCircle, {
      session: session,
      type: type,
      data: data,
      label: label
    }) : null, /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      className: _style.default["select-option-label"],
      children: label
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(ResourceOptionSubtitle, _objectSpread({
      type: type,
      userLabel: userLabel,
      userInactiveLabel: userInactiveLabel,
      groupLabel: groupLabel
    }, data))]
  }), key);
}

var AutoloadAsyncSelect = (0, _get_autoload_selector.default)(_reactSelect.default.Async);
/** Remote selector */

var ResourceSelector = /*#__PURE__*/function (_ExpandExternalValueC) {
  _inherits(ResourceSelector, _ExpandExternalValueC);

  var _super = _createSuper(ResourceSelector);

  function ResourceSelector() {
    var _this;

    _classCallCheck(this, ResourceSelector);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));
    _this.loadResourceOptions = _this.loadResourceOptions.bind(_assertThisInitialized(_this));
    _this.handleInternalValueChanged = _this.handleInternalValueChanged.bind(_assertThisInitialized(_this));
    return _this;
  }
  /** Return formatted options from resource ids. */


  _createClass(ResourceSelector, [{
    key: "getValuesFromRawValues",
    value: function getValuesFromRawValues(resourceIds) {
      var _this2 = this;

      var _this$props = this.props,
          session = _this$props.session,
          users = _this$props.users,
          groups = _this$props.groups;

      if (!resourceIds || !(users || groups)) {
        return Promise.resolve([]);
      } // Attempt to get values from loaded options.


      var formattedValues = resourceIds.map(function (value) {
        return _this2._options.find(function (option) {
          return option.value === value;
        });
      }).filter(function (option) {
        return option;
      });

      if (formattedValues.length === resourceIds.length) {
        return Promise.resolve(formattedValues);
      } // Load formatted values from API.


      var promises = [];

      if (users) {
        promises.push(session.query("select ".concat(USER_ATTRIBUTES.join(","), " from User where\n                id in (\"").concat(resourceIds.join('","'), "\")\n                order by first_name, last_name")));
      }

      if (groups) {
        promises.push(session.query("select ".concat(GROUP_ATTRIBUTES.join(","), " from Group where\n                id in (\"").concat(resourceIds.join('","'), "\")\n                order by name")));
      }

      return Promise.all(promises).then(function (responses) {
        var options = optionsFromResponses(responses, {
          users: users,
          groups: groups
        });
        return options;
      });
    }
  }, {
    key: "handleInternalValueChanged",
    value: function handleInternalValueChanged() {
      var _get2;

      for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      (_get2 = _get(_getPrototypeOf(ResourceSelector.prototype), "handleInternalValueChanged", this)).call.apply(_get2, [this].concat(args));

      this.forceUpdateSelectorHeight();
    }
    /**
     * Force update selector height
     *
     * This fixes issues experienced in Chrome/Mac where the
     * React-Virtualized-Select's internal AutoSize Component does not update
     * until the element received a focus/blur event.
     *
     * https://github.com/bvaughn/react-virtualized-select/issues/2
     */

  }, {
    key: "forceUpdateSelectorHeight",
    value: function forceUpdateSelectorHeight() {
      // eslint-disable-next-line react/no-find-dom-node
      var element = _reactDom.default.findDOMNode(this.node);

      if (element) {
        // eslint-disable-next-line no-undef
        element.style.height = window.getComputedStyle(element).height;
        setTimeout(function () {
          element.style.height = "auto";
        }, 0);
      }
    }
  }, {
    key: "loadResourceOptions",
    value: function loadResourceOptions(input) {
      var _this3 = this;

      var fetchOptions = getLoadResourceOptions(this.props)(input);
      fetchOptions.then(function (_ref5) {
        var options = _ref5.options;
        _this3._options = options;
      });
      return fetchOptions;
    }
    /** Render component. */

  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      var _this$props2 = this.props,
          session = _this$props2.session,
          users = _this$props2.users,
          groups = _this$props2.groups,
          memberships = _this$props2.memberships,
          activeUsersOnly = _this$props2.activeUsersOnly,
          maxResults = _this$props2.maxResults,
          moreResultsLabel = _this$props2.moreResultsLabel,
          userLabel = _this$props2.userLabel,
          groupLabel = _this$props2.groupLabel,
          userInactiveLabel = _this$props2.userInactiveLabel,
          value = _this$props2.value,
          selectProps = _objectWithoutProperties(_this$props2, _excluded);

      if (!users && !groups) {
        throw new TypeError("Either users or groups must be specified in props.");
      }

      var ResourceValueSession = ResourceValue.bind(null, session);
      var ResourceOptionSession = VirtualizedResourceOption.bind(null, session, {
        userLabel: userLabel,
        groupLabel: groupLabel,
        userInactiveLabel: userInactiveLabel
      });
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactVirtualizedSelect.default, _objectSpread(_objectSpread({
        selectComponent: AutoloadAsyncSelect,
        loadOptions: this.loadResourceOptions,
        valueRenderer: ResourceValueSession,
        optionRenderer: ResourceOptionSession,
        optionHeight: 40,
        filterOptions: _filter_options.default
      }, selectProps), {}, {
        value: this.state.internalValue,
        ref: function ref(node) {
          _this4.node = node;
        },
        onChange: this.handleInternalValueChanged
      }));
    }
  }]);

  return ResourceSelector;
}(_expand_external_value_component.default);

ResourceSelector.propTypes = {
  session: _propTypes.default.object.isRequired,
  users: _propTypes.default.bool,
  activeUsersOnly: _propTypes.default.bool,
  groups: _propTypes.default.bool,
  memberships: _propTypes.default.bool,
  maxResults: _propTypes.default.number,
  moreResultsLabel: _propTypes.default.node,
  groupLabel: _propTypes.default.node,
  userLabel: _propTypes.default.node,
  userInactiveLabel: _propTypes.default.node,
  userFilters: _propTypes.default.string
};
ResourceSelector.defaultProps = {
  activeUsersOnly: false,
  users: false,
  groups: false,
  memberships: true,
  maxResults: 50,
  moreResultsLabel: "More results available, type to narrow down result"
};

var _default = (0, _safe_inject_intl.default)((0, _localize_selector.localizeResourceSelector)(ResourceSelector));

exports.default = _default;