"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.array.filter.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactSelect = _interopRequireDefault(require("react-select"));

var _ftrackJavascriptApi = require("ftrack-javascript-api");

var _filter_options = _interopRequireDefault(require("./filter_options"));

var _get_autoload_selector = _interopRequireDefault(require("./get_autoload_selector"));

var _remote_selector = require("./remote_selector");

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["session", "projectSchemaId", "entityType", "typeId"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/** Return if *candidate* matches *searchString*. */
function matches(candidate, searchString) {
  var _searchString = searchString.toLowerCase().trim();

  var _candidate = candidate.toLowerCase().trim();

  return !_searchString || _candidate.indexOf(_searchString) !== -1;
}
/** Return function to load status options. */


function getStatusOptions(props) {
  var session = props.session,
      projectSchemaId = props.projectSchemaId,
      entityType = props.entityType,
      typeId = props.typeId;
  return function (input) {
    var statusPromise = _ftrackJavascriptApi.projectSchema.getStatuses(session, projectSchemaId, entityType, typeId);

    return statusPromise.then(function (statuses) {
      var options = statuses.filter(function (status) {
        return matches(status.name, input);
      }).map(function (status) {
        return {
          value: status.id,
          label: status.name,
          data: {
            color: status.color
          }
        };
      });
      return {
        options: options
      };
    });
  };
}

var AutoloadAsyncSelect = (0, _get_autoload_selector.default)(_reactSelect.default.Async);
/** Status selector */

function StatusSelector(allProps) {
  var session = allProps.session,
      projectSchemaId = allProps.projectSchemaId,
      entityType = allProps.entityType,
      typeId = allProps.typeId,
      selectProps = _objectWithoutProperties(allProps, _excluded);

  var ownProps = {
    session: session,
    projectSchemaId: projectSchemaId,
    entityType: entityType,
    typeId: typeId
  };
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(AutoloadAsyncSelect, _objectSpread({
    loadOptions: getStatusOptions(ownProps),
    valueRenderer: _remote_selector.SelectValue,
    optionRenderer: _remote_selector.SelectOption,
    filterOptions: _filter_options.default
  }, selectProps));
}

StatusSelector.propTypes = {
  session: _propTypes.default.object.isRequired,
  projectSchemaId: _propTypes.default.string,
  entityType: _propTypes.default.string,
  typeId: _propTypes.default.string
};
StatusSelector.defaultProps = {
  typeId: null
};
var _default = StatusSelector;
exports.default = _default;