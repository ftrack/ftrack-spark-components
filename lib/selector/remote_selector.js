"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

require("core-js/modules/es6.array.from.js");

require("core-js/modules/es6.array.slice.js");

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SelectLabel = SelectLabel;
exports.SelectValue = SelectValue;
exports.SelectOption = SelectOption;
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.promise.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.array.find.js");

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactSelect = _interopRequireDefault(require("react-select"));

var _filter_options = _interopRequireDefault(require("./filter_options"));

var _get_query_filters = _interopRequireDefault(require("./get_query_filters"));

var _get_autoload_selector = _interopRequireDefault(require("./get_autoload_selector"));

var _expand_external_value_component = _interopRequireDefault(require("./expand_external_value_component"));

var _localize_selector = require("./localize_selector");

var _safe_inject_intl = _interopRequireDefault(require("../util/hoc/safe_inject_intl"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["session", "entityType", "valueField", "labelField", "orderByField", "extraFields", "extraSearchFields", "value"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

/** Select label - render item with optional color circle. */
function SelectLabel(className, _ref) {
  var data = _ref.data,
      label = _ref.label;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
    className: className,
    children: [data && data.color ? /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      className: _style.default["color-circle"],
      style: {
        backgroundColor: data.color
      }
    }) : null, label]
  });
}
/** Selected value */


function SelectValue(_ref2) {
  var data = _ref2.data,
      label = _ref2.label;
  var className = data && data.color ? _style.default["select-value--circle"] : _style.default["select-value"];
  return SelectLabel(className, {
    data: data,
    label: label
  });
}
/** Select menu option */


function SelectOption(_ref3) {
  var data = _ref3.data,
      label = _ref3.label,
      disabled = _ref3.disabled;
  var className = data && data.color ? _style.default["select-option--circle"] : _style.default["select-option"];

  if (disabled) {
    className += " " + _style.default["select-option--disabled"];
  }

  return SelectLabel(className, {
    data: data,
    label: label
  });
}

var AutoloadAsyncSelect = (0, _get_autoload_selector.default)(_reactSelect.default.Async);
/** Remote selector */

var RemoteSelector = /*#__PURE__*/function (_ExpandExternalValueC) {
  _inherits(RemoteSelector, _ExpandExternalValueC);

  var _super = _createSuper(RemoteSelector);

  function RemoteSelector() {
    var _this;

    _classCallCheck(this, RemoteSelector);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));
    _this.loadOptions = _this.loadOptions.bind(_assertThisInitialized(_this));
    return _this;
  }
  /** Return attributes to fetch */


  _createClass(RemoteSelector, [{
    key: "getAttributes",
    value: function getAttributes() {
      var _this$props = this.props,
          valueField = _this$props.valueField,
          labelField = _this$props.labelField,
          extraFields = _this$props.extraFields;
      return [valueField, labelField].concat(_toConsumableArray(extraFields)).join(",");
    }
    /** Return attribute to order by */

  }, {
    key: "getOrderByField",
    value: function getOrderByField() {
      var _this$props2 = this.props,
          orderByField = _this$props2.orderByField,
          labelField = _this$props2.labelField;

      if (orderByField === "labelField") {
        orderByField = labelField;
      }

      return orderByField;
    }
    /**
     * Return `loadOptions` function based on *props*.
     *
     * The returned function will query the API for the specified attributes and
     * return a promise resolved with an options array suitable for
     * usage by react-select.
     */

  }, {
    key: "loadOptions",
    value: function loadOptions(input) {
      var _this2 = this;

      var _this$props3 = this.props,
          session = _this$props3.session,
          entityType = _this$props3.entityType,
          labelField = _this$props3.labelField,
          extraSearchFields = _this$props3.extraSearchFields,
          maxResults = _this$props3.maxResults,
          baseFilter = _this$props3.baseFilter,
          orderByDirection = _this$props3.orderByDirection;
      var filterExpression = (0, _get_query_filters.default)(input, [labelField].concat(_toConsumableArray(extraSearchFields)), baseFilter);
      return session.query("\n            select ".concat(this.getAttributes(), "\n            from ").concat(entityType, " ").concat(filterExpression, "\n            order by ").concat(this.getOrderByField(), " ").concat(orderByDirection, "\n            limit ").concat(maxResults)).then(function (response) {
        _this2._options = _this2.valuesFromResponse(response);
        return {
          options: _this2._options
        };
      });
    }
    /** Convert response to options array suitable for react-select. */

  }, {
    key: "valuesFromResponse",
    value: function valuesFromResponse(response) {
      var _this$props4 = this.props,
          valueField = _this$props4.valueField,
          labelField = _this$props4.labelField,
          maxResults = _this$props4.maxResults,
          moreResultsLabel = _this$props4.moreResultsLabel;
      var values = response.data.map(function (entity) {
        return {
          value: entity[valueField],
          label: entity[labelField],
          data: entity
        };
      });

      if (values.length === maxResults) {
        values.push({
          disabled: true,
          value: null,
          label: moreResultsLabel,
          data: {}
        });
      }

      return values;
    }
    /** Get formatted values based on *rawValues*. */

  }, {
    key: "getValuesFromRawValues",
    value: function getValuesFromRawValues(rawValues) {
      var _this3 = this;

      if (!rawValues || !rawValues.length) {
        return Promise.resolve([]);
      } // Attempt to get values from loaded options.


      var formattedValues = rawValues.map(function (value) {
        return _this3._options.find(function (option) {
          return option.value === value;
        });
      }).filter(function (option) {
        return option;
      });

      if (formattedValues.length === rawValues.length) {
        return Promise.resolve(formattedValues);
      } // Load values


      var _this$props5 = this.props,
          session = _this$props5.session,
          entityType = _this$props5.entityType,
          valueField = _this$props5.valueField;
      return session.query("select ".concat(this.getAttributes(), " from ").concat(entityType, "\n            where ").concat(valueField, " in (\"").concat(rawValues.join('", "'), "\")\n            order by ").concat(this.getOrderByField())).then(function (response) {
        return _this3.valuesFromResponse(response);
      });
    }
    /** Render component. */

  }, {
    key: "render",
    value: function render() {
      var _this$props6 = this.props,
          session = _this$props6.session,
          entityType = _this$props6.entityType,
          valueField = _this$props6.valueField,
          labelField = _this$props6.labelField,
          orderByField = _this$props6.orderByField,
          extraFields = _this$props6.extraFields,
          extraSearchFields = _this$props6.extraSearchFields,
          value = _this$props6.value,
          selectProps = _objectWithoutProperties(_this$props6, _excluded);

      return /*#__PURE__*/(0, _jsxRuntime.jsx)(AutoloadAsyncSelect, _objectSpread(_objectSpread({
        loadOptions: this.loadOptions,
        valueRenderer: SelectValue,
        optionRenderer: SelectOption,
        filterOptions: _filter_options.default
      }, selectProps), {}, {
        value: this.state.internalValue,
        onChange: this.handleInternalValueChanged
      }));
    }
  }]);

  return RemoteSelector;
}(_expand_external_value_component.default);

RemoteSelector.propTypes = {
  session: _propTypes.default.object.isRequired,
  entityType: _propTypes.default.string.isRequired,
  labelField: _propTypes.default.string,
  orderByField: _propTypes.default.string,
  orderByDirection: _propTypes.default.string,
  baseFilter: _propTypes.default.string,
  extraFields: _propTypes.default.array,
  extraSearchFields: _propTypes.default.array,
  maxResults: _propTypes.default.number,
  moreResultsLabel: _propTypes.default.string
};
RemoteSelector.defaultProps = {
  valueField: "id",
  labelField: "name",
  orderByField: "labelField",
  extraFields: [],
  extraSearchFields: [],
  maxResults: 50,
  orderByDirection: "ASC",
  moreResultsLabel: "More results available, type to narrow down result"
};

var _default = (0, _safe_inject_intl.default)((0, _localize_selector.localizeSelector)(RemoteSelector));

exports.default = _default;