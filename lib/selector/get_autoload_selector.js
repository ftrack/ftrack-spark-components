"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Higher-Order Component used to customize the behavior of an Select.Async
 * component.
 *
 * - We do not want any caching of results.
 * - Items should be loaded when the input is focused.
 * - Items should also be loaded when an item is selected, so that all items
 *   are available matching the input field.
 */
function getAutoloadSelector(SelectorComponent) {
  function AutoloadSelector(props) {
    var asyncSelect = null;
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(SelectorComponent, _objectSpread(_objectSpread({
      cache: false,
      openOnFocus: true,
      autoload: false
    }, props), {}, {
      ref: function ref(select) {
        asyncSelect = select;
      },
      onFocus: function onFocus() {
        asyncSelect.loadOptions("");

        if (props.onFocus) {
          props.onFocus.apply(props, arguments);
        }
      }
    }));
  }

  AutoloadSelector.propTypes = {
    onChange: _propTypes.default.func,
    onFocus: _propTypes.default.func
  };
  return AutoloadSelector;
}

var _default = getAutoloadSelector;
exports.default = _default;