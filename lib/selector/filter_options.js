"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

require("core-js/modules/es6.array.filter.js");

// :copyright: Copyright (c) 2016 ftrack

/** Filter resource *options*, excluding only *currentValues* */
function filterAsyncOptions(options, query, currentValues, props) {
  var excludeOptions = currentValues ? currentValues.map(function (currentValue) {
    return currentValue[props.valueKey];
  }) : [];
  return options.filter(function (option) {
    if (excludeOptions.indexOf(option[props.valueKey]) !== -1) {
      return false;
    }

    return true;
  });
}

var _default = filterAsyncOptions;
exports.default = _default;