"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _button = _interopRequireDefault(require("react-toolbox/lib/button"));

var _tooltip = _interopRequireDefault(require("react-toolbox/lib/tooltip"));

var _font_icon = require("react-toolbox/lib/font_icon");

var _dropdown_button_theme = _interopRequireDefault(require("./dropdown_button_theme.scss"));

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["icon", "children", "className", "size", "variant"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var TooltipButton = (0, _tooltip.default)(_button.default);

function DropdownButton(_ref) {
  var _classNames;

  var icon = _ref.icon,
      children = _ref.children,
      className = _ref.className,
      _ref$size = _ref.size,
      size = _ref$size === void 0 ? "medium" : _ref$size,
      _ref$variant = _ref.variant,
      variant = _ref$variant === void 0 ? "flat" : _ref$variant,
      props = _objectWithoutProperties(_ref, _excluded);

  var classes = (0, _classnames.default)((_classNames = {}, _defineProperty(_classNames, _dropdown_button_theme.default.sizeMedium, size === "medium"), _defineProperty(_classNames, _dropdown_button_theme.default.sizeSmall, size === "small"), _defineProperty(_classNames, _dropdown_button_theme.default.rounded, variant === "rounded"), _classNames), className);
  var ButtonComponent = props.tooltip ? TooltipButton : _button.default;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(ButtonComponent, _objectSpread(_objectSpread({
    className: classes,
    theme: _dropdown_button_theme.default
  }, props), {}, {
    children: [icon && /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.FontIcon, {
      className: _dropdown_button_theme.default.icon,
      value: icon
    }), children, /*#__PURE__*/(0, _jsxRuntime.jsx)(_font_icon.FontIcon, {
      className: _dropdown_button_theme.default.arrow,
      value: "arrow_drop_down"
    })]
  }));
}

DropdownButton.propTypes = {
  className: _propTypes.default.string,
  children: _propTypes.default.node,
  size: _propTypes.default.oneOf(["small", "medium"]),
  icon: _propTypes.default.string,
  variant: _propTypes.default.oneOf(["rounded", "flat"])
};
var _default = DropdownButton;
exports.default = _default;