"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es6.reflect.construct.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.string.iterator.js");

require("core-js/modules/es6.object.to-string.js");

require("core-js/modules/es6.array.iterator.js");

require("core-js/modules/web.dom.iterable.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.object.set-prototype-of.js");

require("core-js/modules/es6.object.get-prototype-of.js");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _menu = require("react-toolbox/lib/menu");

var _classnames = _interopRequireDefault(require("classnames"));

var _menu_theme = _interopRequireDefault(require("./menu_theme.scss"));

var _compressed_menu_theme = _interopRequireDefault(require("./compressed_menu_theme.scss"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

/** Button menu component used to display a button that trigger a menu. */
var ButtonMenu = /*#__PURE__*/function (_Component) {
  _inherits(ButtonMenu, _Component);

  var _super = _createSuper(ButtonMenu);

  function ButtonMenu() {
    var _this;

    _classCallCheck(this, ButtonMenu);

    _this = _super.call(this);
    _this.state = {
      active: false
    };
    _this.hideMenu = _this.hideMenu.bind(_assertThisInitialized(_this));
    _this.showMenu = _this.showMenu.bind(_assertThisInitialized(_this));
    _this.handleClick = _this.handleClick.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(ButtonMenu, [{
    key: "hideMenu",
    value: function hideMenu() {
      this.setState({
        active: false
      });
    }
  }, {
    key: "showMenu",
    value: function showMenu(e) {
      e.stopPropagation();
      this.setState({
        active: true
      });
    } // eslint-disable-next-line class-methods-use-this

  }, {
    key: "handleClick",
    value: function handleClick(e) {
      // Capture click events to ensure items behind menus are not clicked
      // when event bubbles.
      e.stopPropagation();
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          children = _this$props.children,
          onSelect = _this$props.onSelect,
          button = _this$props.button,
          className = _this$props.className,
          position = _this$props.position,
          display = _this$props.display,
          variant = _this$props.variant;

      var _classNames = (0, _classnames.default)(_style.default.wrapper, _defineProperty({}, _style.default.displayInlineBlock, display === "inline-block"), className);

      var clonedButton = /*#__PURE__*/(0, _react.cloneElement)(button, {
        onClick: this.showMenu
      });
      return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: _classNames,
        onClick: this.handleClick,
        children: [clonedButton, /*#__PURE__*/(0, _jsxRuntime.jsx)(_menu.Menu, {
          active: this.state.active,
          onHide: this.hideMenu,
          position: position,
          onSelect: onSelect,
          menuRipple: true,
          className: _style.default.menu,
          theme: variant === "compressed" ? _compressed_menu_theme.default : _menu_theme.default,
          children: children
        })]
      });
    }
  }]);

  return ButtonMenu;
}(_react.Component);

exports.default = ButtonMenu;
ButtonMenu.propTypes = {
  className: _propTypes.default.string,
  display: _propTypes.default.oneOf(["inline-block"]),
  button: _propTypes.default.node.isRequired,
  position: _propTypes.default.string,
  children: _propTypes.default.node,
  onSelect: _propTypes.default.func,
  variant: _propTypes.default.oneOf(["compressed", "default"])
};
ButtonMenu.defaultProps = {
  className: ""
};