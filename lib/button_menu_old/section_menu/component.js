"use strict";

require("core-js/modules/es6.object.keys.js");

require("core-js/modules/es6.symbol.js");

require("core-js/modules/es6.array.filter.js");

require("core-js/modules/es6.object.get-own-property-descriptor.js");

require("core-js/modules/es7.object.get-own-property-descriptors.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.map.js");

var _react = require("react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _menu = require("react-toolbox/lib/menu");

var _identifiers = require("react-toolbox/lib/identifiers");

var _classnames = _interopRequireDefault(require("classnames"));

var _heading = _interopRequireDefault(require("../../heading"));

var _menu_item_theme = _interopRequireDefault(require("./menu_item_theme.scss"));

var _style = _interopRequireDefault(require("./style.scss"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function SectionTitle(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_heading.default, {
    variant: "label",
    color: "secondary",
    className: _style.default.header,
    children: children
  });
}

SectionTitle.propTypes = {
  children: _propTypes.default.node
};

function SectionMenuItem(_ref2) {
  var _classNames;

  var value = _ref2.value,
      icon = _ref2.icon,
      selectedIcon = _ref2.selectedIcon,
      caption = _ref2.caption,
      selected = _ref2.selected,
      disabled = _ref2.disabled,
      onSelectionChange = _ref2.onSelectionChange,
      className = _ref2.className;

  var onClick = function onClick() {
    if (onSelectionChange) {
      if (!selected) {
        onSelectionChange(value, !selected);
      }
    }
  };

  var classes = (0, _classnames.default)(className, (_classNames = {}, _defineProperty(_classNames, _menu_item_theme.default.selected, selected), _defineProperty(_classNames, _menu_item_theme.default.selectedIcon, !icon && selectedIcon), _classNames));
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_menu.MenuItem, {
    theme: _menu_item_theme.default,
    icon: icon || selectedIcon,
    value: value,
    caption: caption,
    className: classes,
    onClick: onClick,
    disabled: disabled
  });
}

SectionMenuItem.propTypes = {
  icon: _propTypes.default.oneOf([_propTypes.default.string, _propTypes.default.node]),
  selectedIcon: _propTypes.default.oneOf([_propTypes.default.string, _propTypes.default.node]),
  caption: _propTypes.default.node,
  value: _propTypes.default.string,
  selected: _propTypes.default.bool,
  disabled: _propTypes.default.bool,
  className: _propTypes.default.string
};
SectionMenuItem.toolboxId = _identifiers.MENU_ITEM;

function SectionMenu(_ref3) {
  var sections = _ref3.sections,
      onSelectionChange = _ref3.onSelectionChange;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_react.Fragment, {
    children: sections.map(function (section) {
      return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_react.Fragment, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(SectionTitle, {
          children: section.title
        }), section.options.map(function (option) {
          return /*#__PURE__*/(0, _react.createElement)(SectionMenuItem, _objectSpread(_objectSpread({}, option), {}, {
            key: option.value,
            onSelectionChange: onSelectionChange
          }));
        })]
      });
    })
  });
}

SectionMenu.propTypes = {
  sections: _propTypes.default.arrayOf(_propTypes.default.shape({
    title: _propTypes.default.node,
    options: _propTypes.default.arrayOf(_propTypes.default.shape(SectionMenuItem.propTypes))
  })),
  onSelectionChange: _propTypes.default.func
};
var _default = SectionMenu;
exports.default = _default;