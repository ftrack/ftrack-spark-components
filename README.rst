##################
Deprecated package
##################

This package has been deprecated and put in a read-only mode as we are working on new and improved components. If you have applications or widgets that use ftrack-spark-components, it is recommended that you fork this repository if you need to make changes.

----------

#######################
ftrack spark components
#######################

ftrack-spark-components is a collection of reusable React components used to
build ftrack integrations and web applications by first and third-parties.


Installation
============

```
yarn install
husky install  # to get pre-commit hooks set up properly
```

Commands
========

Start storybook for development::

    yarn start

Compile and copy the code to lib/::

    yarn build

Setup
=====

Some components require a API session to function properly. For them to function
properly, you should define the following environment variables:

* FTRACK_SERVER
* FTRACK_API_USER
* FTRACK_API_KEY

Theme
=====

The components can be used with a dark or a light theme (default). To use the
dark theme for the storybook, run `yarn start:dark`.

MUI theme
---------

For components that uses MUI components, you can apply the theme under
style/mui_theme. See the `style/mui_theme/README.rst` for more information.

Sass theme
----------

For consuming applications that use components that uses React Toolbox, or
general sass-based styling, you should load the theme variables in your
application styles. This can be done by including in all sass files:

    @import "~ftrack-spark-components/lib/style/[theme]/theme.scss";

If you want to use any selector component (based on react-select), also include
the following:

    @import "~ftrack-spark-components/lib/selector/style/index.scss";

Dataview
========

The data view and supporting modules allow for building specialised an generic
views of ftrack data. It takes care of loading, filtering, refreshing and
rendering of entities in ftrack and consists of the following submodules in
ftrack-spark-component.

dataview/container.js
---------------------

Contains redux reducer, actions and sagas to load data via the API. It is
completely agnostic to what entities or projections it loads. It can load data
with a filter expression string, limit, offset and supports ordering. It can
also refresh the data given a primary key.

It exports it’s sagas, actions and reducer and will let the consumer decide how
to use them.

It also exports a withDataLoader hoc that will add a “loader” prop to
the wrapped component. The loader prop is an object that defines the what
data to load.

dataview/grid/*
---------------

A grid view (spreadsheet) component - takes data
(provided by the withDataLoader) and attributes as an input and renders them.

Designed to be agnostic to how the data is loaded.

dataview/card/*
---------------

A card view component - takes data (provided by the withDataLoader) and
attributes as an input and renders them.

Designed to be agnostic to how the data is loaded.

dataview/util.js
----------------

This contains logic to semi-automatically build a set of attribute definitions
from a session.schema.

It supports:

* Attribute formatting options and label (ATTRIBUTE_OVERRIDES)
* Filtering options and label (FILTER_OVERRIDES)
* Custom attributes

It will generate labels automatically but supports overriding them. Any
formatters are imported from other places in ftrack-spark-component.

Exports getFilters and getAttributes - the result of these methods are
filter and attribute definitions that are consumed by the withDataLoader,
card and grid view, filter panel.

attribute/component.js
-----------------------

Simply component for localising and displaying attributes from
dataview/util.js:getAttributes and dataview/util.js:getFilters.

attribute_browser/component.js
------------------------------

Browse and select attributes as given by
dataview/util.js:getAttributes and dataview/util.js:getFilters.

filter/component.js
-------------------

Filter panel, consumes result of dataview/util.js:getFilters.

filter/util.js
--------------

Will produce a valid api filter expression string given input of filter panel
result and dataview/util.js:getFilters.
