// :copyright: Copyright (c) 2017 ftrack
const manageTranslations = require("react-intl-translations-manager").default;

manageTranslations({
  messagesDirectory: "lib/i18n/message/",
  translationsDirectory: "lib/i18n/messages/",
  singleMessagesFile: true,
  languages: ["en", "zh-CN"],
});
