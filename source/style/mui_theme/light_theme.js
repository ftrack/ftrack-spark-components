// :copyright: Copyright (c) 2021 ftrack

import { lightThemePalette } from "./light_theme_palette";
import { createTheme } from "./create_theme";

const lightTheme = createTheme(lightThemePalette);

export default lightTheme;
