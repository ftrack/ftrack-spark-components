// :copyright: Copyright (c) 2021 ftrack
import * as data from "../token/design_tokens.json";
const {
  red,
  teal,
  orange,
  yellow,
  purple,
  green,
  darkContrastColor,
  lightContrastColor,
  neutral,
  backgroundLight,
} = data.default.colors;

export const lightThemePalette = {
  mode: "light",
  background: {
    paper: backgroundLight.paper,
    default: backgroundLight.default,
    imageOverlay: darkContrastColor.disabled,
    raised: backgroundLight.raised,
    backdrop: darkContrastColor.disabled,
  },
  text: {
    primary: darkContrastColor.highEmphasis,
    secondary: darkContrastColor.mediumEmphasis,
    disabled: darkContrastColor.disabled,
    icon: darkContrastColor.highEmphasis,
  },
  primary: {
    main: purple[400],
    light: purple[300],
    dark: purple[700],
    contrastText: lightContrastColor.highEmphasis,
  },
  neutral: {
    main: neutral[800],
    light: neutral[300],
    dark: neutral[900],
  },
  secondary: {
    main: yellow[400],
    light: yellow[300],
    dark: yellow[500],
    contrastText: lightContrastColor.highEmphasis,
  },
  error: {
    main: red[400],
    dark: red[700],
    light: red[300],
    contrastText: lightContrastColor.highEmphasis,
  },
  info: {
    main: teal[600],
    dark: teal[800],
    light: teal[400],
    contrastText: lightContrastColor.highEmphasis,
  },
  warning: {
    main: orange[500],
    dark: orange[700],
    light: orange[400],
    contrastText: lightContrastColor.highEmphasis,
  },
  success: {
    main: green[600],
    dark: green[800],
    light: green[400],
    contrastText: lightContrastColor.highEmphasis,
  },
  divider: darkContrastColor.faded,
  other: {
    outlineBorder: darkContrastColor.faded,
    standardFieldLine: lightContrastColor.mediumEmphasis,
    snackbar: darkContrastColor.highEmphasis,
    avatarBackground: neutral[300],
  },
  action: {
    active: darkContrastColor.highEmphasis,
    hover: darkContrastColor.highlighted,
    selected: darkContrastColor.highlighted,
    disabled: darkContrastColor.highlighted,
    disabledBackground: darkContrastColor.highlighted,
    focus: darkContrastColor.faded,
  },
};
