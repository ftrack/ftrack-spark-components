// :copyright: Copyright (c) 2021 ftrack

import * as data from "../token/design_tokens.json";
const { fontSize, fontWeight } = data.default.typography;

export const muiTypography = {
  htmlFontSize: 10,
  letterSpacing: "normal",
  fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  h1: {
    fontSize: fontSize.h1,
    letterSpacing: "normal",
    fontWeight: fontWeight.thin,
  },
  h2: {
    fontSize: fontSize.h2,
    letterSpacing: "normal",
    fontWeight: fontWeight.thin,
  },
  h3: {
    fontSize: fontSize.h3,
    letterSpacing: "normal",
    fontWeight: fontWeight.regular,
  },
  h4: {
    fontSize: fontSize.h4,
    letterSpacing: "normal",
    fontWeight: fontWeight.regular,
  },
  h5: {
    fontSize: fontSize.h5,
    letterSpacing: "normal",
    fontWeight: fontWeight.medium,
  },
  h6: {
    fontSize: fontSize.h5,
    letterSpacing: "normal",
    fontWeight: fontWeight.medium,
  },
  subtitle1: {
    fontSize: fontSize.regular,
    letterSpacing: "normal",
  },
  subtitle2: {
    fontSize: fontSize.small,
    letterSpacing: "normal",
  },
  body1: {
    fontSize: fontSize.regular,
    letterSpacing: "normal",
  },
  body2: {
    fontSize: fontSize.small,
    letterSpacing: "normal",
  },
  button: {
    fontSize: fontSize.regular,
  },
  caption: {
    fontSize: fontSize.xsmall,
    letterSpacing: "normal",
  },
  overline: {
    fontSize: fontSize.xsmall,
  },
  fontSizeLarge: fontSize.large,
  fontSizeRegular: fontSize.regular,
  fontSizeSmall: fontSize.small,
  fontSizeXsmall: fontSize.xsmall,
  fontWeightThin: fontWeight.thin,
  fontWeightRegular: fontWeight.regular,
  fontWeightMedium: fontWeight.medium,
  fontWeightBold: fontWeight.bold,
};
