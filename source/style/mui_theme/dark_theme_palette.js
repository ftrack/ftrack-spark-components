// :copyright: Copyright (c) 2021 ftrack
import * as data from "../token/design_tokens.json";
const {
  red,
  teal,
  orange,
  yellow,
  purple,
  green,
  darkContrastColor,
  lightContrastColor,
  neutral,
  backgroundDark,
} = data.default.colors;

export const darkThemePalette = {
  mode: "dark",
  background: {
    paper: backgroundDark.paper,
    default: backgroundDark.default,
    imageOverlay: darkContrastColor.disabled,
    raised: backgroundDark.raised,
    backdrop: darkContrastColor.disabled,
  },
  text: {
    primary: lightContrastColor.highEmphasis,
    secondary: lightContrastColor.mediumEmphasis,
    disabled: lightContrastColor.disabled,
    icon: lightContrastColor.highEmphasis,
  },
  primary: {
    main: purple[200],
    light: purple[100],
    dark: purple[400],
    contrastText: darkContrastColor.highEmphasis,
  },
  neutral: {
    main: neutral[100],
    light: neutral[50],
    dark: neutral[400],
  },
  secondary: {
    main: yellow[200],
    light: yellow[100],
    dark: yellow[300],
    contrastText: darkContrastColor.highEmphasis,
  },
  error: {
    main: red[300],
    dark: red[400],
    light: red[100],
    contrastText: darkContrastColor.highEmphasis,
  },
  info: {
    main: teal[200],
    dark: teal[400],
    light: teal[100],
    contrastText: darkContrastColor.highEmphasis,
  },
  warning: {
    main: orange[300],
    dark: orange[400],
    light: orange[100],
    contrastText: darkContrastColor.highEmphasis,
  },
  success: {
    main: green[200],
    dark: green[400],
    light: green[100],
    contrastText: darkContrastColor.highEmphasis,
  },
  divider: lightContrastColor.faded,
  other: {
    outlineBorder: lightContrastColor.faded,
    standardFieldLine: darkContrastColor.mediumEmphasis,
    snackbar: lightContrastColor.highEmphasis,
    avatarBackground: neutral[600],
  },
  action: {
    active: lightContrastColor.highEmphasis,
    hover: lightContrastColor.highlighted,
    selected: lightContrastColor.highlighted,
    disabled: lightContrastColor.highlighted,
    disabledBackground: lightContrastColor.highlighted,
    focus: lightContrastColor.faded,
  },
};
