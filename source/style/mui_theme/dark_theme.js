// :copyright: Copyright (c) 2021 ftrack

import { darkThemePalette } from "./dark_theme_palette";
import { createTheme } from "./create_theme";

const darkTheme = createTheme(darkThemePalette);

export default darkTheme;
