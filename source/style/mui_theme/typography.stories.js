// :copyright: Copyright (c) 2021 ftrack
import { Box, Typography } from "@mui/material";

export default {
  title: "MUI / Typography",
};

export const TypographyDemo = () => (
  <Box>
    <Box
      sx={{
        p: 4,
        minHeight: "100vh",
        maxWidth: 400,
        float: "left",
        border: "1px solid",
      }}
    >
      <Typography variant="overline">Veniam</Typography>
      <Typography variant="h5" noWrap gutterBottom>
        Sit laboris
      </Typography>
      <Typography variant="subtitle1" paragraph>
        Veniam commodo ad duis fugiat sunt quis est magna ad do. Sit laboris
        voluptate nulla aliquip qui.
      </Typography>
      <Typography variant="body1" paragraph>
        Veniam commodo ad duis fugiat sunt quis est magna ad do. Sit laboris
        voluptate nulla aliquip qui. Lorem qui deserunt non excepteur et
        adipisicing voluptate non in exercitation.
      </Typography>
      <Typography variant="body1" paragraph>
        Incididunt eu consequat nostrud in officia. Adipisicing aliqua
        exercitation reprehenderit ipsum fugiat sint culpa cupidatat qui
        consequat Lorem.
      </Typography>
      <Typography variant="subtitle2" paragraph>
        Veniam commodo ad duis fugiat sunt quis est magna ad do. Sit laboris
        voluptate nulla aliquip qui.
      </Typography>
      <Typography variant="body2" paragraph>
        Veniam commodo ad duis fugiat sunt quis est magna ad do. Sit laboris
        voluptate nulla aliquip qui. Lorem qui deserunt non excepteur et
        adipisicing voluptate non in exercitation.
      </Typography>
      <Typography variant="body2" paragraph>
        Incididunt eu consequat nostrud in officia. Adipisicing aliqua
        exercitation reprehenderit ipsum fugiat sint culpa cupidatat qui
        consequat Lorem.
      </Typography>
    </Box>
    <Box
      sx={{
        p: 4,
        minHeight: "100vh",
        float: "left",
        border: "1px solid",
      }}
    >
      <Typography paragraph variant="h1">
        Heading 1
      </Typography>
      <Typography paragraph variant="h2">
        Heading 2
      </Typography>
      <Typography paragraph variant="h3">
        Heading 3
      </Typography>
      <Typography paragraph variant="h4">
        Heading 4
      </Typography>
      <Typography paragraph variant="h5">
        Heading 5
      </Typography>
      <Typography paragraph variant="h6">
        Heading 6
      </Typography>
      <Typography paragraph variant="subtitle1">
        subtitle1
      </Typography>
      <Typography paragraph variant="subtitle2">
        subtitle2
      </Typography>
      <Typography paragraph variant="body1">
        body1
      </Typography>
      <Typography paragraph variant="body2">
        body2
      </Typography>
      <Typography paragraph variant="button">
        BUTTON TEXT
      </Typography>
      <Typography paragraph variant="caption">
        caption text
      </Typography>
      <Typography paragraph variant="overline">
        OVERLINE TEXT
      </Typography>
    </Box>
  </Box>
);
