// :copyright: Copyright (c) 2021 ftrack
import { Box, Typography } from "@mui/material";
import { getForegroundColor } from "../../component/util/color.js";

import * as data from "../token/design_tokens.json";
const { colors } = data.default;

export default {
  title: "MUI / Color palette",
};

function ColorBox({ color, shade }) {
  return (
    <Box
      sx={{
        display: "inline-flex",
        backgroundColor: color[shade],
        color: getForegroundColor(color[shade]),
        border: "1px solid",
        borderColor: "divider",
        borderRadius: 2,
        width: (theme) => theme.spacing(5),
        height: (theme) => theme.spacing(5),
        mr: 0.5,
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      {Number.isInteger(+shade) ? shade : ""}
    </Box>
  );
}

export const ColorDemo = () => {
  const colorBoxes = Object.entries(colors).map(([color, shades]) => (
    <Box sx={{ mb: 2 }}>
      <Typography variant="h5">{color}</Typography>
      {Object.keys(shades).map((shade) => (
        <ColorBox key={shade} color={shades} shade={shade} />
      ))}
    </Box>
  ));
  return (
    <Box sx={{ mt: 2 }}>
      <Typography variant="h3" gutterBottom>
        Color palette
      </Typography>
      {colorBoxes}
    </Box>
  );
};
