// :copyright: Copyright (c) 2021 ftrack
import { useState } from "react";
import PropTypes from "prop-types";
import {
  Button,
  Box,
  Dialog,
  DialogActions,
  DialogContentText,
  DialogContent,
  DialogTitle,
  Typography,
  Toolbar,
  Drawer,
  List,
  ListItemButton,
  TextField,
  Card,
  CardContent,
  CardMedia,
  Alert,
  AlertTitle,
  Stack,
  IconButton,
  Menu,
  MenuItem,
  Slider,
} from "@mui/material";

import CircleIcon from "@mui/icons-material/Circle";
import PeopleIcon from "@mui/icons-material/People";
import PlayCircleOutlineIcon from "@mui/icons-material/PlayCircleOutline";
import MoreVertIcon from "@mui/icons-material/MoreVert";

const options = [
  "Shots",
  "Assets",
  "Projects",
  "Sequence",
  "Milestone",
  "User",
];

function MenuTest() {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <IconButton
        aria-label="more"
        id="long-button"
        aria-controls="long-menu"
        aria-expanded={open ? "true" : undefined}
        aria-haspopup="true"
        onClick={handleClick}
        sx={{ width: "100%", ml: "auto", mr: "auto" }}
      >
        <MoreVertIcon size="large" />
      </IconButton>
      <Menu
        id="long-menu"
        MenuListProps={{
          "aria-labelledby": "long-button",
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        PaperProps={{
          style: {
            width: "20",
          },
        }}
      >
        {options.map((option) => (
          <MenuItem
            key={option}
            selected={option === "User"}
            onClick={handleClose}
          >
            {option}
          </MenuItem>
        ))}
      </Menu>
    </div>
  );
}

function ReviewSessionCard({
  reviewName,
  numberOfRso,
  numberOfCollaborators,
  image,
}) {
  return (
    <Card
      sx={{
        width: "260px",
        marginRight: 2,
      }}
    >
      <CardContent
        sx={{
          p: 0,
          ":last-child": {
            pb: 0,
          },
        }}
      >
        <CardMedia component="img" height="144" image={image} />
        <Box sx={{ px: 2, py: "15px" }}>
          <Typography variant="h5" sx={{ pb: "0.5px" }}>
            {reviewName}
          </Typography>
          <Typography variant="subtitle1" color="primary" sx={{ pb: 2 }}>
            {numberOfRso} items
          </Typography>
          <Box sx={{ display: "flex", flexDirection: "column" }}>
            <Typography variant="subtitle2">Open</Typography>
            <Typography variant="subtitle2">
              {numberOfCollaborators} collaborators
            </Typography>
          </Box>
        </Box>
      </CardContent>
    </Card>
  );
}

ReviewSessionCard.propTypes = {
  reviewName: PropTypes.string,
  numberOfRso: PropTypes.number,
  numberOfCollaborators: PropTypes.number,
  image: PropTypes.string,
};

function DialogTest({ open, handleClose }) {
  return (
    <Dialog open={open} onClose={handleClose} fullWidth>
      <DialogTitle>Create a Review</DialogTitle>
      <DialogContent>
        <DialogContentText>
          <TextField fullWidth variant="standard" label="Name" />
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Close</Button>
        <Button onClick={handleClose}>Save</Button>
      </DialogActions>
    </Dialog>
  );
}

DialogTest.propTypes = {
  handleClose: PropTypes.func,
  open: PropTypes.bool,
};

const listOfRsos = [
  {
    reviewName: "Steps, steps, steps",
    numberOfRso: 6,
    numberOfCollaborators: 7,
    image:
      "https://images.unsplash.com/photo-1634502795504-f0f685b62d8e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2064&q=80",
  },
  {
    reviewName: "Travel back in time",
    numberOfRso: 60,
    numberOfCollaborators: 4,
    image:
      "https://images.unsplash.com/photo-1593642632823-8f785ba67e45?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2064&q=80",
  },
];

function ClassicFtrackScreen() {
  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <Box
      sx={{
        display: "flex",
        minHeight: "100vh",
      }}
    >
      <DialogTest handleClose={handleClose} open={open} />
      <Drawer
        sx={{
          width: 64,
          flexShrink: 0,
          "& .MuiDrawer-paper": {
            width: 64,
            boxSizing: "border-box",
            borderRight: "none",
          },
        }}
        variant="permanent"
        anchor="left"
      >
        <Toolbar />
        <List>
          <ListItemButton>
            <CircleIcon
              fontSize="medium"
              sx={{ width: "100%", ml: "auto", mr: "auto" }}
            />
          </ListItemButton>
          <ListItemButton>
            <PeopleIcon
              fontSize="medium"
              sx={{ width: "100%", ml: "auto", mr: "auto" }}
            />
          </ListItemButton>
          <MenuTest />
        </List>
      </Drawer>

      <Box component="main" sx={{ flex: 1 }}>
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            m: 2,
          }}
        >
          <PlayCircleOutlineIcon
            sx={{ mr: 2, verticalAlign: "top" }}
            color="primary"
            fontSize="medium"
          />
          <Typography variant="h5">Reviews</Typography>
        </Box>
        <Box
          sx={{
            display: "flex",
            flexDirection: "rows",
            m: 2,
          }}
        >
          {listOfRsos.map((item) => (
            <ReviewSessionCard
              reviewName={item.reviewName}
              numberOfRso={item.numberOfRso}
              numberOfCollaborators={item.numberOfCollaborators}
              image={item.image}
            />
          ))}
        </Box>
      </Box>

      <Box sx={{ flexShrink: 0, width: (theme) => theme.spacing(50) }}>
        <Stack sx={{ mb: 2 }}>
          <Alert
            sx={{ mb: 2 }}
            onClose={() => {}}
            severity="error"
            variant="outlined"
          >
            <AlertTitle>Error</AlertTitle>
            This is an error alert — <strong>handle it now!</strong>
          </Alert>
          <Alert onClose={() => {}} severity="success" variant="filled">
            <AlertTitle>Success</AlertTitle>
            This is a success alert — <strong>Great success!</strong>
          </Alert>
        </Stack>
        <Button
          onClick={handleOpen}
          variant="outlined"
          size="small"
          color="primary"
        >
          Create review
        </Button>
      </Box>
    </Box>
  );
}

export const MUIComponentPlayground = () => (
  <>
    <Stack
      spacing={2}
      sx={{
        p: 2,
        maxWidth: 300,
        margin: "auto",
        backgroundColor: "background.paper",
      }}
    >
      <IconButton size="small">
        <PeopleIcon fontSize="inherit" />
      </IconButton>
      <IconButton size="medium">
        <PeopleIcon fontSize="inherit" />
      </IconButton>
      <IconButton size="large">
        <PeopleIcon fontSize="inherit" />
      </IconButton>
    </Stack>
    <Stack
      spacing={2}
      sx={{
        p: 2,
        maxWidth: 300,
        margin: "auto",
        backgroundColor: "background.paper",
      }}
    >
      <Typography>No outline</Typography>
      <IconButton size="medium" variant="default">
        <PeopleIcon fontSize="inherit" />
      </IconButton>
    </Stack>
    <Stack
      spacing={2}
      sx={{
        p: 2,
        maxWidth: 300,
        margin: "auto",
        backgroundColor: "background.paper",
      }}
    >
      <Typography>Colored</Typography>
      <IconButton size="medium" color="primary">
        <PeopleIcon fontSize="inherit" />
      </IconButton>
      <IconButton size="medium" color="secondary">
        <PeopleIcon fontSize="inherit" />
      </IconButton>
      <IconButton size="medium" color="error">
        <PeopleIcon fontSize="inherit" />
      </IconButton>
    </Stack>
  </>
);

export default {
  title: "MUI / Examples",
};

export const MaterialUiScreens = () => <ClassicFtrackScreen />;
