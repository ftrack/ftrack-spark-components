..
    :copyright: Copyright (c) 2021 ftrack
    
#######################
ftrack MUI theme
#######################


MUI (Material UI) is a UI library with React components, ready to use in your application.
Read more on https://mui.com/.

This is a guide on how to use the **ftrack styled MUI theme** in your application.
There are two themes to your disposal; a darkTheme and a lightTheme. 



##### Setup #####

Install @mui/material as a dependency to your project. 
Import the theme file to the location where you want to use the ftrack theme for your MUI components. You will need
to add these imports to get everything working:

    import { StyledEngineProvider, ThemeProvider } from "@mui/material/styles";
    import { lightTheme, darkTheme } from "ftrack-spark-components/lib/style/mui_theme";

ThemeProvider has a prop called theme. Wrap the highest level of your application with this ThemeProvider,
then pass either the lightTheme or darkTheme to it.

Use `<StyledEngineProvider injectFirst>` if you rely on styles from other
styling solutions (e.g. CSS Modules) to override MUI styles.

##### Example, light theme #####

    import { Button } from "@mui/material";
    import { ThemeProvider } from "@mui/material/styles";
    import { lightTheme, darkTheme } from "ftack-spark-components/lib/style/mui_theme";

    function MyApp() {
        return (
            <StyledEngineProvider injectFirst>
                <ThemeProvider theme={lightTheme}>
                    <Button color="primary" variant="outlined" size="medium">
                        Click this button
                    </Button>
                </ThemeProvider>
            </StyledEngineProvider>
        )
    }
