// :copyright: Copyright (c) 2021 ftrack

import { createTheme as createMuiTheme } from "@mui/material/styles";

import * as data from "../token/design_tokens.json";
import { muiTypography } from "./typography";

const { fontSize } = data.default.typography;

export const createTheme = (palette) =>
  createMuiTheme({
    palette,
    typography: muiTypography,
    spacing: 8,
    shape: {
      borderRadius: 2,
    },
    components: {
      MuiAvatar: {
        defaultProps: { size: "medium" },
        styleOverrides: {
          root: {
            backgroundColor: palette.other.avatarBackground,
            textAlign: "center",
            display: "flex",
            border: "2px solid transparent",
          },
        },
        variants: [
          {
            props: { size: "small" },
            style: {
              width: "1.8rem",
              height: "1.8rem",
              fontSize: fontSize.xsmall,
              color: palette.text.primary,
            },
          },
          {
            props: { size: "medium" },
            style: {
              width: "3.2rem",
              height: "3.2rem",
              color: palette.text.primary,
            },
          },
          {
            props: { size: "large" },
            style: {
              width: "6.4rem",
              height: "6.4rem",
              fontSize: "4rem",
              color: palette.text.primary,
            },
          },
        ],
      },

      MuiBackdrop: {
        styleOverrides: {
          root: {
            "&:not(.MuiBackdrop-invisible)": {
              backgroundColor: palette.other.backdrop,
            },
          },
        },
      },

      MuiButton: {
        variants: [
          {
            props: { size: "small" },
            style: {
              fontSize: fontSize.small,
            },
          },
          {
            props: { size: "large" },
            style: {
              fontSize: fontSize.large,
            },
          },
          {
            props: { rounded: true, size: "small" },
            style: {
              borderRadius: "1.5rem",
              lineHeight: "2.4rem",
              padding: "0 0.8rem",
            },
          },
        ],
      },

      MuiCard: {
        defaultProps: {
          variant: "outlined", // Default to outlined instead of elevated cards
        },
        variants: [
          {
            props: { variant: "outlined-dashed" },
            style: {
              background: "none",
              borderWidth: "2px",
              borderStyle: "dashed",
              borderColor: palette.divider,
            },
          },
        ],
      },

      MuiCardActionArea: {
        variants: [
          {
            props: { variant: "disabled" },
            style: {
              cursor: "default",
              pointerEvents: "none",
            },
          },
        ],
      },

      MuiCardMedia: {
        variants: [
          {
            props: { variant: "round" },
            style: {
              maxHeight: "12rem",
              maxWidth: "12rem",
              borderRadius: "50%",
              margin: "0 auto",
            },
          },
          {
            props: { variant: "hidden" },
            style: {
              opacity: "0.3",
            },
          },
        ],
      },

      MuiCssBaseline: {
        styleOverrides: {
          body: {
            // Use font-size and line-height from react-toolbox
            fontSize: "1.6rem",
            lineHeight: 1.15,
          },
        },
      },

      MuiDialog: {
        styleOverrides: {
          paper: {
            backgroundImage: "none",
            backgroundColor: palette.background.paper,
          },
        },
      },

      MuiIconButton: {
        defaultProps: {
          "&:not(.MuiIconButton)": {
            variant: "outlined",
          },
        },
        variants: [
          {
            props: { variant: "outlined" },
            style: { border: "1px solid", borderColor: "currentColor" },
          },
          {
            props: { variant: "outlined", color: "default" },
            style: { borderColor: palette.divider },
          },
          {
            props: { size: "small" },
            style: { width: "24px", height: "24px", fontSize: "12px" },
          },
          {
            props: { size: "medium" },
            style: { width: "32px", height: "32px", fontSize: "16px" },
          },
          {
            props: { size: "large" },
            style: { width: "40px", height: "40px", fontSize: "20px" },
          },
        ],
      },
      MuiList: {
        variants: [
          {
            props: { dense: true },
            style: {
              padding: "0.4rem 0",
            },
          },
        ],
      },

      MuiListItemIcon: {
        styleOverrides: {
          root: {
            ".MuiSvgIcon-root": {
              fontSize: "2.0rem",
            },
          },
        },
      },

      MuiMenu: {
        styleOverrides: {
          root: {
            margin: "0.2rem 0",
          },
        },
      },

      MuiMenuItem: {
        variants: [
          {
            props: { dense: true },
            style: {
              minHeight: "2.4rem",
              padding: "0 0.8rem",
              ".MuiListItemIcon-root": {
                minWidth: "2rem",
              },
            },
          },
        ],
      },

      MuiPaper: {
        styleOverrides: {
          elevation: {
            backgroundImage: "unset",
            border: `1px solid ${palette.other.outlineBorder}`,
            backgroundColor: palette.background.raised,
          },
        },
      },

      MuiSlider: {
        defaultProps: {
          size: "small",
        },
        styleOverrides: {
          thumb: {
            outline: `2px solid ${palette.background.paper}`,
            cursor: "grab",
            "&.Mui-active": {
              cursor: "grabbing",
            },
          },
        },
      },

      MuiTooltip: {
        styleOverrides: {
          tooltip: {
            backgroundColor: "#000",
            fontSize: fontSize.xsmall,
          },
        },
      },
    },
  });
