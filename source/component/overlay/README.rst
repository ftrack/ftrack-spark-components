..
    :copyright: Copyright (c) 2016 ftrack

#######
Overlay
#######

The overlay component can be used to display a message, error or loading
indicator.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
className                      CSS class name for Link
active             false       Overlay is shown when active
fixed              false       Display overlay using position fixed
loader             false       Add loading spinner
progress           null        Optional progress for loader
title              null        Title
message            null        Message
details            null        Reveable details, e.g. error message
dismissable        false       True to display a dismss button
onDismss           null        Callback when dismiss is clicked
dismissLabel       "Close"     Dismiss button label
