// :copyright: Copyright (c) 2016 ftrack

import PropTypes from "prop-types";

import ProgressBar from "react-toolbox/lib/progress_bar";
import Button from "react-toolbox/lib/button";
import classNames from "classnames";
import FontIcon from "react-toolbox/lib/font_icon";

import Reveal from "../reveal";

import style from "./style.scss";

/**
 * Overlay component.
 *
 * Displays a full-screen overlay which can be customized with various props.
 */
function Overlay(props) {
  const {
    className,
    active,
    fixed,
    icon,
    loader,
    progress,
    title,
    message,
    details,
    dismissable,
    onDismss,
    dismissLabel,
  } = props;

  const _classNames = classNames(
    style.outer,
    {
      [style.active]: active,
      [style.fixed]: fixed,
    },
    className
  );

  const children = [];

  if (icon) {
    const iconElement =
      typeof icon === "string" ? (
        <FontIcon value={icon} className={style.icon} />
      ) : (
        icon
      );
    children.push(iconElement);
  }

  if (loader) {
    const mode = progress === null ? "indeterminate" : "determinate";
    children.push(
      <ProgressBar
        className={style.loader}
        key="loader"
        type="circular"
        mode={mode}
        value={progress}
      />
    );
  }

  if (title) {
    children.push(<h3 key="title">{title}</h3>);
  }

  if (message) {
    children.push(
      <h6 key="message" className={style.message}>
        {message}
      </h6>
    );
  }

  if (details) {
    children.push(
      <div key="details" className={style.details}>
        <Reveal label="Details">{details}</Reveal>
      </div>
    );
  }

  if (dismissable) {
    children.push(
      <Button
        key="dismiss-button"
        label={dismissLabel}
        onClick={onDismss}
        raised
      />
    );
  }

  return (
    <div className={_classNames}>
      <div className={style.inner}>
        {children}
        {props.children}
      </div>
    </div>
  );
}

Overlay.propTypes = {
  active: PropTypes.bool,
  children: PropTypes.node,
  className: PropTypes.string,
  details: PropTypes.node,
  dismissable: PropTypes.bool,
  dismissLabel: PropTypes.node,
  fixed: PropTypes.bool,
  icon: PropTypes.node,
  loader: PropTypes.bool,
  message: PropTypes.node,
  onDismss: PropTypes.func,
  progress: PropTypes.number,
  title: PropTypes.node,
};

Overlay.defaultProps = {
  className: "",
  active: false,
  fixed: false,
  loader: false,
  progress: null,
  title: null,
  message: null,
  details: null,
  dismissable: false,
  onDismss: null,
  dismissLabel: "Close",
};

export default Overlay;
