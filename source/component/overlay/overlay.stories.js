import PropTypes from "prop-types";
import { cloneElement, Component } from "react";
import Button from "react-toolbox/lib/button";

import Overlay from ".";

export default {
  title: "Overlay",
};

class OverlayTest extends Component {
  constructor() {
    super();
    this.state = {
      active: false,
    };
    this.handleToggle = this.handleToggle.bind(this);
  }

  handleToggle() {
    this.setState({ active: !this.state.active });
  }

  render() {
    return (
      <div
        style={{
          minHeight: "500px",
          border: "1px solid hotpink",
        }}
      >
        {cloneElement(this.props.children, {
          active: this.state.active,
          onDismss: this.handleToggle,
        })}

        <div style={{ margin: "auto", padding: "20px" }}>
          <Button label="Toggle overlay" onClick={this.handleToggle} />
          <p>
            Lorem ipsum Occaecat exercitation adipisicing adipisicing ut eu ut
            in dolore elit mollit in Duis qui incididunt dolore laborum ex
            laboris minim sunt mollit sed sunt pariatur ut id laborum occaecat
            officia id et exercitation exercitation laboris cillum mollit aute
            ea proident sunt tempor sit dolor adipisicing nulla enim anim
            ullamco Duis pariatur esse id in non deserunt sunt dolore in
            consectetur adipisicing tempor eu Ut do minim fugiat aute in est
            dolor magna ex laborum anim consectetur proident do non et sit aute
            Duis quis consectetur ea cillum ea Duis eu nostrud minim ad eiusmod
            laborum fugiat Ut fugiat irure et laboris eiusmod in fugiat nisi non
            dolor ut in occaecat deserunt Excepteur id mollit aliquip elit
            exercitation sunt pariatur ut eiusmod ea adipisicing reprehenderit
            dolor Excepteur in eiusmod aute culpa commodo ad reprehenderit nisi
            ea Duis nostrud sed velit ex reprehenderit dolore amet mollit minim
            aliqua labore ut consequat adipisicing pariatur nulla dolore laboris
            veniam consequat nulla est proident aliqua reprehenderit qui ut eu
            dolor magna aliquip deserunt consequat fugiat reprehenderit dolore
            magna esse tempor labore Excepteur cupidatat dolore dolore nostrud
            officia Excepteur aliquip culpa exercitation ut laboris.
          </p>
        </div>
      </div>
    );
  }
}

OverlayTest.propTypes = { children: PropTypes.node };

export const TogglableOverlay = () => (
  <OverlayTest>
    <Overlay
      title="Togglable overlay"
      message="This is a test overlay, content goes here"
      icon="info"
      dismissable
    />
  </OverlayTest>
);

TogglableOverlay.storyName = "Togglable overlay";

TogglableOverlay.parameters = {
  info: `This example shows a toggleable overlay. Once active the overlay will
display a blocking overlay with a message which can be dissmissed.`,
};

export const LoadingOverlay = () => (
  <div style={{ height: "600px", border: "1px solid grey" }}>
    <Overlay title="Loading..." active loader />
  </div>
);

LoadingOverlay.storyName = "Loading overlay";

LoadingOverlay.parameters = {
  info: `This example shows a loading overlay. Specify the *loader* prop to add
a spinner. You can show progress by specifying *progress* as a value between
0 and 100, or leave it out for an indeterminate progress indicator.`,
};

export const IconOverlay = () => (
  <div style={{ height: "600px", border: "1px solid grey" }}>
    <Overlay
      title="Icon overlay"
      message="This is a test overlay, content goes here"
      active
      icon="warning"
    />
  </div>
);

IconOverlay.storyName = "Icon overlay";

IconOverlay.parameters = {
  info: `This example shows a loading overlay. Specify the *loader* prop to add
a spinner. You can show progress by specifying *progress* as a value between
0 and 100, or leave it out for an indeterminate progress indicator.`,
};

export const RevealableDetails = () => (
  <div style={{ height: "600px", border: "1px solid grey" }}>
    <Overlay
      title="Test error"
      active
      message="An example error occurred"
      details="ParseError: Failed to parse: select id..."
    />
  </div>
);

RevealableDetails.storyName = "Revealable details";

RevealableDetails.parameters = {
  info: "You can specify *details* to add reveable details.",
};
