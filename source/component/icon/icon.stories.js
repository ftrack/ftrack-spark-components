// :copyright: Copyright (c) 2017 ftrack
import centered from "@storybook/addon-centered/react";
import { Box, IconButton, Stack, Typography } from "@mui/material";

import { ICONS } from "./icons_mapping";

import { EntityTypeIcon } from ".";
import { ObjectTypeIcon } from ".";

export default {
  title: "Icon",
  decorators: [centered],
};

const SizeDebug = ({ sx, ...props }) => {
  return (
    <Box
      sx={[
        {
          display: "inline-block",
          verticalAlign: "middle",
          border: 1,
          borderColor: "text.primary",
          marginRight: 0.5,
          backgroundImage: ({ palette }) =>
            `repeating-linear-gradient(90deg, transparent, transparent 4px, ${palette.divider} 4px, ${palette.divider} 5px), repeating-linear-gradient(0deg, transparent, transparent 4px, ${palette.divider} 4px, ${palette.divider} 5px)`,
        },
        ...(Array.isArray(sx) ? sx : [sx]),
      ]}
      {...props}
    />
  );
};

export const IconSizeComparison = () => (
  <Stack spacing={2}>
    <Box>
      <Typography>Inline-block</Typography>
      <SizeDebug>
        <EntityTypeIcon type="Project" />
      </SizeDebug>
      <SizeDebug>
        <EntityTypeIcon
          type="TypedContext"
          icon="task"
          sx={{ color: "info.main" }}
        />
      </SizeDebug>
      <SizeDebug>
        <EntityTypeIcon type="User" sx={{ color: "warning.dark" }} />
      </SizeDebug>
      <SizeDebug>
        <ObjectTypeIcon icon="audio" sx={{ color: "secondary.dark" }} />
      </SizeDebug>
      <SizeDebug>
        <ObjectTypeIcon icon="favorite" sx={{ color: "error.light" }} />
      </SizeDebug>
      <SizeDebug>
        <ObjectTypeIcon icon="asset_build" sx={{ color: "neutral.dark" }} />
      </SizeDebug>
      <SizeDebug>
        <ObjectTypeIcon icon="milestone" sx={{ color: "primary.main" }} />
      </SizeDebug>
    </Box>
    <Box>
      <Typography>Inline</Typography>
      <SizeDebug>
        <Typography sx={{ verticalAlign: "middle" }}>
          <EntityTypeIcon type="Project" />
          Sample text
        </Typography>
      </SizeDebug>
      <SizeDebug>
        <Typography sx={{ verticalAlign: "middle" }}>
          <ObjectTypeIcon icon="milestone" />
          Sample text
        </Typography>
      </SizeDebug>
    </Box>
    <Box>
      <Typography>Icon button</Typography>
      <SizeDebug>
        <IconButton variant="outlined" size="small">
          <ObjectTypeIcon icon="favorite" fontSize="inherit" />
        </IconButton>
      </SizeDebug>
      <SizeDebug>
        <IconButton variant="outlined" size="small">
          <ObjectTypeIcon icon="milestone" fontSize="inherit" />
        </IconButton>
      </SizeDebug>
      <SizeDebug>
        <IconButton variant="outlined" size="medium">
          <ObjectTypeIcon icon="favorite" fontSize="inherit" />
        </IconButton>
      </SizeDebug>
      <SizeDebug>
        <IconButton variant="outlined" size="medium">
          <ObjectTypeIcon icon="milestone" fontSize="inherit" />
        </IconButton>
      </SizeDebug>
      <SizeDebug>
        <IconButton variant="outlined" size="large">
          <ObjectTypeIcon icon="favorite" fontSize="inherit" />
        </IconButton>
      </SizeDebug>
      <SizeDebug>
        <IconButton variant="outlined" size="large">
          <ObjectTypeIcon icon="milestone" fontSize="inherit" />
        </IconButton>
      </SizeDebug>
    </Box>
    <Box>
      <Typography>Custom sizes</Typography>
      <SizeDebug>
        <ObjectTypeIcon icon="favorite" sx={{ fontSize: "16px" }} />
      </SizeDebug>
      <SizeDebug>
        <ObjectTypeIcon icon="milestone" sx={{ fontSize: "16px" }} />
      </SizeDebug>
      <SizeDebug>
        <ObjectTypeIcon icon="favorite" sx={{ fontSize: "20px" }} />
      </SizeDebug>
      <SizeDebug>
        <ObjectTypeIcon icon="milestone" sx={{ fontSize: "20px" }} />
      </SizeDebug>
      <SizeDebug>
        <ObjectTypeIcon icon="favorite" sx={{ fontSize: "24px" }} />
      </SizeDebug>
      <SizeDebug>
        <ObjectTypeIcon icon="milestone" sx={{ fontSize: "24px" }} />
      </SizeDebug>
      <SizeDebug>
        <ObjectTypeIcon icon="favorite" sx={{ fontSize: "32px" }} />
      </SizeDebug>
      <SizeDebug>
        <ObjectTypeIcon icon="milestone" sx={{ fontSize: "32px" }} />
      </SizeDebug>
      <SizeDebug>
        <ObjectTypeIcon icon="favorite" sx={{ fontSize: "42px" }} />
      </SizeDebug>
      <SizeDebug>
        <ObjectTypeIcon icon="milestone" sx={{ fontSize: "42px" }} />
      </SizeDebug>
    </Box>
  </Stack>
);

export const ObjectTypeIcons = () => (
  <Stack spacing={1}>
    {Object.entries(ICONS)
      .sort(([name1], [name2]) => name1.localeCompare(name2))
      .map(([name, ObjectIcon]) => (
        <Typography sx={{ verticalAlign: "middle" }}>
          <SizeDebug>
            <ObjectIcon />
          </SizeDebug>
          {name}
        </Typography>
      ))}
  </Stack>
);
