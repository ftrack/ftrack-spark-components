// :copyright: Copyright (c) 2022 ftrack

export { default as ObjectTypeIcon } from "./object_type_icon";
export { default as EntityTypeIcon } from "./entity_type_icon";
