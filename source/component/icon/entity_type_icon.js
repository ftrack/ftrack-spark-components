// :copyright: Copyright (c) 2022 ftrack

import ObjectTypeIcon from "./object_type_icon";

function EntityTypeIcon({ type, icon, ...props }) {
  let defaultIcon;

  switch (type) {
    case "Project":
      defaultIcon = "project";
      break;
    case "User":
      defaultIcon = "user";
      break;
    case "Group":
      defaultIcon = "group";
      break;
    case "AssetVersion":
      defaultIcon = "asset_version";
      break;
    case "List":
      defaultIcon = "list";
      break;
    case "Asset":
      defaultIcon = "asset";
      break;
    default:
      defaultIcon = icon;
      break;
  }
  return <ObjectTypeIcon icon={defaultIcon} {...props}></ObjectTypeIcon>;
}

export default EntityTypeIcon;
