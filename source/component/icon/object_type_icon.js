// :copyright: Copyright (c) 2022 ftrack

import { ICONS } from "./icons_mapping";
import { Folder } from "@mui/icons-material";

function ObjectTypeIcon({ icon, ...props }) {
  const IconComponent = ICONS[icon] || Folder;
  return <IconComponent {...props} />;
}

export default ObjectTypeIcon;
