// :copyright: Copyright (c) 2018 ftrack

import PropTypes from "prop-types";

import { Component } from "react";

import Input from "react-toolbox/lib/input";
import Dropdown from "react-toolbox/lib/dropdown";
import Switch from "react-toolbox/lib/switch";
import { Box, MenuItem, Button } from "@mui/material";
import { defineMessages, intlShape, FormattedMessage } from "react-intl";
import { compose, withHandlers } from "recompose";
import classNames from "classnames";

import Header from "../heading";
import ButtonMenu from "../button_menu";
import DateFilter from "./date_filter";
import DropdownFilter from "./dropdown_filter";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import IconButton from "../icon_button";
import CustomAttributeLinksFilter from "./custom_attribute_links_filter";
import { AttributeLabel, AttributeGroupLabel } from "../dataview/attribute";
import { attributeShape } from "../dataview/util";

import style from "./style.scss";
import switchTheme from "./switch_theme.scss";
import dropdownTheme from "./dropdown_theme.scss";

const messages = defineMessages({
  "value-true": {
    defaultMessage: "Yes",
    description: "Boolean filter value true",
    id: "ftrack-spark-components.filter.boolean-filter.value-true",
  },
  "value-false": {
    defaultMessage: "No",
    description: "Boolean filter value false",
    id: "ftrack-spark-components.filter.boolean-filter.value-false",
  },
  "operator-is_less": {
    defaultMessage: "Less than",
    description: 'filter operator "<"',
    id: "ftrack-spark-components.filter.operator-less-than",
  },
  "operator-is_greater": {
    defaultMessage: "Greater than",
    description: 'filter operator ">"',
    id: "ftrack-spark-components.filter.operator-greater-than",
  },
  "operator-is_less_or_equal": {
    defaultMessage: "Less than or equal",
    description: 'filter operator "<="',
    id: "ftrack-spark-components.filter.operator-less-than-or-equal",
  },
  "operator-is_greater_or_equal": {
    defaultMessage: "Greater than or equal",
    description: 'filter operator ">="',
    id: "ftrack-spark-components.filter.operator-greater-than-or-equal",
  },
  "operator-is": {
    defaultMessage: "Is",
    description: 'filter operator "="',
    id: "ftrack-spark-components.filter.operator-is",
  },
  "operator-is_not": {
    defaultMessage: "Is not",
    description: 'filter operator "!="',
    id: "ftrack-spark-components.filter.operator-is-not",
  },
  "operator-in": {
    defaultMessage: "Includes",
    description: 'filter operator "in"',
    id: "ftrack-spark-components.filter.operator-in",
  },
  "operator-not_in": {
    defaultMessage: "Does not include",
    description: 'filter operator "not_in"',
    id: "ftrack-spark-components.filter.operator-not-in",
  },
  "operator-set": {
    defaultMessage: "Is set",
    description: 'filter operator "set"',
    id: "ftrack-spark-components.filter.operator-set",
  },
  "operator-not_set": {
    defaultMessage: "Is not set",
    description: 'filter operator "not_set"',
    id: "ftrack-spark-components.filter.operator-not-set",
  },
  "operator-contains": {
    defaultMessage: "Contains",
    description: 'filter operator "contains"',
    id: "ftrack-spark-components.filter.operator-contains",
  },
  "operator-not_contains": {
    defaultMessage: "Does not contain",
    description: 'filter operator "not_contains"',
    id: "ftrack-spark-components.filter.operator-not-contains",
  },
  "operator-starts_with": {
    defaultMessage: "Starts with",
    description: 'filter operator "starts_with"',
    id: "ftrack-spark-components.filter.operator-starts-with",
  },
  "operator-ends_with": {
    defaultMessage: "Ends with",
    description: 'filter operator "ends_with"',
    id: "ftrack-spark-components.filter.operator-ends-with",
  },
  "filter-match-any-label": {
    defaultMessage: "Match any filter",
    description: "Match any filter switch label",
    id: "ftrack-spark-components.filter.filter-match-any-label",
  },
  "remove-filter": {
    defaultMessage: "Remove",
    description: "Remove filter button",
    id: "ftrack-spark-components.filter.remove-filter",
  },
  "invert-filter": {
    defaultMessage: "Invert",
    description: "Invert filter button",
    id: "ftrack-spark-components.filter.invert-filter",
  },
  "disable-filter": {
    defaultMessage: "Disable",
    description: "Disable filter button",
    id: "ftrack-spark-components.filter.disable-filter",
  },
  "unknown-attribute": {
    defaultMessage: "Unknown filter",
    description: "Unknown filter label",
    id: "ftrack-spark-components.filter.unknown-attribute",
  },
});

function UnknownFilterField_({ onRemoveFilter, intl }) {
  return (
    <div className={style["filter-field"]}>
      <div className={style["filter-field-top"]} key="filter-top">
        <div className={style["filter-header-operator"]}>
          <Header variant="label" color="secondary">
            {intl.formatMessage(messages["unknown-attribute"])}
          </Header>
        </div>
        <div className={style["filter-field-control-buttons"]}>
          <IconButton
            tooltip={intl.formatMessage(messages["remove-filter"])}
            className={style["visible-on-hover"]}
            icon="clear"
            onClick={onRemoveFilter}
          />
        </div>
      </div>
    </div>
  );
}
UnknownFilterField_.propTypes = {
  intl: intlShape.isRequired,
  onRemoveFilter: PropTypes.func.isRequired,
};
const UnknownFilterField = compose(
  safeInjectIntl,
  withHandlers({
    onRemoveFilter: (props) => () => props.onRemoveFilter(props.index),
  })
)(UnknownFilterField_);
function FilterField_({
  attribute,
  value,
  operator,
  index,
  invert,
  disable,
  intl,
  onValueChange,
  onOperatorChange,
  onRemoveFilter,
  onDisableChange,
  onInvertChange,
  enableInvert,
  disableRemove,
}) {
  const { type, filterOperators: operators } = attribute;

  let FilterComponent = UnknownFilterField;
  let filterProps;
  if (attribute.filterComponent) {
    FilterComponent = attribute.filterComponent;
  } else if (type === "string") {
    FilterComponent = Input;
    filterProps = {
      className: style["field-field-standard-input"],
    };
  } else if (type === "number" || type === "integer") {
    FilterComponent = Input;
    filterProps = {
      type: "number",
      className: style["field-field-standard-input"],
    };
  } else if (type === "boolean") {
    FilterComponent = Dropdown;
    filterProps = {
      theme: dropdownTheme,
      auto: true,
      source: [
        {
          label: intl.formatMessage(messages["value-true"]),
          value: true,
        },
        {
          label: intl.formatMessage(messages["value-false"]),
          value: false,
        },
      ],
    };
  } else if (type === "date-time") {
    FilterComponent = DateFilter;
  } else if (type === "enumerator") {
    let options;
    try {
      options = JSON.parse(attribute.config.data);
    } catch (err) {
      options = [];
    }
    FilterComponent = DropdownFilter;
    filterProps = {
      theme: dropdownTheme,
      items: options.map((item) => ({
        value: item.value,
        label: item.menu,
      })),
      multiSelect: true,
    };
  } else if (type === "link") {
    FilterComponent = CustomAttributeLinksFilter;
    filterProps = {
      className: style["field-field-standard-input"],
      entityType: attribute.options.entityType,
      objectTypeId: attribute.options.objectTypeId,
      objectTypeName: attribute.options.objectTypeName,
      baseFilter: attribute.options.baseFilter,
    };
  }

  return (
    <div className={style["filter-field"]}>
      <div className={style["filter-field-top"]} key="filter-top">
        <div className={style["filter-header-operator"]}>
          <Header variant="label" color="secondary" noWrap>
            {(attribute.path && attribute.path.length) ||
            attribute.groupLabel ||
            attribute.key.endsWith("$entities") ? (
              <Box
                component="span"
                sx={{
                  mr: !attribute.key.endsWith("$entities") ? 0.5 : 0,
                  textTransform: "capitalize",
                }}
              >
                <AttributeGroupLabel
                  path={attribute.path || []}
                  label={attribute.groupLabel}
                />
              </Box>
            ) : null}
            {!attribute.key.endsWith("$entities") && (
              <AttributeLabel attribute={attribute} />
            )}
          </Header>
          {operators && operators.length ? (
            <ButtonMenu
              button={
                <Button
                  sx={{
                    minWidth: 0,
                    fontSize: "fontSizeSmall",
                    textTransform: "lowercase",
                    fontWeight: "fontWeightThin",
                    color: "text.secondary",
                    height: ({ spacing }) => spacing(2),
                    lineHeight: ({ spacing }) => spacing(2),
                    px: 0.5,
                  }}
                >
                  {(messages[`operator-${operator}`] &&
                    intl.formatMessage(messages[`operator-${operator}`])) ||
                    operator}
                </Button>
              }
              onClick={onOperatorChange}
            >
              {operators.map((item) => (
                <MenuItem key={item}>
                  {(messages[`operator-${item}`] &&
                    intl.formatMessage(messages[`operator-${item}`])) ||
                    item}
                </MenuItem>
              ))}
            </ButtonMenu>
          ) : null}
          {attribute.filterMessage ? (
            <span
              className={
                (style["filter-field-group-label"], style.colorAccent2)
              }
            >
              *
            </span>
          ) : null}
        </div>
        <div className={style["filter-field-control-buttons"]}>
          <IconButton
            tooltip={intl.formatMessage(messages["disable-filter"])}
            className={disable ? "" : style["visible-on-hover"]}
            icon="pause"
            onClick={onDisableChange}
          />
          {enableInvert ? (
            <IconButton
              tooltip={intl.formatMessage(messages["invert-filter"])}
              className={invert ? "" : style["visible-on-hover"]}
              icon="exposure"
              onClick={onInvertChange}
            />
          ) : null}
          {!disableRemove && (
            <IconButton
              tooltip={intl.formatMessage(messages["remove-filter"])}
              className={style["visible-on-hover"]}
              icon="clear"
              onClick={onRemoveFilter}
            />
          )}
        </div>
      </div>

      {operator !== "set" && operator !== "not_set" ? (
        <FilterComponent
          key={`filter-${attribute.key}-${index}`}
          onChange={onValueChange}
          value={value}
          {...filterProps}
        />
      ) : null}
      {attribute.filterMessage ? (
        <div
          className={classNames(
            style["filter-field-group-label"],
            style.colorAccent2
          )}
        >
          {"* "}
          <AttributeGroupLabel
            label={intl.formatMessage(attribute.filterMessage)}
          />
        </div>
      ) : null}
    </div>
  );
}
FilterField_.propTypes = {
  attribute: attributeShape.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool,
    PropTypes.arrayOf(PropTypes.string),
  ]),
  enableInvert: PropTypes.bool,
  disableRemove: PropTypes.bool,
  operator: PropTypes.string,
  index: PropTypes.number,
  invert: PropTypes.bool,
  disable: PropTypes.bool,
  intl: intlShape.isRequired,
  onValueChange: PropTypes.func.isRequired,
  onOperatorChange: PropTypes.func.isRequired,
  onRemoveFilter: PropTypes.func.isRequired,
  onDisableChange: PropTypes.func.isRequired,
  onInvertChange: PropTypes.func.isRequired,
};
FilterField_.defaultProps = {
  enableInvert: false,
};

const FilterField = compose(
  safeInjectIntl,
  withHandlers({
    onValueChange: (props) => (value) =>
      props.onValueChange(props.index, value),
    onOperatorChange: (props) => (operator) =>
      props.onOperatorChange(props.index, operator),
    onDisableChange: (props) => () =>
      props.onDisableChange(props.index, !props.disable),
    onInvertChange: (props) => () =>
      props.onInvertChange(props.index, !props.invert),
    onRemoveFilter: (props) => () => props.onRemoveFilter(props.index),
  })
)(FilterField_);

class FilterPanel extends Component {
  constructor(props) {
    super(props);

    this.onOperatorChange = this.onOperatorChange.bind(this);
    this.onInvertChange = this.onInvertChange.bind(this);
    this.onDisableChange = this.onDisableChange.bind(this);
    this.onValueChange = this.onValueChange.bind(this);
    this.onRemoveFilter = this.onRemoveFilter.bind(this);
    this.onFilterMatchChange = this.onFilterMatchChange.bind(this);
    this.onItemsUpdate = this.onItemsUpdate.bind(this);
  }

  onValueChange(index, value) {
    const {
      data: { items },
    } = this.props;
    const nextItems = [...items];
    nextItems[index] = Object.assign({}, items[index], { value });
    this.onItemsUpdate(nextItems);
  }

  onOperatorChange(index, operator) {
    const {
      data: { items },
    } = this.props;
    const nextItems = [...items];
    nextItems[index] = Object.assign({}, items[index], { operator });
    this.onItemsUpdate(nextItems);
  }

  onDisableChange(index, disable) {
    const {
      data: { items },
    } = this.props;
    const nextItems = [...items];
    nextItems[index] = Object.assign({}, items[index], {
      disable: disable || undefined,
    });
    this.onItemsUpdate(nextItems);
  }

  onInvertChange(index, invert) {
    const {
      data: { items },
    } = this.props;
    const nextItems = [...items];
    nextItems[index] = Object.assign({}, items[index], {
      invert: invert || undefined,
    });
    this.onItemsUpdate(nextItems);
  }

  onRemoveFilter(removeAtIndex) {
    const {
      data: { items },
    } = this.props;
    this.onItemsUpdate(items.filter((item, index) => index !== removeAtIndex));
  }

  onItemsUpdate(items) {
    const { data, onChange } = this.props;

    onChange(Object.assign({}, data, { items }));
  }

  onFilterMatchChange(match) {
    const { data, onChange } = this.props;

    onChange(Object.assign({}, data, { match }));
  }

  render() {
    const {
      title,
      data: { items, match },
      attributes,
      className,
      disableRemove,
    } = this.props;
    const fields = items.map((item, index) => {
      const attribute = attributes[item.key];
      if (!attribute) {
        return (
          <UnknownFilterField
            key={`${item.key}-${index}`}
            onRemoveFilter={this.onRemoveFilter}
            index={index}
          />
        );
      }

      return (
        <FilterField
          disableRemove={disableRemove}
          key={`${item.key}-${index}`}
          value={item.value}
          operator={item.operator}
          invert={!!item.invert}
          disable={!!item.disable}
          attribute={attribute}
          index={index}
          onOperatorChange={this.onOperatorChange}
          onDisableChange={this.onDisableChange}
          onInvertChange={this.onInvertChange}
          onValueChange={this.onValueChange}
          onRemoveFilter={this.onRemoveFilter}
        />
      );
    });

    const showAnySwitch = fields.length > 1;
    if (title || showAnySwitch) {
      return (
        <div className={className}>
          <hgroup className={style.heading}>
            {showAnySwitch ? (
              <div className={style["filter-any-switch"]}>
                <span className={style["filter-any-switch-label"]}>
                  <FormattedMessage {...messages["filter-match-any-label"]} />
                </span>
                <Switch
                  theme={switchTheme}
                  checked={match === "any"}
                  onChange={() =>
                    this.onFilterMatchChange(match === "any" ? "all" : "any")
                  }
                />
              </div>
            ) : null}
            <Header variant="subheading" color="default">
              {title}
            </Header>
          </hgroup>
          {fields}
        </div>
      );
    }

    return <div className={className}>{fields}</div>;
  }
}

FilterPanel.propTypes = {
  attributes: PropTypes.arrayOf(attributeShape),
  data: PropTypes.shape({
    match: PropTypes.string,
    items: PropTypes.array,
  }).isRequired,
  onChange: PropTypes.func,
  className: PropTypes.string,
  title: PropTypes.string,
  intl: intlShape.isRequired,
  disableRemove: PropTypes.bool,
};

FilterPanel.defaultProps = {
  title: "",
  disableRemove: false,
};

export default safeInjectIntl(FilterPanel);
