/* eslint-disable camelcase */
// :copyright: Copyright (c) 2021 ftrack

import { Fragment, useState, useEffect } from "react";
import PropTypes from "prop-types";
import Chip from "@ftrack/react-toolbox/lib/chip";
import FontIcon from "@ftrack/react-toolbox/lib/font_icon";
import { defineMessages, FormattedMessage } from "react-intl";
import EntityPicker from "../entity_picker";
import { withSession } from "../util/hoc";
import style from "./style.scss";
import { useSessionQuery } from "../util/hooks";
import { Entity } from "../formatter/entityUtils";

const messages = defineMessages({
  "select-entity-type": {
    id: "ftrack-spark-components.filter.select-entity-type",
    defaultMessage: "Select {entityType}...",
  },
});

function CustomAttributeLinksFilterItem({ children, onClick, placeholder }) {
  function handleKeyDown(e) {
    if (e.key === "Enter" || e.key === " ") {
      e.preventDefault();
      onClick();
    }
  }
  return (
    <div
      onClick={onClick}
      onKeyDown={handleKeyDown}
      className={style.customAttributeLinksFilterItem}
      tabIndex="0"
    >
      <span className={style.customAttributeLinksFilterItemChip}>
        {children.length === 0 ? placeholder : children}
      </span>
      <FontIcon value="edit" />
    </div>
  );
}

CustomAttributeLinksFilterItem.propTypes = {
  children: PropTypes.arrayOf(PropTypes.object),
  onClick: PropTypes.func,
  placeholder: PropTypes.string,
};

const query = (entityType, projection, filter) =>
  `
        select ${projection.join(",")}
        from ${entityType}
        ${filter && `where ${filter}`}
    `;

function useFetchLabels(session, ids, entityType) {
  const [labels, setLabels] = useState(ids ? ids.map((v) => ({ id: v })) : []);

  const { isLoading, data } = useSessionQuery({
    session,
    baseQuery: query(
      entityType,
      Entity[entityType].labelProjection,
      `id in(${labels.map(({ id }) => `'${id}'`).join(",")})`
    ),
    config: {
      limit: 25,
      pause: labels.length === 0,
    },
  });

  useEffect(
    () => {
      if (!isLoading && Array.isArray(data)) {
        setLabels(
          data.map((entity) => ({
            id: entity.id,
            label: Entity[entityType].formatter(entity),
          }))
        );
      }
    },
    // TODO: Feel free to fix this eslint error if you got time
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isLoading, data]
  );
  return [labels, setLabels];
}

function CustomAttributeLinksFilter({
  value,
  entityType,
  objectTypeId,
  objectTypeName,
  contextId,
  baseFilter,
  onChange,
  session,
}) {
  const [entityPickerIsOpen, setEntityPickerIsOpen] = useState(false);
  const [labels, setLabels] = useFetchLabels(session, value, entityType);

  function handleClick() {
    setEntityPickerIsOpen(true);
  }
  function handleClose() {
    setEntityPickerIsOpen(false);
  }
  function handleSelect(val) {
    setLabels(val.map((v) => ({ id: v.id, label: v.returnLabel })));
    onChange(val.length > 0 ? val.map((v) => v.id) : null);
    setEntityPickerIsOpen(false);
  }
  const entityTypeName =
    objectTypeName ||
    (Entity[entityType] && Entity[entityType].name) ||
    entityType ||
    "";

  return (
    <Fragment>
      <CustomAttributeLinksFilterItem
        onClick={handleClick}
        placeholder={
          <FormattedMessage
            {...messages["select-entity-type"]}
            values={{
              entityType: (
                <span className={style.entityTypeName}>{entityTypeName}</span>
              ),
            }}
          />
        }
      >
        {labels.map((v) => (
          <Chip key={v.id}>{v.label || "…"}</Chip>
        ))}
      </CustomAttributeLinksFilterItem>
      {entityPickerIsOpen && (
        <EntityPicker
          open
          mode="MULTI"
          contextId={contextId}
          onClose={handleClose}
          entityType={entityType}
          objectTypeId={objectTypeId}
          onSelected={handleSelect}
          selected={labels.map((v) => v.id)}
          baseFilter={baseFilter}
          session={session}
        />
      )}
    </Fragment>
  );
}

CustomAttributeLinksFilter.propTypes = {
  value: PropTypes.arrayOf(PropTypes.string),
  entityType: PropTypes.string,
  objectTypeId: PropTypes.string,
  objectTypeName: PropTypes.string,
  onChange: PropTypes.func,
  baseFilter: PropTypes.string,
  session: PropTypes.func,
  contextId: PropTypes.string,
};

export default withSession(CustomAttributeLinksFilter);
