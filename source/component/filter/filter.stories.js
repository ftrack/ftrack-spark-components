import { Component } from "react";
import { action } from "@storybook/addon-actions";
// eslint-disable-next-line
import centered from "@storybook/addon-centered/react";
import { Provider as ReduxProvider } from "react-redux";
import debounce from "lodash/debounce";
import moment from "moment";

import getSession from "../../story/get_session";
import configureStore from "../../story/configure_store.js";
import { getFilters, getFilterOperators } from "../dataview/util";
import { AttributeList } from "../attribute_browser/component";
import { withSettings, SparkProvider, SettingsProvider } from "../util/hoc";
import FilterPanel from ".";
import QueryFilter, { replaceVariables } from "./query_filter";
import { toQueryExpression } from "./util";
import { DEFAULT_WORKDAY_LENGTH } from "../util/constant";
import { Box } from "@mui/material";

function Provider({ story, store, session }) {
  return (
    <SparkProvider session={session}>
      <ReduxProvider store={store}>{story}</ReduxProvider>
    </SparkProvider>
  );
}

const store = configureStore((state = {}) => state, [], getSession());

export default {
  title: "Filter",
  decorators: [
    (Story) => (
      <Provider story={<Story />} store={store} session={getSession()} />
    ),
    centered,
  ],
};

const filterChanged = action("Filters object");
const filterQuery = action("Filters query");
const filterResult = action("Filters result");

class TestFilterConfigurationPanel_ extends Component {
  constructor(props) {
    super(props);
    this.onFilterAdd = this.onFilterAdd.bind(this);
    this.onFilterChange = this.onFilterChange.bind(this);
    this.logResult = debounce(this.logResult.bind(this), 500);

    this.state = {
      attributeSource: null,
      data: { items: props.initialFilters, match: "all" },
    };
  }

  componentWillMount() {
    const { session, model, relations } = this.props;
    session.initializing
      .then(() => getFilters(model, relations, session))
      .then((attributeSource) => {
        this.setState({
          attributeSource,
        });
        this.logResult();
      });
  }

  onFilterAdd(selected) {
    const {
      data,
      attributeSource: { attributes },
    } = this.state;

    const attribute = attributes[selected];
    const operator = (attribute.filterOperators ||
      getFilterOperators(attribute.type))[0];

    this.setState({
      data: Object.assign({}, data, {
        items: [...data.items, { key: selected, value: null, operator }],
      }),
    });
  }

  onFilterChange(data) {
    this.setState({ data });
    this.logResult();
  }

  logResult() {
    const { data } = this.state;
    const { model, session, isSundayFirstDayOfWeek, workdayLength } =
      this.props;
    const condition = toQueryExpression(
      data,
      this.state.attributeSource.attributes,
      model,
      this.props.session.schemas,
      {
        isSundayFirstDayOfWeek,
        workdayLength,
      }
    );
    filterChanged(JSON.stringify(data));
    filterQuery(condition);

    if (condition.length) {
      session
        .query(`select id from ${model} where ${condition} limit 10`)
        .then(({ data: result }) => filterResult(result));
    } else {
      filterResult("--- no active filters ---");
    }
  }

  render() {
    const { attributeSource, data } = this.state;

    if (!attributeSource) {
      return <div />;
    }

    return (
      <Box
        sx={{
          bgcolor: "background.paper",
          border: 1,
          borderColor: "divider",
          borderRadius: 1,
          display: "flex",
          width: "800px",
          height: "600px",
        }}
      >
        <Box
          sx={{
            width: "300px",
            overflow: "auto",
            p: 2,
          }}
        >
          <AttributeList
            attributeGroups={attributeSource.groups}
            onChange={this.onFilterAdd}
          />
        </Box>
        <Box sx={{ width: "300px", overflow: "auto", p: 2 }}>
          <FilterPanel
            attributes={attributeSource.attributes}
            onChange={this.onFilterChange}
            data={data}
            title={this.props.title}
          />
        </Box>
      </Box>
    );
  }
}

const TestFilterConfigurationPanel = withSettings({
  workdayLength: DEFAULT_WORKDAY_LENGTH,
  isSundayFirstDayOfWeek: false,
})(TestFilterConfigurationPanel_);

const TEST_TASK_RELATIONS = [
  "status",
  "status.state",
  "type",
  "parent",
  "children",
  "outgoing_links.from",
  "outgoing_links.to",
  "project",
  "notes",
  "scopes",
];

export const BasicTypedContext = () => (
  <TestFilterConfigurationPanel
    model="TypedContext"
    relations={TEST_TASK_RELATIONS}
    session={getSession()}
    initialFilters={[]}
  />
);

BasicTypedContext.storyName = "Basic typed context";

BasicTypedContext.parameters = {
  info: "This example shows a basic filters component.",
};

export const BasicTypedContextWithTitle = () => (
  <TestFilterConfigurationPanel
    model="TypedContext"
    relations={TEST_TASK_RELATIONS}
    session={getSession()}
    initialFilters={[]}
    title="Example title"
  />
);

BasicTypedContextWithTitle.storyName = "Basic typed context with title";

BasicTypedContextWithTitle.parameters = {
  info: "This example shows a basic filters component.",
};

export const BasicCalendarEvent = () => (
  <TestFilterConfigurationPanel
    model="CalendarEvent"
    relations={[]}
    session={getSession()}
    initialFilters={[]}
  />
);

BasicCalendarEvent.storyName = "Basic calendar event";

BasicCalendarEvent.parameters = {
  info: "This example shows a basic calendare event filters component.",
};

export const BasicTypedContext10HourWorkday = () => (
  <SettingsProvider settings={{ workdayLength: 10 * 3600 }}>
    <TestFilterConfigurationPanel
      model="TypedContext"
      relations={TEST_TASK_RELATIONS}
      session={getSession()}
      initialFilters={[]}
    />
  </SettingsProvider>
);

BasicTypedContext10HourWorkday.storyName =
  "Basic typed context 10 hour workday";

BasicTypedContext10HourWorkday.parameters = {
  info: "This example shows a basic filters component.",
};

export const BasicAssetVersion = () => (
  <TestFilterConfigurationPanel
    model="AssetVersion"
    relations={["status", "asset", "user", "task", "asset.parent"]}
    session={getSession()}
    initialFilters={[]}
  />
);

BasicAssetVersion.storyName = "Basic asset version";

BasicAssetVersion.parameters = {
  info: "This example shows a basic asset versions filters component.",
};

export const PolymorphicAssetVersion = () => (
  <TestFilterConfigurationPanel
    model="AssetVersion"
    relations={[
      "status",
      "asset",
      "user",
      "task",
      "asset.parent[TypedContext]",
      "asset.parent[TypedContext].status",
      "asset.parent[Project]",
    ]}
    session={getSession()}
    initialFilters={[]}
  />
);

PolymorphicAssetVersion.storyName = "Polymorphic asset version";

PolymorphicAssetVersion.parameters = {
  info: "This example shows a polymorphic asset versions filters component.",
};

export const ShotCustomAttributes = () => (
  <TestFilterConfigurationPanel
    model="Shot"
    relations={["status"]}
    session={getSession()}
    initialFilters={[]}
  />
);

ShotCustomAttributes.storyName = "Shot custom attributes";

ShotCustomAttributes.parameters = {
  info: "This example shows a shot and custom attribute filters component.",
};

export const ProjectCustomAttributes = () => (
  <TestFilterConfigurationPanel
    model="Project"
    relations={[]}
    session={getSession()}
    initialFilters={[]}
  />
);

ProjectCustomAttributes.storyName = "Project custom attributes";

ProjectCustomAttributes.parameters = {
  info: "This example shows a Project and custom attribute filters component.",
};

export const User = () => (
  <TestFilterConfigurationPanel
    model="User"
    relations={[]}
    session={getSession()}
    initialFilters={[]}
  />
);

User.parameters = {
  info: "This example shows a User filters component.",
};

export const TaskWithCustomAttributes = () => (
  <TestFilterConfigurationPanel
    model="Task"
    relations={["parent[TypedContext]"]}
    session={getSession()}
    initialFilters={[
      {
        key: "custom_attribute-a06e903d-7521-477e-900c-1d65f6415b5e",
        value: [
          "59f0963a-15e2-11e1-a5f1-0019bb4983d8",
          "5d32a91e-c4dd-11e1-afff-f23c91df25eb",
        ],
        operator: "in",
      },
    ]}
  />
);

TaskWithCustomAttributes.storyName = "Task (with custom attributes)";

TaskWithCustomAttributes.parameters = {
  info: "This example shows a Task filters component.",
};

export const Project = () => (
  <TestFilterConfigurationPanel
    model="Project"
    relations={[]}
    session={getSession()}
    initialFilters={[]}
  />
);

Project.parameters = {
  info: "This example shows a Project filters component.",
};

export const BasicWeekStartSunday = () => (
  <SettingsProvider settings={{ isSundayFirstDayOfWeek: true }}>
    <TestFilterConfigurationPanel
      model="AssetVersion"
      relations={[]}
      session={getSession()}
      initialFilters={[{ key: "date", value: "null", operator: "is" }]}
    />
  </SettingsProvider>
);

BasicWeekStartSunday.storyName = "Basic week start sunday";

BasicWeekStartSunday.parameters = {
  info: "This example shows a versions filters component where week start is sunday.",
};

export const BasicWeekStartNotSunday = () => (
  <SettingsProvider settings={{ isSundayFirstDayOfWeek: false }}>
    <TestFilterConfigurationPanel
      model="AssetVersion"
      relations={[]}
      session={getSession()}
      initialFilters={[{ key: "date", value: "null", operator: "is" }]}
    />
  </SettingsProvider>
);

BasicWeekStartNotSunday.storyName = "Basic week start not sunday";

BasicWeekStartNotSunday.parameters = {
  info: "This example shows a versions filters component where week start is not sunday.",
};

export const ComplexTask = () => (
  <TestFilterConfigurationPanel
    model="Task"
    relations={TEST_TASK_RELATIONS}
    session={getSession()}
    initialFilters={[
      {
        key: "outgoing_links.from.name",
        value: "<from-name>",
        operator: "is",
      },
      {
        key: "outgoing_links.from.priority_id",
        value: "<from-priority-id>",
        operator: "is",
      },
      {
        key: "status.name",
        value: "<status-name>",
        operator: "is",
      },
      {
        key: "status.state.name",
        value: "<status-state-name>",
        operator: "is",
      },
      {
        key: "status.state.short",
        value: "<status-state-short>",
        operator: "is",
      },
    ]}
  />
);

ComplexTask.storyName = "Complex task";

ComplexTask.parameters = {
  info: "This example shows a complex filters component.",
};

export const AdvancedQueryOnTask = () => <QueryFilter model="Task" />;

AdvancedQueryOnTask.storyName = "Advanced query on task";

AdvancedQueryOnTask.parameters = {
  info: "This example shows a advanced filters component.",
};

const configuration = {
  userId: "<user-id>",
  username: "<username>",
  safeNow: moment(),
};

export const AdvancedQueryReplaceVariables = () => (
  <div>
    {[
      "CURRENT_USER_ID",
      "CURRENT_USERNAME",
      "LAST_MONTH",
      "THIS_MONTH",
      "NEXT_MONTH",
      "LAST_WEEK",
      "THIS_WEEK",
      "NEXT_WEEK",
      "YESTERDAY",
      "TODAY",
      "NOW",
      "TOMORROW",
      "DAYS(10)",
      "DAYS(-10)",
      "DAYS(0)",
      "DAYS(FOO)",
      "DAYS(1.0)",
    ].map((item) => (
      <div>
        {`{${item}}`}
        {replaceVariables(` is replaced with {${item}}`, configuration)}
      </div>
    ))}
  </div>
);

AdvancedQueryReplaceVariables.storyName = "Advanced query replace variables";

AdvancedQueryReplaceVariables.parameters = {
  info: "This example shows variable replacement component.",
};
