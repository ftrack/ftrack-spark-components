// :copyright: Copyright (c) 2016 ftrack
/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/no-multi-comp */

import PropTypes from "prop-types";

import { Component, createElement } from "react";
import Checkbox from "react-toolbox/lib/checkbox";
import { intlShape, defineMessages } from "react-intl";
import { DatePicker } from "../picker";
import moment from "moment";
import isString from "lodash/isString";

import safeInjectIntl from "../util/hoc/safe_inject_intl";
import style from "./style";
import { compose } from "recompose";
import { withSettings } from "../util/hoc";

const messages = defineMessages({
  "mode-this-week": {
    defaultMessage: "This week",
    description: 'Date range mode "this_week"',
    id: "overview.date-range-filter.mode-this-week",
  },
  "mode-this-month": {
    defaultMessage: "This month",
    description: 'Date range mode "this_month"',
    id: "overview.date-range-filter.mode-this-month",
  },
  "mode-custom-range": {
    defaultMessage: "Date range",
    description: 'Date range mode "custom_range"',
    id: "overview.date-range-filter.mode-custom-range",
  },
  start: {
    defaultMessage: "Start",
    description: "Date range input label start",
    id: "overview.date-range-input.start",
  },
  end: {
    defaultMessage: "End",
    description: "Date range input label end",
    id: "overview.date-range-input.end",
  },
  date: {
    defaultMessage: "Date",
    description: "Date range input label date",
    id: "overview.date-range-input.date",
  },
  "mode-today": {
    defaultMessage: "Today",
    description: "Today date",
    id: "overview.date-range-input.mode-today",
  },
  "mode-yesterday": {
    defaultMessage: "Yesterday",
    description: "Yesterday date",
    id: "overview.date-range-input.mode-yesterday",
  },
  "mode-date": {
    defaultMessage: "Date",
    description: "Specific date",
    id: "overview.date-range-input.mode-date",
  },
  "mode-tomorrow": {
    defaultMessage: "Tomorrow",
    description: "Tomorrow date",
    id: "overview.date-range-input.mode-tomorrow",
  },
  "mode-next-week": {
    defaultMessage: "Next week",
    description: 'Date range mode "next_week"',
    id: "overview.date-range-filter.mode-next-week",
  },
  "mode-next-month": {
    defaultMessage: "Next month",
    description: 'Date range mode "next_month"',
    id: "overview.date-range-filter.mode-next-month",
  },
});

function formatDate(date) {
  return moment(date).format("MMM Do, YYYY");
}

class DateRangeInput_ extends Component {
  handleDateChange(field, value) {
    this.props.onChange(
      Object.assign({}, this.props.value, { [field]: value })
    );
  }

  render() {
    const { value = {}, isSundayFirstDayOfWeek } = this.props;
    let valueStart = null;
    let valueEnd = null;
    if (value.start) {
      valueStart = isString(value.start) ? new Date(value.start) : value.start;
    }
    if (value.end) {
      valueEnd = isString(value.end) ? new Date(value.end) : value.end;
    }
    return (
      <div className={style["custom-date-range-wrapper"]}>
        <DatePicker
          label={this.props.intl.formatMessage(messages.start)}
          inputFormat={formatDate}
          onChange={this.handleDateChange.bind(this, "start")}
          value={valueStart}
          sundayFirstDayOfWeek={isSundayFirstDayOfWeek}
          autoOk
        />
        <DatePicker
          label={this.props.intl.formatMessage(messages.end)}
          inputFormat={formatDate}
          onChange={this.handleDateChange.bind(this, "end")}
          value={valueEnd}
          sundayFirstDayOfWeek={isSundayFirstDayOfWeek}
          autoOk
        />
      </div>
    );
  }
}

DateRangeInput_.propTypes = {
  intl: intlShape.isRequired,
  value: PropTypes.string,
  onChange: PropTypes.func,
  isSundayFirstDayOfWeek: PropTypes.bool,
};
DateRangeInput_.defaultProps = {
  isSundayFirstDayOfWeek: false,
};

const DateRangeInput = safeInjectIntl(DateRangeInput_);

class DateInput_ extends Component {
  constructor() {
    super();
    this.handleDateChange = this.handleDateChange.bind(this);
  }

  handleDateChange(value) {
    this.props.onChange(Object.assign({}, this.props.value, { date: value }));
  }

  render() {
    const { value = {}, isSundayFirstDayOfWeek } = this.props;
    let valueDate = null;
    if (value.date) {
      valueDate = isString(value.date) ? new Date(value.date) : value.date;
    }
    return (
      <div className={style["custom-date-range-wrapper"]}>
        <DatePicker
          label={this.props.intl.formatMessage(messages.date)}
          inputFormat={formatDate}
          onChange={this.handleDateChange}
          value={valueDate}
          sundayFirstDayOfWeek={isSundayFirstDayOfWeek}
          autoOk
        />
      </div>
    );
  }
}

DateInput_.propTypes = {
  intl: intlShape.isRequired,
  value: PropTypes.string,
  onChange: PropTypes.func,
  isSundayFirstDayOfWeek: PropTypes.bool,
};
DateInput_.defaultProps = {
  isSundayFirstDayOfWeek: false,
};

const DateInput = safeInjectIntl(DateInput_);

class DateRangeFilter extends Component {
  constructor(props) {
    super(props);
    this.modes = [
      {
        id: "this_month",
        i18n_label: messages["mode-this-month"],
        value: {
          mode: "this_month",
        },
      },
      {
        id: "this_week",
        i18n_label: messages["mode-this-week"],
        value: {
          mode: "this_week",
        },
      },
      {
        id: "yesterday",
        i18n_label: messages["mode-yesterday"],
        value: {
          mode: "yesterday",
        },
      },
      {
        id: "today",
        i18n_label: messages["mode-today"],
        value: {
          mode: "today",
        },
      },
      {
        id: "tomorrow",
        i18n_label: messages["mode-tomorrow"],
        value: {
          mode: "tomorrow",
        },
      },
      {
        id: "next_week",
        i18n_label: messages["mode-next-week"],
        value: {
          mode: "next_week",
        },
      },
      {
        id: "next_month",
        i18n_label: messages["mode-next-month"],
        value: {
          mode: "next_month",
        },
      },
      {
        id: "date",
        i18n_label: messages["mode-date"],
        component: DateInput,
        value: {
          mode: "date",
        },
      },
      {
        id: "custom_range",
        i18n_label: messages["mode-custom-range"],
        component: DateRangeInput,
        value: {
          mode: "custom_range",
        },
      },
    ];

    if (props.modes) {
      this.modes = this.modes.filter(
        (mode) => props.modes.indexOf(mode.id) !== -1
      );
    }
    this.state = {};
  }

  handleModeChange(modeId, modeValue) {
    let nextModeId = null;
    let nextValue = null;
    const currentModeId = (this.props.value && this.props.value.mode) || null;
    if (currentModeId !== modeId && modeValue) {
      nextModeId = modeId;
      nextValue = this.modes.find((item) => item.id === nextModeId).value;
    }
    this.setState({ value: nextValue });
    this.props.onChange(nextValue);
  }

  render() {
    const { isSundayFirstDayOfWeek, value = {}, onChange } = this.props;
    return (
      <div>
        {this.modes.map((mode) => {
          const { id } = mode;
          const currentMode = id === (value && value.mode);
          return (
            <div key={id}>
              <Checkbox
                className={style["filter-checkbox"]}
                checked={currentMode}
                label={this.props.intl.formatMessage(mode.i18n_label)}
                onChange={this.handleModeChange.bind(this, id)}
              />
              {currentMode && mode.component
                ? createElement(mode.component, {
                    value,
                    onChange,
                    isSundayFirstDayOfWeek,
                  })
                : null}
            </div>
          );
        })}
      </div>
    );
  }
}

DateRangeFilter.propTypes = {
  intl: intlShape.isRequired,
  value: PropTypes.object,
  onChange: PropTypes.func,
  modes: PropTypes.arrayOf(PropTypes.string),
  isSundayFirstDayOfWeek: PropTypes.bool,
};

export default compose(
  safeInjectIntl,
  withSettings({
    isSundayFirstDayOfWeek: false,
  })
)(DateRangeFilter);
