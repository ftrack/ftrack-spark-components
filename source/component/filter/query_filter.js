// :copyright: Copyright (c) 2018 ftrack

import PropTypes from "prop-types";

import { Component } from "react";

import debounce from "lodash/debounce";
import { Input } from "react-toolbox/lib/input";
import { compose } from "recompose";
import { intlShape, FormattedMessage, defineMessages } from "react-intl";
import ProgressBar from "react-toolbox/lib/progress_bar";
import moment from "moment";
import loglevel from "loglevel";

import { withSession } from "../util/hoc";
import { ENCODE_DATE_FORMAT, ENCODE_DATETIME_FORMAT } from "../util/constant";
import safeInjectIntl from "../util/hoc/safe_inject_intl";

import style from "./style.scss";
import queryFilterProgressTheme from "./query_filter_progress_theme.scss";
import queryFilterInputTheme from "./query_filter_input_theme.scss";

const logger = loglevel.getLogger("query-filter");

/** Return new expression with repalced variables in *expression*.
 *
 * A configuration must be passed userId, username. Optionally a `safeNow`
 * variable can be passed. This allows for "stable" expression generation.
 *
 */
export function replaceVariables(expression, configuration) {
  const { userId, username, safeNow = moment() } = configuration;
  const replace = {
    "{CURRENT_USER_ID}": () => userId,
    "{CURRENT_USERNAME}": () => username,
    "{LAST_MONTH}": () =>
      moment(safeNow)
        .startOf("month")
        .add(-1, "month")
        .format(ENCODE_DATE_FORMAT),
    "{THIS_MONTH}": () =>
      moment(safeNow).startOf("month").format(ENCODE_DATE_FORMAT),
    "{NEXT_MONTH}": () =>
      moment(safeNow)
        .startOf("month")
        .add(1, "month")
        .format(ENCODE_DATE_FORMAT),
    "{LAST_WEEK}": () =>
      moment(safeNow)
        .startOf("isoWeek")
        .add(-1, "week")
        .format(ENCODE_DATE_FORMAT),
    "{THIS_WEEK}": () =>
      moment(safeNow).startOf("isoWeek").format(ENCODE_DATE_FORMAT),
    "{NEXT_WEEK}": () =>
      moment(safeNow)
        .startOf("isoWeek")
        .add(1, "week")
        .format(ENCODE_DATE_FORMAT),
    "{YESTERDAY}": () =>
      moment(safeNow).startOf("day").add(-1, "days").format(ENCODE_DATE_FORMAT),
    "{TODAY}": () => moment(safeNow).format(ENCODE_DATE_FORMAT),
    "{TOMORROW}": () =>
      moment(safeNow).startOf("day").add(1, "days").format(ENCODE_DATE_FORMAT),
    "{NOW}": () => moment(safeNow).format(ENCODE_DATETIME_FORMAT),
  };
  const daysExpression = (variable) => {
    let offset = 0;
    try {
      offset = parseInt(variable.match(/(-?\d+)/g)[0], 10);
    } catch (error) {
      logger.error("Could not parse days expression", error);
    }

    return moment(safeNow).add(offset, "days").format(ENCODE_DATE_FORMAT);
  };

  const replaceKeysRegexp = new RegExp(
    ["{DAYS\\(\\-?\\d*\\)}", ...Object.keys(replace)].join("|"),
    "g"
  );
  return expression.replace(replaceKeysRegexp, (matched) => {
    if (matched.startsWith("{DAYS(")) {
      return daysExpression(matched);
    }
    return replace[matched]();
  });
}

const messages = defineMessages({
  "api-help": {
    id: "ftrack-spark-components.filter.query-filter.api-ehlp",
    defaultMessage:
      "Filter using an API Query. For syntax and more information see the {helpLink}",
  },
  "api-help-link": {
    id: "ftrack-spark-components.filter.query-filter.api-help-link",
    description: "Link to help page about api queries in filters",
    defaultMessage:
      "http://help.ftrack.com/using-ftrack-studio/advanced/filtering-using-api-like-queries",
  },
  documentation: {
    id: "ftrack-spark-components.filter.query-filter.documentation",
    defaultMessage: "documentation",
  },
});

class QueryFilter extends Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.validateQuery = debounce(this.validateQuery.bind(this), 500);

    this.state = { processing: false, errors: false, value: props.value };
  }

  onChange(value) {
    this.setState({ processing: !!value, errors: false, value });

    if (value) {
      this.validateQuery(value);
    } else if (value === "") {
      this.props.onChange(null);
    }
  }

  validateQuery(value) {
    const { model, session } = this.props;
    session
      .call([
        {
          action: "parse_query",
          expression: `select id from ${model} where ${value}`,
        },
      ])
      .then(([{ data }]) => {
        this.setState({
          errors: data !== true ? data : false,
          processing: false,
        });
        if (data === true) {
          this.props.onChange(value);
        }
      });
  }

  render() {
    const { value, processing, errors } = this.state;
    const { intl } = this.props;

    let hint = null;

    if (!errors && !processing) {
      const helpLink = (
        <a href={intl.formatMessage(messages["api-help-link"])} target="blank">
          {intl.formatMessage(messages.documentation)}
        </a>
      );
      hint = (
        <p className={style["filter-hint"]}>
          <FormattedMessage
            {...messages["api-help"]}
            values={{
              helpLink,
            }}
          />
        </p>
      );
    }

    return (
      <div className={style["query-filter"]}>
        <Input
          multiline
          rows={3}
          value={value}
          onChange={this.onChange}
          theme={queryFilterInputTheme}
          className={style["field-field-standard-input"]}
          error={errors}
        />
        {processing && (
          <ProgressBar
            theme={queryFilterProgressTheme}
            type="linear"
            mode="indeterminate"
          />
        )}
        {hint}
      </div>
    );
  }
}

QueryFilter.propTypes = {
  model: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  intl: intlShape,
  session: PropTypes.shape({
    call: PropTypes.func.isRequired,
  }).isRequired,
};

export default compose(withSession, safeInjectIntl)(QueryFilter);
