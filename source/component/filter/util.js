// :copyright: Copyright (c) 2018 ftrack

import moment from "moment";
import isString from "lodash/isString";
import isArray from "lodash/isArray";
import {
  CUSTOM_ATTRIBUTE_KEY_PREFIX,
  CUSTOM_ATTRIBUTE_LINK_FROM_KEY_PREFIX,
  CUSTOM_ATTRIBUTE_LINK_TO_KEY_PREFIX,
  getPolymorphicRelationDetails,
} from "../dataview/util";
import { ENCODE_DATETIME_FORMAT, ENCODE_DATE_FORMAT } from "../util/constant";
/**
 * Convert *value* to a date range expression for for *configuration.attribute*.
 *
 * value.mode can be one of:
 *     - this_week
 *     - this_month
 *     - custom_range
 *
 * For `custom_range`, specify value.start and value.end as either Date objects
 * or ISO-formatted strings.
 *
 * *isDateTime* if `true`, utc date-time value will be used,
 *  if `false`, date without time will be used (default).
 *
 */
function toDateRangeExpression(
  value,
  attribute,
  operator,
  configuration,
  isDateTime
) {
  let valueStart = null;
  let valueEnd = null;
  let inclusiveEnd = true;

  const { isSundayFirstDayOfWeek = false } = configuration;

  if (!value || !value.mode) {
    return null;
  } else if (value.mode === "custom_range") {
    valueStart = moment(value.start);
    valueEnd = moment(value.end);
  } else if (value.mode === "date") {
    if (!value.date) {
      return null;
    }
    valueStart = moment(value.date);
    if (isDateTime) {
      valueEnd = moment(value.date);
    } else {
      valueEnd = moment(value.date).add(1, "days");
      inclusiveEnd = false;
    }
  } else if (value.mode === "this_week") {
    valueStart = moment().startOf("week");
    valueEnd = moment().endOf("week");

    if (!isSundayFirstDayOfWeek) {
      valueStart.add(1, "days");
      valueEnd.add(1, "days");
    }
  } else if (value.mode === "this_month") {
    valueStart = moment().startOf("month");
    valueEnd = moment().endOf("month");
  } else if (value.mode === "next_month") {
    valueStart = moment().startOf("month").add(1, "months");
    valueEnd = moment().endOf("month").add(1, "months");
  } else if (value.mode === "next_week") {
    valueStart = moment().startOf("week").add(7, "days");
    valueEnd = moment().endOf("week").add(7, "days");

    if (!isSundayFirstDayOfWeek) {
      valueStart.add(1, "days");
      valueEnd.add(1, "days");
    }
  } else if (value.mode === "today") {
    valueStart = moment();
    if (isDateTime) {
      valueEnd = moment();
    } else {
      valueEnd = moment().add(1, "days");
      inclusiveEnd = false;
    }
  } else if (value.mode === "tomorrow") {
    valueStart = moment().add(1, "days");
    if (isDateTime) {
      valueEnd = moment().add(1, "days");
    } else {
      valueEnd = moment().add(2, "days");
      inclusiveEnd = false;
    }
  } else if (value.mode === "yesterday") {
    valueStart = moment().add(-1, "days");
    if (isDateTime) {
      valueEnd = moment().add(-1, "days");
    } else {
      valueEnd = moment();
      inclusiveEnd = false;
    }
  }

  if (!valueStart && !valueEnd) {
    return null;
  }
  if (isString(valueStart)) {
    valueStart = moment(valueStart);
  }
  if (isString(valueEnd)) {
    valueEnd = moment(valueEnd);
  }

  let startExpression;
  if (valueStart) {
    if (isDateTime) {
      const utcStart = moment(valueStart.startOf("day")).utc();
      startExpression = `${attribute} >= "${utcStart.format(
        ENCODE_DATETIME_FORMAT
      )}"`;
    } else {
      startExpression = `${attribute} >= "${valueStart.format(
        ENCODE_DATE_FORMAT
      )}"`;
    }
  }

  let endExpression;
  if (valueEnd) {
    if (isDateTime) {
      const utcEnd = moment(valueEnd.endOf("day")).utc();
      endExpression = `${attribute} <= "${utcEnd.format(
        ENCODE_DATETIME_FORMAT
      )}"`;
    } else {
      endExpression = `${attribute} <${
        inclusiveEnd ? "=" : ""
      } "${valueEnd.format(ENCODE_DATE_FORMAT)}"`;
    }
  }

  let expression;
  if (valueStart && valueEnd) {
    expression = `${startExpression} and ${endExpression}`;
  } else if (valueStart) {
    expression = startExpression;
  } else {
    expression = endExpression;
  }

  if (operator === "is_not") {
    expression = `not (${expression})`;
  }

  return expression;
}

export function getCondition(
  attribute,
  value,
  operator,
  invert,
  configuration,
  isFirstInBranch
) {
  const { key, type, objectTypeId } = attribute;
  const attributeName = key.split(".").pop();
  const apiOperator = {
    is: "is",
    is_not: "is_not",
    is_greater: ">",
    is_less: "<",
    is_greater_or_equal: ">=",
    is_less_or_equal: "<=",
    in: "in",
    not_in: "not_in",
    contains: "like",
    not_contains: "not_like",
    starts_with: "like",
    ends_with: "like",
    set: "set",
    not_set: "not_set",
  }[operator];

  if (attributeName.startsWith(CUSTOM_ATTRIBUTE_KEY_PREFIX)) {
    // Logically, a  custom attribute link filter with `not_in` operator would look like this (notice the `not_in` operator after `to_id`):
    // select name, id from Task
    //    where (project.status is \"active\")
    //       and (custom_attribute_links any (configuration_id is \"b06cb185-0745-4252-9c92-4ae4f59165ad\"
    //             and to_id not_in (\"459e5ab6-4265-11df-80c8-002219661452\")))
    // Unfortunately, due to how custom attribute links are stored (sparsely),
    // this will not include tasks which have no links at all for the given configuration_id
    // Instead, we have to inverse the entire `in` query, as such:
    // select name, id from Task
    //    where (project.status is \"active\")
    //       and not (custom_attribute_links any (configuration_id is \"b06cb185-0745-4252-9c92-4ae4f59165ad\"
    //                 and to_id in (\"459e5ab6-4265-11df-80c8-002219661452\")))

    const linkOperator =
      attribute.type === "link" && apiOperator === "not_in" ? "in" : operator;
    const condition = getCondition(
      { type, key: "value", filterCondition: attribute.filterCondition },
      value,
      linkOperator,
      invert,
      configuration
    );
    const configurationId = attributeName.replace(
      new RegExp(
        `${CUSTOM_ATTRIBUTE_LINK_FROM_KEY_PREFIX}|${CUSTOM_ATTRIBUTE_LINK_TO_KEY_PREFIX}`
      ),
      ""
    );
    const linkPropertyName =
      attribute.options && attribute.options.isIncomingLink
        ? "custom_attribute_links_from"
        : "custom_attribute_links";
    const customAttributePropertyName =
      attribute.type === "link" ? linkPropertyName : "custom_attributes";

    const returnValue = `${customAttributePropertyName} any (configuration_id is "${configurationId}" and ${condition})`;
    if (attribute.type === "link" && apiOperator === "not_in") {
      return `not (${returnValue})`;
    }
    return returnValue;
  }

  let condition;
  if (attribute.filterCondition) {
    condition = attribute.filterCondition(
      apiOperator || operator,
      value,
      configuration
    );
  } else if (type === "string") {
    let apiValue = value;
    if (operator === "contains" || operator === "not_contains") {
      apiValue = `%${apiValue}%`;
    } else if (operator === "starts_with") {
      apiValue = `${apiValue}%`;
    } else if (operator === "ends_with") {
      apiValue = `%${apiValue}`;
    }
    condition = `${attributeName} ${apiOperator} "${apiValue}"`;
  } else if (type === "number" || type === "integer") {
    condition = `${attributeName} ${apiOperator} "${value}"`;
  } else if (type === "date-time") {
    condition = toDateRangeExpression(
      value,
      attributeName,
      operator,
      configuration,
      true
    );
  } else if (type === "enumerator") {
    let apiValue = value;
    if (isArray(apiValue)) {
      apiValue = `("${apiValue.join('", "')}")`;
    } else if (apiValue === null) {
      apiValue = '""';
    }
    condition = `${attributeName} ${apiOperator} ${apiValue}`;
  } else if (type === "boolean") {
    const apiValue = value === true ? "True" : "False";
    condition = `${attributeName} ${apiOperator} ${apiValue}`;
  } else {
    condition = `${attributeName} ${apiOperator} ${value}`;
  }

  if (objectTypeId && isFirstInBranch) {
    condition = `${condition} and object_type_id is "${objectTypeId}"`;
  }

  if (invert) {
    return `not (${condition})`;
  }

  return condition;
}

export function isFilterActive({ disable, operator, value }, type) {
  if (type === "date-time" && value && value.mode === "date" && !value.date) {
    // If mode is date the date must be set for the filter to be active.
    return false;
  }

  if (disable) {
    return false;
  }

  return value !== null || operator === "set" || operator === "not_set";
}

export function toQueryExpression(
  data,
  attributes,
  schemaId,
  schemas,
  configuration = {}
) {
  const activeFilters = data.items.filter((item) => {
    const attribute = attributes[item.key];

    return isFilterActive(item, attribute.type);
  });

  const matchKeyword = data.match === "all" ? "and" : "or";

  const tree = activeFilters.reduce(
    (accumulator, item) => {
      const { key, value, operator, invert } = item;
      const attribute = attributes[key];
      const parts = attribute.relation ? attribute.relation.split(".") : [];
      let fromSchema = schemas.find((schema) => schema.id === schemaId);
      if (parts.length) {
        let traverseItem = accumulator;
        parts.forEach((part, index) => {
          const details = getPolymorphicRelationDetails(part);
          let property;

          if (details) {
            property = fromSchema.properties[details.name];
          } else {
            property = fromSchema.properties[part];
          }

          let relationshipCondition;
          let ref;
          if (property.type === "array") {
            ref = property.items.$ref;
            relationshipCondition = "any";
          } else {
            ref = property.$ref;
            relationshipCondition = "has";
          }
          const traverseBranchKey =
            attribute.objectTypeSchemaId && details
              ? `${details.name}[${attribute.objectTypeSchemaId}]`
              : part;
          if (traverseItem.branch[traverseBranchKey] === undefined) {
            traverseItem.branch[traverseBranchKey] = {
              relationshipCondition,
              branch: {},
              condition: [],
            };
          }
          traverseItem = traverseItem.branch[traverseBranchKey];

          if (index === parts.length - 1) {
            traverseItem.condition.push(
              getCondition(
                attribute,
                value,
                operator,
                invert,
                configuration,
                traverseItem.condition.length === 0
              )
            );
          }

          if (details) {
            fromSchema = schemas.find(
              (schema) => schema.id === details.targetSchemaId
            );
          } else {
            fromSchema = schemas.find((schema) => schema.id === ref);
          }
        });
      } else {
        accumulator.condition.push(
          getCondition(attribute, value, operator, invert, configuration)
        );
      }

      return accumulator;
    },
    {
      branch: {},
      condition: [],
    }
  );

  function getConditionFromBranch({ condition, branch }) {
    let expression = "";
    if (condition.length) {
      expression += `${condition.join(` ${matchKeyword} `)}`;
    }

    const relationshipConditions = Object.entries(branch).map(
      ([branchKey, branchValue]) =>
        `${branchKey} ${
          branchValue.relationshipCondition
        } (${getConditionFromBranch(branchValue)})`
    );

    if (relationshipConditions.length && expression.length) {
      return `${expression} ${matchKeyword} (${relationshipConditions.join(
        ` ${matchKeyword} `
      )})`;
    } else if (relationshipConditions.length) {
      return relationshipConditions.join(` ${matchKeyword} `);
    } else if (expression.length) {
      return expression;
    }

    return [];
  }

  const query = getConditionFromBranch(tree);

  return query;
}
