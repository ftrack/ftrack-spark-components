// :copyright: Copyright (c) 2016 ftrack
import PropTypes from "prop-types";

import Select from "react-select";
import Dropdown from "react-toolbox/lib/dropdown";
import { intlShape } from "react-intl";
import { compose, withHandlers } from "recompose";

import safeInjectIntl from "../util/hoc/safe_inject_intl";

import dropdownTheme from "./dropdown_theme.scss";

function formatOptions(intl, items) {
  const options = (items || []).map((item) => ({
    label: item.i18n_label ? intl.formatMessage(item.i18n_label) : item.label,
    value: item.value,
  }));

  return options;
}

function DropdownFilter({ intl, value, items, multiSelect, onChange }) {
  const options = formatOptions(intl, items);

  if (multiSelect) {
    return (
      <Select
        multi
        options={options}
        value={value}
        onChange={(selected) => {
          const values = selected.map((item) => item.value);
          onChange(values);
        }}
      />
    );
  }

  return (
    <Dropdown
      auto
      theme={dropdownTheme}
      onChange={onChange}
      source={options}
      value={value}
    />
  );
}

DropdownFilter.propTypes = {
  value: PropTypes.string,
  items: PropTypes.array.isRequired,
  multiSelect: PropTypes.bool,
  onChange: PropTypes.func,
  intl: intlShape.isRequired,
};

export default compose(
  safeInjectIntl,
  withHandlers({
    onChange: (props) => (value) =>
      props.onChange(value && value.length ? value : null),
  })
)(DropdownFilter);
