// :copyright: Copyright (c) 2022 ftrack
import { Children, cloneElement, isValidElement, useState } from "react";
import { Menu } from "@mui/material";
import PropTypes from "prop-types";

function ButtonMenu({
  button,
  children,
  onClick,
  dense,
  anchorOrigin = { horizontal: "left", vertical: "bottom" },
  transformOrigin,
  sx = {},
}) {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleButtonClick = (event) => {
    event.stopPropagation();
    if (Boolean(anchorEl)) {
      setAnchorEl(null);
    } else {
      setAnchorEl(event.currentTarget);
    }
  };

  const handleClose = (event) => {
    if (event?.stopPropagation) {
      event.stopPropagation();
    }
    setAnchorEl(null);
  };

  const handleItemClick = (key, callback, event) => {
    handleClose(event);
    if (callback) {
      callback();
    } else if (onClick) {
      onClick(key);
    }
  };

  const Button = cloneElement(button, { onClick: handleButtonClick });

  return (
    <>
      {Button}
      <Menu
        sx={sx}
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        MenuListProps={{ dense }}
        anchorOrigin={anchorOrigin}
        transformOrigin={transformOrigin}
      >
        {Children.map(children, (child) => {
          if (isValidElement(child)) {
            return cloneElement(child, {
              onClick: child.props.onClick
                ? (e) => handleItemClick(null, child.props.onClick, e)
                : (e) => handleItemClick(child.key, null, e),
            });
          }
          return child;
        })}
      </Menu>
    </>
  );
}

ButtonMenu.propTypes = {
  button: PropTypes.element.isRequired,
  dense: PropTypes.bool,
  children: PropTypes.node,
  onClick: PropTypes.func,
  anchorOrigin: PropTypes.shape({
    horizontal: PropTypes.oneOf(["center", "left", "right"]),
    vertical: PropTypes.oneOf(["center", "bottom", "top"]),
  }),
  transformOrigin: PropTypes.shape({
    horizontal: PropTypes.oneOf(["center", "left", "right"]),
    vertical: PropTypes.oneOf(["center", "bottom", "top"]),
  }),
  sx: PropTypes.object,
};

export default ButtonMenu;
