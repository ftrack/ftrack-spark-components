// :copyright: Copyright (c) 2022 ftrack

import { useState } from "react";
import { action } from "@storybook/addon-actions";
import centered from "@storybook/addon-centered/react";

import { defineMessages, FormattedMessage } from "react-intl";
import { MenuItem, Box, ListItemIcon, Tooltip } from "@mui/material";
import { Lens } from "@mui/icons-material";

import ButtonMenu from "./button_menu";
import DropdownButton from "./dropdown_button";

import { getForegroundColor } from "../util/color";

const messages = defineMessages({
  "status-selector": {
    id: "player.review-app.review.status-button",
    description: "Status text for status selector button",
    defaultMessage: "Status",
  },
});

export default {
  title: "Button menu",
  decorators: [centered],
};

export const Default = (args) => {
  return (
    <ButtonMenu {...args}>
      <MenuItem key="profile" onClick={action("profile click")}>
        Profile
      </MenuItem>
      <MenuItem key="my-account">My account</MenuItem>
      <MenuItem key="logout">Logout</MenuItem>
    </ButtonMenu>
  );
};

Default.args = {
  tooltip: "Open",
  button: (
    <DropdownButton rounded size="small">
      Open
    </DropdownButton>
  ),
  onClick: action("onClick"),
  dense: true,
};

function StatusSelector() {
  const statuses = [
    { name: "Approved", color: "green" },
    { name: "WIP", color: "gray" },
    { name: "Pending review", color: "#f4c430" },
    { name: "Rejected", color: "red" },
    { name: "test", color: "#ffffff" },
  ];

  const [localStatus, setLocalStatus] = useState({
    color: "#29bb89",
    name: "status",
  });

  return (
    <Box sx={{ display: "flex" }}>
      <Box sx={{ width: 2 }} />
      <ButtonMenu
        dense
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        button={
          <Tooltip
            title={<FormattedMessage {...messages["status-selector"]} />}
            placement="top"
          >
            <DropdownButton
              rounded
              variant="contained"
              size="small"
              sx={{
                bgcolor: localStatus.color,
                color: `${getForegroundColor(localStatus.color)}`,
                textTransform: "capitalize",
                "&:hover": {
                  bgcolor: localStatus.color,
                },
              }}
            >
              {localStatus.name}
            </DropdownButton>
          </Tooltip>
        }
      >
        {statuses.map((status) => (
          <MenuItem key={status.name} onClick={() => setLocalStatus(status)}>
            <ListItemIcon>
              <Lens fontSize="small" sx={{ color: status.color }} />
            </ListItemIcon>
            {status.name}
          </MenuItem>
        ))}
      </ButtonMenu>
    </Box>
  );
}

export const Compressed = () => <StatusSelector></StatusSelector>;
