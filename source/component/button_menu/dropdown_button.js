// :copyright: Copyright (c) 2022 ftrack
import { forwardRef, useMemo } from "react";
import { Button } from "@mui/material";
import ArrowDropDownOutlinedIcon from "@mui/icons-material/ArrowDropDownOutlined";

const DropdownButton = forwardRef(function DropdownButton(
  { children, size = "medium", sx = {}, ...restProps },
  ref
) {
  const fontSizeIcon = useMemo(
    () => (["small", "medium"].includes(size) ? "fontSizeRegular" : undefined),
    [size]
  );
  return (
    <Button
      ref={ref}
      variant="outlined"
      size={size}
      sx={{
        pr: 0.5,
        textTransform: "unset",
        fontWeight: "fontWeightRegular",
        "& .MuiButton-endIcon": {
          m: 0,
        },
        "& .MuiButton-endIcon>*:nth-of-type(1)": {
          fontSize: fontSizeIcon,
        },
        ...sx,
      }}
      {...restProps}
      endIcon={<ArrowDropDownOutlinedIcon />}
    >
      {children}
    </Button>
  );
});

export default DropdownButton;
