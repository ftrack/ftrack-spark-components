import { action } from "@storybook/addon-actions";
// eslint-disable-next-line
import centered from "@storybook/addon-centered/react";
import EntityCard from ".";
import getSession from "../../story/get_session";
import { SparkProvider } from "../util/hoc";
import style from "./style.scss";
import { compose, withProps, withState } from "recompose";

function Provider({ story, session }) {
  return <SparkProvider session={session}>{story}</SparkProvider>;
}

const linkClicked = action("Link clicked");

const withTestDataProps = withProps({
  className: style["entity-card"],
  onItemClicked: linkClicked,
  data: {
    name: "Title",
    thumbnail_id: "278ea6de-72b4-11e7-94e7-0a580ae40a16",
    subtitle_1: "Subtitle 1",
    subtitle_2: "Subtitle 2",
    attr_1: "Attribute 1 value",
    attr_2: "Attribute 2 value",
    attr_3: "Attribute 3 value",
    color_1: "pink",
    __entity_id__: "<entity-id>",
    __entity_type__: "<entity-type>",
  },
});

const TestEntityCard = withTestDataProps(EntityCard);

const TestSelectableEntityCard = compose(
  withProps({ selectable: true }),
  withTestDataProps,
  withState("selected", "onSelectionChanged", true)
)(EntityCard);

export default {
  title: "Entity card",
  decorators: [
    (Story) => <Provider story={<Story />} session={getSession()} />,
    centered,
  ],
};

export const CardBasic = () => (
  <TestEntityCard
    attributes={[
      {
        key: "name",
        label: "Label",
      },
      {
        key: "thumbnail_id",
      },
      {
        key: "subtitle_1",
        label: "Subtitle",
      },
    ]}
    subtitleAttributes={["subtitle_1"]}
    thumbnail="thumbnail_id"
    titleAttribute="name"
  />
);

CardBasic.storyName = "Card basic";

CardBasic.parameters = {
  info: "Basic card with title and subtitle",
};

export const CardColor = () => (
  <TestEntityCard
    attributes={[
      {
        key: "name",
        label: "Label",
      },
      {
        key: "thumbnail_id",
      },
      {
        key: "subtitle_1",
        label: "Subtitle",
      },
      {
        key: "color_1",
        label: "Color 1",
      },
    ]}
    subtitleAttributes={["subtitle_1"]}
    colorAttribute="color_1"
    thumbnail="thumbnail_id"
    titleAttribute="name"
  />
);

CardColor.storyName = "Card color";

CardColor.parameters = {
  info: "Basic card with color, title and subtitle",
};

export const CardBasicSelectable = () => (
  <TestSelectableEntityCard
    attributes={[
      {
        key: "name",
        label: "Label",
      },
      {
        key: "thumbnail_id",
      },
      {
        key: "subtitle_1",
        label: "Subtitle",
      },
    ]}
    subtitleAttributes={["subtitle_1"]}
    thumbnail="thumbnail_id"
    titleAttribute="name"
  />
);

CardBasicSelectable.storyName = "Card basic selectable";

CardBasicSelectable.parameters = {
  info: "Basic card with title and subtitle",
};

export const CardWith2Subtitles = () => (
  <TestEntityCard
    attributes={[
      {
        key: "name",
        label: "Label",
      },
      {
        key: "thumbnail_id",
      },
      {
        key: "subtitle_1",
        label: "Subtitle",
      },
      {
        key: "subtitle_2",
        label: "Subtitle",
      },
    ]}
    subtitleAttributes={["subtitle_1", "subtitle_2"]}
    thumbnail="thumbnail_id"
    titleAttribute="name"
  />
);

CardWith2Subtitles.storyName = "Card with 2 subtitles";

CardWith2Subtitles.parameters = {
  info: "Basic card with title and multiple subtitles",
};

export const CardWithAttributes = () => (
  <div style={{ display: "flex" }}>
    <TestEntityCard
      attributes={[
        {
          key: "name",
          label: "Label",
        },
        {
          key: "thumbnail_id",
        },
        {
          key: "subtitle_1",
          label: "Subtitle",
        },
        {
          key: "attr_1",
          label: "Attribute 1",
        },
        {
          key: "attr_2",
          label: "Attribute 2",
        },
        {
          key: "attr_3",
          label: "Attribute 3",
        },
      ]}
      subtitleAttributes={["subtitle_1"]}
      thumbnail="thumbnail_id"
      titleAttribute="name"
    />
    <div style={{ padding: "2rem" }} />
    <TestEntityCard
      attributes={[
        {
          key: "name",
          label: "Label",
        },
        {
          key: "thumbnail_id",
        },
        {
          key: "subtitle_1",
          label: "Subtitle",
        },
        {
          key: "attr_1",
          label: "Attribute 1",
        },
        {
          key: "attr_2",
          label: "Attribute 2",
        },
        {
          key: "attr_3",
          label: "Attribute 3",
        },
      ]}
      oneAttributePerLine
      subtitleAttributes={["subtitle_1"]}
      thumbnail="thumbnail_id"
      titleAttribute="name"
    />
  </div>
);

CardWithAttributes.storyName = "Card with attributes";

CardWithAttributes.parameters = {
  info: "Basic card with title and attributes",
};

export const CardWithCallbacks = () => (
  <TestEntityCard
    attributes={[
      {
        key: "attr_1",
        label: "Attribute 1",
      },
    ]}
    thumbnail={({ thumbnail_id }) => getSession().thumbnailUrl(thumbnail_id)}
    subtitleAttributes={[
      ({ subtitle_1 }) => (
        <div style={{ backgroundColor: "lightgreen" }}>{subtitle_1}</div>
      ),
      ({ subtitle_2 }) => (
        <div style={{ backgroundColor: "lightblue" }}>{subtitle_2}</div>
      ),
    ]}
    titleAttribute={({ name }) => (
      <div style={{ backgroundColor: "lightyellow" }}>{name}</div>
    )}
  />
);

CardWithCallbacks.storyName = "Card with callbacks";

CardWithCallbacks.parameters = {
  info: "Basic card with callbacks for title and subtitles",
};
