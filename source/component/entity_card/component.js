// :copyright: Copyright (c) 2016 ftrack

import PropTypes from "prop-types";

import { createElement, PureComponent } from "react";
import { Card, CardMedia, CardContent, Box, Avatar } from "@mui/material";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import Checkbox from "react-toolbox/lib/checkbox";
import { Button, IconButton } from "react-toolbox/lib/button";
import { withProps, withHandlers, compose } from "recompose";
import classNames from "classnames";
import isFunction from "lodash/isFunction";
import { AttributeLabel } from "../dataview/attribute";
import { withSession } from "../util/hoc";

import style from "./style.scss";

const ThumbnailPlayIcon = compose(
  withHandlers({
    onClick: (props) => () => props.onClick("thumbnail", props.thumbnailId),
  }),
  withProps({
    icon: "play_circle_filled",
    flat: true,
  })
)(IconButton);

function MediumHeadingLink_({ className, onClick, children }) {
  return (
    <h5 onClick={onClick} className={className}>
      {children}
    </h5>
  );
}

MediumHeadingLink_.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  entityId: PropTypes.arrayOf(PropTypes.string),
  entityType: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
};
const MediumHeadingLink = withHandlers({
  onClick: (props) => () =>
    props.onClick("link", { id: props.entityId, type: props.entityType }),
})(MediumHeadingLink_);

const CustomCardMedia = ({ entityType, image, thumbnailId }) => {
  if (entityType !== "User" && image) {
    return <CardMedia image={image} alt="" sx={{ height: "14.4rem" }} />;
  } else if (entityType === "User" && image && thumbnailId) {
    return (
      <CardMedia
        sx={{
          m: "1.6rem auto 0",
          position: "relative",
          height: "14.4rem",
          backgroundColor: "background.paper",
        }}
        image={image}
        variant="round"
        alt=""
      />
    );
  } else {
    return (
      <Avatar
        sx={{
          m: "1.6rem auto 0",
          position: "relative",
          height: "12rem",
          width: "12rem",
        }}
        src={<AccountCircleIcon />}
      />
    );
  }
};

/** Card component with thumbnail, name and attributes. */
class CustomizableCard extends PureComponent {
  constructor() {
    super();
    this.onSelectionChanged = this.onSelectionChanged.bind(this);
  }

  onSelectionChanged(state) {
    const { onSelectionChanged, index } = this.props;
    onSelectionChanged(state, index);
  }

  render() {
    const {
      attributes,
      data,
      thumbnail,
      footer,
      selected,
      selectable,
      session,
      onItemClicked,
      disableThumbnailClick,
      oneAttributePerLine,
    } = this.props;

    const ignoreAttributes = [];

    let thumbnailUrl;
    if (isFunction(thumbnail)) {
      thumbnailUrl = thumbnail(data);
    } else {
      const thumbnailAttribute = attributes.find(
        (candidate) => candidate.key === thumbnail
      );
      thumbnailUrl = session.thumbnailUrl(data[thumbnailAttribute.key]);
      ignoreAttributes.push(thumbnailAttribute.key);
    }

    let footerElement = null;
    if (footer) {
      footerElement = createElement(footer, { data });
    }

    let titleElement;
    if (isFunction(this.props.titleAttribute)) {
      titleElement = this.props.titleAttribute(data);
    } else {
      const attribute = attributes.find(
        (candidate) => candidate.key === this.props.titleAttribute
      );
      titleElement = data[attribute.key];

      if (attribute.formatter) {
        titleElement = createElement(attribute.formatter, {
          value: titleElement,
        });
      }
      ignoreAttributes.push(this.props.titleAttribute);
    }

    const subtitleElements = [];
    this.props.subtitleAttributes.forEach((subtitleAttribute) => {
      if (isFunction(subtitleAttribute)) {
        subtitleElements.push(subtitleAttribute(data));
      } else {
        const attribute = attributes.find(
          (candidate) => candidate.key === subtitleAttribute
        );
        let value = data[attribute.key];

        if (attribute.formatter) {
          value = createElement(attribute.formatter, { value });
        }
        subtitleElements.push(value);
        ignoreAttributes.push(subtitleAttribute);
      }
    });

    if (this.props.colorAttribute) {
      ignoreAttributes.push(this.props.colorAttribute);
    }

    let cardTextElement = null;
    if (attributes.length) {
      cardTextElement = (
        <div className={style["card-text"]}>
          {attributes.map((attribute) => {
            let valueElement = data[attribute.key];

            if (ignoreAttributes.indexOf(attribute.key) !== -1) {
              return null;
            }

            if (attribute.formatter) {
              valueElement = createElement(attribute.formatter, {
                value: data[attribute.key],
                onClick: this.props.onItemClicked,
              });
            }
            return (
              <div
                className={classNames(style["attribute-item"], {
                  [style["full-width"]]: oneAttributePerLine,
                })}
                key={attribute.key}
              >
                <h6 className={style["attribute-title"]}>
                  <AttributeLabel
                    attribute={attribute}
                    enableGroup
                    className={style["attribute-upper-case"]}
                  />
                </h6>
                <div className={style["attribute-value"]}>{valueElement}</div>
              </div>
            );
          })}
        </div>
      );
    }

    const cardMenuClasses = classNames(
      style["card-menu"],
      selected && style["card-selected"]
    );

    const cardClasses = classNames(
      this.props.className,
      style.card,
      selected && style["card-selected"]
    );

    const cardOverlayClasses = classNames(
      style["card-thumb-overlay"],
      selected && style["card-thumb-overlay-highlighted"]
    );

    const cardMenu = this.props.cardMenu ? (
      <div className={cardMenuClasses}>
        <this.props.cardMenu
          entityId={data.__entity_id__}
          entityType={data.__entity_type__}
          name={data.name ? data.name : this.props.model}
        />
      </div>
    ) : (
      <span />
    );

    return (
      <Card
        elevation={0}
        className={cardClasses}
        sx={{
          width: "26rem",
          minHeight: "26rem",
        }}
      >
        <Box>
          <Box className={cardOverlayClasses}>
            {cardMenu}
            {selectable && (
              <Checkbox
                className={style["card-checkbox"]}
                checked={selected}
                onChange={this.onSelectionChanged}
              />
            )}
            {!disableThumbnailClick && data.thumbnail_id ? (
              <div className={style["button-container"]}>
                <ThumbnailPlayIcon
                  thumbnailId={data.thumbnail_id}
                  onClick={onItemClicked}
                />
              </div>
            ) : null}
          </Box>
          <CustomCardMedia
            entityType={data.__entity_type__}
            image={thumbnailUrl}
            thumbnailId={data.thumbnail_id}
          />
        </Box>
        <CardContent sx={{ padding: 0, ":last-child": { paddingBottom: 0 } }}>
          <hgroup className={style["card-title-group"]}>
            <MediumHeadingLink
              className={style["card-title"]}
              entityId={data.__entity_id__}
              entityType={data.__entity_type__}
              onClick={onItemClicked}
            >
              {titleElement}
            </MediumHeadingLink>
            {subtitleElements.map((subtitleElement, index) => (
              <h6
                key={`subtitle-${index}`}
                className={style["card-title-subtitle"]}
              >
                {subtitleElement}
              </h6>
            ))}
            {this.props.colorAttribute ? (
              <Button
                icon=""
                floating
                primary
                mini
                className={style["card-color-fab"]}
                style={{
                  backgroundColor: data[this.props.colorAttribute],
                }}
              />
            ) : null}
          </hgroup>
          {cardTextElement}
          <div>{footerElement}</div>
        </CardContent>
      </Card>
    );
  }
}

CustomizableCard.propTypes = {
  data: PropTypes.object.isRequired,
  cardTheme: PropTypes.object,
  attributes: PropTypes.array.isRequired,
  className: PropTypes.string,
  thumbnail: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  titleAttribute: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  subtitleAttributes: PropTypes.array,
  colorAttribute: PropTypes.string,
  footer: PropTypes.func,
  cardMenu: PropTypes.func,
  onItemClicked: PropTypes.func,
  onSelectionChanged: PropTypes.func,
  selected: PropTypes.bool,
  selectable: PropTypes.bool,
  disableThumbnailClick: PropTypes.bool,
  index: PropTypes.number,
  oneAttributePerLine: PropTypes.bool,
  session: PropTypes.shape({
    thumbnailUrl: PropTypes.func.isRequired,
  }),
};

CustomizableCard.defaultProps = {
  className: "",
  disableThumbnailClick: false,
  selectable: false,
  oneAttributePerLine: false,
};

export default withSession(CustomizableCard);
