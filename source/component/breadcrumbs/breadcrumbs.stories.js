// :copyright: Copyright (c) 2017 ftrack
import centered from "@storybook/addon-centered/react";

import Breadcrumbs from ".";
import getFixedSizeDecorator from "../../story/get_fixed_size_decorator";

export default {
  title: "Breadcrumbs",
  decorators: [getFixedSizeDecorator(), centered],
};

const BREADCRUMB_ITEMS = [
  { id: "projects", title: "Projects" },
  {
    id: "foo-project",
    title: "Project officia",
  },
  { id: "library", title: "Library" },
];

export const Default = () => <Breadcrumbs items={BREADCRUMB_ITEMS} />;

const BREADCRUMB_ITEMS_LONG = [
  { id: "projects", title: "Projects" },
  {
    id: "foo-project",
    title:
      "Project officia ea quis esse sunt proident ut ex sunt aute enim fugiat ut eiusmod aliquip commodo.",
  },
  { id: "library", title: "Library" },
];

export const LongNameCollapseToMenu = () => (
  <Breadcrumbs truncation="menu" items={BREADCRUMB_ITEMS_LONG} />
);

LongNameCollapseToMenu.storyName = "Long name (collapse to menu)";

export const LongNameTooltipTruncation = () => (
  <Breadcrumbs truncation="tooltip" items={BREADCRUMB_ITEMS_LONG} />
);

LongNameTooltipTruncation.storyName = "Long name (tooltip truncation)";

export const LongNameSimpleTruncation = () => (
  <Breadcrumbs truncation="simple" items={BREADCRUMB_ITEMS_LONG} />
);

LongNameSimpleTruncation.storyName = "Long name (simple truncation)";

export const CommaSeparator = () => (
  <Breadcrumbs separator=", " items={BREADCRUMB_ITEMS} />
);

CommaSeparator.storyName = "Comma separator";

export const WithDisabledItems = () => {
  BREADCRUMB_ITEMS[1].disabled = true;
  return <Breadcrumbs items={BREADCRUMB_ITEMS} />;
};

WithDisabledItems.storyName = "With disabled items";

export const WithSkeleton = () => {
  BREADCRUMB_ITEMS[0].isLoading = true;
  BREADCRUMB_ITEMS[2].isLoading = true;
  return <Breadcrumbs items={BREADCRUMB_ITEMS} />;
};

WithSkeleton.storyName = "With skeleton";

export const LotsOfBreadcrumbs = () => {
  return (
    <div style={{ border: "1px solid red" }}>
      {Array.apply(null, { length: 1000 }).map((value, index) => (
        <Breadcrumbs key={index} items={BREADCRUMB_ITEMS_LONG} />
      ))}
    </div>
  );
};

LotsOfBreadcrumbs.storyName = "Lots of breadcrumbs";

export const LotsOfBreadcrumbsComparasion = () => {
  return (
    <div style={{ border: "1px solid green" }}>
      {Array.apply(null, { length: 1000 }).map((value, index) => (
        <div
          style={{
            whiteSpace: "nowrap",
            overflow: "hidden",
            textOverflow: "ellipsis",
          }}
        >
          {BREADCRUMB_ITEMS_LONG.map((item, index) => (
            <>
              {index > 0 ? " / " : null}
              {item.title}
            </>
          ))}
        </div>
      ))}
    </div>
  );
};

LotsOfBreadcrumbsComparasion.storyName = "Lots of breadcrumbs (comparasion)";
