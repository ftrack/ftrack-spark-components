..
    :copyright: Copyright (c) 2021 ftrack

###########
Breadcrumbs
###########

The Breadcrumb component shows a list of Links separated by
seperators.

If breadcrumbs do not fit within the parent container, they will collapse into
a leading menu. You can disable this behavior with truncation="simple" or
use truncation="tooltip" to display a tooltip instead of menu.

========================= =========== ======================================
Prop                      Default     Description
========================= =========== ======================================
className                             CSS class for root element.
items                     []          List of items to display, renders Link component.
classNameBreadcrumb                   Maximum number of items to display before collapsing.
truncation                "menu"      Truncation mode: menu, tooltip or simple.
separator                 " / "       Separator to show between items
========================= =========== ======================================


Item props 
========================= =========== ======================================
Prop                      Default     Description
========================= =========== ======================================
id                                    Id used in react as key.
to                                    If no callback is used, link is used on href to open new page on click.
title                                 Text on link in Breadcrumb.
onClick                               Callback for on click, if not given href link will be used.
disabled                  false       If link is disabled
isLoading                 false       If link is disabled
========================= =========== ======================================
