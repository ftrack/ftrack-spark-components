// :copyright: Copyright (c) 2021 ftrack

import { createRef, useState, useLayoutEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { Box, IconButton, MenuItem, Tooltip } from "@mui/material";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";

import Link from "../link";
import ButtonMenu from "../button_menu";

const styles = {
  breadcrumbRoot: {
    display: "flex",
    alignItems: "center",
    minWidth: 0,
  },
  breadcrumbContainer: {
    textAlign: "right",
    overflow: "hidden",
    whiteSpace: "nowrap",
    position: "relative",
  },
  disabledBreadcrumbContainer: {
    textAlign: "left",
    textOverflow: "ellipsis",
  },
  breadcrumb: {
    float: "right",
  },
  disabledBreadcrumb: {
    float: "unset",
  },
  breadcrumbButton: {
    padding: 0,
    borderRadius: "1.5rem",
    mr: 0.5,
    bgcolor: "lightContrastColor.highEmphasis",
  },
  buttonIcon: {
    height: "1.5rem",
    width: "2.4rem",
    fontSize: "fontSizeLarge",
  },
  truncatedItemsIcon: {
    marginRight: "1px",
    paddingTop: "0.6rem",
    width: "1.6rem",
    height: "2.2rem",
    lineHeight: 1,
    userSelect: "none",
  },
};

const DEFAULT_SEPARATOR = " / ";

function Breadcrumbs({
  items = [],
  sx,
  sxBreadcrumb,
  separator = DEFAULT_SEPARATOR,
  truncation = "menu",
}) {
  const refContainer = createRef();
  const refContent = createRef();
  const [truncationEnabled, setTruncationEnabled] = useState(false);

  useLayoutEffect(
    () => {
      if (truncation !== "simple") {
        if (refContainer.current.clientWidth < refContent.current.clientWidth) {
          setTruncationEnabled(true);
        } else {
          setTruncationEnabled(false);
        }
      }
    },
    // TODO: Feel free to fix this eslint error if you got time
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [refContainer, refContent]
  );

  const handleSelect = (selectedIndex) => {
    const { to, onClick, disabled } = items[selectedIndex] || {};
    if (!disabled) {
      if (onClick) {
        onClick();
      } else {
        // eslint-disable-next-line
        const newWindow = window.open(to, "_blank");
        newWindow.focus();
      }
    }
  };

  let truncatedItems = null;
  if (truncationEnabled && truncation === "menu") {
    truncatedItems = (
      <ButtonMenu
        dense
        button={
          <IconButton
            sx={{
              ...styles.breadcrumbButton,
            }}
          >
            <MoreHorizIcon sx={styles.buttonIcon} />
          </IconButton>
        }
        onSelect={handleSelect}
      >
        {items.map(({ title, disabled }, i) => (
          <MenuItem key={i} disabled={disabled}>
            {title}
          </MenuItem>
        ))}
      </ButtonMenu>
    );
  } else if (truncationEnabled && truncation === "tooltip") {
    truncatedItems = (
      <Tooltip title={items.map((item) => item.title).join(" / ")}>
        <MoreHorizIcon sx={styles.truncatedItemsIcon} />
      </Tooltip>
    );
  }

  return (
    <Box
      sx={{
        ...styles.breadcrumbRoot,
        ...sx,
      }}
    >
      {truncatedItems}
      <Box
        ref={refContainer}
        sx={{
          ...styles.breadcrumbContainer,
          ...(truncation === "simple"
            ? styles.disabledBreadcrumbContainer
            : {}),
        }}
      >
        <Box
          ref={refContent}
          component="span"
          sx={{
            ...styles.breadcrumb,
            ...(truncation === "simple" ? styles.disabledBreadcrumb : {}),
          }}
        >
          {items.map(({ to, title, id, onClick, disabled, isLoading }, i) => (
            <Fragment key={id}>
              {!!i && separator}
              <Link
                disabled={disabled}
                to={to}
                title={title}
                onClick={onClick}
                isLoading={isLoading}
              />
            </Fragment>
          ))}
        </Box>
      </Box>
    </Box>
  );
}

Breadcrumbs.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      to: PropTypes.string,
      title: PropTypes.string,
      onClick: PropTypes.func,
      disabled: PropTypes.bool,
      isLoading: PropTypes.bool,
    })
  ).isRequired,
  className: PropTypes.string,
  classNameBreadcrumb: PropTypes.string,
  separator: PropTypes.node,
  truncation: PropTypes.oneOf(["menu", "tooltip", "simple"]),
};

export default Breadcrumbs;
