// :copyright: Copyright (c) 2018 ftrack
import PropTypes from "prop-types";
import classNames from "classnames";

import style from "./style.scss";

function Header(props) {
  const { children, className, variant, color, noWrap, ...other } = props;
  const classes = classNames(style.header, className, {
    [style.variantHeadline]: variant === "headline",
    [style.variantDisplay1]: variant === "display1",
    [style.variantTitle]: variant === "title",
    [style.variantSubheading]: variant === "subheading",
    [style.variantLabel]: variant === "label",
    [style.colorInherit]: color === "inherit",
    [style.colorPrimary]: color === "primary",
    [style.colorLight]: color === "light",
    [style.colorDefault]: color === "default",
    [style.colorSecondary]: color === "secondary",
    [style.colorLightSecondary]: color === "lightSecondary",
    [style.noWrap]: noWrap,
  });

  return (
    <h2 className={classes} {...other}>
      {children}
    </h2>
  );
}

Header.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  variant: PropTypes.oneOf([
    "display1",
    "headline",
    "title",
    "subheading",
    "label",
  ]),
  color: PropTypes.oneOf([
    "inherit",
    "default",
    "primary",
    "light",
    "secondary",
  ]),
  noWrap: PropTypes.bool,
};

Header.defaultProps = {
  variant: "headline",
  color: "inherit",
  noWrap: false,
};

export default Header;
