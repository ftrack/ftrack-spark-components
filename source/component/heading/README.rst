..
    :copyright: Copyright (c) 2018 ftrack

#######
Heading
#######

The Heading component can be used to display a heading/title with a consistent
style.

The variant can be one of:

    * headline
    * title
    * subheading

Color can be one of:

    * inherit
    * primary
    * light

Specify the property `noWrap` to to have a single-line heading that truncates
the text with ellipsis if it overflows.

Other props are spread to the root span element.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
className                      CSS class for root element.
children                       Any nodes or text to display as heading.
variant            headline    The variant to use, see above.
color              inherit     The color to use, see above.
noWrap             false       Single-line heading that truncates with ellipsis.
