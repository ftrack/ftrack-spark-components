// :copyright: Copyright (c) 2018 ftrack

import centered from "@storybook/addon-centered/react";
import Heading from ".";

export default {
  title: "Heading",
  decorators: [centered],
};

export const _Heading = () => <Heading>Heading content</Heading>;

_Heading.parameters = {
  info: "This example shows a Heading component.",
};

export const Sizes = () => (
  <div
    style={{
      padding: "1.6rem",
      backgroundSize: "1.6rem 1.6rem",
      backgroundImage:
        "linear-gradient(to right, lightblue 1px, transparent 1px), linear-gradient(to bottom, lightblue 1px, transparent 1px)",
    }}
  >
    <Heading variant="display1">Heading content</Heading>
    <Heading variant="headline">Heading content</Heading>
    <Heading variant="title">Heading content</Heading>
    <Heading variant="subheading">Heading content</Heading>
  </div>
);

Sizes.parameters = {
  info: "This example shows various sizes",
};

export const Wrap = () => (
  <Heading variant="subheading">
    Qui elit in minim ut exercitation deserunt ut dolor eu nisi in ea nostrud
    proident sunt excepteur ut exercitation duis in non commodo non excepteur
    aliqua ullamco aute non pariatur ad irure eu mollit ut anim laboris fugiat
    culpa in laborum pariatur ullamco enim tempor dolore ullamco quis nulla
    occaecat laborum ea ullamco excepteur ut amet elit nostrud occaecat elit
    dolor cupidatat sit non reprehenderit esse consectetur enim aliqua
    incididunt aliquip minim enim eiusmod cillum excepteur cillum sit ad esse
    elit officia ex occaecat laborum proident minim non ad in amet veniam amet
    non culpa sit id ut veniam pariatur est ut eiusmod eu id ut eu deserunt
    incididunt in do ut adipisicing nulla laborum est dolore anim ad fugiat
    reprehenderit laboris quis amet in sint qui sunt id dolor velit ad mollit
    deserunt amet in in ullamco magna anim excepteur ad in in in occaecat dolore
    in in ex labore laboris laboris nostrud deserunt irure ex et dolor in eu
    cupidatat irure id qui incididunt pariatur elit incididunt adipisicing est
    exercitation velit proident sunt veniam minim sunt pariatur cupidatat nisi
    eiusmod aliquip sit nulla fugiat cillum ut in consectetur id tempor tempor
    tempor ex in exercitation anim.
  </Heading>
);

export const Light = () => (
  <Heading color="light" style={{ backgroundColor: "#282E34" }}>
    Heading content
  </Heading>
);

export const NoWrapPrimary = () => (
  <Heading noWrap color="primary" style={{ width: "200px" }}>
    In ea duis in ad duis dolore esse irure eiusmod laboris sed duis voluptate.
  </Heading>
);

NoWrapPrimary.storyName = "No wrap & Primary";
