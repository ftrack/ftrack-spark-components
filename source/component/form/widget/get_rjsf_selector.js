// :copyright: Copyright (c) 2016 ftrack
import { Component } from "react";
import PropTypes from "prop-types";

function getRjsfSelector(SelectorComponent, additionalProps) {
  class RjsfSelector extends Component {
    constructor(props) {
      super(props);
      this.handleChange = this.handleChange.bind(this);
    }

    handleChange(value) {
      const rjsfValue = value === null ? undefined : value;
      this.props.onChange(rjsfValue);
    }

    render() {
      // omit children & options from RJSF to selector props.
      // eslint-disable-next-line no-unused-vars
      const { children, options, ...props } = this.props;

      return (
        <SelectorComponent
          tabSelectsValue={false}
          {...additionalProps}
          {...props}
          session={this.props.formContext.session}
          onChange={this.handleChange}
        />
      );
    }
  }

  RjsfSelector.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any,
    // eslint-disable-next-line react/forbid-prop-types
    options: PropTypes.any,
    onChange: PropTypes.func,
    formContext: PropTypes.shape({
      session: PropTypes.object,
    }),
  };

  return RjsfSelector;
}

export default getRjsfSelector;
