// :copyright: Copyright (c) 2016 ftrack
import PropTypes from "prop-types";

import { Component } from "react";
import Input from "react-toolbox/lib/input";
import log from "loglevel";
import classNames from "classnames";
import style from "./style.scss";

function asNumber(value) {
  const result = parseFloat(value);
  if (Number.isFinite(result)) {
    return result;
  }
  return undefined;
}

/** Unit number widget. */
class UnitNumberWidget extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);

    const value = asNumber(props.value);
    const formattedValue = this.format(value);
    this.state = { value, formattedValue };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      const value = asNumber(nextProps.value);
      const formattedValue = this.format(value);
      this.setState({ value, formattedValue });
    }
  }

  secondsPerUnit() {
    const { unit } = this.props;
    if (unit === "hour") {
      return 60 * 60;
    }

    if (unit === "workday") {
      if (!this.props.formContext || !this.props.formContext.workDayLength) {
        log.warn("workDayLength not set in form context.");
        return 8 * 60 * 60;
      }
      return this.props.formContext.workDayLength;
    }

    return 24 * 60 * 60;
  }

  parseInput(_value) {
    const value = asNumber(_value);
    return value * this.secondsPerUnit();
  }

  format(value) {
    if (!Number.isFinite(value)) {
      return undefined;
    }
    return Math.round(100 * (value / this.secondsPerUnit())) / 100;
  }

  handleChange(value) {
    const seconds = this.parseInput(value);
    this.props.onChange(seconds);
  }

  render() {
    const inputClassName = classNames(style.input, {
      [style.readonly]: this.props.readonly,
    });

    return (
      <Input
        className={inputClassName}
        value={this.state.formattedValue}
        onChange={this.handleChange}
        type="number"
        placeholder="1"
        readOnly={this.props.readonly}
      />
    );
  }
}

UnitNumberWidget.propTypes = {
  formContext: PropTypes.shape({
    workDayLength: PropTypes.number,
  }),
  onChange: PropTypes.func,
  readonly: PropTypes.bool,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  unit: PropTypes.oneOf(["hour", "workday", "day"]),
};

export default UnitNumberWidget;
