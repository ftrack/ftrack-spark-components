// :copyright: Copyright (c) 2016 ftrack
import PropTypes from "prop-types";

import { Component } from "react";
import Input from "react-toolbox/lib/input";
import style from "./style.scss";

/**
 * Input widget adapting react-toolbox Input to RJS Form
 *
 * Also adds an autofocus prop, focusing the element on mount.
 */
class InputWidget extends Component {
  /**
   * Focus element on mount, if autofocus is set.
   * Allow the parent component to finish rendering before triggering the
   * focus change.
   **/
  componentDidMount() {
    if (this.props.autofocus) {
      setImmediate(() => {
        if (this.input) {
          this.input.focus();
        }
      });
    }
  }

  render() {
    const props = this.props;
    return (
      <Input
        className={style.input}
        value={props.value || ""}
        onChange={props.onChange}
        placeholder={props.placeholder}
        floating={false}
        readOnly={props.readonly}
        error={props.error}
        required={props.required}
        type={props.type}
        innerRef={(node) => {
          this.input = node;
        }}
      />
    );
  }
}

/* eslint-disable react/no-unused-prop-types, react/forbid-prop-types */
InputWidget.propTypes = {
  autofocus: PropTypes.bool,
  error: PropTypes.string,
  type: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  readonly: PropTypes.bool,
  required: PropTypes.bool,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

InputWidget.defaultProps = {
  type: "text",
  required: false,
  disabled: false,
  readonly: false,
  autofocus: false,
};

export default InputWidget;
