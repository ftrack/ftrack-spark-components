// :copyright: Copyright (c) 2017 ftrack
import PropTypes from "prop-types";

import { Component } from "react";
import { DatePicker, TimePicker } from "../../picker";
import isEqual from "lodash/isEqual";
import moment from "moment";
import log from "loglevel";
import style from "./style.scss";

/**
 * Convert date value to to string for JSON Schema.
 */
export function dateToString(
  date,
  { dateOnly, displayOffsetUnit, displayOffsetValue }
) {
  let dateValue = moment(date);
  if (displayOffsetUnit && displayOffsetValue) {
    dateValue.add(displayOffsetValue, displayOffsetUnit);
  }
  if (dateOnly) {
    dateValue = dateValue.startOf("day");
  }
  return dateValue.format();
}

/**
 * Convert string value to date object for display.
 */
export function stringToDate(
  stringValue,
  { dateOnly, displayOffsetUnit, displayOffsetValue }
) {
  let dateValue = moment(stringValue, moment.ISO_8601, true);
  if (!dateValue.isValid()) {
    dateValue = moment();
  }
  if (dateOnly) {
    dateValue = dateValue.startOf("day");
  }
  if (displayOffsetUnit && displayOffsetValue) {
    dateValue.subtract(displayOffsetValue, displayOffsetUnit);
  }
  return dateValue.toDate();
}

class DateTimeWidget extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      value: stringToDate(props.value, props),
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      !isEqual(nextProps.value, this.props.value) ||
      nextProps.dateOnly !== this.props.dateOnly ||
      nextProps.displayOffsetUnit !== this.props.displayOffsetUnit ||
      nextProps.displayOffsetValue !== this.props.displayOffsetValue
    ) {
      this.setState({
        value: stringToDate(nextProps.value, nextProps),
      });
    }
  }

  handleChange(value) {
    this.setState({ value });
    this.props.onChange(dateToString(value, this.props));
  }

  render() {
    const { dateOnly = false, formContext = {} } = this.props;
    const sundayFirstDayOfWeek = formContext.sundayFirstDayOfWeek;
    if (sundayFirstDayOfWeek == null) {
      log.warn("sundayFirstDayOfWeek not specified in form context.");
    }
    const DatePickerComponent = this.props.datePickerComponent || DatePicker;
    const TimePickerComponent = this.props.timePickerComponent || TimePicker;

    return (
      <div className={style.datetime}>
        <DatePickerComponent
          inputClassName={style.input}
          onChange={this.handleChange}
          value={this.state.value}
          autoOk
          sundayFirstDayOfWeek={sundayFirstDayOfWeek}
        />

        {dateOnly === true ? null : (
          <TimePickerComponent
            inputClassName={style.input}
            onChange={this.handleChange}
            value={this.state.value}
          />
        )}
      </div>
    );
  }
}

DateTimeWidget.propTypes = {
  onChange: PropTypes.func,
  dateOnly: PropTypes.bool,
  displayOffsetUnit: PropTypes.number,
  displayOffsetValue: PropTypes.string,
  value: PropTypes.string,
  formContext: PropTypes.object, // eslint-disable-line
  datePickerComponent: PropTypes.element, // eslint-disable-line
  timePickerComponent: PropTypes.element, // eslint-disable-line
};

DateTimeWidget.defaultProps = {
  datePickerComponent: null,
  timePickerComponent: null,
};

export default DateTimeWidget;
