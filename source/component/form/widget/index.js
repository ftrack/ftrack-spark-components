// :copyright: Copyright (c) 2017 ftrack
import InputWidget from "./input";
import DateTimeWidget from "./date_time";
import UnitNumberWidget from "./unit_number";

const DateWidget = (props) => <DateTimeWidget {...props} dateOnly />;
const EndDateWidget = (props) => (
  <DateTimeWidget
    {...props}
    dateOnly
    displayOffsetUnit="day"
    displayOffsetValue={1}
  />
);
const HourUnitNumberWidget = (props) => (
  <UnitNumberWidget {...props} unit="hour" />
);
const WorkDayUnitNumberWidget = (props) => (
  <UnitNumberWidget {...props} unit="workday" />
);
const DayUnitNumberWidget = (props) => (
  <UnitNumberWidget {...props} unit="day" />
);

// TODO: Feel free to fix this eslint error if you got time
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  TextWidget: InputWidget,
  date: DateWidget,
  enddate: EndDateWidget,
  "date-time": DateTimeWidget,
  hour: HourUnitNumberWidget,
  workday: WorkDayUnitNumberWidget,
  day: DayUnitNumberWidget,
};
