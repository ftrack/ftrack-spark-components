..
    :copyright: Copyright (c) 2017 ftrack

####
Form
####

The form component adapts `react jsonschema form <https://github.com/mozilla-services/react-jsonschema-form>`_
to work with spark and react toolbox components, fitting with material design.

It is currently a work in progress, and only a limited set of widgets and
functionality is supported.

See the CreateProjectDialog for example usage.
