// :copyright: Copyright (c) 2017 ftrack
import PropTypes from "prop-types";

import { Component } from "react";
import { action } from "@storybook/addon-actions";
import centered from "@storybook/addon-centered/react";
import Button from "react-toolbox/lib/button";
import withHandlers from "recompose/withHandlers";
import withState from "recompose/withState";
import compose from "recompose/compose";

import getSession from "../../story/get_session";
import getFixedSizeDecorator from "../../story/get_fixed_size_decorator";
import Form from ".";
import { SwitchInput } from "./field/switch";

import ResourceSelector from "../selector/resource_selector";
import getCustomField from "./form_field/get_custom_field";
import getRjsfSelector from "./widget/get_rjsf_selector";

const UserSelector = getRjsfSelector(ResourceSelector, {
  multi: true,
  users: true,
});

const widgets = {};
const fields = {
  "selector-User-multi": getCustomField(UserSelector),
};
const session = getSession();

class TestForm extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);

    this.schema = {
      type: "object",
      required: ["name"],
      properties: {
        name: { type: "string", title: "Name" },
        users: {
          type: "array",
          items: { type: "string" },
          default: [],
          title: "Users",
        },
        bool: { type: "boolean", title: "Boolean" },
        time: {
          type: "string",
          format: "date-time",
          title: "Date-time",
        },
        date: { type: "string", format: "date", title: "Date" },
        enddate: { type: "string", format: "date", title: "End date" },
        hour: { type: "number", title: "Hours" },
        workday: { type: "number", title: "Work days" },
        day: { type: "number", title: "Days" },
      },
    };

    this.uiSchema = {
      name: {
        "ui:autofocus": true,
        "ui:placeholder": "Test string",
      },
      users: {
        "ui:field": "selector-User-multi",
        "ui:help": "helping",
      },
      bool: { "ui:field": "switch", "ui:help": "helping" },
      date: {},
      enddate: { "ui:widget": "enddate" },
      hour: { "ui:widget": "hour", "ui:readonly": true },
      workday: { "ui:widget": "workday" },
      day: { "ui:widget": "day" },
    };
    this.initialFormContext = {
      workDayLength: 8 * 60 * 60,
      sundayFirstDayOfWeek: false,
      session,
      statuses: {
        options: [],
        loading: false,
        placeholder: "First select project",
      },
      types: {
        options: [],
        loading: false,
        placeholder: "First select project",
      },
    };

    this.state = { formData: {}, formContext: this.initialFormContext };
  }

  handleChange({ formData }) {
    this.setState({ formData });
  }

  render() {
    return (
      <Form
        schema={this.schema}
        uiSchema={this.uiSchema}
        {...this.props}
        fields={fields}
        widgets={widgets}
        formContext={this.state.formContext}
        formData={this.state.formData}
        onChange={this.handleChange}
        onSubmit={action("Submit")}
        onError={action("Error")}
      />
    );
  }
}

TestForm.propTypes = {
  onSubmit: PropTypes.func.isRequired, // eslint-disable-line
};

export default {
  title: "Form",
  decorators: [getFixedSizeDecorator(), centered],
};

export const Default = () => (
  <TestForm>
    <Button type="submit" primary raised>
      Submit
    </Button>
  </TestForm>
);

const StatefulSwitchInput = compose(
  withState("value", "setValue", false),
  withHandlers({
    onChange:
      ({ setValue }) =>
      () =>
        setValue((current) => !current),
  })
)(SwitchInput);

export const SwitchInputLabelDescription = () => (
  <StatefulSwitchInput
    name="switch"
    label="Aute ut non"
    description="Eiusmod sit nisi voluptate aute."
  />
);

SwitchInputLabelDescription.storyName = "Switch input (label & description)";

export const SwitchLabelOnly = () => (
  <StatefulSwitchInput name="switch" label="Aute ut non" />
);

SwitchLabelOnly.storyName = "Switch (label only)";

export const SwitchWithHelp = () => (
  <StatefulSwitchInput
    name="switch"
    label="Aute ut non"
    description="Eiusmod sit nisi voluptate aute."
    help="Foooo heelp baaaar."
  />
);

SwitchWithHelp.storyName = "Switch with help";

export const SwitchHelpAndLabelOnly = () => (
  <StatefulSwitchInput name="switch" label="Aute ut non" help="Help meeee!!!" />
);

SwitchHelpAndLabelOnly.storyName = "Switch (help and label only)";
