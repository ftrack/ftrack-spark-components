// :copyright: Copyright (c) 2017 ftrack
import SwitchField from "./switch";

// TODO: Feel free to fix this eslint error if you got time
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  switch: SwitchField,
};
