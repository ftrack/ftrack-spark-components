// :copyright: Copyright (c) 2017 ftrack
import PropTypes from "prop-types";

import DroppableThumbnail from "../../droppable_area/thumbnail";

function ThumbnailField(props) {
  return (
    <DroppableThumbnail
      thumbnailSize={props.uiSchema.thumbnailSize}
      session={props.formContext.session}
      value={props.formData}
      onChange={props.onChange}
    />
  );
}

ThumbnailField.propTypes = {
  uiSchema: PropTypes.object, // eslint-disable-line
  formContext: PropTypes.object, // eslint-disable-line
  formData: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

export default ThumbnailField;
