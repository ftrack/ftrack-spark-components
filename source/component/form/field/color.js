// :copyright: Copyright (c) 2017 ftrack
import PropTypes from "prop-types";

import ColorPicker from "../../color_picker";

/** Color field */
function ColorField({ classNames, formData, onChange }) {
  return (
    <ColorPicker
      className={classNames}
      position="right"
      border
      value={formData}
      onChange={onChange}
    />
  );
}

ColorField.propTypes = {
  classNames: PropTypes.string,
  formData: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

export default ColorField;
