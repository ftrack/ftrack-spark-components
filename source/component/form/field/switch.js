// :copyright: Copyright (c) 2017 ftrack
import classNames from "classnames";
import Switch from "react-toolbox/lib/switch";
import PropTypes from "prop-types";
import style from "./switch.scss";
import HelpIcon from "../../help_icon";

import switchTheme from "./switch_theme.scss";

export const SwitchInput = ({
  className,
  onChange,
  label,
  name,
  value,
  description,
  help,
  theme,
  labelClassName,
  descriptionClassName,
}) => {
  const twoLines = label && description;
  const classes = classNames(style["switch-field"], className);
  const labelClass = twoLines
    ? style["switch-field-title"]
    : style["switch-field-description"];

  return (
    <div className={classes}>
      <Switch
        name={name}
        className={twoLines && switchTheme.fieldTwoLines}
        checked={value}
        onChange={onChange}
        theme={theme || switchTheme}
      />
      <div
        onClick={() => {
          onChange(!value);
        }}
      >
        <p className={labelClassName || labelClass}>
          {label}
          {help ? <HelpIcon text={help} className={style.helpIcon} /> : null}
        </p>
        <p
          className={descriptionClassName || style["switch-field-description"]}
        >
          {description}
        </p>
      </div>
    </div>
  );
};

function SwitchField({ onChange, schema = {}, id, formData, uiSchema }) {
  return (
    <SwitchInput
      name={id}
      value={formData}
      onChange={onChange}
      label={schema.title || schema.name}
      description={uiSchema.description}
      help={uiSchema["ui:help"]}
    />
  );
}

SwitchInput.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string,
  description: PropTypes.string,
  help: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.bool,
  // eslint-disable-next-line react/forbid-prop-types
  theme: PropTypes.object,
  labelClassName: PropTypes.string,
  descriptionClassName: PropTypes.string,
};

SwitchField.propTypes = {
  schema: PropTypes.object,
  id: PropTypes.string,
  formData: PropTypes.bool,
  onChange: PropTypes.func,
  uiSchema: PropTypes.object,
};

export default SwitchField;
