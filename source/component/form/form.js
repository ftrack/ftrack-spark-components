// :copyright: Copyright (c) 2017 ftrack

import PropTypes from "prop-types";

import { Component } from "react";
// eslint-disable-next-line
import { default as RjsForm } from "react-jsonschema-form";
import widgets from "./widget";
import fields from "./field";
import FormField from "./form_field";
import style from "./style.scss";

class Form extends Component {
  constructor(props) {
    super(props);
    this.transformErrors = this.transformErrors.bind(this);
    this.validate = this.validate.bind(this);
    this.widgets = { ...widgets, ...this.props.widgets };
    this.fields = { ...fields, ...this.props.fields };
  }

  validate(formData, errors) {
    // Add required errors to empty strings and arrays
    for (const property of this.props.schema.required) {
      const propertySchema = this.props.schema.properties[property] || {};
      const title =
        propertySchema.title || propertySchema.name || "Unknown field";
      const value = formData[property];
      let hasValue = true;
      if (value === undefined || value === null) {
        hasValue = false;
      } else if (
        propertySchema.type === "string" ||
        propertySchema.type === "array"
      ) {
        hasValue = !!formData[property].length;
      }

      if (!hasValue) {
        errors[property].addError(`${title} is required.`);
      }
    }

    if (this.props.validate) {
      // eslint-disable-next-line no-param-reassign
      errors = this.props.validate(formData, errors);
    }

    return errors;
  }

  submit() {
    this.submitButton.click();
  }

  transformErrors(errors) {
    const transformedErrors = errors
      .map((error) => {
        // Required properties are checked in `validate`, avoid duplicate entries.
        if (error.name === "required") {
          return null;
        }

        return error;
      })
      .filter((error) => error);

    if (this.props.transformErrors) {
      return this.props.transformErrors(transformedErrors);
    }
    return transformedErrors;
  }

  render() {
    return (
      <RjsForm
        noHtml5Validate
        showErrorList={false}
        FieldTemplate={FormField}
        {...this.props}
        transformErrors={this.transformErrors}
        validate={this.validate}
        widgets={this.widgets}
        fields={this.fields}
      >
        <input
          type="submit"
          className={style.hidden}
          ref={(node) => {
            this.submitButton = node;
          }}
        />
        {this.props.children}
      </RjsForm>
    );
  }
}

Form.propTypes = {
  schema: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  fields: PropTypes.objectOf(PropTypes.func),
  widgets: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.func, PropTypes.object])
  ),
  children: PropTypes.node,
  validate: PropTypes.func,
  transformErrors: PropTypes.func,
};

export default Form;
