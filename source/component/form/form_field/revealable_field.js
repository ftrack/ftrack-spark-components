// :copyright: Copyright (c) 2017 ftrack
import PropTypes from "prop-types";

import classNames from "classnames";
import Reveal from "../../reveal";
import HelpIcon from "../../help_icon";
import style from "./style.scss";

function RevealableField(props) {
  const reveal = props.reveal;
  const help = props.help;
  const hasErrors = props.errors && props.errors.length;
  const required = props.required;
  const label = props.label;
  const fieldClassName = classNames(style.field, props.className);

  const labelContent = label ? (
    <label htmlFor={props.id} className={style.label}>
      {label}
      {required ? <span className={style.required}>*</span> : null}
      {help ? <HelpIcon text={help} className={style.helpIcon} /> : null}
    </label>
  ) : null;

  return (
    <Reveal
      className={style.reveal}
      label={labelContent}
      active={!reveal || hasErrors}
    >
      <div className={fieldClassName}>
        {labelContent}
        {props.children}
        {hasErrors ? (
          <ul className={style.errors}>
            {props.errors.map((errorMessage) => (
              <li>{errorMessage}</li>
            ))}
          </ul>
        ) : null}
      </div>
    </Reveal>
  );
}

RevealableField.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.node,
  reveal: PropTypes.bool,
  help: PropTypes.string,
  errors: PropTypes.arrayOf(PropTypes.string),
  required: PropTypes.bool,
  label: PropTypes.string,
};

RevealableField.defaultProps = {
  id: null,
  children: null,
  reveal: false,
  help: null,
  errors: [],
  required: false,
  label: null,
};

export default RevealableField;
