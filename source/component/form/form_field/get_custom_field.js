// :copyright: Copyright (c) 2017 ftrack
import PropTypes from "prop-types";

import RevealableField from "./revealable_field";

function getCustomField(Widget) {
  const CustomField = (props) => {
    const label = props.schema.title || props.schema.name;
    const help = props.uiSchema["ui:help"];
    const reveal = props.uiSchema.reveal;
    const classNames = props.uiSchema.classNames;

    return (
      <RevealableField
        className={classNames}
        label={label}
        reveal={reveal}
        errors={props.rawErrors}
        help={help}
        required={props.required}
      >
        <Widget {...props} value={props.formData} />
      </RevealableField>
    );
  };
  CustomField.propTypes = {
    classNames: PropTypes.string,
    schema: PropTypes.shape({
      title: PropTypes.string,
      name: PropTypes.string,
    }),
    uiSchema: PropTypes.shape({
      reveal: PropTypes.bool,
      classNames: PropTypes.string,
    }),
    rawErrors: PropTypes.arrayOf(PropTypes.string),
    rawHelp: PropTypes.string,
    required: PropTypes.bool,
    formData: PropTypes.any, // eslint-disable-line react/forbid-prop-types
  };

  return CustomField;
}

export default getCustomField;
