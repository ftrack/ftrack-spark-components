// :copyright: Copyright (c) 2017 ftrack
import PropTypes from "prop-types";

import { cloneElement } from "react";
import RevealableField from "./revealable_field";

function FormField(props) {
  if (props.hidden) {
    return props.children;
  }

  if (props.schema && props.schema.type === "array") {
    return cloneElement(props.children, Object.assign({}, props));
  }

  return (
    <RevealableField
      className={props.classNames}
      id={props.id}
      label={props.displayLabel ? props.label : null}
      reveal={props.uiSchema.reveal}
      errors={props.rawErrors}
      help={props.rawHelp}
      required={props.required}
    >
      {props.children}
      {props.displayLabel && props.description ? props.description : null}
      {/* ui:help passed as a react component */}
      {props.help && !props.rawHelp ? props.help : null}
    </RevealableField>
  );
}

/* eslint-disable react/no-unused-prop-types, react/forbid-prop-types */
FormField.propTypes = {
  id: PropTypes.string,
  classNames: PropTypes.string,
  label: PropTypes.string,
  children: PropTypes.node.isRequired,
  errors: PropTypes.element,
  rawErrors: PropTypes.arrayOf(PropTypes.string),
  help: PropTypes.element,
  rawHelp: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  description: PropTypes.element,
  rawDescription: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  hidden: PropTypes.bool,
  required: PropTypes.bool,
  readonly: PropTypes.bool,
  displayLabel: PropTypes.bool,
  fields: PropTypes.object,
  formContext: PropTypes.object,
  uiSchema: PropTypes.object,
};

FormField.defaultProps = {
  classNames: "",
  hidden: false,
  readonly: false,
  required: false,
  displayLabel: true,
};

export default FormField;
