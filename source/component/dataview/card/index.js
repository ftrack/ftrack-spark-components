// :copyright: Copyright (c) 2016 ftrack

import PropTypes from "prop-types";

import { cloneElement, Component } from "react";
import { Waypoint } from "react-waypoint";
import classNames from "classnames";
import isArray from "lodash/isArray";

import style from "./style.scss";
import Card from "../../entity_card";
import { attributeShape } from "../util";
import { withSelectionHandler } from "../selection_handler";

class CardViewContainer extends Component {
  constructor(...args) {
    super(...args);
    this.renderItems = this.renderItems.bind(this);
  }

  renderItems(renderProps) {
    const {
      thumbnail,
      titleAttribute,
      subtitleAttributes,
      attributes,
      colorAttribute,
      footer,
      model,
      selectable,
      data: rows,
      heading,
      showHeadingOnEmpty,
      viewId,
      loading,
      card: CardComponent,
      onItemClicked,
      selected,
      className,
      disableThumbnailClick,
    } = this.props;

    const waypoint = !loading ? (
      <Waypoint
        key="list-waypoint"
        onEnter={() => {
          this.props.onFetchMoreData();
        }}
        bottomOffset={-400}
      />
    ) : null;

    let children = [];
    if (this.props.children) {
      children = isArray(this.props.children)
        ? this.props.children
        : [this.props.children];
    }

    const showHeading = showHeadingOnEmpty || (heading && rows.length > 0);

    const cardViewClasses = classNames(className, {
      // only set class if content should be shown
      [style["cards-view"]]: children.length || rows.length || showHeading,
    });

    return (
      <div>
        {showHeading && <h4>{heading}</h4>}
        <div className={cardViewClasses}>
          <div className={style["extra-children"]}>
            {children.map((child) => cloneElement(child, { ...renderProps }))}
          </div>
          {rows.map((data, rowIndex) => {
            const selectionProps = selectable
              ? {
                  selected: selected.indexOf(data.__entity_id__) !== -1,
                  onSelectionChanged: this.props.onCheckboxClicked,
                }
              : {};

            return (
              <CardComponent
                thumbnail={thumbnail}
                className={style["card-item"]}
                data={data}
                key={`card-${rowIndex}`}
                id={data.id}
                attributes={attributes}
                titleAttribute={titleAttribute}
                subtitleAttributes={subtitleAttributes}
                colorAttribute={colorAttribute}
                footer={footer}
                model={model}
                selectable={selectable}
                {...selectionProps}
                index={rowIndex}
                onItemClicked={onItemClicked}
                disableThumbnailClick={disableThumbnailClick}
                viewId={viewId}
                {...renderProps}
              />
            );
          })}
        </div>
        {waypoint}
      </div>
    );
  }

  renderContent() {
    return <div>{this.renderItems()}</div>;
  }

  render() {
    const { renderProps } = this.props;

    if (renderProps) {
      return this.renderItems(renderProps);
    }
    return this.renderContent();
  }
}

CardViewContainer.propTypes = {
  card: PropTypes.node,
  children: PropTypes.arrayOf(PropTypes.node),
  titleAttribute: PropTypes.func,
  subtitleAttributes: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.func, PropTypes.string]).isRequired
  ).isRequired,
  attributes: PropTypes.arrayOf(attributeShape),
  data: PropTypes.array,
  colorAttribute: PropTypes.string,
  footer: PropTypes.func,
  renderProps: PropTypes.shape({ onDrop: PropTypes.func }),
  onCheckboxClicked: PropTypes.func,
  onFetchMoreData: PropTypes.func,
  onItemClicked: PropTypes.func,
  model: PropTypes.string,
  thumbnail: PropTypes.func,
  heading: PropTypes.string,
  className: PropTypes.string,
  showHeadingOnEmpty: PropTypes.bool,
  selectable: PropTypes.bool,
  viewId: PropTypes.string,
  loading: PropTypes.bool,
  disableThumbnailClick: PropTypes.bool,
  selected: PropTypes.array.isRequired,
};

CardViewContainer.defaultProps = {
  card: Card,
  selected: [],
};

export default withSelectionHandler()(CardViewContainer);
