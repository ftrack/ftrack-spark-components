// :copyright: Copyright (c) 2017 ftrack

import PropTypes from "prop-types";

import { Fragment } from "react";

import { mapProps, withProps, compose, withHandlers } from "recompose";
import { operation } from "ftrack-javascript-api";
import isArray from "lodash/isArray";
import { defineMessages } from "react-intl";

import { withSession } from "../util/hoc";
import RemoteSelector from "../selector";
import ProjectSelector from "../selector/project_selector";
import ResourceSelector from "../selector/resource_selector";
import DateFilter from "../filter/date_filter";
import QueryFilter, { replaceVariables } from "../filter/query_filter";
import DropdownFilter from "../filter/dropdown_filter";
import { Entity } from "../formatter/entityUtils";
import isTypedContextSubclass, {
  getObjectTypeSchemaId,
} from "../util/schema_operations";

import {
  TextFormatter,
  BooleanFormatter,
  LinkFormatter,
  FullnameFormatter,
  DateFormatter,
  LocalizedDateFormatter,
  ThumbnailFormatter,
  ColorLabelFormatter,
  NamesFormatter,
  SecondToHoursFormatter,
  ColorFormatter,
  SentenceCaseTextFormatter,
  AvatarThumbnailFormatter,
  BidTimeFormatter,
  VersionWithStatusFormatter,
  withAttributeLoadingState,
  AssetFormatter,
  ListsFormatter,
} from "../formatter";

import {
  DEFAULT_WORKDAY_LENGTH,
  DEFAULT_DISPLAY_BID_AS_DAYS,
  SECONDS_IN_HOUR,
} from "../util/constant";

export const attributeShape = PropTypes.shape({
  icon: PropTypes.string,
  key: PropTypes.string.isRequired,
  description: PropTypes.string,
  label: PropTypes.string,
});

export const attributeGroupShape = PropTypes.shape({
  key: PropTypes.string.isRequired,
  label: PropTypes.string,
  attributes: PropTypes.arrayOf(attributeShape).isRequired,
});

export const CUSTOM_ATTRIBUTE_KEY_PREFIX = "custom_attribute-";
// We would prefer CUSTOM_ATTRIBUTE_LINK_TO_KEY_PREFIX to include `-to-`, but
// unfortunately this will break filters that have already added links while from support
// wasn't shipped.
export const CUSTOM_ATTRIBUTE_LINK_TO_KEY_PREFIX = "custom_attribute-";
export const CUSTOM_ATTRIBUTE_LINK_FROM_KEY_PREFIX = "custom_attribute-from-";

function encodeArrayCondition(items) {
  return items.map((item) => `"${item}"`).join(", ");
}

/** Return true if *attribute* is a custom attribute */
export function isCustomAttribute(attribute) {
  const attributeName = attribute.key.split(".").pop();
  return attributeName.startsWith(CUSTOM_ATTRIBUTE_KEY_PREFIX);
}

/** Return object details from *relation* or null.
 *
 * Passing parent[TypedContext] as an argument will return an object with:
 *
 * * `name`: parent
 * * `targetSchemaId`: TypedContext
 *
 * Passing parent will return null.
 */
export function getPolymorphicRelationDetails(relation) {
  const regExp = /.*\[([^\]]*)\]/;
  const result = regExp.exec(relation);
  if (!result) {
    return null;
  }
  const targetSchemaId = result[1];

  return {
    name: relation.replace(`[${targetSchemaId}]`, ""),
    targetSchemaId,
  };
}

const FILTER_MESSAGES = defineMessages({
  "ancestors-hint": {
    defaultMessage: "Matches objects above in project hierarchy",
    description: "Filter hint message TypedContext ancestors",
    id: "ftrack-spark-components.dataview.utils.ancestors-hint",
  },
});

export function getFilterOperators(type) {
  const operators = {
    number: [
      "is",
      "is_not",
      "is_greater",
      "is_greater_or_equal",
      "is_less",
      "is_less_or_equal",
    ],
    integer: [
      "is",
      "is_not",
      "is_greater",
      "is_greater_or_equal",
      "is_less",
      "is_less_or_equal",
    ],
    string: [
      "is",
      "is_not",
      "is_greater",
      "is_greater_or_equal",
      "is_less",
      "is_less_or_equal",
      "contains",
      "not_contains",
      "starts_with",
      "ends_with",
    ],
    boolean: ["is"],
    enumerator: ["in", "not_in", "set", "not_set"],
    link: ["in", "not_in"],
  };
  return operators[type] || ["is", "is_not"];
}

function CustomAttributeFormatWrapper(id, type, config, { value, onClick }) {
  // Return correct formatter based on *value* and *configuration*.
  if (type === "link") {
    if (value && value.length > 0) {
      return value.map((v, i) => {
        const entityValue = config.getDisplayData(v);
        if (!entityValue) {
          // The entity value might be undefined if pruned by a
          // server side permission engine. I.e. the link exist,
          // but the user doesn't have the permission to see the value.
          return null;
        }
        return (
          <Fragment key={entityValue.id}>
            {!!i && <span>, </span>}
            <config.formatter value={entityValue} onClick={onClick} />
          </Fragment>
        );
      });
    }
    return null;
  }

  const customAttributeItem = (value || []).find(
    (item) => item.configuration_id === id
  );
  const customAttributeValue = customAttributeItem && customAttributeItem.value;

  if (customAttributeValue === null || customAttributeValue === undefined) {
    return null;
  }

  if (type === "number") {
    return (
      <span>{customAttributeValue.toFixed(config.isdecimal ? 2 : 0)}</span>
    );
  }

  if (type === "date-time") {
    return <DateFormatter value={customAttributeValue} />;
  }

  if (type === "boolean") {
    return <BooleanFormatter value={customAttributeValue} />;
  }

  if (type === "enumerator") {
    let data;
    try {
      data = JSON.parse(config.data);
    } catch (error) {
      data = [];
    }

    // Build list of enumerator labels. Use raw value if label does not
    // exist.
    const enumeratorLabels = customAttributeValue.map((item) => {
      const candidate = data.find((option) => option.value === item);
      return candidate && candidate.menu ? candidate.menu : item;
    });
    return <span>{enumeratorLabels.join(", ")}</span>;
  }

  if (type === "dynamic enumerator") {
    return <span>{customAttributeValue.join(", ")}</span>;
  }

  return <span>{customAttributeValue}</span>;
}

const remoteSelectorHandler = withHandlers({
  onChange: (props) => (value) => {
    props.onChange(
      value && value.length
        ? value.map((item) => (item.value ? item.value : item))
        : null
    );
  },
});

function bidFilterToSeconds(value, isDisplayBidAsDays, workdayLength) {
  if (isDisplayBidAsDays) {
    return (value || 0) * workdayLength;
  }
  return (value || 0) * SECONDS_IN_HOUR;
}

function getBidFilterCondition(attributeName) {
  return (operator, value, configuration = {}) => {
    const workdayLength = configuration.workdayLength || DEFAULT_WORKDAY_LENGTH;
    const isDisplayBidAsDays =
      configuration.isDisplayBidAsDays || DEFAULT_DISPLAY_BID_AS_DAYS;
    return `${attributeName} ${operator} "${bidFilterToSeconds(
      value,
      isDisplayBidAsDays,
      workdayLength
    )}"`;
  };
}

function addObjectTypeInformationToAddtribute(attribute, objectType, schemas) {
  // Name filters according to the object type name instead of relationship (ancestors -> Episode)
  attribute.groupLabel = objectType.name;

  // Add hint to filters about how it is applied
  attribute.filterMessage = FILTER_MESSAGES["ancestors-hint"];

  // Used to apply filter to group (... and object_type_id is ${objectTypeId})
  attribute.objectTypeId = objectType.id;

  // Used as an identifier for the filter, `ancestors[${objectTypeSchemaId}]`
  attribute.objectTypeSchemaId = getObjectTypeSchemaId(schemas, objectType.id);

  if (attribute.key.endsWith("$entities")) {
    // When selecting entity we need to add more information to entity picker
    attribute.options = {
      ...attribute.options,
      objectTypeId: objectType.id,
      objectTypeName: objectType.name,
      objectTypeSchemaId: getObjectTypeSchemaId(schemas, objectType.id),
    };
  }
}

function buildRelationalGroup(
  relation,
  schemas,
  schema,
  model,
  purpose,
  objectType
) {
  const parts = [];
  const targetSchema = relation
    .split(".")
    .reduce((fromSchema, relationFragment) => {
      const details = getPolymorphicRelationDetails(relationFragment);

      let nextSchema;
      if (details) {
        // The relation is polymorphic and the schema and key
        // must be changed.
        nextSchema = schemas.find((item) => item.id === details.targetSchemaId);
      } else {
        const property = fromSchema.properties[relationFragment];
        let ref;
        if (property.type === "array") {
          ref = property.items.$ref;
        } else {
          ref = property.$ref;
        }
        nextSchema = schemas.find((item) => item.id === ref);
      }

      parts.push({
        key: relationFragment,
        targetModel: nextSchema.id,
        model: fromSchema.id,
      });
      return nextSchema;
    }, schema);

  const relationalAttributes = getPropertiesFromSchema(
    targetSchema,
    purpose,
    parts,
    objectType
  ).scalar;

  if (objectType) {
    // When object type is set, we need to add additonal information about relations
    // for each attribute to be used on filters in query, entity picker and label
    relationalAttributes.forEach((attribute) => {
      addObjectTypeInformationToAddtribute(attribute, objectType, schemas);
    });
  }

  return {
    path: parts,
    groupLabel: objectType?.name,
    key: `${model}.${relation}`,
    model: targetSchema.id,
    relation,
    attributes: relationalAttributes,
  };
}

export const FILTER_OVERRIDES = {
  TypedContext: {
    bid: {
      filterCondition: getBidFilterCondition("bid"),
    },
    bid_time_logged_difference: {
      filterCondition: getBidFilterCondition("bid_time_logged_difference"),
    },
    time_logged: {
      filterCondition: getBidFilterCondition("time_logged"),
    },
    $query: {
      filterComponent: withProps({
        model: "TypedContext",
      })(QueryFilter),
      filterCondition: (operator, value, configuration) =>
        replaceVariables(value, configuration),
      filterOperators: [],
    },
    $project: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          multi: true,
        })
      )(ProjectSelector),
      filterCondition(operator, value) {
        return `project_id ${operator} (${encodeArrayCondition(value)})`;
      },
      filterOperators: ["in", "not_in"],
    },
    $status: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          entityType: "Status",
          multi: true,
          extraFields: ["color"],
          orderByField: "sort",
          orderByDirection: "DESC",
        })
      )(RemoteSelector),
      filterCondition(operator, value) {
        return `status_id ${operator} (${encodeArrayCondition(value)})`;
      },
      filterOperators: ["in", "not_in"],
    },
    $type: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          entityType: "Type",
          multi: true,
          extraFields: ["color"],
          orderByField: "sort",
        })
      )(RemoteSelector),
      filterCondition(operator, value) {
        return `type_id ${operator} (${encodeArrayCondition(value)})`;
      },
      filterOperators: ["in", "not_in"],
    },
    $priority: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          entityType: "Priority",
          multi: true,
          extraFields: ["color"],
          orderByField: "sort",
        })
      )(RemoteSelector),
      filterCondition(operator, value) {
        return `priority_id ${operator} (${encodeArrayCondition(value)})`;
      },
      filterOperators: ["in", "not_in"],
    },
    $assignees: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          users: true,
          multi: true,
        })
      )(ResourceSelector),
      filterCondition(operator, value) {
        if (operator === "set") {
          return "assignments any ()";
        }

        if (operator === "not_set") {
          return "not assignments any ()";
        }

        return `assignments.resource_id ${operator} (${encodeArrayCondition(
          value
        )})`;
      },
      filterOperators: ["in", "not_in", "set", "not_set"],
    },
    $objectType: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          entityType: "ObjectType",
          multi: true,
          orderByField: "sort",
        })
      )(RemoteSelector),
      filterCondition(operator, value) {
        return `object_type_id ${operator} (${encodeArrayCondition(value)})`;
      },
      filterOperators: ["in", "not_in"],
    },
    $scope: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          entityType: "Scope",
          multi: true,
        })
      )(RemoteSelector),
      filterCondition(operator, value) {
        if (operator === "set") {
          return "scopes any ()";
        }

        if (operator === "not_set") {
          return "not scopes any ()";
        }

        return `scopes.id ${operator} (${encodeArrayCondition(value)})`;
      },
      filterOperators: ["in", "not_in", "set", "not_set"],
    },
    created_at: {
      filterComponent: withProps({
        modes: [
          "this_week",
          "this_month",
          "today",
          "yesterday",
          "date",
          "custom_range",
        ],
      })(DateFilter),
    },
    $entities: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "TypedContext",
      },
      filterCondition(operator, value) {
        return `id ${operator} (${encodeArrayCondition(value)})`;
      },
    },
    $lists: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "List",
        baseFilter: "system_type is 'task'",
      },
      filterCondition(operator, value) {
        return `lists.id ${operator} (${encodeArrayCondition(value)})`;
      },
    },
    $outgoing_links: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "TypedContext",
      },
      filterCondition(operator, value) {
        return `outgoing_links.to_id ${operator} (${encodeArrayCondition(
          value
        )})`;
      },
    },
    $incoming_links: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "TypedContext",
      },
      filterCondition(operator, value) {
        return `incoming_links.from_id ${operator} (${encodeArrayCondition(
          value
        )})`;
      },
    },
  },
  ReviewSession: {},
  AssetVersion: {
    $isLatestVersion: {
      type: "boolean",
      filterCondition(operator, value) {
        return value
          ? "is_latest_version is True"
          : "is_latest_version is False";
      },
    },
    date: {
      filterComponent: withProps({
        modes: [
          "this_week",
          "this_month",
          "today",
          "yesterday",
          "date",
          "custom_range",
        ],
      })(DateFilter),
    },
    $query: {
      filterComponent: withProps({
        model: "AssetVersion",
      })(QueryFilter),
      filterCondition: (operator, value, configuration) =>
        replaceVariables(value, configuration),
      filterOperators: [],
    },
    $project: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          multi: true,
        })
      )(ProjectSelector),
      filterCondition(operator, value) {
        const values = encodeArrayCondition(value);
        const condition = `(asset.parent.project_id in (${values}))`;
        if (operator === "not_in") {
          return `not ${condition}`;
        }

        return condition;
      },
      filterOperators: ["in", "not_in"],
    },
    $status: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          entityType: "Status",
          multi: true,
          extraFields: ["color"],
          orderByField: "sort",
          orderByDirection: "DESC",
        })
      )(RemoteSelector),
      filterCondition(operator, value) {
        return `status_id ${operator} (${encodeArrayCondition(value)})`;
      },
      filterOperators: ["in", "not_in"],
    },
    $assetType: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          entityType: "AssetType",
          multi: true,
        })
      )(RemoteSelector),
      filterCondition(operator, value) {
        return `asset.type_id ${operator} (${encodeArrayCondition(value)})`;
      },
      filterOperators: ["in", "not_in"],
    },
    $publishedBy: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          users: true,
          multi: true,
        })
      )(ResourceSelector),
      filterCondition(operator, value) {
        return `user_id ${operator} (${encodeArrayCondition(value)})`;
      },
      filterOperators: ["in", "not_in"],
    },
    $asset_parent: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "Context",
      },
      filterCondition(operator, value) {
        return `asset.parent.id ${operator} (${encodeArrayCondition(value)})`;
      },
    },
    $lists: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "List",
        baseFilter: "system_type is 'assetversion'",
      },
      filterCondition(operator, value) {
        return `lists.id ${operator} (${encodeArrayCondition(value)})`;
      },
    },
    $outgoing_links: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "AssetVersion",
      },
      filterCondition(operator, value) {
        return `outgoing_links.to_id ${operator} (${encodeArrayCondition(
          value
        )})`;
      },
    },
    $incoming_links: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "AssetVersion",
      },
      filterCondition(operator, value) {
        return `incoming_links.from_id ${operator} (${encodeArrayCondition(
          value
        )})`;
      },
    },
    $review_sessions: {
      type: "link",
      filterOperators: ["in", "not_in"],
      options: {
        entityType: "ReviewSession",
      },
      filterCondition(operator, value) {
        return `review_session_objects.review_session.id ${operator} (${encodeArrayCondition(
          value
        )})`;
      },
    },
  },
  Type: {
    $type: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          entityType: "Type",
          multi: true,
          extraFields: ["color"],
          orderByField: "sort",
        })
      )(RemoteSelector),
      filterCondition(operator, value) {
        return `id ${operator} (${encodeArrayCondition(value)})`;
      },
      filterOperators: ["in", "not_in"],
    },
  },
  Note: {
    $author: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          users: true,
          multi: true,
        })
      )(ResourceSelector),
      filterCondition(operator, value) {
        return `user_id ${operator} (${encodeArrayCondition(value)})`;
      },
      filterOperators: ["in", "not_in"],
    },
    date: {
      filterComponent: withProps({
        modes: [
          "this_week",
          "this_month",
          "today",
          "yesterday",
          "date",
          "custom_range",
        ],
      })(DateFilter),
    },
    thread_activity: {
      filterComponent: withProps({
        modes: [
          "this_week",
          "this_month",
          "today",
          "yesterday",
          "date",
          "custom_range",
        ],
      })(DateFilter),
    },
    completed_at: {
      filterComponent: withProps({
        modes: [
          "this_week",
          "this_month",
          "today",
          "yesterday",
          "date",
          "custom_range",
        ],
      })(DateFilter),
    },
    $is_completed: {
      type: "boolean",
      filterCondition(operator, value) {
        if (value) {
          return "completed_at is_not None";
        }
        return "completed_at is None";
      },
    },
    $recipients: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          users: true,
          multi: true,
          groups: true,
        })
      )(ResourceSelector),
      filterCondition(operator, value) {
        return `recipients.resource_id ${operator} (${encodeArrayCondition(
          value
        )})`;
      },
      filterOperators: ["in", "not_in"],
    },
    $note_labels: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          entityType: "NoteLabel",
          multi: true,
          extraFields: ["name"],
          orderByField: "sort",
        })
      )(RemoteSelector),
      filterCondition(operator, value) {
        return `note_label_links.label_id ${operator} (${encodeArrayCondition(
          value
        )})`;
      },
      filterOperators: ["in", "not_in"],
    },
    $query: {
      filterComponent: withProps({
        model: "Note",
      })(QueryFilter),
      filterCondition: (operator, value, configuration) =>
        replaceVariables(value, configuration),
      filterOperators: [],
    },
  },
  Project: {
    $project: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          multi: true,
        })
      )(ProjectSelector),
      filterCondition(operator, value) {
        const values = encodeArrayCondition(value);
        if (operator === "not_in") {
          return `id not_in (${values})`;
        }
        return `id in (${values})`;
      },
      filterOperators: ["in", "not_in"],
    },
    $scope: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          entityType: "Scope",
          multi: true,
        })
      )(RemoteSelector),
      filterCondition(operator, value) {
        if (operator === "set") {
          return "scopes any ()";
        }

        if (operator === "not_set") {
          return "not scopes any ()";
        }

        return `scopes.id ${operator} (${encodeArrayCondition(value)})`;
      },
      filterOperators: ["in", "not_in", "set", "not_set"],
    },
    status: {
      filterComponent: withProps({
        multiSelect: false,
        items: [
          {
            value: null,
            label: "Any",
          },
          {
            value: "active",
            label: "Active",
          },
          {
            value: "hidden",
            label: "Hidden",
          },
        ],
      })(DropdownFilter),
      filterOperators: ["is", "is_not"],
    },
    created_at: {
      filterComponent: withProps({
        modes: [
          "this_week",
          "this_month",
          "today",
          "yesterday",
          "date",
          "custom_range",
        ],
      })(DateFilter),
    },
  },
  User: {
    $user: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          users: true,
          multi: true,
        })
      )(ResourceSelector),
      filterCondition(operator, value) {
        return `id ${operator} (${encodeArrayCondition(value)})`;
      },
      filterOperators: ["in", "not_in"],
    },
    $group_membership: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          users: false,
          groups: true,
          multi: true,
        })
      )(ResourceSelector),
      filterCondition(operator, value) {
        return `memberships.group_id ${operator} (${encodeArrayCondition(
          value
        )})`;
      },
      filterOperators: ["in", "not_in"],
    },
  },
  Group: {
    $group: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          users: false,
          groups: true,
          multi: true,
        })
      )(ResourceSelector),
      filterCondition(operator, value) {
        return `id ${operator} (${encodeArrayCondition(value)})`;
      },
      filterOperators: ["in", "not_in"],
    },
  },
  CalendarEvent: {
    $project: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          multi: true,
        })
      )(ProjectSelector),
      filterCondition(operator, value) {
        return `project_id ${operator} (${encodeArrayCondition(value)})`;
      },
      filterOperators: ["in", "not_in"],
    },
    $type: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          entityType: "Type",
          multi: true,
          extraFields: ["color"],
          orderByField: "sort",
        })
      )(RemoteSelector),
      filterCondition(operator, value) {
        return `type_id ${operator} (${encodeArrayCondition(value)})`;
      },
      filterOperators: ["in", "not_in"],
    },
    $users: {
      filterComponent: compose(
        withSession,
        remoteSelectorHandler,
        withProps({
          users: true,
          multi: true,
        })
      )(ResourceSelector),
      filterCondition(operator, value) {
        if (operator === "set") {
          return "calendar_event_resources any ()";
        }

        if (operator === "not_set") {
          return "not calendar_event_resources any ()";
        }

        return `calendar_event_resources.resource_id ${operator} (${encodeArrayCondition(
          value
        )})`;
      },
      filterOperators: ["in", "not_in", "set", "not_set"],
    },
    estimate: {
      filterCondition(operator, value, configuration = {}) {
        const workdayLength =
          configuration.workdayLength || DEFAULT_WORKDAY_LENGTH;
        return `estimate ${operator} "${(value || 0) * workdayLength}"`;
      },
    },
    effort: {
      filterCondition(operator, value) {
        return `effort ${operator} "${(value || 0) * 3600}"`;
      },
    },
  },
};

export const ATTRIBUTE_OVERRIDES = {
  Asset: {
    $type: {
      formatter: mapProps(({ value }) => ({
        value: value && value.type ? value.type.name : null,
      }))(TextFormatter),
      projection: ["type.name"],
    },
  },
  AssetVersion: {
    date: {
      formatter: mapProps(({ value }) => ({
        value,
        displayTooltip: true,
        localize: true,
      }))(DateFormatter),
    },
    $name: {
      formatter: mapProps(({ value, onClick }) => ({
        value:
          (value && value.link && [value.link[value.link.length - 1]]) || [],
        onClick,
      }))(LinkFormatter),
      projection: ["link"],
    },
    link: {
      formatter: LinkFormatter,
    },
    _link: {
      formatter: LinkFormatter,
    },
    $linkedTo: {
      formatter: mapProps(({ value, onClick }) => ({
        value:
          (value && value.link && value.link.slice(0, value.link.length - 1)) ||
          [],
        onClick,
      }))(LinkFormatter),
      projection: ["link"],
    },
    thumbnail_: {
      formatter: mapProps(({ value, onClick, height }) => ({
        thumbnailId: value && value.thumbnail_id,
        onClick,
        height,
      }))(ThumbnailFormatter),
      projection: ["thumbnail_id"],
    },
    $status: {
      formatter: mapProps(({ value }) => ({
        name: value && value.status ? value.status.name : null,
        color: value && value.status ? value.status.color : null,
      }))(ColorLabelFormatter),
      projection: ["status.color", "status.name"],
      sortOn: ["status.sort"],
    },
    $user: {
      formatter: mapProps(({ value }) => ({
        first: value && value.user ? value.user.first_name : null,
        last: value && value.user ? value.user.last_name : null,
      }))(FullnameFormatter),
      projection: ["user.first_name", "user.last_name"],
    },
    $lists: {
      formatter: mapProps(({ value = {}, onClick }) => ({
        values: value.lists,
        onClick,
      }))(ListsFormatter),
      projection: ["lists.name", "lists.id"],
    },
  },
  Context: {
    link: {
      formatter: LinkFormatter,
    },
    name: {
      formatter: mapProps(({ value, onClick }) => ({
        value:
          (value && [
            {
              name: value.name,
              id: value.id,
              type: "TypedContext",
            },
          ]) ||
          [],
        onClick,
      }))(LinkFormatter),
      projection: ["name", "id"],
    },
    $createdBy: {
      formatter: mapProps(({ value }) => ({
        first: value && value.created_by ? value.created_by.first_name : null,
        last: value && value.created_by ? value.created_by.last_name : null,
      }))(FullnameFormatter),
      projection: ["created_by.first_name", "created_by.last_name"],
    },
    created_at: { formatter: LocalizedDateFormatter },
  },
  TypedContext: {
    name: {
      formatter: mapProps(({ value, onClick }) => ({
        value:
          (value && [
            {
              name: value.name,
              id: value.id,
              type: "TypedContext",
            },
          ]) ||
          [],
        onClick,
      }))(LinkFormatter),
      projection: ["name", "id"],
    },
    $latestVersions: {
      formatter: withAttributeLoadingState(VersionWithStatusFormatter),
      options: {
        loading: "rollup",
      },
    },
    $linkedTo: {
      formatter: mapProps(({ value, onClick }) => ({
        value:
          (value && value.link && value.link.slice(0, value.link.length - 1)) ||
          [],
        onClick,
      }))(LinkFormatter),
      projection: ["link"],
    },
    link: {
      formatter: LinkFormatter,
    },
    _link: {
      formatter: LinkFormatter,
    },
    $assignees: {
      projection: [
        "assignments.resource.first_name",
        "assignments.resource.last_name",
      ],
      formatter: mapProps(({ value }) => ({
        names:
          value && value.assignments
            ? value.assignments.map((item) => item.resource)
            : null,
      }))(NamesFormatter),
    },
    bid: {
      formatter: BidTimeFormatter,
    },
    time_logged: {
      formatter: BidTimeFormatter,
    },
    bid_time_logged_difference: {
      formatter: BidTimeFormatter,
    },
    start_date: { formatter: LocalizedDateFormatter },
    end_date: { formatter: LocalizedDateFormatter },
    $scope: {
      projection: ["scopes.name"],
      formatter: mapProps(({ value }) => ({
        value:
          value && value.scopes
            ? value.scopes
                .map((scope) => scope.name)
                .sort()
                .join(", ")
            : null,
      }))(TextFormatter),
    },
    thumbnail_: {
      formatter: mapProps(({ value, onClick, height }) => ({
        thumbnailId: value && value.thumbnail_id,
        onClick,
        height,
      }))(ThumbnailFormatter),
      projection: ["thumbnail_id"],
    },
    $type: {
      formatter: mapProps(({ value }) => ({
        value: value && value.type ? value.type.name : null,
      }))(TextFormatter),
      projection: ["type.name"],
      sortOn: ["type.sort"],
    },
    $status: {
      formatter: mapProps(({ value }) => ({
        name: value && value.status ? value.status.name : null,
        color: value && value.status ? value.status.color : null,
      }))(ColorLabelFormatter),
      projection: ["status.color", "status.name"],
      sortOn: ["status.sort"],
    },
    $priority: {
      formatter: mapProps(({ value }) => ({
        name: value && value.priority ? value.priority.name : null,
        color: value && value.priority ? value.priority.color : null,
      }))(ColorLabelFormatter),
      projection: ["priority.color", "priority.name"],
      sortOn: ["priority.sort"],
    },
    $project: {
      formatter: mapProps(({ value, onClick }) => ({
        value:
          (value && [
            {
              name: value.project.full_name,
              id: value.project.id,
              type: "Project",
            },
          ]) ||
          [],
        onClick,
      }))(LinkFormatter),
      projection: ["project.full_name", "project.id"],
      sortOn: ["project.full_name"],
    },
    $objectType: {
      formatter: mapProps(({ value }) => ({
        value: value && value.object_type ? value.object_type.name : null,
      }))(TextFormatter),
      projection: ["object_type.name"],
      sortOn: ["object_type.sort"],
    },
    $createdBy: {
      formatter: mapProps(({ value }) => ({
        first: value && value.created_by ? value.created_by.first_name : null,
        last: value && value.created_by ? value.created_by.last_name : null,
      }))(FullnameFormatter),
      projection: ["created_by.first_name", "created_by.last_name"],
    },
    $lists: {
      formatter: mapProps(({ value, onClick }) => ({
        values: value?.lists || [],
        onClick,
      }))(ListsFormatter),
      projection: ["lists.name", "lists.id"],
    },
  },
  User: {
    thumbnail_: {
      formatter: mapProps(({ value, height }) => ({
        user: {
          thumbnail_id: value.thumbnail_id,
          first_name: value.first_name,
          last_name: value.last_name,
        },
        height,
      }))(AvatarThumbnailFormatter),
      projection: ["thumbnail_id", "first_name", "last_name"],
    },
    $user: {
      formatter: mapProps(({ value, onClick }) => ({
        value:
          (value && [
            {
              name: `${value.first_name} ${value.last_name}`,
              id: value.id,
              type: "User",
            },
          ]) ||
          [],
        onClick,
      }))(LinkFormatter),
      projection: ["first_name", "last_name", "id"],
    },
  },
  Project: {
    link: {
      formatter: LinkFormatter,
    },
    $project_schema_name: {
      formatter: mapProps(({ value }) => ({
        value: value && value.project_schema ? value.project_schema.name : null,
      }))(TextFormatter),
      projection: ["project_schema.name"],
    },
    $managers: {
      projection: ["managers.user.first_name", "managers.user.last_name"],
      formatter: mapProps(({ value }) => ({
        names:
          value && value.managers
            ? value.managers.map((item) => item.user)
            : null,
      }))(NamesFormatter),
    },
    $scope: {
      projection: ["scopes.name"],
      formatter: mapProps(({ value }) => ({
        value:
          value && value.scopes
            ? value.scopes
                .map((scope) => scope.name)
                .sort()
                .join(", ")
            : null,
      }))(TextFormatter),
    },
    thumbnail_: {
      formatter: mapProps(({ value, onClick, height }) => ({
        thumbnailId: value && value.thumbnail_id,
        onClick,
        height,
      }))(ThumbnailFormatter),
      projection: ["thumbnail_id"],
    },
    color: {
      formatter: ColorFormatter,
    },
    name: {
      formatter: mapProps(({ value, onClick }) => ({
        value:
          (value && [
            {
              name: value.name,
              id: value.id,
              type: "Project",
            },
          ]) ||
          [],
        onClick,
      }))(LinkFormatter),
      projection: ["name", "id"],
    },
    status: {
      formatter: SentenceCaseTextFormatter,
    },
    $createdBy: {
      formatter: mapProps(({ value }) => ({
        first: value && value.created_by ? value.created_by.first_name : null,
        last: value && value.created_by ? value.created_by.last_name : null,
      }))(FullnameFormatter),
      projection: ["created_by.first_name", "created_by.last_name"],
    },
  },
  Timelog: {
    duration: {
      formatter: SecondToHoursFormatter,
    },
  },
  ReviewSession: {
    $reviewSession: {
      formatter: mapProps(({ value, onClick }) => ({
        value:
          (value && [
            {
              name: value.name,
              id: value.id,
              type: "ReviewSession",
            },
          ]) ||
          [],
        onClick,
      }))(LinkFormatter),
      projection: ["name", "id"],
    },
    thumbnail_: {
      formatter: compose(
        mapProps(({ value, onClick, height }) => ({
          thumbnailId: value && value.thumbnail_id,
          onClick,
          height,
        })),
        withProps({
          disableThumbnailClick: true,
        })
      )(ThumbnailFormatter),
      projection: ["thumbnail_id"],
    },
    $project: {
      formatter: mapProps(({ value, onClick }) => ({
        value:
          (value && [
            {
              name: value.project.full_name,
              id: value.project.id,
              type: "Project",
            },
          ]) ||
          [],
        onClick,
      }))(LinkFormatter),
      projection: ["project.full_name", "project.id"],
      sortOn: ["project.full_name"],
    },
    $createdBy: {
      formatter: mapProps(({ value }) => ({
        first: value && value.created_by ? value.created_by.first_name : null,
        last: value && value.created_by ? value.created_by.last_name : null,
      }))(FullnameFormatter),
      projection: ["created_by.first_name", "created_by.last_name"],
    },
    created_at: { formatter: LocalizedDateFormatter },
  },
};

export function getCustomAttributeLinksProperties(entityTypeTo) {
  const properties = { projections: [], entityType: "" };

  // TODO: Combine this with formatter/entityUtils.js
  switch (entityTypeTo) {
    case "user":
      properties.entityType = "User";
      properties.projections = Entity.User.linkProjection;
      properties.linkRelation = Entity.User.linkRelation;
      properties.formatter = ATTRIBUTE_OVERRIDES.User.$user.formatter;
      properties.getDisplayData = (value) =>
        value.user && {
          type: value.user.__entity_type__,
          id: value.user.id,
          first_name: value.user.first_name,
          last_name: value.user.last_name,
        };
      break;
    case "asset_version":
      properties.entityType = "AssetVersion";
      properties.projections = Entity.AssetVersion.linkProjection;
      properties.linkRelation = Entity.AssetVersion.linkRelation;
      properties.formatter = withProps({ shortLabel: true })(
        VersionWithStatusFormatter
      );
      // Refactor the VersionWithStatusFormatter, currently it operates on an array but
      // perhaps we should instead have it work on a single version?
      properties.getDisplayData = (value) => [value.asset_version];
      break;
    case "list":
      properties.entityType = "List";
      properties.projections = Entity.List.linkProjection;
      properties.linkRelation = Entity.List.linkRelation;
      properties.formatter = mapProps(({ value, onClick }) => ({
        value:
          (value.project &&
            value && [
              {
                name: value.project.full_name,
                id: value.project.id,
                type: "Project",
              },
              {
                name: value.name,
                id: value.id,
                type: "List",
              },
            ]) ||
          [],
        onClick,
        shortLabel: true,
      }))(LinkFormatter);
      properties.getDisplayData = (value) => value.list;
      break;
    case "group":
      properties.entityType = "Group";
      properties.projections = Entity.Group.linkProjection;
      properties.linkRelation = Entity.Group.linkRelation;
      properties.formatter = withProps({
        shortLabel: true,
        onClick: null,
      })(LinkFormatter);
      properties.getDisplayData = (value) => value.group && value.group.link;
      break;
    case "asset":
      properties.entityType = "Asset";
      properties.projections = Entity.Asset.linkProjection;
      properties.linkRelation = Entity.Asset.linkRelation;
      properties.formatter = AssetFormatter;
      properties.getDisplayData = (value) => value.asset;
      break;
    case "task":
      properties.entityType = "TypedContext";
      properties.projections = Entity.TypedContext.linkProjection;
      properties.linkRelation = Entity.TypedContext.linkRelation;
      properties.formatter = withProps({ shortLabel: true })(LinkFormatter);
      properties.getDisplayData = (value) =>
        value.context && value.context.link;
      break;
    case "show":
      properties.entityType = "Project";
      properties.projections = Entity.Project.linkProjection;
      properties.linkRelation = Entity.Project.linkRelation;
      properties.formatter = withProps({ shortLabel: true })(LinkFormatter);
      properties.getDisplayData = (value) =>
        value.context && value.context.link;
      break;
    case "context":
      properties.entityType = "Context";
      properties.projections = Entity.Context.linkProjection;
      properties.linkRelation = Entity.Context.linkRelation;
      properties.formatter = withProps({ shortLabel: true })(LinkFormatter);
      properties.getDisplayData = (value) =>
        value.context && value.context.link;
      break;
    default:
      properties.projections = Entity.Context.linkProjection;
      properties.linkRelation = Entity.Context.linkRelation;
      properties.formatter = withProps({ shortLabel: true })(LinkFormatter);
      properties.getDisplayData = (value) =>
        value.context && value.context.link;
      break;
  }
  return properties;
}

function attachCustomAttributeLinkData(customLinkResult, model, objectTypeId) {
  return customLinkResult.data
    .map((link) => {
      const currentType = getCustomAttributeLinksProperties(
        link.entity_type_to
      ).entityType;
      let isIncomingLink = currentType === model;
      // if there is a subtype, check that it matches too
      if (link.object_type_id_to) {
        isIncomingLink = link.object_type_id_to === objectTypeId;
      }
      const objectTypeName = isIncomingLink
        ? link.object_type && link.object_type.name
        : link.object_type_to && link.object_type_to.name;
      const customAttributeLinkProperties = getCustomAttributeLinksProperties(
        isIncomingLink ? link.entity_type : link.entity_type_to
      );
      if (isIncomingLink) {
        customAttributeLinkProperties.linkRelation = `${customAttributeLinkProperties.linkRelation}From`;
      }
      function buildCustomAttributeLinkProperty(isIncoming) {
        return {
          ...link,
          label: isIncoming
            ? `${
                objectTypeName || customAttributeLinkProperties.entityType
              } (from ${link.label})`
            : link.label,
          type: {
            name: "link",
          },
          options: {
            isIncomingLink: isIncoming,
            loading: "custom_attribute_links",
            configuration_id: link.id,
            entity_type: link.entity_type,
            entity_type_to: link.entity_type_to,
            object_type_id: link.object_type_id,
            object_type_id_to: link.object_type_id_to,
            objectTypeId: isIncoming
              ? link.object_type_id
              : link.object_type_id_to,
            objectTypeName,
            ...customAttributeLinkProperties,
          },
        };
      }
      let incomingAndOutgoingIsSameEntity =
        link.entity_type === link.entity_type_to;
      if (link.entity_type_to === "task") {
        incomingAndOutgoingIsSameEntity =
          link.object_type_id_to === link.object_type_id;
      }
      // Since we want to include both the from and to direction
      // in the case where incoming and outgoing are the same entity,
      // but only the incoming link when they are not the same, we are
      // returning an array that we later flatten.
      return incomingAndOutgoingIsSameEntity
        ? [
            buildCustomAttributeLinkProperty(true),
            buildCustomAttributeLinkProperty(false),
          ]
        : [buildCustomAttributeLinkProperty(isIncomingLink)];
    })
    .flat();
}

export function buildCustomAttributes(relation, path, configurations, purpose) {
  const attributes = [];
  configurations.forEach((configuration) => {
    let config;
    try {
      config = JSON.parse(configuration.config);
    } catch (error) {
      config = {};
    }

    const type =
      {
        date: "date-time",
        text: "string",
      }[configuration.type.name] || configuration.type.name;

    let keyPrefix = CUSTOM_ATTRIBUTE_KEY_PREFIX;
    if (type === "link") {
      keyPrefix = configuration.options.isIncomingLink
        ? CUSTOM_ATTRIBUTE_LINK_FROM_KEY_PREFIX
        : CUSTOM_ATTRIBUTE_LINK_TO_KEY_PREFIX;
    }

    let attribute = {
      label: configuration.label,
      key: relation
        ? `${relation}.${keyPrefix}${configuration.id}`
        : `${keyPrefix}${configuration.id}`,
      path,
      type,
      relation,
      customAttributeGroupName:
        (configuration.group && configuration.group.name) || null,
    };
    if (purpose === "filters") {
      attribute = {
        ...attribute,
        config,
        filterOperators: getFilterOperators(type),
      };

      if (type === "enumerator") {
        attribute.filterCondition = (operator, value) => {
          if (operator === "set") {
            return 'value is_not ""';
          }

          if (operator === "not_set") {
            return 'value is ""';
          }

          const encodedValue = isArray(value)
            ? encodeArrayCondition(value)
            : value;
          return `value ${operator} (${encodedValue})`;
        };
      }

      if (type === "link") {
        attribute.options = configuration.options;
        const direction = configuration.options.isIncomingLink
          ? "from_id"
          : "to_id";
        attribute.filterCondition = (operator, value) => {
          const values = encodeArrayCondition(value);
          if (operator === "not_in") {
            return `${direction} not_in (${values})`;
          }
          return `${direction} in (${values})`;
        };
      }
    } else {
      const defaultProjections =
        type === "link"
          ? configuration.options.projections
          : "custom_attributes";
      const defaultConfig = type === "link" ? configuration.options : config;
      attribute = {
        ...attribute,
        projection: relation
          ? `${relation}.custom_attributes`
          : defaultProjections,
        width: 150,
        formatter: CustomAttributeFormatWrapper.bind(
          this,
          configuration.id,
          type,
          defaultConfig
        ),
        options: configuration.options,
      };
    }
    attributes.push(attribute);
  });
  return attributes;
}

function getFormatter(property) {
  if (property.formatter) {
    return property.formatter;
  }

  if (property.type === "string" && property.format === "date-time") {
    return DateFormatter;
  }

  if (property.type === "boolean") {
    return BooleanFormatter;
  }

  return null;
}

function getPropertiesFromSchema(schema, purpose, path = [], objectType) {
  let schemaOverrides;
  if (purpose === "filters") {
    schemaOverrides = FILTER_OVERRIDES;
  } else if (purpose === "attributes") {
    schemaOverrides = ATTRIBUTE_OVERRIDES;
  } else {
    throw new Error(
      `Purpose must be either filters or attributes. Got: ${purpose}.`
    );
  }

  const properties = Object.assign({}, schema.properties);
  let override = schemaOverrides[schema.id];

  if (schema.alias_for && schema.alias_for.id === "Task") {
    // Pick overrides from typed context.
    override = schemaOverrides.TypedContext;
  }

  Object.keys(override || []).forEach((key) => {
    properties[key] = Object.assign({}, properties[key] || {}, override[key]);
  });

  const attributes = Object.keys(properties).reduce(
    (accumulator, key) => {
      const property = properties[key];

      const pathKeys = path.map((item) => item.key);
      const fullKey = [...pathKeys, objectType?.id, key]
        .filter((keyPart) => keyPart)
        .join(".");
      let projection = fullKey;
      if (property.projection) {
        projection = property.projection.map((item) =>
          [...pathKeys, item].join(".")
        );
      }

      const type =
        property.format === "date-time" ? property.format : property.type;
      const relation = pathKeys.join(".");
      let sortOn = property.sortOn;
      if (sortOn && relation.length) {
        // The attribute should be sorted on something else than the
        // default projection.
        sortOn = property.sortOn.map((item) => `${relation}.${item}`);
      }

      const attribute = {
        key: fullKey,
        model: schema.id,
        path,
        relation,
        icon: property.icon,
        sortOn,
        type,
        options: property.options,
      };

      if (purpose === "filters") {
        Object.assign(attribute, {
          filterOperators: property.filterOperators || getFilterOperators(type),
          filterComponent: property.filterComponent,
          filterCondition: property.filterCondition,
          groupLabel: property.groupLabel,
        });
      } else {
        Object.assign(attribute, {
          projection,
          width: property.width,
          options: property.options,
          formatter: getFormatter(property),
        });
      }

      if (property.$ref) {
        accumulator.single.push(attribute);
      } else if (property.items) {
        accumulator.collection.push(attribute);
      } else {
        accumulator.scalar.push(attribute);
      }

      return accumulator;
    },
    {
      scalar: [],
      single: [],
      collection: [],
    }
  );

  return attributes;
}

export function getProperties(model, relations, session, purpose) {
  const schemas = session.schemas;
  const schema = schemas.find((item) => item.id === model);

  const supportedCustomAttributeTypes = [
    "text",
    "date",
    "enumerator",
    "number",
    "boolean",
    "link",
  ];

  if (purpose === "attributes") {
    supportedCustomAttributeTypes.push("dynamic enumerator");
  }

  const operations = [
    operation.query(
      "select id, name, is_prioritizable, is_time_reportable, is_taskable, is_typeable, " +
        "is_statusable, is_schedulable, is_leaf from ObjectType"
    ),
  ];

  if (schema.properties.custom_attributes) {
    operations.push(
      operation.query(
        "select label, key, type.name, entity_type, config, object_type_id, group.name " +
          "from CustomAttributeConfiguration where project_id is None and " +
          "is_hierarchical is False order by label"
      )
    );
  }

  if (schema.properties.custom_attribute_links) {
    operations.push(
      operation.query(
        "select label, key, object_type_id, group.name, entity_type, entity_type_to, object_type_id_to, object_type.name, object_type_to.name " +
          "from CustomAttributeLinkConfiguration where project_id is None " +
          "order by label"
      )
    );
  }

  return session
    .call(operations)
    .then(
      ([
        objectTypesResult,
        customAttributesResult,
        customLinkResult = { data: [] },
      ]) => {
        const ownObjectType = objectTypesResult.data.find(
          (object) => getObjectTypeSchemaId(schemas, object.id) === model
        );
        const ownAttributes = getPropertiesFromSchema(schema, purpose).scalar;
        ownAttributes.forEach((attribute) => {
          // Rename top-level "select objects" to e.g. "Select shot"
          // and add object type information to entity-picker filter.
          if (attribute.key.endsWith("$entities")) {
            attribute.groupLabel = ownObjectType?.name;
            attribute.options = attribute.options || {};
            attribute.options = {
              ...attribute.options,
              objectTypeId: ownObjectType?.id,
              objectTypeName: ownObjectType?.name,
              objectTypeSchemaId: model,
            };
          }
        });
        const groups = [
          {
            label: model,
            key: model,
            model,
            path: [],
            relation: null,
            attributes: ownAttributes,
          },
        ];

        const relationGroups = relations.map((relation) =>
          buildRelationalGroup(relation, schemas, schema, model, purpose)
        );

        if (
          purpose === "filters" &&
          (model === "AssetVersion" || isTypedContextSubclass(model, schemas))
        ) {
          objectTypesResult.data
            .filter(
              (objectType) =>
                !objectType.is_leaf &&
                getObjectTypeSchemaId(schemas, objectType.id) !== model
            )
            .forEach((objectType) => {
              const relation =
                model === "AssetVersion" ? "asset.ancestors" : "ancestors";

              const genericGroup = buildRelationalGroup(
                `${relation}[TypedContext]`,
                schemas,
                schema,
                model,
                purpose,
                objectType
              );
              const ObjectTypeSchemaId = getObjectTypeSchemaId(
                schemas,
                objectType.id
              );
              genericGroup.key = `${model}.${relation}[${ObjectTypeSchemaId}]`;
              genericGroup.objectType = {
                ...objectType,
              };
              relationGroups.push(genericGroup);
            });
        }

        groups.push(...relationGroups);

        const FILTER_CONSTRAINTS = {
          status_id: "is_statusable",
          $status: "is_statusable",
          type_id: "is_typeable",
          $type: "is_typeable",
          $assignees: "is_leaf",
          bid: "is_leaf",
          time_logged: "is_leaf",
          bid_time_logged_difference: "is_leaf",
          start_date: "is_schedulable",
          end_date: "is_schedulable",
          $priority: "is_prioritizable",
          priority_id: "is_prioritizable",
        };

        groups.forEach((group) => {
          const targetSchema = schemas.find((item) => item.id === group.model);
          const objectTypeId =
            targetSchema.alias_for &&
            targetSchema.alias_for.classifiers &&
            targetSchema.alias_for.classifiers.object_typeid;

          if (objectTypeId || group.objectType) {
            const objectTypeConfig = group.objectType
              ? group.objectType
              : objectTypesResult.data.find(
                  (candidate) => candidate.id === objectTypeId
                );

            if (objectTypeConfig) {
              group.attributes = group.attributes.filter((attribute) => {
                // For related objects key is not same as attribute
                const candidateKey = group.path.length
                  ? attribute.key.split(".").pop()
                  : attribute.key;

                // Remove object type filter when there is an implicit filter.
                if (
                  candidateKey === "$objectType" ||
                  candidateKey === "object_type_id"
                ) {
                  return false;
                }

                const constraint = FILTER_CONSTRAINTS[candidateKey];

                if (constraint && objectTypeConfig[constraint] === false) {
                  // The object type does not allow for the attribute.
                  return false;
                }
                return true;
              });
            }
          } else if (group.path.length && targetSchema.id === "TypedContext") {
            // Handle attribute filter for related TypedContext
            group.attributes = group.attributes.filter((attribute) => {
              const candidateKey = attribute.key.split(".").pop();
              const constraint = FILTER_CONSTRAINTS[candidateKey];
              if (
                objectTypesResult.data
                  .filter((objectType) => !objectType.is_leaf)
                  .some((objectType) => objectType[constraint]) ||
                !constraint
              ) {
                return true;
              }
              return false;
            });
          }

          if (
            schema.properties.custom_attributes ||
            schema.properties.custom_attribute_links
          ) {
            const combinedResult = [
              ...attachCustomAttributeLinkData(
                customLinkResult,
                model,
                objectTypeId
              ),
              ...customAttributesResult.data,
            ];

            const items = combinedResult.filter((item) => {
              const entityTypes = [item.entity_type, item.entity_type_to];
              if (
                supportedCustomAttributeTypes.indexOf(item.type.name) === -1
              ) {
                return false;
              }

              if (objectTypeId) {
                return [item.object_type_id, item.object_type_id_to].includes(
                  targetSchema.alias_for.classifiers.object_typeid
                );
              }

              if (group.objectType) {
                return [item.object_type_id, item.object_type_id_to].includes(
                  group.objectType.id
                );
              }

              if (group.model === "Asset" && entityTypes.includes("asset")) {
                return true;
              }

              if (group.model === "Project" && entityTypes.includes("show")) {
                return true;
              }

              if (
                group.model === "AssetVersion" &&
                (entityTypes.includes("assetversion") ||
                  entityTypes.includes("asset_version"))
              ) {
                return true;
              }

              if (group.model === "User" && entityTypes.includes("user")) {
                return true;
              }

              if (group.model === "List" && entityTypes.includes("list")) {
                return true;
              }

              return false;
            });
            const customAttributes = buildCustomAttributes(
              group.relation,
              group.path,
              items,
              purpose
            );
            group.attributes = [...group.attributes, ...customAttributes];
          }
        });
        return Promise.resolve({
          groups,
          attributes: groups.reduce((accumulator, group) => {
            group.attributes.forEach((attribute) => {
              // accumulator[attribute.key] and attribute needs to point to the
              // same object beacuse we modify group attribute in other places
              // and expect then changes to be carried over to attributes.
              accumulator[attribute.key] = attribute;
            });
            return accumulator;
          }, {}),
        });
      }
    );
}

export function getFilters(model, relations, session) {
  return getProperties(model, relations, session, "filters");
}

export default function getAttributes(model, relations, session) {
  return getProperties(model, relations, session, "attributes");
}
