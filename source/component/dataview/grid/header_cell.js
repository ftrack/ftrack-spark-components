// :copyright: Copyright (c) 2016 ftrack

import PropTypes from "prop-types";

import { PureComponent } from "react";
import { Cell } from "fixed-data-table-2";
import FontIcon from "react-toolbox/lib/font_icon";
import { AttributeLabel } from "../attribute";

import style from "./style";

class HeaderCell extends PureComponent {
  constructor(props) {
    super(props);
    this.handleSortChange = this.handleSortChange.bind(this);
  }

  getSortInidcator() {
    let sortIndicator = "";
    if (this.props.sortDirection === "DESC") {
      sortIndicator = (
        <FontIcon
          className={style["header-cell-sort-icon"]}
          value="arrow_downward"
        />
      );
    } else if (this.props.sortDirection === "ASC") {
      sortIndicator = (
        <FontIcon
          className={style["header-cell-sort-icon"]}
          value="arrow_upward"
        />
      );
    }
    return sortIndicator;
  }

  handleSortChange() {
    if (!this.props.sortable) {
      return;
    }
    const nextSort = this.props.sortDirection === "ASC" ? "DESC" : "ASC";
    this.props.onSortChanged(this.props.columnKey, nextSort);
  }

  render() {
    const { attribute } = this.props;
    const sortIndicator = this.props.sortable ? this.getSortInidcator() : null;
    const className = this.props.sortable
      ? "header-cell--sortable"
      : "header-cell";
    return (
      <Cell
        {...this.props}
        className={style[className]}
        onClick={this.handleSortChange}
      >
        {sortIndicator}
        <AttributeLabel attribute={attribute} enableGroup />
      </Cell>
    );
  }
}

HeaderCell.propTypes = {
  attribute: PropTypes.string,
  sortable: PropTypes.bool,
  onSortChanged: PropTypes.func,
  sortDirection: PropTypes.string,
  columnKey: PropTypes.string,
};

export default HeaderCell;
