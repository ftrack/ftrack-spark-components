// :copyright: Copyright (c) 2016 ftrack

import PropTypes from "prop-types";

import { createElement, PureComponent } from "react";
import { Cell } from "fixed-data-table-2";

import { attributeShape } from "../util";
import { TextFormatter } from "../../formatter";

class FormattedCell extends PureComponent {
  constructor(props) {
    super(props);
    this.handleDoubleClick = this.handleDoubleClick.bind(this);
  }

  handleDoubleClick() {
    if (this.props.onCellDoubleClick) {
      this.props.onCellDoubleClick(
        this.props.row,
        this.props.attribute,
        this.props.columnKey
      );
    }
  }

  render() {
    const { row, attribute, width, height, onClick, ...otherProps } =
      this.props;

    const value = row[attribute.key];

    const formattedValue = createElement(attribute.formatter || TextFormatter, {
      value,
      attribute,
      onClick,
      height,
    });

    return (
      <Cell
        width={width}
        height={height}
        onDoubleClick={
          this.props.onCellDoubleClick ? this.handleDoubleClick : null
        }
        {...otherProps}
      >
        {formattedValue}
      </Cell>
    );
  }
}

FormattedCell.propTypes = {
  attribute: attributeShape,
  columnKey: PropTypes.string,
  height: PropTypes.number,
  onCellDoubleClick: PropTypes.func,
  onClick: PropTypes.func,
  row: PropTypes.object, // eslint-disable-line
  width: PropTypes.number,
};

export default FormattedCell;
