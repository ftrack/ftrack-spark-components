// :copyright: Copyright (c) 2016 ftrack

import PropTypes from "prop-types";

import { Component } from "react";
import debounce from "lodash/debounce";
import isUndefined from "lodash/isUndefined";
import { Table, Column } from "fixed-data-table-2";
import log from "loglevel";

import HeaderCell from "./header_cell";
import SelectionCell from "./selection_cell";
import FormattedCell from "./formatted_cell";
import { withSelectionHandler } from "../selection_handler";

/** Grid component */
class Grid extends Component {
  constructor(props) {
    super();
    this.state = {
      nextOffset: 0,
      sortAttribute: props.sortAttribute,
      sortDirection: props.sortDirection,
    };
    this.handleAllRowsRendered = debounce(
      this.handleAllRowsRendered.bind(this),
      25
    );
    this.handleGridSort = this.handleGridSort.bind(this);
    this.handleRowSelectionChanged = this.handleRowSelectionChanged.bind(this);
    this.handleColumnReorder = this.handleColumnReorder.bind(this);
    this.handleColumnResize = this.handleColumnResize.bind(this);
  }

  getGridColumns() {
    const { attributes, data, sortAttribute, sortDirection, selected } =
      this.props;

    const dataIds = data.map((item) => item.__entity_id__);
    const selectedIndexes = selected.reduce((accumulator, identifier) => {
      const index = dataIds.indexOf(identifier);
      if (index !== -1) {
        accumulator.push(index);
      }

      return accumulator;
    }, []);

    const checkboxColumn = (
      <Column
        key="checkbox-column"
        width={34}
        cell={(props) => (
          <SelectionCell
            lastRowIndex={data.length - 1}
            onLastRowRendered={this.handleAllRowsRendered}
            onSelectionChanged={this.handleRowSelectionChanged}
            rowIndex={props.rowIndex}
            selectedIndexes={selectedIndexes}
          />
        )}
      />
    );

    const columns = attributes.map((attribute, index) => {
      let columnSortDirection = null;
      if (attribute.sortable && attribute.key === sortAttribute) {
        columnSortDirection = sortDirection;
      }

      // Force last column to flex.
      const lastColumn = index === attributes.length - 1;
      let flexGrow = attribute.width ? null : 1;
      let attributeWidth = attribute.width || null;

      if (lastColumn) {
        flexGrow = 1;
      }

      // Min width should be set as width if column is flex.
      // https://github.com/schrodinger/fixed-data-table-2/issues/23
      if (flexGrow && attribute.minWidth) {
        attributeWidth = attribute.minWidth;
      }

      return (
        <Column
          key={`column-${attribute.key}`}
          width={attributeWidth}
          minWidth={attribute.minWidth}
          maxWidth={attribute.maxWidth}
          flexGrow={flexGrow}
          columnKey={attribute.key}
          isResizable={
            isUndefined(attribute.resizable) ? true : attribute.resizable
          }
          isReorderable={
            isUndefined(attribute.reorderable) ? true : attribute.reorderable
          }
          allowCellsRecycling
          header={(headerProps) => (
            <HeaderCell
              attribute={attribute}
              sortable={attribute.sortable}
              sortDirection={columnSortDirection}
              onSortChanged={this.handleGridSort}
              {...headerProps}
            />
          )}
          cell={(cellProps) => (
            <FormattedCell
              attribute={attribute}
              row={data[cellProps.rowIndex]}
              onCellDoubleClick={this.props.onCellDoubleClick}
              onClick={this.props.onItemClicked}
              {...cellProps}
            />
          )}
        />
      );
    });

    return [checkboxColumn, ...columns];
  }

  handleAllRowsRendered() {
    if (this.props.onFetchMoreData) {
      this.props.onFetchMoreData();
    }
  }

  handleRowSelectionChanged(state, selectedIndex) {
    if (this.props.onCheckboxClicked) {
      this.props.onCheckboxClicked(state, selectedIndex);
    }
  }

  handleGridSort(sortAttribute, sortDirection) {
    this.props.onSortChanged(sortAttribute, sortDirection);
  }

  handleColumnReorder(event) {
    if (!event.reorderColumn) {
      log.warn("Reordering column without key defined.", event);
      return;
    }

    const nextColumnOrder = this.props.attributes
      .map((attribute) => attribute.key)
      .filter((columnKey) => columnKey !== event.reorderColumn);

    if (event.columnAfter) {
      const insertAt = nextColumnOrder.indexOf(event.columnAfter);
      nextColumnOrder.splice(insertAt, 0, event.reorderColumn);
    } else {
      nextColumnOrder.push(event.reorderColumn);
    }

    this.props.onColumnReorder(nextColumnOrder);
  }

  handleColumnResize(nextWidth, columnKey) {
    this.props.onColumnResize(columnKey, nextWidth);
  }

  render() {
    const columns = this.getGridColumns();
    return (
      <div className={this.props.className}>
        <Table
          rowHeight={this.props.rowHeight}
          headerHeight={56}
          rowsCount={this.props.data.length}
          width={this.props.width}
          height={this.props.height}
          onColumnReorderEndCallback={this.handleColumnReorder}
          onColumnResizeEndCallback={this.handleColumnResize}
          isColumnReordering={false}
          isColumnResizing={false}
        >
          {columns}
        </Table>
      </div>
    );
  }
}

Grid.propTypes = {
  attributes: PropTypes.array,
  selected: PropTypes.array,
  height: PropTypes.number,
  loading: PropTypes.bool,
  onFetchMoreData: PropTypes.func,
  onCellDoubleClick: PropTypes.func,
  onColumnReorder: PropTypes.func,
  onColumnResize: PropTypes.func,
  onComponentDidMount: PropTypes.func,
  onCheckboxClicked: PropTypes.func,
  onItemClicked: PropTypes.func,
  onSortChanged: PropTypes.func,
  rowHeight: PropTypes.number,
  data: PropTypes.array,
  sortAttribute: PropTypes.string,
  sortDirection: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.number,
};
Grid.defaultProps = {
  selected: [],
};

export default withSelectionHandler()(Grid);
