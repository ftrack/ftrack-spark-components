// :copyright: Copyright (c) 2016 ftrack

import PropTypes from "prop-types";

import { PureComponent } from "react";
import { Cell } from "fixed-data-table-2";
import Checkbox from "react-toolbox/lib/checkbox";

import style from "./style";
import checkboxTheme from "./selection_checkbox_theme.scss";

class SelectionCell extends PureComponent {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(state) {
    this.props.onSelectionChanged(state, this.props.rowIndex);
  }

  render() {
    const isChecked = this.props.selectedIndexes.includes(this.props.rowIndex);
    if (this.props.rowIndex === this.props.lastRowIndex) {
      this.props.onLastRowRendered();
    }

    return (
      <Cell className={style["selection-cell"]}>
        <Checkbox
          theme={checkboxTheme}
          checked={isChecked}
          onChange={this.handleChange}
        />
      </Cell>
    );
  }
}

SelectionCell.propTypes = {
  lastRowIndex: PropTypes.number,
  onLastRowRendered: PropTypes.func,
  onSelectionChanged: PropTypes.func,
  rowIndex: PropTypes.number,
  selectedIndexes: PropTypes.array,
};

export default SelectionCell;
