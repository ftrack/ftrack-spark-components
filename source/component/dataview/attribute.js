// :copyright: Copyright (c) 2017 ftrack

import PropTypes from "prop-types";

import { intlShape, defineMessages } from "react-intl";
import classNames from "classnames";

import style from "./style.scss";
import {
  isCustomAttribute,
  getPolymorphicRelationDetails,
  attributeShape,
} from "../dataview/util";
import {
  getLocalisedLabel,
  AttributeLabel as AttributeLabelItem,
} from "../attribute";

import safeInjectIntl from "../util/hoc/safe_inject_intl";

const messages = defineMessages({
  "unknown-attribute": {
    defaultMessage: "Unknown attribute",
    description: "Unknown attribute label",
    id: "ftrack-spark-components.attribute.unknown-attribute",
  },
});

export function sortLocalisedAttributes(attributes, schema, intl) {
  const localisedStrings = attributes.reduce(
    (localisedAccumulator, { key, label }) => {
      if (label) {
        localisedAccumulator[key] = label;
      } else {
        const keyPath = key.split(".");
        const attributeKey = keyPath[keyPath.length - 1];
        const i18nMessage = getLocalisedLabel(schema, attributeKey);
        localisedAccumulator[key] = i18nMessage
          ? intl.formatMessage(i18nMessage)
          : key;
      }

      return localisedAccumulator;
    },
    {}
  );

  return [...attributes].sort((a, b) => {
    if (
      (isCustomAttribute(a) && !isCustomAttribute(b)) ||
      b.key.endsWith("$entities")
    ) {
      return 1;
    }

    if (
      (!isCustomAttribute(a) && isCustomAttribute(b)) ||
      a.key.endsWith("$entities")
    ) {
      return -1;
    }

    return localisedStrings[a.key].localeCompare(localisedStrings[b.key]);
  });
}

export function sortLocalisedGroups(groups, session, intl) {
  const localisedStrings = groups.reduce(
    (accumumulator, { key, path, groupLabel }) => {
      if (!path.length) return accumumulator;

      if (groupLabel) {
        accumumulator[key] = groupLabel;
      } else {
        const combinedLabel = path.reduce(
          (combinedAccumulator, { key: pathKey, model }) => {
            let candidateKey = pathKey;
            const details = getPolymorphicRelationDetails(candidateKey);
            if (details) {
              candidateKey = details.name;
            }
            const keys = candidateKey.split(".");
            const schema = session.schemas.find(
              (candidate) => candidate.id === model
            );
            const i18nMessage = getLocalisedLabel(
              schema,
              keys[keys.length - 1]
            );
            combinedAccumulator.push(
              i18nMessage ? intl.formatMessage(i18nMessage) : key
            );

            return combinedAccumulator;
          },
          []
        );
        accumumulator[key] = combinedLabel.join(" ");
      }
      return accumumulator;
    },
    {}
  );

  return [...groups].sort((a, b) => {
    if (!localisedStrings[b.key]) {
      return 1;
    }

    if (!localisedStrings[a.key]) {
      return -1;
    }

    return localisedStrings[a.key].localeCompare(localisedStrings[b.key]);
  });
}

export function AttributeGroupLabel({ path, label }) {
  return (
    <span>
      {label ||
        path.reduce(
          (accumulator, { key: candidateKey, model, label }, index) => {
            let key = candidateKey;
            const details = getPolymorphicRelationDetails(key);
            if (details) {
              key = details.name;
            }
            const keys = key.split(".");
            accumulator.push(
              <AttributeLabelItem
                key={`${model}-${key}`}
                attribute={label || keys[keys.length - 1]}
                entityType={model}
              />
            );
            if (index < path.length - 1) {
              accumulator.push(
                <span key={`${model}-${key}-separator`}> </span>
              );
            }
            return accumulator;
          },
          []
        )}
    </span>
  );
}
AttributeGroupLabel.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  path: PropTypes.array.isRequired,
  label: PropTypes.string,
};
AttributeGroupLabel.defaultProps = {
  path: [],
};

export function AttributeLabel_({
  attribute,
  enableGroup,
  intl,
  transform,
  className,
}) {
  if (!attribute) {
    return <span>{intl.formatMessage(messages["unknown-attribute"])}</span>;
  }

  const { path, key, model, label } = attribute;
  const fragments = key.split(".");

  let groupElements = null;
  if (enableGroup && path && path.length) {
    groupElements = [
      <AttributeGroupLabel key="group" path={path} />,
      <span key="space"> </span>,
    ];
  }

  let labelElement;
  if (label) {
    labelElement = <span>{label}</span>;
  } else {
    labelElement = (
      <AttributeLabelItem
        key={`${model}-${key}`}
        attribute={fragments[fragments.length - 1]}
        entityType={model}
      />
    );
  }

  const className_ = classNames(
    style["attribute-label"],
    {
      [style["attribute-label-capitalize"]]: transform === "capitalize",
      [style["attribute-label-uppercase"]]: transform === "uppercase",
    },
    className
  );

  return (
    <div className={className_}>
      {groupElements}
      {labelElement}
    </div>
  );
}
AttributeLabel_.propTypes = {
  intl: intlShape.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  attribute: attributeShape.isRequired,
  className: PropTypes.string,
  enableGroup: PropTypes.bool,
  transform: PropTypes.oneOf(["capitalize", "uppercase"]),
};

AttributeLabel_.defaultProps = {
  enableGroup: false,
  transform: "capitalize",
};

export const AttributeLabel = safeInjectIntl(AttributeLabel_);
