// :copyright: Copyright (c) 2017 ftrack

/* eslint-disable react/no-find-dom-node */
/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/no-multi-comp */

import PropTypes from "prop-types";

import { createElement, Component } from "react";
import { connect } from "react-redux";
import { createSelector } from "reselect";
import debounce from "lodash/debounce";
import {
  put,
  getContext,
  cancelled,
  select,
  call,
  all,
  fork,
  take,
  race,
} from "redux-saga/effects";
import { takeLatest, takeEvery } from "redux-saga";
import log from "loglevel";
import isArray from "lodash/isArray";
import isEqual from "lodash/isEqual";
import uniq from "lodash/uniq";
import flatten from "lodash/flatten";
import cloneDeep from "lodash/cloneDeep";
import zip from "lodash/zip";

import { operation } from "ftrack-javascript-api";

import { attributeShape } from "./util";

import { ATTRIBUTE_LOADING } from "./symbol";

const DELAY_FILTER_CHANGED_LOAD_DATA = 500;

const LOAD_DATA = "LOAD_DATA";
const RELOAD_DATA = "RELOAD_DATA";
const LOAD_DATA_DONE = "LOAD_DATA_DONE";
const LOAD_DATA_FAILED = "LOAD_DATA_FAILED";
const LOAD_DATA_CANCELLED = "LOAD_DATA_CANCELLED";
const SORT_DATA = "SORT_DATA";
const LOAD_DATA_ITEM = "LOAD_DATA_ITEM";
const LOAD_DATA_ITEM_DONE = "LOAD_DATA_ITEM_DONE";
const LOAD_DELAYED_ATTRIBUTE_DONE = "LOAD_DELAYED_ATTRIBUTE_DONE";

export function reloadData(payload) {
  return {
    type: RELOAD_DATA,
    payload,
  };
}

export function loadData(payload) {
  return {
    type: LOAD_DATA,
    payload,
  };
}

export function loadDataDone(payload) {
  return {
    type: LOAD_DATA_DONE,
    payload,
  };
}

export function loadDataFailed(payload) {
  return {
    type: LOAD_DATA_FAILED,
    payload,
  };
}

export function loadDataCancelled(payload) {
  return {
    type: LOAD_DATA_CANCELLED,
    payload,
  };
}

export function sortData(payload) {
  return {
    type: SORT_DATA,
    payload,
  };
}

export function loadDataItem(payload) {
  return {
    type: LOAD_DATA_ITEM,
    payload,
  };
}

export function loadDataItemDone(payload) {
  return {
    type: LOAD_DATA_ITEM_DONE,
    payload,
  };
}

export function loadDelayedAttributeDone(payload) {
  return {
    type: LOAD_DELAYED_ATTRIBUTE_DONE,
    payload,
  };
}

export const actions = {
  LOAD_DATA,
  LOAD_DELAYED_ATTRIBUTE_DONE,
  LOAD_DATA_DONE,
  LOAD_DATA_CANCELLED,
  LOAD_DATA_FAILED,
  RELOAD_DATA,
  SORT_DATA,
  LOAD_DATA_ITEM,
  LOAD_DATA_ITEM_DONE,
};

// eslint-disable-next-line
function processData(data, projections, path = "") {
  if (!data) {
    return data;
  }
  Object.keys(data).forEach((key) => {
    const fullPath = `${path}${key}`;
    if (
      projections.some((projection) => projection.startsWith(`${fullPath}.`))
    ) {
      // Partial path found, continue traverse.
      if (isArray(data[key])) {
        data[key].forEach((item) =>
          processData(item, projections, `${fullPath}.`)
        );
      } else {
        processData(data[key], projections, `${fullPath}.`);
      }
    } else if (!projections.some((projection) => projection === fullPath)) {
      delete data[key];
    }
  });
}

function buildQuery(
  model,
  attributes,
  sortAttribute,
  sortDirection,
  filters,
  offset,
  limit
) {
  let query = `select ${attributes.join(", ")} from ${model}`;
  if (filters) {
    query += ` where ${filters}`;
  }

  if (sortAttribute) {
    if (isArray(sortAttribute)) {
      query += ` order by ${sortAttribute
        .map((item) => `${item} ${sortDirection}`)
        .join(", ")}`;
    } else {
      query += ` order by ${sortAttribute} ${sortDirection}`;
    }
  }

  query += ` offset ${offset} limit ${limit}`;

  return query;
}

function* reloadDataHandler(action) {
  const params = yield select((state) => state.params);
  if (params) {
    yield put(
      loadData({
        model: params.model,
        sortAttribute: params.sortAttribute,
        sortDirection: params.sortDirection,
        attributes: params.attributes,
        filters: params.filters,
        loading: true,
        nextOffset: 0,
        ...action.payload,
      })
    );
  }
}

const processAttribute = (attribute, data) => {
  let value;
  if (!isArray(attribute.projection)) {
    const attributeFragments = attribute.projection.split(".");
    value = attributeFragments.reduce((accumulator, fragment) => {
      if (accumulator !== undefined && accumulator !== null) {
        return accumulator[fragment];
      }
      return undefined;
    }, data);
  } else {
    // Process a multi projection attribute.

    // Inspect the key to find the relation.
    const relation = attribute.key.split(".");
    relation.pop();

    // Traverse the relation to find the target value.
    value = relation.reduce((accumulator, fragment) => {
      if (accumulator !== undefined && accumulator !== null) {
        return accumulator[fragment];
      }
      return undefined;
    }, data);

    // Loop projections and remove relation up to the attributes'
    // projection.
    const projections = [];
    attribute.projection.forEach((item) => {
      const projectionFragments = item.split(".");
      projectionFragments.splice(0, relation.length);
      projections.push(projectionFragments.join("."));
    });

    // Clone the value before removing all attributes that are not part
    // of the attribute.
    value = cloneDeep(value);

    // Now clean the data to garantuee that no data is passed to the
    // formatter that was not asked for.
    processData(value, projections);
  }
  if (attribute.transform) {
    value = attribute.transform(value);
  }

  return value;
};

/** Resolve rollup on *attribute* for *entities* */
function* resolveRollup(attribute, entities) {
  const session = yield getContext("ftrackSession");

  const response = yield session.call([
    {
      action: "compute_rollups",
      data: entities,
      rollup: "latest_version",
    },
  ]);

  const data = zip(response[0].data, entities).map(
    ([attributeData, entity]) => ({
      [attribute.key]: attributeData,
      ...entity,
    })
  );

  yield put(
    loadDelayedAttributeDone({
      rows: data,
    })
  );
}

function* resolveCustomAttributeLinks(attributes, entities) {
  const session = yield getContext("ftrackSession");
  const linkedEntities = entities.map((e) => `'${e.__entity_id__}'`).join(",");

  if (linkedEntities && linkedEntities.length > 0) {
    const operations = attributes.map(({ options, projection }) => {
      const isIncomingLink = options.isIncomingLink;
      const linkDirection = isIncomingLink ? "to_id" : "from_id";
      return operation.query(`
                select from_id, to_id, configuration_id, ${projection.join(",")}
                from ${options.linkRelation}
                where (${linkDirection} in (${linkedEntities}))
                and configuration_id is '${options.configuration_id}'`);
    });

    const response = yield session.call(operations);
    // TODO: this is O(entities * attributes^2) and might explode CPU wise
    // when entities and attributes are too many, find a way to reduce the complexity
    const rows = entities.map((entity) =>
      attributes.reduce((acc, { options, key }, i) => {
        const attributeResponse = response[i].data;
        const result = attributeResponse.filter((attribute) => {
          const isMatchingConfigurationId =
            attribute.configuration_id === options.configuration_id;
          let isMatchingAttributeId =
            attribute.from_id === entity.__entity_id__;
          if (options.isIncomingLink) {
            isMatchingAttributeId = attribute.to_id === entity.__entity_id__;
          }
          return isMatchingAttributeId && isMatchingConfigurationId;
        });
        return { ...acc, [key]: result };
      }, entity)
    );

    yield put(loadDelayedAttributeDone({ rows }));
  }
}

/** Fetch data from server. */
function* fetchData(
  attributes,
  model,
  sortAttribute,
  sortDirection,
  filters,
  offset = 0,
  limit = 25
) {
  const session = yield getContext("ftrackSession");

  const batches = [];
  const mainBatch = {
    keys: [],
    projection: [],
  };
  const delayedAttributes = [];

  attributes.forEach((attribute) => {
    if (attribute.options && attribute.options.loading === "split-batch") {
      batches.push({
        keys: [attribute.key],
        projection: isArray(attribute.projection)
          ? attribute.projection
          : [attribute.projection],
      });
    } else if (
      attribute.options &&
      (attribute.options.loading === "rollup" ||
        attribute.options.loading === "custom_attribute_links")
    ) {
      // Do nothing, rollups are fetched separately.
      delayedAttributes.push(attribute.key);
    } else {
      mainBatch.keys.push(attribute.key);
      mainBatch.projection.push(
        ...(isArray(attribute.projection)
          ? attribute.projection
          : [attribute.projection])
      );
    }
  });

  if (mainBatch.keys.length) {
    batches.push(mainBatch);
  }

  const operations = batches.map((batch) =>
    operation.query(
      buildQuery(
        model,
        batch.projection,
        sortAttribute,
        sortDirection,
        filters,
        offset,
        limit
      )
    )
  );
  const primaryKeyNames = session.getPrimaryKeyAttributes(model);

  log.debug("Query operations", operations);

  const fetchResponse = session.call(operations);
  const responses = yield fetchResponse;

  log.debug("Query response", responses);
  const processedData = responses[0].data.reduce(
    (accumulator, row) => {
      const data = {};
      data.__entity_id__ = primaryKeyNames.map((key) => row[key]).join(",");
      data.__entity_type__ = row.__entity_type__;
      accumulator.map[data.__entity_id__] = data;
      accumulator.rows.push(data);
      return accumulator;
    },
    {
      rows: [],
      map: {},
    }
  );
  responses.forEach((response, index) => {
    response.data.forEach((row) => {
      const entityId = primaryKeyNames.map((key) => row[key]).join(",");
      const processed = processedData.map[entityId];
      attributes.forEach((attribute) => {
        if (batches[index].keys.includes(attribute.key)) {
          processed[attribute.key] = processAttribute(attribute, row);
        }
      });
    });
  });

  delayedAttributes.forEach((key) =>
    processedData.rows.forEach((row) => {
      row[key] = ATTRIBUTE_LOADING;
    })
  );

  return {
    rows: processedData.rows,
    nextOffset: responses[0].metadata.next.offset,
  };
}

function* loadDataItemHandler(action) {
  const { id } = action.payload;
  const { attributes, model } = yield select((state) => state.params);
  const {
    rows: [row],
  } = yield fetchData(attributes, model, null, null, `id is "${id}"`, 0);
  if (row) {
    yield put(loadDataItemDone({ row, attributes }));
  } else {
    log.debug("Could not load data item for ", action);
  }
}

function* loadDataHandler(action) {
  const model = action.payload.model;
  const sortAttribute = action.payload.sortAttribute;
  const sortDirection = action.payload.sortDirection;
  const attributes = action.payload.attributes;
  const offset = action.payload.nextOffset;
  const limit = action.payload.limit;
  const discreet = action.payload.discreet;
  const isAppend = offset > 0;
  const filters = action.payload.filters;
  try {
    const { rows, nextOffset } = yield fetchData(
      attributes,
      model,
      sortAttribute,
      sortDirection,
      filters,
      offset,
      limit
    );

    yield put(
      loadDataDone({
        attributes,
        rows,
        nextOffset,
        append: isAppend,
        discreet,
      })
    );
  } catch (error) {
    log.error("Error when loading data", error);
    yield put(
      loadDataFailed({
        append: isAppend,
        discreet,
        error,
      })
    );
  } finally {
    const isCancelled = yield cancelled();
    if (isCancelled) {
      yield put(
        loadDataCancelled({
          append: isAppend,
          discreet,
        })
      );
    }
  }
}

function* loadDelayedData(attributes, rows) {
  log.debug("Handling action for delayed load of data", attributes, rows);

  const entities = rows.map(({ __entity_id__, __entity_type__ }) => ({
    __entity_id__,
    __entity_type__,
  }));

  const delayed = attributes
    .filter(
      (attribute) => attribute.options && attribute.options.loading === "rollup"
    )
    .map((attribute) => call(resolveRollup, attribute, entities));

  const delayedLinkAttributes = attributes.filter(
    (attribute) =>
      attribute.options &&
      attribute.options.loading === "custom_attribute_links"
  );

  if (delayedLinkAttributes.length > 0 && entities.length > 0) {
    delayed.push(
      call(resolveCustomAttributeLinks, delayedLinkAttributes, entities)
    );
  }

  log.debug("Delayed data identified", delayed);

  yield race({
    // The delayed load of data should cancel if the dataview reloads. This
    // is indicated by the nextOffset being set to 0.
    dataviewReloaded: take(
      (loadDataAction) =>
        loadDataAction.type === actions.LOAD_DATA_DONE &&
        loadDataAction.payload.nextOffset === 0
    ),
    delayedDataDone: fork(all, delayed),
  });
}

export function* loadDelayedDataHandler(action) {
  const { attributes, rows } = action.payload;

  yield loadDelayedData(attributes, rows);
}

export function* loadDelayedItemDataHandler(action) {
  const { attributes, row } = action.payload;
  if (attributes) {
    yield loadDelayedData(attributes, [row]);
  }
}

/** Handle LOAD_DATA action. */
export function* loadDataSaga() {
  yield takeLatest(actions.LOAD_DATA, loadDataHandler);
}

/** Handle RELOAD_DATA action. */
export function* reloadDataSaga() {
  yield takeLatest(actions.RELOAD_DATA, reloadDataHandler);
}

/** Handle LOAD_DATA_ITEM action. */
export function* loadDataItemSaga() {
  yield takeEvery(actions.LOAD_DATA_ITEM, loadDataItemHandler);
}

/** Handle LOAD_DATA_DONE action and load delayed data. */
export function* loadDelayedDataSaga() {
  yield takeEvery(actions.LOAD_DATA_DONE, loadDelayedDataHandler);
}

/** Handle LOAD_DATA_ITEM_DONE action and load delayed data. */
export function* loadDelayedItemDataSaga() {
  yield takeEvery(actions.LOAD_DATA_ITEM_DONE, loadDelayedItemDataHandler);
}

export const sagas = [
  loadDataSaga,
  reloadDataSaga,
  loadDataItemSaga,
  loadDelayedDataSaga,
  loadDelayedItemDataSaga,
];

export function reducer(state = {}, action) {
  let nextState = state;
  if (action.type === actions.LOAD_DATA_DONE) {
    let rows = [];
    if (action.payload.append === true) {
      rows = [...state.rows, ...action.payload.rows];
    } else {
      rows = action.payload.rows;
    }
    nextState = {
      ...state,
      loading: false,
      rows,
      nextOffset: action.payload.nextOffset,
    };
  } else if (action.type === actions.LOAD_DATA) {
    nextState = {
      ...state,
      params: {
        // Params are used to reload data.
        sortAttribute: action.payload.sortAttribute,
        sortDirection: action.payload.sortDirection,
        attributes: action.payload.attributes,
        filters: action.payload.filters,
        model: action.payload.model,
      },
      model: action.payload.model,
      loading: action.payload.loading,
      rows:
        action.payload.nextOffset > 0 || action.payload.discreet
          ? state.rows
          : [],
    };
  } else if (action.type === actions.SORT_DATA) {
    const sortOrder = action.payload.sortOrder;

    const rows = [...state.rows].sort(
      (a, b) =>
        sortOrder.indexOf(a.__entity_id__) - sortOrder.indexOf(b.__entity_id__)
    );

    nextState = {
      ...state,
      rows,
    };
  } else if (action.type === actions.LOAD_DATA_ITEM_DONE) {
    const {
      row: newRow,
      refreshOnly = false,
      mergeChanges = false,
    } = action.payload;
    const rows = [...state.rows];
    const rowIndexToReplace = rows.findIndex(
      (row) => row.__entity_id__ === newRow.__entity_id__
    );

    if (rowIndexToReplace !== -1 && mergeChanges) {
      rows[rowIndexToReplace] = Object.assign(
        {},
        rows[rowIndexToReplace],
        newRow
      );
    } else if (rowIndexToReplace !== -1) {
      rows[rowIndexToReplace] = newRow;
    } else if (!refreshOnly) {
      // Add new entity at the start of the list.
      rows.unshift(newRow);
    }

    nextState = {
      ...state,
      rows,
    };
  } else if (action.type === actions.LOAD_DELAYED_ATTRIBUTE_DONE) {
    const { rows: updateRows } = action.payload;
    const rows = [...state.rows];

    updateRows.forEach((updateRow) => {
      const index = rows.findIndex(
        (row) => row.__entity_id__ === updateRow.__entity_id__
      );
      // Prepare data and update the row.
      const newData = { ...updateRow };
      delete newData.__entity_id__;
      delete newData.__entity_type__;
      if (index !== -1) {
        rows[index] = {
          ...rows[index],
          ...updateRow,
        };
      }
    });

    nextState = {
      ...state,
      rows,
    };
  }

  return nextState;
}

class DataProvider extends Component {
  constructor() {
    super();
    this.onFetchData = this.onFetchData.bind(this);
    this.onFetchMoreData = this.onFetchMoreData.bind(this);
    this.handleSignificantPropsChanged = debounce(
      this.handleSignificantPropsChanged.bind(this),
      DELAY_FILTER_CHANGED_LOAD_DATA
    );
  }

  componentDidMount() {
    this.handleComponentMount();
  }

  componentWillReceiveProps(props) {
    let shouldReload = false;

    if (
      !isEqual(
        uniq(
          flatten(this.props.attributes.map((attribute) => attribute.key))
        ).sort(),
        uniq(flatten(props.attributes.map((attribute) => attribute.key))).sort()
      )
    ) {
      shouldReload = true;
    }

    // TODO: Remove the use of the loader attributes as they are not used.
    // Current implementation only uses the attributes from props and not
    // props.loader.
    const oldAttributes = uniq(flatten(this.props.loader.attributes)).sort();
    const newAttributes = uniq(flatten(props.loader.attributes)).sort();

    if (!isEqual(oldAttributes, newAttributes)) {
      shouldReload = true;
    }

    if (
      !isEqual(props.loader.sortAttribute, this.props.loader.sortAttribute) ||
      props.loader.sortDirection !== this.props.loader.sortDirection
    ) {
      shouldReload = true;
    }

    if (props.loader.filters !== this.props.loader.filters) {
      shouldReload = true;
    }

    if (shouldReload) {
      this.handleSignificantPropsChanged(props.loader);
    }
  }

  componentWillUnmount() {
    this.props.onClearData();
  }

  onFetchMoreData() {
    if (!this.props.loading) {
      this.onFetchData();
    }
  }

  onFetchData(params) {
    const {
      model: newModel,
      sortAttribute: newSortAttribute,
      sortDirection: newSortDirection,
      nextOffset: newNextOffset,
      discreet,
    } = params || {};
    const {
      model,
      sortAttribute,
      sortDirection,
      nextOffset,
      filters,
      pageSize,
    } = this.props.loader;

    const offset = newNextOffset !== undefined ? newNextOffset : nextOffset;

    if (offset === null) {
      return;
    }

    this.props.onFetchData(
      newModel !== undefined ? newModel : model,
      newSortAttribute !== undefined ? newSortAttribute : sortAttribute,
      newSortDirection !== undefined ? newSortDirection : sortDirection,
      filters,
      discreet || false,
      offset,
      pageSize
    );
  }

  handleComponentMount() {
    const { loader } = this.props;
    if (loader.onRef) {
      loader.onRef(this);
    }
    const params = Object.assign({}, this.props.loader, { nextOffset: 0 });
    this.onFetchData(params);
  }

  reload(discreet = false) {
    const params = Object.assign({}, this.props.loader, {
      nextOffset: 0,
      discreet,
    });
    this.onFetchData(params);
  }

  handleSignificantPropsChanged(props) {
    const params = Object.assign({}, props.loader, { nextOffset: 0 });
    this.onFetchData(params);
  }

  render() {
    const { wrapComponent, data, ...props } = this.props;
    delete props.loader;

    const passProps = Object.assign({}, props, {
      data,
      onFetchMoreData: this.onFetchMoreData,
    });
    return createElement(wrapComponent, passProps);
  }
}

DataProvider.propTypes = {
  wrapComponent: PropTypes.element,
  onFetchData: PropTypes.func,
  onRef: PropTypes.func,
  onClearData: PropTypes.func,
  data: PropTypes.array,
  attributes: PropTypes.arrayOf(attributeShape),
  loader: PropTypes.object,
  loading: PropTypes.bool,
};

DataProvider.defaultProps = {};

const getPagedLoader = createSelector(
  [(props) => props.loader, (props, state) => state.nextOffset],
  (loader, nextOffset) => Object.assign({ nextOffset }, loader)
);

function mapStateToProps(state, ownProps) {
  const loader = getPagedLoader(ownProps, state);
  return {
    data: state.rows || [],
    loader,
    loading: state.loading,
  };
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    onFetchData(
      model,
      sortAttribute,
      sortDirection,
      filters,
      discreet,
      nextOffset = 0,
      pageSize = 25
    ) {
      dispatch(
        loadData({
          model,
          sortAttribute,
          sortDirection,
          filters,
          loading: true,
          nextOffset,
          discreet,
          limit: pageSize,
          attributes: ownProps.attributes,
        })
      );
    },
    onClearData() {
      dispatch(
        loadDataDone({
          rows: [],
          attributes: [],
          nextOffset: 0,
          append: false,
          reset: true,
        })
      );
    },
  };
}

const ConnectedDataProvider = connect(
  mapStateToProps,
  mapDispatchToProps
)(DataProvider);

export function withDataLoader(wrapComponent) {
  return function WrappedComponent(props) {
    return <ConnectedDataProvider {...props} wrapComponent={wrapComponent} />;
  };
}
