// :copyright: Copyright (c) 2018 ftrack

import PropTypes from "prop-types";

import { Component } from "react";
import classNames from "classnames";

import withMonitorKeyPress from "../util/hoc/with_monitor_key_events";
import style from "./style.scss";

export function uniqueAndSortedArray(arrayToSort) {
  return [...new Set(arrayToSort.sort((a, b) => a - b))];
}

export function withSelectionHandler() {
  return function wrapComponent(InnerComponent) {
    class WrappedComponent extends Component {
      constructor(props) {
        super(props);

        this.state = {
          lastSelectedIndex: null,
        };

        this.handleCheckboxClicked = this.handleCheckboxClicked.bind(this);
      }

      getShiftSelection(checked, clickedIndex, previouslySelected) {
        let selectedIndexes = [];
        const { lastSelectedIndex } = this.state;
        const max = Math.max(clickedIndex, lastSelectedIndex);
        const min = Math.min(clickedIndex, lastSelectedIndex);
        const length = max - min + 1;
        const shiftSelectedIndexes = uniqueAndSortedArray(
          [...Array(length).keys()].map(
            (element, arrayIndex) => min + arrayIndex
          )
        );

        if (checked) {
          selectedIndexes = [
            ...previouslySelected.filter((item) => item !== clickedIndex),
            ...shiftSelectedIndexes,
          ];
        } else {
          selectedIndexes = Array.from(shiftSelectedIndexes);

          /**
           * Get indexes that are previously selected and not included in new shift-click.
           */
          const shiftSelectedIndexesSet = new Set([...shiftSelectedIndexes]);
          const difference = previouslySelected.filter(
            (cardIndex) => !shiftSelectedIndexesSet.has(cardIndex)
          );

          /**
           * Get sequences of numbers
           */
          const sequences = [];
          let sequence = [];
          difference.forEach((cardIndex, differenceArrayIndex) => {
            /**
             * If index is +1 from previous index or the first element, it should be
             * a part of an ongoing sequence.
             */
            if (
              differenceArrayIndex === 0 ||
              cardIndex === Math.max(...sequence) + 1
            ) {
              sequence.push(cardIndex);
            } else {
              /**
               * If not part of a sequence with the previous index, count it
               * as start of a new sequence.
               */
              sequences.push(Array.from(sequence));
              sequence = [cardIndex];
            }
          });

          /**
           * Push the last ongoing sequence to the sequences array.
           */
          sequences.push(Array.from(sequence));

          /**
           * For every sequence, check if they are neighbors to the last shift selection.
           * If they are, the should not be selected.
           */
          sequences.forEach((indexSequence) => {
            const sequenceMax = Math.max(...indexSequence);
            const sequenceMin = Math.min(...indexSequence);
            if (
              Math.abs(sequenceMax - max) !== 1 &&
              Math.abs(sequenceMax - min) !== 1 &&
              Math.abs(sequenceMin - max) !== 1 &&
              Math.abs(sequenceMin - min) !== 1
            ) {
              selectedIndexes = selectedIndexes.concat(indexSequence);
            }
          });
        }

        return selectedIndexes;
      }

      handleCheckboxClicked(checked, clickedIndex) {
        const { onSelectionChanged, shiftIsPressed, selected, data } =
          this.props;

        const dataIds = data.map((item) => item.__entity_id__);
        const previouslySelected = selected.reduce(
          (accumulator, identifier) => {
            const index = dataIds.indexOf(identifier);
            if (index !== -1) {
              accumulator.push(index);
            }

            return accumulator;
          },
          []
        );
        let selectedIndexes = [];
        let lastSelectedIndex = this.state.lastSelectedIndex;

        if (shiftIsPressed) {
          selectedIndexes = this.getShiftSelection(
            checked,
            clickedIndex,
            previouslySelected
          );
        } else {
          selectedIndexes = checked
            ? [...previouslySelected, clickedIndex]
            : previouslySelected.filter((item) => item !== clickedIndex);
          /**
           * Keep track of the last item that was checked/unchecked without pressing
           * shift at the same time to be able to decide what should be selected on the next
           * Shift-Click.
           */
          lastSelectedIndex = clickedIndex;
        }

        selectedIndexes = uniqueAndSortedArray(selectedIndexes);

        this.setState({
          lastSelectedIndex,
        });
        onSelectionChanged(
          selectedIndexes.map((index) => data[index].__entity_id__)
        );
      }

      render() {
        const { shiftIsPressed, className, ...props } = this.props;

        return (
          <InnerComponent
            {...props}
            className={classNames(className, {
              [style["disable-text-selection"]]: shiftIsPressed,
            })}
            onCheckboxClicked={this.handleCheckboxClicked}
          />
        );
      }
    }

    WrappedComponent.propTypes = {
      onSelectionChanged: PropTypes.func.isRequired,
      shiftIsPressed: PropTypes.bool.isRequired,
      selected: PropTypes.array.isRequired,
      className: PropTypes.string,
      data: PropTypes.array.isRequired,
    };

    WrappedComponent.defaultTyps = {
      selected: [],
    };

    return withMonitorKeyPress({
      16: "shiftIsPressed",
    })(WrappedComponent);
  };
}
