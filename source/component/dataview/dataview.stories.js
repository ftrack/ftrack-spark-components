import { Component } from "react";

import { action } from "@storybook/addon-actions";

import { Provider as ReduxProvider } from "react-redux";
import Button from "react-toolbox/lib/button";
import ProgressBar from "react-toolbox/lib/progress_bar";

import configureStore from "../../story/configure_store.js";
import getSession from "../../story/get_session";
import getAttributes from "./util";

import {
  withSubspace,
  SparkProvider,
  withAutoSize,
  SettingsProvider,
} from "../util/hoc";
import { reducer, sagas, withDataLoader } from "./container";

import Card from "./card";
import Grid from "./grid";

const DataGrid = withDataLoader(Grid);
const DataCard = withDataLoader(Card);

const AutoSizeGrid = withAutoSize()(DataGrid);

const onItemClickedAction = action("Entity link clicked");
const selectionChanged = action("Selection changed");

function Provider({ story, store, session }) {
  return (
    <SparkProvider session={session}>
      <ReduxProvider store={store}>{story}</ReduxProvider>
    </SparkProvider>
  );
}

const store = configureStore((state = {}) => state, [], getSession());

export default {
  title: "Dataview",
  decorators: [
    (Story) => (
      <Provider story={<Story />} store={store} session={getSession()} />
    ),
  ],
};

const DataGridDefault = withSubspace(
  reducer,
  sagas,
  "grid-view-default"
)(AutoSizeGrid);
const DataCardDefault = withSubspace(
  reducer,
  sagas,
  "card-view-default"
)(DataCard);

class SizedTestDataView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      attributeSource: null,
      selected: [],
    };
    this.onReload = this.onReload.bind(this);
    this.onRef = this.onRef.bind(this);
    this.onSelectionChanged = this.onSelectionChanged.bind(this);
  }

  componentDidMount() {
    const { model, relations, session } = this.props;
    session.initializing
      .then(() => getAttributes(model, relations, session))
      .then((attributeSource) => {
        this.setState({
          attributeSource,
        });
      });
  }

  onRef(node) {
    this.loader = node;
  }

  onReload() {
    this.loader.reload();
  }

  onSelectionChanged(selected) {
    selectionChanged(selected);
    this.setState({ selected });
  }

  render() {
    const { attributeSource, selected } = this.state;
    const { model, mode, selectedAttributes, sort, selectable } = this.props;
    let element;
    if (!attributeSource) {
      element = <ProgressBar mode="indeterminate" />;
    } else {
      const attributes = selectedAttributes.map(
        (item) => attributeSource.attributes[item]
      );

      let modeProps;
      let View;
      if (mode === "card") {
        View = DataCardDefault;

        modeProps = {
          subtitleAttributes: this.props.subtitleAttributes,
          titleAttribute: this.props.titleAttribute,
          colorAttribute: this.props.colorAttribute,
          thumbnail: this.props.thumbnail,
          selectable,
          disableThumbnailClick: this.props.disableThumbnailClick,
        };
      } else {
        View = DataGridDefault;

        modeProps = {
          rowHeight: 100,
        };
      }
      element = (
        <div
          style={{
            overflow: "auto",
            position: "fixed",
            top: "0px",
            left: "0px",
            bottom: "0px",
            right: "0px",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <div style={{ margin: "10px", width: "100%" }}>
            <Button label="Reload" onClick={this.onReload} />
            <View
              attributes={attributes}
              onItemClicked={onItemClickedAction}
              onSelectionChanged={this.onSelectionChanged}
              selected={selected}
              model={model}
              {...modeProps}
              loader={{
                onRef: this.onRef,
                model,
                filters: this.props.filters || null,
                sortAttribute: sort,
                sortDirection: "ASC",
              }}
            />
          </div>
        </div>
      );
    }

    return element;
  }
}

export const DataviewTaskGrid = () => (
  <SettingsProvider settings={{ workdayLength: 10 * 3600 }}>
    <SizedTestDataView
      model="Task"
      mode="grid"
      sort="link"
      selectedAttributes={[
        "thumbnail_",
        "name",
        "$status",
        "$type",
        "$priority",
        "link",
        "$linkedTo",
        "description",
        "type.is_billable",
        "status.name",
        "bid",
      ]}
      relations={["status", "status.state", "type", "parent"]}
      session={getSession()}
    />
  </SettingsProvider>
);

DataviewTaskGrid.storyName = "Dataview task grid";

DataviewTaskGrid.parameters = {
  info: "This example shows a task dataview component.",
};

export const DataviewUserGrid = () => (
  <SettingsProvider>
    <SizedTestDataView
      model="User"
      mode="grid"
      sort="first_name"
      selectedAttributes={["thumbnail_", "first_name", "is_active"]}
      relations={[]}
      session={getSession()}
    />
  </SettingsProvider>
);

DataviewUserGrid.storyName = "Dataview user grid";

DataviewUserGrid.parameters = {
  info: "This example shows a user dataview component.",
};

export const DataviewTaskCard = () => (
  <SizedTestDataView
    model="Task"
    mode="card"
    sort="link"
    thumbnail="thumbnail_id"
    titleAttribute="name"
    subtitleAttributes={["$linkedTo"]}
    selectedAttributes={[
      "$linkedTo",
      "$status",
      "$scope",
      "name",
      "$assignees",
      "bid",
      "end_date",
      "thumbnail_id",
      "type.is_billable",
      "status.name",
    ]}
    relations={["status", "status.state", "type", "parent"]}
    session={getSession()}
  />
);

DataviewTaskCard.storyName = "Dataview task card";

DataviewTaskCard.parameters = {
  info: "This example shows a task dataview component.",
};

export const DataviewAssetVersionCard = () => (
  <SizedTestDataView
    model="AssetVersion"
    mode="card"
    sort="link"
    thumbnail="thumbnail_id"
    titleAttribute="$name"
    subtitleAttributes={["$linkedTo"]}
    selectedAttributes={[
      "$name",
      "$linkedTo",
      "comment",
      "link",
      "$user",
      "asset.parent.link",
      "task.$assignees",
      "task.$status",
      "thumbnail_id",
    ]}
    relations={["asset", "task", "asset.parent"]}
    session={getSession()}
  />
);

DataviewAssetVersionCard.storyName = "Dataview asset version card";

DataviewAssetVersionCard.parameters = {
  info: "This example shows a assetv version dataview component.",
};

export const DataviewShotCard = () => (
  <SizedTestDataView
    model="Shot"
    mode="card"
    sort="link"
    thumbnail="thumbnail_id"
    titleAttribute="link"
    subtitleAttributes={["status.name"]}
    selectedAttributes={[
      "link",
      "$status",
      "$priority",
      "$objectType",
      "thumbnail_id",
      "status.name",
    ]}
    relations={["status", "status.state", "parent"]}
    session={getSession()}
  />
);

DataviewShotCard.storyName = "Dataview shot card";

DataviewShotCard.parameters = {
  info: "This example shows a shot dataview component.",
};

export const DataviewTaskCardWithSelection = () => (
  <SizedTestDataView
    model="Task"
    mode="card"
    sort="link"
    thumbnail="thumbnail_id"
    titleAttribute="link"
    selectable
    subtitleAttributes={["bid"]}
    selectedAttributes={[
      "link",
      "bid",
      "thumbnail_id",
      "type.name",
      "status.name",
    ]}
    relations={["status", "status.state", "type", "parent"]}
    session={getSession()}
  />
);

DataviewTaskCardWithSelection.storyName = "Dataview task card with selection";

DataviewTaskCardWithSelection.parameters = {
  info: "This example shows a task dataview component.",
};

export const DataviewProjectCardWithSelection = () => (
  <SizedTestDataView
    model="Project"
    mode="card"
    sort="link"
    thumbnail="thumbnail_id"
    titleAttribute="link"
    selectable
    subtitleAttributes={["full_name"]}
    colorAttribute="color"
    selectedAttributes={[
      "thumbnail_id",
      "color",
      "link",
      "start_date",
      "end_date",
      "status",
      "$project_schema_name",
      "$managers",
      "full_name",
    ]}
    relations={[]}
    session={getSession()}
  />
);

DataviewProjectCardWithSelection.storyName =
  "Dataview project card with selection";

DataviewProjectCardWithSelection.parameters = {
  info: "This example shows a project dataview component.",
};
export const DataviewUserCardWithSelection = () => (
  <SizedTestDataView
    model="User"
    mode="card"
    sort="first_name"
    thumbnail="thumbnail_id"
    subtitleAttributes={["username"]}
    titleAttribute="$user"
    disableThumbnailClick
    selectedAttributes={[
      "thumbnail_id",
      "first_name",
      "last_name",
      "email",
      "username",
      "$user",
      "is_active",
      "username",
    ]}
    relations={[]}
    session={getSession()}
  />
);

DataviewUserCardWithSelection.storyName = "Dataview user card with selection";

DataviewUserCardWithSelection.parameters = {
  info: "This example shows a user dataview component.",
};

export const DataviewTaskGridCustomDelayed = () => (
  <SettingsProvider settings={{ workdayLength: 10 * 3600 }}>
    <SizedTestDataView
      model="Task"
      mode="grid"
      sort="link"
      selectedAttributes={[
        "thumbnail_",
        "name",
        "$latestVersions",
        "link",
        "$linkedTo",
        "description",
        "type.is_billable",
        "status.name",
        "bid",
      ]}
      relations={["status", "status.state", "type", "parent"]}
      session={getSession()}
      filters="id in (select task_id from AssetVersion)"
    />
  </SettingsProvider>
);

DataviewTaskGridCustomDelayed.storyName = "Dataview task grid custom delayed";

DataviewTaskGridCustomDelayed.parameters = {
  info: "This example shows a task dataview component.",
};
