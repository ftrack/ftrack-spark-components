import { action } from "@storybook/addon-actions";
// eslint-disable-next-line
import centered from "@storybook/addon-centered/react";
import StatusToggle from ".";

export default {
  title: "Review status toggle",
  decorators: [centered],
};

const clicked = action("Toggle clicked");

export const Default = () => <StatusToggle onChange={clicked} />;

Default.parameters = {
  info: "Status toggle in default state.",
};

export const RequireChangesState = () => (
  <StatusToggle status="require_changes" onChange={clicked} />
);

RequireChangesState.storyName = "Require changes state";

RequireChangesState.parameters = {
  info: "Status toggle in require changes state.",
};

export const ApprovedChangesState = () => (
  <StatusToggle status="approved" onChange={clicked} />
);

ApprovedChangesState.storyName = "Approved changes state";

ApprovedChangesState.parameters = {
  info: "Status toggle in approved state.",
};
