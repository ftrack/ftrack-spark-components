..
    :copyright: Copyright (c) 2018 ftrack

#########################
ReviewSessionStatusToggle
#########################

Component to toggle between require changes and approve.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
status                         One of `approve`, `require_changes` or `seen`.
onApprove                      Callback when approve is clicked.
onRequireChanges               Callback when require changes is clicked.
className                      CSS Class to add to the root element.
