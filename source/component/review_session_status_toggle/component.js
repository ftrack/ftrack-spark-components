// :copyright: Copyright (c) 2018 ftrack

import PropTypes from "prop-types";
import { defineMessages, FormattedMessage } from "react-intl";
import { withHandlers } from "recompose";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import IconButton from "../icon_button";
import style from "./style.scss";

const IconButtonWithValue = withHandlers({
  onClick: (props) => () => props.onChange(props.value),
})(IconButton);

const messages = defineMessages({
  "require-changes": {
    id: "ftrack-spark-components.status-toggle.require-changes",
    description: "Tooltip for require changes button",
    defaultMessage: "Require changes",
  },
  approve: {
    id: "ftrack-spark-components.status-toggle.approve",
    description: "Tooltip for approval button",
    defaultMessage: "Approve",
  },
});

function StatusToggle({ status, onChange, ...props }) {
  return (
    <div {...props}>
      <IconButtonWithValue
        variant="large-icon-negative"
        active={status === "require_changes"}
        icon="clear"
        onChange={onChange}
        value="require_changes"
        className={style.spacing}
        tooltip={<FormattedMessage {...messages["require-changes"]} />}
      />
      <IconButtonWithValue
        variant="large-icon-positive"
        active={status === "approved"}
        icon="check"
        onChange={onChange}
        value="approved"
        tooltip={<FormattedMessage {...messages.approve} />}
      />
    </div>
  );
}

StatusToggle.propTypes = {
  status: PropTypes.oneOf(["require_changes", "approved", "seen"]),
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string,
};

export default safeInjectIntl(StatusToggle);
