..
    :copyright: Copyright (c) 2019 ftrack

##################
Progress Indicator
##################

Progress Indicator component, 
displaying seen items & items with feedback.
