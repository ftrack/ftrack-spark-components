// :copyright: Copyright (c) 2019 ftrack

import PropTypes from "prop-types";
import ProgressBar from "react-toolbox/lib/progress_bar";
import Header from "../heading/component";
import classNames from "classnames";
import safeInjectIntl from "../util/hoc/safe_inject_intl";

import style from "./style.scss";
import progressBarTheme from "./progressbar_theme.scss";
import progressBarThemeApproved from "./progressbar_theme_approved.scss";
import progressBarThemeRequireChanges from "./progressbar_theme_requires_changes.scss";
import progressBarThemeSeen from "./progressbar_theme_seen.scss";

function ProgressIndicator({ variant, className, label, value, ...props }) {
  const progressBarClasses = classNames(style.progressBar, className, {
    [style.primaryBar]: variant === "primary",
    [style.secondaryBar]: variant === "secondary",
  });
  const textClasses = classNames(style.textStyles, className, {
    [style.primaryText]: variant === "primary",
    [style.secondaryText]: variant === "secondary",
    [style.approvedText]: variant === "approved",
    [style.requireChangesText]: variant === "require_changes",
    [style.seenText]: variant === "seen",
  });
  const percentageClasses = classNames(style.percentage, className, {
    [style.primaryPercentage]: variant === "primary",
    [style.secondaryPercentage]: variant === "secondary",
    [style.approvedPercentage]: variant === "approved",
    [style.requireChangesPercentage]: variant === "require_changes",
    [style.seenPercentage]: variant === "seen",
  });

  const theme = () => {
    switch (variant) {
      case "secondary":
        return progressBarTheme;
      case "approved":
        return progressBarThemeApproved;
      case "require_changes":
        return progressBarThemeRequireChanges;
      case "seen":
        return progressBarThemeSeen;
      default:
        return undefined;
    }
  };

  return (
    <div {...props}>
      <Header variant="subheading" className={textClasses}>
        {label}
      </Header>
      <Header variant="subheading" className={percentageClasses}>
        {value}%
      </Header>
      <ProgressBar
        className={progressBarClasses}
        theme={theme()}
        mode="determinate"
        value={value}
      />
    </div>
  );
}

ProgressIndicator.propTypes = {
  variant: PropTypes.oneOf(["primary", "secondary"]),
  className: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.number,
};

export default safeInjectIntl(ProgressIndicator);
