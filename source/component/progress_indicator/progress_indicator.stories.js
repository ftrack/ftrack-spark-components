// :copyright: Copyright (c) 2019 ftrack

import ProgressIndicator from "./component";
import centered from "@storybook/addon-centered/react";
import Header from "../heading/component";

export default {
  title: "Progress Indicator",
  decorators: [centered],
};

function ReviewSessionProgressIndicator() {
  return (
    <div
      style={{
        width: "310px",
      }}
    >
      <Header
        variant="headline"
        style={{
          marginBottom: "1em",
          borderBottom: "1px solid gray",
        }}
      >
        Review items
      </Header>
      <Header variant="subheading" style={{ marginBottom: "20px" }}>
        45 items
      </Header>
      <Header
        style={{
          fontWeight: "bold",
          fontSize: "14px",
          marginBottom: "-25px",
          color: "#B38CBE",
        }}
      >
        Items with feedback
      </Header>
      <ProgressIndicator variant="primary" value={39} />
      <Header
        style={{
          fontWeight: "bold",
          fontSize: "14px",
          marginBottom: "-25px",
          color: "#A9ABAE",
        }}
      >
        Seen
      </Header>
      <ProgressIndicator variant="secondary" value={88} />
    </div>
  );
}

export const ProgressbarWithText = () => (
  <div>
    <ReviewSessionProgressIndicator />
  </div>
);

ProgressbarWithText.storyName = "Progressbar with text";
