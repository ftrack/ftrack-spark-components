import { action } from "@storybook/addon-actions";
import centered from "@storybook/addon-centered/react";

import EntityListItem, {
  StudioEntityListItem,
  ReviewEntityListItem,
  EntityListItemRow,
  EntityListItemContent,
  EntityStatusBar,
  ListItemChip,
} from ".";

const FixedWidth = (Story) => (
  <div style={{ width: "36rem", border: "2px dashed" }}>
    <Story />
  </div>
);

export default {
  title: "Entity List Item",
  decorators: [FixedWidth, centered],
};

const onItemClicked = action("onClick activated");
const onViewInStudioClicked = action("onViewInStudio clicked");

const exampleMedia = {
  id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
  entity: {
    id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
    type: "AssetVersion",
  },
  components: [
    {
      id: "5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c",
      name: "ftrackreview-mp4",
      type: "video",
      frameRate: 24,
      frameIn: 0,
      frameOut: 115.992,
      encoding: false,
      playable: true,
      value:
        "http://localhost/component/get?id=5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c&signature=0KzIs1YODoLmh3cOJcSzf7DVTwRjrSXV4i_xJ4ip2Pc%3D",
      playableProblems: [],
      fileType: ".mp4",
    },
    {
      id: "450b6e25-2986-4887-a451-4e693b86b185",
      name: "01_035_a_anim_v01",
      type: "video",
      frameRate: 1,
      frameIn: 0,
      frameOut: 0,
      encoding: false,
      playable: false,
      value: null,
      playableProblems: ["no-location", "no-ftr-meta"],
      fileType: ".mp4",
    },
    {
      id: "1aed7f18-a0e3-11e9-9843-d27cf242b68b",
      name: "thumbnail",
      type: "image",
      frameRate: 1,
      frameIn: 0,
      frameOut: 0,
      encoding: false,
      playable: false,
      value:
        "http://localhost/component/get?id=1aed7f18-a0e3-11e9-9843-d27cf242b68b&signature=VQf9R57PPbmSwRyAVEAWojYXirZDedqyYeoCFcl_C6I%3D",
      playableProblems: ["no-ftr-meta"],
      fileType: ".jpg",
    },
  ],
  encoding: false,
  playable: true,
  mediaType: "video",
  fileType: ".mp4",
  assetId: "2d1e8ce6-c3e3-46ab-ad9a-065924fb5b5f",
  thumbnailUrl:
    "http://localhost/thumbor/bm8MChFGYaoCrCj_sbcg3T_mw1E=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3D1aed7f18-a0e3-11e9-9843-d27cf242b68b%26signature%3DVQf9R57PPbmSwRyAVEAWojYXirZDedqyYeoCFcl_C6I%253D",
  version: 1,
  name: "anim",
  assetVersion: {
    status: {
      color: "#FF6600",
      __entity_type__: "Status",
      id: "2286947e-a2d4-11e9-825d-d27cf242b68b",
      name: "Revise",
    },
    asset_id: "2d1e8ce6-c3e3-46ab-ad9a-065924fb5b5f",
    task: {
      status: {
        color: "#1cbc90",
        __entity_type__: "Status",
        id: "a529eede-9dcb-11e9-b01a-d27cf242b68b",
        name: "Final",
      },
      type: {
        color: "#2d5959",
        __entity_type__: "Type",
        id: "44dd23b6-4164-11df-9218-0019bb4983d8",
        name: "Compositing",
      },
      name: "Animation with extra long name",
      context_type: "task",
      __entity_type__: "Task",
      object_type_id: "11c137c0-ee7e-4f9c-91c5-8c77cec22b2c",
      project_id: "ae918736-9dce-11e9-9e65-d27cf242b68b",
      id: "70da4fbe-991c-44c7-a073-774a64abba66",
    },
    version: 1,
    thumbnail_id: "1aed7f18-a0e3-11e9-9843-d27cf242b68b",
    _link: [
      {
        type: "Project",
        id: "ae918736-9dce-11e9-9e65-d27cf242b68b",
        name: "Spring",
      },
      {
        type: "TypedContext",
        id: "5205cdd4-9dd2-11e9-8094-d27cf242b68b",
        name: "sq001",
      },
      {
        type: "TypedContext",
        id: "a49ad3d0-06fc-4211-af65-fa9880e08057",
        name: "01_035_a with extra long name",
      },
      {
        type: "AssetVersion",
        id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
        name: "anim v1",
      },
    ],
    __entity_type__: "AssetVersion",
    link: [
      {
        type: "Project",
        id: "ae918736-9dce-11e9-9e65-d27cf242b68b",
        name: "Spring",
      },
      {
        type: "TypedContext",
        id: "5205cdd4-9dd2-11e9-8094-d27cf242b68b",
        name: "sq001",
      },
      {
        type: "TypedContext",
        id: "a49ad3d0-06fc-4211-af65-fa9880e08057",
        name: "01_035_a with extra long name",
      },
      {
        type: "AssetVersion",
        id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
        name: "anim v1",
      },
    ],
    asset: {
      name: "anim",
      parent: {
        context_type: "task",
        object_type: {
          __entity_type__: "ObjectType",
          id: "bad911de-3bd6-47b9-8b46-3476e237cb36",
          name: "Shot",
        },
        __entity_type__: "Shot",
        object_type_id: "bad911de-3bd6-47b9-8b46-3476e237cb36",
        project_id: "ae918736-9dce-11e9-9e65-d27cf242b68b",
        id: "a49ad3d0-06fc-4211-af65-fa9880e08057",
      },
      context_id: "a49ad3d0-06fc-4211-af65-fa9880e08057",
      __entity_type__: "Asset",
      type: {
        __entity_type__: "AssetType",
        id: "8f4144e0-a8e6-11e2-9e96-0800200c9a66",
        short: "upload",
      },
      id: "2d1e8ce6-c3e3-46ab-ad9a-065924fb5b5f",
    },
    id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
    components: [
      {
        name: "thumbnail",
        __entity_type__: "FileComponent",
        id: "1aed7f18-a0e3-11e9-9843-d27cf242b68b",
        component_locations: [
          {
            component_id: "1aed7f18-a0e3-11e9-9843-d27cf242b68b",
            url: {
              value:
                "http://localhost/component/get?id=1aed7f18-a0e3-11e9-9843-d27cf242b68b&signature=VQf9R57PPbmSwRyAVEAWojYXirZDedqyYeoCFcl_C6I%3D",
            },
            __entity_type__: "ComponentLocation",
            location_id: "3a372bde-05bc-11e4-8908-20c9d081909b",
            id: "2463cac0-a0e3-11e9-b295-d27cf242b68b",
          },
        ],
        file_type: ".jpg",
        version_id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
        system_type: "file",
        metadata: [],
      },
      {
        name: "01_035_a_anim_v01",
        __entity_type__: "FileComponent",
        id: "450b6e25-2986-4887-a451-4e693b86b185",
        component_locations: [
          {
            component_id: "450b6e25-2986-4887-a451-4e693b86b185",
            url: null,
            __entity_type__: "ComponentLocation",
            location_id: "e3c43bdd-a126-4ff1-9aee-a7fe45286887",
            id: "1d31986f-6777-4b1a-ba61-8b9ab3da2157",
          },
        ],
        file_type: ".mp4",
        version_id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
        system_type: "file",
        metadata: [],
      },
      {
        name: "ftrackreview-mp4",
        __entity_type__: "FileComponent",
        id: "5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c",
        component_locations: [
          {
            component_id: "5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c",
            url: {
              value:
                "http://localhost/component/get?id=5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c&signature=0KzIs1YODoLmh3cOJcSzf7DVTwRjrSXV4i_xJ4ip2Pc%3D",
            },
            __entity_type__: "ComponentLocation",
            location_id: "3a372bde-05bc-11e4-8908-20c9d081909b",
            id: "24244d00-a0e3-11e9-b295-d27cf242b68b",
          },
        ],
        file_type: ".mp4",
        version_id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
        system_type: "file",
        metadata: [
          {
            parent_id: "5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c",
            __entity_type__: "Metadata",
            value: '{"frameRate": 24.0, "frameIn": 0, "frameOut": 115.992}',
            key: "ftr_meta",
          },
          {
            parent_id: "5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c",
            __entity_type__: "Metadata",
            value: "d9149b59-0ffc-42ce-9a2d-6b0ca512812d",
            key: "source_component_id",
          },
        ],
      },
    ],
    thumbnail_url: {
      url: "http://localhost/thumbor/bm8MChFGYaoCrCj_sbcg3T_mw1E=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3D1aed7f18-a0e3-11e9-9843-d27cf242b68b%26signature%3DVQf9R57PPbmSwRyAVEAWojYXirZDedqyYeoCFcl_C6I%253D",
      value:
        "http://localhost/thumbor/bm8MChFGYaoCrCj_sbcg3T_mw1E=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3D1aed7f18-a0e3-11e9-9843-d27cf242b68b%26signature%3DVQf9R57PPbmSwRyAVEAWojYXirZDedqyYeoCFcl_C6I%253D",
    },
    user: {
      first_name: "Erik",
      last_name: "L",
      email: "erik@example.com",
      __entity_type__: "User",
      thumbnail_url: {
        url: "http://localhost/thumbor/ctwwA3w7sXa7h6WPxNZdDxwFwqU=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3Dec43b086-aeef-11e9-98ca-d27cf242b68b%26signature%3D4OXHOBcCpBfi7U93xnhC1-ZhtN8dT-RHLYVoFHHAsXA%253D",
        value:
          "http://localhost/thumbor/ctwwA3w7sXa7h6WPxNZdDxwFwqU=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3Dec43b086-aeef-11e9-98ca-d27cf242b68b%26signature%3D4OXHOBcCpBfi7U93xnhC1-ZhtN8dT-RHLYVoFHHAsXA%253D",
      },
      id: "bcdf57b0-acc6-11e1-a554-f23c91df1211",
      resource_type: "user",
    },
  },
  link: [
    {
      type: "Project",
      id: "ae918736-9dce-11e9-9e65-d27cf242b68b",
      name: "Spring",
    },
    {
      type: "TypedContext",
      id: "5205cdd4-9dd2-11e9-8094-d27cf242b68b",
      name: "sq001",
    },
    {
      type: "TypedContext",
      id: "a49ad3d0-06fc-4211-af65-fa9880e08057",
      name: "01_035_a with extra long name",
    },
  ],
  numberOfVersions: 1,
};

const exampleMediaLongSequence = {
  id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
  entity: {
    id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
    type: "AssetVersion",
  },
  components: [
    {
      id: "5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c",
      name: "ftrackreview-mp4",
      type: "video",
      frameRate: 24,
      frameIn: 0,
      frameOut: 115.992,
      encoding: false,
      playable: true,
      value:
        "http://localhost/component/get?id=5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c&signature=0KzIs1YODoLmh3cOJcSzf7DVTwRjrSXV4i_xJ4ip2Pc%3D",
      playableProblems: [],
      fileType: ".mp4",
    },
    {
      id: "450b6e25-2986-4887-a451-4e693b86b185",
      name: "01_035_a_anim_v01",
      type: "video",
      frameRate: 1,
      frameIn: 0,
      frameOut: 0,
      encoding: false,
      playable: false,
      value: null,
      playableProblems: ["no-location", "no-ftr-meta"],
      fileType: ".mp4",
    },
    {
      id: "1aed7f18-a0e3-11e9-9843-d27cf242b68b",
      name: "thumbnail",
      type: "image",
      frameRate: 1,
      frameIn: 0,
      frameOut: 0,
      encoding: false,
      playable: false,
      value:
        "http://localhost/component/get?id=1aed7f18-a0e3-11e9-9843-d27cf242b68b&signature=VQf9R57PPbmSwRyAVEAWojYXirZDedqyYeoCFcl_C6I%3D",
      playableProblems: ["no-ftr-meta"],
      fileType: ".jpg",
    },
  ],
  encoding: false,
  playable: true,
  mediaType: "video",
  fileType: ".mp4",
  assetId: "2d1e8ce6-c3e3-46ab-ad9a-065924fb5b5f",
  thumbnailUrl:
    "http://localhost/thumbor/bm8MChFGYaoCrCj_sbcg3T_mw1E=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3D1aed7f18-a0e3-11e9-9843-d27cf242b68b%26signature%3DVQf9R57PPbmSwRyAVEAWojYXirZDedqyYeoCFcl_C6I%253D",
  version: 1,
  name: "anim this is a long asset name even longer",
  assetVersion: {
    status: {
      color: "#FF6600",
      __entity_type__: "Status",
      id: "2286947e-a2d4-11e9-825d-d27cf242b68b",
      name: "Revise",
    },
    asset_id: "2d1e8ce6-c3e3-46ab-ad9a-065924fb5b5f",
    task: {
      status: {
        color: "#1cbc90",
        __entity_type__: "Status",
        id: "a529eede-9dcb-11e9-b01a-d27cf242b68b",
        name: "Final",
      },
      type: {
        color: "#2d5959",
        __entity_type__: "Type",
        id: "44dd23b6-4164-11df-9218-0019bb4983d8",
        name: "Compositing",
      },
      name: "Animation",
      context_type: "task",
      __entity_type__: "Task",
      object_type_id: "11c137c0-ee7e-4f9c-91c5-8c77cec22b2c",
      project_id: "ae918736-9dce-11e9-9e65-d27cf242b68b",
      id: "70da4fbe-991c-44c7-a073-774a64abba66",
    },
    version: 1,
    thumbnail_id: "1aed7f18-a0e3-11e9-9843-d27cf242b68b",
    _link: [
      {
        type: "Project",
        id: "ae918736-9dce-11e9-9e65-d27cf242b68b",
        name: "Spring",
      },
      {
        type: "TypedContext",
        id: "5205cdd4-9dd2-11e9-8094-d27cf242b68b",
        name: "sq001",
      },
      {
        type: "TypedContext",
        id: "a49ad3d0-06fc-4211-af65-fa9880e08057",
        name: "01_035_a",
      },
      {
        type: "AssetVersion",
        id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
        name: "anim v1",
      },
    ],
    __entity_type__: "AssetVersion",
    link: [
      {
        type: "Project",
        id: "ae918736-9dce-11e9-9e65-d27cf242b68b",
        name: "Spring",
      },
      {
        type: "TypedContext",
        id: "5205cdd4-9dd2-11e9-8094-d27cf242b68b",
        name: "sq001",
      },
      {
        type: "TypedContext",
        id: "a49ad3d0-06fc-4211-af65-fa9880e08057",
        name: "01_035_a",
      },
      {
        type: "AssetVersion",
        id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
        name: "anim v1",
      },
    ],
    asset: {
      name: "anim",
      parent: {
        context_type: "task",
        object_type: {
          __entity_type__: "ObjectType",
          id: "bad911de-3bd6-47b9-8b46-3476e237cb36",
          name: "Shot",
        },
        __entity_type__: "Shot",
        object_type_id: "bad911de-3bd6-47b9-8b46-3476e237cb36",
        project_id: "ae918736-9dce-11e9-9e65-d27cf242b68b",
        id: "a49ad3d0-06fc-4211-af65-fa9880e08057",
      },
      context_id: "a49ad3d0-06fc-4211-af65-fa9880e08057",
      __entity_type__: "Asset",
      type: {
        __entity_type__: "AssetType",
        id: "8f4144e0-a8e6-11e2-9e96-0800200c9a66",
        short: "upload",
      },
      id: "2d1e8ce6-c3e3-46ab-ad9a-065924fb5b5f",
    },
    id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
    components: [
      {
        name: "thumbnail",
        __entity_type__: "FileComponent",
        id: "1aed7f18-a0e3-11e9-9843-d27cf242b68b",
        component_locations: [
          {
            component_id: "1aed7f18-a0e3-11e9-9843-d27cf242b68b",
            url: {
              value:
                "http://localhost/component/get?id=1aed7f18-a0e3-11e9-9843-d27cf242b68b&signature=VQf9R57PPbmSwRyAVEAWojYXirZDedqyYeoCFcl_C6I%3D",
            },
            __entity_type__: "ComponentLocation",
            location_id: "3a372bde-05bc-11e4-8908-20c9d081909b",
            id: "2463cac0-a0e3-11e9-b295-d27cf242b68b",
          },
        ],
        file_type: ".jpg",
        version_id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
        system_type: "file",
        metadata: [],
      },
      {
        name: "01_035_a_anim_v01",
        __entity_type__: "FileComponent",
        id: "450b6e25-2986-4887-a451-4e693b86b185",
        component_locations: [
          {
            component_id: "450b6e25-2986-4887-a451-4e693b86b185",
            url: null,
            __entity_type__: "ComponentLocation",
            location_id: "e3c43bdd-a126-4ff1-9aee-a7fe45286887",
            id: "1d31986f-6777-4b1a-ba61-8b9ab3da2157",
          },
        ],
        file_type: ".mp4",
        version_id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
        system_type: "file",
        metadata: [],
      },
      {
        name: "ftrackreview-mp4",
        __entity_type__: "FileComponent",
        id: "5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c",
        component_locations: [
          {
            component_id: "5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c",
            url: {
              value:
                "http://localhost/component/get?id=5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c&signature=0KzIs1YODoLmh3cOJcSzf7DVTwRjrSXV4i_xJ4ip2Pc%3D",
            },
            __entity_type__: "ComponentLocation",
            location_id: "3a372bde-05bc-11e4-8908-20c9d081909b",
            id: "24244d00-a0e3-11e9-b295-d27cf242b68b",
          },
        ],
        file_type: ".mp4",
        version_id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
        system_type: "file",
        metadata: [
          {
            parent_id: "5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c",
            __entity_type__: "Metadata",
            value: '{"frameRate": 24.0, "frameIn": 0, "frameOut": 115.992}',
            key: "ftr_meta",
          },
          {
            parent_id: "5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c",
            __entity_type__: "Metadata",
            value: "d9149b59-0ffc-42ce-9a2d-6b0ca512812d",
            key: "source_component_id",
          },
        ],
      },
    ],
    thumbnail_url: {
      url: "http://localhost/thumbor/bm8MChFGYaoCrCj_sbcg3T_mw1E=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3D1aed7f18-a0e3-11e9-9843-d27cf242b68b%26signature%3DVQf9R57PPbmSwRyAVEAWojYXirZDedqyYeoCFcl_C6I%253D",
      value:
        "http://localhost/thumbor/bm8MChFGYaoCrCj_sbcg3T_mw1E=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3D1aed7f18-a0e3-11e9-9843-d27cf242b68b%26signature%3DVQf9R57PPbmSwRyAVEAWojYXirZDedqyYeoCFcl_C6I%253D",
    },
    user: {
      first_name: "Erik",
      last_name: "L",
      email: "erik@example.com",
      __entity_type__: "User",
      thumbnail_url: {
        url: "http://localhost/thumbor/ctwwA3w7sXa7h6WPxNZdDxwFwqU=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3Dec43b086-aeef-11e9-98ca-d27cf242b68b%26signature%3D4OXHOBcCpBfi7U93xnhC1-ZhtN8dT-RHLYVoFHHAsXA%253D",
        value:
          "http://localhost/thumbor/ctwwA3w7sXa7h6WPxNZdDxwFwqU=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3Dec43b086-aeef-11e9-98ca-d27cf242b68b%26signature%3D4OXHOBcCpBfi7U93xnhC1-ZhtN8dT-RHLYVoFHHAsXA%253D",
      },
      id: "bcdf57b0-acc6-11e1-a554-f23c91df1211",
      resource_type: "user",
    },
  },
  link: [
    {
      type: "Project",
      id: "ae918736-9dce-11e9-9e65-d27cf242b68b",
      name: "Spring",
    },
    {
      type: "TypedContext",
      id: "5205cdd4-9dd2-11e9-8094-d27cf242b68b",
      name: "sq001 - a long sequence",
    },
    {
      type: "TypedContext",
      id: "a49ad3d0-06fc-4211-af65-fa9880e08057",
      name: "01_035_a - a long shot",
    },
  ],
  numberOfVersions: 5,
};

const exampleVersions = {
  id: "74f77c01-d350-4ba4-9d92-bf2c098e1a6a",
  entity: {
    id: "74f77c01-d350-4ba4-9d92-bf2c098e1a6a",
    type: "AssetVersion",
  },
  components: [
    {
      id: "f4a9e05c-d936-4700-8179-45360d71c777",
      name: "ftrackreview-mp4",
      type: "video",
      frameRate: 24,
      frameIn: 0,
      frameOut: 115.992,
      encoding: false,
      playable: true,
      value:
        "http://localhost/component/get?id=f4a9e05c-d936-4700-8179-45360d71c777&signature=n7Y0QJ5Azt8oxb7Edc1MHp1TbsK24FrKae2OJ5oUSbU%3D",
      playableProblems: [],
      fileType: ".mp4",
    },
    {
      id: "ed61bca1-0205-4ae1-811f-84f913fe13bc",
      name: "01_035_a_anim_v02",
      type: "video",
      frameRate: 1,
      frameIn: 0,
      frameOut: 0,
      encoding: false,
      playable: false,
      value: null,
      playableProblems: ["no-location", "no-ftr-meta"],
      fileType: ".mp4",
    },
    {
      id: "b705c90a-a4c1-11e9-af7f-d27cf242b68b",
      name: "thumbnail",
      type: "image",
      frameRate: 1,
      frameIn: 0,
      frameOut: 0,
      encoding: false,
      playable: false,
      value:
        "http://localhost/component/get?id=b705c90a-a4c1-11e9-af7f-d27cf242b68b&signature=ahY7PDxo19IBraPmmDce9rBnbRw7_BWMtxPWRDnQoY4%3D",
      playableProblems: ["no-ftr-meta"],
      fileType: ".jpg",
    },
  ],
  encoding: false,
  playable: true,
  mediaType: "video",
  fileType: ".mp4",
  assetId: "2d1e8ce6-c3e3-46ab-ad9a-065924fb5b5f",
  thumbnailUrl:
    "http://localhost/thumbor/ScQytCjwykfaohEbyXo_umNjwzU=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3Db705c90a-a4c1-11e9-af7f-d27cf242b68b%26signature%3DahY7PDxo19IBraPmmDce9rBnbRw7_BWMtxPWRDnQoY4%253D",
  version: 2,
  name: "anim",
  assetVersion: {
    status: {
      color: "#1cbc90",
      __entity_type__: "Status",
      id: "44de097a-4164-11df-9218-0019bb4983d8",
      name: "Approved",
    },
    asset_id: "2d1e8ce6-c3e3-46ab-ad9a-065924fb5b5f",
    task: {
      status: {
        color: "#1cbc90",
        __entity_type__: "Status",
        id: "a529eede-9dcb-11e9-b01a-d27cf242b68b",
        name: "Final",
      },
      name: "Animation",
      context_type: "task",
      __entity_type__: "Task",
      object_type_id: "11c137c0-ee7e-4f9c-91c5-8c77cec22b2c",
      project_id: "ae918736-9dce-11e9-9e65-d27cf242b68b",
      type: {
        color: "#35592d",
        __entity_type__: "Type",
        id: "44dc3636-4164-11df-9218-0019bb4983d8",
        name: "Animation",
      },
      id: "70da4fbe-991c-44c7-a073-774a64abba66",
    },
    version: 2,
    thumbnail_id: "b705c90a-a4c1-11e9-af7f-d27cf242b68b",
    _link: [
      {
        type: "Project",
        id: "ae918736-9dce-11e9-9e65-d27cf242b68b",
        name: "Spring",
      },
      {
        type: "TypedContext",
        id: "5205cdd4-9dd2-11e9-8094-d27cf242b68b",
        name: "sq001",
      },
      {
        type: "TypedContext",
        id: "a49ad3d0-06fc-4211-af65-fa9880e08057",
        name: "01_035_a",
      },
      {
        type: "AssetVersion",
        id: "74f77c01-d350-4ba4-9d92-bf2c098e1a6a",
        name: "anim v2",
      },
    ],
    __entity_type__: "AssetVersion",
    link: [
      {
        type: "Project",
        id: "ae918736-9dce-11e9-9e65-d27cf242b68b",
        name: "Spring",
      },
      {
        type: "TypedContext",
        id: "5205cdd4-9dd2-11e9-8094-d27cf242b68b",
        name: "sq001",
      },
      {
        type: "TypedContext",
        id: "a49ad3d0-06fc-4211-af65-fa9880e08057",
        name: "01_035_a",
      },
      {
        type: "AssetVersion",
        id: "74f77c01-d350-4ba4-9d92-bf2c098e1a6a",
        name: "anim v2",
      },
    ],
    asset: {
      name: "anim",
      parent: {
        context_type: "task",
        object_type: {
          __entity_type__: "ObjectType",
          id: "bad911de-3bd6-47b9-8b46-3476e237cb36",
          name: "Shot",
        },
        __entity_type__: "Shot",
        object_type_id: "bad911de-3bd6-47b9-8b46-3476e237cb36",
        project_id: "ae918736-9dce-11e9-9e65-d27cf242b68b",
        id: "a49ad3d0-06fc-4211-af65-fa9880e08057",
      },
      context_id: "a49ad3d0-06fc-4211-af65-fa9880e08057",
      __entity_type__: "Asset",
      type: {
        __entity_type__: "AssetType",
        id: "8f4144e0-a8e6-11e2-9e96-0800200c9a66",
        short: "upload",
      },
      id: "2d1e8ce6-c3e3-46ab-ad9a-065924fb5b5f",
    },
    id: "74f77c01-d350-4ba4-9d92-bf2c098e1a6a",
    components: [
      {
        name: "thumbnail",
        __entity_type__: "FileComponent",
        id: "b705c90a-a4c1-11e9-af7f-d27cf242b68b",
        component_locations: [
          {
            component_id: "b705c90a-a4c1-11e9-af7f-d27cf242b68b",
            url: {
              value:
                "http://localhost/component/get?id=b705c90a-a4c1-11e9-af7f-d27cf242b68b&signature=ahY7PDxo19IBraPmmDce9rBnbRw7_BWMtxPWRDnQoY4%3D",
            },
            __entity_type__: "ComponentLocation",
            location_id: "3a372bde-05bc-11e4-8908-20c9d081909b",
            id: "c253d63a-a4c1-11e9-ad68-d27cf242b68b",
          },
        ],
        file_type: ".jpg",
        version_id: "74f77c01-d350-4ba4-9d92-bf2c098e1a6a",
        system_type: "file",
        metadata: [],
      },
      {
        name: "01_035_a_anim_v02",
        __entity_type__: "FileComponent",
        id: "ed61bca1-0205-4ae1-811f-84f913fe13bc",
        component_locations: [
          {
            component_id: "ed61bca1-0205-4ae1-811f-84f913fe13bc",
            url: null,
            __entity_type__: "ComponentLocation",
            location_id: "e3c43bdd-a126-4ff1-9aee-a7fe45286887",
            id: "53d2aad4-9a41-421a-acf4-a91712e465eb",
          },
        ],
        file_type: ".mp4",
        version_id: "74f77c01-d350-4ba4-9d92-bf2c098e1a6a",
        system_type: "file",
        metadata: [],
      },
      {
        name: "ftrackreview-mp4",
        __entity_type__: "FileComponent",
        id: "f4a9e05c-d936-4700-8179-45360d71c777",
        component_locations: [
          {
            component_id: "f4a9e05c-d936-4700-8179-45360d71c777",
            url: {
              value:
                "http://localhost/component/get?id=f4a9e05c-d936-4700-8179-45360d71c777&signature=n7Y0QJ5Azt8oxb7Edc1MHp1TbsK24FrKae2OJ5oUSbU%3D",
            },
            __entity_type__: "ComponentLocation",
            location_id: "3a372bde-05bc-11e4-8908-20c9d081909b",
            id: "c214db24-a4c1-11e9-ad68-d27cf242b68b",
          },
        ],
        file_type: ".mp4",
        version_id: "74f77c01-d350-4ba4-9d92-bf2c098e1a6a",
        system_type: "file",
        metadata: [
          {
            parent_id: "f4a9e05c-d936-4700-8179-45360d71c777",
            __entity_type__: "Metadata",
            value: '{"frameRate": 24.0, "frameIn": 0, "frameOut": 115.992}',
            key: "ftr_meta",
          },
          {
            parent_id: "f4a9e05c-d936-4700-8179-45360d71c777",
            __entity_type__: "Metadata",
            value: "f3ead924-371e-469d-aec5-2c31c17691f9",
            key: "source_component_id",
          },
        ],
      },
    ],
    thumbnail_url: {
      url: "http://localhost/thumbor/ScQytCjwykfaohEbyXo_umNjwzU=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3Db705c90a-a4c1-11e9-af7f-d27cf242b68b%26signature%3DahY7PDxo19IBraPmmDce9rBnbRw7_BWMtxPWRDnQoY4%253D",
      value:
        "http://localhost/thumbor/ScQytCjwykfaohEbyXo_umNjwzU=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3Db705c90a-a4c1-11e9-af7f-d27cf242b68b%26signature%3DahY7PDxo19IBraPmmDce9rBnbRw7_BWMtxPWRDnQoY4%253D",
    },
    user: {
      first_name: "Erik",
      last_name: "L",
      email: "erik@example.com",
      __entity_type__: "User",
      thumbnail_url: {
        url: "http://localhost/thumbor/ctwwA3w7sXa7h6WPxNZdDxwFwqU=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3Dec43b086-aeef-11e9-98ca-d27cf242b68b%26signature%3D4OXHOBcCpBfi7U93xnhC1-ZhtN8dT-RHLYVoFHHAsXA%253D",
        value:
          "http://localhost/thumbor/ctwwA3w7sXa7h6WPxNZdDxwFwqU=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3Dec43b086-aeef-11e9-98ca-d27cf242b68b%26signature%3D4OXHOBcCpBfi7U93xnhC1-ZhtN8dT-RHLYVoFHHAsXA%253D",
      },
      id: "bcdf57b0-acc6-11e1-a554-f23c91df1211",
      resource_type: "user",
    },
  },
  link: [
    {
      type: "Project",
      id: "ae918736-9dce-11e9-9e65-d27cf242b68b",
      name: "Spring",
    },
    {
      type: "TypedContext",
      id: "5205cdd4-9dd2-11e9-8094-d27cf242b68b",
      name: "sq001",
    },
    {
      type: "TypedContext",
      id: "a49ad3d0-06fc-4211-af65-fa9880e08057",
      name: "01_035_a",
    },
    {
      type: "AssetVersion",
      id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
      name: "anim v1",
    },
  ],
  groupId: "2d1e8ce6-c3e3-46ab-ad9a-065924fb5b5f",
  versions: [
    {
      id: "74f77c01-d350-4ba4-9d92-bf2c098e1a6a",
      encoding: false,
      playable: true,
      mediaType: "video",
      fileType: ".mp4",
      assetId: "2d1e8ce6-c3e3-46ab-ad9a-065924fb5b5f",
      thumbnailUrl:
        "http://localhost/thumbor/ScQytCjwykfaohEbyXo_umNjwzU=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3Db705c90a-a4c1-11e9-af7f-d27cf242b68b%26signature%3DahY7PDxo19IBraPmmDce9rBnbRw7_BWMtxPWRDnQoY4%253D",
      version: 2,
      name: "anim",
      groupId: "2d1e8ce6-c3e3-46ab-ad9a-065924fb5b5f",
    },
    {
      id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
      entity: {
        id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
        type: "AssetVersion",
      },
      components: [
        {
          id: "5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c",
          name: "ftrackreview-mp4",
          type: "video",
          frameRate: 24,
          frameIn: 0,
          frameOut: 115.992,
          encoding: false,
          playable: true,
          value:
            "http://localhost/component/get?id=5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c&signature=0KzIs1YODoLmh3cOJcSzf7DVTwRjrSXV4i_xJ4ip2Pc%3D",
          playableProblems: [],
          fileType: ".mp4",
        },
        {
          id: "450b6e25-2986-4887-a451-4e693b86b185",
          name: "01_035_a_anim_v01",
          type: "video",
          frameRate: 1,
          frameIn: 0,
          frameOut: 0,
          encoding: false,
          playable: false,
          value: null,
          playableProblems: ["no-location", "no-ftr-meta"],
          fileType: ".mp4",
        },
        {
          id: "1aed7f18-a0e3-11e9-9843-d27cf242b68b",
          name: "thumbnail",
          type: "image",
          frameRate: 1,
          frameIn: 0,
          frameOut: 0,
          encoding: false,
          playable: false,
          value:
            "http://localhost/component/get?id=1aed7f18-a0e3-11e9-9843-d27cf242b68b&signature=VQf9R57PPbmSwRyAVEAWojYXirZDedqyYeoCFcl_C6I%3D",
          playableProblems: ["no-ftr-meta"],
          fileType: ".jpg",
        },
      ],
      encoding: false,
      playable: true,
      mediaType: "video",
      fileType: ".mp4",
      assetId: "2d1e8ce6-c3e3-46ab-ad9a-065924fb5b5f",
      thumbnailUrl:
        "http://localhost/thumbor/bm8MChFGYaoCrCj_sbcg3T_mw1E=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3D1aed7f18-a0e3-11e9-9843-d27cf242b68b%26signature%3DVQf9R57PPbmSwRyAVEAWojYXirZDedqyYeoCFcl_C6I%253D",
      version: 1,
      name: "anim",
      assetVersion: {
        status: {
          color: "#FF6600",
          __entity_type__: "Status",
          id: "2286947e-a2d4-11e9-825d-d27cf242b68b",
          name: "Revise",
        },
        asset_id: "2d1e8ce6-c3e3-46ab-ad9a-065924fb5b5f",
        version: 1,
        thumbnail_id: "1aed7f18-a0e3-11e9-9843-d27cf242b68b",
        _link: [
          {
            type: "Project",
            id: "ae918736-9dce-11e9-9e65-d27cf242b68b",
            name: "Spring",
          },
          {
            type: "TypedContext",
            id: "5205cdd4-9dd2-11e9-8094-d27cf242b68b",
            name: "sq001",
          },
          {
            type: "TypedContext",
            id: "a49ad3d0-06fc-4211-af65-fa9880e08057",
            name: "01_035_a",
          },
          {
            type: "AssetVersion",
            id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
            name: "anim v1",
          },
        ],
        __entity_type__: "AssetVersion",
        link: [
          {
            type: "Project",
            id: "ae918736-9dce-11e9-9e65-d27cf242b68b",
            name: "Spring",
          },
          {
            type: "TypedContext",
            id: "5205cdd4-9dd2-11e9-8094-d27cf242b68b",
            name: "sq001",
          },
          {
            type: "TypedContext",
            id: "a49ad3d0-06fc-4211-af65-fa9880e08057",
            name: "01_035_a",
          },
          {
            type: "AssetVersion",
            id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
            name: "anim v1",
          },
        ],
        id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
        components: [
          {
            name: "thumbnail",
            __entity_type__: "FileComponent",
            id: "1aed7f18-a0e3-11e9-9843-d27cf242b68b",
            component_locations: [
              {
                component_id: "1aed7f18-a0e3-11e9-9843-d27cf242b68b",
                url: {
                  value:
                    "http://localhost/component/get?id=1aed7f18-a0e3-11e9-9843-d27cf242b68b&signature=VQf9R57PPbmSwRyAVEAWojYXirZDedqyYeoCFcl_C6I%3D",
                },
                __entity_type__: "ComponentLocation",
                location_id: "3a372bde-05bc-11e4-8908-20c9d081909b",
                id: "2463cac0-a0e3-11e9-b295-d27cf242b68b",
              },
            ],
            file_type: ".jpg",
            version_id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
            system_type: "file",
            metadata: [],
          },
          {
            name: "01_035_a_anim_v01",
            __entity_type__: "FileComponent",
            id: "450b6e25-2986-4887-a451-4e693b86b185",
            component_locations: [
              {
                component_id: "450b6e25-2986-4887-a451-4e693b86b185",
                url: null,
                __entity_type__: "ComponentLocation",
                location_id: "e3c43bdd-a126-4ff1-9aee-a7fe45286887",
                id: "1d31986f-6777-4b1a-ba61-8b9ab3da2157",
              },
            ],
            file_type: ".mp4",
            version_id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
            system_type: "file",
            metadata: [],
          },
          {
            name: "ftrackreview-mp4",
            __entity_type__: "FileComponent",
            id: "5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c",
            component_locations: [
              {
                component_id: "5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c",
                url: {
                  value:
                    "http://localhost/component/get?id=5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c&signature=0KzIs1YODoLmh3cOJcSzf7DVTwRjrSXV4i_xJ4ip2Pc%3D",
                },
                __entity_type__: "ComponentLocation",
                location_id: "3a372bde-05bc-11e4-8908-20c9d081909b",
                id: "24244d00-a0e3-11e9-b295-d27cf242b68b",
              },
            ],
            file_type: ".mp4",
            version_id: "5f859e10-2eb8-42ba-9a2b-bb65f914340e",
            system_type: "file",
            metadata: [
              {
                parent_id: "5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c",
                __entity_type__: "Metadata",
                value: '{"frameRate": 24.0, "frameIn": 0, "frameOut": 115.992}',
                key: "ftr_meta",
              },
              {
                parent_id: "5406bf01-8f5b-45fe-b20a-1fb1e5e9aa3c",
                __entity_type__: "Metadata",
                value: "d9149b59-0ffc-42ce-9a2d-6b0ca512812d",
                key: "source_component_id",
              },
            ],
          },
        ],
        thumbnail_url: {
          url: "http://localhost/thumbor/bm8MChFGYaoCrCj_sbcg3T_mw1E=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3D1aed7f18-a0e3-11e9-9843-d27cf242b68b%26signature%3DVQf9R57PPbmSwRyAVEAWojYXirZDedqyYeoCFcl_C6I%253D",
          value:
            "http://localhost/thumbor/bm8MChFGYaoCrCj_sbcg3T_mw1E=/fit-in/1280x1280/http%3A//localhost/component/get%3Fid%3D1aed7f18-a0e3-11e9-9843-d27cf242b68b%26signature%3DVQf9R57PPbmSwRyAVEAWojYXirZDedqyYeoCFcl_C6I%253D",
        },
      },
      link: [null, null, null],
      groupId: "2d1e8ce6-c3e3-46ab-ad9a-065924fb5b5f",
    },
  ],
  numberOfVersions: 2,
};

export const _EntityListItem = () => (
  <EntityListItem onClick={onItemClicked}>
    <EntityListItemContent>
      <EntityListItemRow>
        This is a single row of text in the generic EntityListItemRow. Text is
        truncated if it is too long.
      </EntityListItemRow>
    </EntityListItemContent>
  </EntityListItem>
);

_EntityListItem.storyName = "EntityListItem";

export const EntityListItemActive = () => (
  <EntityListItem active onClick={onItemClicked}>
    <EntityListItemContent>
      <EntityListItemRow>
        This is a single row of text in the generic EntityListItemRow. Text is
        truncated if it is too long.
      </EntityListItemRow>
    </EntityListItemContent>
  </EntityListItem>
);

EntityListItemActive.storyName = "EntityListItem, Active";

export const EntityListItemWithStatus = () => (
  <EntityListItem onClick={onItemClicked}>
    <EntityListItemContent>
      <EntityListItemRow>
        This is a single row of text in the generic EntityListItemRow. Text is
        truncated if it is too long.
      </EntityListItemRow>
    </EntityListItemContent>
    <EntityStatusBar status={{ color: "hotpink" }} />
  </EntityListItem>
);

EntityListItemWithStatus.storyName = "EntityListItem with status";

export const EntityListItem2RowsOfContentWithStatus = () => (
  <EntityListItem onClick={onItemClicked}>
    <EntityListItemContent>
      <EntityListItemRow>
        This is the first row that can be very long. Text is truncated if it is
        too long.
      </EntityListItemRow>
      <EntityListItemRow>
        This is the second row that can be very long. Text is truncated if it is
        too long.
      </EntityListItemRow>
    </EntityListItemContent>
    <EntityStatusBar status={{ color: "hotpink" }} />
  </EntityListItem>
);

EntityListItem2RowsOfContentWithStatus.storyName =
  "EntityListItem, 2 rows of content with status";

export const _StudioEntityListItem = () => (
  <StudioEntityListItem
    media={exampleMedia}
    onClick={onItemClicked}
    onViewInStudio={onViewInStudioClicked}
  />
);

_StudioEntityListItem.storyName = "StudioEntityListItem";

export const StudioEntityListItemWLongSequence = () => (
  <StudioEntityListItem
    media={exampleMediaLongSequence}
    onClick={onItemClicked}
    onViewInStudio={onViewInStudioClicked}
  />
);

StudioEntityListItemWLongSequence.storyName =
  "StudioEntityListItem w/Long sequence";

export const StudioEntityListItemActive = () => (
  <StudioEntityListItem
    active
    media={exampleMedia}
    onClick={onItemClicked}
    onViewInStudio={onViewInStudioClicked}
  />
);

StudioEntityListItemActive.storyName = "StudioEntityListItem, Active";

export const StudioEntityListItemVersion = () => (
  <StudioEntityListItem
    active
    media={exampleVersions}
    onClick={onItemClicked}
    onViewInStudio={onViewInStudioClicked}
  />
);

StudioEntityListItemVersion.storyName = "StudioEntityListItem, Version";

export const _ReviewEntityListItem = () => (
  <ReviewEntityListItem media={exampleMedia} onClick={onItemClicked} />
);

_ReviewEntityListItem.storyName = "ReviewEntityListItem";

export const ReviewEntityListItemActive = () => (
  <ReviewEntityListItem media={exampleMedia} onClick={onItemClicked} active />
);

ReviewEntityListItemActive.storyName = "ReviewEntityListItem, Active";

export const _ListItemChip = () => (
  <ListItemChip name="Example task name" color="slate" icon="assignment" />
);

_ListItemChip.storyName = "ListItemChip";

export const ForDesignApprovalActive = () => (
  <StudioEntityListItem
    selectable
    active
    media={exampleMedia}
    onClick={onItemClicked}
    onViewInStudio={onViewInStudioClicked}
  />
);

ForDesignApprovalActive.storyName = "For design approval, Active";
