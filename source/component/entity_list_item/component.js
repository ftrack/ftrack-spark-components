// :copyright: Copyright (c) 2016 ftrack
import { useState, useRef, Fragment } from "react";

import * as React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import useOnClickOutside from "use-onclickoutside";
import {
  IconButton,
  Popper,
  ClickAwayListener,
  MenuItem,
  ListItemIcon,
} from "@mui/material";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import AddIcon from "@mui/icons-material/Add";
import DeleteIcon from "@mui/icons-material/Delete";
import LayersIcon from "@mui/icons-material/Layers";

import { FormattedMessage, defineMessages } from "react-intl";
import safeInjectIntl from "../util/hoc/safe_inject_intl";

import Checkbox from "react-toolbox/lib/checkbox";
import FontIcon from "react-toolbox/lib/font_icon";
import withTooltip from "react-toolbox/lib/tooltip";

import { Thumbnail } from "../image";
import FileTypeIcon from "../file_type_icon";
import Breadcrumbs from "../breadcrumbs";
import ButtonMenu from "../button_menu";

import style from "./style.scss";
import checkboxTheme from "./checkbox_theme.scss";
import tooltipTheme from "./tooltip_theme.scss";

const DivWithTooltip = withTooltip("div");

const messages = defineMessages({
  "menuitem-view-in-studio": {
    id: "ftrack-spark-components.entity-list-item.menuitem-view-in-studio",
    description: '"View in Studio" menu item"',
    defaultMessage: "View in Studio",
  },
  "menuitem-view-in-library": {
    id: "ftrack-spark-components.entity-list-item.menuitem-view-in-library",
    description: '"View in library" menu item"',
    defaultMessage: "View in library",
  },
  "menuitem-remove": {
    id: "ftrack-spark-components.entity-list-item.menuitem-remove",
    description: '"Remove" menu item"',
    defaultMessage: "Remove",
  },
  "menuitem-addrelatedversions": {
    id: "ftrack-spark-components.entity-list-item.menuitem-addrelatedversions",
    description: '"Add related versions" menu item"',
    defaultMessage: "Add related versions",
  },
});

const ListItemChip = ({
  name,
  color,
  icon,
  tooltip,
  sentenceCase,
  truncation = true,
}) => (
  <div
    className={classNames(style.taskChip, {
      [style.taskChipTruncate]: truncation,
    })}
  >
    {!icon && color ? (
      <div className={style.taskChipColor} style={{ backgroundColor: color }} />
    ) : (
      <FontIcon className={style.taskChipIcon} value={icon} style={{ color }} />
    )}
    <span
      title={tooltip || (typeof name === "string" ? name : null)}
      className={classNames(style.taskChipText, {
        [style.taskChipTruncate]: truncation,
        [style.sentenceCase]: sentenceCase,
      })}
    >
      {name}
    </span>
  </div>
);

ListItemChip.propTypes = {
  name: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  icon: PropTypes.string,
  tooltip: PropTypes.string,
  truncation: PropTypes.bool,
  sentenceCase: PropTypes.bool,
};

const ListItemChips = ({ items = [] }) => {
  let chips = [];
  if (items.length > 3) {
    chips = items.splice(0, 3);
    chips.push({
      id: items.map((r) => r.id).join("-"),
      name: `+ ${items.length}`,
      tooltip: items.map((r) => r.name).join(", "),
      truncation: false,
    });
  } else {
    chips = items;
  }

  return (
    <Fragment>
      {chips.map(({ id, name, color, icon, tooltip, truncation }) => (
        <ListItemChip
          key={id}
          name={name}
          color={color}
          icon={icon}
          tooltip={tooltip}
          truncation={truncation}
        />
      ))}
    </Fragment>
  );
};

ListItemChips.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      color: PropTypes.string,
      icon: PropTypes.string,
      tooltip: PropTypes.string,
    })
  ),
};

const VersionsList = ({
  versionMenuOpen,
  setVersionMenuOpen,
  versionMenuRef,
  versions,
  selectVersion,
  extraChildren,
  assetId,
  disableClickOutside,
}) => {
  const versionsListRef = useRef(null);

  useOnClickOutside(versionsListRef, (e) => {
    if (disableClickOutside) {
      return;
    }
    const closestIgnore = e.target.closest("[data-ignoreonclick]");
    const ignoreonclick =
      closestIgnore &&
      closestIgnore.getAttribute("data-ignoreonclick") === assetId;
    if (!ignoreonclick) {
      setVersionMenuOpen(false);
    }
  });

  return (
    <Popper
      open={versionMenuOpen}
      anchorEl={versionMenuRef.current}
      placement="bottom-start"
    >
      <div
        ref={versionsListRef}
        className={style.entityVersionSelectorPopperWrapper}
        style={{
          overflowY: versions.length > 10 ? "scroll" : "inherit",
        }}
      >
        {versions.map((v) => (
          <EntityListItem
            key={v.id}
            onClick={(e) => {
              e.stopPropagation();
              setVersionMenuOpen(!versionMenuOpen);
              selectVersion(v);
            }}
          >
            <EntityListItemContent>
              <EntityListItemRow>
                <div className={style.versionTitleContainer}>
                  <span title={v.name} className={style.entityVersionName}>
                    {v.name}
                  </span>
                  <span className={style.entityVersionVersion}>
                    v{v.version}
                  </span>
                </div>
                {v.assetVersion &&
                  v.assetVersion.task &&
                  v.assetVersion.task.type && (
                    <div className={style.taskChipContainer}>
                      <ListItemChip
                        name={v.assetVersion.task.name}
                        color={v.assetVersion.task.type.color}
                        icon="assignment"
                      />
                    </div>
                  )}
              </EntityListItemRow>
            </EntityListItemContent>
            {v.assetVersion && v.assetVersion.status && (
              <EntityStatusBar status={v.assetVersion.status} />
            )}
          </EntityListItem>
        ))}
        {extraChildren}
      </div>
    </Popper>
  );
};

VersionsList.propTypes = {
  versionMenuOpen: PropTypes.bool,
  versionMenuRef: PropTypes.any,
  versions: PropTypes.array,
  setVersionMenuOpen: PropTypes.func,
  selectVersion: PropTypes.func,
  extraChildren: PropTypes.node,
  assetId: PropTypes.string,
  disableClickOutside: PropTypes.bool,
};

const EntityVersionSelector = ({
  media,
  selectVersion,
  extraChildren,
  disabled,
  largeButton,
  hideCount,
}) => {
  const { name, numberOfVersions, version, versions } = media;

  const versionMenuRef = useRef(null);
  const [versionMenuOpen, setVersionMenuOpen] = useState(false);

  const versionSelectorButtonClasses = classNames(
    style.entityVersionSelectorDecoration,
    {
      [style.entityVersionSelectorOpen]: versionMenuOpen,
    }
  );
  const versionSelectorClasses = classNames(style.entityVersionSelector, {
    [style.entityVersionSelectorHover]: largeButton && !disabled,
  });
  return (
    <ClickAwayListener
      onClickAway={() => {
        if (largeButton && !disabled) {
          setVersionMenuOpen(false);
        }
      }}
    >
      <div
        className={versionSelectorClasses}
        onClick={(e) => {
          e.stopPropagation();
          if (largeButton && !disabled) {
            setVersionMenuOpen(!versionMenuOpen);
          }
        }}
      >
        <div ref={versionMenuRef} className={style.entityVersionSelectorInner}>
          <span title={name} className={style.entityVersionName}>
            {name}
          </span>
          <span className={style.entityVersionVersion}>v{version}</span>{" "}
          <Fragment>
            {!hideCount && <span>({numberOfVersions})</span>}
            {!disabled && (
              <div
                data-ignoreonclick={media.id}
                className={versionSelectorButtonClasses}
                onClick={(e) => {
                  e.stopPropagation();
                  setVersionMenuOpen(!versionMenuOpen);
                }}
              >
                <FontIcon
                  className={style.entityVersionSelectorDecorationIcon}
                  value={!versionMenuOpen ? "expand_more" : "expand_less"}
                />
              </div>
            )}
          </Fragment>
        </div>
        {versions && (
          <VersionsList
            disableClickOutside={largeButton}
            versionMenuOpen={versionMenuOpen}
            setVersionMenuOpen={setVersionMenuOpen}
            versionMenuRef={versionMenuRef}
            versions={versions}
            selectVersion={selectVersion}
            assetId={media.id}
            extraChildren={extraChildren}
          />
        )}
      </div>
    </ClickAwayListener>
  );
};

EntityVersionSelector.propTypes = {
  media: PropTypes.object,
  selectVersion: PropTypes.func,
  extraChildren: PropTypes.node,
  disabled: PropTypes.bool,
  largeButton: PropTypes.bool,
  hideCount: PropTypes.bool,
};

const EntityListThumbnailWrapper = ({ children, disabled, disabledTooltip }) =>
  disabled && disabledTooltip ? (
    <DivWithTooltip
      theme={tooltipTheme}
      className={style.entityListItemThumbnailWrapper}
      tooltipPosition="top"
      tooltip={disabledTooltip}
    >
      {children}
    </DivWithTooltip>
  ) : (
    <div>{children}</div>
  );

EntityListThumbnailWrapper.propTypes = {
  children: PropTypes.node,
  disabled: PropTypes.bool,
  disabledTooltip: PropTypes.node,
};

const EntityThumbnail = ({
  media,
  selectable,
  selected,
  disabled,
  disabledTooltip,
  onClick,
}) => {
  const { thumbnailUrl, fileType, encoding } = media;
  const thumbnailId = media.assetVersion && media.assetVersion.thumbnail_id;

  const selectionStateClasses = classNames(style.selectionOverlay, {
    [style.selectionOverlayShown]: selected || disabled,
  });

  const handleClick = (_, event) => {
    event.preventDefault();
    event.stopPropagation();
    if (onClick) {
      onClick();
    }
  };

  return thumbnailId ? (
    <EntityListThumbnailWrapper
      disabled={disabled}
      disabledTooltip={disabledTooltip}
    >
      {selectable && (
        <div className={style.selectionOverlayWrapper}>
          <div
            className={selectionStateClasses}
            onClick={(event) => handleClick(null, event)}
          >
            <Checkbox
              theme={checkboxTheme}
              checked={selected || disabled}
              disabled={disabled}
              onChange={handleClick}
            />
          </div>
        </div>
      )}
      <div className={style.thumbnailOverlay} />
      <Thumbnail
        size="extra-tiny"
        className={style.thumbnail}
        loading={encoding}
        src={thumbnailId && thumbnailUrl}
      />
    </EntityListThumbnailWrapper>
  ) : (
    <EntityListThumbnailWrapper
      disabled={disabled}
      disabledTooltip={disabledTooltip}
    >
      {selectable && (
        <div className={style.selectionOverlayWrapper}>
          <div className={selectionStateClasses} onClick={handleClick}>
            <Checkbox
              theme={checkboxTheme}
              checked={selected || disabled}
              disabled={disabled}
              onClick={handleClick}
            />
          </div>
        </div>
      )}
      <div className={style.thumbnailOverlay} />
      <div className={style.thumbnail}>
        <FileTypeIcon variant="tiny" label={fileType} loading={encoding} />
      </div>
    </EntityListThumbnailWrapper>
  );
};

EntityThumbnail.propTypes = {
  media: PropTypes.object.isRequired,
  selectable: PropTypes.bool,
  selected: PropTypes.bool,
  disabled: PropTypes.bool,
  disabledTooltip: PropTypes.node,
  onClick: PropTypes.func,
};

const EntityToggle = ({ selected, disabled, onClick }) => {
  const handleClick = (_, event) => {
    event.preventDefault();
    event.stopPropagation();
    if (onClick) {
      onClick();
    }
  };
  return (
    <div className={style.entityToggle}>
      <Checkbox
        theme={checkboxTheme}
        checked={selected}
        disabled={disabled}
        onChange={handleClick}
      />
    </div>
  );
};

EntityToggle.propTypes = {
  selected: PropTypes.bool,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
};

const EntityStatusBar = ({ status }) => {
  const StatusBarWithToolTip = withTooltip("div");

  const colorStyle = {
    backgroundColor: status ? status.color : "transparent",
  };

  return (
    <StatusBarWithToolTip
      className={style.entityStatusBar}
      style={colorStyle}
      tooltip={status.name}
      tooltipPosition="left"
    />
  );
};

EntityStatusBar.propTypes = {
  status: PropTypes.shape({
    color: PropTypes.string.isRequired,
  }),
};

const EntityListItemContent = ({ children }) => (
  <div className={style.entityListItemContent}>{children}</div>
);
EntityListItemContent.propTypes = {
  children: PropTypes.node,
};

const EntityListItemRow = ({ children, className }) => (
  <div className={classNames(style.entityListItemRow, className)}>
    {children}
  </div>
);
EntityListItemRow.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};

const EntityListItemContextMenu = ({ children }) => (
  <div className={style.entityListItemMenuButtonWrapper}>
    <ButtonMenu
      button={
        <IconButton size="small">
          <MoreVertIcon />
        </IconButton>
      }
    >
      {children}
    </ButtonMenu>
  </div>
);

EntityListItemContextMenu.propTypes = {
  children: PropTypes.node,
};

const EntityListItem = ({
  active,
  disabled,
  disableActiveIndicator,
  dragging,
  innerRef,
  children,
  onClick,
  disableShadows,
  ...props
}) => {
  const wrapperClasses = classNames(
    style.entityListItem,
    { [style.active]: active },
    { [style.disabled]: disabled },
    { [style.dragging]: dragging },
    { [style.entityListItemShadow]: !disableShadows }
  );

  const handleClick = (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (onClick) {
      onClick(event);
    }
  };

  return (
    <li
      ref={innerRef}
      className={wrapperClasses}
      onClick={handleClick}
      {...props}
    >
      {!disableActiveIndicator && <div className={style.activeIndicator} />}
      {children}
    </li>
  );
};

EntityListItem.propTypes = {
  active: PropTypes.bool,
  disabled: PropTypes.bool,
  disableActiveIndicator: PropTypes.bool,
  dragging: PropTypes.bool,
  children: PropTypes.node,
  onClick: PropTypes.func,
  disableShadows: PropTypes.bool,
};

const StudioEntityListItem = ({
  active,
  selected,
  selectable,
  media,
  canViewInStudio,
  onViewInStudio,
  canViewInLibrary,
  onViewInLibrary,
  onRemove,
  selectVersion,
  versionSelectorChildren,
  hideVersionSelector,
  canAddVersions,
  onAddVersion,
  ...props
}) => (
  <EntityListItem active={active} selected={selected} {...props}>
    <EntityThumbnail
      selectable={selectable}
      selected={selected}
      media={media}
    />
    <EntityListItemContent>
      <EntityListItemRow>
        <EntityVersionSelector
          media={media}
          selectVersion={selectVersion}
          extraChildren={versionSelectorChildren}
          disabled={hideVersionSelector}
        />
      </EntityListItemRow>
      <EntityListItemRow>
        {media.link && (
          <Breadcrumbs
            truncation="tooltip"
            items={media.link.slice(1).map(({ id, name: title }) => ({
              id,
              title,
              disabled: true,
            }))}
          />
        )}
        {media.assetVersion &&
          media.assetVersion.task &&
          media.assetVersion.task.type && (
            <ListItemChip
              name={media.assetVersion.task.name}
              color={media.assetVersion.task.type.color}
              icon="assignment"
            />
          )}
      </EntityListItemRow>
    </EntityListItemContent>
    {(canViewInStudio || canViewInLibrary || canAddVersions) && (
      <EntityListItemContextMenu>
        {canViewInStudio && (
          <MenuItem onClick={onViewInStudio}>
            <ListItemIcon>
              <LayersIcon />
            </ListItemIcon>
            <FormattedMessage {...messages["menuitem-view-in-studio"]} />
          </MenuItem>
        )}
        {canViewInLibrary && (
          <MenuItem onClick={onViewInLibrary}>
            <ListItemIcon>
              <LayersIcon />
            </ListItemIcon>
            <FormattedMessage {...messages["menuitem-view-in-library"]} />
          </MenuItem>
        )}
        {canAddVersions && (
          <MenuItem onClick={onRemove}>
            <ListItemIcon>
              <DeleteIcon />
            </ListItemIcon>
            <FormattedMessage {...messages["menuitem-remove"]} />
          </MenuItem>
        )}
        {canAddVersions && (
          <MenuItem onClick={onAddVersion}>
            <ListItemIcon>
              <AddIcon />
            </ListItemIcon>
            <FormattedMessage {...messages["menuitem-addrelatedversions"]} />
          </MenuItem>
        )}
      </EntityListItemContextMenu>
    )}
    {media.assetVersion && media.assetVersion.status && (
      <EntityStatusBar status={media.assetVersion.status} />
    )}
  </EntityListItem>
);

StudioEntityListItem.propTypes = {
  active: PropTypes.bool,
  selected: PropTypes.bool,
  selectable: PropTypes.bool,
  media: PropTypes.object.isRequired,
  canViewInStudio: PropTypes.bool,
  canViewInLibrary: PropTypes.bool,
  onViewInStudio: PropTypes.func,
  onViewInLibrary: PropTypes.func,
  onRemove: PropTypes.func,
  onAddVersion: PropTypes.func,
  selectVersion: PropTypes.func,
  hideVersionSelector: PropTypes.bool,
  canAddVersions: PropTypes.bool,
  versionSelectorChildren: PropTypes.node,
};

StudioEntityListItem.defaultProps = {
  active: false,
  selected: false,
  selectable: false,
  selectVersion: () => {},
};

const ReviewEntityListItem = ({
  active,
  selected,
  media,
  onViewInLibrary,
  onDelete,
  ...props
}) => {
  const { name, version } = media;
  return (
    <EntityListItem active={active} selected={selected} {...props}>
      <EntityThumbnail selected={selected} media={media} />
      <EntityListItemContent>
        <EntityListItemRow>
          <span className={style.reviewItemName}>{name}</span>
        </EntityListItemRow>
        <EntityListItemRow>
          <span className={style.reviewItemVersion}>Version {version}</span>
        </EntityListItemRow>
      </EntityListItemContent>
      <EntityListItemContextMenu>
        <MenuItem onClick={onViewInLibrary}>
          <ListItemIcon>
            <LayersIcon />
          </ListItemIcon>
          <FormattedMessage {...messages["menuitem-view-in-library"]} />
        </MenuItem>
        <MenuItem onClick={onDelete}>
          <ListItemIcon>
            <DeleteIcon />
          </ListItemIcon>
          <FormattedMessage {...messages["menuitem-remove"]} />
        </MenuItem>
      </EntityListItemContextMenu>
    </EntityListItem>
  );
};

ReviewEntityListItem.propTypes = {
  active: PropTypes.bool,
  selected: PropTypes.bool,
  media: PropTypes.object.isRequired,
  onViewInLibrary: PropTypes.func,
  onDelete: PropTypes.func,
};

ReviewEntityListItem.defaultProps = {
  active: false,
  selected: false,
};

safeInjectIntl(ReviewEntityListItem);
safeInjectIntl(StudioEntityListItem);
safeInjectIntl(EntityListItem);

export {
  StudioEntityListItem,
  ReviewEntityListItem,
  EntityListItemContent,
  EntityListItemRow,
  EntityStatusBar,
  ListItemChip,
  ListItemChips,
  EntityVersionSelector,
  VersionsList,
  EntityThumbnail,
  EntityToggle,
};

export default EntityListItem;
