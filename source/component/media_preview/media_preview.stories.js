import PropTypes from "prop-types";
import { Component } from "react";
import centered from "@storybook/addon-centered/react";
import { Button } from "react-toolbox/lib/button";

import getSession from "../../story/get_session";
import { Image, Video, Media } from "../media";
import MediaPreview from ".";

const session = getSession();

export default {
  title: "Media Preview",
  decorators: [centered],
};

class MediaExample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      components: [],
    };
  }

  componentWillMount() {
    const { reviewSessionObjectId } = this.props;
    session
      .query(
        `select asset_version.components.id, asset_version.components.name, asset_version.components.file_type\
                    from ReviewSessionObject where id is ${reviewSessionObjectId}\
                    and asset_version.components.component_locations.location.name is "ftrack.server"`
      )
      .then((response) => {
        const components = response.data[0].asset_version.components;

        this.setState({ component: components[0] });
      });
  }

  render() {
    const { component } = this.state;
    return <Media component={component} session={session} />;
  }
}

MediaExample.propTypes = {
  reviewSessionObjectId: PropTypes.string,
};

const initialItems = [
  <Video url={"http://techslides.com/demos/sample-videos/small.mp4"} />,
  <MediaExample
    reviewSessionObjectId={"cf92c87a-7860-11e4-8e7d-040132734d01"}
  />,
  <Image
    url={
      "https://www.ftrack.com/wp-content/uploads/2016/08/ftrack-logo-light-x1.png"
    }
  />,
];

const meta = [
  <span>Here you can put whatever JSX content you like.</span>,
  <h1>Hello</h1>,
  <h2>Hello again</h2>,
];

const downloadLinks = [
  "http://techslides.com/demos/sample-videos/small.mp4",
  "",
  "https://www.ftrack.com/wp-content/uploads/2016/08/ftrack-logo-light-x1.png",
];

class MediaPreviewExample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: initialItems,
    };

    this.onClose = this.onClose.bind(this);
    this.onShow = this.onShow.bind(this);
    this.showSecondObject = this.showSecondObject.bind(this);
  }

  onClose() {
    this.setState({ items: [], visibleIndex: null });
  }

  onShow() {
    this.setState({
      items: initialItems,
    });
  }

  showSecondObject() {
    this.setState({
      items: initialItems,
      visibleIndex: 1,
    });
  }

  render() {
    const { items, visibleIndex } = this.state;

    return (
      <div>
        <MediaPreview
          onClose={this.onClose}
          items={items}
          visibleIndex={visibleIndex}
          meta={meta}
          downloadLinks={downloadLinks}
        />
        <div>
          <Button label={"Open Preview"} onClick={this.onShow} raised primary />
        </div>
        <div style={{ marginTop: "20px" }}>
          <Button
            label={"Open Preview with the second media object"}
            onClick={this.showSecondObject}
            raised
            primary
          />
        </div>
      </div>
    );
  }
}

export const _MediaPreview = () => <MediaPreviewExample />;
