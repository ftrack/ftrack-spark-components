// :copyright: Copyright (c) 2018 ftrack

import PropTypes from "prop-types";
import { Button } from "react-toolbox/lib/button";
import { Component } from "react";

import classNames from "classnames";
import { defineMessages, intlShape } from "react-intl";

import safeInjectIntl from "../util/hoc/safe_inject_intl";
import withMonitorKeyPress from "../util/hoc/with_monitor_key_events";
import IconButton from "../icon_button";
import style from "./style.scss";

const messages = defineMessages({
  "message-x-of-y": {
    id: "ftrack-spark-components.media_preview.message-x-of-y",
    defaultMessage: "{x} of {y}",
  },
  "download-button": {
    id: "ftrack-spark-components.media_preview.download",
    defaultMessage: "Download",
  },
});

const ANIMATION_TIME = 200;

const stopPropagation = (event) => {
  event.stopPropagation();
  event.nativeEvent.stopImmediatePropagation();
};

class MediaPreview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleIndex: 0,
      mounted: false,
    };

    this.changeVisible = this.changeVisible.bind(this);
    this.increase = this.increase.bind(this);
    this.decrease = this.decrease.bind(this);
    this.close = this.close.bind(this);
    this.setMountAnimation = this.setMountAnimation.bind(this);
  }

  componentWillMount() {
    this.setMountAnimation(true);

    if (this.props.visibleIndex || this.props.visibleIndex !== 0) {
      this.setState({ visibleIndex: this.props.visibleIndex });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.visible !== this.props.visible && nextProps.visible) {
      this.setMountAnimation(true);
    }

    if (nextProps.items !== this.props.items && nextProps.items.length > 0) {
      this.setMountAnimation(true);
    }

    if (nextProps.leftIsPressed) {
      this.decrease();
    }

    if (nextProps.rightIsPressed) {
      this.increase();
    }

    if (
      nextProps.escapeIsPressed !== this.props.escapeIsPressed &&
      nextProps.escapeIsPressed
    ) {
      this.close();
    }

    if (nextProps.visibleIndex !== this.props.visibleIndex) {
      this.setState({ visibleIndex: nextProps.visibleIndex });
    }
  }

  setMountAnimation(mounted, timer = ANIMATION_TIME) {
    const that = this;
    setTimeout(() => {
      that.setState({ mounted });
    }, timer);
  }

  changeVisible(change) {
    const { items } = this.props;
    const min = 0;
    const max = items.length - 1;
    let oldVisibleIndex =
      this.state.visibleIndex !== null
        ? this.state.visibleIndex
        : this.props.visibleIndex;

    oldVisibleIndex = Math.min(Math.max(min, oldVisibleIndex), max);
    const visibleIndex = Math.min(Math.max(oldVisibleIndex + change, min), max);

    this.setState({ visibleIndex });
  }

  increase(event) {
    if (event) {
      stopPropagation(event);
    }
    this.changeVisible(1);
  }

  decrease(event) {
    if (event) {
      stopPropagation(event);
    }
    this.changeVisible(-1);
  }

  close() {
    const { onClose } = this.props;
    setTimeout(() => {
      onClose();
      this.setState({ visibleIndex: 0 });
    }, ANIMATION_TIME * 2);
    this.setMountAnimation(false);
  }

  render() {
    const { items, visible, meta, intl, downloadLinks } = this.props;
    const { formatMessage } = intl;

    const { mounted } = this.state;
    const max = items.length - 1;
    const min = 0;

    let visibleIndex = this.state.visibleIndex;

    if (visibleIndex > items.length) {
      visibleIndex = items.length - 1;
    } else if (visibleIndex < 0) {
      visibleIndex = 0;
    }

    const leftButtonClasses = classNames(
      style["left-button"],
      visibleIndex > min && style.visible
    );

    const rightButtonClasses = classNames(
      style["right-button"],
      visibleIndex < max && style.visible
    );

    if (items.length === 0 || !visible) {
      return null;
    }

    const containerClasses = classNames(
      style.container,
      mounted && style.visible
    );

    return (
      <div role={"button"} className={containerClasses} onClick={this.close}>
        <IconButton
          icon={"close"}
          inverse
          className={style["close-button"]}
          onClick={this.close}
        />
        {items.map((item, index) => {
          if (index !== visibleIndex) {
            return null;
          }

          return (
            <div className={style["item-container"]} key={`item-${index}`}>
              <div
                onClick={stopPropagation}
                role={"presentation"}
                className={style["item-inner-container"]}
              >
                <div
                  className={classNames(
                    style.item,
                    meta && meta[index] && style["with-meta"]
                  )}
                >
                  {item}
                </div>
                {meta && meta[index] && (
                  <div className={style.meta}>
                    {meta[index]}
                    {downloadLinks && downloadLinks[index] && (
                      <Button
                        icon="get_app"
                        label={intl.formatMessage(messages["download-button"])}
                        onClick={() => window.open(downloadLinks[index])}
                        className={style["download-button"]}
                      />
                    )}
                  </div>
                )}
              </div>
            </div>
          );
        })}

        <Button
          icon={"keyboard_arrow_left"}
          floating
          className={leftButtonClasses}
          onClick={this.decrease}
        />
        <Button
          icon={"keyboard_arrow_right"}
          inverse
          floating
          className={rightButtonClasses}
          onClick={this.increase}
        />
        {items.length > 1 && (
          <div className={style["text-container"]}>
            <span>
              {formatMessage(messages["message-x-of-y"], {
                x: visibleIndex + 1,
                y: items.length,
              })}
            </span>
          </div>
        )}
      </div>
    );
  }
}

MediaPreview.propTypes = {
  items: PropTypes.array, // eslint-disable-line
  meta: PropTypes.array, // eslint-disable-line
  downloadLinks: PropTypes.array, // eslint-disable-line
  onClose: PropTypes.func,
  visible: PropTypes.bool,
  leftIsPressed: PropTypes.bool,
  rightIsPressed: PropTypes.bool,
  escapeIsPressed: PropTypes.bool,
  visibleIndex: PropTypes.number,
  intl: intlShape.isRequired,
};

MediaPreview.defaultProps = {
  visible: true,
  visibleIndex: 0,
  intl: intlShape.isRequired,
};

export default withMonitorKeyPress({
  27: "escapeIsPressed",
  37: "leftIsPressed",
  39: "rightIsPressed",
})(safeInjectIntl(MediaPreview));
