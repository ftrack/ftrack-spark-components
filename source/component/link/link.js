import PropTypes from "prop-types";
import classNames from "classnames";

import styles from "./styles.scss";
import Skeleton from "../skeleton";

function Link({ className, to, title, onClick, disabled, isLoading }) {
  if (isLoading) {
    return (
      <span className={classNames(styles.root, className)}>
        <Skeleton style={{ width: "100px" }} />
      </span>
    );
  }
  if (onClick) {
    return (
      <span
        className={classNames(styles.root, className, {
          [`${styles.disabled}`]: disabled,
        })}
        onClick={onClick}
      >
        {title}
      </span>
    );
  }

  return (
    <a
      className={classNames(styles.root, className, {
        [`${styles.disabled}`]: disabled,
      })}
      href={to}
      target="_blank"
      rel="noopener noreferrer"
    >
      {title}
    </a>
  );
}

Link.propTypes = {
  to: PropTypes.string,
  title: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  isLoading: PropTypes.bool,
};

export default Link;
