// :copyright: Copyright (c) 2019 ftrack

import PropTypes from "prop-types";
import { defineMessages, intlShape } from "react-intl";
import { SwitchInput } from "../form/field/switch";
import safeInjectIntl from "../util/hoc/safe_inject_intl";

const messages = defineMessages({
  "switch-label-public": {
    id: "ftrack-spark-components.create-project-dialog.switch-label-public",
    description: "Label for when creating a public project.",
    defaultMessage: "Private access off",
  },
  "switch-label-private": {
    id: "ftrack-spark-components.create-project-dialog.switch-label-private",
    description: "Label for when creating a private project.",
    defaultMessage: "Private access on",
  },
  "switch-description-open-review": {
    id: "ftrack-spark-components.create-project-dialog.switch-description-open-review",
    description: "Label for when creating a open access project.",
    defaultMessage: "All users in the workspace have access",
  },
  "switch-description-open-studio": {
    id: "ftrack-spark-components.create-project-dialog.switch-description-open-studio",
    description: "Label for when creating a open access project.",
    defaultMessage:
      "Users with roles set for open access projects can view this project",
  },
  "switch-description-private": {
    id: "ftrack-spark-components.create-project-dialog.switch-description-private",
    description: "Label for when creating a private project.",
    defaultMessage: "Users must be invited to view this project",
  },
});

function ProjectAccessSwitch({ intl, product, value, onChange }) {
  let label;
  let description;
  if (!value) {
    label = intl.formatMessage(messages["switch-label-public"]);
    description = intl.formatMessage(
      messages[`switch-description-open-${product}`]
    );
  } else {
    label = intl.formatMessage(messages["switch-label-private"]);
    description = intl.formatMessage(messages["switch-description-private"]);
  }

  return (
    <SwitchInput
      label={label}
      description={description}
      onChange={onChange}
      value={value}
    />
  );
}

ProjectAccessSwitch.propTypes = {
  value: PropTypes.bool,
  intl: intlShape.isRequired,
  product: PropTypes.oneOf(["review", "studio"]),
  onChange: PropTypes.func,
};

ProjectAccessSwitch.defaultProps = {
  product: "review",
};

export default safeInjectIntl(ProjectAccessSwitch);
