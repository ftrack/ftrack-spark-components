// :copyright: Copyright (c) 2019 ftrack

import PropTypes from "prop-types";
import ProjectAccessSwitch from "./project_access_switch";

function ProjectAccessSwitchField({ id, formData, onChange, ...props }) {
  return (
    <ProjectAccessSwitch
      name={id}
      value={formData}
      onChange={onChange}
      {...props}
    />
  );
}

ProjectAccessSwitchField.propTypes = {
  id: PropTypes.string,
  formData: PropTypes.bool,
  onChange: PropTypes.func,
};

export default ProjectAccessSwitchField;
