// :copyright: Copyright (c) 2017 ftrack
import PropTypes from "prop-types";

import { defineMessages, FormattedMessage } from "react-intl";
import safeInjectIntl from "../util/hoc/safe_inject_intl";

const HELP_CENTER_URL = "https://www.ftrack.com/help";

const messages = defineMessages({
  message: {
    id: "ftrack-spark-components.create-project-dialog-success.message",
    defaultMessage: "Your new project has been created.",
  },
  "message-detail": {
    id: "ftrack-spark-components.create-project-dialog-success.message-detail",
    defaultMessage:
      "Go to project to manage tasks and users or click here to {inviteTeamMembersLink} to the project. Need help getting started? Check out this {gettingStartedLink}.",
  },
  "invite-team-members": {
    id: "ftrack-spark-components.create-project-dialog-success.invite-team-members",
    defaultMessage: "invite team members",
  },
  "help-center": {
    id: "ftrack-spark-components.create-project-dialog-success.help-center",
    defaultMessage: "help center",
  },
});

function CreateProjectSuccessMessage(props) {
  const onInviteClick = (event) => {
    props.onGoto(props.projectId, "team");
    event.preventDefault();
  };

  const inviteTeamMembersLink = (
    <a href="#invite-team-members" onClick={onInviteClick}>
      <FormattedMessage {...messages["invite-team-members"]} />
    </a>
  );

  const gettingStartedLink = (
    <a href={HELP_CENTER_URL} target="_blank" rel="noopener noreferrer">
      <FormattedMessage {...messages["help-center"]} />
    </a>
  );

  return (
    <p>
      <FormattedMessage {...messages.message} />
      <br />
      <FormattedMessage
        {...messages["message-detail"]}
        values={{
          inviteTeamMembersLink,
          gettingStartedLink,
        }}
      />
    </p>
  );
}

CreateProjectSuccessMessage.propTypes = {
  onGoto: PropTypes.func.isRequired,
  projectId: PropTypes.string.isRequired,
};

export default safeInjectIntl(CreateProjectSuccessMessage);
