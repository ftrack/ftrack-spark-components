// :copyright: Copyright (c) 2017 ftrack
import PropTypes from "prop-types";

import { action } from "@storybook/addon-actions";
import centered from "@storybook/addon-centered/react";
import { IntlProvider, addLocaleData } from "react-intl";

import getSession from "../../story/get_session";
import CreateProjectDialog from ".";
import CreateProjectSuccess from "./create_project_success";
import CreateProjectSuccessMessage from "./create_project_success_message";

// Add locale data for supported locales
import en from "react-intl/locale-data/en";
import zh from "react-intl/locale-data/zh";
import messages from "../../story/en-TEST.json";

addLocaleData([...en, ...zh]);

export default {
  title: "Create Project Dialog",
  decorators: [centered],
};

const session = getSession();

function CustomSuccessMessage({ projectId }) {
  return <span>{projectId}</span>;
}
CustomSuccessMessage.propTypes = { projectId: PropTypes.string };

export const Default = () => (
  <CreateProjectDialog
    session={session}
    onCancel={action("Cancel")}
    onProjectCreatedClose={action("Closing dialog, project created")}
    onProjectCreatedGoto={action("Navigating to project")}
  />
);

function LocalizedCreateProjectDialog({ intlProps, ...props }) {
  return (
    <IntlProvider {...intlProps}>
      <CreateProjectDialog {...props} />
    </IntlProvider>
  );
}

export const I18N = () => (
  <LocalizedCreateProjectDialog
    intlProps={{
      locale: "zh-CN",
      messages,
    }}
    session={session}
    onCancel={action("Cancel")}
    onProjectCreatedClose={action("Closing dialog, project created")}
    onProjectCreatedGoto={action("Navigating to project")}
  />
);

I18N.storyName = "i18n";

export const ReviewProject = () => (
  <CreateProjectDialog
    session={getSession()}
    onCancel={action("Cancel")}
    onProjectCreatedClose={action("Closing dialog, project created")}
    onProjectCreatedGoto={action("Navigating to project")}
    renderSuccess={null}
    attributes={["full_name", "is_private"]}
    style={{
      maxHeight: "250px",
      maxWidth: "400px",
    }}
  />
);

export const SuccessDefault = () => (
  <CreateProjectSuccess>
    <CreateProjectSuccessMessage
      projectId="foo"
      onGoto={action("Goto called")}
    />
  </CreateProjectSuccess>
);

SuccessDefault.storyName = "Success default";

export const SuccessCustom = () => (
  <CreateProjectSuccess>
    <CustomSuccessMessage projectId="foo" onGoto={action("Goto called")} />
  </CreateProjectSuccess>
);

SuccessCustom.storyName = "Success custom";
