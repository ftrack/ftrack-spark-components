const slugify = (string) =>
  (string || "")
    .trim()
    .toLowerCase()
    .replace(/ /g, "_")
    .replace(/([^a-zA-Z0-9._]+)/g, "");

export default slugify;
