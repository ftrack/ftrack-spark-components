..
    :copyright: Copyright (c) 2017 ftrack

#####################
Create Project Dialog
#####################

Create Project Dialog is a dialog for creating new projects. It is intended
to be useable by ftrack v3 as well as in other interfaces.

It has the following dependencies:

* "react-color": "^2.11.1",
* "react-dropzone": "^4.2.9",
* "react-intl": "^2.4.0",
* "react-jsonschema-form": "0.43.0",
* "react-onclickoutside": "^6.7.1",
* "react-select": "1.0.0-rc.2",
* "react-toolbox": "^1.0.0",
* "recompose": "^0.26.0"

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
className                      CSS Class to add to the root element.
session                        ftrack JS API session instance.
onCancel                       When canceling project creation
onProjectCreatedClose          When "Close" is clicked after creating a project
onProjectCreatedGoto           When "Goto" is clicked after creating a project
