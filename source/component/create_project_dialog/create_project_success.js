// :copyright: Copyright (c) 2017 ftrack
import FontIcon from "react-toolbox/lib/font_icon";
import { defineMessages, FormattedMessage } from "react-intl";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import style from "./style.scss";

const messages = defineMessages({
  title: {
    id: "ftrack-spark-components.create-project-dialog-success.title",
    defaultMessage: "Congratulations",
  },
});

function CreateProjectSuccess(props) {
  return (
    <div className={style.success}>
      <FontIcon className={style.icon} value="done" />
      <h1 className={style.successHeader}>
        <FormattedMessage {...messages.title} />
      </h1>
      {props.children}
    </div>
  );
}

export default safeInjectIntl(CreateProjectSuccess);
