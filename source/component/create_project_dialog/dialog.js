// :copyright: Copyright (c) 2017 ftrack
import PropTypes from "prop-types";

import { Component } from "react";
import log from "loglevel";
import moment from "moment";
import { v4 as uuidV4 } from "uuid";
import Dialog from "react-toolbox/lib/dialog";
import classNames from "classnames";
import { defineMessages, intlShape } from "react-intl";
import safeInjectIntl from "../util/hoc/safe_inject_intl";

import Overlay from "../overlay";
import { ENCODE_DATETIME_FORMAT } from "../util/constant";
import CreateProjectForm from "./form";
import CreateProjectSuccess from "./create_project_success";
import CreateProjectSuccessMessage from "./create_project_success_message";
import style from "./style.scss";
import slugify from "./slugify";

const messages = defineMessages({
  errorTitle: {
    id: "ftrack-spark-components.create-project-dialog.error-title",
    defaultMessage: "Failed to create project",
  },
  closeLabel: {
    id: "ftrack-spark-components.create-project-dialog.close-label",
    defaultMessage: "Close",
  },
  gotoLabel: {
    id: "ftrack-spark-components.create-project-dialog.goto-label",
    defaultMessage: "Go to project",
  },
  cancelLabel: {
    id: "ftrack-spark-components.create-project-dialog.cancel-label",
    defaultMessage: "Cancel",
  },
  createProjectLabel: {
    id: "ftrack-spark-components.create-project-dialog.create-project-label",
    defaultMessage: "Create project",
  },
});

/**
 * Create Project Dialog.
 */
class CreateProjectDialog extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: false, success: false, projectId: null };
    this.submitForm = this.submitForm.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleKeys = this.handleKeys.bind(this);
    this.handlePrimaryAction = this.handlePrimaryAction.bind(this);
  }

  componentWillMount() {
    window.document.body.addEventListener("keydown", this.handleKeys);
  }

  componentDidUpdate() {
    if (this.state.success && !this.props.renderSuccess) {
      this.props.onProjectCreatedGoto(this.state.projectId, null);
    }
  }

  componentWillUnmount() {
    window.document.body.removeEventListener("keydown", this.handleKeys);
  }

  handleKeys(e) {
    // Submit form on command/ctrl + return.
    if ((e.ctrlKey || e.metaKey) && e.which === 13) {
      e.preventDefault();
      this.handlePrimaryAction();
    }
  }

  handlePrimaryAction() {
    if (this.state.success) {
      this.props.onProjectCreatedGoto(this.state.projectId, null);
    } else {
      this.submitForm();
    }
  }

  /** Trigger form submission. */
  submitForm() {
    this.form.submit();
  }

  handleSubmit({ formData }) {
    this.setState({ loading: true });
    const { name, full_name } = formData;
    let code = name || (full_name ? slugify(full_name) : null);

    const ensureUniqueProjectCode = () =>
      new Promise((resolve) => {
        if (code === slugify(full_name)) {
          this.props.session
            .query(`select id from Project where name is "${code}" limit 1`)
            .then((response) => {
              if (response.data.length) {
                code = `${code}_${uuidV4().substr(0, 5)}`;
              }
              resolve();
            });
        } else {
          resolve();
        }
      });

    ensureUniqueProjectCode()
      .then(() => {
        log.debug("Submitting", formData);
        return this.props.session.create("Project", {
          color: formData.color.toUpperCase(),
          end_date: moment(formData.end_date).format(ENCODE_DATETIME_FORMAT),
          full_name: formData.full_name,
          name: code,
          project_schema_id: formData.project_schema,
          start_date: moment(formData.start_date).format(
            ENCODE_DATETIME_FORMAT
          ),
          status: "active",
          thumbnail_id: formData.thumbnail_id,
          is_private: formData.is_private,
        });
      })
      .then((response) => {
        const projectId = response.data.id;
        this.setState({ loading: false, success: true, projectId });
        if (this.props.onSuccess) {
          this.props.onSuccess(response.data.id);
        }
      })
      .catch((error) => {
        this.setState({ loading: false, error });
      });
  }

  render() {
    const { intl, title } = this.props;
    let overlayProps = {};
    if (this.state.loading) {
      overlayProps = { active: true, loader: true };
    }

    if (this.state.error) {
      overlayProps = {
        active: true,
        title: intl.formatMessage(messages.errorTitle),
        details: this.state.error.message,
        dismissable: true,
        onDismss: () => {
          this.setState({ error: null });
        },
      };
    }
    let actions = [];
    if (this.state.success) {
      actions = [
        {
          label: intl.formatMessage(messages.closeLabel),
          onClick: this.props.onProjectCreatedClose,
        },
        {
          label: intl.formatMessage(messages.gotoLabel),
          primary: true,
          onClick: this.handlePrimaryAction,
        },
      ];
    } else {
      actions = [
        {
          label: intl.formatMessage(messages.cancelLabel),
          onClick: this.props.onCancel,
        },
        {
          label: intl.formatMessage(messages.createProjectLabel),
          primary: true,
          onClick: this.handlePrimaryAction,
        },
      ];
    }
    const dialogClassNames = classNames(style.dialog, this.props.className);
    return (
      <Dialog
        active
        actions={actions}
        title={title}
        className={dialogClassNames}
      >
        {this.state.success && this.props.renderSuccess ? (
          this.props.renderSuccess({
            projectId: this.state.projectId,
            onGoto: this.props.onProjectCreatedGoto,
          })
        ) : (
          <CreateProjectForm
            formRef={(node) => {
              this.form = node;
            }}
            session={this.props.session}
            attributes={this.props.attributes}
            onSubmit={this.handleSubmit}
            intlMessages={{}}
          />
        )}
        <Overlay {...overlayProps} />
      </Dialog>
    );
  }
}

CreateProjectDialog.propTypes = {
  onCancel: PropTypes.func.isRequired,
  onProjectCreatedClose: PropTypes.func,
  onProjectCreatedGoto: PropTypes.func.isRequired,
  session: PropTypes.shape({
    create: PropTypes.func.isRequired,
    query: PropTypes.func,
  }).isRequired,
  className: PropTypes.string,
  attributes: PropTypes.arrayOf(PropTypes.string),
  renderSuccess: PropTypes.func,
  onSuccess: PropTypes.func,
  intl: intlShape,
  title: PropTypes.string,
};

CreateProjectDialog.defaultProps = {
  className: "",
  intlMessages: {
    errorTitle: "Failed to create project",
    closeLabel: "Close",
    gotoLabel: "Go to project",
    cancelLabel: "Cancel",
    createProjectLabel: "Create project",
  },
  renderSuccess: (props) => (
    <CreateProjectSuccess>
      <CreateProjectSuccessMessage {...props} />
    </CreateProjectSuccess>
  ),
};

export default safeInjectIntl(CreateProjectDialog);
