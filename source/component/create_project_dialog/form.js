// :copyright: Copyright (c) 2017 ftrack

import PropTypes from "prop-types";

import { Component } from "react";
import moment from "moment";
import log from "loglevel";
import { compose, withProps } from "recompose";
import debounce from "lodash/debounce";
import { operation } from "ftrack-javascript-api";
import { defineMessages, intlShape } from "react-intl";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import getRjsfSelector from "../form/widget/get_rjsf_selector";
import getCustomField from "../form/form_field/get_custom_field";
import RemoteSelector from "../selector/remote_selector";
import Form from "../form";
import ColorField from "../form/field/color";
import ThumbnailField from "../form/field/thumbnail";
import ProjectAccessSwitchField from "./project_access_switch_field";
import { PROJECT_COLORS, DATETIME_ISO_FORMAT } from "../util/constant";
import style from "./style.scss";
import slugify from "./slugify";

const withRjsfSelector = (BaseComponent) => getRjsfSelector(BaseComponent);
const withCustomField = (BaseComponent) => getCustomField(BaseComponent);
const ProjectSchemaField = compose(
  withCustomField,
  withRjsfSelector,
  withProps(() => ({
    entityType: "ProjectSchema",
  }))
)(RemoteSelector);

const messages = defineMessages({
  "attribute-thumbnail-message": {
    id: "ftrack-spark-components.create-project-form.attribute-thumbnail-message",
    defaultMessage: "Drop image here or click to browse",
  },
  "attribute-title-full-name": {
    id: "ftrack-spark-components.create-project-form.attribute-title-full-name",
    defaultMessage: "Name",
  },
  "attribute-title-name": {
    id: "ftrack-spark-components.create-project-form.attribute-title-name",
    defaultMessage: "Code",
  },
  "attribute-title-project-schema": {
    id: "ftrack-spark-components.create-project-form.attribute-title-project-schema",
    defaultMessage: "Workflow",
  },
  "attribute-title-start-date": {
    id: "ftrack-spark-components.create-project-form.attribute-title-start-date",
    defaultMessage: "Start",
  },
  "attribute-title-end-date": {
    id: "ftrack-spark-components.create-project-form.attribute-title-end-date",
    defaultMessage: "End",
  },
  "attribute-help-name": {
    id: "ftrack-spark-components.create-project-form.attribute-help-name",
    defaultMessage: "A unique name used by the API and integrations.",
  },
  "attribute-help-project-schema": {
    id: "ftrack-spark-components.create-project-form.attribute-help-project-schema",
    defaultMessage:
      "The project workflow defines what objects, types and statuses will be used for this project and can be configured in system settings.",
  },
  "error-project-code-taken": {
    id: "ftrack-spark-components.create-project-form.error-project-code-taken",
    defaultMessage: "Project code must be unique.",
  },
  "error-start-date": {
    id: "ftrack-spark-components.create-project-form.error-start-date",
    defaultMessage: "Start date must be before end.",
  },
});

const fields = {
  color: ColorField,
  thumbnail: ThumbnailField,
  project_schema: ProjectSchemaField,
  is_private_review: withProps({ product: "review" })(ProjectAccessSwitchField),
  is_private_studio: withProps({ product: "studio" })(ProjectAccessSwitchField),
};

function randomChoice(choices) {
  const index = Math.floor(Math.random() * choices.length);
  return choices[index];
}

class CreateProjectForm extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.validate = this.validate.bind(this);
    this.getSettings = this.getSettings.bind(this);
    this.validateProjectCode = this.validateProjectCode.bind(this);
    const VALIDATE_PROJECT_CODE_DELAY = 400;
    this.validateProjectCodeDebounced = debounce(
      this.validateProjectCode,
      VALIDATE_PROJECT_CODE_DELAY,
      { leading: false, trailing: true }
    );

    const formatMessage = this.props.intl.formatMessage;
    const formData = {
      full_name: "",
      start_date: moment().startOf("day").format(DATETIME_ISO_FORMAT),
      end_date: moment()
        .startOf("day")
        .add("month", "1")
        .format(DATETIME_ISO_FORMAT),
      color: randomChoice(PROJECT_COLORS),
    };
    const formContext = {
      session: props.session,
      sundayFirstDayOfWeek: false,
    };
    this.state = { formData, formContext };

    this.schema = {
      type: "object",
      required: [
        "full_name",
        "name",
        "project_schema",
        "start_date",
        "end_date",
      ],
      properties: {
        thumbnail_id: { type: "string" },
        color: { type: "string" },
        full_name: {
          type: "string",
          title: formatMessage(messages["attribute-title-full-name"]),
          default: "",
        },
        name: {
          type: "string",
          title: formatMessage(messages["attribute-title-name"]),
        },
        project_schema: {
          type: "string",
          title: formatMessage(messages["attribute-title-project-schema"]),
        },
        start_date: {
          type: "string",
          format: "date-time",
          title: formatMessage(messages["attribute-title-start-date"]),
        },
        end_date: {
          type: "string",
          format: "date-time",
          title: formatMessage(messages["attribute-title-end-date"]),
        },
        is_private: { type: "boolean" },
      },
    };

    this.schema.required = this.schema.required.filter((attribute) =>
      this.props.attributes.includes(attribute)
    );
    this.schema.properties = this.props.attributes.reduce((result, key) => {
      result[key] = this.schema.properties[key];
      return result;
    }, {});

    const isStudio = this.props.attributes.includes("project_schema");

    const uiSchema = {
      name: {
        reveal: true,
        "ui:help": formatMessage(messages["attribute-help-name"]),
      },
      full_name: {
        "ui:autofocus": true,
      },
      project_schema: {
        "ui:field": "project_schema",
        "ui:help": formatMessage(messages["attribute-help-project-schema"]),
      },
      thumbnail_id: {
        classNames: style.thumbnailField,
        "ui:field": "thumbnail",
        thumbnailSize: "xlarge",
        message: formatMessage(messages["attribute-thumbnail-message"]),
      },
      start_date: {
        "ui:widget": "date",
        classNames: style.dateField,
      },
      end_date: {
        "ui:widget": "date",
        classNames: style.dateField,
      },
      is_private: {
        "ui:field": isStudio ? "is_private_studio" : "is_private_review",
        classNames: style.privateField,
      },
      color: {
        "ui:displayLabel": false,
        "ui:field": "color",
        classNames: style.colorField,
      },
    };

    this.uiSchema = this.props.attributes.reduce((result, key) => {
      result[key] = uiSchema[key];
      return result;
    }, {});
  }

  componentDidMount() {
    this.getSettings();
  }

  getSettings() {
    this.props.session
      .call([
        operation.query(
          'select value from Setting where group is "DEFAULT" and name is "workflowid" limit 1'
        ),
        operation.query(
          'select value from Setting where group is "TIME" and name is "week_startday" limit 1'
        ),
      ])
      .then((responses) => {
        const defaultSchemaId = responses[0].data[0]
          ? responses[0].data[0].value
          : undefined;
        log.debug("Retrieved default project schema", defaultSchemaId);
        const weekStartDay = responses[1].data[0] && responses[1].data[0].value;
        log.debug("Retrieved week startday", weekStartDay);
        const sundayFirstDayOfWeek = weekStartDay === "0";

        this.setState({
          formContext: Object.assign({}, this.state.formContext, {
            sundayFirstDayOfWeek,
          }),
          formData: Object.assign({}, this.state.formData, {
            project_schema: defaultSchemaId,
          }),
        });
      });
  }

  transformFormData(formData) {
    let nextFormData = formData;
    if (nextFormData.full_name !== this.state.formData.full_name) {
      if (
        !this.state.formData.name ||
        this.state.formData.name === slugify(this.state.formData.full_name)
      ) {
        nextFormData = Object.assign({}, formData, {
          name: slugify(nextFormData.full_name),
        });
      }
    }
    return nextFormData;
  }

  validateProjectCode() {
    const name = this.state.formData.name;
    if (name === this._lastValidatedName) {
      return;
    }
    this._lastValidatedName = name;
    this.setState({ projectCodeTaken: false });
    this.props.session
      .query(`select id from Project where name is "${name}" limit 1`)
      .then((response) => {
        if (response.data.length) {
          this.setState({
            projectCodeTaken: true,
          });
        }
      });
  }

  validate(formData, errors) {
    const formatMessage = this.props.intl.formatMessage;
    if (this.state.projectCodeTaken) {
      errors.name.addError(formatMessage(messages["error-project-code-taken"]));
    }

    if (formData.end_date && formData.end_date < formData.start_date) {
      errors.start_date.addError(formatMessage(messages["error-start-date"]));
    }

    return errors;
  }

  handleChange({ formData }) {
    if (this.props.attributes.indexOf("name") !== -1) {
      this.setState({
        formData: this.transformFormData(formData),
      });
      this.validateProjectCodeDebounced();
    }
  }

  render() {
    return (
      <Form
        ref={this.props.formRef}
        schema={this.schema}
        uiSchema={this.uiSchema}
        {...this.props}
        validate={this.validate}
        fields={fields}
        formContext={this.state.formContext}
        formData={this.state.formData}
        onChange={this.handleChange}
      />
    );
  }
}

CreateProjectForm.propTypes = {
  intl: intlShape.isRequired,
  onSubmit: PropTypes.func.isRequired, // eslint-disable-line
  session: PropTypes.object.isRequired, // eslint-disable-line
  intlMessages: PropTypes.object.isRequired, // eslint-disable-line
  attributes: PropTypes.arrayOf(PropTypes.string),
  formRef: PropTypes.string,
};

CreateProjectForm.defaultProps = {
  attributes: [
    "thumbnail_id",
    "color",
    "full_name",
    "name",
    "project_schema",
    "start_date",
    "end_date",
    "is_private",
  ],
};

export default safeInjectIntl(CreateProjectForm, { withRef: true });
