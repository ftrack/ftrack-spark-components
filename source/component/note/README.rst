..
    :copyright: Copyright (c) 2018 ftrack

####
Note
####

A set of components that can be used to display, write and edit ftrack Notes.

Components in this module has the following dependencies:

    * react-onclickoutside ^5.8.4

Editable note
-------------

This component renders a note and if the author prop is matching the Note ahutor
id it will allow the user to edit and delete the note from a dropdown menu.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================

note                           Note data passed when to the Note component.
entityInformation              An optional entity information component.
session                        A ftrack-javascript-api session.
collapsed          true        Switch to indicate if the form or note should be displayed.
pendingTodo        false       Render the todo component as pending or not.
content                        Content of the note.
pending            false       If the note is beeing saved or not.
onShowForm                     Callback to show the form.
onHideForm                     Callback to hide the form.
onSubmitForm                   Callback to submit the form.
onRemove                       Callback to remove the note.
onAttachmentClick              Callback when an attachment is clicked.
onTodoChange                   Callback when a note todo is clicked.
author                         The author of the note.

Note
----

This component allows the user to view a note, including attachments and if
it can be completed.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================

session                        An ftrack-javascript-api session.
entityInformation              An optional entity information component.
data                           The note data to be displayed.
onAttachmentClick              Callback when an attachment is clicked.
onTodoChange                   Callback when a note todo is clicked.
category           false       If the note should display the category or not.
todo               false       If the note is a todo or not.
completedBy        null        User who completed the note or null.
completedAt        null        A moment date when the note was completed or null.
pendingTodo        false       Render the todo component as pending or not.

Form
----

This form component can be used to or edit an existing note.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
content                        Content of the note.
className                      CSS Class to add to the root element.
onClickOutside                 Callback if a user clicks outside of the form.
onSubmit                       Callback to submit the form.
onExpand                       Callback if the form is expanded.
edit                           Indiciates if the for is for edit or create of a note.
collapsed                      If the form is collapsed or not.
pending            false       If the note is beeing saved or not.
autoFocus                      If the note form input field should be focused or not.
author                         The author of the note.


Reply
-----

A reply button component that reveals a Note form if clicked.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
author                         The author of the note.
collapsed                      If the reply form is collapsed or not.
content                        Content of the note.
pending            false       If the note is beeing saved or not.
onShowForm                     Callback to show the form.
onHideForm                     Callback to hide the form.
onSubmitForm                   Callback to submit the form.
parentNote                     The note parent data.

Attachment area
---------------

Displays a series of attachments. If the attachment file is of a supported
image format a small thumbnail will be displayed.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================

session                        A ftrack-javascript-api session.
components                     The components that the notes area should show.
onAttachmentClick              Callback when an attachment is clicked.
