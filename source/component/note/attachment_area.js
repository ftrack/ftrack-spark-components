// :copyright: Copyright (c) 2016 ftrack

import PropTypes from "prop-types";

import { Component } from "react";

import FontIcon from "react-toolbox/lib/font_icon";
import isMedia from "../util/is_media";

import style from "./style.scss";

/** Attachment area component to display an array of *components*.
 *
 * *onAttachmentClick* prop is called when an attachment media or document is
 * clicked.
 *
 */

class AttachmentArea extends Component {
  /** Handle click on attachment *component*. */
  onAttachmentClick(component) {
    const { noteComponents } = this.props;
    if (this.props.onAttachmentClick) {
      const clickedPreviewIndex = noteComponents.findIndex(
        (componentToCheck) => componentToCheck.component.id === component.id
      );
      this.props.onAttachmentClick(
        clickedPreviewIndex,
        noteComponents.map((noteComponent) => ({
          ...noteComponent.component,
          url: noteComponent.url && noteComponent.url.value,
          thumbnail_url:
            noteComponent.thumbnail_url && noteComponent.thumbnail_url.value,
        }))
      );
    }
  }

  render() {
    const { noteComponents } = this.props;

    const images = [];
    const other = [];

    noteComponents.forEach((noteComponent) => {
      const { component } = noteComponent;
      const fileType = component.file_type.slice(1);
      if (isMedia(fileType) && noteComponent.thumbnail_url) {
        const thumbnailUrl = noteComponent.thumbnail_url.value;
        images.push(
          <div
            key={component.id}
            onClick={() => this.onAttachmentClick(component)}
            className={style.image}
            style={{ backgroundImage: `url('${thumbnailUrl}')` }}
          />
        );
      } else {
        other.push(
          <p key={component.id}>
            <FontIcon className={style["attachment-icon"]} value="attachment" />
            <span
              className={style["file-name"]}
              onClick={() => this.onAttachmentClick(component)}
            >
              {`${component.name}${component.file_type}`}
            </span>
          </p>
        );
      }
    });

    if (images.length || other.length) {
      return (
        <div className={style["attachments-area"]}>
          <div className={style.images}>{images}</div>
          <div className={style.other}>{other}</div>
        </div>
      );
    }

    return null;
  }
}

AttachmentArea.propTypes = {
  session: PropTypes.object.isRequired,
  noteComponents: PropTypes.array.isRequired,
  onAttachmentClick: PropTypes.func,
};

export default AttachmentArea;
