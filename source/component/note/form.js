// :copyright: Copyright (c) 2016 ftrack

import PropTypes from "prop-types";

import { useState, useEffect } from "react";
import { ProgressBar, Button } from "react-toolbox";
import onClickOutsideWrapper from "react-onclickoutside";
import { defineMessages, intlShape, FormattedMessage } from "react-intl";
import classNames from "classnames";
import debounce from "lodash/debounce";

import safeInjectIntl from "../util/hoc/safe_inject_intl";
import Editor from "../editor";
import IconButton from "../icon_button";

import EntitySelector from "./entity_selector";
import NoteLabelSelector from "./note_label_selector";

import style from "./style.scss";
import activeButtonTheme from "./active_button_theme.scss";
import inactiveButtonTheme from "./inactive_button_theme.scss";

const messages = defineMessages({
  "attribute-label-update-note": {
    id: "ftrack-spark-components.note-form.label-update-note",
    defaultMessage: "Update your feedback...",
  },
  "attribute-label-write-comment": {
    id: "ftrack-spark-components.note-form.label-write-comment",
    defaultMessage: "Write something...",
  },
  "attribute-label-update": {
    id: "ftrack-spark-components.note-form.label-update",
    defaultMessage: "Update",
  },
  "attribute-label-comment": {
    id: "ftrack-spark-components.note-form.label-comment",
    defaultMessage: "Post",
  },
  "completable-note-tooltip": {
    id: "ftrack-spark-components.note-form.completable-note-tooltip",
    description: "Tooltip for completable note",
    defaultMessage: "Select if this note can be marked as completed",
  },
});

const FORM_INPUT_DELAY = 250;

/** Note form use to create or edit a note. */
function NoteForm({
  session,
  content,
  recipients,
  isTodo,
  labels,
  pending,
  collapsed,
  edit,
  autoFocus,
  className,
  onSubmit,
  onExpand,
  onClickOutside,
  entitySelector,
  entity,
  intl,
  canMention,
  showCompletableNotes,
  showNoteLabelSelector,
}) {
  const { formatMessage } = intl;
  const [editorKey, setEditorKey] = useState(0);
  const [editorContent, setEditorContent] = useState(content);

  const [editorIsCompletable, setEditorIsCompletable] = useState(isTodo);
  const [editorSelectedLabels, setEditorSelectedLabels] = useState(
    labels || []
  );

  const [selectedRecipients, setSelectedRecipients] = useState([]);
  const [selectedEntity, setSelectedEntity] = useState(entity);
  const classes = classNames(style["note-form"], className);

  const handleSubmit = () => {
    const labelIds = editorSelectedLabels.map((label) => label.id);
    onSubmit({
      content: editorContent,
      entity: selectedEntity,
      recipients: selectedRecipients,
      is_todo: editorIsCompletable,
      labelIds,
    });
    setEditorKey(editorKey + 1);
  };

  const onContentChangeDebounced = debounce(
    ({ markdown, recipients: newRecipients }) => {
      setEditorContent(markdown);
      setSelectedRecipients(newRecipients);
    },
    FORM_INPUT_DELAY
  );

  const handleUserKeyPress = (e) => {
    if ((e.ctrlKey || e.metaKey) && e.key === "Enter" && collapsed !== true) {
      handleSubmit();
    }
  };

  useEffect(
    () => {
      window.addEventListener("keydown", handleUserKeyPress);
      return () => {
        window.removeEventListener("keydown", handleUserKeyPress);
      };
    }, // TODO: Feel free to fix this eslint error if you got time
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [editorContent, editorKey]
  );

  NoteForm.handleClickOutside = (e) => {
    const formatToolSelected = [
      "format_bold",
      "format_italic",
      "format_strikethrough",
      "insert_link",
      "format_list_bulleted",
    ].includes(e.target.innerText);
    const ancestor = e.target.closest("[data-mention]");
    const mentionSelected = ancestor && ancestor.hasAttribute("data-mention");
    if (onClickOutside && !formatToolSelected && !mentionSelected) {
      onClickOutside({
        content: editorContent,
        collapsed,
      });
    }
  };

  const tools = [];

  if (pending) {
    tools.push(
      <div key="filler" />,
      <div key="progress-wrapper" className={style.progressbar}>
        <ProgressBar type="circular" mode="indeterminate" />
      </div>
    );
  } else {
    if (entitySelector) {
      tools.push(
        <EntitySelector
          key="entity-selector"
          onSelect={setSelectedEntity}
          data={entitySelector}
          selected={selectedEntity}
        />
      );
    } else {
      tools.push(<div key="filler" />);
    }

    tools.push(
      <Button
        key="submit-button"
        className={style.submitButton}
        onClick={handleSubmit}
        disabled={!editorContent}
        label={
          edit
            ? formatMessage(messages["attribute-label-update"])
            : formatMessage(messages["attribute-label-comment"])
        }
      />
    );
  }

  return (
    <div className={classes}>
      {showCompletableNotes && edit && (
        <IconButton
          icon="check_circle"
          onClick={() => setEditorIsCompletable(!editorIsCompletable)}
          tooltip={
            <FormattedMessage {...messages["completable-note-tooltip"]} />
          }
          theme={!editorIsCompletable ? inactiveButtonTheme : activeButtonTheme}
        />
      )}
      {showNoteLabelSelector && edit && (
        <NoteLabelSelector
          session={session}
          selectedNoteLabels={editorSelectedLabels}
          attachNoteLabel={(noteLabel) => {
            const noteLabels = [...editorSelectedLabels];
            if (
              !noteLabels.some((candidate) => candidate.id === noteLabel.id)
            ) {
              noteLabels.push(noteLabel);
            }
            setEditorSelectedLabels(noteLabels);
          }}
          removeNoteLabel={(noteLabel) => {
            const noteLabels = editorSelectedLabels.filter(
              (selectedNoteLabel) => noteLabel.id !== selectedNoteLabel.id
            );
            setEditorSelectedLabels(noteLabels);
          }}
          position="topLeft"
        />
      )}
      <Editor
        key={editorKey}
        placeholder={
          edit
            ? formatMessage(messages["attribute-label-update-note"])
            : formatMessage(messages["attribute-label-write-comment"])
        }
        submitTitle={
          edit
            ? formatMessage(messages["attribute-label-update"])
            : formatMessage(messages["attribute-label-comment"])
        }
        content={content}
        recipients={recipients}
        disabled={pending}
        autoFocus={autoFocus}
        canMention={canMention}
        onChange={onContentChangeDebounced}
        onFocus={() => {
          if (collapsed) onExpand();
        }}
        legacyStyle
      />
      <div className={style.toolbar}>{tools}</div>
    </div>
  );
}

NoteForm.propTypes = {
  session: PropTypes.object,
  isTodo: PropTypes.object,
  labels: PropTypes.array,
  content: PropTypes.string,
  recipients: PropTypes.arrayOf(
    PropTypes.shape({
      resource_id: PropTypes.string,
      text_mentioned: PropTypes.string,
    })
  ),
  className: PropTypes.string,
  onSubmit: PropTypes.func,
  onExpand: PropTypes.func,
  onClickOutside: PropTypes.func,
  edit: PropTypes.bool,
  autoFocus: PropTypes.bool,
  collapsed: PropTypes.bool,
  pending: PropTypes.bool,
  entity: PropTypes.object,
  entitySelector: PropTypes.array,
  intl: intlShape.isRequired,
  canMention: PropTypes.bool,
  showNoteLabelSelector: PropTypes.bool,
  showCompletableNotes: PropTypes.bool,
};

NoteForm.defaultProps = {
  onSubmit: () => {},
  onExpand: () => {},
  showNoteLabelSelector: false,
  showCompletableNotes: false,
};

const clickOutsideConfig = {
  handleClickOutside: () => NoteForm.handleClickOutside,
};

export default safeInjectIntl(
  onClickOutsideWrapper(NoteForm, clickOutsideConfig)
);
