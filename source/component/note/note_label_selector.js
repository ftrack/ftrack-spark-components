// :copyright: Copyright (c) 2021 ftrack

import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { defineMessages, FormattedMessage } from "react-intl";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import { IconButton, MenuItem } from "@mui/material";
import LocalOfferIcon from "@mui/icons-material/LocalOffer";

import withTooltip from "react-toolbox/lib/tooltip";

import Label from "../label";
import ButtonMenu from "../button_menu";

import style from "./style.scss";

const TooltipDiv = withTooltip("div");

const messages = defineMessages({
  "note-label-remove-tooltip": {
    id: "overview.feedback.note-label-remove-tooltip",
    description: "Tooltip to remove note labels",
    defaultMessage: "Click to remove",
  },
});

const NoteLabelSelector = ({
  selectedNoteLabels,
  attachNoteLabel,
  removeNoteLabel,
  session,
  position,
}) => {
  const [noteLabels, setNoteLabels] = useState(null);

  useEffect(() => {
    session
      .query("select id, color, name, sort from NoteLabel")
      .then((response) => {
        const labels = response.data;
        labels.sort((l1, l2) => l1.sort - l2.sort);
        setNoteLabels(labels);
      });
  }, [session]);

  return [
    <ButtonMenu
      button={
        <IconButton size="small">
          <LocalOfferIcon sx={{ fontSize: "18px", color: "text.secondary" }} />
        </IconButton>
      }
    >
      {noteLabels &&
        noteLabels
          .filter(
            (nl) =>
              !selectedNoteLabels.some(
                (selectedNoteLabel) => selectedNoteLabel.id === nl.id
              )
          )
          .map((nl) => (
            <MenuItem
              key={nl.id}
              onClick={() => {
                setTimeout(() => attachNoteLabel(nl), 200);
              }}
            >
              {`${nl.name}`}
            </MenuItem>
          ))}
    </ButtonMenu>,
    selectedNoteLabels.map((l) => (
      <TooltipDiv
        className={style["selected-note-label"]}
        tooltipPosition="top"
        tooltip={
          <FormattedMessage {...messages["note-label-remove-tooltip"]} />
        }
        onClick={() => {
          removeNoteLabel(l);
        }}
      >
        <Label color={l.color}>{l.name}</Label>
      </TooltipDiv>
    )),
  ];
};

NoteLabelSelector.propTypes = {
  session: PropTypes.object.isRequired,
  selectedNoteLabels: PropTypes.array,
  attachNoteLabel: PropTypes.func.isRequired,
  removeNoteLabel: PropTypes.func.isRequired,
  position: PropTypes.string,
};

NoteLabelSelector.defaultProps = {
  selectedNoteLabels: [],
  position: "bottomLeft",
};

export default safeInjectIntl(NoteLabelSelector);
