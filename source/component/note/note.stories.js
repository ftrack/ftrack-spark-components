import { action } from "@storybook/addon-actions";
// eslint-disable-next-line
import centered from "@storybook/addon-centered/react";
import moment from "moment";
import { withState } from "recompose";
import PeopleIcon from "@mui/icons-material/People";
import OndemandVideoIcon from "@mui/icons-material/OndemandVideo";

import configureStore from "../../story/configure_store.js";
import getSession from "../../story/get_session";
import components from ".";
import { SparkProvider } from "../util/hoc";

const REVIEW_SESSION_NOTE_CATEGORY = "42983ba0-53b0-11e4-916c-0800200c9a66";

const { Note, NoteCompact, EditableNote, ReplyForm, NoteForm } = components;

function Provider({ story, store, session }) {
  return <SparkProvider session={session}>{story}</SparkProvider>;
}

const store = configureStore((state = {}) => state, [], getSession());

const SizeDecorator = (storyFn) => (
  <div
    style={{
      width: "600px",
      display: "flex",
      flexDirection: "column",
    }}
  >
    {storyFn()}
  </div>
);

export default {
  title: "Note",
  decorators: [
    (Story) => (
      <Provider story={<Story />} store={store} session={getSession()} />
    ),
    SizeDecorator,
    centered,
  ],
};

export const NoteWithAnnotationMarker = () => (
  <Note
    session={getSession()}
    data={{
      date: moment(),
      note_components: [
        {
          component: {
            id: "some-component",
            name: "Annotation",
            file_type: ".jpg",
            metadata: [
              {
                key: "annotationData",
                value: '[{ "type": "circle" }]',
              },
            ],
            component_locations: [],
          },
        },
      ],
      content: "**Foo** bar",
      frame: { number: 0, type: "image" },
      author: {
        thumbnail_id: null,
        first_name: "Emma",
        last_name: "Back",
        __entity_type__: "User",
      },
    }}
  />
);

NoteWithAnnotationMarker.storyName = "Note with annotation marker";

NoteWithAnnotationMarker.parameters = {
  info: "Note with annotation marker",
};

export const NoteBasic = () => (
  <Note
    session={getSession()}
    data={{
      date: moment(),
      note_components: [],
      content: "**Foo** bar",
      author: {
        thumbnail_id: null,
        first_name: "Mattias",
        last_name: "Lagergren",
        __entity_type__: "User",
      },
    }}
  />
);

NoteBasic.storyName = "Note basic";

NoteBasic.parameters = {
  info: "This example shows a basic Note component.",
};

export const _NoteCompact = () => (
  <NoteCompact
    session={getSession()}
    data={{
      date: moment(),
      note_components: [],
      content: "Always Look On The Bright Side Of Life",
      author: {
        thumbnail_id: null,
        first_name: "Viola",
        last_name: "Alvarez",
        __entity_type__: "User",
      },
    }}
  />
);

_NoteCompact.storyName = "Note compact";

_NoteCompact.parameters = {
  info: "This example shows a NoteCompoact",
};

export const NoteWithApproval = () => (
  <Note
    session={getSession()}
    data={{
      date: moment(),
      note_components: [],
      content: "**Foo** bar",
      inviteeStatus: "approved",
      author: {
        thumbnail_id: null,
        first_name: "Mattias",
        last_name: "Lagergren",
        __entity_type__: "User",
      },
    }}
  />
);

NoteWithApproval.storyName = "Note with approval";

NoteWithApproval.parameters = {
  info: "This example shows a Note with approval from invitee.",
};

export const NoteWithRequireChanges = () => (
  <Note
    session={getSession()}
    data={{
      date: moment(),
      note_components: [],
      content: "**Foo** bar",
      inviteeStatus: "require_changes",
      author: {
        thumbnail_id: null,
        first_name: "Mattias",
        last_name: "Lagergren",
        __entity_type__: "User",
      },
    }}
  />
);

NoteWithRequireChanges.storyName = "Note with require changes";

NoteWithRequireChanges.parameters = {
  info: "This example shows a Note with require changes from invitee.",
};

export const NoteWithFrameNumber = () => (
  <Note
    session={getSession()}
    data={{
      date: moment(),
      note_components: [],
      content: "**Foo** bar",
      frame: { number: 5 },
      author: {
        thumbnail_id: null,
        first_name: "Mattias",
        last_name: "Lagergren",
        __entity_type__: "User",
      },
    }}
  />
);

NoteWithFrameNumber.storyName = "Note with frame number";

NoteWithFrameNumber.parameters = {
  info: "This example shows a Note with frame component.",
};

export const NoteWithFrameNumberAsTime = () => (
  <Note
    session={getSession()}
    data={{
      date: moment(),
      note_components: [],
      content: "**Foo** bar",
      frame: { number: 128, time: 5.123 },
      author: {
        thumbnail_id: null,
        first_name: "Mattias",
        last_name: "Lagergren",
        __entity_type__: "User",
      },
    }}
  />
);

NoteWithFrameNumberAsTime.storyName = "Note with frame number as time";

NoteWithFrameNumberAsTime.parameters = {
  info: "This example shows a Note with frame component.",
};

export const NoteWithPage = () => (
  <Note
    session={getSession()}
    data={{
      date: moment(),
      note_components: [],
      content: "**Foo** bar",
      frame: { type: "pdf", number: 0 },
      author: {
        thumbnail_id: null,
        first_name: "Mattias",
        last_name: "Lagergren",
        __entity_type__: "User",
      },
    }}
  />
);

NoteWithPage.storyName = "Note with page";

NoteWithPage.parameters = {
  info: "This example shows a Note with frame component for a page.",
};

export const NoteWithRequireChangesAndFrame = () => (
  <Note
    session={getSession()}
    data={{
      date: moment(),
      note_components: [],
      content: "**Foo** bar",
      frame: { number: 5 },
      inviteeStatus: "require_changes",
      author: {
        thumbnail_id: null,
        first_name: "Mattias",
        last_name: "Lagergren",
        __entity_type__: "User",
      },
    }}
  />
);

NoteWithRequireChangesAndFrame.storyName =
  "Note with require changes and frame";

NoteWithRequireChangesAndFrame.parameters = {
  info: "This example shows a Note with require changes and frame from invitee.",
};

export const NoteWithCategoryBackwardsCompatibility = () => (
  <Note
    session={getSession()}
    category
    data={{
      date: moment(),
      note_components: [],
      content: "**Foo** bar",
      category: { name: "Animation", id: "foo-bar" },
      author: {
        thumbnail_id: null,
        first_name: "Mattias",
        last_name: "Lagergren",
        __entity_type__: "User",
      },
    }}
  />
);

NoteWithCategoryBackwardsCompatibility.storyName =
  "Note with category (backwards compatibility)";

NoteWithCategoryBackwardsCompatibility.parameters = {
  info: "This example shows a Note with category component.",
};

export const NoteWithLabels = () => (
  <Note
    session={getSession()}
    category
    data={{
      date: moment(),
      note_components: [],
      content: "**Foo** bar",
      note_label_links: [
        {
          label: {
            name: "Animation",
            color: "#05AB63",
            id: "foo-bar",
          },
        },
        {
          label: {
            name: "Compositing",
            color: "#F08154",
            id: "foo-bar-2",
          },
        },
      ],
      author: {
        thumbnail_id: null,
        first_name: "Mattias",
        last_name: "Lagergren",
        __entity_type__: "User",
      },
    }}
  />
);

NoteWithLabels.storyName = "Note with labels";

NoteWithLabels.parameters = {
  info: "This example shows a Note with labels.",
};

export const NoteWithLabelsAndApproval = () => (
  <Note
    session={getSession()}
    category
    data={{
      date: moment(),
      note_components: [],
      content: "**Foo** bar",
      note_label_links: [
        {
          label: {
            name: "Animation",
            color: "#05AB63",
            id: "foo-bar",
          },
        },
        {
          label: {
            name: "Compositing",
            color: "#F08154",
            id: "foo-bar-2",
          },
        },
      ],
      inviteeStatus: "approved",
      author: {
        thumbnail_id: null,
        first_name: "Mattias",
        last_name: "Lagergren",
        __entity_type__: "User",
      },
    }}
  />
);

NoteWithLabelsAndApproval.storyName = "Note with labels and approval";

NoteWithLabelsAndApproval.parameters = {
  info: "This example shows a Note with labels and approval.",
};

export const NoteWithUnknownAuthor = () => (
  <Note
    session={getSession()}
    category
    data={{
      date: moment(),
      note_components: [],
      content: "**Foo** bar",
      author: null,
    }}
  />
);

NoteWithUnknownAuthor.storyName = "Note with unknown author";

NoteWithUnknownAuthor.parameters = {
  info: "This example shows a Note with unknown author component.",
};

export const NoteWithReviewSessionCategory = () => (
  <Note
    session={getSession()}
    category
    data={{
      date: moment(),
      note_components: [],
      content: "**Foo** bar",
      category: {
        name: "Animation",
        id: REVIEW_SESSION_NOTE_CATEGORY,
      },
      author: {
        thumbnail_id: null,
        first_name: "Mattias",
        last_name: "Lagergren",
        __entity_type__: "User",
      },
    }}
  />
);

NoteWithReviewSessionCategory.storyName = "Note with review session category";

NoteWithReviewSessionCategory.parameters = {
  info: "This example shows a Note with review session category component.",
};

export const NoteWithTodo = () => (
  <Note
    session={getSession()}
    category
    todo
    onTodoChange={action("onTodoChange")}
    data={{
      date: moment(),
      note_components: [],
      content: "**Foo** bar",
      author: {
        thumbnail_id: null,
        first_name: "Mattias",
        last_name: "Lagergren",
        __entity_type__: "User",
      },
    }}
  />
);

NoteWithTodo.storyName = "Note with todo";

NoteWithTodo.parameters = {
  info: "This example shows a Note with incomplete todo.",
};

export const NoteTodoCompleted = () => (
  <Note
    session={getSession()}
    category
    todo
    completedAt={moment()}
    onTodoChange={action("onTodoChange")}
    data={{
      date: moment(),
      note_components: [],
      content: "**Foo** bar",
      author: {
        thumbnail_id: null,
        first_name: "Mattias",
        last_name: "Lagergren",
        __entity_type__: "User",
      },
    }}
  />
);

NoteTodoCompleted.storyName = "Note todo completed";

NoteTodoCompleted.parameters = {
  info: "This example shows a Note with completed todo by unknown.",
};

export const NoteTodoKnownCompleted = () => (
  <Note
    session={getSession()}
    category
    todo
    completedAt={moment()}
    completedBy={{ first_name: "Lucas", last_name: "Correia" }}
    onTodoChange={action("onTodoChange")}
    data={{
      date: moment(),
      note_components: [],
      content: "**Foo** bar",
      author: {
        thumbnail_id: null,
        first_name: "Mattias",
        last_name: "Lagergren",
        __entity_type__: "User",
      },
    }}
  />
);

NoteTodoKnownCompleted.storyName = "Note todo known completed";

NoteTodoKnownCompleted.parameters = {
  info: "This example shows a Note with completed todo.",
};

export const NoteTodoPending = () => (
  <Note
    session={getSession()}
    category
    todo
    pendingTodo
    completedAt={moment()}
    completedBy={{ first_name: "Lucas", last_name: "Correia" }}
    onTodoChange={action("onTodoChange")}
    data={{
      date: moment(),
      note_components: [],
      content: "**Foo** bar",
      author: {
        thumbnail_id: null,
        first_name: "Mattias",
        last_name: "Lagergren",
        __entity_type__: "User",
      },
    }}
  />
);

NoteTodoPending.storyName = "Note todo pending";

NoteTodoPending.parameters = {
  info: "This example shows a Note with pending todo.",
};

export const NoteWithAttachments = () => (
  <Note
    session={getSession()}
    onAttachmentClick={() => alert("on-attachment-click")}
    category
    data={{
      date: moment(),
      note_components: [
        {
          thumbnail_url: null,
          component: {
            id: "foo-bar",
            name: "File",
            file_type: ".zip",
          },
        },
        {
          thumbnail_url: null,
          component: {
            id: "foo-bar-2",
            name: "File 2",
            file_type: ".rar",
          },
        },
        {
          thumbnail_url: null,
          component: {
            id: "cd6ecac4-0003-41ba-a39c-b2eb5c1b250c",
            name: "Image",
            file_type: ".png",
          },
        },
      ],
      content: "**Foo** bar",
      author: {
        thumbnail_id: null,
        first_name: "Mattias",
        last_name: "Lagergren",
        __entity_type__: "User",
      },
    }}
  />
);

NoteWithAttachments.storyName = "Note with attachments";

NoteWithAttachments.parameters = {
  info: "This example shows a Note with attachments component.",
};

const StatefulEditableNote = withState(
  "collapsed",
  "setCollapsed",
  true
)(({ collapsed, setCollapsed }) => {
  const author = {
    thumbnail_id: null,
    first_name: "Mattias",
    last_name: "Lagergren",
    __entity_type__: "User",
    id: "foo",
  };
  const note = {
    date: moment(),
    note_components: [],
    content: "**Foo** bar",
    author,
  };

  return (
    <EditableNote
      session={getSession()}
      collapsed={collapsed}
      note={note}
      onShowForm={() => setCollapsed(false)}
      onHideForm={() => setCollapsed(true)}
      onRemove={action("remove-note")}
      onSubmitForm={action("submit-form")}
      content={note.content}
      author={author}
    />
  );
});

export const NoteEditable = () => <StatefulEditableNote />;

NoteEditable.storyName = "Note editable";

NoteEditable.parameters = {
  info: "This example shows a editable Note component.",
};

export const Form = () => (
  <NoteForm
    content={`*Foo bar*

Another line...`}
  />
);

Form.parameters = {
  info: "This example shows a Note form.",
};

export const FormWithEntitySelection = () => (
  <NoteForm
    content={`*Foo bar*

Another line...`}
    entity={{ type: "AssetVersion", id: "p" }}
    entitySelector={[
      {
        label: "Team",
        icon: <PeopleIcon />,
        description: "Share only with your team members",
        entity: { type: "AssetVersion", id: "p" },
      },
      {
        label:
          "Compositing approvals and this is a very long title that will not break" +
          " the rendering as it is handled gracefully",
        icon: <OndemandVideoIcon />,
        description: "Share comment in the review session",
        entity: { type: "ReviewSessionObject", id: "f" },
      },
      {
        label: "First feedback",
        icon: <OndemandVideoIcon />,
        description: "Share comment in the review session",
        entity: { type: "ReviewSessionObject", id: "b" },
      },
    ]}
    onSubmit={(form, entity) => action(`Submit ${entity.id}`)}
  />
);

FormWithEntitySelection.storyName = "Form with entity selection";

FormWithEntitySelection.parameters = {
  info: "This example shows a Note form with entity selection.",
};

export const FormAutoFocus = () => (
  <NoteForm
    autoFocus
    content={`*Foo bar*

Another line...`}
  />
);

FormAutoFocus.storyName = "Form auto focus";

FormAutoFocus.parameters = {
  info: "This example shows a Note form with auto focus.",
};

export const FormCollapsed = () => (
  <NoteForm
    collapsed
    content={`*Foo bar*

Another line...`}
  />
);

FormCollapsed.storyName = "Form collapsed";

FormCollapsed.parameters = {
  info: "This example shows a Note form rendered collapsed.",
};

export const FormPending = () => (
  <NoteForm
    pending
    content={`*Foo bar*

Another line...`}
  />
);

FormPending.storyName = "Form pending";

FormPending.parameters = {
  info: "This example shows a Note form rendered pending.",
};

const StatefulReply = withState(
  "collapsed",
  "setCollapsed",
  true
)(({ collapsed, setCollapsed }) => (
  <ReplyForm
    collapsed={collapsed}
    onShowForm={() => setCollapsed(false)}
    onHideForm={() => setCollapsed(true)}
    onSubmitForm={action("submit-form")}
  />
));

export const ReplyFormReveal = () => <StatefulReply />;

ReplyFormReveal.storyName = "Reply form reveal";

ReplyFormReveal.parameters = {
  info: "This example shows a Note reply form reveal button.",
};
