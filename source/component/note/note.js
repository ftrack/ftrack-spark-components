// :copyright: Copyright (c) 2016 ftrack

import PropTypes from "prop-types";

import { Component } from "react";
import moment from "moment";
import FontIcon from "react-toolbox/lib/font_icon";
import withTooltip from "react-toolbox/lib/tooltip";
import {
  defineMessages,
  intlShape,
  FormattedRelative,
  FormattedMessage,
} from "react-intl";
import { compose, withHandlers } from "recompose";

import safeInjectIntl from "../util/hoc/safe_inject_intl";
import {
  durationToTimeParts,
  formatStandard,
  framesToTimeParts,
} from "../util/get_formatted_time";
import style from "./style.scss";

import AttachmentArea from "./attachment_area";
import Skeleton from "../skeleton";
import EntityAvatar from "../entity_avatar";
import Markdown from "../markdown";
import Label from "../label";
import { injectMentions } from "../editor/utils";
import { REVIEW_SESSION_NOTE_CATEGORY } from "../util/constant";

const SpanWithTooltip = withTooltip("span");

const messages = defineMessages({
  page: {
    id: "ftrack-spark-components.note-note.page",
    defaultMessage: "Page {number}",
  },
  frame: {
    id: "ftrack-spark-components.note-note.frame",
    defaultMessage: "Frame {number}",
  },
  "attribute-message-click-to-complete": {
    id: "ftrack-spark-components.note-note.attribute-message-to-complete",
    defaultMessage: "Click to mark as completed.",
  },
  "attribute-message-marked-as-completed-by": {
    id: "ftrack-spark-components.note-note.attribute-message-marked-as-completed-by",
    defaultMessage: "Marked as completed by",
  },
  "attribute-unknown": {
    id: "ftrack-spark-components.note-note.attribute-unknown",
    defaultMessage: "Unknown",
  },
  "attribute-change": {
    id: "ftrack-spark-components.note-note.attribute-change",
    defaultMessage: "(Change)",
  },
  "attribute-message-posted-by-unknown-user": {
    id: "ftrack-spark-components.note-note.attribute-message-posted-by-unknown-user",
    defaultMessage:
      "This note is posted by an unknown user. Most likely this is a user or invitee that has been removed from ftrack.",
  },
  "review-session-object-status-approved": {
    id: "ftrack-spark-components.note-note.review-session-object-status-approved",
    defaultMessage: "Approved",
  },
  "review-session-object-status-require_changes": {
    id: "ftrack-spark-components.note-note.review-session-object-status-require_changes",
    defaultMessage: "Require changes",
  },
});

/** Display author information. */
function Author({ data }) {
  if (
    data?.__entity_type__ === "User" ||
    data?.__entity_type__ === "Collaborator"
  ) {
    return (
      <span
        className={style.authorName}
      >{`${data.first_name} ${data.last_name}`}</span>
    );
  }

  const title = (
    <FormattedMessage
      {...messages["attribute-message-posted-by-unknown-user"]}
    />
  );

  return (
    <span className={style.authorName} title={title}>
      {<FormattedMessage {...messages["attribute-unknown"]} />}
    </span>
  );
}

Author.propTypes = {
  data: PropTypes.object,
  intl: intlShape.isRequired,
};

function Todo_({ completedBy, completedAt, onTodoChange, pendingTodo, intl }) {
  if (pendingTodo) {
    return (
      <div className={style["pending-todo"]}>
        <Skeleton className={style["icon-skeleton"]} />
        <Skeleton className={style["todo-text-skeleton"]} />
        {/* <ProgressBar type="circular" mode="indeterminate" /> */}
      </div>
    );
  }

  const { formatMessage } = intl;
  let content = formatMessage(messages["attribute-message-click-to-complete"]);

  const completed = !!completedAt;

  if (completed) {
    const localDate = moment(completedAt).local(true);
    content = (
      <span>
        {`${formatMessage(
          messages["attribute-message-marked-as-completed-by"]
        )} `}
        {completedBy
          ? `${completedBy.first_name} ${completedBy.last_name}`
          : formatMessage(messages["attribute-unknown"])}{" "}
        {<FormattedRelative value={localDate.toDate()} />}.{" "}
        {
          <button onClick={onTodoChange} className={style["change-todo"]}>
            {formatMessage(messages["attribute-change"])}
          </button>
        }
      </span>
    );
  }

  return (
    <div
      className={`${style.todo} ${completed ? style.completed : ""}`}
      // role="button"
      onClick={completed ? undefined : onTodoChange}
    >
      <div>
        <FontIcon className={style["todo-icon"]} value="check_circle" />
      </div>
      <div className={style["todo-content"]}>{content}</div>
    </div>
  );
}

Todo_.propTypes = {
  completedAt: PropTypes.object,
  completedBy: PropTypes.shape({
    first_name: PropTypes.string,
    last_name: PropTypes.string,
  }),
  onTodoChange: PropTypes.func.isRequired,
  pendingTodo: PropTypes.bool,
  intl: intlShape.isRequired,
};

const Todo = safeInjectIntl(Todo_);

/**
 * Render note frame annotation in the following order:
 *
 * 1. If type is PDF or Image, return page number
 * 2. If number and frameRate, or time, is defined. Format as standard.
 * 3. If only number is set, return frame number
 */
function FrameTagBase({
  onClick,
  type,
  time,
  number,
  frameRate,
  annotationComponent,
}) {
  let tooltip;
  const content = [];

  const frameNumber = Number.isFinite(number) ? (
    <FormattedMessage {...messages.frame} values={{ number: number + 1 }} />
  ) : null;

  if (annotationComponent) {
    content.push(<FontIcon className={style.annotationIcon} value="brush" />);
  }

  if (type === "pdf" || type === "image") {
    content.push(
      <FormattedMessage {...messages.page} values={{ number: number + 1 }} />
    );
  } else if (Number.isFinite(number) && Number.isFinite(frameRate)) {
    content.push(formatStandard(framesToTimeParts(number, frameRate)));
    tooltip = frameNumber;
  } else if (Number.isFinite(time)) {
    content.push(formatStandard(durationToTimeParts(time)));
    tooltip = frameNumber;
  } else if (Number.isFinite(number)) {
    content.push(frameNumber);
    tooltip = frameNumber;
  }

  if (tooltip) {
    return (
      <SpanWithTooltip
        className={style["frame-info"]}
        onClick={onClick}
        tooltip={tooltip}
      >
        {content}
      </SpanWithTooltip>
    );
  }
  return (
    <span className={style["frame-info"]} onClick={onClick}>
      {content}
    </span>
  );
}

FrameTagBase.propTypes = {
  type: PropTypes.oneOf(["video", "image", "pdf"]),
  time: PropTypes.number,
  number: PropTypes.number,
  frameRate: PropTypes.number,
  onClick: PropTypes.func.isRequired,
  annotationComponent: PropTypes.object,
};

const FrameTag = compose(
  withHandlers({
    onClick: (props) => () =>
      props.onClick(props.number, props.annotationComponent),
  }),
  safeInjectIntl
)(FrameTagBase);

/** Top part of a note */
function TopOfNote_({
  data,
  session,
  entityInformationTooltip,
  entityInformation,
  intl,
  onFrameClick,
}) {
  const tags = [];
  const localDate = moment(data.date).local();

  if (data.frame) {
    const annotationComponent = data.note_components.find((noteComponent) =>
      noteComponent.component.metadata.some(
        (meta) =>
          meta.key === "annotationData" && meta.value && meta.value !== "[]"
      )
    );
    if (tags.length > 0) {
      tags.push(<span>, </span>);
    }
    tags.push(
      <FrameTag
        {...data.frame}
        annotationComponent={annotationComponent}
        onClick={onFrameClick}
      />
    );
  }

  const tagsElement = tags.length ? (
    <span className={style.tags}>
      {tags.map((tag, index) => (
        <span key={`tag-${index}`}>{tag}</span>
      ))}
      <span> - </span>
    </span>
  ) : null;

  return (
    <div className={style["top-wrapper"]}>
      {data.author ? (
        <EntityAvatar
          className={style["avatar-column"]}
          session={session}
          entity={data.author}
        />
      ) : null}
      <div className={style.top}>
        <div className={style.author}>
          <Author data={data.author} intl={intl} />{" "}
          {entityInformationTooltip && entityInformation
            ? entityInformation
            : null}
        </div>
        {tagsElement}
        <span className={style.date}>
          <FormattedRelative
            value={localDate.toDate()}
            className={style.datetime}
          />
        </span>
      </div>
    </div>
  );
}

TopOfNote_.propTypes = {
  session: PropTypes.object,
  entityInformation: PropTypes.element,
  entityInformationTooltip: PropTypes.bool,
  data: PropTypes.object,
  onFrameClick: PropTypes.func,
  intl: intlShape.isRequired,
};

const TopOfNote = safeInjectIntl(TopOfNote_);

/** Note component to display note data. */
class Note extends Component {
  constructor() {
    super();

    this.handleAttachmentClick = this.handleAttachmentClick.bind(this);
    this.handleFrameClick = this.handleFrameClick.bind(this);
    this.getAttachmentMeta = this.getAttachmentMeta.bind(this);
  }

  getAttachmentMeta() {
    const { data, entityInformation, entityInformationTooltip, onFrameClick } =
      this.props;
    const attachmentMeta = data.note_components ? (
      <div className={style["note-item"]}>
        <TopOfNote
          data={data}
          entityInformationTooltip={entityInformationTooltip}
          entityInformation={entityInformation}
          onFrameClick={onFrameClick}
        />
        <Markdown source={data.content || ""} />
      </div>
    ) : null;
    return attachmentMeta;
  }

  handleAttachmentClick(index, noteComponents) {
    const attachmentMeta = this.getAttachmentMeta();
    this.props.onAttachmentClick(index, noteComponents, attachmentMeta);
  }

  handleFrameClick(frameNumber, annotationComponent) {
    const { data } = this.props;
    const attachmentMeta = this.getAttachmentMeta();
    this.props.onFrameClick(
      frameNumber,
      annotationComponent,
      attachmentMeta,
      data
    );
  }

  render() {
    const {
      data,
      category,
      session,
      todo,
      onTodoChange,
      completedAt,
      completedBy,
      pendingTodo,
      entityInformation,
      intl,
      entityInformationTooltip,
    } = this.props;

    const labels = [];
    const { formatMessage } = intl;

    const noteCategories = [];
    if (data.note_label_links) {
      noteCategories.push(...data.note_label_links.map(({ label }) => label));
    } else if (data.category) {
      noteCategories.push(data.category);
    }

    if (category && noteCategories.length) {
      labels.push(
        ...noteCategories.map((item) => {
          let color = item.color;
          if (item.id === REVIEW_SESSION_NOTE_CATEGORY) {
            color = "feedback";
          }
          return (
            <Label key={item.id} color={color} className={style["note-label"]}>
              {item.name}
            </Label>
          );
        })
      );
    }
    if (data.inviteeStatus === "require_changes") {
      labels.push(
        <Label key="require_changes" color="negative">
          {formatMessage(
            messages[`review-session-object-status-${data.inviteeStatus}`]
          )}
        </Label>
      );
    } else if (data.inviteeStatus === "approved") {
      labels.push(
        <Label key="approved" color="positive">
          {formatMessage(
            messages[`review-session-object-status-${data.inviteeStatus}`]
          )}
        </Label>
      );
    }

    return (
      <div className={style["note-item"]}>
        <TopOfNote
          data={data}
          entityInformationTooltip={entityInformationTooltip}
          entityInformation={entityInformation}
          onFrameClick={this.handleFrameClick}
        />
        {todo ? (
          <Todo
            onTodoChange={onTodoChange}
            completedAt={completedAt}
            completedBy={completedBy}
            pendingTodo={pendingTodo}
            intl={intl}
          />
        ) : null}
        {labels.length ? <div className={style.tagLabels}>{labels}</div> : null}
        <Markdown
          source={injectMentions(data.content, data.recipients) || ""}
        />
        {!entityInformationTooltip && entityInformation
          ? entityInformation
          : null}
        <AttachmentArea
          session={session}
          onAttachmentClick={this.handleAttachmentClick}
          noteComponents={data.note_components.filter(
            (noteComponent) =>
              !noteComponent.component.metadata.some(
                (meta) => meta.key === "annotationData"
              )
          )}
        />
      </div>
    );
  }
}

Note.propTypes = {
  session: PropTypes.object,
  entityInformation: PropTypes.element,
  entityInformationTooltip: PropTypes.bool,
  data: PropTypes.object.isRequired,
  category: PropTypes.bool,
  onAttachmentClick: PropTypes.func,
  onFrameClick: PropTypes.func,
  onTodoChange: PropTypes.func,
  todo: PropTypes.bool,
  completedBy: PropTypes.object,
  completedAt: PropTypes.object,
  pendingTodo: PropTypes.bool,
  intl: intlShape.isRequired,
};

Note.defaultProps = {
  todo: false,
  completedBy: null,
  completedAt: null,
  category: false,
};

export default safeInjectIntl(Note);
