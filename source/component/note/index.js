// :copyright: Copyright (c) 2016 ftrack

import AttachmentArea from "./attachment_area";
import EditableNote from "./editable";
import NoteForm from "./form";
import ReplyForm from "./reply";
import Note from "./note";
import NoteCompact from "./note_compact";
import NoteLabelSelector from "./note_label_selector";

// TODO: Feel free to fix this eslint error if you got time
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  AttachmentArea,
  EditableNote,
  NoteForm,
  ReplyForm,
  Note,
  NoteCompact,
  NoteLabelSelector,
};
