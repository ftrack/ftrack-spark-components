// :copyright: Copyright (c) 2016 ftrack

import PropTypes from "prop-types";

import { Button } from "react-toolbox";
import { defineMessages, intlShape } from "react-intl";

import safeInjectIntl from "../util/hoc/safe_inject_intl";
import style from "./style.scss";
import NoteForm from "./form";
import replyTheme from "./reply_theme.scss";

/** Display a reply button or a note form if *collapsed* is false.
 *
 * The *pending* and *content* props are passed to the note form.
 *
 */

const messages = defineMessages({
  "attribute-label-reply": {
    id: "ftrack-spark-components.note-reply.attribute-label-reply",
    defaultMessage: "Reply",
  },
});

function ReplyForm({
  content,
  recipients,
  pending,
  collapsed,
  onSubmitForm,
  onHideForm,
  onShowForm,
  intl,
  canMention,
}) {
  const { formatMessage } = intl;
  if (!collapsed) {
    return (
      <NoteForm
        className={style["reply-form"]}
        content={content}
        recipients={recipients}
        pending={pending}
        onClickOutside={onHideForm}
        onSubmit={onSubmitForm}
        canMention={canMention}
        autoFocus
      />
    );
  }

  return (
    <div className={style["reply-button-wrapper"]}>
      <Button
        className={style["reply-button"]}
        theme={replyTheme}
        label={formatMessage(messages["attribute-label-reply"])}
        mini
        onClick={onShowForm}
      />
    </div>
  );
}

ReplyForm.propTypes = {
  collapsed: PropTypes.bool,
  content: PropTypes.string,
  recipients: PropTypes.arrayOf(
    PropTypes.shape({
      resource_id: PropTypes.string,
      text_mentioned: PropTypes.string,
    })
  ),
  pending: PropTypes.bool,
  onShowForm: PropTypes.func,
  onHideForm: PropTypes.func,
  onSubmitForm: PropTypes.func,
  intl: intlShape.isRequired,
  canMention: PropTypes.bool,
};

export default safeInjectIntl(ReplyForm);
