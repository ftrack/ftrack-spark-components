// :copyright: Copyright (c) 2016 ftrack
/* eslint-disable react/jsx-no-bind */

import PropTypes from "prop-types";

import { Component } from "react";
import classNames from "classnames";
import Dialog from "react-toolbox/lib/dialog";
import { defineMessages, intlShape } from "react-intl";
import { IconButton, ListItemIcon, MenuItem } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import EditIcon from "@mui/icons-material/Edit";

import ButtonMenu from "../button_menu";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import Note from "./note";
import NoteForm from "./form";
import style from "./style.scss";

const messages = defineMessages({
  "attribute-label-cancel": {
    id: "ftrack-spark-components.note-editable.label-cancel",
    defaultMessage: "Cancel",
  },
  "attribute-label-delete": {
    id: "ftrack-spark-components.note-editable.label-delete",
    defaultMessage: "Delete",
  },
  "attribute-label-edit": {
    id: "ftrack-spark-components.note-editable.label-edit",
    defaultMessage: "Edit",
  },
  "attribute-message-confirm-delete": {
    id: "ftrack-spark-components.note-editable.message-confirm-delete",
    defaultMessage: "Are you sure you want to delete the comment?",
  },
});

/** Editable note component that displays either a note or a note form.
 *
 * Options to edit or delete are displayed if the *note* author is matching the
 * *author*.
 *
 * Display the note form if *collpased* is false. The *pending* and *content*
 * props are passed ot the note form.
 *
 */
class EditableNote extends Component {
  constructor() {
    super();

    this.state = {
      confirmDelete: false,
    };
  }

  /** Handle dialog interaction and remove note if *remove* is true. */
  handleDialog(remove) {
    if (remove === true) {
      this.props.onRemove();
    }

    this.setState({ confirmDelete: false });
  }

  render() {
    const {
      note,
      collapsed,
      pending,
      content,
      recipients,
      author,
      onShowForm,
      onHideForm,
      onSubmitForm,
      session,
      intl,
      colors,
      className,
      canMention,
      showCompletableNotes,
      showNoteLabelSelector,
      ...noteProps
    } = this.props;

    const classes = classNames(style["editable-note-container"], className);

    const { formatMessage } = intl;

    /* eslint-disable react/jsx-no-bind */
    const actions = [
      {
        label: formatMessage(messages["attribute-label-cancel"]),
        onClick: this.handleDialog.bind(this, false),
      },
      {
        label: formatMessage(messages["attribute-label-delete"]),
        onClick: this.handleDialog.bind(this, true),
        className: style["destructive-action"],
      },
    ];

    const isTodo = note.is_todo;
    const noteLabels = (note.note_label_links || []).map(
      (labelLink) => labelLink.label
    );

    /* eslint-enable react/jsx-no-bind */
    if (!collapsed) {
      return (
        <NoteForm
          key={`edit-note-form-${note.id}`}
          session={session}
          pending={pending}
          content={content}
          recipients={recipients}
          isTodo={isTodo}
          labels={noteLabels}
          onClickOutside={onHideForm}
          onSubmit={onSubmitForm}
          canMention={canMention}
          showCompletableNotes={!note.completed_at && showCompletableNotes}
          showNoteLabelSelector={showNoteLabelSelector}
          autoFocus
          edit
        />
      );
    }

    let menu = false;

    if (author && note.author && author.id === note.author.id) {
      menu = (
        <ButtonMenu
          button={
            <IconButton size="small">
              <MoreVertIcon />
            </IconButton>
          }
        >
          <MenuItem onClick={onShowForm}>
            <ListItemIcon>
              <EditIcon />
            </ListItemIcon>
            {formatMessage(messages["attribute-label-edit"])}
          </MenuItem>

          <MenuItem onClick={() => this.setState({ confirmDelete: true })}>
            <ListItemIcon>
              <DeleteIcon />
            </ListItemIcon>
            {formatMessage(messages["attribute-label-delete"])}
          </MenuItem>
        </ButtonMenu>
      );
    }

    let confirmationDialog = false;
    if (this.state.confirmDelete) {
      confirmationDialog = (
        <Dialog
          type="small"
          title={formatMessage(messages["attribute-message-confirm-delete"])}
          actions={actions}
          onEscKeyDown={() => this.handleDialog(false)}
          active
        />
      );
    }

    return (
      <div className={classes}>
        {confirmationDialog}
        <Note
          session={session}
          data={note}
          key={note.id}
          category
          todo={note.is_todo}
          completedBy={note.completed_by}
          completedAt={note.completed_at}
          colors={colors}
          {...noteProps}
        />
        <div className={style["icon-menu"]}>{menu}</div>
      </div>
    );
  }
}

EditableNote.propTypes = {
  className: PropTypes.string,
  note: PropTypes.object,
  entityInformation: PropTypes.element,
  session: PropTypes.object,
  collapsed: PropTypes.bool,
  content: PropTypes.string,
  recipients: PropTypes.arrayOf(
    PropTypes.shape({
      resource_id: PropTypes.string,
      text_mentioned: PropTypes.string,
    })
  ),
  pending: PropTypes.bool,
  onShowForm: PropTypes.func,
  onHideForm: PropTypes.func,
  onSubmitForm: PropTypes.func,
  onRemove: PropTypes.func,
  author: PropTypes.object,
  colors: PropTypes.arrayOf(PropTypes.string),
  intl: intlShape.isRequired,
  canMention: PropTypes.bool,
  showNoteLabelSelector: PropTypes.bool,
  showCompletableNotes: PropTypes.bool,
};

export default safeInjectIntl(EditableNote);
