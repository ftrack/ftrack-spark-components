// :copyright: Copyright (c) 2016 ftrack

import { Component } from "react";
import moment from "moment";
import FontIcon from "react-toolbox/lib/font_icon";
import withTooltip from "react-toolbox/lib/tooltip";
import PropTypes from "prop-types";
import { intlShape, defineMessages, FormattedDate } from "react-intl";

import style from "./note_compact.scss";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import Skeleton from "../skeleton";

const SpanWithTooltip = withTooltip("span");

const messages = defineMessages({
  "attribute-unknown": {
    id: "ftrack-spark-components.compact-note.attribute-unknown",
    defaultMessage: "Unknown",
  },
  "todo-tooltip-done": {
    id: "ftrack-spark-components.compact-note.todo-tooltip-done",
    defaultMessage: "Click to unmark todo as completed.",
  },
  "todo-tooltip-not-done": {
    id: "ftrack-spark-components.compact-note.todo-tooltip-not-done",
    defaultMessage: "Click to mark todo as completed.",
  },
});

function Bottom({ data, date, formatMessage }) {
  if (
    data?.__entity_type__ === "User" ||
    data?.__entity_type__ === "Collaborator"
  ) {
    return (
      <div className={style["note-compact-bottom"]}>
        <span>
          {data.first_name} {data.last_name}
        </span>{" "}
        -{" "}
        <span>
          <FormattedDate value={date} />
        </span>
      </div>
    );
  }

  return (
    <div className={style["note-compact-bottom"]}>
      {formatMessage(messages["attribute-unknown"])}
    </div>
  );
}

Bottom.propTypes = {
  data: PropTypes.object,
  date: PropTypes.string,
  formatMessage: PropTypes.func.isRequired,
};

class Todo extends Component {
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.completedAt && prevState.pendingTodo) {
      return { pendingTodo: false };
    }
    return null;
  }

  constructor() {
    super();
    this.state = { pendingTodo: false };
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.props.onTodoChange();
    this.setState({ pendingTodo: true });
  }

  render() {
    const { completedAt, formatMessage } = this.props;
    if (this.state.pendingTodo) {
      return (
        <div className={style["pending-todo"]}>
          <Skeleton className={style["icon-skeleton"]} />
        </div>
      );
    }

    const completed = !!completedAt;

    return (
      <div className={`${style.todo}`} onClick={this.onClick}>
        <SpanWithTooltip
          tooltip={formatMessage(
            messages[
              completed === true ? "todo-tooltip-done" : "todo-tooltip-not-done"
            ]
          )}
        >
          <FontIcon
            className={style["todo-icon"]}
            value={
              completed === true ? "check_circle" : "radio_button_unchecked"
            }
          />
        </SpanWithTooltip>
      </div>
    );
  }
}

Todo.propTypes = {
  completedAt: PropTypes.object,
  onTodoChange: PropTypes.func.isRequired,
  formatMessage: PropTypes.func.isRequired,
};

/** Note component to display note data. */
function NoteCompact({ data, onTodoChange, completedAt, intl }) {
  const localDate = moment(data.date).format("MM/DD/YYYY");
  const { formatMessage } = intl;
  return (
    <div className={style["note-compact-row"]}>
      <div className={style["note-compact-row-inner"]}>
        <div className={style["note-compact-text"]}>{data.content}</div>
        <Bottom
          data={data.author}
          date={localDate}
          formatMessage={formatMessage}
        />
      </div>
      {data.is_todo ? (
        <Todo
          formatMessage={formatMessage}
          onTodoChange={onTodoChange}
          completedAt={completedAt}
        />
      ) : null}
    </div>
  );
}

NoteCompact.propTypes = {
  data: PropTypes.object.isRequired,
  onTodoChange: PropTypes.func,
  completedAt: PropTypes.object,
  intl: intlShape.isRequired,
};

NoteCompact.defaultProps = {
  completedAt: null,
};

export default safeInjectIntl(NoteCompact);
