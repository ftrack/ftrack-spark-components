// :copyright: Copyright (c) 2018 ftrack

import {
  Button,
  ListItemIcon,
  ListItemText,
  MenuItem,
  Typography,
} from "@mui/material";
import PropTypes from "prop-types";

import ButtonMenu from "../button_menu";
function EntitySelector({ data, selected, onSelect }) {
  const handleSelect = (value) => {
    const item = data.find((option) => value === option.entity.id);
    onSelect(item.entity);
  };

  const active = data.find((option) => selected.id === option.entity.id);

  return (
    <ButtonMenu
      button={
        <Button
          startIcon={active.icon}
          sx={{
            textTransform: "none",
            maxWidth: "20rem",
            overflow: "hidden",
            textOverflow: "ellipsis",
            color: "text.primary",

            ".MuiButton-startIcon": {
              color: "primary.main",
            },
          }}
        >
          <Typography variant="inherit" noWrap fontWeight="fontWeightMedium">
            {active.label}
          </Typography>
        </Button>
      }
      sx={{
        maxWidth: "40rem",
      }}
    >
      {data.map((item) => (
        <MenuItem
          key={`menu-item-${item.entity.id}`}
          onClick={() => handleSelect(item.entity.id)}
          sx={{
            ".MuiListItemIcon-root": {
              color: "primary.main",
            },
          }}
        >
          {item.icon && <ListItemIcon>{item.icon}</ListItemIcon>}

          <ListItemText
            primary={
              <Typography variant="inherit" noWrap>
                {item.label}
              </Typography>
            }
            secondary={
              <Typography variant="inherit" noWrap>
                {item.description}
              </Typography>
            }
          />
        </MenuItem>
      ))}
    </ButtonMenu>
  );
}

EntitySelector.propTypes = {
  data: PropTypes.array.isRequired,
  selected: PropTypes.object.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default EntitySelector;
