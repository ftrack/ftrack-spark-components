..
    :copyright: Copyright (c) 2018 ftrack

##########
Empty text
##########

The empty text component is used to render an empty text with optional icon,
heading and child element(s). The child elements would typically be a call to
action button or something else.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
className                      CSS Class to add to the root element.
icon                           Icon that is displayed at the top of the component.
label                          Optional label of the empty text.
header                         Optional header of the empty text.
children                       Child elements of the empty text, rendered at the bottom.
cover               true       Boolean to indicate if the empty text should cover the sibling components.
