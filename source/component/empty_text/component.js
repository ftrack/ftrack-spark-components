// :copyright: Copyright (c) 2018 ftrack

import PropTypes from "prop-types";
import Heading from "../heading";
import style from "./style.scss";

function EmptyText({ label, header, icon, children, className, cover }) {
  const classNames = [style.emptyText];
  if (cover) {
    classNames.push(style.cover);
  }
  if (className) {
    classNames.push(className);
  }

  let iconElement = null;
  if (icon) {
    if (typeof icon === "string") {
      iconElement = (
        <img className={style.icon} alt="" role="presentation" src={icon} />
      );
    } else {
      iconElement = <div className={style["font-icon"]}>{icon}</div>;
    }
  }

  return (
    <div className={classNames.join(" ")}>
      <div className={style.inner}>
        {iconElement}
        {header ? (
          <Heading variant="title" className={style.header} color="primary">
            {header}
          </Heading>
        ) : null}
        {label ? <h5 className={style.label}>{label}</h5> : null}
        {children}
      </div>
    </div>
  );
}

EmptyText.propTypes = {
  icon: PropTypes.node,
  label: PropTypes.node,
  header: PropTypes.node,
  children: PropTypes.node,
  cover: PropTypes.bool,
  className: PropTypes.string,
};

EmptyText.defaultProps = {
  cover: true,
};

export default EmptyText;
