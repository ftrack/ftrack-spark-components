import centered from "@storybook/addon-centered/react";
import FontIcon from "react-toolbox/lib/font_icon";
import Button from "react-toolbox/lib/button";

import EmptyText from ".";

export default {
  title: "Empty text",
  decorators: [centered],
};

function ParentElement({ children }) {
  return (
    <div
      style={{
        border: "1px solid hotpink",
        minHeight: "250px",
        width: "500px",
      }}
    >
      {children}
    </div>
  );
}

function AnotherComponent() {
  return (
    <div
      style={{
        height: "240px",
        backgroundColor: "lightgray",
        lineHeight: "240px",
        textAlign: "center",
        color: "black",
      }}
    >
      Another component that can be more or less complex.
    </div>
  );
}

export const Header = () => <EmptyText header="Empty text header" />;
export const Label = () => <EmptyText label="Empty text label" />;

export const ChildElements = () => (
  <EmptyText label="Empty text label">
    <Button
      label="Call to action"
      primary
      raised
      style={{ marginTop: "10px" }}
    />
  </EmptyText>
);

ChildElements.storyName = "Child elements";

export const Icon = () => (
  <EmptyText icon={<FontIcon value="message" />}>
    <p>Empty text can have an icon.</p>
  </EmptyText>
);

export const FullExample = () => (
  <ParentElement>
    <EmptyText icon={<FontIcon value="message" />} header="Empty text header">
      <Button
        label="Call to action"
        primary
        raised
        style={{ marginTop: "10px" }}
      />
    </EmptyText>
  </ParentElement>
);

FullExample.storyName = "Full example";

export const CoverSiblingComponent = () => (
  <ParentElement>
    <AnotherComponent />
    <EmptyText header="Empty text component" />
  </ParentElement>
);

CoverSiblingComponent.storyName = "Cover sibling component";

export const WithoutCover = () => (
  <ParentElement>
    <AnotherComponent />
    <EmptyText cover={false} header="Empty text component" />
  </ParentElement>
);

WithoutCover.storyName = "Without cover";
