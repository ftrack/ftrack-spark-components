..
    :copyright: Copyright (c) 2018 ftrack

##############
File Drop Container
##############

The File Drop Container is used to make it possible to drop files on a container with children such as card views.

Base File Drop Container
-------------------

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
className                      CSS Class to add to the root element.
dropzoneProps                  Additional props to pass to the dropzone component.
onChange                       Invoked with *value* when upload completes.
onUploadError                  Invoked when uploading fails.
session                        ftrack JS API session instance.
accept                         MIME type of acceptable files.
showFileBrowser                boolean to trigger the file browser to open
onShowFileBrowser              callback function called when file browser opening is triggered through props
rejectClassName                CSS class to indicate that certain files will not be allowed to upload
activeClassName                CSS class to indicate that certain files will be allowed to upload
onUploadStarted                Callback function that will take the number of files that is being uploaded as input

