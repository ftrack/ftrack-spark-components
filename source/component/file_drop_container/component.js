// :copyright: Copyright (c) 2018 ftrack

import PropTypes from "prop-types";

import { Component } from "react";
import Dropzone from "react-dropzone";
import log from "loglevel";
import { v4 as uuidV4 } from "uuid";
import classNames from "classnames";

import style from "./style.scss";

class FileDropContainer extends Component {
  constructor(props) {
    super(props);
    this.handleDropAccepted = this.handleDropAccepted.bind(this);
    this.handleDropRejected = this.handleDropRejected.bind(this);
    this.droppableAreaRef = null;
  }

  componentDidUpdate(prevProps) {
    const { showFileBrowser, onShowFileBrowser } = this.props;

    if (
      showFileBrowser !== prevProps.showFileBrowser &&
      showFileBrowser &&
      this.droppableAreaRef
    ) {
      this.droppableAreaRef.open();

      if (onShowFileBrowser) {
        onShowFileBrowser();
      }
    }
  }

  handleDropAccepted(files) {
    log.debug("Dropped files", files);
    if (files.length) {
      this.uploadFile(files);
    }
  }

  handleDropRejected(rejectedFiles) {
    const { maxSize, onFileSizeTooBig } = this.props;
    log.debug("Files were rejected", rejectedFiles);
    if (rejectedFiles.some((file) => file.size > maxSize)) {
      onFileSizeTooBig();
    }
  }

  uploadFile(files) {
    const { onUploadStarted, onAborted, onChange, onUploadError } = this.props;

    const uploadItems = files.map((file) => ({
      id: uuidV4(),
      xhr: new XMLHttpRequest(),
      file,
    }));

    if (onUploadStarted) {
      const shouldContinue = onUploadStarted(uploadItems);
      if (shouldContinue === false) {
        return;
      }
    }

    const promises = files.map((file, index) => {
      const xhr = uploadItems[index].xhr;
      return this.props.session.createComponent(file, {
        onAborted,
        xhr,
        onProgress: (progress) =>
          this.props.onProgress &&
          this.props.onProgress(progress, uploadItems[index].id),
        data: { id: uploadItems[index].id },
      });
    });

    promises.forEach((p) => {
      p.then((res) => {
        onChange(res[0].data.id);
      }).catch((err) => {
        onUploadError(err);
      });
    });
  }

  render() {
    const {
      disabled,
      multiple,
      maxSize,
      accept,
      dropzoneProps,
      rejectClassName,
      activeClassName,
      className,
    } = this.props;

    return (
      <Dropzone
        multiple={multiple}
        maxSize={maxSize}
        accept={accept}
        onDropAccepted={this.handleDropAccepted}
        {...dropzoneProps}
        disabled={disabled}
        noKeyboard
        noClick
        ref={(node) => {
          this.droppableAreaRef = node;
        }}
      >
        {({ getRootProps, getInputProps, isDragAccept, isDragReject }) => {
          const classes = classNames(
            style.container,
            {
              [activeClassName]: isDragAccept,
              [rejectClassName]: isDragReject,
            },
            className
          );

          return (
            <div className={classes} {...getRootProps()}>
              <input {...getInputProps()} />
              {this.props.children}
            </div>
          );
        }}
      </Dropzone>
    );
  }
}

FileDropContainer.propTypes = {
  accept: PropTypes.string,
  dropzoneProps: PropTypes.object, // eslint-disable-line
  onChange: PropTypes.func,
  onAborted: PropTypes.func,
  onProgress: PropTypes.func,
  onUploadError: PropTypes.func,
  session: PropTypes.shape({
    createComponent: PropTypes.func.isRequired,
  }).isRequired,
  multiple: PropTypes.bool,
  maxSize: PropTypes.number,
  disabled: PropTypes.bool,
  showFileBrowser: PropTypes.bool,
  onShowFileBrowser: PropTypes.func,
  onUploadStarted: PropTypes.func,
  onFileSizeTooBig: PropTypes.func,
  children: PropTypes.node,
  rejectClassName: PropTypes.string,
  activeClassName: PropTypes.string,
  className: PropTypes.string,
};

FileDropContainer.defaultProps = {
  value: null,
  onAborted: () => {},
  onFileSizeTooBig: () => {},
  dropzoneProps: {},
  multiple: false,
  maxSize: 1e9, // 1000 MB
  accept: null,
  disabled: false,
  showFileBrowser: false,
};

export default FileDropContainer;
