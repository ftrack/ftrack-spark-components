// :copyright: Copyright (c) 2019 ftrack
import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import theme from "./lazy_image.scss";

function LazyImage({ src, className, style = {}, ...props }) {
  const [isLoaded, setLoaded] = useState(false);
  useEffect(() => {
    setLoaded(false);
    if (!src) {
      return;
    }
    const image = new Image(); // eslint-disable-line no-undef
    const onLoaded = () => {
      setLoaded(true);
    };
    image.onload = onLoaded;
    image.onerror = onLoaded;
    image.src = src;
  }, [src]);

  const classes = classNames(
    theme.root,
    { [theme.loaded]: isLoaded },
    className
  );

  const imageStyle = isLoaded
    ? { ...style, backgroundImage: `url(${src})` }
    : style;

  return <div className={classes} style={imageStyle} {...props} />;
}

LazyImage.propTypes = {
  className: PropTypes.string,
  src: PropTypes.string,
  // eslint-disable-next-line react/forbid-prop-types
  style: PropTypes.object,
};

export default LazyImage;
