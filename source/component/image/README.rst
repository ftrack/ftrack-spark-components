..
    :copyright: Copyright (c) 2018 ftrack

#########
LazyImage
#########

The LazyImage component can be used to display an image which fades in when
the image has finished loading to reduce flickering.

Other props are spread to the root span element.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
src                            The image source

#########
Thumbnail
#########

The Thumbnail component can be used to display an thumbnail image which fades in when
the image has finished loading to reduce flickering.

Other props are spread to the root span element.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
src                            The image source
size               medium      The size of the image
position           relative    The CSS positioning
rounded            false       If the image should have rounded corners.
