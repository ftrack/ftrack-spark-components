// :copyright: Copyright (c) 2019 ftrack
import PropTypes from "prop-types";
import classNames from "classnames";
import LazyImage from "./lazy_image";
import FileTypeIcon from "../file_type_icon";
import style from "./thumbnail.scss";

function Thumbnail({
  src,
  size = "small",
  position = "relative",
  rounded,
  className,
  children,
  ...props
}) {
  const classes = classNames(
    style.root,
    {
      [style.positionRelative]: position === "relative",
      [style.positionAbsolute]: position === "absolute",
      [style.rounded]: rounded,
    },
    style[size],
    className
  );
  const image = src ? <LazyImage src={src} /> : <FileTypeIcon variant={size} />;

  return (
    <div className={classes} {...props}>
      {image}
      {children}
    </div>
  );
}

Thumbnail.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  rounded: PropTypes.bool,
  src: PropTypes.string,
  size: PropTypes.oneOf(["tiny", "small", "extra-tiny"]),
  position: PropTypes.oneOf(["relative", "absolute"]),
};

export default Thumbnail;
