// :copyright: Copyright (c) 2019 ftrack
import { useState } from "react";
import centered from "@storybook/addon-centered/react";
import {
  ThumbnailOverlay,
  OverlayCardTitle,
  OverlayCardSubtitle,
} from "../overlay_card";
import { LazyImage, Thumbnail } from ".";
import FontIcon from "react-toolbox/lib/font_icon";

export default {
  title: "Image",
  decorators: [centered],
};

const sources = [
  "https://placeimg.com/640/480/animals",
  "https://placeimg.com/640/480/arch",
  "https://placeimg.com/640/480/nature",
  "https://placeimg.com/640/480/people",
  "https://placeimg.com/640/480/tech",
];

function LazyImageTest() {
  const [src, setSrc] = useState();
  const setRandomImage = () => {
    const randomIndex = Math.floor(Math.random() * sources.length);
    setSrc(`${sources[randomIndex]}?d=${+new Date()}`);
  };

  return (
    <section>
      <button onClick={setRandomImage}>Randomize image</button>
      <br />
      <input
        style={{ width: "100%" }}
        value={src}
        onChange={(e) => {
          setSrc(e.target.value);
        }}
      />
      <hr />
      <div
        style={{
          width: "400px",
          height: "400px",
          position: "relative",
          border: "1px solid #777",
        }}
      >
        <LazyImage src={src} />
      </div>
    </section>
  );
}

export const _LazyImage = () => <LazyImageTest />;

_LazyImage.storyName = "Lazy image";

export const _Thumbnail = () => <Thumbnail src={sources[0]} />;
export const ThumbnailRounded = () => <Thumbnail rounded src={sources[1]} />;

ThumbnailRounded.storyName = "Thumbnail (rounded)";

export const ThumbnailWithOverlay = () => (
  <Thumbnail rounded src={sources[2]}>
    <ThumbnailOverlay justify="between" visibility="visible">
      <FontIcon value="info_outline" />
      <OverlayCardTitle>Another variant</OverlayCardTitle>
    </ThumbnailOverlay>
  </Thumbnail>
);

ThumbnailWithOverlay.storyName = "Thumbnail with overlay";

export const ThumbnailWithOverlayOnHover = () => (
  <Thumbnail rounded src={sources[2]}>
    <ThumbnailOverlay justify="end" visibility="hover">
      <OverlayCardTitle>Title</OverlayCardTitle>
      <OverlayCardSubtitle>Subtitle</OverlayCardSubtitle>
    </ThumbnailOverlay>
  </Thumbnail>
);

ThumbnailWithOverlayOnHover.storyName = "Thumbnail with overlay on hover";
