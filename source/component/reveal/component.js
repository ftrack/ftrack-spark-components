// :copyright: Copyright (c) 2016 ftrack
/* eslint-disable jsx-a11y/anchor-has-content */

import PropTypes from "prop-types";

import { Component } from "react";

import style from "./style.scss";

/**
 * Reveal component.
 *
 * Shows a clickable link until active, then children.
 */
class Reveal extends Component {
  constructor(props) {
    super(props);
    this.state = { active: props.active };
    this._onButtonClicked = this._onButtonClicked.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.active !== this.props.active) {
      this.setState({ active: nextProps.active });
    }
  }

  _onButtonClicked() {
    this.setState({ active: true });
  }

  render() {
    let content = null;
    if (this.state.active) {
      content = this.props.children;
    } else if (this.props.label) {
      content = (
        <button className={style.link} onClick={this._onButtonClicked}>
          {this.props.label}
        </button>
      );
    }
    return <div className={this.props.className}>{content}</div>;
  }
}

Reveal.propTypes = {
  children: PropTypes.node.isRequired,
  active: PropTypes.bool,
  className: PropTypes.string,
  label: PropTypes.node,
  icon: PropTypes.string,
};

Reveal.defaultProps = {
  className: "",
  active: false,
  label: null,
  icon: null,
};

export default Reveal;
