..
    :copyright: Copyright (c) 2016 ftrack

######
Reveal
######

The reveal component is a simple component which hides its children and displays
a *Link* component. Once the link is clicked, the children are revealed.
