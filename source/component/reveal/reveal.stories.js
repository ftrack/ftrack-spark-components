import centered from "@storybook/addon-centered/react";
import Reveal from ".";

export default {
  title: "Reveal",
  decorators: [centered],
};

export const _Reveal = () => (
  <Reveal label="Test reveal (click to show content)">
    <h2>Revealed content</h2>
    <p>Some more content here</p>
  </Reveal>
);

_Reveal.parameters = {
  info: `This example shows a Reveal component. It displays a clickable label
which once clicked will reveal the contents.`,
};
