// :copyright: Copyright (c) 2019 ftrack

import centered from "@storybook/addon-centered/react";
import { action } from "@storybook/addon-actions";

import ProcessingButton from ".";

export default {
  title: "Processing button",
  decorators: [centered],
};

export const ProcessingButtonDefault = () => (
  <ProcessingButton onClick={action("Verify")} primary label={"Not loading"} />
);

ProcessingButtonDefault.storyName = "Processing button default";

export const ProcessingButtonLoading = () => (
  <ProcessingButton primary processing="true" label={"Loading..."} />
);

ProcessingButtonLoading.storyName = "Processing button loading";
