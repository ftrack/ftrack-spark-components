// :copyright: Copyright (c) 2019 ftrack

import PropTypes from "prop-types";
import Button from "react-toolbox/lib/button";
import ProgressBar from "react-toolbox/lib/progress_bar";

import style from "./style.scss";

function ProcessingButton({ processing, children, ...props }) {
  return (
    <Button {...props}>
      {processing ? (
        <ProgressBar
          className={style.spinner}
          mode="indeterminate"
          type="circular"
        />
      ) : null}
      {children}
    </Button>
  );
}

ProcessingButton.propTypes = {
  processing: PropTypes.bool,
  children: PropTypes.node,
};

export default ProcessingButton;
