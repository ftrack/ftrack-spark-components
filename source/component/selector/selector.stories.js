/* eslint-disable react/no-multi-comp */
/* eslint-disable react/prop-types */

import PropTypes from "prop-types";

import { Component } from "react";
// eslint-disable-next-line
import centered from "@storybook/addon-centered/react";
import Button from "react-toolbox/lib/button";
import sample from "lodash/sample";
// eslint-disable-next-line
import ResourceSelector from "./resource_selector";
import StatusSelector from "./status_selector";
import ProjectSelector from "./project_selector";
import CollaboratorSelector from "./collaborator_selector";
import RemoteSelector from "./remote_selector";
import getSession from "../../story/get_session";

export default {
  title: "Selector",
  decorators: [centered],
};

const session = getSession();

function addSelectorState(SelectorComponent) {
  return class SelectorState extends Component {
    constructor(props) {
      super(props);
      this.state = { value: props.value || null };
      this.setValue = this.setValue.bind(this);
      this.setExtraValue = this.setExtraValue.bind(this);
    }

    setValue(value) {
      this.setState({ value });
    }

    setExtraValue() {
      const value = [...this.state.value, this.props.extraValue];
      this.setState({ value });
    }

    render() {
      const additionalProps = {
        value: this.state.value,
        onChange: this.setValue,
        session,
      };

      return (
        <div>
          {this.props.extraValue ? (
            <button onClick={this.setExtraValue}>Add extra value</button>
          ) : null}
          <SelectorComponent {...this.props} {...additionalProps} />
        </div>
      );
    }
  };
}

const StatefulResourceSelector = addSelectorState(ResourceSelector);
const StatefulRemoteSelector = addSelectorState(RemoteSelector);
const StatefulStatusSelector = addSelectorState(StatusSelector);
const StatefulProjectSelector = addSelectorState(ProjectSelector);

const wrapperStyle = {
  width: "500px",
  padding: "20px",
  border: "1px solid gray",
};

export const _ResourceSelector = () => (
  <div style={wrapperStyle}>
    <StatefulResourceSelector multi users groups />
  </div>
);

export const UserSelectorSingle = () => (
  <div style={wrapperStyle}>
    <StatefulResourceSelector users />
  </div>
);

UserSelectorSingle.storyName = "User selector (single)";

export const UserSelectorMulti = () => (
  <div style={wrapperStyle}>
    <StatefulResourceSelector multi users />
  </div>
);

UserSelectorMulti.storyName = "User selector (multi)";

export const UserSelectorCustomFilter = () => (
  <div style={wrapperStyle}>
    <StatefulResourceSelector
      multi
      users
      userFilters={'first_name like "E%"'}
    />
  </div>
);

UserSelectorCustomFilter.storyName = "User selector (custom filter)";

export const GroupSelectorSingle = () => (
  <div style={wrapperStyle}>
    <StatefulResourceSelector groups />
  </div>
);

GroupSelectorSingle.storyName = "Group selector (single)";

export const GroupSelectorMulti = () => (
  <div style={wrapperStyle}>
    <StatefulResourceSelector multi groups />
  </div>
);

GroupSelectorMulti.storyName = "Group selector (multi)";

export const ResourceSelectorCustomLabels = () => (
  <div style={wrapperStyle}>
    <StatefulResourceSelector
      multi
      users
      groups
      moreResultsLabel="~~More results available, type to narrow down result~~"
      groupLabel="~~Group~~"
      userLabel="~~User~~"
      userInactiveLabel="~~User (Inactive)~~"
      placeholder="~~Select...~~"
      loadingPlaceholder="~~Loading~~"
      searchPromptText="~~Search...~~"
      clearValueText="~~Clear value~~"
      clearAllText="~~Clear all~~"
    />
  </div>
);

ResourceSelectorCustomLabels.storyName = "Resource Selector (Custom labels)";

export const ResourceSelectorWExternalValue = () => (
  <div style={wrapperStyle}>
    <StatefulResourceSelector
      users
      value="3a002420-fec0-11e3-b88d-04011030cf01"
    />
  </div>
);

ResourceSelectorWExternalValue.storyName =
  "Resource selector w/ external value";

export const ResourceSelectorWManyValues = () => (
  <div style={wrapperStyle}>
    <StatefulResourceSelector
      multi
      users
      extraValue="3a002420-fec0-11e3-b88d-04011030cf01"
      value={[
        "92119880-8159-11e9-8c2f-8c8590a671fa",
        "9219ac45-8159-11e9-90e4-8c8590a671fa",
        "96249cca-8158-11e9-97d9-8c8590a671fa",
        "9638788a-8158-11e9-a3ba-8c8590a671fa",
        "9955f3d1-8159-11e9-abb7-8c8590a671fa",
        "9964163a-8159-11e9-a6b2-8c8590a671fa",
        "9d45eda8-7701-11e9-862f-8c8590a671fa",
      ]}
    />
  </div>
);

ResourceSelectorWManyValues.storyName = "Resource selector w/ many values";

export const ScopeSelectorSingle = () => (
  <div style={wrapperStyle}>
    <StatefulRemoteSelector entityType={"Scope"} />
  </div>
);

ScopeSelectorSingle.storyName = "Scope selector (single)";

export const ScopeSelectorMulti = () => (
  <div style={wrapperStyle}>
    <StatefulRemoteSelector entityType={"Scope"} multi />
  </div>
);

ScopeSelectorMulti.storyName = "Scope selector (multi)";

export const WorkflowSelector = () => (
  <div style={wrapperStyle}>
    <StatefulRemoteSelector entityType={"WorkflowSchema"} multi />
  </div>
);

WorkflowSelector.storyName = "Workflow selector";

export const PrioritySelector = () => (
  <div style={wrapperStyle}>
    <StatefulRemoteSelector
      entityType={"Priority"}
      extraFields={["color"]}
      orderByField={"sort"}
      multi
    />
  </div>
);

PrioritySelector.storyName = "Priority selector";

export const BaseFilterCompositingTasks = () => (
  <div style={wrapperStyle}>
    <StatefulRemoteSelector
      entityType={"Task"}
      baseFilter={'type.name is "Compositing"'}
    />
  </div>
);

BaseFilterCompositingTasks.storyName = "Base filter: Compositing tasks";

class ExternalValueTest extends Component {
  constructor(props) {
    super(props);
    this.state = { value: props.value || null };
    this.setValue = this.setValue.bind(this);
    this.addRandomId = this.addRandomId.bind(this);

    session.query("select id from AssetType").then((response) => {
      this.assetIds = response.data.map((assetType) => assetType.id);
    });
  }

  setValue(value) {
    this.setState({ value });
  }

  addRandomId() {
    const randomId = sample(this.assetIds);
    if (this.props.multi) {
      const values = this.state.value || [];
      this.setValue([...values, randomId]);
    } else {
      this.setValue(randomId);
    }
  }

  render() {
    return (
      <div>
        <RemoteSelector
          entityType={"AssetType"}
          extraSearchFields={["short"]}
          value={this.state.value}
          onChange={this.setValue}
          session={session}
          multi={this.props.multi}
        />
        <div style={{ height: "300px", overflowY: "scroll" }}>
          <p>Add new value:</p>
          <Button label="Add random asset type" onClick={this.addRandomId} />
          <p>Current value:</p>
          <pre>{JSON.stringify(this.state.value, null, 2)}</pre>
        </div>
      </div>
    );
  }
}

export const AssetTypeWithExternalValueSingle = () => (
  <div style={wrapperStyle}>
    <ExternalValueTest value="2679f11a-a574-11e3-9614-080027331d71" />
  </div>
);

AssetTypeWithExternalValueSingle.storyName =
  "AssetType with external value (single)";

export const AssetTypeWithExternalValuesMultiple = () => (
  <div style={wrapperStyle}>
    <ExternalValueTest multi />
  </div>
);

AssetTypeWithExternalValuesMultiple.storyName =
  "AssetType with external values (multiple)";

export const StatusSelectorProjectSchema = () => (
  <div style={wrapperStyle}>
    <StatefulStatusSelector
      projectSchemaId={"69cb7f92-4dbf-11e1-9902-f23c91df25eb"}
      entityType={"Task"}
      multi
    />
  </div>
);

StatusSelectorProjectSchema.storyName = "Status selector (project schema)";

export const ProjectSelectorSingle = () => (
  <div style={wrapperStyle}>
    <StatefulProjectSelector />
  </div>
);

ProjectSelectorSingle.storyName = "Project selector (single)";

export const ProjectSelectorMulti = () => (
  <div style={wrapperStyle}>
    <StatefulProjectSelector multi />
  </div>
);

ProjectSelectorMulti.storyName = "Project selector (multi)";

class CollaboratorSelectorExample extends Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.state = { collaborators: [] };
  }

  /** Update project when component is mounted. */
  componentWillMount() {
    this._updateReviewSessionId(this.props.reviewSessionId);
  }

  onChange(collaborators) {
    this.setState({ collaborators });
  }

  /** Update current project to *projectId*.  */
  _updateReviewSessionId(reviewSessionId) {
    let collaborators = [];
    session
      .query(
        "select name, email from ReviewSessionInvitee where " +
          `review_session_id is "${reviewSessionId}"`
      )
      .then((response) => {
        const quotedEmails = [];
        response.data.forEach((item) => {
          if (item.email) {
            quotedEmails.push(`"${item.email}"`);
          }
        });
        collaborators = response.data;

        if (quotedEmails.length) {
          return session.query(
            "select email, first_name, last_name, thumbnail_id from User" +
              ` where email in (${quotedEmails.join(", ")})`
          );
        }

        return Promise.resolve({ data: [] });
      })
      .then((response) => {
        const collaboratorsMap = collaborators.reduce((accumulator, item) => {
          accumulator[item.email] = item;
          return accumulator;
        }, {});

        response.data.forEach((item) => {
          collaboratorsMap[item.email].thumbnail_id = item.thumbnail_id;
        });

        this.setState({ collaborators });
      });
  }

  render() {
    const { collaborators } = this.state;
    return (
      <div style={wrapperStyle}>
        <CollaboratorSelector
          session={session}
          label="Collaborators"
          value={collaborators}
          onChange={this.onChange}
        />
      </div>
    );
  }
}

CollaboratorSelectorExample.propTypes = {
  reviewSessionId: PropTypes.string.isRequired,
};

export const _CollaboratorSelector = () => (
  <CollaboratorSelectorExample reviewSessionId="bbfeae42-5910-11e4-a871-3c0754282242" />
);

_CollaboratorSelector.storyName = "Collaborator selector";

export const RemoteSelectorTest = () => (
  <div style={wrapperStyle}>
    <RemoteSelector
      multi
      value={["DONE", "BLOCKED"]}
      session={session}
      entityType="State"
      placeholder="Select state..."
      valueField="short"
    />
  </div>
);

RemoteSelectorTest.storyName = "Remote selector, with custom value field";

export const RemoteSelectorDefault = () => (
  <div style={wrapperStyle}>
    <RemoteSelector
      multi
      value={["cea0ba04-acb5-11e1-8668-f23c91df1211"]}
      session={session}
      entityType="State"
      placeholder="Select state..."
    />
  </div>
);

RemoteSelectorDefault.storyName = "Remote selector default value field";
