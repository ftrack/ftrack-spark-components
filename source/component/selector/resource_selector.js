// :copyright: Copyright (c) 2016 ftrack
import PropTypes from "prop-types";

import ReactDom from "react-dom";
import Select from "react-select";
import VirtualizedSelect from "react-virtualized-select";
import classNames from "classnames";

import filterAsyncOptions from "./filter_options";
import getQueryFilters from "./get_query_filters";
import getAutoloadSelector from "./get_autoload_selector";
import ExpandExternalValueComponent from "./expand_external_value_component";
import { localizeResourceSelector } from "./localize_selector";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import theme from "./style.scss";
import { THUMBNAIL_SIZES, RESOURCE_TYPE_COLORS } from "../util/constant";

/** Attributes to load for users */
const USER_ATTRIBUTES = [
  "id",
  "thumbnail_id",
  "first_name",
  "last_name",
  "is_active",
];

/** Attributes to load for groups */
const GROUP_ATTRIBUTES = ["id", "name", "link"];

/** Translate User entities to values */
const optionFromUser = (user) => ({
  label: `${user.first_name} ${user.last_name}`,
  value: user.id,
  type: "user",
  data: {
    isActive: user.is_active,
    thumbnailId: user.thumbnail_id,
  },
});

/** Translate Group entities to values */
const optionFromGroup = (group) => ({
  label: group.name,
  value: group.id,
  type: "group",
  data: {
    groupName: group.link
      .slice(0, -1)
      .map((item) => item.name)
      .join(" / "),
  },
});

/** Translate Membership entities to values */
const optionFromMembership = (membership) => {
  const { user, group } = membership;
  return {
    label: `${user.first_name} ${user.last_name}`,
    value: user.id,
    type: "membership",
    data: {
      isActive: user.is_active,
      groupName: group.link.map((item) => item.name).join(" / "),
      thumbnailId: user.thumbnail_id,
    },
  };
};

/** Translate API responses to values */
function optionsFromResponses(responses, { users, groups, memberships }) {
  const userResponse = (users && responses.shift()) || null;
  const groupResponse = (groups && responses.shift()) || null;
  const membershipResponse = (memberships && responses.shift()) || null;
  let options = [];

  if (userResponse && userResponse.data && userResponse.data.length) {
    for (const user of userResponse.data) {
      options.push(optionFromUser(user));
    }
  }

  if (groupResponse && groupResponse.data && groupResponse.data.length) {
    for (const group of groupResponse.data) {
      options.push(optionFromGroup(group));
    }
  }

  if (
    membershipResponse &&
    membershipResponse.data &&
    membershipResponse.data.length
  ) {
    const membershipOptions = [];
    for (const membership of membershipResponse.data) {
      membershipOptions.push(optionFromMembership(membership));
    }

    // Sort memberships client-side as API does not currently
    // support ordering by relationships.
    // TODO: Address this behavior when pagination is introduced.
    membershipOptions.sort((a, b) => a.label.localeCompare(b.label));
    options = options.concat(membershipOptions);
  }

  return options;
}

/**
 * Return function to load resources based on *props*
 *
 * Query and return `User` resources if `users` is set.
 * Query and return `Group` resources if `groups` is set.
 * Query and return `User` resources based on their memberships if `users`
 * and `memberships` is set.
 * Don't include inactive users if `activeUsersOnly` is set.
 *
 * Users are searched based on username, first_name and last_name,
 * Groups are searched based on the groups name.
 */
export function getLoadResourceOptions(props) {
  const {
    session,
    maxResults,
    moreResultsLabel,
    users = true,
    activeUsersOnly = false,
    groups = false,
    memberships = true,
    userFilters = null,
  } = props;
  return function loadResourceOptions(input) {
    const promises = [];

    if (users) {
      const userQueryAttributes = ["username", "first_name", "last_name"];
      let queryFilters = getQueryFilters(
        input,
        userQueryAttributes,
        userFilters
      );
      if (activeUsersOnly && queryFilters) {
        queryFilters += " and is_active is True";
      } else if (activeUsersOnly) {
        queryFilters = "where is_active is True";
      }

      const userQuery = `
                select ${USER_ATTRIBUTES.join(",")} from User
                ${queryFilters}
                order by first_name, last_name
                limit ${maxResults}
            `;
      promises.push(session.query(userQuery));
    }

    if (groups) {
      const queryFilters = getQueryFilters(input, ["link"]);
      const projectGroupFilter = `${
        queryFilters ? "and" : "where"
      } local is False`;
      const groupQuery = `
                select ${GROUP_ATTRIBUTES.join(",")} from Group
                ${queryFilters} ${projectGroupFilter}
                order by link
                limit ${maxResults}
            `;
      promises.push(session.query(groupQuery));
    }

    // Only load memberships if there is any input to avoid listing
    // users twice when only expanding selector
    if (users && memberships && input) {
      let queryFilters = getQueryFilters(
        input,
        ["group.link"],
        userFilters ? `user has (${userFilters})` : null
      );
      if (activeUsersOnly && queryFilters) {
        queryFilters += " and user.is_active is True";
      } else if (activeUsersOnly) {
        queryFilters = "where user.is_active is True";
      }
      const membershipAttributes = [
        "id",
        ...USER_ATTRIBUTES.map((attribute) => `user.${attribute}`),
        ...GROUP_ATTRIBUTES.map((attribute) => `group.${attribute}`),
      ];
      const projectGroupFilter = `${
        queryFilters ? "and" : "where"
      } group.local is False`;
      const membershipQuery = `
                select ${membershipAttributes.join(",")} from Membership
                ${queryFilters} ${projectGroupFilter}
                order by id
                limit ${maxResults}
            `;
      promises.push(session.query(membershipQuery));
    }

    return Promise.all(promises).then((responses) => {
      let moreResultsAvailable = false;
      for (const response of responses) {
        if (response.data && response.data.length === maxResults) {
          moreResultsAvailable = true;
        }
      }
      const options = optionsFromResponses(responses, {
        users,
        groups,
        memberships,
      });
      if (moreResultsAvailable) {
        options.push({
          disabled: true,
          value: null,
          type: "label",
          label: moreResultsLabel,
          data: {},
        });
      }
      return { options };
    });
  };
}

/** Return CSS style for thumbnail. */
function getThumbnailStyle(session, thumbnailId, type) {
  const thumbnailUrl =
    thumbnailId &&
    session.thumbnailUrl(thumbnailId, { size: THUMBNAIL_SIZES.small });
  const backgroundImage = (thumbnailUrl && `url(${thumbnailUrl})`) || null;
  const backgroundColor = RESOURCE_TYPE_COLORS[type];
  return { backgroundImage, backgroundColor };
}

/**
 * Return initials for *inputString*.
 *
 * Can be used to generate initials for an user name or group name.
 * Eg. "Carl Claesson" -> "CC"
 */
function getInitialsFromString(inputString) {
  return inputString
    .split(" ")
    .map((word) => word.charAt(0))
    .join("")
    .slice(0, 2)
    .toUpperCase();
}

/** Resource circle */
export function ResourceCircle({ className, session, type, label, data }) {
  const classes = classNames(theme["color-circle"], className);
  return (
    <span
      className={classes}
      style={getThumbnailStyle(session, data.thumbnailId, type)}
    >
      {data.thumbnailId ? null : getInitialsFromString(label)}
    </span>
  );
}

ResourceCircle.propTypes = {
  className: PropTypes.string,
  session: PropTypes.object.isRequired, // eslint-disable-line
  type: PropTypes.oneOf(["user", "group", "membership"]),
  label: PropTypes.string,
  data: PropTypes.object, // eslint-disable-line
};

/** Resource value */
export function ResourceValue(session, { data, label, type }) {
  let title = label;
  if (data && data.groupName) {
    title += ` (${data.groupName})`;
  }

  return (
    <span className={theme["select-value--circle"]} title={title}>
      {type !== "label" ? (
        <ResourceCircle
          session={session}
          type={type}
          data={data}
          label={label}
        />
      ) : null}
      {label}
    </span>
  );
}

/** Return option subtitle for resource */
export function ResourceOptionSubtitle({
  className,
  type,
  groupName,
  isActive,
  userLabel,
  groupLabel,
  userInactiveLabel,
}) {
  const classes = classNames(theme["select-option-subtitle"], className);

  let subtitle = "";
  if (type === "user" && isActive === false) {
    subtitle = userInactiveLabel;
  } else if (type === "user") {
    subtitle = userLabel;
  } else if (type === "group") {
    subtitle = groupName || groupLabel;
  } else if (type === "membership") {
    subtitle = groupName;
  }

  return <div className={classes}>{subtitle}</div>;
}

ResourceOptionSubtitle.propTypes = {
  className: PropTypes.string,
  type: PropTypes.oneOf(["user", "group", "membership"]).isRequired,
  groupName: PropTypes.string,
  groupLabel: PropTypes.node,
  userLabel: PropTypes.node,
  userInactiveLabel: PropTypes.node,
  isActive: PropTypes.bool,
};

ResourceOptionSubtitle.defaultProps = {
  groupLabel: "Group",
  userLabel: "User",
  userInactiveLabel: "User (inactive)",
};

/** Resource option react-virtualized-select. */
export function VirtualizedResourceOption(session, labels, props) {
  const { userLabel, userInactiveLabel, groupLabel } = labels;

  // eslint-disable-next-line
  const { focusedOption, focusOption, key, option, selectValue, style } = props;
  const className = [theme["select-option--resource"]];
  if (option === focusedOption) {
    className.push(theme["select-option--focused"]);
  }
  if (option.disabled) {
    className.push(theme["select-option--disabled"]);
  }

  const events = option.disabled
    ? {}
    : {
        onClick: () => selectValue(option),
        onMouseOver: () => focusOption(option),
      };

  const { data, label, type } = option;
  return (
    <div className={className.join(" ")} key={key} style={style} {...events}>
      {type !== "label" ? (
        <ResourceCircle
          session={session}
          type={type}
          data={data}
          label={label}
        />
      ) : null}
      <span className={theme["select-option-label"]}>{label}</span>
      <ResourceOptionSubtitle
        type={type}
        userLabel={userLabel}
        userInactiveLabel={userInactiveLabel}
        groupLabel={groupLabel}
        {...data}
      />
    </div>
  );
}

const AutoloadAsyncSelect = getAutoloadSelector(Select.Async);

/** Remote selector */
class ResourceSelector extends ExpandExternalValueComponent {
  constructor(...args) {
    super(...args);
    this.loadResourceOptions = this.loadResourceOptions.bind(this);
    this.handleInternalValueChanged =
      this.handleInternalValueChanged.bind(this);
  }

  /** Return formatted options from resource ids. */
  getValuesFromRawValues(resourceIds) {
    const { session, users, groups } = this.props;
    if (!resourceIds || !(users || groups)) {
      return Promise.resolve([]);
    }

    // Attempt to get values from loaded options.
    const formattedValues = resourceIds
      .map((value) => this._options.find((option) => option.value === value))
      .filter((option) => option);
    if (formattedValues.length === resourceIds.length) {
      return Promise.resolve(formattedValues);
    }

    // Load formatted values from API.
    const promises = [];
    if (users) {
      promises.push(
        session.query(
          `select ${USER_ATTRIBUTES.join(",")} from User where
                id in ("${resourceIds.join('","')}")
                order by first_name, last_name`
        )
      );
    }

    if (groups) {
      promises.push(
        session.query(
          `select ${GROUP_ATTRIBUTES.join(",")} from Group where
                id in ("${resourceIds.join('","')}")
                order by name`
        )
      );
    }

    return Promise.all(promises).then((responses) => {
      const options = optionsFromResponses(responses, { users, groups });
      return options;
    });
  }

  handleInternalValueChanged(...args) {
    super.handleInternalValueChanged(...args);
    this.forceUpdateSelectorHeight();
  }

  /**
   * Force update selector height
   *
   * This fixes issues experienced in Chrome/Mac where the
   * React-Virtualized-Select's internal AutoSize Component does not update
   * until the element received a focus/blur event.
   *
   * https://github.com/bvaughn/react-virtualized-select/issues/2
   */
  forceUpdateSelectorHeight() {
    // eslint-disable-next-line react/no-find-dom-node
    const element = ReactDom.findDOMNode(this.node);
    if (element) {
      // eslint-disable-next-line no-undef
      element.style.height = window.getComputedStyle(element).height;
      setTimeout(() => {
        element.style.height = "auto";
      }, 0);
    }
  }

  loadResourceOptions(input) {
    const fetchOptions = getLoadResourceOptions(this.props)(input);
    fetchOptions.then(({ options }) => {
      this._options = options;
    });
    return fetchOptions;
  }

  /** Render component. */
  render() {
    const {
      session,
      users,
      groups,
      memberships,
      activeUsersOnly,
      maxResults,
      moreResultsLabel,
      userLabel,
      groupLabel,
      userInactiveLabel,
      value,
      ...selectProps
    } = this.props;

    if (!users && !groups) {
      throw new TypeError("Either users or groups must be specified in props.");
    }

    const ResourceValueSession = ResourceValue.bind(null, session);
    const ResourceOptionSession = VirtualizedResourceOption.bind(
      null,
      session,
      { userLabel, groupLabel, userInactiveLabel }
    );
    return (
      <VirtualizedSelect
        selectComponent={AutoloadAsyncSelect}
        loadOptions={this.loadResourceOptions}
        valueRenderer={ResourceValueSession}
        optionRenderer={ResourceOptionSession}
        optionHeight={40}
        filterOptions={filterAsyncOptions}
        {...selectProps}
        value={this.state.internalValue}
        ref={(node) => {
          this.node = node;
        }}
        onChange={this.handleInternalValueChanged}
      />
    );
  }
}

ResourceSelector.propTypes = {
  session: PropTypes.object.isRequired,
  users: PropTypes.bool,
  activeUsersOnly: PropTypes.bool,
  groups: PropTypes.bool,
  memberships: PropTypes.bool,
  maxResults: PropTypes.number,
  moreResultsLabel: PropTypes.node,
  groupLabel: PropTypes.node,
  userLabel: PropTypes.node,
  userInactiveLabel: PropTypes.node,
  userFilters: PropTypes.string,
};

ResourceSelector.defaultProps = {
  activeUsersOnly: false,
  users: false,
  groups: false,
  memberships: true,
  maxResults: 50,
  moreResultsLabel: "More results available, type to narrow down result",
};

export default safeInjectIntl(localizeResourceSelector(ResourceSelector));
