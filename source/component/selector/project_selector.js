// :copyright: Copyright (c) 2016 ftrack
import RemoteSelector from "./remote_selector";

const PROJECT_SELECTOR_PROPS = {
  entityType: "Project",
  labelField: "full_name",
  extraFields: ["name", "color"],
  extraSearchFields: ["name"],
};

/** Project selector */
function ProjectSelector(props) {
  return <RemoteSelector {...PROJECT_SELECTOR_PROPS} {...props} />;
}

export default ProjectSelector;
