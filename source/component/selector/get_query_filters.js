// :copyright: Copyright (c) 2020 ftrack
import getQueryFilter from "../util/get_query_filter";

function getQueryFilters(queryString, attributes, baseFilter) {
  const queryFilter = getQueryFilter(queryString, attributes, baseFilter);

  if (queryFilter.length) {
    return `where ${queryFilter}`;
  }

  return "";
}

export default getQueryFilters;
