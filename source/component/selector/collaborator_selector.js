// :copyright: Copyright (c) 2017 ftrack
/* eslint-disable react/no-multi-comp */

import PropTypes from "prop-types";

import { Component, Fragment } from "react";
import { debounce } from "lodash/function";
import Chip from "react-toolbox/lib/chip";
import { operation } from "ftrack-javascript-api";
import { without } from "lodash/array";
import { defineMessages, FormattedMessage, intlShape } from "react-intl";
import classNames from "classnames";
import Input from "react-toolbox/lib/input";
import { List, ListItem } from "react-toolbox";
import Button from "react-toolbox/lib/button";
import EntityAvatar from "../entity_avatar";
import safeInjectIntl from "../util/hoc/safe_inject_intl";

import { guessName } from "../util/string";

import style from "./style.scss";

const messages = defineMessages({
  "add-collaborator": {
    id: "ftrack-spark-overview.collaborator-selector.add-collaborator",
    defaultMessage: "Add collaborator",
  },
  "search-help-text": {
    id: "ftrack-spark-overview.collaborator-selector.search-help-text",
    defaultMessage:
      "Enter an email address to invite an external collaborator.",
  },
  "invite-prompt": {
    id: "ftrack-spark-overview.collaborator-selector.invite-prompt",
    defaultMessage: "Invite {name} as a collaborator to this review? ",
  },
  "invite-prompt-name": {
    id: "ftrack-spark-overview.collaborator-selector.invite-prompt-name",
    defaultMessage: "You can edit the suggested name below before saving.",
  },
  "invite-empty-name": {
    id: "ftrack-spark-overview.collaborator-selector.invite-empty-name",
    defaultMessage: "Give {email} a name to be able to send an invite.",
  },
  "add-button": {
    id: "ftrack-spark-overview.collaborator-selector.add-button",
    defaultMessage: "Add",
  },
  "known-collaborators": {
    id: "ftrack-spark-components.collaborator-selector.known-collaborators",
    defaultMessage:
      "Previously added collaborators or users in your workspace:",
  },
});

/** Return a LIKE query string from *keys* and *values*. */
function _getFilterString(keys, values) {
  const results = [];

  for (const value of values) {
    const keyResults = [];
    for (const key of keys) {
      if (value) {
        keyResults.push(`${key} like "%${value}%"`);
      }
    }
    if (keyResults.length) {
      results.push(`(${keyResults.join(" or ")})`);
    }
  }

  return results.join(" and ");
}

/** Render result list of collaborator search. */
function ResultList({ items, onClick, className, session }) {
  if (items.length) {
    const result = items.map((item) => {
      const handleClick = onClick.bind(null, item);

      return (
        <ListItem
          key={item.id}
          leftActions={[
            <EntityAvatar
              entity={item}
              session={session}
              className={style["avatar"]}
            />,
          ]}
          caption={item.name}
          legend={item.email}
          onClick={handleClick}
        />
      );
    });

    return (
      <List className={className} selectable ripple>
        {result}
      </List>
    );
  }

  return <div />;
}

ResultList.propTypes = {
  className: PropTypes.string,
  items: PropTypes.shape([
    {
      id: PropTypes.string,
      name: PropTypes.string,
      email: PropTypes.string,
    },
  ]),
  onClick: PropTypes.func,
  session: PropTypes.shape({
    thumbnailUrl: PropTypes.func.isRequired,
  }),
};

/** Represent a collaborator chip that can be removed. */
class ExistingCollaboratorChip extends Component {
  constructor(props) {
    super(props);

    this.onDeleteClick = this.onDeleteClick.bind(this);
  }

  onDeleteClick() {
    this.props.onDeleteClick(this.props.item);
  }

  render() {
    const { item, session } = this.props;
    return (
      <Chip
        deletable
        onDeleteClick={this.onDeleteClick}
        className={style["selected-collaborator-item"]}
      >
        <EntityAvatar
          entity={item}
          session={session}
          className={style.avatar}
        />
        <span>{item.name}</span>
      </Chip>
    );
  }
}

ExistingCollaboratorChip.propTypes = {
  onDeleteClick: PropTypes.func.isRequired,
  item: PropTypes.shape({
    name: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    thumbnail_id: PropTypes.string,
  }),
  session: PropTypes.shape({
    thumbnailUrl: PropTypes.func.isRequired,
  }),
};

const validateEmailRegexp =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

/** Quick review view */
class CollaboratorSelector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      availableCollaborators: [],
      inputActive: false,
      name: "",
      inputValue: "",
    };
    // this._updateProject = this._updateProject.bind(this);
    this._loadCollaborators = debounce(this._loadCollaborators.bind(this), 500);
    this._onCollaboratorsChange = this._onCollaboratorsChange.bind(this);
    this.collaboratorExists = this.collaboratorExists.bind(this);
    this.addCollaborator = this.addCollaborator.bind(this);
    this.removeCollaborator = this.removeCollaborator.bind(this);
    this._onNameChange = this._onNameChange.bind(this);
    this._addNewCollaborator = this._addNewCollaborator.bind(this);
    this._onCollaboratorsKeyDown = this._onCollaboratorsKeyDown.bind(this);
    this._onInputFieldFocus = this._onInputFieldFocus.bind(this);
    this._onInputFieldBlur = this._onInputFieldBlur.bind(this);
    this.collaboratorExists = this.collaboratorExists.bind(this);
  }

  /** Handle changes to the collaborators field. */
  _onCollaboratorsChange(inputValue) {
    this._loadCollaborators(inputValue);

    let name = "";
    if (validateEmailRegexp.test(inputValue)) {
      name = guessName(inputValue);
    }

    this.setState({
      name,
      inputValue,
    });
  }

  /** Handle input field focus. */
  _onInputFieldFocus() {
    this.setState({ inputActive: true });
  }

  /** Handle input field blur. */
  _onInputFieldBlur() {
    this.setState({ inputActive: false });
  }

  /** Load collaborators from server based on *value*. */
  _loadCollaborators(value) {
    // Clear state and return early if nothing to query.
    if (!value || !value.length) {
      this.setState({
        availableCollaborators: [],
      });
      return;
    }

    const parts = value.split(" ");

    const userQuery =
      "select id, first_name, last_name, email,thumbnail_id from BaseUser where " +
      `${_getFilterString(["first_name", "last_name", "email"], parts)}`;

    const { session, alreadyIncludedCollaborators } = this.props;

    const alreadyIncluded = (email) =>
      alreadyIncludedCollaborators
        ? alreadyIncludedCollaborators.some(
            (collaborator) =>
              collaborator.email &&
              collaborator.email.toLowerCase() === email.toLowerCase()
          )
        : false;

    const promise = session.call([operation.query(userQuery)]);

    promise.then((responses) => {
      const results = {};
      let collaborators = [];
      const users = responses[0].data;

      if (users.length) {
        for (const user of users) {
          if (user.email) {
            if (user.email && !alreadyIncluded(user.email)) {
              results[user.email] = {
                id: user.id,
                email: user.email,
                name: `${user.first_name} ${user.last_name}`,
                first_name: user.first_name,
                last_name: user.last_name,
                thumbnail_id: user.thumbnail_id,
              };
            }
          }
        }
      }

      Object.keys(results).forEach((email) => {
        if (!this.collaboratorExists(email)) {
          collaborators.push(results[email]);
        }
      });
      collaborators = collaborators.slice(0, 5);

      collaborators.sort((a, b) => {
        if (a.name < b.name) {
          return -1;
        } else if (a.name > b.name) {
          return 1;
        }
        return 0;
      });

      this.setState({
        availableCollaborators: collaborators.slice(0, 5),
      });
    });
  }

  /** Return if a collaborator already exists. */
  collaboratorExists(email) {
    const matchesEmail = (collaborator) =>
      collaborator.email &&
      collaborator.email.toLowerCase() === email.toLowerCase();

    return (
      this.props.value.some(matchesEmail) ||
      this.props.alreadyIncludedCollaborators.some(matchesEmail)
    );
  }

  /** Add collaborator from *item*. */
  addCollaborator(item) {
    // User already exists.
    const exists = this.collaboratorExists(item.email);
    const collaborators = this.props.value;

    // Clear form.
    this.setState({
      availableCollaborators: [],
    });

    if (exists) {
      return;
    }

    this.setState({
      name: "",
      inputValue: "",
    });

    this.props.onChange([...collaborators, item]);
  }

  /** Remove collaborator *item*. */
  removeCollaborator(item) {
    this.props.onChange(without(this.props.value, item));
  }

  /** Add a new collaborator from the form. */
  _addNewCollaborator() {
    const email = this.state.inputValue.toLowerCase();
    const name = this.state.name;
    const nameParts = name.split(" ");
    const first_name =
      nameParts.length === 1 ? nameParts[0] : nameParts.slice(0, -1).join(" ");
    const last_name = nameParts.length > 1 ? nameParts.slice(-1).join(" ") : "";

    this.addCollaborator({
      name,
      first_name,
      last_name,
      email,
      thumbnail_id: null,
    });
  }

  /** Update the name in state. */
  _onNameChange(name) {
    this.setState({ name });
  }

  /** Handle key press in collaborators. */
  _onCollaboratorsKeyDown(event) {
    const { inputValue, availableCollaborators, name } = this.state;
    if (event.key === "Enter") {
      const emailTaken = this.collaboratorExists(inputValue);

      if (emailTaken) {
        return;
      }

      if (availableCollaborators.length) {
        this.addCollaborator(availableCollaborators[0]);
        event.stopPropagation();
        event.preventDefault();
      } else if (name !== "") {
        this._addNewCollaborator();
        event.stopPropagation();
        event.preventDefault();
      }
    }
  }

  /** Render *collaborators*. */
  renderResult(collaborators) {
    const result = [];
    const { session, footerClassName } = this.props;
    const { inputValue, name, inputActive, availableCollaborators } =
      this.state;

    const emailTaken = this.collaboratorExists(inputValue);

    if (collaborators.length) {
      result.push(
        <Fragment>
          <p className={style["dropdown-header"]}>
            <FormattedMessage {...messages["known-collaborators"]} />
          </p>
          <ResultList
            key="result-list"
            session={session}
            className={style["collaborator-matches"]}
            items={availableCollaborators}
            onClick={this.addCollaborator}
          />
        </Fragment>
      );
    } else if (
      (name !== "" || validateEmailRegexp.test(inputValue)) &&
      !emailTaken
    ) {
      result.push(
        <div
          key="collaborator-add-new"
          className={style["collaborator-add-new"]}
        >
          {name ? (
            <Fragment>
              <p>
                <FormattedMessage
                  {...messages["invite-prompt"]}
                  values={{ name }}
                />
              </p>
              <p className={style["invite-prompt-message"]}>
                <FormattedMessage {...messages["invite-prompt-name"]} />
              </p>
            </Fragment>
          ) : (
            <p>
              <FormattedMessage
                {...messages["invite-empty-name"]}
                values={{ email: inputValue }}
              />
            </p>
          )}

          <div>
            <Input value={name} onChange={this._onNameChange} />
            <Button
              className={style["add-button"]}
              label={this.props.intl.formatMessage(messages["add-button"])}
              primary
              disabled={!name}
              onClick={this._addNewCollaborator}
              type="button"
            />
          </div>
        </div>
      );
    } else if (inputActive) {
      result.push(
        <p key="collaborator-info" className={style["collaborator-info"]}>
          <FormattedMessage {...messages["search-help-text"]} />
        </p>
      );
    }

    if (result && result.length) {
      const footerClasses = classNames(
        style["collaborator-footer"],
        footerClassName
      );

      return <div className={footerClasses}>{result}</div>;
    }
    return null;
  }

  render() {
    const { inputValue } = this.state;
    const { session, label } = this.props;
    const collaborators = this.props.value;

    let selectedCollaborators = null;
    if (collaborators && collaborators.length) {
      const items = collaborators.map((item, index) => (
        <ExistingCollaboratorChip
          key={`collaborator-${index}`}
          item={item}
          session={session}
          onDeleteClick={this.removeCollaborator}
        />
      ));

      selectedCollaborators = (
        <div className={style["selected-collaborators"]}>{items}</div>
      );
    }

    return (
      <div>
        {selectedCollaborators}
        <Input
          label={label}
          placeholder={this.props.intl.formatMessage(
            messages["add-collaborator"]
          )}
          onFocus={this._onInputFieldFocus}
          onBlur={this._onInputFieldBlur}
          type="text"
          value={inputValue}
          onChange={this._onCollaboratorsChange}
          onKeyDown={this._onCollaboratorsKeyDown}
          autoFocus={this.props.autoFocus}
        />
        {this.renderResult(this.state.availableCollaborators)}
      </div>
    );
  }
}

CollaboratorSelector.propTypes = {
  value: PropTypes.arrayOf(
    PropTypes.shape([
      {
        name: PropTypes.string.isRequired,
        email: PropTypes.string,
        id: PropTypes.string.isRequired,
      },
    ]).isRequired
  ),
  session: PropTypes.shape({
    query: PropTypes.func.isRequired,
    call: PropTypes.func.isRequired,
  }).isRequired,
  intl: intlShape.isRequired,
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string,
  alreadyIncludedCollaborators: PropTypes.arrayOf(
    PropTypes.shape([
      {
        name: PropTypes.string.isRequired,
        email: PropTypes.string,
        id: PropTypes.string.isRequired,
      },
    ])
  ),
  footerClassName: PropTypes.string,
  autoFocus: PropTypes.bool,
};

CollaboratorSelector.defaultProps = {
  value: [],
  alreadyIncludedCollaborators: [],
};

export default safeInjectIntl(CollaboratorSelector);
