// :copyright: Copyright (c) 2016 ftrack
import PropTypes from "prop-types";

import Select from "react-select";

import { projectSchema } from "ftrack-javascript-api";
import filterAsyncOptions from "./filter_options";
import getAutoloadSelector from "./get_autoload_selector";
import { SelectValue, SelectOption } from "./remote_selector";

/** Return if *candidate* matches *searchString*. */
function matches(candidate, searchString) {
  const _searchString = searchString.toLowerCase().trim();
  const _candidate = candidate.toLowerCase().trim();
  return !_searchString || _candidate.indexOf(_searchString) !== -1;
}

/** Return function to load status options. */
function getStatusOptions(props) {
  const { session, projectSchemaId, entityType, typeId } = props;
  return (input) => {
    const statusPromise = projectSchema.getStatuses(
      session,
      projectSchemaId,
      entityType,
      typeId
    );

    return statusPromise.then((statuses) => {
      const options = statuses
        .filter((status) => matches(status.name, input))
        .map((status) => ({
          value: status.id,
          label: status.name,
          data: {
            color: status.color,
          },
        }));

      return { options };
    });
  };
}

const AutoloadAsyncSelect = getAutoloadSelector(Select.Async);

/** Status selector */
function StatusSelector(allProps) {
  const { session, projectSchemaId, entityType, typeId, ...selectProps } =
    allProps;

  const ownProps = {
    session,
    projectSchemaId,
    entityType,
    typeId,
  };

  return (
    <AutoloadAsyncSelect
      loadOptions={getStatusOptions(ownProps)}
      valueRenderer={SelectValue}
      optionRenderer={SelectOption}
      filterOptions={filterAsyncOptions}
      {...selectProps}
    />
  );
}

StatusSelector.propTypes = {
  session: PropTypes.object.isRequired,
  projectSchemaId: PropTypes.string,
  entityType: PropTypes.string,
  typeId: PropTypes.string,
};

StatusSelector.defaultProps = {
  typeId: null,
};

export default StatusSelector;
