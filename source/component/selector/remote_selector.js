// :copyright: Copyright (c) 2016 ftrack
import PropTypes from "prop-types";

import Select from "react-select";

import filterAsyncOptions from "./filter_options";
import getQueryFilters from "./get_query_filters";
import getAutoloadSelector from "./get_autoload_selector";
import ExpandExternalValueComponent from "./expand_external_value_component";
import { localizeSelector } from "./localize_selector";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import style from "./style.scss";

/** Select label - render item with optional color circle. */
export function SelectLabel(className, { data, label }) {
  return (
    <span className={className}>
      {data && data.color ? (
        <span
          className={style["color-circle"]}
          style={{ backgroundColor: data.color }}
        />
      ) : null}
      {label}
    </span>
  );
}

/** Selected value */
export function SelectValue({ data, label }) {
  const className =
    data && data.color ? style["select-value--circle"] : style["select-value"];
  return SelectLabel(className, { data, label });
}

/** Select menu option */
export function SelectOption({ data, label, disabled }) {
  let className =
    data && data.color
      ? style["select-option--circle"]
      : style["select-option"];
  if (disabled) {
    className += " " + style["select-option--disabled"];
  }
  return SelectLabel(className, { data, label });
}

const AutoloadAsyncSelect = getAutoloadSelector(Select.Async);

/** Remote selector */
class RemoteSelector extends ExpandExternalValueComponent {
  constructor(...args) {
    super(...args);
    this.loadOptions = this.loadOptions.bind(this);
  }

  /** Return attributes to fetch */
  getAttributes() {
    const { valueField, labelField, extraFields } = this.props;
    return [valueField, labelField, ...extraFields].join(",");
  }

  /** Return attribute to order by */
  getOrderByField() {
    let { orderByField, labelField } = this.props;
    if (orderByField === "labelField") {
      orderByField = labelField;
    }
    return orderByField;
  }

  /**
   * Return `loadOptions` function based on *props*.
   *
   * The returned function will query the API for the specified attributes and
   * return a promise resolved with an options array suitable for
   * usage by react-select.
   */
  loadOptions(input) {
    const {
      session,
      entityType,
      labelField,
      extraSearchFields,
      maxResults,
      baseFilter,
      orderByDirection,
    } = this.props;

    const filterExpression = getQueryFilters(
      input,
      [labelField, ...extraSearchFields],
      baseFilter
    );

    return session
      .query(
        `
            select ${this.getAttributes()}
            from ${entityType} ${filterExpression}
            order by ${this.getOrderByField()} ${orderByDirection}
            limit ${maxResults}`
      )
      .then((response) => {
        this._options = this.valuesFromResponse(response);
        return { options: this._options };
      });
  }

  /** Convert response to options array suitable for react-select. */
  valuesFromResponse(response) {
    const { valueField, labelField, maxResults, moreResultsLabel } = this.props;
    const values = response.data.map((entity) => ({
      value: entity[valueField],
      label: entity[labelField],
      data: entity,
    }));
    if (values.length === maxResults) {
      values.push({
        disabled: true,
        value: null,
        label: moreResultsLabel,
        data: {},
      });
    }
    return values;
  }

  /** Get formatted values based on *rawValues*. */
  getValuesFromRawValues(rawValues) {
    if (!rawValues || !rawValues.length) {
      return Promise.resolve([]);
    }

    // Attempt to get values from loaded options.
    const formattedValues = rawValues
      .map((value) => this._options.find((option) => option.value === value))
      .filter((option) => option);

    if (formattedValues.length === rawValues.length) {
      return Promise.resolve(formattedValues);
    }

    // Load values
    const { session, entityType, valueField } = this.props;

    return session
      .query(
        `select ${this.getAttributes()} from ${entityType}
            where ${valueField} in ("${rawValues.join('", "')}")
            order by ${this.getOrderByField()}`
      )
      .then((response) => this.valuesFromResponse(response));
  }

  /** Render component. */
  render() {
    const {
      session,
      entityType,
      valueField,
      labelField,
      orderByField,
      extraFields,
      extraSearchFields,
      value,
      ...selectProps
    } = this.props;

    return (
      <AutoloadAsyncSelect
        loadOptions={this.loadOptions}
        valueRenderer={SelectValue}
        optionRenderer={SelectOption}
        filterOptions={filterAsyncOptions}
        {...selectProps}
        value={this.state.internalValue}
        onChange={this.handleInternalValueChanged}
      />
    );
  }
}

RemoteSelector.propTypes = {
  session: PropTypes.object.isRequired,
  entityType: PropTypes.string.isRequired,
  labelField: PropTypes.string,
  orderByField: PropTypes.string,
  orderByDirection: PropTypes.string,
  baseFilter: PropTypes.string,
  extraFields: PropTypes.array,
  extraSearchFields: PropTypes.array,
  maxResults: PropTypes.number,
  moreResultsLabel: PropTypes.string,
};

RemoteSelector.defaultProps = {
  valueField: "id",
  labelField: "name",
  orderByField: "labelField",
  extraFields: [],
  extraSearchFields: [],
  maxResults: 50,
  orderByDirection: "ASC",
  moreResultsLabel: "More results available, type to narrow down result",
};

export default safeInjectIntl(localizeSelector(RemoteSelector));
