// :copyright: Copyright (c) 2018 ftrack
import { defineMessages } from "react-intl";
import { withProps } from "recompose";

export const selectMessages = defineMessages({
  placeholder: {
    id: "ftrack-spark-components.selector.placeholder",
    description: "Placeholder for select components",
    defaultMessage: "Select...",
  },
  "loading-placeholder": {
    id: "ftrack-spark-components.selector.loading-placeholder",
    description: "Loading placeholder for select components",
    defaultMessage: "Loading...",
  },
  "search-prompt-text": {
    id: "ftrack-spark-components.selector.search-prompt-text",
    description: "Type to search placeholder for select components",
    defaultMessage: "Type to search",
  },
  "clear-value-text": {
    id: "ftrack-spark-components.selector.clear-value-text",
    description: "Tooltip for clear value icon for select components",
    defaultMessage: "Clear",
  },
  "clear-all-text": {
    id: "ftrack-spark-components.selector.clear-all-text",
    description: "Tooltip for clear all icon for select components",
    defaultMessage: "Clear all",
  },
  "group-label": {
    id: "ftrack-spark-components.selector.group-label",
    description: "Selector option subtitle for group",
    defaultMessage: "Group",
  },
  "user-label": {
    id: "ftrack-spark-components.selector.user-label",
    description: "Selector option subtitle for user",
    defaultMessage: "User",
  },
  "user-inactive-label": {
    id: "ftrack-spark-components.selector.user-inactive-label",
    description: "Selector option subtitle for inactive user.",
    defaultMessage: "User (inactive)",
  },
  "more-results-label": {
    id: "ftrack-spark-components.selector.more-results-label",
    description: "Selector message when more results are available.",
    defaultMessage: "More results available, type to narrow down result",
  },
});

export const localizeSelector = withProps(
  ({
    intl,
    searchPromptText,
    loadingPlaceholder,
    placeholder,
    clearValueText,
    clearAllText,
  }) => ({
    placeholder: placeholder || intl.formatMessage(selectMessages.placeholder),
    loadingPlaceholder:
      loadingPlaceholder ||
      intl.formatMessage(selectMessages["loading-placeholder"]),
    searchPromptText:
      searchPromptText ||
      intl.formatMessage(selectMessages["search-prompt-text"]),
    clearValueText:
      clearValueText || intl.formatMessage(selectMessages["clear-value-text"]),
    clearAllText:
      clearAllText || intl.formatMessage(selectMessages["clear-all-text"]),
  })
);

export const localizeResourceSelector = withProps(
  ({
    intl,
    searchPromptText,
    loadingPlaceholder,
    placeholder,
    clearValueText,
    clearAllText,
    groupLabel,
    userLabel,
    userInactiveLabel,
  }) => ({
    placeholder: placeholder || intl.formatMessage(selectMessages.placeholder),
    loadingPlaceholder:
      loadingPlaceholder ||
      intl.formatMessage(selectMessages["loading-placeholder"]),
    searchPromptText:
      searchPromptText ||
      intl.formatMessage(selectMessages["search-prompt-text"]),
    clearValueText:
      clearValueText || intl.formatMessage(selectMessages["clear-value-text"]),
    clearAllText:
      clearAllText || intl.formatMessage(selectMessages["clear-all-text"]),
    moreResultsLabel: intl.formatMessage(selectMessages["more-results-label"]),
    groupLabel: groupLabel || intl.formatMessage(selectMessages["group-label"]),
    userLabel: userLabel || intl.formatMessage(selectMessages["user-label"]),
    userInactiveLabel:
      userInactiveLabel ||
      intl.formatMessage(selectMessages["user-inactive-label"]),
  })
);
