..
    :copyright: Copyright (c) 2016 ftrack

#########
Selectors
#########

This directory contains a collection of Selector components that allows for easy
selection of entities using the ftrack JavaScript API.


Selectors based on react-select
===============================

The follwoing selectors are built as a wrappers around react-select with custom
styling to match react-toolbox.

To import the (global) styling, include the following in you application's
sass.

    @import "~ftrack-spark-components/lib/selector/style/index.scss";

These selector components have the following dependencies:

    * ftrack-javascript-api: git+ssh://git@bitbucket.org/ftrack/ftrack-javascript-api.git#24b48a3
    * normalize.css: ^4.1.1
    * react: ^15.1.0
    * react-addons-css-transition-group: ^15.1.0
    * react-dom: ^15.1.0
    * react-select: ^1.0.0-rc.2
    * react-toolbox: ^1.0.0
    * react-virtualized-select: ^2.0.1



General properties
-------------------

A few of the general properties can be found here, props are passed down
to ReactSelect.Async, so you can specify any props it accepts as well.

https://github.com/JedWatson/react-select

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
value                           Selected value(s).
onChange                        Handler called when values changes.
placeholder        Select...   Field placeholder, displayed when there's no value.
multi              false        Multi-value input.
cache              false        Enables the options cache for options.
================== =========== ======================================


RemoteSelector
--------------

Generic selector which works with most models in exposed in the API. Additional
properties are:

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
session                        ftrack JavaScript API session
entityType                     The entity type (model) to query.
valueField         id          The field (attribute) to use as value
labelField         name        The field (attribute) to use as label
extraFields        []          Additional fields to select, such as thumbnail_id and color.
extraSearchFields  []          Additional fields to search, such as full_name.
orderByField       labelField  Fields to order results by, such as sort index.
orderByDirection   ASC         Direction to order result by.
maxResults         50          Amount of maximum results to show
moreResultsLabel   More resu.. Label displayed at bottom of results when not all where fetched.
baseFilter                     Extra filters to apply to API query.
================== =========== ======================================

ProjectSelector
---------------

Selector for Projects. Simply a configuration of the RemoteSelector that works
well for projects.

StatusSelector
--------------

Selector for statuses, which can filter based on a schema, entity type and
optionally type id.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
session                        ftrack JavaScript API session
projectSchemaId                The project schema id to query statuses for.
entityType                     The entity type for which to query statuses for.
typeId             null        If specified, only load statuses valid for the type.
================== =========== ======================================


ResourceSelector
----------------

Selector for users and groups.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
session                        ftrack JavaScript API session
users              false       Select users
groups             false       Select groups
memberships        true        Search users via group memberships.
activeUsersOnly    false       Don't include inactive users in results.
maxResults         50          Amount of maximum results per type to show
moreResultsLabel   More resu.. Label displayed at bottom of results when not all where fetched.
groupLabel         Group       Subtitle for group results without a parent group.
userLabel          User        Subtitle for active user results
userInactiveLabel  User (ina.. Subtitle for inactive user results
================== =========== ======================================


Selectors based on react-toolbox
================================

The follwoing selectors are built on react-toolbox and have the following
dependencies:

    * ftrack-javascript-api: git+ssh://git@bitbucket.org/ftrack/ftrack-javascript-api.git#24b48a3
    * normalize.css: ^4.1.1
    * react: ^15.1.0
    * react-dom: ^15.1.0
    * react-toolbox: ^1.0.0

CollaboratorSelector
--------------------

Selector for users and invitees to be used as collaborators on a review session.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
session                         ftrack JavaScript API session
value                           Selected value(s).
onChange                        Handler called when values changes.
explicitInvite     false        Mention that invitee emails are not
                                automatically sent.
================== =========== ======================================
