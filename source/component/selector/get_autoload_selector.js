// :copyright: Copyright (c) 2016 ftrack
import PropTypes from "prop-types";

/**
 * Higher-Order Component used to customize the behavior of an Select.Async
 * component.
 *
 * - We do not want any caching of results.
 * - Items should be loaded when the input is focused.
 * - Items should also be loaded when an item is selected, so that all items
 *   are available matching the input field.
 */
function getAutoloadSelector(SelectorComponent) {
  function AutoloadSelector(props) {
    let asyncSelect = null;
    return (
      <SelectorComponent
        cache={false}
        openOnFocus
        autoload={false}
        {...props}
        ref={(select) => {
          asyncSelect = select;
        }}
        onFocus={(...args) => {
          asyncSelect.loadOptions("");
          if (props.onFocus) {
            props.onFocus(...args);
          }
        }}
      />
    );
  }

  AutoloadSelector.propTypes = {
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
  };

  return AutoloadSelector;
}

export default getAutoloadSelector;
