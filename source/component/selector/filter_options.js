// :copyright: Copyright (c) 2016 ftrack

/** Filter resource *options*, excluding only *currentValues* */
function filterAsyncOptions(options, query, currentValues, props) {
  const excludeOptions = currentValues
    ? currentValues.map((currentValue) => currentValue[props.valueKey])
    : [];

  return options.filter((option) => {
    if (excludeOptions.indexOf(option[props.valueKey]) !== -1) {
      return false;
    }
    return true;
  });
}

export default filterAsyncOptions;
