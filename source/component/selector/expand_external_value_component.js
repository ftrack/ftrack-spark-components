// :copyright: Copyright (c) 2016 ftrack
import { Component } from "react";
import PropTypes from "prop-types";
import castArray from "lodash/castArray";
import isUndefined from "lodash/isUndefined";
import isEqual from "lodash/isEqual";

function getRawValue(item) {
  return item && item.value ? item.value : item;
}

function getRawValues(formattedValue) {
  return castArray(formattedValue)
    .map(getRawValue)
    .filter((item) => item);
}

/**
 * Abstract component handling external value changes for Selector components
 *
 * When the value prop is changed, fetches new formatted values for display.
 *
 * When values are changed, the *onChange* prop is called with the raw values.
 *
 * To support loading of options when setting raw values, extend this component
 * and implement the `getValuesFromRawValues` method.
 */
class ExpandExternalValueComponent extends Component {
  constructor() {
    super();
    this.state = { internalValue: null, externalValue: null };
    this.handleInternalValueChanged =
      this.handleInternalValueChanged.bind(this);
    this._options = [];
  }

  /** Handle external values when component mounts */
  componentDidMount() {
    if (this.props.value) {
      this.handleExternalValueChanged(this.props.value);
    }
  }

  /** Handle external values when value changes */
  componentWillReceiveProps(nextProps) {
    if (!isEqual(nextProps.value, this.props.value)) {
      this.handleExternalValueChanged(nextProps.value);
    }
  }

  /**
   * Return promise resolved with formatted values suitable for selector
   * based on *rawValues*.
   */
  // eslint-disable-next-line
  getValuesFromRawValues(rawValues) {
    throw new Error("Not implemented", rawValues);
  }

  /** Return a single item if single selection or or *values* if multi. */
  getFirstItemOrArray(values) {
    return this.props.multi ? values : values[0];
  }

  /** Internal change handler */
  handleInternalValueChanged(internalValue) {
    let externalValue = this.getFirstItemOrArray(getRawValues(internalValue));
    if (isUndefined(externalValue)) {
      externalValue = null;
    }
    const nextInternalValue = this.getFirstItemOrArray(
      castArray(internalValue)
    );

    this.setState({
      internalValue: nextInternalValue,
      externalValue,
    });
    if (this.props.onFormattedValueChange) {
      this.props.onFormattedValueChange(nextInternalValue);
    }
    if (this.props.onChange) {
      this.props.onChange(externalValue);
    }
  }

  /**
   * External change handler
   *
   * Loads internal/formatted values for new values and removes any
   * internal/formatted value no longer present in *value*.
   */
  handleExternalValueChanged(value) {
    const { internalValue, externalValue } = this.state;
    const nextExternalValues = getRawValues(value);
    const nextExternalValue = this.getFirstItemOrArray(nextExternalValues);

    if (!isEqual(nextExternalValue, externalValue)) {
      let nextInternalValues = castArray(internalValue);
      const missingValues = nextExternalValues.filter(
        (rawValue) =>
          !nextInternalValues.includes(
            (formattedValue) => getRawValue(formattedValue) === rawValue
          )
      );
      nextInternalValues = nextInternalValues.filter((formattedValue) =>
        nextExternalValues.some(
          (rawValue) => rawValue === getRawValue(formattedValue)
        )
      );
      const nextInternalValue = this.getFirstItemOrArray(nextInternalValues);
      this.setState({
        internalValue: nextInternalValue,
        externalValue: nextExternalValue,
      });

      if (missingValues.length) {
        this.getValuesFromRawValues(nextExternalValues).then((newValues) => {
          this.handleInternalValueChanged(newValues);
        });
      }
    }
  }
}

ExpandExternalValueComponent.propTypes = {
  onFormattedValueChange: PropTypes.func,
  onChange: PropTypes.func,
  multi: PropTypes.bool,
  value: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object,
    PropTypes.string,
  ]),
};
export default ExpandExternalValueComponent;
