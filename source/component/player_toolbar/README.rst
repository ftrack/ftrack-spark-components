..
    :copyright: Copyright (c) 2018 ftrack

##############
Player Toolbar
##############

The player toolbar can be used to control media playback.

All props are spread to the root span element.
