// :copyright: Copyright (c) 2018 ftrack
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import classNames from "classnames";
import ButtonMenu, { DropdownButton, SectionMenu } from "../button_menu_old";
import getFormattedTime, {
  FORMAT_FRAMES,
  FORMAT_STANDARD,
  FORMAT_TIMECODE,
} from "../util/get_formatted_time";

import messages from "./messages";
import style from "./style.scss";

function FormattedTime({
  className,
  currentFrame,
  format,
  frameRate,
  onChangeFormat,
  tooltipPosition,
  totalFrames,
}) {
  const timeFormats = [
    {
      title: <FormattedMessage {...messages["time-format-title"]} />,
      options: [
        {
          value: FORMAT_STANDARD,
          caption: <FormattedMessage {...messages["time-format-standard"]} />,
          selectedIcon: "check",
          selected: format === "standard",
        },
        {
          value: FORMAT_FRAMES,
          caption: <FormattedMessage {...messages["time-format-frames"]} />,
          selectedIcon: "check",
          selected: format === "frames",
        },
        {
          value: FORMAT_TIMECODE,
          caption: <FormattedMessage {...messages["time-format-timecode"]} />,
          selectedIcon: "check",
          selected: format === "timecode",
        },
      ],
    },
  ];
  const formattedTime = getFormattedTime(
    format,
    currentFrame,
    totalFrames,
    frameRate
  );

  const buttonLabel = (
    <span>
      {formattedTime[0]}
      {format !== FORMAT_TIMECODE && (
        <span className={style.formattedTimeTotal}> / {formattedTime[1]}</span>
      )}
    </span>
  );

  const classes = classNames(style.formattedTime, className);
  return (
    <ButtonMenu
      button={
        <DropdownButton
          tooltip={<FormattedMessage {...messages["time-format-tooltip"]} />}
          tooltipPosition={tooltipPosition}
        >
          {buttonLabel}
        </DropdownButton>
      }
      className={classes}
      position="bottomLeft"
    >
      <SectionMenu
        sections={timeFormats}
        key={format}
        onSelectionChange={(value) => {
          onChangeFormat(value);
        }}
      />
    </ButtonMenu>
  );
}

FormattedTime.propTypes = {
  className: PropTypes.string,
  currentFrame: PropTypes.number,
  format: PropTypes.string,
  frameRate: PropTypes.number,
  onChangeFormat: PropTypes.func,
  tooltipPosition: PropTypes.string,
  totalFrames: PropTypes.number,
};

export default FormattedTime;
