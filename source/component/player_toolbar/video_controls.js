// :copyright: Copyright (c) 2018 ftrack
import { Fragment } from "react";
import PropTypes from "prop-types";
import Slider from "@mui/material/Slider";
import withTooltip from "react-toolbox/lib/tooltip";
import FontIcon from "react-toolbox/lib/font_icon";

import classNames from "classnames";
import { FormattedMessage } from "react-intl";
import DownloadButton from "../player_toolbar/download_button";
import PlaybackMenu from "../player_toolbar/playback_menu.js";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import IconButton from "../icon_button";

import style from "./style.scss";
import activeButtonTheme from "./active_button_theme.scss";
import clearAnnotationButtonTheme from "./clear_annotation_button_theme.scss";
import messages from "./messages";

const TooltipIconButton = withTooltip(IconButton);
const TooltipSpan = withTooltip("span");

function PlayButton({ isPlaying, onTogglePlay, tooltipPosition }) {
  const playIcon = isPlaying ? "pause" : "play_arrow";
  return (
    <TooltipIconButton
      className={style.largeIcon}
      icon={playIcon}
      onClick={() => {
        onTogglePlay(!isPlaying);
      }}
      tooltip={
        isPlaying ? (
          <FormattedMessage {...messages["pause-tooltip"]} />
        ) : (
          <FormattedMessage {...messages["play-tooltip"]} />
        )
      }
      tooltipPosition={tooltipPosition}
    />
  );
}

PlayButton.propTypes = {
  onTogglePlay: PropTypes.func,
  isPlaying: PropTypes.bool,
  tooltipPosition: PropTypes.string,
};

function VideoControls({
  className,
  isPlaying,
  onTogglePlay,
  onToolClicked,
  onVolumeChanged,
  volume,
  activeTool,
  downloadableComponents,
  loop,
  playbackMode,
  onPlaybackModeChanged,
  playbackRate,
  onPlaybackRateChanged,
  onLoopToggle,
  playAllSequenceMediaIds,
  children,
  onClearAnnotation,
  tooltipPosition,
  onZoomToolChanged,
  zoomValue,
  isComparing,
  canCompare,
  ...props
}) {
  let volumeIcon = "volume_off";
  if (volume) {
    volumeIcon = (
      <Fragment>
        <FontIcon className={style.volumeDown} value="volume_down" />
        <FontIcon
          className={style.volumeUp}
          style={{ opacity: volume }}
          value="volume_up"
        />
      </Fragment>
    );
  }

  const classes = classNames(style.controls, className);
  return (
    <div className={classes} {...props}>
      <div>
        <PlayButton
          isPlaying={isPlaying}
          onTogglePlay={onTogglePlay}
          tooltipPosition={tooltipPosition}
        />
        <TooltipIconButton
          value="volume"
          onClick={() => onToolClicked("volume")}
          icon={volumeIcon}
          tooltip={
            volume === 0 ? (
              <FormattedMessage {...messages["unmute-volume-tooltip"]} />
            ) : (
              <FormattedMessage {...messages["mute-volume-tooltip"]} />
            )
          }
          tooltipPosition={tooltipPosition}
        />
        <Slider
          min={0}
          max={1}
          step={0.01}
          onChange={(event, selectedValue) => {
            onVolumeChanged(selectedValue);
          }}
          value={volume}
          color="neutral"
          sx={{
            flex: 1,
            my: 0,
            mr: 2,
            ml: 1,
            zIndex: 0,
            display: "inline-block",
            verticalAlign: "middle",
            width: 90,
          }}
        />
        <PlaybackMenu
          // Force menu to close on value changes.
          key={`menu-${loop}-${playbackMode}-${playbackRate}`}
          className={style.playbackButton}
          loop={loop}
          onLoopToggle={onLoopToggle}
          onPlaybackModeChanged={onPlaybackModeChanged}
          playbackMode={playbackMode}
          onPlaybackRateChanged={onPlaybackRateChanged}
          playbackRate={playbackRate}
          playAllSequenceMediaIds={playAllSequenceMediaIds}
          tooltipPosition={tooltipPosition}
        />
        {children}
      </div>
      <div className={style.tools}>
        <TooltipSpan
          tooltipPosition={tooltipPosition}
          tooltip={<FormattedMessage {...messages["compare-mode"]} />}
          className={style.compare}
        >
          <IconButton
            icon="compare"
            value="COMPARE"
            disabled={!canCompare}
            onClick={() => onToolClicked("COMPARE")}
            theme={isComparing ? activeButtonTheme : undefined}
          />
        </TooltipSpan>
        <TooltipIconButton
          icon="wb_sunny"
          value="LASER"
          onClick={() => onToolClicked("LASER")}
          tooltip={<FormattedMessage {...messages["laser-tool"]} />}
          theme={activeTool === "LASER" ? activeButtonTheme : undefined}
          tooltipPosition={tooltipPosition}
        />
        <TooltipIconButton
          icon="brush"
          onClick={onClearAnnotation}
          tooltip={
            <FormattedMessage {...messages["annotation-button-tooltip"]} />
          }
          theme={clearAnnotationButtonTheme}
          tooltipPosition={tooltipPosition}
        />
        <TooltipIconButton
          theme={activeTool === "KEYBOARD" ? activeButtonTheme : undefined}
          value="KEYBOARD"
          onClick={() => onToolClicked("KEYBOARD")}
          icon="keyboard"
          tooltip={
            <FormattedMessage {...messages["keyboard-shortcuts-tooltip"]} />
          }
          tooltipPosition={tooltipPosition}
        />
        <TooltipIconButton
          theme={activeTool === "ZOOM" ? activeButtonTheme : undefined}
          value="ZOOM"
          onClick={() => {
            onZoomToolChanged("ZOOM");
            onToolClicked("ZOOM");
          }}
          icon="zoom_in"
          tooltip={<FormattedMessage {...messages["zoom-tool-tooltip"]} />}
          tooltipPosition={tooltipPosition}
        />
        <TooltipIconButton
          value="PAN"
          theme={activeTool === "PAN" ? activeButtonTheme : undefined}
          onClick={() => onToolClicked("PAN")}
          icon="pan_tool"
          className={style.smallIcon}
          tooltip={<FormattedMessage {...messages["pan-tool-tooltip"]} />}
          tooltipPosition={tooltipPosition}
        />
        <TooltipIconButton
          value="fullscreen"
          onClick={() => onToolClicked("fullscreen")}
          icon="fullscreen"
          tooltip={
            <FormattedMessage {...messages["fullscreen-mode-tooltip"]} />
          }
          tooltipPosition={tooltipPosition}
        />
        {downloadableComponents && downloadableComponents.length ? (
          <DownloadButton
            downloadableComponents={downloadableComponents}
            tooltipPosition={tooltipPosition}
          />
        ) : null}
      </div>
    </div>
  );
}

VideoControls.propTypes = {
  className: PropTypes.string,
  isPlaying: PropTypes.bool,
  onTogglePlay: PropTypes.func,
  onToolClicked: PropTypes.func,
  onVolumeChanged: PropTypes.func,
  volume: PropTypes.number,
  activeTool: PropTypes.string,
  downloadableComponents: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.node.isRequired,
      value: PropTypes.string.isRequired,
    })
  ),
  loop: PropTypes.bool,
  onLoopToggle: PropTypes.func,
  onPlaybackModeChanged: PropTypes.func,
  onPlaybackRateChanged: PropTypes.func,
  onClick: PropTypes.func,
  playbackMode: PropTypes.string,
  playbackRate: PropTypes.number,
  mediaType: PropTypes.string,
  playAllSequenceMediaIds: PropTypes.arrayOf(PropTypes.string),
  children: PropTypes.node,
  onClearAnnotation: PropTypes.func,
  tooltipPosition: PropTypes.string,
  onZoomToolChanged: PropTypes.func,
  zoomValue: PropTypes.string,
  isComparing: PropTypes.bool,
  canCompare: PropTypes.bool,
};

VideoControls.defaultProps = {
  isPlaying: false,
};

export default safeInjectIntl(VideoControls);
