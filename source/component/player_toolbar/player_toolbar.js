// :copyright: Copyright (c) 2018 ftrack
import { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import ProgressBar from "react-toolbox/lib/progress_bar";

import style from "./style.scss";
import progressBarTheme from "./progress_bar_theme.scss";

class PlayerToolbarBase extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.handleMouseUp = this.handleMouseUp.bind(this);
    this.handleTouchStart = this.handleTouchStart.bind(this);
    this.handleTouchMove = this.handleTouchMove.bind(this);
    this.handleTouchEnd = this.handleTouchEnd.bind(this);
    this.handleSeekMove = this.handleSeekMove.bind(this);
    this.removeSeekerListeners = this.removeSeekerListeners.bind(this);
  }

  componentWillUnmount() {
    this.removeSeekerListeners();
  }

  handleTouchStart(event) {
    event.preventDefault();
    event.stopPropagation();
    if (this.props.onIsSeekingChanged) {
      this.props.onIsSeekingChanged(true);
    }
    document.body.addEventListener("touchmove", this.handleTouchMove);
    document.body.addEventListener("touchend", this.handleTouchEnd);
    const firstTouch = event.changedTouches[0];
    this.handleSeekMove(firstTouch);
  }

  handleTouchMove(event) {
    event.preventDefault();
    event.stopPropagation();
    const firstTouch = event.changedTouches[0];
    this.handleSeekMove(firstTouch);
  }

  handleTouchEnd(event) {
    event.preventDefault();
    event.stopPropagation();
    this.removeSeekerListeners();
  }

  handleMouseDown(event) {
    if (this.props.onIsSeekingChanged) {
      this.props.onIsSeekingChanged(true);
    }
    document.body.addEventListener("mousemove", this.handleMouseMove);
    document.body.addEventListener("mouseup", this.handleMouseUp);
    this.handleSeekMove(event);
  }

  handleMouseMove(event) {
    event.stopPropagation();
    event.preventDefault();
    this.handleSeekMove(event);
  }

  handleSeekMove(event) {
    const clientX = event.clientX;
    const bounds = this.seekerNode.getBoundingClientRect();

    // Get normalized frame [firstFrame, totalFrames]
    const progress = (clientX - bounds.left) / bounds.width;
    let frame = progress * this.props.totalFrames + this.props.firstFrame;
    frame = Math.trunc(
      Math.min(Math.max(frame, this.props.firstFrame), this.props.totalFrames)
    );
    this.props.onChangeFrame(frame);
  }

  handleMouseUp(event) {
    event.stopPropagation();
    event.preventDefault();
    this.removeSeekerListeners();
  }

  removeSeekerListeners() {
    if (this.props.onIsSeekingChanged) {
      this.props.onIsSeekingChanged(false);
    }
    document.body.removeEventListener("mousemove", this.handleMouseMove);
    document.body.removeEventListener("mouseup", this.handleMouseUp);
    document.body.removeEventListener("touchmove", this.handleTouchMove);
    document.body.removeEventListener("touchend", this.handleTouchEnd);
  }

  render() {
    const {
      buffer,
      children,
      className,
      currentFrame,
      firstFrame, // eslint-disable-line
      onChangeFrame,
      totalFrames,
      loopIn,
      loopOut,
      sequenceMemberFrameOuts,
      ...props
    } = this.props;
    const showProgressBar = this.props.totalFrames > 1;
    const classes = classNames(style.root, className);

    return (
      <div className={classes} {...props}>
        {showProgressBar ? (
          <div
            className={style.progressBarWrapper}
            ref={(node) => {
              this.seekerNode = node;
            }}
            onMouseDown={onChangeFrame && this.handleMouseDown}
            onTouchStart={onChangeFrame && this.handleTouchStart}
          >
            <ProgressBar
              className={style.progressBar}
              theme={progressBarTheme}
              type="linear"
              mode="determinate"
              value={currentFrame}
              buffer={buffer}
              min={0}
              max={totalFrames - 1}
            />
            <div className={style["pointers-wrapper"]}>
              {loopIn >= 0 ? (
                <span
                  style={{
                    left: `${(loopIn / totalFrames) * 100}%`,
                  }}
                  className={style["point-in-out-mark"]}
                />
              ) : null}
              {loopOut >= 0 ? (
                <span
                  style={{
                    left: `${(loopOut / totalFrames) * 100}%`,
                  }}
                  className={style["point-in-out-mark"]}
                />
              ) : null}
              {sequenceMemberFrameOuts && sequenceMemberFrameOuts.length >= 1
                ? sequenceMemberFrameOuts.map((item) => (
                    <span
                      style={{
                        left: `${(item / totalFrames) * 100}%`,
                      }}
                      className={style["frame-out-marker"]}
                    />
                  ))
                : null}
            </div>
          </div>
        ) : null}
        {children}
      </div>
    );
  }
}

PlayerToolbarBase.propTypes = {
  buffer: PropTypes.number,
  children: PropTypes.node,
  className: PropTypes.string,
  currentFrame: PropTypes.number,
  firstFrame: PropTypes.number,
  onChangeFrame: PropTypes.func,
  totalFrames: PropTypes.number,
  loopIn: PropTypes.number,
  loopOut: PropTypes.number,
  sequenceMemberFrameOuts: PropTypes.arrayOf(PropTypes.number),
  onIsSeekingChanged: PropTypes.func,
};

PlayerToolbarBase.defaultProps = {
  buffer: 0,
  currentFrame: 0,
  firstFrame: 0,
  totalFrames: 1,
};

const PlayerToolbar = PlayerToolbarBase;

export default PlayerToolbar;
