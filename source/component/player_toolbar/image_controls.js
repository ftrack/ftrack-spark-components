// :copyright: Copyright (c) 2018 ftrack
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import withTooltip from "react-toolbox/lib/tooltip";
import classNames from "classnames";
import PageIndicator from "../page_indicator";
import DownloadButton from "../player_toolbar/download_button";
import IconButton from "../icon_button";
import safeInjectIntl from "../util/hoc/safe_inject_intl";

import style from "./style.scss";
import activeButtonTheme from "./active_button_theme.scss";
import clearAnnotationButtonTheme from "./clear_annotation_button_theme.scss";
import messages from "./messages";

const TooltipIconButton = withTooltip(IconButton);
const TooltipSpan = withTooltip("span");

function ImageControls({
  className,
  currentFrame,
  totalFrames,
  onChangeFrame,
  activeTool,
  onToolClicked,
  downloadableComponents,
  onClearAnnotation,
  tooltipPosition,
  onZoomToolChanged,
  zoomValue,
  bigZoomIsEnabled,
  bigZoom,
  isComparing,
  canCompare,
  ...props
}) {
  const classes = classNames(style.controls, className);
  return (
    <div className={classes} {...props}>
      <div>
        {totalFrames > 1 ? (
          <PageIndicator
            current={currentFrame}
            total={totalFrames}
            onChangePage={onChangeFrame}
          />
        ) : null}
      </div>
      <div>
        <TooltipSpan
          tooltipPosition={tooltipPosition}
          tooltip={<FormattedMessage {...messages["compare-mode"]} />}
          className={style.compare}
        >
          <IconButton
            icon="compare"
            value="COMPARE"
            disabled={!canCompare}
            onClick={() => onToolClicked("COMPARE")}
            theme={isComparing ? activeButtonTheme : undefined}
          />
        </TooltipSpan>
        <TooltipIconButton
          icon="wb_sunny"
          value="LASER"
          onClick={() => onToolClicked("LASER")}
          tooltip={<FormattedMessage {...messages["laser-tool"]} />}
          theme={activeTool === "LASER" ? activeButtonTheme : undefined}
          tooltipPosition={tooltipPosition}
        />
        <TooltipIconButton
          icon="brush"
          onClick={onClearAnnotation}
          tooltip={
            <FormattedMessage {...messages["annotation-button-tooltip"]} />
          }
          theme={clearAnnotationButtonTheme}
          tooltipPosition={tooltipPosition}
        />
        <TooltipIconButton
          theme={activeTool === "KEYBOARD" ? activeButtonTheme : undefined}
          value="KEYBOARD"
          onClick={() => onToolClicked("KEYBOARD")}
          icon="keyboard"
          tooltip={
            <FormattedMessage {...messages["keyboard-shortcuts-tooltip"]} />
          }
          tooltipPosition={tooltipPosition}
        />
        {bigZoomIsEnabled ? (
          <TooltipIconButton
            theme={
              bigZoom && zoomValue !== "ZOOM" ? activeButtonTheme : undefined
            }
            value="ZOOM_100"
            onClick={() => {
              if (zoomValue === "ZOOM_100") {
                onZoomToolChanged("ZOOM_RESET");
              } else {
                onZoomToolChanged("ZOOM_100");
              }
            }}
            icon={bigZoom ? "youtube_searched_for" : "crop_original"}
            tooltip={
              zoomValue === "ZOOM_100" ? (
                <FormattedMessage {...messages["reset-zoom"]} />
              ) : (
                <FormattedMessage {...messages["zoom-100"]} />
              )
            }
            tooltipPosition={tooltipPosition}
          />
        ) : null}
        <TooltipIconButton
          theme={activeTool === "ZOOM" ? activeButtonTheme : undefined}
          value="ZOOM"
          onClick={() => {
            onZoomToolChanged("ZOOM");
            onToolClicked("ZOOM");
          }}
          icon="zoom_in"
          tooltip={<FormattedMessage {...messages["zoom-tool-tooltip"]} />}
          tooltipPosition={tooltipPosition}
        />
        <TooltipIconButton
          value="PAN"
          theme={activeTool === "PAN" ? activeButtonTheme : undefined}
          onClick={() => onToolClicked("PAN")}
          icon="pan_tool"
          className={style.smallIcon}
          tooltip={<FormattedMessage {...messages["pan-tool-tooltip"]} />}
          tooltipPosition={tooltipPosition}
        />
        <TooltipIconButton
          value="fullscreen"
          onClick={() => onToolClicked("fullscreen")}
          icon="fullscreen"
          tooltip={
            <FormattedMessage {...messages["fullscreen-mode-tooltip"]} />
          }
          tooltipPosition={tooltipPosition}
        />
        {downloadableComponents && downloadableComponents.length ? (
          <DownloadButton
            downloadableComponents={downloadableComponents}
            tooltipPosition={tooltipPosition}
          />
        ) : null}
      </div>
    </div>
  );
}

ImageControls.propTypes = {
  className: PropTypes.string,
  onToolClicked: PropTypes.func,
  activeTool: PropTypes.string,
  currentFrame: PropTypes.number,
  totalFrames: PropTypes.number,
  onChangeFrame: PropTypes.func,
  downloadableComponents: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.node.isRequired,
      url: PropTypes.string.isRequired,
    })
  ),
  onClearAnnotation: PropTypes.func,
  tooltipPosition: PropTypes.string,
  onZoomToolChanged: PropTypes.func,
  zoomValue: PropTypes.string,
  bigZoomIsEnabled: PropTypes.bool,
  bigZoom: PropTypes.bool,
  isComparing: PropTypes.bool,
  canCompare: PropTypes.bool,
};

export default safeInjectIntl(ImageControls);
