// :copyright: Copyright (c) 2020 ftrack

import { defineMessages } from "react-intl";

const messages = defineMessages({
  "annotation-button-tooltip": {
    id: "ftrack-spark-components.player_toolbar.annotation-button-tooltip",
    description: 'Tooltip for "clear annotation button"',
    defaultMessage: "Clear annotations",
  },
  "pan-tool-tooltip": {
    id: "ftrack-spark-components.player_toolbar.pan-tool-tooltip",
    description: "Tooltip for Pan tool",
    defaultMessage: "Pan tool",
  },
  "zoom-tool-tooltip": {
    id: "ftrack-spark-components.player_toolbar.zoom-tool-tooltip",
    description: "Tooltip for Zoom tool",
    defaultMessage: "Zoom tool",
  },
  "keyboard-shortcuts-tooltip": {
    id: "ftrack-spark-components.player_toolbar.keyboard-shortcuts-tooltip",
    description: "Tooltip for keyboard shortcuts",
    defaultMessage: "Keyboard shortcuts",
  },
  "unmute-volume-tooltip": {
    id: "ftrack-spark-components.player_toolbar.unmute-volume-tooltip",
    description: "Tooltip for volume button when un-muting",
    defaultMessage: "Unmute",
  },
  "mute-volume-tooltip": {
    id: "ftrack-spark-components.player_toolbar.mute-volume-tooltip",
    description: "Tooltip for volume button when muting",
    defaultMessage: "Mute",
  },
  "fullscreen-mode-tooltip": {
    id: "ftrack-spark-components.player_toolbar.fullscreen-mode-tooltip",
    description: "Tooltip for fullscreen mode",
    defaultMessage: "Full screen",
  },
  "download-button-tooltip": {
    id: "ftrack-spark-components.player_toolbar.download-button-tooltip",
    description: "Tooltip for download button",
    defaultMessage: "Download media",
  },
  "pause-tooltip": {
    id: "ftrack-spark-components.player_toolbar.pause-tooltip",
    description: "Tooltip for play button when playing",
    defaultMessage: "Pause",
  },
  "play-tooltip": {
    id: "ftrack-spark-components.player_toolbar.play-tooltip",
    description: "Tooltip for play button when paused",
    defaultMessage: "Play",
  },
  "time-format-tooltip": {
    id: "ftrack-spark-components.player_toolbar.time-format-tooltip",
    description: "Tooltip for time format menu",
    defaultMessage: "Time format",
  },
  "time-format-standard": {
    id: "ftrack-spark-components.player_toolbar.time-format-standard",
    description: "Show standard time format",
    defaultMessage: "Standard",
  },
  "time-format-frames": {
    id: "ftrack-spark-components.player_toolbar.time-format-frames",
    description: "Show frames as time format",
    defaultMessage: "Frames",
  },
  "time-format-timecode": {
    id: "ftrack-spark-components.player_toolbar.time-format-timecode",
    description: "Show timecode as time format",
    defaultMessage: "Timecode",
  },
  "time-format-title": {
    id: "ftrack-spark-components.player_toolbar.time-format-title",
    description: "Title for time format menu",
    defaultMessage: "Time format",
  },
  "zoom-100": {
    id: "ftrack-spark-components.player_toolbar.zoom-100",
    defaultMessage: "Zoom to 100%",
  },
  "reset-zoom": {
    id: "ftrack-spark-components.player_toolbar.reset-zoom",
    defaultMessage: "Reset zoom",
  },
  "laser-tool": {
    id: "ftrack-spark-components.player_toolbar.laser-tool",
    description: "Label for Laser tool",
    defaultMessage: "Laser tool",
  },
  "compare-mode": {
    id: "ftrack-spark-components.player_toolbar.compare-mode",
    description: "Label for Compare mode",
    defaultMessage: "Compare",
  },
});

export default messages;
