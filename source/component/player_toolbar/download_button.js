import { IconButton, Button } from "react-toolbox/lib/button";
import { MenuItem } from "react-toolbox/lib/menu";
import withTooltip from "react-toolbox/lib/tooltip";
import { intlShape } from "react-intl";
import PropTypes from "prop-types";
import ButtonMenu from "../button_menu_old";
import messages from "./messages";
import style from "./style.scss";
import safeInjectIntl from "../util/hoc/safe_inject_intl";

const TooltipDownloadButton = withTooltip(IconButton);

function DownloadButton({
  downloadableComponents,
  tooltipPosition,
  intl,
  label,
}) {
  const button = label ? (
    <Button icon="get_app" label={label} />
  ) : (
    <TooltipDownloadButton
      icon="get_app"
      tooltip={intl.formatMessage(messages["download-button-tooltip"])}
      tooltipPosition={tooltipPosition}
    />
  );

  return (
    <ButtonMenu
      display="inline-block"
      button={button}
      position="bottomRight"
      onSelect={(value) => {
        // eslint-disable-next-line no-undef
        window.open(value);
      }}
    >
      {downloadableComponents.map((comp) => (
        <MenuItem
          key={comp.value}
          value={comp.value}
          icon="insert_drive_file"
          caption={
            <div>
              <div>{comp.name}</div>
              {comp.description ? (
                <div className={style.downloadButtonDescription}>
                  {comp.description}
                </div>
              ) : null}
            </div>
          }
        />
      ))}
    </ButtonMenu>
  );
}

DownloadButton.propTypes = {
  downloadableComponents: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.node.isRequired,
      description: PropTypes.string,
      value: PropTypes.string.isRequired,
    })
  ),
  tooltipPosition: PropTypes.string,
  intl: intlShape,
  label: PropTypes.string,
};

DownloadButton.defaultProps = {
  downloadableComponents: [],
  tooltipPosition: "bottom",
  label: "",
};

export default safeInjectIntl(DownloadButton);
