// :copyright: Copyright (c) 2019 ftrack

import PropTypes from "prop-types";
import { defineMessages, FormattedMessage } from "react-intl";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import ButtonMenu, { DropdownButton, SectionMenu } from "../button_menu_old";

const messages = defineMessages({
  "rate-heading": {
    id: "ftrack-spark-overview.playback-menu.rate-heading",
    description: "Heading for playback rate choices",
    defaultMessage: "Speed",
  },
  rate: {
    id: "ftrack-spark-overview.playback-menu.rate",
    description: "Generic menu option to diplay rate",
    defaultMessage: "{rate,number}x",
  },
  "rate-1x": {
    id: "ftrack-spark-overview.playback-menu.rate-1x",
    description: "Menu option for rate 1x",
    defaultMessage: "Normal",
  },
  "first-heading": {
    id: "ftrack-spark-overview.playback-menu.first-heading",
    description: "Heading for first part of menu.",
    defaultMessage: "Loop",
  },
  "loop-on": {
    id: "ftrack-spark-overview.playback-menu.loop-on",
    description: "Menu option for loop on",
    defaultMessage: "Loop on",
  },
  "loop-off": {
    id: "ftrack-spark-overview.playback-menu.loop-off",
    description: "Menu option for loop off.",
    defaultMessage: "Loop off",
  },
  "second-heading": {
    id: "ftrack-spark-overview.playback-menu.second-heading",
    description: "Heading for second part of menu",
    defaultMessage: "Playback",
  },
  "play-one": {
    id: "ftrack-spark-overview.playback-menu.play-one",
    description: "Menu option for selecting to play one clip.",
    defaultMessage: "Play one",
  },
  "play-all": {
    id: "ftrack-spark-overview.playback-menu.play-all",
    description: "Menu option for selecting to play all clips in that Review.",
    defaultMessage: "Play all",
  },
  "play-selection": {
    id: "ftrack-spark-components.playback-menu.play-selection",
    description: "Menu option for selecting specific clips",
    defaultMessage: "Play selection",
  },
  "playback-mode-tooltip": {
    id: "ftrack-spark-components.playback-menu.playback-mode-tooltip",
    description: "Tooltip for changing playback mode",
    defaultMessage: "Playback options",
  },
});

function PlaybackMenu(props) {
  const {
    playbackRate,
    onPlaybackRateChanged,
    loop,
    onLoopToggle,
    onPlaybackModeChanged,
    playbackMode,
    playAllSequenceMediaIds,
    tooltipPosition,
    ...rest
  } = props;

  const list = [
    {
      title: <FormattedMessage {...messages["rate-heading"]} />,
      options: [
        {
          caption: (
            <FormattedMessage {...messages.rate} values={{ rate: 0.25 }} />
          ),
          selectedIcon: "check",
          value: 0.25,
          selected: playbackRate === 0.25,
        },
        {
          caption: (
            <FormattedMessage {...messages.rate} values={{ rate: 0.5 }} />
          ),
          selectedIcon: "check",
          value: 0.5,
          selected: playbackRate === 0.5,
        },
        {
          caption: (
            <FormattedMessage {...messages.rate} values={{ rate: 0.75 }} />
          ),
          selectedIcon: "check",
          value: 0.75,
          selected: playbackRate === 0.75,
        },
        {
          caption: <FormattedMessage {...messages["rate-1x"]} />,
          selectedIcon: "check",
          value: 1,
          selected: playbackRate === 1,
        },
        {
          caption: (
            <FormattedMessage {...messages.rate} values={{ rate: 1.25 }} />
          ),
          selectedIcon: "check",
          value: 1.25,
          selected: playbackRate === 1.25,
        },
        {
          caption: (
            <FormattedMessage {...messages.rate} values={{ rate: 1.5 }} />
          ),
          selectedIcon: "check",
          value: 1.5,
          selected: playbackRate === 1.5,
        },
        {
          caption: (
            <FormattedMessage {...messages.rate} values={{ rate: 1.75 }} />
          ),
          selectedIcon: "check",
          value: 1.75,
          selected: playbackRate === 1.75,
        },
        {
          caption: <FormattedMessage {...messages.rate} values={{ rate: 2 }} />,
          selectedIcon: "check",
          value: 2,
          selected: playbackRate === 2,
        },
      ],
    },
    {
      title: <FormattedMessage {...messages["first-heading"]} />,
      options: [
        {
          caption: <FormattedMessage {...messages["loop-on"]} />,
          selectedIcon: "check",
          value: "loop-on",
          selected: loop,
        },
        {
          caption: <FormattedMessage {...messages["loop-off"]} />,
          selectedIcon: "check",
          value: "loop-off",
          selected: !loop,
        },
      ],
    },
  ];

  if (playAllSequenceMediaIds && playAllSequenceMediaIds.length > 1) {
    list.push({
      title: <FormattedMessage {...messages["second-heading"]} />,
      options: [
        {
          caption: <FormattedMessage {...messages["play-one"]} />,
          selectedIcon: "check",
          value: "PlayOne",
          selected: playbackMode === "PlayOne",
        },
        {
          caption: <FormattedMessage {...messages["play-all"]} />,
          selectedIcon: "check",
          value: "PlayAll",
          selected: playbackMode === "PlayAll",
        },
        {
          caption: <FormattedMessage {...messages["play-selection"]} />,
          selectedIcon: "check",
          value: "PlaySelection",
          selected: playbackMode === "PlaySelection",
        },
      ],
    });
  }

  return (
    <ButtonMenu
      display="inline-block"
      position="bottomLeft"
      button={
        <DropdownButton
          icon="settings"
          tooltip={<FormattedMessage {...messages["playback-mode-tooltip"]} />}
          tooltipPosition={tooltipPosition}
        />
      }
      {...rest}
    >
      <SectionMenu
        sections={list}
        onSelectionChange={(value, selected) => {
          if (Number.isFinite(value)) {
            onPlaybackRateChanged(value);
          } else if (
            value === "PlayOne" ||
            value === "PlayAll" ||
            value === "PlaySelection"
          ) {
            onPlaybackModeChanged(value);
          } else if (value === "loop-on") {
            onLoopToggle(selected);
          } else if (value === "loop-off") {
            onLoopToggle(!selected);
          }
        }}
      />
    </ButtonMenu>
  );
}

PlaybackMenu.propTypes = {
  playbackRate: PropTypes.number,
  onPlaybackRateChanged: PropTypes.func,
  loop: PropTypes.bool,
  onLoopToggle: PropTypes.func,
  onPlaybackModeChanged: PropTypes.func,
  playbackMode: PropTypes.string,
  playAllSequenceMediaIds: PropTypes.arrayOf(PropTypes.string),
  tooltipPosition: PropTypes.string,
};

export default safeInjectIntl(PlaybackMenu);
