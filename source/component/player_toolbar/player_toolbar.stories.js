/* eslint-disable react/prop-types */
// :copyright: Copyright (c) 2017 ftrack
import centered from "@storybook/addon-centered/react";
import compose from "recompose/compose";
import withState from "recompose/withState";

import PlayerToolbar from ".";
import VideoControls from "./video_controls";
import ImageControls from "./image_controls";

export default {
  title: "PlayerToolbar",
  decorators: [centered],
};

const DOWNLOAD_COMPONENT_URL =
  "https://www.ftrack.com/wp-content/uploads/2016/08/ftrack-logo-dark-x1.png";

function VideoToolbar(props) {
  return (
    <PlayerToolbar
      totalFrames={props.totalFrames}
      currentFrame={props.currentFrame}
      onChangeFrame={props.onChangeFrame}
      loopIn={props.loopIn || -1}
      loopOut={props.loopOut || -1}
      sequenceMemberFrameOuts={props.sequenceMemberFrameOuts}
    >
      <VideoControls
        currentFrame={props.currentFrame}
        totalFrames={props.totalFrames}
        frameRate={props.frameRate}
        timeFormat={props.timeFormat}
        onChangeTimeFormat={props.onChangeTimeFormat}
        isPlaying={props.isPlaying}
        onTogglePlay={props.onTogglePlay}
        downloadableComponents={props.downloadableComponents}
        onLoopToggle={props.onLoopToggle}
        loop={props.loop}
        onPlaybackModeChanged={props.onPlaybackModeChanged}
        playbackMode={props.playbackMode}
        onPlaybackRateChanged={props.onPlaybackRateChanged}
        playbackRate={props.playbackRate}
        bigZoomIsEnabled={props.bigZoomIsEnabled}
        onVolumeChanged={props.onVolumeChanged}
        volume={props.volume}
      />
    </PlayerToolbar>
  );
}

const VideoToolbarTest = compose(
  withState("currentFrame", "onChangeFrame", 0),
  withState("timeFormat", "onChangeTimeFormat"),
  withState("isPlaying", "onTogglePlay", false),
  withState("loop", "onLoopToggle", true),
  withState("playbackMode", "onPlaybackModeChanged", "PlayOne"),
  withState("volume", "onVolumeChanged", 0),
  withState("playbackRate", "onPlaybackRateChanged", 1)
)(VideoToolbar);

function ImageToolbar(props) {
  return (
    <PlayerToolbar
      totalFrames={props.totalFrames}
      currentFrame={props.currentFrame}
      firstFrame={props.firstFrame}
      onChangeFrame={props.onChangeFrame}
    >
      <ImageControls
        totalFrames={props.totalFrames}
        currentFrame={props.currentFrame}
        onChangeFrame={props.onChangeFrame}
        downloadableComponents={props.downloadableComponents}
        bigZoomIsEnabled={props.bigZoomIsEnabled}
      />
    </PlayerToolbar>
  );
}

const ImageToolbarTest = compose(withState("currentFrame", "onChangeFrame", 1))(
  ImageToolbar
);

function PlayerTest({ children }) {
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <div
        style={{
          width: "640px",
          height: "360px",
          backgroundColor: "#30363C",
        }}
      />
      {children}
    </div>
  );
}

export const Video2Minutes = () => (
  <PlayerTest>
    <VideoToolbarTest totalFrames={3000} frameRate={25} />
  </PlayerTest>
);

Video2Minutes.storyName = "Video (2 minutes)";

export const Video3Frames = () => (
  <PlayerTest>
    <VideoToolbarTest totalFrames={3} frameRate={25} />
  </PlayerTest>
);

Video3Frames.storyName = "Video (3 frames)";

export const VideoInAndOutPoints = () => (
  <PlayerTest>
    <VideoToolbarTest totalFrames={15} loopIn={7} loopOut={14} frameRate={25} />
  </PlayerTest>
);

VideoInAndOutPoints.storyName = "Video (in and out points)";

export const VideoWithBigZoom = () => (
  <PlayerTest>
    <VideoToolbarTest bigZoomIsEnabled />
  </PlayerTest>
);

VideoWithBigZoom.storyName = "Video with big zoom";

const listOfSequenceDurations = [11, 33, 66];

export const VideoWithPlaybackSequence = () => (
  <PlayerTest>
    <VideoToolbarTest
      totalFrames={130}
      loop
      sequenceMemberFrameOuts={listOfSequenceDurations}
    />
  </PlayerTest>
);

VideoWithPlaybackSequence.storyName = "Video with playback sequence";

export const ImageSinglePage = () => (
  <PlayerTest>
    <ImageToolbarTest totalFrames={1} />
  </PlayerTest>
);

ImageSinglePage.storyName = "Image (single page)";

export const Image3Pages = () => (
  <PlayerTest>
    <ImageToolbarTest totalFrames={3} />
  </PlayerTest>
);

Image3Pages.storyName = "Image (3 pages)";

export const Image25Pages = () => (
  <PlayerTest>
    <ImageToolbarTest totalFrames={25} />
  </PlayerTest>
);

Image25Pages.storyName = "Image (25 pages)";

export const ImageWithBigZoom = () => (
  <PlayerTest>
    <ImageToolbarTest bigZoomIsEnabled totalFrames={25} />
  </PlayerTest>
);

ImageWithBigZoom.storyName = "Image with big zoom";

export const DownloadFilesImages = () => (
  <PlayerTest>
    <ImageToolbarTest
      downloadableComponents={[
        {
          name: "Image",
          url: DOWNLOAD_COMPONENT_URL,
        },
      ]}
    />
  </PlayerTest>
);

DownloadFilesImages.storyName = "Download files (images)";

export const DownloadFilesVideo = () => (
  <PlayerTest>
    <VideoToolbarTest
      downloadableComponents={[
        {
          name: "ftrack-testing",
          url: DOWNLOAD_COMPONENT_URL,
        },
      ]}
    />
  </PlayerTest>
);

DownloadFilesVideo.storyName = "Download files (video)";

export const DownloadFilesDescription = () => (
  <PlayerTest>
    <VideoToolbarTest
      downloadableComponents={[
        {
          name: "ftrack-testing",
          description: "MP4 2000x1000",
          url: DOWNLOAD_COMPONENT_URL,
        },
      ]}
    />
  </PlayerTest>
);

DownloadFilesDescription.storyName = "Download files (description)";

export const DownloadFilesMultiple = () => (
  <PlayerTest>
    <VideoToolbarTest
      downloadableComponents={[
        {
          name: "Foo",
          description: "MP4 - 2000x1000",
          url: DOWNLOAD_COMPONENT_URL,
        },
        {
          name: "Bar",
          url: DOWNLOAD_COMPONENT_URL,
        },
        {
          name: "Baz",
          description: "MP4 - 2000x1000",
          url: DOWNLOAD_COMPONENT_URL,
        },
        {
          name: "Foo bar",
          url: DOWNLOAD_COMPONENT_URL,
        },
      ]}
    />
  </PlayerTest>
);

DownloadFilesMultiple.storyName = "Download files (multiple)";
