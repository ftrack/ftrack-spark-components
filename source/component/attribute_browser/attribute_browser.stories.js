import { Component } from "react";

import { action } from "@storybook/addon-actions";
import { withState } from "recompose";
import { Provider as ReduxProvider } from "react-redux";
// eslint-disable-next-line
import centered from "@storybook/addon-centered/react";

import configureStore from "../../story/configure_store.js";
import getSession from "../../story/get_session";

import { SparkProvider } from "../util/hoc";
import { AttributeList, SelectedAttributes } from ".";
import getAttributes from "../dataview/util";

function Provider({ story, store, session }) {
  return (
    <SparkProvider session={session}>
      <ReduxProvider store={store}>{story}</ReduxProvider>
    </SparkProvider>
  );
}

const store = configureStore((state = {}) => state, [], getSession());

export default {
  title: "Attribute browser",
  decorators: [
    (Story) => (
      <Provider story={<Story />} store={store} session={getSession()} />
    ),
  ],
};

function AttributeBrowser_({
  attributeGroups,
  attributes,
  selected,
  onChange,
  disabled,
}) {
  return (
    <div style={{ display: "flex" }}>
      <AttributeList
        attributeGroups={attributeGroups}
        selected={selected}
        onChange={onChange}
        enableSelectionModel
        disabled={disabled}
      />
      <SelectedAttributes
        attributes={attributes}
        selected={selected}
        onChange={onChange}
      />
    </div>
  );
}

const AttributesBrowser = withState(
  "selected",
  "onChange",
  []
)(AttributeBrowser_);

class AttributeBrowserTest extends Component {
  constructor() {
    super();
    this.state = { attributeSource: null };
  }

  componentDidMount() {
    const { session, base, relations } = this.props;
    session.initializing
      .then(() => getAttributes(base, relations, session))
      .then((attributeSource) => {
        this.setState({
          attributeSource,
        });
      });
  }

  render() {
    const { attributeSource } = this.state;

    if (!attributeSource) {
      return <span>waiting...</span>;
    }

    const { attributes, groups } = attributeSource;
    return (
      <AttributesBrowser
        attributes={attributes}
        attributeGroups={groups}
        disabled={this.props.disabled}
      />
    );
  }
}

class AttributeListTest extends Component {
  constructor() {
    super();
    this.state = { attributeSource: null };
  }

  componentDidMount() {
    const { session, base, relations } = this.props;
    session.initializing
      .then(() => getAttributes(base, relations, session))
      .then((attributeSource) => {
        this.setState({
          attributeSource,
        });
      });
  }

  render() {
    const { attributeSource } = this.state;
    const { enableSelectionModel } = this.props;

    if (!attributeSource) {
      return <span>waiting...</span>;
    }

    return (
      <AttributeList
        attributeGroups={attributeSource.groups}
        selected={[]}
        onChange={action("Attribute clicked")}
        enableSelectionModel={enableSelectionModel}
      />
    );
  }
}

export const Task = (args) => (
  <div style={{ height: 400, overflow: "auto", backgroundColor: "#EEE" }}>
    <AttributeBrowserTest session={getSession()} {...args} />
  </div>
);

Task.args = {
  relations: ["status", "status.state", "type"],
  base: "Task",
};

Task.parameters = {
  info: "Browse attributes task",
};

export const TaskWithDisabledName = (args) => (
  <div style={{ height: 400, overflow: "auto", backgroundColor: "#EEE" }}>
    <AttributeBrowserTest session={getSession()} {...args} />
  </div>
);

TaskWithDisabledName.args = {
  ...Task.args,
  disabled: ["name"],
};

TaskWithDisabledName.storyName = "Task with disabled name";

TaskWithDisabledName.parameters = {
  info: "Browse attributes task with disabled name",
};

export const TaskDisableSelection = (args) => (
  <div style={{ height: 400, overflow: "auto", backgroundColor: "#EEE" }}>
    <AttributeListTest session={getSession()} {...args} />
  </div>
);

TaskDisableSelection.args = {
  ...Task.args,
  enableSelectionModel: false,
};

TaskDisableSelection.storyName = "Task disable selection";

TaskDisableSelection.parameters = {
  info: "Browse attributes task disable selection model",
};

export const ShotCustomAttributes = (args) => (
  <div style={{ height: 400, overflow: "auto", backgroundColor: "#EEE" }}>
    <AttributeBrowserTest session={getSession()} {...args} />
  </div>
);

ShotCustomAttributes.args = {
  ...Task.args,
  base: "Shot",
};

ShotCustomAttributes.storyName = "Shot + custom attributes";

ShotCustomAttributes.parameters = {
  info: "Browse attributes shot + custom attributes",
};

export const AssetVersionsCustomAttributes = (args) => (
  <div style={{ height: 400, overflow: "auto", backgroundColor: "#EEE" }}>
    <AttributeBrowserTest session={getSession()} {...args} />
  </div>
);

AssetVersionsCustomAttributes.args = {
  base: "AssetVersion",
  relations: ["task"],
};

AssetVersionsCustomAttributes.storyName = "Asset versions + custom attributes";

AssetVersionsCustomAttributes.parameters = {
  info: "Browse attributes asset version + custom attributes",
};

const StatefulSelectedAttributes = withState("selected", "onChange", [
  "1",
  "2",
  "3",
])(SelectedAttributes);

export const AttributeItemWithHint = (args) => (
  <div style={{ backgroundColor: "#EEE", maxWidth: "300px" }}>
    <StatefulSelectedAttributes {...args} />
  </div>
);

AttributeItemWithHint.args = {
  selected: ["1", "2", "3"],
  attributes: {
    1: {
      key: "1",
      label: "Attribute 1",
    },
    2: {
      key: "2",
      label: "Attribute 2 is extremely long and should be cut off",
    },
    3: {
      key: "3",
      label: "Attribute 3 is a long one",
    },
  },
  attributeHints: ["title", "subtitle"],
};

AttributeItemWithHint.storyName = "Attribute item with hint";

AttributeItemWithHint.parameters = {
  info: "Show attribute item with hint",
};

export const AttributeItemWithMoveUpAndDown = (args) => (
  <div style={{ backgroundColor: "#EEE", maxWidth: "300px" }}>
    <StatefulSelectedAttributes {...args} />
  </div>
);

AttributeItemWithMoveUpAndDown.args = {
  attributes: {
    1: {
      key: "1",
      label: "Attribute 1",
    },
    2: {
      key: "2",
      label: "Attribute 2 is extremely long and should be cut off",
    },
    3: {
      key: "3",
      label: "Attribute 3 is a long one",
    },
  },
};

AttributeItemWithMoveUpAndDown.storyName =
  "Attribute item with move up and down";

AttributeItemWithMoveUpAndDown.parameters = {
  info: "Show attribute item with move up and down",
};
