..
    :copyright: Copyright (c) 2018 ftrack

#################
Attribute browser
#################


AttributeBrowser
================

The Attribute browser component shows a list of selectable attributes and a
list of selected.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
attributes                     List of attributes.
attributeGroups                List of attribute groups.
selected                       List of selected attribute keys.
onChange                       Callback when attribute selection changes.


SelectedAttributes
==================

The selected attributes component shows a list of selected attributes and a
allows the sure to remove them.

Depends on react-flip-move.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
attributes                     List of attributes.
selected                       List of selected attribute keys.
onChange                       Callback when attribute selection changes.
attributeHints     []          Optional list of hints on how attributes are used.

AttributeList
=============

The attributes list component shows a list of selectable attributes.

==================      =========== =========================================
Prop                    Default         Description
==================      =========== =========================================
enableSelectionModel    false       Set to true to allow multiple selections.
attributeGroups                     List of attribute groups.
selected                            List of selected attribute keys.
