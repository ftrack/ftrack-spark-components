// :copyright: Copyright (c) 2017 ftrack
/* eslint-disable react/no-multi-comp */
import PropTypes from "prop-types";

import { Component } from "react";
import { List, ListCheckbox, ListItem } from "react-toolbox/lib/list";
import { IconButton } from "react-toolbox/lib/button";
import { FontIcon } from "react-toolbox/lib/font_icon";
import classNames from "classnames";
import { withState, withHandlers, compose } from "recompose";
import FlipMove from "react-flip-move";
import { intlShape, FormattedMessage, defineMessages } from "react-intl";

import { TRANSITION_DURATION } from "../util/constant";
import {
  AttributeLabel,
  AttributeGroupLabel,
  sortLocalisedAttributes,
  sortLocalisedGroups,
} from "../dataview/attribute";
import {
  attributeGroupShape,
  attributeShape,
  isCustomAttribute,
} from "../dataview/util";
import listItemTheme from "./list_item_theme.scss";
import listItemGroupTheme from "./list_item_group_theme.scss";
import listTheme from "./list_theme.scss";
import style from "./style.scss";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import { withSession } from "../util/hoc";
import { Entity } from "../formatter/entityUtils";

const messages = defineMessages({
  "unknown-attribute": {
    defaultMessage: "Unknown attribute",
    description: "Unknown attribute label",
    id: "ftrack-spark-components.attribute-browser.unknown-attribute",
  },
  attributes: {
    defaultMessage: "Attributes",
    description: "Attribute label",
    id: "ftrack-spark-components.attribute-browser.attributes",
  },
  "custom-attributes": {
    defaultMessage: "Custom attributes",
    description: "Custom attributes label",
    id: "ftrack-spark-components.attribute-browser.custom-attributes",
  },
  related: {
    defaultMessage: "Related",
    description: "Custom attributes label",
    id: "ftrack-spark-components.attribute-browser.related",
  },
});

const ListItemWithHandlers = withHandlers({
  onClick: (props) => () => props.onClick(props.itemKey),
})(ListItem);

const ListCheckboxWithHandlers = withHandlers({
  onChange: (props) => (value) => props.onChange(props.itemKey, value),
})(ListCheckbox);

export class AttributeList_ extends Component {
  constructor() {
    super();

    this.onExpandClick = this.onExpandClick.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onClick = this.onClick.bind(this);
  }

  onExpandClick(key) {
    const { setExpanded, expanded } = this.props;
    setExpanded(Object.assign({}, expanded, { [key]: !expanded[key] }));
  }

  onChange(key, value) {
    const { onChange, selected } = this.props;
    if (value) {
      onChange([...selected, key]);
    } else {
      onChange(selected.filter((item) => item !== key));
    }
  }

  onClick(key) {
    const { onChange } = this.props;
    onChange(key);
  }

  render() {
    const {
      attributeGroups,
      attributeTransform,
      className,
      disabled,
      enableSelectionModel,
      expanded,
      intl,
      selected,
      session,
    } = this.props;
    const sortedGroups = sortLocalisedGroups(attributeGroups, session, intl);
    const items = sortedGroups.reduce((accumulator, group, index) => {
      const isExpanded = index === 0 || expanded[group.key];
      const isInExpandedGroup = isExpanded && index !== 0;
      const listItemClassName = classNames({
        [style["list-item-indent"]]: isInExpandedGroup,
      });

      if (index === 1) {
        accumulator.push(
          <ListItem
            disabled
            key="Related"
            theme={listItemGroupTheme}
            caption={<FormattedMessage {...messages.related} />}
          />
        );
      }

      if (index > 0) {
        const caption = (
          <div className={style["attribute-group-label"]}>
            <AttributeGroupLabel path={group.path} label={group.groupLabel} />
          </div>
        );
        // Do not label first group.
        accumulator.push(
          <ListItemWithHandlers
            key={group.key}
            theme={listItemGroupTheme}
            className={style["list-group-item"]}
            leftIcon={
              isExpanded ? "keyboard_arrow_down" : "keyboard_arrow_right"
            }
            itemKey={group.key}
            caption={caption}
            onClick={this.onExpandClick}
          />
        );
      }

      const schema = session.schemas.find(
        (candidate) => candidate.id === group.model
      );
      const attributes = sortLocalisedAttributes(
        group.attributes,
        schema,
        intl
      );

      if (isExpanded) {
        accumulator.push(
          ...attributes.reduce((attributeAccumulator, item, attributeIndex) => {
            const caption = (
              <span>
                <FontIcon value={item.icon} className={style.icon} />
                <AttributeLabel
                  attribute={item}
                  transform={attributeTransform}
                />
                {item.key.endsWith("$entities") && (
                  <AttributeLabel
                    attribute={{
                      key: item.key,
                      label: item.groupLabel || Entity[item.model]?.name,
                    }}
                    transform="uppercase"
                    className={style["attribute-small-margin"]}
                  />
                )}
              </span>
            );
            if (
              attributeIndex > 0 &&
              attributes[attributeIndex - 1].key.endsWith("$entities")
            ) {
              attributeAccumulator.push(
                <ListItem
                  disabled
                  key={`${item.key}-attributes-${attributeIndex}`}
                  theme={listItemGroupTheme}
                  className={listItemClassName}
                  caption={<FormattedMessage {...messages["attributes"]} />}
                />
              );
            }

            if (
              attributeIndex > 0 &&
              isCustomAttribute(item) &&
              !isCustomAttribute(attributes[attributeIndex - 1])
            ) {
              // Custom attributes are at the end. Add a heading.
              attributeAccumulator.push(
                <ListItem
                  disabled
                  key={`${item.key}-custom-attributes-${attributeIndex}`}
                  theme={listItemGroupTheme}
                  className={listItemClassName}
                  caption={
                    <FormattedMessage {...messages["custom-attributes"]} />
                  }
                />
              );
            }

            if (enableSelectionModel) {
              attributeAccumulator.push(
                <ListCheckboxWithHandlers
                  key={item.key}
                  theme={listItemTheme}
                  className={listItemClassName}
                  checked={selected.indexOf(item.key) !== -1}
                  disabled={disabled.indexOf(item.key) !== -1}
                  caption={caption}
                  itemKey={item.key}
                  onChange={this.onChange}
                />
              );
            } else {
              attributeAccumulator.push(
                <ListItemWithHandlers
                  key={item.key}
                  theme={listItemTheme}
                  className={listItemClassName}
                  disabled={disabled.indexOf(item.key) !== -1}
                  caption={caption}
                  legend={item.description}
                  itemKey={item.key}
                  onClick={this.onClick}
                />
              );
            }

            return attributeAccumulator;
          }, [])
        );
      }

      return accumulator;
    }, []);

    return (
      <List selectable className={className} theme={listTheme}>
        {items}
      </List>
    );
  }
}

AttributeList_.propTypes = {
  enableSelectionModel: PropTypes.bool,
  attributeGroups: PropTypes.arrayOf(attributeGroupShape).isRequired,
  selected: PropTypes.arrayOf(PropTypes.string).isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  expanded: PropTypes.object.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  session: PropTypes.object.isRequired,
  setExpanded: PropTypes.func.isRequired,
  intl: intlShape.isRequired,
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string,
  disabled: PropTypes.arrayOf(PropTypes.string),
  attributeTransform: PropTypes.oneOf(["capitalize", "uppercase"]),
};

AttributeList_.defaultProps = {
  enableSelectionModel: false,
  selected: [],
  disabled: [],
};

export const AttributeList = compose(
  withState("expanded", "setExpanded", {}),
  safeInjectIntl,
  withSession
)(AttributeList_);

const AttributeIconButton = withHandlers({
  onClick: (props) => () => props.onClick(props.attribute),
})(IconButton);

// Written as class to be compatible with react-flip-move
// eslint-disable-next-line react/prefer-stateless-function
export class AttributeItem extends Component {
  render() {
    const {
      hint,
      attribute,
      onMoveUpClick,
      onMoveDownClick,
      onRemoveClick,
      disableMoveUp,
      disableMoveDown,
      attributeKey,
    } = this.props;
    const labelElement = attribute ? (
      <AttributeLabel attribute={attribute} enableGroup />
    ) : (
      <FormattedMessage {...messages["unknown-attribute"]} />
    );

    return (
      <ListItem
        theme={listItemTheme}
        className={style["attribute-item"]}
        caption={
          <div className={style["attribute-item-caption"]}>
            <div className={style["attribute-item-label"]}>{labelElement}</div>
            <div className={style["attribute-item-hint"]}>{hint}</div>
          </div>
        }
        rightActions={[
          <AttributeIconButton
            key="up"
            icon="keyboard_arrow_up"
            onClick={onMoveUpClick}
            attribute={attributeKey}
            disabled={disableMoveUp}
          />,
          <AttributeIconButton
            down="down"
            icon="keyboard_arrow_down"
            onClick={onMoveDownClick}
            attribute={attributeKey}
            disabled={disableMoveDown}
          />,
          <AttributeIconButton
            down="remove"
            icon="clear"
            onClick={onRemoveClick}
            attribute={attributeKey}
          />,
        ]}
      />
    );
  }
}

AttributeItem.propTypes = {
  hint: PropTypes.string,
  onMoveUpClick: PropTypes.func.isRequired,
  onMoveDownClick: PropTypes.func.isRequired,
  onRemoveClick: PropTypes.func.isRequired,
  attribute: attributeShape.isRequired,
  disableMoveUp: PropTypes.bool.isRequired,
  disableMoveDown: PropTypes.bool.isRequired,
  attributeKey: PropTypes.string.isRequired,
};

class SelectedAttributes_ extends Component {
  constructor() {
    super();

    this.onRemove = this.onRemove.bind(this);
    this.onMoveUp = this.onMoveUp.bind(this);
    this.onMoveDown = this.onMoveDown.bind(this);
  }

  onRemove(attributeKey) {
    const { selected } = this.props;

    this.props.onChange([...selected].filter((item) => item !== attributeKey));
  }

  onMoveUp(attributeKey) {
    const { selected } = this.props;
    const index = selected.indexOf(attributeKey);

    const nextSelected = selected.reduce((accumulator, item, itemIndex) => {
      if (itemIndex === index) {
        return accumulator;
      }

      if (itemIndex === index - 1) {
        accumulator.push(attributeKey);
      }

      accumulator.push(item);

      return accumulator;
    }, []);

    this.props.onChange(nextSelected);
  }

  onMoveDown(attributeKey) {
    const { selected } = this.props;
    const index = selected.indexOf(attributeKey);

    const nextSelected = selected.reduce((accumulator, item, itemIndex) => {
      if (itemIndex === index) {
        return accumulator;
      }

      accumulator.push(item);

      if (itemIndex === index + 1) {
        accumulator.push(attributeKey);
      }

      return accumulator;
    }, []);

    this.props.onChange(nextSelected);
  }

  render() {
    const { selected, attributes, attributeHints } = this.props;

    const items = selected.map((key, index) => {
      const hint = attributeHints[index];
      const moveProps = {
        onMoveUpClick: this.onMoveUp,
        onMoveDownClick: this.onMoveDown,
        disableMoveUp: index === 0,
        disableMoveDown: index === selected.length - 1,
      };
      const attribute = attributes[key];
      return (
        <AttributeItem
          hint={hint}
          attribute={attribute}
          attributeKey={key}
          key={key}
          onRemoveClick={this.onRemove}
          {...moveProps}
        />
      );
    });

    return (
      <List>
        <FlipMove duration={TRANSITION_DURATION}>{items}</FlipMove>
      </List>
    );
  }
}

SelectedAttributes_.propTypes = {
  selected: PropTypes.arrayOf(PropTypes.string),
  onChange: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  attributes: PropTypes.object.isRequired,
  attributeHints: PropTypes.arrayOf(PropTypes.string),
};

SelectedAttributes_.defaultProps = {
  attributeHints: [],
  selected: [],
};

export const SelectedAttributes = safeInjectIntl(SelectedAttributes_);
