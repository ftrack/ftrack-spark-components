// :copyright: Copyright (c) 2020 ftrack

import { Fragment, useState } from "react";
import { action } from "@storybook/addon-actions";
import centered from "@storybook/addon-centered/react";

import {
  ReviewProSplashScreenWithText,
  ReviewProSplashScreenWithLink,
} from ".";

export default {
  title: "Splash screen",
  decorators: [centered],
};

const clicked = action("Closed dialog");
const ctaClicked = action("Redirected user and closed dialog");

const buttonStyle = {
  padding: "16px",
  background: "blue",
  borderRadius: "10px",
};
function ReviewProSplashScreenWithLinkStory() {
  const [isActive, setActive] = useState(false);
  return (
    <Fragment>
      <button style={buttonStyle} onClick={() => setActive(true)}>
        Get Review Pro!
      </button>
      <ReviewProSplashScreenWithLink
        active={isActive}
        onEscKeyDown={() => {
          setActive(false);
          clicked();
        }}
        onOverlayClick={() => {
          setActive(false);
          clicked();
        }}
        onAction={() => {
          ctaClicked();
          setActive(false);
        }}
      />
    </Fragment>
  );
}

function ReviewProSplashScreenWithTextStory() {
  const [isActive, setActive] = useState(false);
  return (
    <Fragment>
      <button style={buttonStyle} onClick={() => setActive(true)}>
        Get Review Pro!
      </button>
      <ReviewProSplashScreenWithText
        active={isActive}
        onEscKeyDown={() => {
          setActive(false);
          clicked();
        }}
        onOverlayClick={() => {
          setActive(false);
          clicked();
        }}
      />
    </Fragment>
  );
}

export const ReviewProSplashScreenWithCtaLink = () => (
  <ReviewProSplashScreenWithLinkStory />
);

ReviewProSplashScreenWithCtaLink.storyName =
  "Review Pro splash screen with CTA link";

export const ReviewProSplashScreenWithCtaText = () => (
  <ReviewProSplashScreenWithTextStory />
);

ReviewProSplashScreenWithCtaText.storyName =
  "Review Pro splash screen with CTA text";
