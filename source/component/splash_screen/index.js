// :copyright: Copyright (c) 2020 ftrack

export { default as ReviewProSplashScreenWithLink } from "./review_pro_splash_screen_with_link.js";
export { default as ReviewProSplashScreenWithText } from "./review_pro_splash_screen_with_text.js";
