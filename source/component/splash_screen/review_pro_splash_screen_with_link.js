// :copyright: Copyright (c) 2020 ftrack

import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import TourDialog from "../tour";
import messages from "./messages";
import image from "./images/ui-sync.png";

import style from "./review_pro_splash_screen_with_link.scss";

function ReviewProSplashScreenWithLink({ onAction, ...props }) {
  const steps = [
    {
      title: <FormattedMessage {...messages.title} />,
      text: (
        <div>
          <FormattedMessage {...messages["text-p1"]} />
          <div style={{ marginBottom: "16px" }} />
          <FormattedMessage {...messages["text-p2"]} />
          <div style={{ marginBottom: "8px" }} />
          <ul className={style.list}>
            <li className={style["list-item"]}>
              <FormattedMessage {...messages["text-li-1"]} />
            </li>
            <li className={style["list-item"]}>
              <FormattedMessage {...messages["text-li-2"]} />
            </li>
            <li className={style["list-item"]}>
              <FormattedMessage {...messages["text-li-3"]} />
            </li>
          </ul>
        </div>
      ),
      image,
      layout: "image_right",
      actions: [
        {
          text: <FormattedMessage {...messages["action-button"]} />,
          value: "close",
          primary: true,
          href: null,
          className: style.ctaButton,
        },
      ],
    },
  ];
  return <TourDialog active steps={steps} onAction={onAction} {...props} />;
}

ReviewProSplashScreenWithLink.propTypes = {
  onAction: PropTypes.func,
  active: PropTypes.bool,
};

export default safeInjectIntl(ReviewProSplashScreenWithLink);
