// :copyright: Copyright (c) 2019 ftrack
import PropTypes from "prop-types";
import moment from "moment";
import Button from "react-toolbox/lib/button";
import { defineMessages, FormattedMessage } from "react-intl";
import safeInjectIntl from "../util/hoc/safe_inject_intl";

import style from "./style.scss";
import darkTheme from "./dark_button_theme.scss";

const messages = defineMessages({
  "free-period-expired": {
    id: "ftrack-spark-components.trial-expiry-button.free-period-expired",
    defaultMessage: "Free period expired",
  },
  "free-period-ends-in": {
    id: "ftrack-spark-components.trial-expiry-button.free-period-ends-in",
    defaultMessage: "Free period ends in {numberOfDays} days",
  },
  "free-period-ends-today": {
    id: "ftrack-spark-components.trial-expiry-button.free-period-ends-today",
    defaultMessage: "Free period ends today",
  },
  "free-period-ends-tomorrow": {
    id: "ftrack-spark-components.trial-expiry-button.free-period-ends-tomorrow",
    defaultMessage: "Free period ends tomorrow",
  },
  "upgrade-now": {
    id: "ftrack-spark-components.trial-expiry-button.upgrade-now",
    defaultMessage: "Upgrade now",
  },
});

/*
 * Trial expiry button with text to indicate how much time is left of trial.
 *
 * Takes a *expiresAt* to indicate when trial expires. If *expiresAt* is:
 *
 *   -   In the past the button will state "Trial expired".
 *   -   Today's date it will state "Today".
 *   -   Tomorrow's date it will state "Tomorrow".
 *   -   For other durations it will state "duration" days where duration is rounded to
 *       the closest integer number.
 *
 */
function TrialExpiryButton({ variant, expiresAt, ...props }) {
  const buttonTheme = variant === "dark" ? darkTheme : undefined;
  let message = null;
  const msUntilExpiry = moment(expiresAt).diff(moment.utc());
  const localExpireAt = moment(expiresAt).local();
  const daysUntilExpiry = Math.round(
    moment(expiresAt).diff(moment.utc(), "hours") / 24.0
  );
  const isToday = localExpireAt.startOf("day").isSame(moment().startOf("day"));
  const isTomorrow = localExpireAt
    .startOf("day")
    .isSame(moment().startOf("day").add(1, "days"));

  if (msUntilExpiry >= 0) {
    if (isToday) {
      message = <FormattedMessage {...messages["free-period-ends-today"]} />;
    } else if (isTomorrow) {
      message = <FormattedMessage {...messages["free-period-ends-tomorrow"]} />;
    } else {
      message = (
        <FormattedMessage
          {...messages["free-period-ends-in"]}
          values={{ numberOfDays: daysUntilExpiry }}
        />
      );
    }
  } else {
    message = <FormattedMessage {...messages["free-period-expired"]} />;
  }

  return (
    <Button theme={buttonTheme} {...props}>
      {message}
      <label className={style.label}>
        <FormattedMessage {...messages["upgrade-now"]} />
      </label>
    </Button>
  );
}

TrialExpiryButton.propTypes = {
  expiresAt: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.string,
};

TrialExpiryButton.defaultProps = {};

export default safeInjectIntl(TrialExpiryButton);
