// :copyright: Copyright (c) 2018 ftrack
import centered from "@storybook/addon-centered/react";
import { action } from "@storybook/addon-actions";
import moment from "moment";

import TrialExpiryButton from ".";

export default {
  title: "Trial expiry button",
  decorators: [centered],
};

export const In4Days = () => (
  <TrialExpiryButton
    expiresAt={moment().add("days", 4).add("hour", 1).format()}
    onClick={action("Clicked")}
  />
);

export const In6DaysAnd23Hours = () => (
  <TrialExpiryButton
    expiresAt={moment().add("days", 6).add("hour", 23).format()}
    onClick={action("Clicked")}
  />
);

In6DaysAnd23Hours.storyName = "In 6 days and 23 hours";

export const In6DaysAnd11Hours = () => (
  <TrialExpiryButton
    expiresAt={moment().add("days", 6).add("hour", 11).format()}
    onClick={action("Clicked")}
  />
);

In6DaysAnd11Hours.storyName = "In 6 days and 11 hours";

export const OneHourAgo = () => (
  <TrialExpiryButton
    expiresAt={moment().add("hour", -1).format()}
    onClick={action("Clicked")}
  />
);

OneHourAgo.storyName = "One hour ago";

export const Yesterday = () => (
  <TrialExpiryButton
    expiresAt={moment().add("days", -1).add("hour", 1).format()}
    onClick={action("Clicked")}
  />
);

export const Today = () => (
  <TrialExpiryButton
    expiresAt={moment().endOf("day").add("hour", -1).format()}
    onClick={action("Clicked")}
  />
);

export const Tomorrow = () => (
  <TrialExpiryButton
    expiresAt={moment().endOf("day").add("day", 1).add("hour", -1).format()}
    onClick={action("Clicked")}
  />
);

export const Tomorrow0100TomorrowMorning = () => (
  <TrialExpiryButton
    expiresAt={moment().endOf("day").add("hour", 1).format()}
    onClick={action("Clicked")}
  />
);

Tomorrow0100TomorrowMorning.storyName = "Tomorrow (01:00 tomorrow morning)";

export const In74Days = () => (
  <TrialExpiryButton
    expiresAt={moment().add("days", 74).add("hour", 1).format()}
    onClick={action("Clicked")}
  />
);

In74Days.storyName = "In 74 days";

export const _74DaysAgo = () => (
  <TrialExpiryButton
    expiresAt={moment().add("days", -74).add("hour", 1).format()}
    onClick={action("Clicked")}
  />
);

_74DaysAgo.storyName = "74 days ago";

export const OnDarkBackground = () => (
  <div style={{ backgroundColor: "#30363c", padding: "10px 100px" }}>
    <TrialExpiryButton
      variant="dark"
      expiresAt={moment().add("days", 1).add("hour", 1).format()}
      onClick={action("Clicked")}
    />
  </div>
);

OnDarkBackground.storyName = "On dark background";
