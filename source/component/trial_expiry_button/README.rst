..
    :copyright: Copyright (c) 2019 ftrack

###################
Trial expiry button
###################

Trial expiry button, highlighting how many days left until expiry.
