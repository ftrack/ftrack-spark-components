// :copyright: Copyright (c) 2021 ftrack

import React from "react";
import centered from "@storybook/addon-centered/react";
import { action } from "@storybook/addon-actions";
import PropTypes from "prop-types";
import getFixedSizeDecorator from "../../story/get_fixed_size_decorator";
import SearchField from ".";

export default {
  title: "Search field",
  decorators: [getFixedSizeDecorator(), centered],
  argTypes: {
    matches: {
      control: { type: "range", step: 1, min: 0, max: 25 },
    },
    additionalMatchesAvailable: {
      control: { type: "boolean" },
    },
    isLoading: {
      control: { type: "boolean" },
    },
    disableClose: {
      control: { type: "boolean" },
    },
    large: {
      control: { type: "boolean" },
    },
    showInput: {
      control: { type: "boolean" },
    },
    placeholder: {
      control: { type: "text" },
    },
    debounce: {
      control: { type: "range" },
    },
  },
};

const Template = ({ ...args }) => (
  <SearchField
    onChange={action("onChange")}
    onKeyPress={action("key press")}
    onActiveChange={action("active change")}
    {...args}
  />
);

Template.propTypes = {
  counterText: PropTypes.string,
};

export const Default = Template.bind({});
Default.args = {
  matches: 0,
  additionalMatchesAvailable: false,
  isLoading: false,
  disableClose: false,
  placeholder: undefined,
  large: false,
  showInput: false,
  debounce: 0,
};

export const EntityPickerExample = Template.bind({});
EntityPickerExample.args = {
  matches: 0,
  additionalMatchesAvailable: false,
  isLoading: false,
  disableClose: true,
  resetIcon: "backspace",
  large: true,
  debounce: 600,
};

export const ReviewSearchExample = Template.bind({});
ReviewSearchExample.args = {
  matches: 0,
  additionalMatchesAvailable: false,
  isLoading: false,
  disableClose: false,
  placeholder: undefined,
  debounce: 300,
  large: true,
  showInput: true,
};
