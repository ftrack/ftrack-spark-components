// :copyright: Copyright (c) 2021 ftrack

import React, { useState, useEffect } from "react";
import PropTypes, { string } from "prop-types";
import { IconButton } from "react-toolbox/lib/button";
import FontIcon from "react-toolbox/lib/font_icon";
import ProgressBar from "react-toolbox/lib/progress_bar";
import classNames from "classnames";
import { ClickAwayListener } from "@mui/material";
import { intlShape, defineMessages } from "react-intl";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import debounce from "lodash/debounce";

import styles from "./styles.scss";
import { useKeyPress } from "../util/hooks";

const messages = defineMessages({
  placeholder: {
    id: "ftrack-spark-components.search-field.placeholder",
    defaultMessage: "Search",
  },
  matches: {
    id: "ftrack-spark-components.search-field.matches",
    defaultMessage: `{count, plural,
            one {match}
            other {matches}
        }`,
  },
});

const SearchField = ({
  onChange = () => {},
  placeholder,
  matches = null,
  onKeyPress,
  onActiveChange = () => {},
  intl,
  keys = [],
  disableClose = false,
  resetIcon = "close",
  debounce: debounceTime,
  large,
  showInput,
  isLoading,
  additionalMatchesAvailable,
}) => {
  const [searchActive, setSearchActive] = useState(disableClose);
  const [value, setValue] = useState("");
  const inputRef = React.useRef(null);
  const debounceChange = React.useMemo(
    () => debounce(onChange, debounceTime),
    [onChange, debounceTime]
  );

  useEffect(
    () => {
      if (inputRef.current && searchActive) {
        inputRef.current.focus();
      }
    },
    // TODO: Feel free to fix this eslint error if you got time
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [inputRef.current, searchActive]
  );

  const handleChange = (newValue) => {
    const previousValue = value;
    setValue(newValue);
    if (!debounceTime) {
      onChange(newValue);
      return;
    }
    // do not debounce if leading whitespace
    if (newValue.trim().length === 0) {
      // if actually resetting the input
      if (previousValue.trim().length !== 0) {
        debounceChange.cancel();
        onChange(newValue);
      }
    } else {
      debounceChange(newValue);
    }
  };

  const handleResetSearch = () => {
    if (!disableClose) {
      setSearchActive(false);
      onActiveChange(false);
    }
    setValue("");
    onChange("");
    if (inputRef.current) {
      inputRef.current.blur();
    }
  };

  const handleShowSearch = () => {
    setSearchActive(true);
    onActiveChange(true);
  };

  const handleClickAway = () => {
    if (value === "" && !disableClose) {
      handleResetSearch();
    }
  };

  const handleKeyPress = (key) => {
    if (key === "Escape") {
      if (!disableClose) {
        handleResetSearch();
      }
    } else if (onKeyPress && keys.includes(key)) {
      onKeyPress(key);
    }
  };

  const listenerKeys = React.useMemo(() => [...keys, "Escape"], [keys]);
  useKeyPress(inputRef, listenerKeys, handleKeyPress);

  const containerClasses = classNames(styles.container, {
    [styles.containerActive]: searchActive,
    [styles.large]: large,
    [styles.showInput]: showInput,
    [styles.alwaysOpen]: disableClose,
  });

  return (
    <ClickAwayListener onClickAway={handleClickAway}>
      <div className={containerClasses}>
        {(searchActive || showInput) && (
          <div className={styles.inputContainer}>
            <input
              ref={inputRef}
              className={classNames(styles.input, {
                [styles.large]: large,
              })}
              type="text"
              value={value}
              onFocus={() => setSearchActive(true)}
              onChange={(e) => handleChange(e.target.value)}
              placeholder={
                placeholder || intl.formatMessage(messages.placeholder)
              }
              onKeyDown={(e) => {
                if (e.key === "ArrowUp" || e.key === "ArrowDown") {
                  e.preventDefault();
                }
              }}
            />
            {searchActive && (
              <div
                className={classNames(styles.rightContainer, {
                  [styles.large]: large,
                })}
              >
                {value !== "" && !isLoading && matches !== null && (
                  <React.Fragment>
                    <span>
                      {`${matches}${
                        additionalMatchesAvailable ? "+" : ""
                      } ${intl.formatMessage(messages.matches, {
                        count: matches,
                      })}`}
                    </span>
                  </React.Fragment>
                )}
                {isLoading && Boolean(value) && (
                  <span>
                    <ProgressBar
                      type="circular"
                      mode="indeterminate"
                      theme={{ circular: styles.spinner }}
                    />
                  </span>
                )}
                {((disableClose && Boolean(value)) || !disableClose) && (
                  <IconButton
                    onClick={handleResetSearch}
                    icon={resetIcon}
                    theme={{
                      toggle: large ? undefined : styles.button,
                      icon: styles.icon,
                    }}
                  />
                )}
              </div>
            )}
          </div>
        )}

        {!searchActive && !disableClose && (
          <div className={styles.flex}>
            <div className={styles.flex} />
            <IconButton
              onClick={handleShowSearch}
              icon="search"
              theme={{
                toggle: large ? undefined : styles.button,
                icon: styles.icon,
              }}
            />
          </div>
        )}
        {disableClose && value === "" && (
          <FontIcon
            value="search"
            className={classNames(styles.icon, styles.iconMargin, {
              [styles.iconSmall]: !large,
              [styles.iconLarge]: large,
            })}
          />
        )}
      </div>
    </ClickAwayListener>
  );
};

SearchField.propTypes = {
  onChange: PropTypes.func.isRequired,
  onKeyPress: PropTypes.func,
  onActiveChange: PropTypes.func,
  keys: PropTypes.arrayOf(string),
  placeholder: PropTypes.string,
  intl: intlShape,
  matches: PropTypes.number,
  additionalMatchesAvailable: PropTypes.bool,
  disableClose: PropTypes.bool,
  debounce: PropTypes.number,
  large: PropTypes.bool,
  isLoading: PropTypes.bool,
  showInput: PropTypes.bool,
  resetIcon: PropTypes.string,
};

export default safeInjectIntl(SearchField);
