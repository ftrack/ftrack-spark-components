// :copyright: Copyright (c) 2017 ftrack

import PropTypes from "prop-types";

import { Component } from "react";
import Dropzone from "react-dropzone";
import classNames from "classnames";
import log from "loglevel";
import { FontIcon } from "react-toolbox";
import { defineMessages, intlShape } from "react-intl";
import safeInjectIntl from "../util/hoc/safe_inject_intl";

import style from "./style.scss";
import UploadingProgress from "./uploading_progress";

const messages = defineMessages({
  "drop-files-label": {
    id: "ftrack.spark-components.droppable-area.drop-files-label",
    defaultMessage: "Drop files or",
  },
  "drop-files-label-highlighted": {
    id: "ftrack.spark-components.droppable-area.drop-files-label-highlighted",
    defaultMessage: "click here to browse",
  },
});

/**
 * Droppable area component.
 *
 * Shows an area where users can drag & drop files for upload.
 *
 */

class DroppableArea extends Component {
  constructor(props) {
    super(props);
    this.state = { uploading: false, uploadedPercentage: 0 };
    this.handleDropAccepted = this.handleDropAccepted.bind(this);
    this.droppableAreaRef = null;
  }

  componentDidUpdate(prevProps) {
    const { showFileBrowser, onShowFileBrowser } = this.props;

    if (
      showFileBrowser !== prevProps.showFileBrowser &&
      showFileBrowser &&
      this.droppableAreaRef
    ) {
      this.droppableAreaRef.open();

      if (onShowFileBrowser) {
        onShowFileBrowser();
      }
    }
  }

  setLoadIndicator(percentage) {
    this.setState({ uploadedPercentage: percentage });
  }

  resetUploadState() {
    this.setState({ uploading: false, uploadedPercentage: 0 });
  }

  handleDropAccepted(files) {
    log.debug("Dropped files", files);
    if (files.length) {
      this.uploadFile(files);
    }
  }

  uploadFile(files) {
    this.setState({ uploading: true });
    const promises = files.map((file) =>
      this.props.session.createComponent(file, {
        onProgress: (progress) => this.setLoadIndicator(progress),
      })
    );

    Promise.all(promises)
      .then((responses) => {
        if (this.props.onChange) {
          responses.forEach((response) => {
            this.props.onChange(response[0].data.id);
          });
        }
        setTimeout(() => {
          this.resetUploadState();
        }, this.props.uploadCompleteDelay);
      })
      .catch((error) => {
        this.resetUploadState();
        if (this.props.onUploadError) {
          this.props.onUploadError(error);
        }
      });
  }

  render() {
    const {
      message,
      intl,
      disabled,
      multiple,
      maxSize,
      accept,
      dropzoneProps,
      border,
      dark,
      onUploadError,
    } = this.props;
    const { uploadedPercentage, uploading } = this.state;
    const { formatMessage } = intl;
    let content = null;
    if (uploading) {
      content = (
        <div className={style.content}>
          <UploadingProgress progress={uploadedPercentage} />
        </div>
      );
    } else {
      content = (
        <div className={style.content}>
          <div className={style["icon-container"]}>
            <FontIcon className={style.icon} value={"cloud_queue"} />
          </div>
          <div
            className={classNames(style.dropMessage, style["add-files-text"])}
          >
            {message || (
              <span>
                {formatMessage(messages["drop-files-label"])} <br />
                <span className={style.highlight}>
                  {formatMessage(messages["drop-files-label-highlighted"])}
                </span>
              </span>
            )}
          </div>
        </div>
      );
    }
    return (
      <Dropzone
        multiple={multiple}
        maxSize={maxSize}
        accept={accept}
        onDropAccepted={this.handleDropAccepted}
        onDropRejected={(files) => {
          const fileSize = files[0].size;
          if (onUploadError && fileSize > maxSize) {
            onUploadError({ errorCode: "file-size-error" });
          }
        }}
        {...dropzoneProps}
        disabled={disabled}
        ref={(node) => {
          this.droppableAreaRef = node;
        }}
      >
        {({ getRootProps, getInputProps, isDragAccept, isDragReject }) => {
          const classes = classNames(style.dropzone, {
            [style.border]: border,
            [style.dark]: dark,
            [style.dropzoneActive]: isDragAccept,
            [style.dropzoneReject]: isDragReject,
          });

          return (
            <div className={classes} {...getRootProps()}>
              <input {...getInputProps()} />
              {content}
            </div>
          );
        }}
      </Dropzone>
    );
  }
}

DroppableArea.propTypes = {
  accept: PropTypes.string,
  dropzoneProps: PropTypes.object, // eslint-disable-line
  onChange: PropTypes.func,
  onUploadError: PropTypes.func,
  session: PropTypes.shape({
    createComponent: PropTypes.func.isRequired,
  }).isRequired,
  message: PropTypes.element,
  multiple: PropTypes.bool,
  maxSize: PropTypes.number,
  uploadCompleteDelay: PropTypes.number,
  intl: intlShape.isRequired,
  disabled: PropTypes.bool,
  border: PropTypes.bool,
  dark: PropTypes.bool,
  showFileBrowser: PropTypes.bool,
  onShowFileBrowser: PropTypes.func,
};

DroppableArea.defaultProps = {
  uploadCompleteDelay: 0,
  value: null,
  dropzoneProps: {},
  multiple: false,
  maxSize: 1e9, // 1000 MB
  accept: null,
  disabled: false,
  showFileBrowser: false,
};

export default safeInjectIntl(DroppableArea);
