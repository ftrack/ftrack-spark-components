import * as React from "react";
import centered from "@storybook/addon-centered/react";
import { action } from "@storybook/addon-actions";

import DroppableThumbnail from "./thumbnail";
import getTestSession from "../../story/get_session";

const session = getTestSession();

export default {
  title: "Droppable thumbnail",
  decorators: [centered],
};

const messages = {
  message: {
    id: "ftrack-spark-components.droppable-thumbnail.message-company",
    defaultMessage: "Drop image or {browseButton} to add new company logo",
  },
};

const ThumbnailState = (Component) =>
  class extends React.Component {
    constructor(props) {
      super(props);
      this.state = { value: props.value || null };
      this.handleChange = this.handleChange.bind(this);
    }

    handleChange(value) {
      this.setState({ value });
    }

    render() {
      let wrapperStyle = { width: "300px", height: "200px" };
      if (this.props.thumbnailSize === "large") {
        wrapperStyle = { width: "500px", height: "500px" };
      }

      return (
        <div style={wrapperStyle}>
          <Component
            {...this.props}
            value={this.state.value}
            onChange={this.handleChange}
            loading={this.props.loading}
          />
        </div>
      );
    }
  };

const DroppableThumbnailWithState = ThumbnailState(DroppableThumbnail);

export const NoValue = () => <DroppableThumbnailWithState session={session} />;

NoValue.storyName = "No value";

export const Loading = () => (
  <DroppableThumbnailWithState session={session} loading />
);

export const RemoveButton = () => (
  <DroppableThumbnailWithState
    onDelete={action("Image removed.")}
    deletable
    session={session}
  />
);

RemoveButton.storyName = "Remove button";

export const ValueSet = () => (
  <DroppableThumbnailWithState
    session={session}
    value="26171d9a-6766-11e1-997b-f23c91df25eb"
  />
);

ValueSet.storyName = "Value set";

export const Large = () => (
  <DroppableThumbnailWithState session={session} thumbnailSize="large" />
);

export const CompanyLogo = () => (
  <DroppableThumbnailWithState
    session={session}
    label={messages.message}
    thumbnailSize="large"
  />
);
