// :copyright: Copyright (c) 2019 ftrack
import PropTypes from "prop-types";
import ProgressBar from "react-toolbox/lib/progress_bar";
import classNames from "classnames";
import { defineMessages, FormattedMessage } from "react-intl";
import style from "./style.scss";
import spinnerBackgroundTheme from "./spinner_background_theme.scss";
import safeInjectIntl from "../util/hoc/safe_inject_intl";

const messages = defineMessages({
  "loading-message": {
    id: "ftrack-spark-components.droppable-area.loading-message",
    defaultMessage: "Uploading...",
  },
  "loading-message-finalizing": {
    id: "ftrack-spark-components.droppable-area.loading-message-finalizing",
    defaultMessage: "Finalizing upload...",
  },
});

function ProgressMessage({ progress }) {
  if (progress <= 0) {
    return <FormattedMessage {...messages["loading-message"]} />;
  } else if (progress >= 100) {
    return <FormattedMessage {...messages["loading-message-finalizing"]} />;
  }

  return <span>{progress}%</span>;
}

ProgressMessage.propTypes = {
  progress: PropTypes.number,
};

function UploadingProgress({ message, mode, className, progress, ...props }) {
  const classes = classNames(style.uploadingProgress, className);
  // Force indeterminate spinner when upload is starting.
  let progressMode = mode;
  if (progressMode === "determinate" && progress <= 0) {
    progressMode = "indeterminate";
  }

  return (
    <div className={classes} {...props}>
      <div>
        {progressMode === "determinate" && (
          <ProgressBar
            theme={spinnerBackgroundTheme}
            type="circular"
            mode="determinate"
            value={100}
          />
        )}
        <ProgressBar type="circular" mode={progressMode} value={progress} />
      </div>
      <p className={style.dropMessage}>
        {message || <ProgressMessage progress={progress} />}
      </p>
    </div>
  );
}

UploadingProgress.propTypes = {
  mode: PropTypes.oneOf(["determinate", "indeterminate"]),
  progress: PropTypes.number,
  message: PropTypes.node,
  className: PropTypes.string,
};

UploadingProgress.defaultProps = {
  mode: "determinate",
  message: null,
};

export default safeInjectIntl(UploadingProgress);
