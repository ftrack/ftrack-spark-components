..
    :copyright: Copyright (c) 2017 ftrack

##############
Droppable area
##############

The droppable area component is used to render a dropzone where files
can be dropped to create a component in ftrack.

Base droppable area
-------------------

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
className                      CSS Class to add to the root element.
dropzoneProps                  Additional props to pass to the dropzone component.
onChange                       Invoked with *value* when upload completes.
onUploadError                  Invoked when uploading fails.
session                        ftrack JS API session instance.
style                          Style object
accept                         MIME type of acceptable files.
dark                           boolean to set dark styling on the drop zone.
showFileBrowser                boolean to trigger the file browser to open
onShowFileBrowser              callback function called when file browser opening is triggered through props

Thumbnail droppable area
------------------------

The thumbnail component is used to upload a thumbnail to an object in ftrack.

The containing component is responsible for maintaining the state by passing
in a *onChange* property and updating *value*. It should also pass in a
*session* which will be used to upload images and generate thumbnail URLs.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
className                      CSS Class to add to the root element.
dropzoneProps                  Additional props to pass to the dropzone component.
onChange                       Invoked with *value* when upload completes.
onUploadError                  Invoked when uploading fails.
session                        ftrack JS API session instance.
style                          Style object
thumbnailSize      medium      Size of image to display: small, medium or large.
value              null        A thumbnail component id to display.
