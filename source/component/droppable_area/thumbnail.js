// :copyright: Copyright (c) 2017 ftrack

import PropTypes from "prop-types";

import { cloneElement } from "react";
import classNames from "classnames";
import { FormattedMessage, defineMessages, intlShape } from "react-intl";
import ProgressBar from "react-toolbox/lib/progress_bar";
import DroppableArea from "../droppable_area";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import IconButton from "../icon_button";

import style from "./style.scss";

import { THUMBNAIL_SIZES } from "../util/constant";

const messages = defineMessages({
  "browse-button": {
    id: "ftrack-spark-components.droppable-thumbnail.browse-button",
    defaultMessage: "click here",
  },
  message: {
    id: "ftrack-spark-components.droppable-thumbnail.message",
    defaultMessage: "Drop image or {browseButton} to add new thumbnail",
  },
  "remove-button-tooltip": {
    id: "ftrack-spark-components.droppable-thumbnail.remove-button-tooltip",
    defaultMessage: "Delete",
  },
});

const Loading = ({ className }) => (
  <div className={classNames(className, style.loading)}>
    <ProgressBar type="circular" mode="indeterminate" />
  </div>
);

Loading.propTypes = {
  className: PropTypes.object, //eslint-disable-line
};

// Time in ms to wait after upload completing to hide spinner,
// wait a while to allow thumbor to load image.
// Can be removed if a browser preview of the image is displayed instead.
const THUMBNAIL_UPLOAD_COMPLETE_DELAY = 1000;

/**
 * Droppable thumbnail component.
 *
 * Shows a thumbnail which can be replaced by dragging and dropping image
 * files.
 */
function DroppableThumbnail({
  loading,
  loadingComponent,
  value,
  style: inputStyle,
  className: inputClassName,
  dropzoneProps,
  thumbnailSize,
  intl,
  label,
  children,
  emptyState,
  deletable,
  onDelete,
  ...props
}) {
  let className = {};
  let inlineStyle = Object.assign({}, inputStyle);
  if (value) {
    const thumbnailUrl = props.session.thumbnailUrl(value, {
      size: THUMBNAIL_SIZES[thumbnailSize],
    });
    className = {
      [style.hasValue]: true,
    };

    inlineStyle = Object.assign({}, inlineStyle, {
      backgroundImage: `url("${thumbnailUrl}")`,
    });
  }

  const browseButton = (
    <span className={style.highlight}>
      {intl.formatMessage(messages["browse-button"])}
    </span>
  );

  if (loading && loadingComponent) {
    return cloneElement(loadingComponent, {
      className: classNames(className, inputClassName),
    });
  }

  const thumbnailClasses = classNames(
    style.droppableThumbnail,
    className,
    inputClassName,
    { [style.hasEmptyState]: !!emptyState }
  );

  return (
    <div className={thumbnailClasses} style={inlineStyle}>
      <DroppableArea
        multiple={false}
        dropzoneProps={Object.assign({}, dropzoneProps, {
          activeClassName: style.dropzoneActive,
          rejectClassName: style.dropzoneReject,
        })}
        uploadCompleteDelay={THUMBNAIL_UPLOAD_COMPLETE_DELAY}
        accept="image/*"
        maxSize={1e7} // 10 MB
        message={<FormattedMessage {...label} values={{ browseButton }} />}
        {...props}
      />
      {!value && emptyState && (
        <div className={style["empty-state-container"]}>{emptyState}</div>
      )}
      <div className={style.children}>{children}</div>
      {deletable ? (
        <IconButton
          className={style["deletable-button"]}
          onClick={onDelete}
          icon="delete"
          tooltip={intl.formatMessage(messages["remove-button-tooltip"])}
        />
      ) : null}
    </div>
  );
}

DroppableThumbnail.propTypes = {
  className: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  dropzoneProps: PropTypes.object, // eslint-disable-line
  onChange: PropTypes.func,
  onUploadError: PropTypes.func,
  session: PropTypes.shape({
    createComponent: PropTypes.func.isRequired,
    thumbnailUrl: PropTypes.func.isRequired,
  }).isRequired,
  style: PropTypes.objectOf(PropTypes.string),
  thumbnailSize: PropTypes.oneOf(["small", "medium", "large", "xlarge"]),
  value: PropTypes.string,
  intl: intlShape,
  label: PropTypes.shape({
    id: PropTypes.string.isRequired,
    defaultMessage: PropTypes.string.isRequired,
  }).isRequired,
  children: PropTypes.element,
  loading: PropTypes.bool,
  loadingComponent: PropTypes.element,
  emptyState: PropTypes.element,
  deletable: PropTypes.bool,
  onDelete: PropTypes.func,
};

DroppableThumbnail.defaultProps = {
  style: {},
  className: "",
  value: null,
  dropzoneProps: {},
  thumbnailSize: "medium",
  label: messages.message,
  loadingComponent: <Loading />,
};

export default safeInjectIntl(DroppableThumbnail);
