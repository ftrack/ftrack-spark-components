import * as React from "react";
import { action } from "@storybook/addon-actions";
import centered from "@storybook/addon-centered/react";
import { withState, withHandlers, compose } from "recompose";

import DroppableArea from ".";
import UploadingProgress from "./uploading_progress";
import getTestSession from "../../story/get_session";

const session = getTestSession();

export default {
  title: "Droppable area",
  decorators: [centered],
};

const Container = (Component) =>
  class extends React.Component {
    render() {
      return (
        <div
          style={{
            width: "300px",
            height: "300px",
            position: "relative",
          }}
        >
          <Component
            onChange={action("onChange")}
            onUploadError={action("onUploadError")}
            {...this.props}
          />
        </div>
      );
    }
  };

const WrappedDropArea = Container(DroppableArea);

export const SingleFileUploadWithTransparentBackground = () => (
  <WrappedDropArea session={session} border />
);

SingleFileUploadWithTransparentBackground.storyName =
  "Single file upload with transparent background";

export const Dark = () => <WrappedDropArea session={session} dark border />;

export const MultiFileUpload = () => (
  <WrappedDropArea session={session} multiple />
);

MultiFileUpload.storyName = "Multi file upload";

const UploadingProgressTest = compose(
  withState("value", "setValue", 0),
  withHandlers({
    onChange: (props) => (event) => props.setValue(event.target.value),
  })
)(({ value, onChange }) => (
  <div>
    <input type="range" value={value} onChange={onChange} min={0} max={100} />
    <UploadingProgress progress={value} />
  </div>
));

export const _UploadingProgress = () => <UploadingProgressTest />;

_UploadingProgress.storyName = "Uploading progress";

export const UploadingProgressIndeterminate = () => (
  <UploadingProgress mode="indeterminate" message="Encoding..." />
);

UploadingProgressIndeterminate.storyName = "Uploading progress (indeterminate)";
