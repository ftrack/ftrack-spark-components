// :copyright: Copyright (c) 2020 ftrack

import PropTypes from "prop-types";

import { AttributeLabel } from "../dataview/attribute";

import style from "./style.scss";

function DetailsPanel({ data, attributes, className, onItemClicked }) {
  const item = data[0];
  if (!item) {
    return null;
  }
  return (
    <div className={className}>
      {attributes.map((attribute) => {
        let valueElement = item[attribute.key];
        if (attribute.formatter) {
          const AttributeValueComponentType = attribute.formatter;
          valueElement = (
            <AttributeValueComponentType
              value={item[attribute.key]}
              onClick={onItemClicked}
            />
          );
        }
        return (
          <div key={attribute.key} className={style.attribute}>
            <AttributeLabel
              attribute={attribute}
              className={style.attributeLabel}
              enableGroup
            />
            <div className={style.attributeValue}>{valueElement}</div>
          </div>
        );
      })}
    </div>
  );
}

DetailsPanel.propTypes = {
  data: PropTypes.arrayOf(PropTypes.objects),
  attributes: PropTypes.arrayOf(PropTypes.objects),
  className: PropTypes.string,
  onItemClicked: PropTypes.func,
};

export default DetailsPanel;
