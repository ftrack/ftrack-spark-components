import centered from "@storybook/addon-centered/react";
import { Provider as ReduxProvider } from "react-redux";

import DetailsPanel from ".";

import configureStore from "../../story/configure_store.js";
import getSession from "../../story/get_session";

import { SparkProvider } from "../util/hoc";

// eslint-disable-next-line react/prop-types
function Provider({ story, store, session }) {
  return (
    <SparkProvider session={session}>
      <ReduxProvider store={store}>{story}</ReduxProvider>
    </SparkProvider>
  );
}

const store = configureStore((state = {}) => state, [], getSession());

export default {
  title: "Details",
  decorators: [
    (Story) => (
      <Provider story={<Story />} store={store} session={getSession()} />
    ),
    centered,
  ],
};

export const AssetVersionDetails = () => (
  <DetailsPanel
    selectedAttributes={[
      "$name",
      "$linkedTo",
      "version",
      "$user",
      "date",
      "comment",
      "status.name",
      "asset.name",
      "task.$status",
    ]}
    entity={{
      id: "6cd65e32-9f19-11ea-aba9-0242ac110002",
      type: "AssetVersion",
    }}
    session={getSession()}
  />
);

AssetVersionDetails.storyName = "Asset version details";

AssetVersionDetails.parameters = {
  info: "This example shows an asset versions details panel component.",
};
