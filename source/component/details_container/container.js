// :copyright: Copyright (c) 2020 ftrack
import { Component } from "react";

import initial from "lodash/initial";
import uniq from "lodash/uniq";
import PropTypes from "prop-types";
import ProgressBar from "react-toolbox/lib/progress_bar";

import DetailsPanel from "./details";
import getAttributes from "../dataview/util";

import { withSubspace, withSession } from "../util/hoc";
import { reducer, sagas, withDataLoader } from "../dataview/container";

const DataDetailsPanel = withDataLoader(DetailsPanel);

const DataDetailsPanelDefault = withSubspace(
  reducer,
  sagas,
  "grid-view-default"
)(DataDetailsPanel);

class DetailsContainerBase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      attributeSource: null,
    };
  }

  componentDidMount() {
    const { entity, session, selectedAttributes } = this.props;
    const relations = uniq(
      selectedAttributes.reduce((accumulator, item) => {
        const fragments = item.split(".");
        if (fragments.length > 1) {
          accumulator.push(initial(fragments).join("."));
        }
        return accumulator;
      }, [])
    );
    session.initializing
      .then(() => getAttributes(entity.type, relations, session))
      .then((attributeSource) => {
        this.setState({
          attributeSource,
        });
      });
  }

  render() {
    const { attributeSource } = this.state;
    const { selectedAttributes, entity, className, onItemClicked } = this.props;
    let element;
    if (!attributeSource) {
      element = <ProgressBar mode="indeterminate" />;
    } else {
      const attributes = selectedAttributes.map(
        (item) => attributeSource.attributes[item]
      );
      Object.keys(attributeSource.attributes).forEach((key) => {
        if (key.startsWith("custom_attribute-")) {
          attributes.push(attributeSource.attributes[key]);
        }
      });
      element = (
        <DataDetailsPanelDefault
          attributes={attributes}
          className={className}
          onItemClicked={onItemClicked}
          loader={{
            model: entity.type,
            limit: 1,
            filters: `id is "${entity.id}"`,
          }}
        />
      );
    }

    return element;
  }
}

DetailsContainerBase.propTypes = {
  entity: PropTypes.shape({
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
  }).isRequired,
  className: PropTypes.string,
  onItemClicked: PropTypes.func,
  session: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  selectedAttributes: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
};

export default withSession(DetailsContainerBase);
