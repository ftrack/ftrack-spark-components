..
    :copyright: Copyright (c) 2019 ftrack

##########
Breakpoint
##########

Breakpoints
-----------

* xs, extra-small: 0px or larger
* sm, small: 600px or larger
* md, medium: 960px or larger
* lg, large: 1280px or larger
* xl, extra-large: 1920px or larger

Components
----------

Various components for managing breakpoints.

withWindowSize
    Higher order component to monitor window width and height and map to props.
withBreakpoints
    Higher order component to monitor window width and map to breakpoints.
withMobile
    Higher order component to monitor window width and return either a mobile or desktop component.
MobileHidden
    Component children are shown only if width is higher than mobile breakpoint.
MobileShown
    Component children are shown only if width is smaller than mobile breakpoint.

Sass mixins
-----------

There are various sass mixins that can be imported to generate media queries
based on the breakpoints.

These are defined as `<breakpoint><Up|Down>`, e.g. `smDown`.

    @import '~ftrack-spark-components/lib/breakpoint/mixin.scss';
    @include smDown {
        .mobile-only {
            display: none;
        }
    }
