// :copyright: Copyright (c) 2019 ftrack
import { Component } from "react";
import throttle from "lodash/throttle";

function getWindowSize(fallback = { width: 1024, height: 768 }) {
  return {
    width: typeof window !== "undefined" ? window.innerWidth : fallback.width,
    height: typeof window !== "undefined" ? window.innerHeight : fallback.heigh,
  };
}

const WINDOW_RESIZE_THROTTLE = 200;

const withWindowSize =
  ({ fallback, mapSizeToProps = (size) => size } = {}) =>
  (WrappedComponent) => {
    class ComponentWithWindowSize extends Component {
      constructor(props, context) {
        super(props, context);
        const size = getWindowSize(fallback);
        this.state = {
          size,
          props: mapSizeToProps(size),
        };

        this.updateWindowSize = this.updateWindowSize.bind(this);
        this.updateWindowSizeThrottled = throttle(
          this.updateWindowSize,
          WINDOW_RESIZE_THROTTLE
        );
      }

      componentDidMount() {
        window.addEventListener("resize", this.updateWindowSizeThrottled);
      }

      componentWillUnmount() {
        window.removeEventListener("resize", this.updateWindowSizeThrottled);
      }

      updateWindowSize() {
        const size = getWindowSize(fallback);
        if (
          size.width !== this.state.size.width ||
          size.height !== this.state.size.height
        ) {
          const props = mapSizeToProps(size);
          this.setState({ size, props });
        }
      }

      render() {
        return <WrappedComponent {...this.props} {...this.state.props} />;
      }
    }

    ComponentWithWindowSize.displayName = "withWindowSize()";

    return ComponentWithWindowSize;
  };

export default withWindowSize;
