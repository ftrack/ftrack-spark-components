// :copyright: Copyright (c) 2019 ftrack
import centered from "@storybook/addon-centered/react";

import { MobileHidden, MobileShown, withMobile, withBreakpoints } from ".";
import withWindowSize from "./with_window_size";

export default {
  title: "Breakpoint",
  decorators: [centered],
};

function Size({ width, height }) {
  return (
    <pre>
      {width} x {height}
    </pre>
  );
}
const WindowSizeTest = withWindowSize()(Size);
export const WithWindowSize = () => <WindowSizeTest />;

WithWindowSize.storyName = "withWindowSize";

function Breakpoint({ xs, sm, lg, xl }) {
  return (
    <ul>
      <li>
        <code>xs: {xs ? "true" : "false"}</code>
      </li>
      <li>
        <code>sm: {sm ? "true" : "false"}</code>
      </li>
      <li>
        <code>lg: {lg ? "true" : "false"}</code>
      </li>
      <li>
        <code>xl: {xl ? "true" : "false"}</code>
      </li>
    </ul>
  );
}
const BreakpointTest = withBreakpoints(Breakpoint);
export const WithBreakpoints = () => <BreakpointTest />;

WithBreakpoints.storyName = "withBreakpoints";

const WithMobileTest = withMobile(
  () => <code>Mobile</code>,
  () => <code>Desktop</code>
);

export const WithMobile = () => <WithMobileTest />;

WithMobile.storyName = "withMobile";

export const _MobileHidden = () => (
  <div>
    Hidden on mobile:{" "}
    <MobileHidden>
      <span>Larger than mobile breakpoint</span>
    </MobileHidden>
  </div>
);

_MobileHidden.storyName = "MobileHidden";

export const _MobileShown = () => (
  <div>
    Shown on mobile:{" "}
    <MobileShown>
      <span>Smaller than mobile breakpoint</span>
    </MobileShown>
  </div>
);

_MobileShown.storyName = "MobileShown";
