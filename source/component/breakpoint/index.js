// :copyright: Copyright (c) 2018 ftrack
import { Children } from "react";
import withWindowSize from "./with_window_size";
import breakpoints from "./breakpoints.scss";

function mapSizeToBreakpoints({ width }) {
  return Object.keys(breakpoints).reduce((accumulator, breakpoint) => {
    accumulator[breakpoint] = width >= breakpoints[breakpoint];
    return accumulator;
  }, {});
}

function mapSizeToMobile({ width }) {
  return { isMobile: width < breakpoints.md };
}

export const withIsMobile = withWindowSize({ mapSizeToProps: mapSizeToMobile });

export const MobileHidden = withIsMobile(({ isMobile, children }) =>
  isMobile ? null : Children.only(children)
);

export const MobileShown = withIsMobile(({ isMobile, children }) =>
  isMobile ? Children.only(children) : null
);

export const withBreakpoints = (BaseComponent) =>
  withWindowSize({
    mapSizeToProps: mapSizeToBreakpoints,
  })(({ ...props }) => <BaseComponent {...props} />);

export const withMobile = (MobileComponent, DesktopComponent) =>
  withIsMobile(({ isMobile, ...props }) =>
    isMobile ? <MobileComponent {...props} /> : <DesktopComponent {...props} />
  );

export default withBreakpoints;
