..
    :copyright: Copyright (c) 2018 ftrack

###########
Button menu
###########

A button menu component to display a Button that can reveal a menu when clicked.

It has the following dependencies:

* "react-toolbox": "^1.0.0"

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
className                      CSS class for root element
button                         An element that will cause the menu to reveal when clicked.
position                       A string position to the react-toolbox menu.
children                       Child elements to be displayed inside the react-toolbox menu when  revealed.
onSelect                       Callback function on menu item click.
