// :copyright: Copyright (c) 2016 ftrack

import PropTypes from "prop-types";

import { cloneElement, Component } from "react";
import { Menu } from "react-toolbox/lib/menu";
import classNames from "classnames";

import menuTheme from "./menu_theme.scss";
import compressed from "./compressed_menu_theme.scss";
import style from "./style.scss";

/** Button menu component used to display a button that trigger a menu. */
export default class ButtonMenu extends Component {
  constructor() {
    super();
    this.state = { active: false };
    this.hideMenu = this.hideMenu.bind(this);
    this.showMenu = this.showMenu.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  hideMenu() {
    this.setState({ active: false });
  }

  showMenu(e) {
    e.stopPropagation();
    this.setState({ active: true });
  }

  // eslint-disable-next-line class-methods-use-this
  handleClick(e) {
    // Capture click events to ensure items behind menus are not clicked
    // when event bubbles.
    e.stopPropagation();
  }

  render() {
    const {
      children,
      onSelect,
      button,
      className,
      position,
      display,
      variant,
    } = this.props;
    const _classNames = classNames(
      style.wrapper,
      {
        [style.displayInlineBlock]: display === "inline-block",
      },
      className
    );

    const clonedButton = cloneElement(button, {
      onClick: this.showMenu,
    });

    return (
      <div className={_classNames} onClick={this.handleClick}>
        {clonedButton}
        <Menu
          active={this.state.active}
          onHide={this.hideMenu}
          position={position}
          onSelect={onSelect}
          menuRipple
          className={style.menu}
          theme={variant === "compressed" ? compressed : menuTheme}
        >
          {children}
        </Menu>
      </div>
    );
  }
}

ButtonMenu.propTypes = {
  className: PropTypes.string,
  display: PropTypes.oneOf(["inline-block"]),
  button: PropTypes.node.isRequired,
  position: PropTypes.string,
  children: PropTypes.node,
  onSelect: PropTypes.func,
  variant: PropTypes.oneOf(["compressed", "default"]),
};

ButtonMenu.defaultProps = {
  className: "",
};
