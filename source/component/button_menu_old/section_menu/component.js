// :copyright: Copyright (c) 2019 ftrack
import { Fragment } from "react";
import PropTypes from "prop-types";
import { MenuItem } from "react-toolbox/lib/menu";
import { MENU_ITEM } from "react-toolbox/lib/identifiers";
import classNames from "classnames";
import Heading from "../../heading";

import menuItemTheme from "./menu_item_theme.scss";
import style from "./style.scss";

function SectionTitle({ children }) {
  return (
    <Heading variant="label" color="secondary" className={style.header}>
      {children}
    </Heading>
  );
}

SectionTitle.propTypes = {
  children: PropTypes.node,
};

function SectionMenuItem({
  value,
  icon,
  selectedIcon,
  caption,
  selected,
  disabled,
  onSelectionChange,
  className,
}) {
  const onClick = () => {
    if (onSelectionChange) {
      if (!selected) {
        onSelectionChange(value, !selected);
      }
    }
  };

  const classes = classNames(className, {
    [menuItemTheme.selected]: selected,
    [menuItemTheme.selectedIcon]: !icon && selectedIcon,
  });

  return (
    <MenuItem
      theme={menuItemTheme}
      icon={icon || selectedIcon}
      value={value}
      caption={caption}
      className={classes}
      onClick={onClick}
      disabled={disabled}
    />
  );
}

SectionMenuItem.propTypes = {
  icon: PropTypes.oneOf([PropTypes.string, PropTypes.node]),
  selectedIcon: PropTypes.oneOf([PropTypes.string, PropTypes.node]),
  caption: PropTypes.node,
  value: PropTypes.string,
  selected: PropTypes.bool,
  disabled: PropTypes.bool,
  className: PropTypes.string,
};

SectionMenuItem.toolboxId = MENU_ITEM;

function SectionMenu({ sections, onSelectionChange }) {
  return (
    <Fragment>
      {sections.map((section) => (
        <Fragment>
          <SectionTitle>{section.title}</SectionTitle>
          {section.options.map((option) => (
            <SectionMenuItem
              {...option}
              key={option.value}
              onSelectionChange={onSelectionChange}
            />
          ))}
        </Fragment>
      ))}
    </Fragment>
  );
}

SectionMenu.propTypes = {
  sections: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.node,
      options: PropTypes.arrayOf(PropTypes.shape(SectionMenuItem.propTypes)),
    })
  ),
  onSelectionChange: PropTypes.func,
};

export default SectionMenu;
