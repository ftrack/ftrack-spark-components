// :copyright: Copyright (c) 2018 ftrack

import { useState } from "react";
import centered from "@storybook/addon-centered/react";
import { action } from "@storybook/addon-actions";

import { Button as ButtonOld } from "react-toolbox/lib/button";
import { MenuItem as MenuItemOld } from "react-toolbox/lib/menu";
import { defineMessages, FormattedMessage } from "react-intl";
import FontIcon from "react-toolbox/lib/font_icon";

import { getForegroundColor } from "../util/color";
import ButtonMenuOld, {
  DropdownButton as DropdownButtonOld,
  SectionMenu as SectionMenuOld,
} from ".";

import { MenuItem, Box, Button, ListItemIcon, Tooltip } from "@mui/material";
import {
  Inbox,
  GetApp,
  Favorite,
  OpenInBrowser,
  Lens,
} from "@mui/icons-material";
import ButtonMenu, { DropdownButton, SectionMenu } from "../button_menu";

const messages = defineMessages({
  "status-selector": {
    id: "player.review-app.review.status-button",
    description: "Status text for status selector button",
    defaultMessage: "Status",
  },
});

export default {
  title: "Button menu old",
  decorators: [centered],
};

export const Basic = () => (
  <Box sx={{ display: "flex" }}>
    <ButtonMenuOld
      button={<ButtonOld icon="inbox" label="More" accent />}
      position="topLeft"
      onSelect={() => false}
    >
      <MenuItemOld value="download" icon="get_app" caption="Download" />
      <MenuItemOld value="help" icon="favorite" caption="Favorite" />
      <MenuItemOld
        value="settings"
        icon="open_in_browser"
        caption="Open in app"
      />
    </ButtonMenuOld>
    <Box sx={{ width: 2 }} />
    <ButtonMenu
      button={<Button startIcon={<Inbox fontSize="small" />}>More</Button>}
    >
      <MenuItem>
        <ListItemIcon>
          <GetApp fontSize="small" />
        </ListItemIcon>
        Download
      </MenuItem>
      <MenuItem>
        <ListItemIcon>
          <Favorite fontSize="small" />
        </ListItemIcon>
        Favorite
      </MenuItem>
      <MenuItem>
        <ListItemIcon>
          <OpenInBrowser fontSize="small" />
        </ListItemIcon>
        Open in app
      </MenuItem>
    </ButtonMenu>
  </Box>
);

function StatusSelector() {
  const statuses = [
    { name: "Approved", color: "green" },
    { name: "WIP", color: "gray" },
    { name: "Pending review", color: "#f4c430" },
    { name: "Rejected", color: "red" },
    { name: "test", color: "#ffffff" },
  ];

  const [localStatus, setLocalStatus] = useState({
    color: "#29bb89",
    name: "status",
  });

  return (
    <Box sx={{ display: "flex" }}>
      <ButtonMenuOld
        button={
          <DropdownButtonOld
            label={localStatus.name}
            variant="rounded"
            style={{
              backgroundColor: localStatus.color,
              color: `${getForegroundColor(localStatus.color)}`,
            }}
            tooltip={<FormattedMessage {...messages["status-selector"]} />}
          />
        }
        position="topRight"
        onSelect={(item) => {
          setLocalStatus(item);
        }}
        variant="compressed"
      >
        {statuses.map((item) => (
          <MenuItemOld
            key={item.name}
            value={item}
            caption={
              <span>
                <FontIcon style={{ color: item.color }} value="lens" />{" "}
                {item.name}
              </span>
            }
          />
        ))}
      </ButtonMenuOld>
      <Box sx={{ width: 2 }} />
      <ButtonMenu
        dense
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        button={
          <Tooltip
            title={<FormattedMessage {...messages["status-selector"]} />}
            placement="top"
          >
            <DropdownButton
              rounded
              variant="contained"
              size="small"
              sx={{
                bgcolor: localStatus.color,
                color: `${getForegroundColor(localStatus.color)}`,
                textTransform: "capitalize",
                "&:hover": {
                  bgcolor: localStatus.color,
                },
              }}
            >
              {localStatus.name}
            </DropdownButton>
          </Tooltip>
        }
      >
        {statuses.map((status) => (
          <MenuItem key={status.name} onClick={() => setLocalStatus(status)}>
            <ListItemIcon>
              <Lens fontSize="small" sx={{ color: status.color }} />
            </ListItemIcon>
            {status.name}
          </MenuItem>
        ))}
      </ButtonMenu>
    </Box>
  );
}

export const Compressed = () => <StatusSelector></StatusSelector>;

const sections = [
  {
    title: "Header 1",
    options: [
      {
        selected: true,
        caption: "Previous",
        selectedIcon: "check",
        value: "caption-one",
      },
      {
        selected: false,
        caption: "Next",
        selectedIcon: "check",
        value: "caption-two",
      },
    ],
  },
  {
    title: "Header 2",
    options: [
      {
        selected: true,
        caption: "Play",
        selectedIcon: "check",
        value: "caption-one",
      },
      {
        selected: false,
        caption: "Pause",
        selectedIcon: "check",
        value: "caption-two",
      },
    ],
  },
];

export const DropdownButtonWithSections = () => (
  <ButtonMenuOld button={<DropdownButtonOld icon="inbox" />} position="topLeft">
    <SectionMenuOld
      sections={sections}
      onSelectionChange={action("Selection changed")}
    />
  </ButtonMenuOld>
);

DropdownButtonWithSections.storyName = "Dropdown button with sections";
