// :copyright: Copyright (c) 2019 ftrack
import PropTypes from "prop-types";
import classNames from "classnames";
import Button from "react-toolbox/lib/button";
import withTooltip from "react-toolbox/lib/tooltip";
import { FontIcon } from "react-toolbox/lib/font_icon";
import dropdownButtonTheme from "./dropdown_button_theme.scss";

const TooltipButton = withTooltip(Button);

function DropdownButton({
  icon,
  children,
  className,
  size = "medium",
  variant = "flat",
  ...props
}) {
  const classes = classNames(
    {
      [dropdownButtonTheme.sizeMedium]: size === "medium",
      [dropdownButtonTheme.sizeSmall]: size === "small",
      [dropdownButtonTheme.rounded]: variant === "rounded",
    },
    className
  );
  const ButtonComponent = props.tooltip ? TooltipButton : Button;
  return (
    <ButtonComponent className={classes} theme={dropdownButtonTheme} {...props}>
      {icon && <FontIcon className={dropdownButtonTheme.icon} value={icon} />}
      {children}
      <FontIcon className={dropdownButtonTheme.arrow} value="arrow_drop_down" />
    </ButtonComponent>
  );
}

DropdownButton.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  size: PropTypes.oneOf(["small", "medium"]),
  icon: PropTypes.string,
  variant: PropTypes.oneOf(["rounded", "flat"]),
};

export default DropdownButton;
