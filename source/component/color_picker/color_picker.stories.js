// :copyright: Copyright (c) 2017 ftrack
import { Component } from "react";
import centered from "@storybook/addon-centered/react";

import getSession from "../../story/get_session";
import getFixedSizeDecorator from "../../story/get_fixed_size_decorator";

import ProjectColorPicker from "./project_color_picker";

export default {
  title: "Color picker",
  decorators: [getFixedSizeDecorator(), centered],
};

/** Project color picker test */
class ProjectColorPickerTest extends Component {
  constructor(props) {
    super(props);
    this.handleProjectColorChanged = this.handleProjectColorChanged.bind(this);
    this.session = getSession();
    this.state = { projects: [], projectId: null };
  }

  componentDidMount() {
    this.session
      .query("select id, full_name, color from Project limit 10")
      .then((response) => {
        this.setState({
          projects: response.data,
          projectId: response.data[0].id,
        });
      });
  }

  changeProject(projectId) {
    this.setState({ projectId });
  }

  handleProjectColorChanged({ value, projectId }) {
    const index = this.state.projects.findIndex(
      (project) => project.id === projectId
    );
    const nextProjects = [
      ...this.state.projects.slice(0, index),
      Object.assign({}, this.state.projects[index], { color: value }),
      ...this.state.projects.slice(index + 1),
    ];
    this.setState({ projects: nextProjects });
  }

  render() {
    return (
      <div>
        <ProjectColorPicker
          session={this.session}
          projectId={this.state.projectId}
          onSuccess={this.handleProjectColorChanged}
        />
        <ul>
          {this.state.projects.map((project) => (
            <li
              onClick={this.changeProject.bind(this, project.id)}
              style={{
                padding: "10px",
                color: "white",
                backgroundColor: project.color,
              }}
            >
              {this.state.projectId === project.id ? "›\u00a0" : ""}
              {project.full_name}
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export const _ProjectColorPicker = () => <ProjectColorPickerTest />;

_ProjectColorPicker.storyName = "Project color picker";
