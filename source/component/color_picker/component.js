// :copyright: Copyright (c) 2017 ftrack

import PropTypes from "prop-types";

import { Component } from "react";
import Button from "react-toolbox/lib/button";
// eslint-disable-next-line
import { TwitterPicker } from "react-color";
import onClickOutside from "react-onclickoutside";
import classnames from "classnames";
import FontIcon from "react-toolbox/lib/font_icon";

import { PROJECT_COLORS } from "../util/constant";
import style from "./style.scss";

/** Color field */
class ColorPicker extends Component {
  constructor(props) {
    super(props);
    this.state = { pickerActive: false };
    this.handleButtonClick = this.handleButtonClick.bind(this);
    this.handleChangeComplete = this.handleChangeComplete.bind(this);
  }

  handleButtonClick() {
    this.setState({ pickerActive: !this.state.pickerActive });
  }

  handleChangeComplete(color) {
    this.setState({ pickerActive: false });
    this.props.onChange(color.hex.toUpperCase());
  }

  handleClickOutside() {
    this.setState({ pickerActive: false });
  }

  render() {
    const pickerTriangle =
      this.props.position === "right" ? "top-right" : "top-left";
    const className = classnames(
      style.root,
      style[this.props.position],
      this.props.className,
      {
        [style.withBorder]: this.props.border,
      }
    );

    return (
      <div className={className}>
        <Button
          className={style.colorButton}
          style={{ backgroundColor: this.props.value }}
          type="button"
          floating
          mini
          theme={this.props.theme}
          onClick={this.handleButtonClick}
        >
          <div className={style["edit-overlay"]}>
            <FontIcon value={"edit"} className={style["edit-icon"]} />
          </div>
        </Button>
        {this.state.pickerActive ? (
          <div className={style.picker}>
            <TwitterPicker
              triangle={pickerTriangle}
              color={this.props.value}
              width="240px"
              colors={PROJECT_COLORS}
              onChangeComplete={this.handleChangeComplete}
            />
          </div>
        ) : null}
      </div>
    );
  }
}

ColorPicker.propTypes = {
  className: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  position: PropTypes.oneOf(["left", "right"]),
  border: PropTypes.bool,
  theme: PropTypes.object,
};

ColorPicker.defaultProps = {
  border: false,
  className: "",
  position: "left",
  value: "#888888",
};

export default onClickOutside(ColorPicker);
