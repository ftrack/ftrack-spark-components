// :copyright: Copyright (c) 2017 ftrack
import PropTypes from "prop-types";

import { Component } from "react";
import log from "loglevel";

import ColorPicker from "./component";

/** Project color picker */
class ProjectColorPicker extends Component {
  constructor(props) {
    super(props);
    this.state = { value: null };
    this.handleChange = this.handleChange.bind(this);
    this.handleProjectIdChanged = this.handleProjectIdChanged.bind(this);
  }

  componentDidMount() {
    this.handleProjectIdChanged(this.props.projectId);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.projectId !== this.props.projectId) {
      this.handleProjectIdChanged(nextProps.projectId);
    }
  }

  handleProjectIdChanged(projectId) {
    this.setState({ value: null });

    if (!projectId) {
      return;
    }

    this.props.session
      .query(`select color from Project where id is "${projectId}"`)
      .then((response) => {
        const color = response.data[0].color;
        this.setState({ value: color });
      });
  }

  handleChange(value) {
    const projectId = this.props.projectId;
    log.debug("Changing project color", projectId, value);
    if (!projectId) {
      return;
    }
    const oldValue = this.state.value;
    this.setState({ value });
    this.props.session
      .update("Project", [projectId], { color: value })
      .then((response) => {
        if (this.props.onSuccess) {
          this.props.onSuccess({ response, projectId, value });
        }
      })
      .catch((error) => {
        this.setState({ value: oldValue });
        if (this.props.onError) {
          this.props.onError(error);
        }
      });
  }

  render() {
    return (
      <ColorPicker
        className={this.props.className}
        value={this.state.value}
        onChange={this.handleChange}
      />
    );
  }
}

ProjectColorPicker.propTypes = {
  session: PropTypes.shape({
    query: PropTypes.func.isRequired,
    update: PropTypes.func.isRequired,
  }).isRequired,
  projectId: PropTypes.string,
  className: PropTypes.string,
  onSuccess: PropTypes.func,
  onError: PropTypes.func,
};

ProjectColorPicker.defaultProps = {};

export default ProjectColorPicker;
