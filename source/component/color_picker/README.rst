..
    :copyright: Copyright (c) 2017 ftrack

############
Color picker
############

A color picker component. Displays a color circle, which when clicked opens the
TwitterPicker from react-color.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
className                      CSS class for root element
value                          6-digit HEX color with leading #
onChange                       Called when color changes with value
position           left        Can be left or right-aligned
border                         Include a border around circle, as when on a thumbnail


Project color picker
--------------------

Can be used to view and edit project color.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
className                      CSS class for root element
session                        API Session instance
projectId                      Id of project
onSuccess                      Callback on success
onError                        Callback on error
