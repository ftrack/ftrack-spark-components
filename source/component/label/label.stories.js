import centered from "@storybook/addon-centered/react";
import Label from "./component";

export default {
  title: "Label",
  decorators: [centered],
};

export const DefaultColor = () => <Label>Default</Label>;

DefaultColor.storyName = "Default color";

export const DarkColor = () => (
  <Label color="#442244">Custom color (#442244)</Label>
);

DarkColor.storyName = "Dark color";

export const LightColor = () => (
  <Label color="#FFFF88">Custom color (#FFFF88)</Label>
);

LightColor.storyName = "Light color";

export const Approved = () => <Label color="positive">Approved</Label>;

export const RequireChanges = () => (
  <Label color="negative">Required changes</Label>
);

RequireChanges.storyName = "Require changes";

export const ClientFeedback = () => (
  <Label color="feedback">Client feedback</Label>
);

ClientFeedback.storyName = "Client feedback";
