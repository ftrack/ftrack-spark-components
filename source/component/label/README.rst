.. 

:copyright: Copyright (c) 2019 ftrack

######
Label
######

The Label component can be used for highlighting important information about an item. 

The color property will be used as the background color. You can specify it as 
a 6-digit hex color, or as one of the following theme colors.

* positive
* negative
* feedback
