// :copyright: Copyright (c) 2019 ftrack
import PropTypes from "prop-types";
import classNames from "classnames";

import { getForegroundColor } from "../util/color";
import style from "./style.scss";

// Theme colors.
const THEME_COLORS = ["positive", "negative", "feedback"];

function Label({ color, children, className }) {
  let backgroundColor;
  let foregroundColor;

  const colorClasses = classNames(style.colorContainer, className, {
    [style.positiveColor]: color === "positive",
    [style.negativeColor]: color === "negative",
    [style.feedbackColor]: color === "feedback",
  });

  if (color && !THEME_COLORS.includes(color)) {
    backgroundColor = color;
    foregroundColor = getForegroundColor(color);
  }

  return (
    <span
      style={{ backgroundColor, color: foregroundColor }}
      className={colorClasses}
    >
      {children}
    </span>
  );
}

Label.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(THEME_COLORS), PropTypes.string]),
  children: PropTypes.node,
  className: PropTypes.string,
};

export default Label;
