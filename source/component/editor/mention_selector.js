// :copyright: Copyright (c) 2021 ftrack

import { memo } from "react";
import PropTypes from "prop-types";
import { Popper } from "@mui/material";

import {
  ResourceCircle,
  ResourceOptionSubtitle,
} from "../selector/resource_selector";
import { localizeResourceSelector } from "../selector/localize_selector";
import safeInjectIntl from "../util/hoc/safe_inject_intl";

import style from "./mention_selector.scss";

const MentionSelectorItem = ({ session, ...props }) => {
  const {
    focusedOption,
    focusOption,
    key,
    option,
    selectValue,
    userLabel,
    userInactiveLabel,
    groupLabel,
  } = props;

  const className = [style["mention-option--resource"]];
  if (option === focusedOption) {
    className.push(style["mention-option--focused"]);
  }
  if (option.disabled) {
    className.push(style["mention-option--disabled"]);
  }

  const events = option.disabled
    ? {}
    : {
        onClick: (e) => selectValue(option, e),
        onMouseOver: () => focusOption(option),
      };

  const { data, label, type } = option;

  const scrollTo = (ref) => {
    if (ref && option === focusedOption) {
      ref.scrollIntoView({
        block: "nearest",
        inline: "nearest",
      });
    }
  };

  return (
    <div
      data-mention
      className={className.join(" ")}
      key={key}
      ref={scrollTo}
      {...events}
    >
      <div className={style["mention-option--container"]}>
        {type !== "label" ? (
          <ResourceCircle
            className={style["mention-option--avatar"]}
            session={session}
            type={type}
            data={data}
            label={label}
          />
        ) : null}
        <span className={style["mention-option--label"]}>{label}</span>
        <span className={style["mention-option--dash"]}>-</span>
        <ResourceOptionSubtitle
          className={style["mention-option--subtitle"]}
          type={type}
          userLabel={userLabel}
          userInactiveLabel={userInactiveLabel}
          groupLabel={groupLabel}
          {...data}
        />
      </div>
    </div>
  );
};

MentionSelectorItem.propTypes = {
  session: PropTypes.object,
  focusedOption: PropTypes.object,
  focusOption: PropTypes.func,
  key: PropTypes.string,
  option: PropTypes.object,
  selectValue: PropTypes.func,
  groupLabel: PropTypes.node,
  userLabel: PropTypes.node,
  userInactiveLabel: PropTypes.node,
};

const MentionSelector = ({
  session,
  mentionState,
  setMentionState,
  ...props
}) => {
  const {
    show,
    decorationNode,
    items = [],
    focusedOption,
    command: insertMention,
    range,
  } = mentionState;
  const decorationVisible =
    decorationNode && decorationNode.offsetParent !== null;

  return (
    <Popper
      open={show && decorationVisible && items.length > 0}
      anchorEl={decorationNode}
      placement="top-start"
      style={{
        zIndex: 300,
      }}
    >
      <div className={style["mention-selector"]}>
        {items.map((mention) =>
          MentionSelectorItem({
            session,
            option: mention,
            focusedOption: focusedOption || items[0],
            selectValue: (option) => {
              insertMention({
                range,
                attrs: {
                  id: option.value,
                  label: option.label,
                  type: option.type,
                },
              });
            },
            focusOption: (option) => {
              setMentionState({
                ...mentionState,
                focusedOption: option,
              });
            },
            props,
          })
        )}
      </div>
    </Popper>
  );
};

MentionSelector.propTypes = {
  session: PropTypes.object,
  mentionState: PropTypes.shape({
    show: PropTypes.bool,
    items: PropTypes.array,
    range: PropTypes.object,
    command: PropTypes.func,
    focusedOption: PropTypes.object,
    decorationNode: PropTypes.object,
  }),
  setMentionState: PropTypes.func,
  groupLabel: PropTypes.node,
  userLabel: PropTypes.node,
  userInactiveLabel: PropTypes.node,
};

MentionSelector.defaultProps = {
  mentionState: {
    show: false,
    items: [],
  },
};

export default safeInjectIntl(localizeResourceSelector(memo(MentionSelector)));
