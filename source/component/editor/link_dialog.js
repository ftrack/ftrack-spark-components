// :copyright: Copyright (c) 2021 ftrack

import { useEffect, useState, useRef } from "react";
import PropTypes from "prop-types";
import Dialog from "react-toolbox/lib/dialog";
import Input from "react-toolbox/lib/input";

import { FormattedMessage, defineMessages } from "react-intl";

import safeInjectIntl from "../util/hoc/safe_inject_intl";

const messages = defineMessages({
  "link-uri-placeholder": {
    id: "ftrack-spark-components.editor.link-uri-placeholder",
    description: "Placeholder for the link URI input field",
    defaultMessage: "Link",
  },
  "link-title-placeholder": {
    id: "ftrack-spark-components.editor.link-title-placeholder",
    description: "Placeholder for the link title input field",
    defaultMessage: "Title",
  },
  "cancel-button-title": {
    id: "ftrack-spark-components.editor.cancel-button-title",
    description: "Title for cancel button",
    defaultMessage: "Cancel",
  },
  "insert-link-button-title": {
    id: "ftrack-spark-components.editor.insert-link-button-title",
    description: "Title for insert link button",
    defaultMessage: "Insert link",
  },
});

const LinkDialog = ({ selection, onInsertLink, onCancel }) => {
  const [link, setLink] = useState({
    title: selection,
    href: "",
  });

  const linkInputRef = useRef();
  useEffect(
    () => {
      if (linkInputRef.current && !!selection) {
        linkInputRef.current.focus();
      }
    },
    // TODO: Feel free to fix this eslint error if you got time
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const insertLink = () => {
    const url = link.href.trim();
    onInsertLink({
      title: link.title,
      href: url.replace(/^(?!(?:\w+:)?\/\/)/, "http://"),
    });
  };

  const actions = [
    {
      label: <FormattedMessage {...messages["cancel-button-title"]} />,
      onClick: onCancel,
    },
    {
      label: <FormattedMessage {...messages["insert-link-button-title"]} />,
      onClick: insertLink,
      disabled: link.href === "",
    },
  ];

  const handleChange = (name, value) => {
    setLink({ ...link, [name]: value });
  };

  return (
    <Dialog
      className="ignore-react-onclickoutside"
      active
      type={"small"}
      actions={actions}
      onEscKeyDown={onCancel}
      onOverlayClick={onCancel}
    >
      <Input
        label={<FormattedMessage {...messages["link-title-placeholder"]} />}
        value={link.title}
        onChange={(value) => handleChange("title", value)}
      />
      <Input
        innerRef={linkInputRef}
        required
        type={"url"}
        label={<FormattedMessage {...messages["link-uri-placeholder"]} />}
        value={link.href}
        onChange={(value) => handleChange("href", value)}
      />
    </Dialog>
  );
};

LinkDialog.propTypes = {
  selection: PropTypes.string,
  onInsertLink: PropTypes.func,
  onCancel: PropTypes.func,
};

LinkDialog.defaultProps = {};

export default safeInjectIntl(LinkDialog);
