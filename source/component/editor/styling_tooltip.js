// :copyright: Copyright (c) 2021 ftrack

import { memo } from "react";
import PropTypes from "prop-types";
import withTooltip from "react-toolbox/lib/tooltip";
import { FormattedMessage, defineMessages } from "react-intl";
import { Popper } from "@mui/material";

import safeInjectIntl from "../util/hoc/safe_inject_intl";
import IconButton from "../icon_button";

import { generateGetBoundingClientRect } from "./utils";
import schema from "./schema";

import coreStyle from "./style.scss";
import buttonTheme from "./tooltip_button_theme.scss";
import activeButtonTheme from "./tooltip_active_button_theme.scss";

const messages = defineMessages({
  "bold-format-tooltip": {
    id: "ftrack-spark-components.editor.bold-format-tooltip",
    description: 'Tooltip for "bold formatting in editor bar"',
    defaultMessage: "Bold",
  },
  "italic-format-tooltip": {
    id: "ftrack-spark-components.editor.italic-format-tooltip",
    description: 'Tooltip for "italic formatting in editor bar"',
    defaultMessage: "Italic",
  },
  "strikethrough-format-tooltip": {
    id: "ftrack-spark-components.editor.strikethrough-format-tooltip",
    description: 'Tooltip for "strikethrough formatting in editor bar"',
    defaultMessage: "Strikethrough",
  },
  "insert-link-format-tooltip": {
    id: "ftrack-spark-components.editor.insert-link-format-tooltip",
    description: 'Tooltip for "insert link in editor bar"',
    defaultMessage: "Insert link",
  },
  "remove-link-format-tooltip": {
    id: "ftrack-spark-components.editor.remove-link-format-tooltip",
    description: 'Tooltip for "remove link in editor bar"',
    defaultMessage: "Remove link",
  },
  "list-format-tooltip": {
    id: "ftrack-spark-components.editor.list-format-tooltip",
    description: 'Tooltip for "list formatting in editor bar"',
    defaultMessage: "List",
  },
});

const TooltipIconButton = withTooltip(IconButton);

const StylingTooltip = ({
  onFormatBold,
  onFormatItalic,
  onFormatStrikethrough,
  onFormatList,
  onLiftList,
  onInsertLink,
  onRemoveLink,
  selectionState,
}) => (
  <Popper
    open={!selectionState.empty}
    anchorEl={selectionState.anchorEl || null}
    placement="top"
    style={{
      zIndex: 300,
    }}
  >
    <div className={coreStyle.tooltip}>
      <TooltipIconButton
        icon={"format_bold"}
        onClick={onFormatBold}
        tooltip={<FormattedMessage {...messages["bold-format-tooltip"]} />}
        tooltipPosition={"top"}
        theme={
          selectionState.selectedMarks.includes(schema.marks.strong)
            ? activeButtonTheme
            : buttonTheme
        }
      />
      <TooltipIconButton
        icon={"format_italic"}
        onClick={onFormatItalic}
        tooltip={<FormattedMessage {...messages["italic-format-tooltip"]} />}
        tooltipPosition={"top"}
        theme={
          selectionState.selectedMarks.includes(schema.marks.em)
            ? activeButtonTheme
            : buttonTheme
        }
      />
      <TooltipIconButton
        icon={"format_strikethrough"}
        onClick={onFormatStrikethrough}
        tooltip={
          <FormattedMessage {...messages["strikethrough-format-tooltip"]} />
        }
        tooltipPosition={"top"}
        theme={
          selectionState.selectedMarks.includes(schema.marks.strikethrough)
            ? activeButtonTheme
            : buttonTheme
        }
      />
      <div className={coreStyle.divider} />
      <TooltipIconButton
        icon={"insert_link"}
        onClick={
          selectionState.selectedMarks.includes(schema.marks.link)
            ? onRemoveLink
            : onInsertLink
        }
        tooltip={
          selectionState.selectedMarks.includes(schema.marks.link) ? (
            <FormattedMessage {...messages["remove-link-format-tooltip"]} />
          ) : (
            <FormattedMessage {...messages["insert-link-format-tooltip"]} />
          )
        }
        tooltipPosition={"top"}
        theme={
          selectionState.selectedMarks.includes(schema.marks.link)
            ? activeButtonTheme
            : buttonTheme
        }
      />
      <div className={coreStyle.divider} />
      <TooltipIconButton
        icon={"format_list_bulleted"}
        onClick={
          selectionState.selectedNodes.includes(schema.nodes.list_item)
            ? onLiftList
            : onFormatList
        }
        tooltip={<FormattedMessage {...messages["list-format-tooltip"]} />}
        tooltipPosition={"top"}
        theme={
          selectionState.selectedNodes.includes(schema.nodes.list_item)
            ? activeButtonTheme
            : buttonTheme
        }
      />
    </div>
  </Popper>
);

StylingTooltip.propTypes = {
  onFormatBold: PropTypes.func,
  onFormatItalic: PropTypes.func,
  onFormatStrikethrough: PropTypes.func,
  onFormatList: PropTypes.func,
  onLiftList: PropTypes.func,
  onInsertLink: PropTypes.func,
  onRemoveLink: PropTypes.func,
  selectionState: PropTypes.shape({
    empty: PropTypes.bool,
    anchorEl: PropTypes.object,
    selectedMarks: PropTypes.array,
  }),
};

StylingTooltip.defaultProps = {
  selectionState: {
    empty: true,
    anchorEl: generateGetBoundingClientRect(0, 0),
    selectedMarks: [],
  },
};

export default safeInjectIntl(memo(StylingTooltip));
