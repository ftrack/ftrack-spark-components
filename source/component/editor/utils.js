// :copyright: Copyright (c) 2021 ftrack

import { InputRule } from "prosemirror-inputrules";
import LinkifyIt from "linkify-it";
import { Fragment } from "prosemirror-model";
import debounce from "lodash/debounce";

import { MENTION_REGEX } from "../util/constant";

/** Matching data URIs of format: data:[<media type>][;base64],<data> */
const DATA_URL_REGEX =
  /^data:([a-z]+\/[a-z0-9-+.]+(;[a-z0-9-.!#$%*+.{}|~`]+=[a-z0-9-.!#$%*+.{}()|~`]+)*)?(;base64)?,([a-z0-9!$&',()*+;=\-._~:@/?%\s]*?)$/i;
const validDataUrl = (s) => DATA_URL_REGEX.test((s || "").trim());
const BASE64_MARKER = ";base64,";

const extensions = {
  "image/png": "png",
  "image/jpg": "jpg",
  "image/jpeg": "jpg",
  "image/gif": "gif",
};

const linkify = LinkifyIt();
const tlds =
  "biz|com|edu|gov|net|org|pro|web|xxx|aero|asia|coop|info|museum|name|shop|рф".split(
    "|"
  );

const tlds2Char =
  "a[cdefgilmnoqrtuwxz]|b[abdefghijmnorstvwyz]|c[acdfghiklmnoruvwxyz]|d[ejkmoz]|e[cegrstu]|f[ijkmor]|g[abdefghilmnpqrstuwy]|h[kmnrtu]|i[delmnoqrst]|j[emop]|k[eghimnprwyz]|l[abcikrstuvy]|m[acdeghklmnopqrtuvwxyz]|n[acefgilopruz]|om|p[aefghkmnrtw]|qa|r[eosuw]|s[abcdegijklmnrtuvxyz]|t[cdfghjklmnortvwz]|u[agksyz]|v[aceginu]|w[fs]|y[et]|z[amw]";

tlds.push(tlds2Char);
linkify.tlds(tlds, false);

const fetchImagesAndConvertToFiles = (images) => {
  const files = [];
  images.forEach((i) => {
    if (validDataUrl(i.src)) {
      const mime = i.src.split(BASE64_MARKER)[0].split(":")[1];
      const bytes = window.atob(i.src.split(BASE64_MARKER)[1]);
      const writer = new Uint8Array(new ArrayBuffer(bytes.length));

      for (let b = 0; b < bytes.length; b += 1) {
        writer[b] = bytes.charCodeAt(b);
      }
      const fileName = `Pasted Image.${extensions[mime]}`;

      files.push(new File([writer.buffer], fileName, { type: mime }));
    }
  });
  return files;
};

class LinkMatcher {
  // eslint-disable-next-line class-methods-use-this
  exec(str) {
    if (str.endsWith(" ")) {
      const chunks = str.slice(0, str.length - 1).split(" ");
      const lastChunk = chunks[chunks.length - 1];
      const links = linkify.match(lastChunk);
      if (links && links.length > 0) {
        const lastLink = links[links.length - 1];
        lastLink.input = lastChunk;
        lastLink.length = lastLink.lastIndex - lastLink.index + 1;
        return [lastLink];
      }
    }
    return null;
  }
}

const createLinkInputRule = (regexp) =>
  // Plain typed text (eg, typing 'www.google.com') should convert to a hyperlink
  new InputRule(regexp, (state, match, start, end) => {
    const { schema, tr } = state;
    if (state.doc.rangeHasMark(start, end, schema.marks.link)) {
      return null;
    }
    const { url, input, lastIndex } = match[0];
    const markType = schema.mark("link", { href: url });
    tr.addMark(
      start - (input.length - lastIndex),
      end - (input.length - lastIndex),
      markType
    ).insertText(" ");
    return tr;
  });

const replaceText =
  (range = null, type, attrs = {}, fragment = Fragment.empty) =>
  (state, dispatch) => {
    const { $from, $to } = state.selection;
    const index = $from.index();
    const from = range ? range.from : $from.pos;
    const to = range ? range.to : $to.pos;

    if (!$from.parent.canReplaceWith(index, index, type)) {
      return false;
    }

    if (dispatch) {
      const tr = state.tr;
      tr.replaceWith(from, to, type.create(attrs, fragment));
      tr.insertText(" ");
      dispatch(tr);
    }

    return true;
  };

/** Matching format of @{NAME_OF_USER ():UUIDV4} */

/** Matching format of @NAME_OF_USER */
const MENTION_SUGGESTION_REGEX =
  /@([\p{L}\p{Mn}0-9_]*\s?[\p{L}\p{Mn}0-9]*)?(?=\\s@|$)/gmu;

const extractMentions = (markdown) => {
  const matches = [...markdown.matchAll(MENTION_REGEX)];
  const mappedMatches = matches.map((match) => ({
    resource_id: match[2],
    text_mentioned: match[1],
  }));
  const uniqueRecipients = Array.from(
    new Set(mappedMatches.map((match) => match.resource_id))
  ).map((resourceId) =>
    mappedMatches.find((match) => match.resource_id === resourceId)
  );

  /**
   * If a mention with the same text referring to a different resource already exists,
   * append an incremental number within parentheses [e.g. John Doe (2)]
   */
  const mentionCounter = {};
  const deduplicatedMentionRecipients = uniqueRecipients.map((recipient) => {
    let textMentioned = recipient.text_mentioned;
    if (mentionCounter[textMentioned]) {
      mentionCounter[textMentioned] += 1;
      textMentioned = textMentioned.concat(
        ` (${mentionCounter[textMentioned]})`
      );
    } else {
      mentionCounter[textMentioned] = 1;
    }
    return {
      resource_id: recipient.resource_id,
      text_mentioned:
        mentionCounter[recipient.text_mentioned] > 1
          ? `@${recipient.text_mentioned} (${
              mentionCounter[recipient.text_mentioned]
            })`
          : `@${recipient.text_mentioned}`,
    };
  });

  return {
    markdown: markdown.replace(
      MENTION_REGEX,
      (match, p1, p2) =>
        deduplicatedMentionRecipients.find(
          (recipient) => recipient.resource_id === p2
        ).text_mentioned
    ),
    recipients: deduplicatedMentionRecipients,
  };
};

const injectMentions = (markdown, recipients = []) => {
  let injectedMarkdown = markdown;
  const filteredRecipients = recipients.filter(
    (recipient) => recipient.text_mentioned
  );
  filteredRecipients.sort(
    (r1, r2) => r2.text_mentioned.length - r1.text_mentioned.length
  );
  filteredRecipients.forEach((recipient) => {
    if (recipient.text_mentioned) {
      const mention = `@{${recipient.text_mentioned.substring(1)}:${
        recipient.resource_id
      }}`;
      injectedMarkdown = injectedMarkdown.replace(
        new RegExp(recipient.text_mentioned, "g"),
        mention
      );
    }
  });
  return injectedMarkdown;
};

const asyncDebounce = (func, wait) => {
  const debounced = debounce((resolve, reject, args) => {
    func(...args)
      .then(resolve)
      .catch(reject);
  }, wait);
  return (...args) =>
    new Promise((resolve, reject) => {
      debounced(resolve, reject, args);
    });
};

const generateGetBoundingClientRect = (x = 0, y = 0) => {
  return () => ({
    width: 0,
    height: 0,
    top: y,
    right: x,
    bottom: y,
    left: x,
  });
};

export {
  fetchImagesAndConvertToFiles,
  createLinkInputRule,
  LinkMatcher,
  replaceText,
  extractMentions,
  injectMentions,
  MENTION_REGEX,
  MENTION_SUGGESTION_REGEX,
  asyncDebounce,
  generateGetBoundingClientRect,
};
