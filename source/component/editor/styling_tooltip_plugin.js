// :copyright: Copyright (c) 2021 ftrack

import PropTypes from "prop-types";
import { EditorView } from "prosemirror-view";
import { Plugin } from "prosemirror-state";

import { generateGetBoundingClientRect } from "./utils";
import schema from "./schema";

/** ProseMirror Plugin to show styling tooltip for text selection
 * along with selected nodes and marks. */
class StylingTooltipPlugin {
  constructor({ editorView, onSelection }) {
    this.onSelection = onSelection;
    this.update(editorView, null);
  }

  update(view, lastState) {
    const { state } = view;

    if (
      lastState &&
      lastState.doc.eq(state.doc) &&
      lastState.selection.eq(state.selection)
    )
      return;

    const { from, $from, to, empty } = state.selection;

    const start = view.coordsAtPos(from);
    const end = view.coordsAtPos(to);
    const left = Math.max((start.left + end.left) / 2, start.left + 3);

    const anchorEl = {
      getBoundingClientRect: generateGetBoundingClientRect(
        left,
        start.top - 40
      ),
      contextElement: view.dom,
    };

    const selectedMarks = [
      schema.marks.strong,
      schema.marks.em,
      schema.marks.strikethrough,
      schema.marks.link,
    ].filter((m) => state.doc.rangeHasMark(from, to, m));
    const selectedNodes = [schema.nodes.list_item].filter(
      (n) => $from.node(2) && $from.node(2).type === n
    );

    this.onSelection({
      empty,
      anchorEl,
      selectedMarks,
      selectedNodes,
    });
  }
}

StylingTooltipPlugin.propTypes = {
  editorView: EditorView,
  onSelection: PropTypes.func,
};

const stylingTooltipPlugin = ({ onSelection }) =>
  new Plugin({
    view(editorView) {
      return new StylingTooltipPlugin({ editorView, onSelection });
    },
  });

export default stylingTooltipPlugin;
