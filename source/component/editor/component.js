// :copyright: Copyright (c) 2021 ftrack

import { useState, useRef, useEffect, useCallback } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { FormattedMessage, defineMessages } from "react-intl";
import { v4 as uuidV4 } from "uuid";
import useOnClickOutside from "use-onclickoutside";
import { compose } from "recompose";

import { useProseMirror, ProseMirror } from "use-prosemirror";
import { keymap } from "prosemirror-keymap";
import { dropCursor } from "prosemirror-dropcursor";
import { gapCursor } from "prosemirror-gapcursor";
import { baseKeymap, toggleMark } from "prosemirror-commands";
import { history, undo, redo } from "prosemirror-history";
import { liftListItem, wrapInList } from "prosemirror-schema-list";
import { Fragment, Slice } from "prosemirror-model";

import withTooltip from "react-toolbox/lib/tooltip";
import ProgressBar from "react-toolbox/lib/progress_bar";

import safeInjectIntl from "../util/hoc/safe_inject_intl";
import { withSession } from "../util/hoc";
import IconButton from "../icon_button";

import {
  fetchImagesAndConvertToFiles,
  extractMentions,
  injectMentions,
  asyncDebounce,
  MENTION_SUGGESTION_REGEX,
} from "./utils";

import schema, { markdownParser, markdownSerializer } from "./schema";
import buildKeymap from "./keymap";
import buildInputRules from "./inputrules";
import placeholderPlugin from "./placeholder_plugin";
import stylingTooltipPlugin from "./styling_tooltip_plugin";
import StylingTooltip from "./styling_tooltip";
import suggestionsPlugin from "./suggestions_plugin";
import { getLoadResourceOptions } from "../selector/resource_selector";
import { replaceHtmlBreaks } from "../markdown";

import style from "./style.scss";
import LinkDialog from "./link_dialog";
import MentionSelector from "./mention_selector";

const TooltipIconButton = withTooltip(IconButton);

const messages = defineMessages({
  "mention-button-tooltip": {
    id: "ftrack-spark-components.editor.mention-button-tooltip",
    description: 'Tooltip for "mention button in editor component"',
    defaultMessage: "Mention someone",
  },
  "emoji-button-tooltip": {
    id: "ftrack-spark-components.editor.emoji-button-tooltip",
    description: 'Tooltip for "mention button in editor component"',
    defaultMessage: "Emoji",
  },
  "attach-button-tooltip": {
    id: "ftrack-spark-components.editor.attach-button-tooltip",
    description: 'Tooltip for "attachment button in editor component"',
    defaultMessage: "Attach file",
  },
  "send-button-tooltip": {
    id: "ftrack-spark-components.editor.send-button-tooltip",
    description: 'Tooltip for "send button in editor component"',
    defaultMessage: "Send message",
  },
});

const MENTION_INPUT_DELAY = 100;

const editorState = ({
  content,
  placeholder,
  onSelection,
  canMention,
  onMentionEnter,
  onMentionChange,
  onMentionFilter,
  onMentionExit,
  onMentionKeyDown,
}) => {
  const plugins = [
    buildInputRules(schema),
    dropCursor(),
    gapCursor(),
    history(),
    placeholderPlugin({ content: placeholder }),
    stylingTooltipPlugin({ onSelection }),
  ];
  if (canMention) {
    plugins.push(
      suggestionsPlugin({
        matcher: {
          char: "@",
          regex: MENTION_SUGGESTION_REGEX,
        },
        onEnter: onMentionEnter,
        onChange: onMentionChange,
        onFilter: onMentionFilter,
        onExit: onMentionExit,
        onKeyDown: onMentionKeyDown,
      })
    );
  }
  plugins.push(keymap(buildKeymap(schema)), keymap(baseKeymap));

  return {
    nodes: schema.nodes,
    marks: schema.marks,
    doc: markdownParser.parse(replaceHtmlBreaks(content)),
    plugins,
  };
};

const Editor = ({
  className,
  session,
  autoFocus,
  legacyStyle,
  disabled,
  progressing,
  placeholder,
  submitTitle,
  content,
  recipients,
  canAttach,
  onChange,
  onAttach,
  onSend,
  onFocus,
  onBlur,
  onUploadStarted,
  onUploadProgress,
  onUploadError,
  onAbortedUpload,
  onFinalizedUpload,
  canMention,
}) => {
  const classes = classNames(
    legacyStyle ? style["wrapper-legacy"] : style.wrapper,
    className
  );
  const editorViewRef = useRef();
  const [selectionState, setSelectionState] = useState({
    selectedMarks: [],
    selectedNodes: [],
  });
  const [isFocused, setIsFocused] = useState(false);
  const [showLinkDialog, setShowLinkDialog] = useState(false);
  const [mentionState, setMentionState] = useState({
    show: false,
    focusedOption: null,
  });

  // TODO: Feel free to fix this eslint error if you got time
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const fetchMentionOptions = useCallback(
    asyncDebounce(
      getLoadResourceOptions({
        session,
        maxResults: 25,
        activeUsersOnly: true,
        groups: true,
        memberships: false,
      }),
      MENTION_INPUT_DELAY
    ),
    []
  );

  const [state, setState] = useProseMirror(
    editorState({
      content: injectMentions(content, recipients),
      placeholder,
      onSelection: setSelectionState,
      canMention,
      onMentionEnter: (props) => {
        setMentionState({
          show: true,
          focusedOption: null,
          ...props,
        });
      },
      onMentionFilter: async (query) => {
        const { options } = await fetchMentionOptions(query);
        return options;
      },
      onMentionChange: (props) => {
        setMentionState((prevState) => ({
          ...prevState,
          show: true,
          ...props,
        }));
      },
      onMentionExit: (props) => {
        setMentionState({
          show: false,
          focusedOption: null,
          ...props,
        });
      },
      onMentionKeyDown: (props) => {
        const isArrowDown = props.event.keyCode === 40;
        const isArrowUp = props.event.keyCode === 38;
        const isEnterKey = props.event.keyCode === 13;
        const isEscKey = props.event.keyCode === 27;
        if (isArrowDown || isArrowUp) {
          props.event.preventDefault();
          setMentionState((prevState) => {
            let newFocusedOptionIndex = 0;
            if (prevState.focusedOption !== null && prevState.items.length) {
              const focusedOptionIndex = prevState.items.indexOf(
                prevState.focusedOption
              );
              if (
                isArrowDown &&
                focusedOptionIndex < prevState.items.length - 1
              ) {
                newFocusedOptionIndex = focusedOptionIndex + 1;
              } else if (isArrowUp && focusedOptionIndex > 0) {
                newFocusedOptionIndex = focusedOptionIndex - 1;
              } else if (isArrowUp && focusedOptionIndex === 0) {
                newFocusedOptionIndex = prevState.items.length - 1;
              }
            }
            return {
              ...prevState,
              focusedOption: prevState.items[newFocusedOptionIndex] || null,
            };
          });
        } else if (isEnterKey) {
          setMentionState((prevState) => {
            const focusedOption = prevState.focusedOption || prevState.items[0];
            if (focusedOption != null) {
              prevState.command({
                range: prevState.range,
                attrs: {
                  id: focusedOption.value,
                  label: focusedOption.label,
                  type: focusedOption.type,
                },
              });
            }
            return {
              ...prevState,
              focusedOption: null,
            };
          });
          return true;
        } else if (isEscKey) {
          setMentionState({
            ...props,
            show: false,
            focusedOption: null,
          });
          return true;
        }
        return false;
      },
    })
  );
  const hasContent = state.doc.childCount > 1 || state.doc.textContent.length;

  const sendAction = () => {
    const serializedMarkdown = markdownSerializer.serialize(state.doc);
    onSend(extractMentions(serializedMarkdown));
  };

  const uploadFiles = (files) => {
    const uploadItems = files.map((file) => ({
      id: uuidV4(),
      xhr: new XMLHttpRequest(),
      file,
    }));

    if (onUploadStarted) {
      const shouldContinue = onUploadStarted(uploadItems);
      if (shouldContinue === false) {
        return;
      }
    }

    const promises = files.map((file, index) => {
      const xhr = uploadItems[index].xhr;
      return session.createComponent(file, {
        onAbortedUpload,
        xhr,
        onProgress: (progress) =>
          onUploadProgress && onUploadProgress(progress, uploadItems[index].id),
        data: { id: uploadItems[index].id },
      });
    });

    promises.forEach((p) => {
      p.then((res) => {
        onFinalizedUpload(res[0].data.id);
      }).catch((err) => {
        onUploadError(err);
      });
    });
  };

  const pasteTransformer = (slice) => {
    const images = [];
    const children = [];
    slice.content.forEach((child) => {
      let newChild = child;
      if (child.type.name === "image") {
        images.push(child.attrs);
        newChild = Fragment.empty;
      } else {
        child.descendants((node, pos) => {
          if (node.type.name === "image") {
            images.push(child.attrs);
            newChild = newChild.replace(
              pos,
              pos + 1,
              new Slice(Fragment.empty, 0, 0)
            );
          }
        });
      }
      children.push(newChild);
    });

    if (canAttach) {
      const files = fetchImagesAndConvertToFiles(images);
      uploadFiles(files);
    }

    return new Slice(
      Fragment.fromArray(children),
      slice.openStart,
      slice.openEnd
    );
  };

  useEffect(
    () => {
      if (autoFocus) {
        editorViewRef.current.view.focus();
      }
    }, // TODO: Feel free to fix this eslint error if you got time
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const wrapperRef = useRef(null);
  useOnClickOutside(wrapperRef, (e) => {
    const formatToolSelected = [
      "format_bold",
      "format_italic",
      "format_strikethrough",
      "insert_link",
      "format_list_bulleted",
    ].includes(e.target.innerText);
    const ancestor = e.target.closest("[data-mention]");
    const mentionSelected = ancestor && ancestor.hasAttribute("data-mention");
    if (!formatToolSelected && !mentionSelected) {
      setSelectionState({
        ...selectionState,
        empty: true,
      });
    }
  });

  return (
    <div ref={wrapperRef} className={classes}>
      <ProseMirror
        ref={editorViewRef}
        className={style.editor}
        state={state}
        dispatchTransaction={(transaction) => {
          const newState = state.apply(transaction);
          if (transaction.docChanged) {
            const serializedMarkdown = markdownSerializer.serialize(
              newState.doc
            );
            onChange(extractMentions(serializedMarkdown));
          }
          setState(newState);
        }}
        handleDOMEvents={{
          blur() {
            onBlur();
            setIsFocused(false);
          },
          focus() {
            onFocus();
            setIsFocused(true);
          },
          beforeinput: (view, event) => {
            switch (event.inputType) {
              case "historyUndo":
                undo(view.state, view.dispatch);
                event.preventDefault();
                return true;
              case "historyRedo":
                redo(view.state, view.dispatch);
                event.preventDefault();
                return true;
              default:
                return false;
            }
          },
        }}
        transformPasted={pasteTransformer}
      />
      {legacyStyle && (
        <div className={style["bottom-bar"]}>
          {canMention && (
            <TooltipIconButton
              icon={"alternate_email"}
              onClick={() => {
                const tr = state.tr.insertText(" @");
                editorViewRef.current.view.dispatch(tr);
                editorViewRef.current.view.focus();
              }}
              tooltip={
                <FormattedMessage {...messages["mention-button-tooltip"]} />
              }
              tooltipPosition={"top"}
            />
          )}
          <span className={`${isFocused ? style["bar-focused"] : style.bar}`} />
        </div>
      )}
      {!legacyStyle && (
        <div
          className={style["bottom-menu"]}
          style={{
            position: hasContent ? "initial" : "absolute",
          }}
        >
          {canAttach && (
            <TooltipIconButton
              icon={"attach_file"}
              onClick={onAttach}
              tooltip={
                <FormattedMessage {...messages["attach-button-tooltip"]} />
              }
              tooltipPosition={"top"}
            />
          )}
          <div
            className={`${style["send-button"]} ${hasContent && style.active}`}
          >
            {progressing ? (
              <ProgressBar
                className={style.spinner}
                type="circular"
                mode="indeterminate"
              />
            ) : (
              <TooltipIconButton
                icon={"send"}
                onClick={sendAction}
                tooltip={
                  submitTitle || (
                    <FormattedMessage {...messages["send-button-tooltip"]} />
                  )
                }
                tooltipPosition={"top"}
                disabled={!hasContent}
              />
            )}
          </div>
        </div>
      )}
      <StylingTooltip
        isFocused={isFocused}
        selectionState={selectionState}
        onFormatBold={() => {
          toggleMark(schema.marks.strong)(
            state,
            editorViewRef.current.view.dispatch
          );
          editorViewRef.current.view.focus();
        }}
        onFormatItalic={() => {
          toggleMark(schema.marks.em)(
            state,
            editorViewRef.current.view.dispatch
          );
          editorViewRef.current.view.focus();
        }}
        onFormatStrikethrough={() => {
          toggleMark(schema.marks.strikethrough)(
            state,
            editorViewRef.current.view.dispatch
          );
          editorViewRef.current.view.focus();
        }}
        onInsertLink={() => {
          setSelectionState({
            ...selectionState,
            empty: true,
          });
          setShowLinkDialog(!showLinkDialog);
        }}
        onRemoveLink={() => {
          toggleMark(schema.marks.link)(
            state,
            editorViewRef.current.view.dispatch
          );
          editorViewRef.current.view.focus();
        }}
        onFormatList={() => {
          wrapInList(schema.nodes.bullet_list)(
            state,
            editorViewRef.current.view.dispatch
          );
          editorViewRef.current.view.focus();
        }}
        onLiftList={() => {
          liftListItem(schema.nodes.list_item)(
            state,
            editorViewRef.current.view.dispatch
          );
          editorViewRef.current.view.focus();
        }}
      />
      {showLinkDialog && (
        <LinkDialog
          selection={state.selection.$head.parent.textContent}
          onCancel={() => setShowLinkDialog(false)}
          onInsertLink={(link) => {
            toggleMark(schema.marks.link, link)(
              state,
              editorViewRef.current.view.dispatch
            );
            editorViewRef.current.view.focus();
            setShowLinkDialog(false);
          }}
        />
      )}
      {canMention && (
        <MentionSelector
          session={session}
          mentionState={mentionState}
          setMentionState={setMentionState}
        />
      )}
    </div>
  );
};

Editor.propTypes = {
  session: PropTypes.shape({
    createComponent: PropTypes.func.isRequired,
  }).isRequired,
  className: PropTypes.string,
  legacyStyle: PropTypes.bool,
  autoFocus: PropTypes.bool,
  disabled: PropTypes.bool,
  progressing: PropTypes.bool,
  placeholder: PropTypes.string,
  submitTitle: PropTypes.string,
  content: PropTypes.string,
  recipients: PropTypes.arrayOf(
    PropTypes.shape({
      resource_id: PropTypes.string,
      text_mentioned: PropTypes.string,
    })
  ),
  canAttach: PropTypes.bool,
  onAttach: PropTypes.func,
  onChange: PropTypes.func,
  onSend: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onUploadStarted: PropTypes.func,
  onUploadProgress: PropTypes.func,
  onUploadError: PropTypes.func,
  onAbortedUpload: PropTypes.func,
  onFinalizedUpload: PropTypes.func,
  canMention: PropTypes.bool,
};

Editor.defaultProps = {
  legacyStyle: false,
  autoFocus: false,
  disabled: false,
  progressing: false,
  placeholder: "Write something...",
  content: "",
  recipients: [],
  canAttach: false,
  onAttach: () => {},
  onChange: () => {},
  onSend: () => {},
  onFocus: () => {},
  onBlur: () => {},
  onFinalizedUpload: () => {},
  canMention: false,
};

export default compose(withSession, safeInjectIntl)(Editor);
