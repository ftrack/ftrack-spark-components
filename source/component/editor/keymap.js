// :copyright: Copyright (c) 2021 ftrack

import { toggleMark, lift, selectParentNode } from "prosemirror-commands";
import {
  wrapInList,
  splitListItem,
  liftListItem,
  sinkListItem,
} from "prosemirror-schema-list";
import { undo, redo } from "prosemirror-history";
import { undoInputRule } from "prosemirror-inputrules";

const mac =
  // eslint-disable-next-line no-undef
  typeof navigator !== "undefined" ? /Mac/.test(navigator.platform) : false;

// Key Bindings
// * **Mod-b** for toggling [strong](#schema-basic.StrongMark)
// * **Mod-i** for toggling [emphasis](#schema-basic.EmMark)
// * **Ctrl-Shift-8** to wrap the selection in an ordered list
// * **Ctrl-Shift-9** to wrap the selection in a bullet list
// * **Ctrl->** to wrap the selection in a block quote
// * **Enter** to split a non-empty textblock in a list item while at
//   the same time splitting the list item
// * **Mod-_** to insert a horizontal rule
// * **Backspace** to undo an input rule
// * **Mod-BracketLeft** to `lift`
// * **Escape** to `selectParentNode`
// * **Mod-Shift-x** for toggling strikethough
// * **Tab** to sink list

export default function buildKeymap(schema) {
  const keys = {};

  function bind(key, cmd) {
    keys[key] = cmd;
  }

  bind("Mod-z", undo);
  bind("Shift-Mod-z", redo);
  bind("Backspace", undoInputRule);
  if (!mac) bind("Mod-y", redo);

  bind("Mod-BracketLeft", lift);
  bind("Escape", selectParentNode);

  bind("Mod-b", toggleMark(schema.marks.strong));
  bind("Mod-B", toggleMark(schema.marks.strong));

  bind("Mod-i", toggleMark(schema.marks.em));
  bind("Mod-I", toggleMark(schema.marks.em));

  bind("Mod-Shift-x", toggleMark(schema.marks.strikethrough));
  bind("Mod-Shift-X", toggleMark(schema.marks.strikethrough));

  bind("Shift-Ctrl-8", wrapInList(schema.nodes.bullet_list));
  bind("Shift-Ctrl-9", wrapInList(schema.nodes.ordered_list));

  bind("Enter", splitListItem(schema.nodes.list_item));
  bind("Mod-[", liftListItem(schema.nodes.list_item));
  bind("Mod-]", sinkListItem(schema.nodes.list_item));
  bind("Tab", sinkListItem(schema.nodes.list_item));

  const hr = schema.nodes.horizontal_rule;
  bind("Mod-_", (state, dispatch) => {
    dispatch(state.tr.replaceSelectionWith(hr.create()).scrollIntoView());
    return true;
  });

  return keys;
}
