// :copyright: Copyright (c) 2021 ftrack

import { Plugin } from "prosemirror-state";
import { Decoration, DecorationSet } from "prosemirror-view";

import style from "./style.scss";

/** ProseMirror Plugin to show placeholder on empty editor state. */
const PlaceholderPlugin = ({ content }) =>
  new Plugin({
    props: {
      decorations: (state) => {
        const decorations = [];

        const decorate = (node, pos) => {
          const hasContent =
            state.doc.childCount > 1 || state.doc.textContent.length;
          if (node.type.isBlock && node.childCount === 0 && !hasContent) {
            decorations.push(
              Decoration.node(pos, pos + node.nodeSize, {
                class: style["empty-node"],
                "data-content": content,
              })
            );
          }
        };

        state.doc.descendants(decorate);
        return DecorationSet.create(state.doc, decorations);
      },
    },
  });

export default PlaceholderPlugin;
