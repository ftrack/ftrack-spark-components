import centered from "@storybook/addon-centered/react";
import { action } from "@storybook/addon-actions";

import getSession from "../../story/get_session";
import Editor from "./component";
import { SparkProvider } from "../util/hoc";

function Provider({ story, session }) {
  return <SparkProvider session={session}>{story}</SparkProvider>;
}

export default {
  title: "Editor",
  decorators: [
    centered,
    (Story) => <Provider story={<Story />} session={getSession()} />,
  ],
};

export const DefaultMarkdownEditor = () => (
  <Editor
    onSend={action("Editor send")}
    onChange={action("Editor changed")}
    content={`This is an example of a CommonMark editor.

We support **bold**, *italic*,  ~~strikethrough~~ and all of them ***~~together.~~***

Along with autolinking <https://www.ftrack.com>

And lists:
- this is
- an example of
- a list
`}
  />
);

DefaultMarkdownEditor.storyName = "Default markdown editor";

export const MarkdownEditorWithLegacyStyling = () => (
  <Editor
    legacyStyle
    onChange={action("Editor changed")}
    content={`This is an example of a CommonMark editor.

We support **bold**, *italic*,  ~~strikethrough~~ and all of them ***~~together.~~***

Along with autolinking <https://www.ftrack.com>

And lists:
- this is
- an example of
- a list
`}
  />
);

MarkdownEditorWithLegacyStyling.storyName =
  "Markdown editor with legacy styling";

export const MarkdownEditorThatIsProgressing = () => (
  <Editor
    progressing
    onSend={action("Editor send")}
    onChange={action("Editor changed")}
    content={`This is an example of a CommonMark editor.

We support **bold**, *italic*,  ~~strikethrough~~ and all of them ***~~together.~~***

Along with autolinking <https://www.ftrack.com>

And lists:
- this is
- an example of
- a list
`}
  />
);

MarkdownEditorThatIsProgressing.storyName =
  "Markdown editor that is progressing";

export const MarkdownEditorWithAutofocusEnabled = () => (
  <Editor
    autoFocus
    content={`This is an example of a CommonMark editor.

We support **bold**, *italic*,  ~~strikethrough~~ and all of them ***~~together.~~***

Along with autolinking <https://www.ftrack.com>

And lists:
- this is
- an example of
- a list
`}
  />
);

MarkdownEditorWithAutofocusEnabled.storyName =
  "Markdown editor with autofocus enabled";

export const MarkdownEditorWithLinks = () => (
  <Editor
    autoFocus
    legacyStyle
    onChange={action("Editor changed")}
    content={`This is an example of a CommonMark editor.

We support **bold**, *italic*,  ~~strikethrough~~ and all of them ***~~together.~~***

Along with autolinking https://www.ftrack.com
`}
  />
);

MarkdownEditorWithLinks.storyName = "Markdown editor with links";

export const MarkdownEditorWithMentions = () => (
  <Editor
    autoFocus
    legacyStyle
    canMention
    onChange={action("Editor changed")}
    recipients={[
      {
        resource_id: "d86284ce-a059-11e9-82b8-d27cf242b68b",
        text_mentioned: "@FX",
      },
      {
        resource_id: "d86284ce-b059-11e9-82b8-d27cf242b68b",
        text_mentioned: "@Otto Müller",
      },
      {
        resource_id: "d86284ce-c059-11e9-82b8-d27cf242b68b",
        text_mentioned: "@c̳̻͚̻̩̻͉̯̄̏͑̋͆̎͐ͬ͑͌́͢h̵͔͈͍͇̪̯͇̞͖͇̜͉̪̪̤̙ͧͣ̓̐̓ͤ͋͒ͥ͑̆͒̓͋̑́͞ǎ̡̮̤̤̬͚̝͙̞͎̇ͧ͆͊ͅo̴̲̺͓̖͖͉̜̟̗̮̳͉̻͉̫̯̫̍̋̿̒͌̃̂͊̏̈̏̿ͧ́ͬ̌ͥ̇̓̀͢͜s̵̵̘̹̜̝̘̺̙̻̠̱͚̤͓͚̠͙̝͕͆̿̽ͥ̃͠͡",
      },
    ]}
    content={`This is an example of a CommonMark editor.

We support **bold**, *italic*,  ~~strikethrough~~ and all of them ***~~together.~~***

Along with autolinking https://www.ftrack.com

@FX @Otto Müller @c̳̻͚̻̩̻͉̯̄̏͑̋͆̎͐ͬ͑͌́͢h̵͔͈͍͇̪̯͇̞͖͇̜͉̪̪̤̙ͧͣ̓̐̓ͤ͋͒ͥ͑̆͒̓͋̑́͞ǎ̡̮̤̤̬͚̝͙̞͎̇ͧ͆͊ͅo̴̲̺͓̖͖͉̜̟̗̮̳͉̻͉̫̯̫̍̋̿̒͌̃̂͊̏̈̏̿ͧ́ͬ̌ͥ̇̓̀͢͜s̵̵̘̹̜̝̘̺̙̻̠̱͚̤͓͚̠͙̝͕͆̿̽ͥ̃͠͡
`}
  />
);

MarkdownEditorWithMentions.storyName = "Markdown editor with mentions";
