// :copyright: Copyright (c) 2021 ftrack

import {
  schema,
  MarkdownParser,
  MarkdownSerializer,
  defaultMarkdownSerializer,
  defaultMarkdownParser,
} from "prosemirror-markdown";
import { Schema, Fragment } from "prosemirror-model";

import { markdown } from "../markdown";

const newMarks = schema.spec.marks.addToEnd("strikethrough", {
  parseDOM: [{ tag: "s" }],
  toDOM: () => ["s", 0],
});

const newNodes = schema.spec.nodes.addToEnd("mention", {
  attrs: {
    id: {},
    label: {},
  },
  group: "inline",
  inline: true,
  content: "text*",
  selectable: false,
  atom: true,
  toDOM: (node) => [
    "span",
    {
      class: "mention",
      "data-mention-id": node.attrs.id,
    },
    `@${node.attrs.label}`,
  ],
  parseDOM: [
    {
      tag: "span[data-mention-id]",
      getAttrs: (dom) => {
        const id = dom.getAttribute("data-mention-id");
        const label = dom.innerText.split("@").join("");
        return { id, label };
      },
      getContent: (dom, s) => {
        const label = dom.innerText.split("@").join("");
        return Fragment.fromJSON(s, [{ type: "text", text: `@${label}` }]);
      },
    },
  ],
});

const customSchema = new Schema({
  ...schema.spec,
  marks: newMarks,
  nodes: newNodes,
});

const markdownParser = new MarkdownParser(customSchema, markdown, {
  ...defaultMarkdownParser.tokens,
  s: { mark: "strikethrough" },
  mention: {
    node: "mention",
    getAttrs: (tok) => ({
      id: tok.attrGet("id"),
      label: tok.attrGet("label"),
    }),
  },
});

const markdownSerializer = new MarkdownSerializer(
  {
    ...defaultMarkdownSerializer.nodes,
    mention(state, node) {
      state.write(`@{${node.attrs.label}:${node.attrs.id}}`);
    },
  },
  {
    ...defaultMarkdownSerializer.marks,
    strikethrough: {
      open: "~~",
      close: "~~",
      mixable: true,
      expelEnclosingWhitespace: true,
    },
  }
);

export { customSchema as default, markdownParser, markdownSerializer };
