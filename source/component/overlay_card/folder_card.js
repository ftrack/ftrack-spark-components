// :copyright: Copyright (c) 2019 ftrack

import PropTypes from "prop-types";
import FontIcon from "react-toolbox/lib/font_icon";
import classNames from "classnames";

import style from "./style.scss";

function FolderCard({ onClick, className, children, ...props }) {
  const classes = classNames(style.folderCard, className);

  return (
    <div className={classes} onClick={onClick} {...props}>
      <FontIcon value="folder" className={style.icon} />
      <div className={style.text}>{children}</div>
    </div>
  );
}

FolderCard.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func,
  className: PropTypes.string,
};

export default FolderCard;
