// :copyright: Copyright (c) 2019 ftrack

import centered from "@storybook/addon-centered/react";
import withState from "recompose/withState";

import {
  ThumbnailCard,
  FolderCard,
  OverlayCardTitle,
  OverlayCardSubtitle,
} from ".";

export default {
  title: "OverlayCard",
  decorators: [centered],
};

const ThumbnailCardWithState = withState(
  "selected",
  "onSelectedChange",
  ({ selected }) => selected
)(ThumbnailCard);
const image =
  "https://images.unsplash.com/photo-1551334787-21e6bd3ab135?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1651&q=80";

export const _ThumbnailCard = () => (
  <div>
    <ThumbnailCardWithState thumbnailUrl={image}>
      <OverlayCardTitle>Title</OverlayCardTitle>
      <OverlayCardSubtitle>Subtitle</OverlayCardSubtitle>
    </ThumbnailCardWithState>
    <br />
    <ThumbnailCard disabled thumbnailUrl={image}>
      <OverlayCardTitle>Disabled view.jpg</OverlayCardTitle>
      <OverlayCardSubtitle>Version 1</OverlayCardSubtitle>
    </ThumbnailCard>
    <br />
    <ThumbnailCard selected disabled thumbnailUrl={image}>
      <OverlayCardTitle>
        Ullamco officia aute id commodo laboris cillum non laboris excepteur.
      </OverlayCardTitle>
      <OverlayCardSubtitle>
        Version Quis eu ad sit adipisicing nisi minim nisi tempor.
      </OverlayCardSubtitle>
    </ThumbnailCard>
    <br />
    <ThumbnailCardWithState selected thumbnailUrl={image}>
      <OverlayCardTitle>Clicked view.jpg</OverlayCardTitle>
      <OverlayCardSubtitle>Version 7</OverlayCardSubtitle>
    </ThumbnailCardWithState>
  </div>
);

_ThumbnailCard.storyName = "Thumbnail card";

export const _FolderCard = () => (
  <div style={{ backgroundColor: "#30363C", padding: "50px" }}>
    <FolderCard>
      <OverlayCardTitle>Technology</OverlayCardTitle>
      <OverlayCardSubtitle>20 Items</OverlayCardSubtitle>
    </FolderCard>
    <br />
    <FolderCard>
      <OverlayCardTitle>Brain Food much much text here right</OverlayCardTitle>
      <OverlayCardSubtitle>10 Items very very much text</OverlayCardSubtitle>
    </FolderCard>
  </div>
);

_FolderCard.storyName = "Folder card";
