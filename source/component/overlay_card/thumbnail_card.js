// :copyright: Copyright (c) 2019 ftrack

import PropTypes from "prop-types";
import Checkbox from "react-toolbox/lib/checkbox";
import classNames from "classnames";

import style from "./style.scss";
import checkboxTheme from "./checkbox_theme.scss";
import ThumbnailOverlay from "./thumbnail_overlay";
import { Thumbnail } from "../image";

function ThumbnailCard({
  disabled,
  selected,
  thumbnailUrl,
  onSelectedChange,
  className,
  children,
  ...props
}) {
  const classes = classNames(style.thumbnailCard, className, {
    [style.selectedCard]: selected,
    [style.disabledCard]: disabled,
  });

  return (
    <Thumbnail
      src={thumbnailUrl}
      onClick={() => {
        onSelectedChange(!selected);
      }}
      className={classes}
      rounded
      {...props}
    >
      <ThumbnailOverlay justify="between" className={style.wrapper}>
        <div className={style.checkbox}>
          {disabled && !selected ? null : (
            <Checkbox
              disabled={disabled}
              checked={selected}
              theme={checkboxTheme}
            />
          )}
        </div>
        <div className={style.innerContent}>{children}</div>
      </ThumbnailOverlay>
    </Thumbnail>
  );
}

ThumbnailCard.propTypes = {
  disabled: PropTypes.bool,
  selected: PropTypes.bool,
  onClick: PropTypes.func,
  className: PropTypes.string,
  thumbnailUrl: PropTypes.string,
  onSelectedChange: PropTypes.func,
  children: PropTypes.node,
};

export default ThumbnailCard;
