// :copyright: Copyright (c) 2019 ftrack

import PropTypes from "prop-types";
import Heading from "../heading";

function OverlayCardTitle({ children, ...props }) {
  return (
    <Heading variant="subheading" color="light" noWrap {...props}>
      {children}
    </Heading>
  );
}

OverlayCardTitle.propTypes = {
  children: PropTypes.node,
};

export default OverlayCardTitle;
