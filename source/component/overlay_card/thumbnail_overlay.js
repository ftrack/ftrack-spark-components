// :copyright: Copyright (c) 2019 ftrack
import PropTypes from "prop-types";
import classNames from "classnames";
import style from "./thumbnail_overlay.scss";

function ThumbnailOverlay({
  className,
  children,
  visibility = "visible",
  justify = "start",
  ...props
}) {
  const classes = classNames(
    style.root,
    {
      [style.justifyEnd]: justify === "end",
      [style.justifyBetween]: justify === "between",
      [style.visibilityHover]: visibility === "hover",
    },
    className
  );

  return (
    <div className={classes} {...props}>
      {children}
    </div>
  );
}

ThumbnailOverlay.propTypes = {
  className: PropTypes.string,
  justify: PropTypes.oneOf(["start", "end", "between"]),
  visibility: PropTypes.oneOf(["visible", "hover"]),
  children: PropTypes.node,
};

export default ThumbnailOverlay;
