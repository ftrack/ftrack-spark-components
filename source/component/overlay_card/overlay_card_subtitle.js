// :copyright: Copyright (c) 2019 ftrack

import PropTypes from "prop-types";
import classNames from "classnames";
import Heading from "../heading";
import style from "./overlay_card_subtitle.scss";

function OverlayCardSubtitle({ className, children, ...props }) {
  const classes = classNames(style.subtitle, className);
  return (
    <Heading
      className={classes}
      variant="label"
      color="lightSecondary"
      noWrap
      {...props}
    >
      {children}
    </Heading>
  );
}

OverlayCardSubtitle.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};

export default OverlayCardSubtitle;
