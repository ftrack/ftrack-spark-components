// :copyright: Copyright (c) 2019 ftrack

import { action } from "@storybook/addon-actions";
import { SparkProvider } from "../util/hoc";
import getSession from "../../story/get_session";

import {
  AddProjectAccessDialog,
  ChangeProjectRoleDialog,
  UserRoleDialog,
  TwoFactorDialog,
  DisableTwoFactorDialog,
  GenerateBackupCodesDialog,
} from ".";

import ProjectAccessSwitch from "../create_project_dialog/project_access_switch";

export default {
  title: "Access dialog",
  decorators: [
    (Story) => (
      <SparkProvider session={getSession()}>
        <Story />
      </SparkProvider>
    ),
  ],
};

export const AddUserAccessToProject = () => (
  <AddProjectAccessDialog
    projectId="de85b97e-18bf-11e9-96ab-0a580ae40d77"
    onClose={action("Cancel")}
    onError={action("Error")}
    onSave={action("onSave")}
  />
);

AddUserAccessToProject.storyName = "Add user access to project";

export const ChangeUsersProjectRole = () => (
  <ChangeProjectRoleDialog
    projectId="35604cac-679e-11e7-bd87-0a580ae40a16"
    userId="e36c0546-fd18-11e3-8c06-04011030cf01"
    onClose={action("Cancel")}
    onError={action("Error")}
    onSave={action("Closing dialog, project access updated")}
  />
);

ChangeUsersProjectRole.storyName = "Change user's project role";

export const ChangeUser2SProjectRole = () => (
  <ChangeProjectRoleDialog
    projectId="35604cac-679e-11e7-bd87-0a580ae40a16"
    userId="c6824928-038e-11e4-83a3-04011030cf01"
    onClose={action("Cancel")}
    onError={action("Error")}
    onSave={action("Closing dialog, project access updated")}
  />
);

ChangeUser2SProjectRole.storyName = "Change user 2's project role";

export const ProjectAccessSettings = () => (
  <div style={{ maxWidth: 500 }}>
    <ProjectAccessSwitch
      value={false}
      product="studio"
      onChange={action("Change project access")}
    />
  </div>
);

ProjectAccessSettings.storyName = "Project access settings";

export const ManageUserRoles = () => (
  <UserRoleDialog
    userId="e36c0546-fd18-11e3-8c06-04011030cf01"
    onClose={action("Cancel")}
    onSave={action("onSave")}
  />
);

ManageUserRoles.storyName = "Manage user roles";

export const TwoFactorAuthenticationDialog = () => (
  <TwoFactorDialog
    onClose={action("Dialog was closed.")}
    onNoScanning={action("User had no scanner, show code.")}
    onVerification={action("User activated verification code.")}
  />
);

TwoFactorAuthenticationDialog.storyName = "Two factor authentication dialog";

export const DisableTwoFactorAuthenticationDialog = () => (
  <DisableTwoFactorDialog
    onClose={action("Dialog was closed")}
    onDisabled={action("Two-factor was disabled.")}
  />
);

DisableTwoFactorAuthenticationDialog.storyName =
  "Disable two factor authentication dialog";

export const _GenerateBackupCodesDialog = () => (
  <GenerateBackupCodesDialog onClose={action("Dialog was closed")} />
);

_GenerateBackupCodesDialog.storyName = "Generate backup codes dialog";
