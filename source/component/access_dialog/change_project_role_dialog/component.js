// :copyright: Copyright (c) 2019 ftrack
import { Component } from "react";
import PropTypes from "prop-types";
import { defineMessages, FormattedMessage } from "react-intl";
import Dialog from "react-toolbox/lib/dialog";
import ProgressBar from "react-toolbox/lib/progress_bar";
import { operation } from "ftrack-javascript-api";
import { RadioButton } from "react-toolbox/lib/radio";
import Checkbox from "react-toolbox/lib/checkbox";
import safeInjectIntl from "../../util/hoc/safe_inject_intl";
import withSession from "../../util/hoc/with_session";

import style from "./style.scss";
import dialogTheme from "./dialog_theme.scss";
import radioButtonTheme from "./radio_button_theme.scss";

const SELECTION_MULTI = "SELECTION_MULTI";

const messages = defineMessages({
  "title-user": {
    id: "ftrack-spark-components.change-access-dialog.title-user",
    description: "Title for dialog",
    defaultMessage: "Roles for {selectedUser}",
  },
  description: {
    id: "ftrack-spark-components.change-access-dialog.description",
    description: "Descriptive text for dialog",
    defaultMessage:
      "Select which role {selectedUser} should have in this project.",
  },
  save: {
    id: "ftrack-spark-components.change-access-dialog.save",
    description: "Descriptive text for dialog",
    defaultMessage: "Save",
  },
  cancel: {
    id: "ftrack-spark-components.change-access-dialog.cancel",
    description: "Descriptive text for dialog",
    defaultMessage: "Cancel",
  },
  "user-has-no-access": {
    id: "ftrack-spark-components.change-access-dialog.user-has-no-access",
    description: "Info text when user do not have access",
    defaultMessage:
      "{selectedUser} does not have any roles that can be applied. New roles can be added from system settings.",
  },
  "user-has-single-role": {
    id: "ftrack-spark-components.change-access-dialog.user-has-single-role",
    description: "Info text when user has singöe role.",
    defaultMessage:
      "{selectedUser} only has the {roleName} role. Additional roles can be given from system settings.",
  },
  "assignee-role": {
    id: "ftrack-spark-components.change-access-dialog.assignee-role",
    defaultMessage: "Added on projects with open access and assigned tasks",
  },
  "role-open-access": {
    id: "ftrack-spark-components.change-access-dialog.role-open-access",
    defaultMessage: "Added on projects with open access",
  },
});

function RoleLabel({ name, type, isAllProjects }) {
  if (type === "ASSIGNED") {
    return (
      <div>
        {name}
        <br />
        <FormattedMessage {...messages["assignee-role"]} />
      </div>
    );
  } else if (isAllProjects) {
    return (
      <div>
        {name}
        <br />
        <FormattedMessage {...messages["role-open-access"]} />
      </div>
    );
  }
  return <div>{name}</div>;
}

RoleLabel.propTypes = {
  name: PropTypes.string,
  type: PropTypes.string,
  isAllProjects: PropTypes.bool,
};

class ChangeProjectRoleDialog extends Component {
  constructor() {
    super();

    this.state = {
      processing: true,
      saveEnabled: false,
      selectedUser: null,
      roles: [],
      currentProjectRoleIds: [],
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSave = this.handleSave.bind(this);
  }

  componentDidMount() {
    const { userId, projectId, session } = this.props;
    if (!projectId) {
      return;
    }
    session
      .call([
        operation.query(
          "select security_role.name, security_role.type, security_role_id, is_all_open_projects " +
            `from UserSecurityRole where user_id is "${userId}"`
        ),
        operation.query(`select first_name from User where id is "${userId}"`),
        operation.query(
          `select is_private from Project where id is "${projectId}"`
        ),
        operation.query(
          "select user_security_role.security_role_id from UserSecurityRoleProject where " +
            `project_id is "${projectId}" and user_security_role.user_id is "${userId}"`
        ),
        operation.query(
          `select id from Appointment where resource_id is "${userId}" and context[TypedContext].project_id is "${projectId}" limit 1`
        ),
      ])
      .then(
        ([
          userSecurityRolesResponse,
          nameResponse,
          projectResponse,
          currentProjectRoleResponse,
          appointmentResponse,
        ]) => {
          const { first_name: firstName } = nameResponse.data[0];
          const { is_private: isPrivate } = projectResponse.data[0];
          const isAssigned = appointmentResponse.data.length > 0;

          const currentProjectRoleIds = currentProjectRoleResponse.data.map(
            (userSecurityRoleProject) =>
              userSecurityRoleProject.user_security_role.security_role_id
          );

          const roles = userSecurityRolesResponse.data
            .map((userSecurityRole) => ({
              id: userSecurityRole.security_role_id,
              isAllProjects:
                !isPrivate && userSecurityRole.is_all_open_projects,
              name: userSecurityRole.security_role.name,
              type: userSecurityRole.security_role.type,
            }))
            .sort((a, b) => a.name.localeCompare(b.name));

          const selectedRoleIds = roles
            .filter((role) => {
              if (role.type === "ASSIGNED") {
                return !isPrivate && isAssigned;
              }
              return (
                currentProjectRoleIds.includes(role.id) || role.isAllProjects
              );
            })
            .map((role) => role.id);

          const disabledRoleIds = roles
            .filter((role) => role.type === "ASSIGNED" || role.isAllProjects)
            .map((role) => role.id);

          this.setState({
            processing: false,
            selectedUser: firstName,
            currentProjectRoleIds,
            selectedRoleIds,
            disabledRoleIds,
            selectionMode: SELECTION_MULTI,
            roles,
          });
        }
      );
  }

  handleChange(roleId, selectionMode, value) {
    const { selectedRoleIds, disabledRoleIds } = this.state;
    let nextSelectedRoleIds = selectedRoleIds;

    if (selectionMode === SELECTION_MULTI) {
      if (value) {
        if (!selectedRoleIds.includes(roleId)) {
          nextSelectedRoleIds = [...selectedRoleIds, roleId];
        }
      } else {
        nextSelectedRoleIds = selectedRoleIds.filter(
          (selectedRoleId) => selectedRoleId !== roleId
        );
      }
    } else {
      const selectedAndDisabledRoleIds = disabledRoleIds.filter(
        (disabledRoleId) => selectedRoleIds.includes(disabledRoleId)
      );
      nextSelectedRoleIds = [...selectedAndDisabledRoleIds, roleId];
    }

    this.setState({
      selectedRoleIds: nextSelectedRoleIds,
      saveEnabled: selectedRoleIds.length > 0,
    });
  }

  handleSave() {
    const { projectId, userId, session } = this.props;
    const { selectedRoleIds, currentProjectRoleIds } = this.state;
    this.setState({ processing: true });

    const revokedRolesList = currentProjectRoleIds
      .filter(
        (currentRoleId) => selectedRoleIds.includes(currentRoleId) === false
      )
      .map((roleId) => ({
        action: "revoke_user_security_role_project",
        role_id: roleId,
        user_id: userId,
        project_id: projectId,
      }));

    const grantedProjectList = selectedRoleIds
      .filter(
        (selectedRoleId) =>
          currentProjectRoleIds.includes(selectedRoleId) === false
      )
      .map((roleId) => ({
        action: "grant_user_security_role_project",
        role_id: roleId,
        user_id: userId,
        project_id: projectId,
      }));

    session
      .call([...revokedRolesList, ...grantedProjectList])
      .then(() => {
        this.setState({ processing: false });
        this.props.onSave();
      })
      .catch((error) => {
        this.setState({ processing: false });
        if (this.props.onError) {
          this.props.onError(error);
        }
      });
  }

  render() {
    const { onClose } = this.props;
    const {
      processing,
      selectedUser,
      roles,
      saveEnabled,
      selectedRoleIds,
      disabledRoleIds,
      selectionMode,
    } = this.state;

    const actions = [
      {
        label: <FormattedMessage {...messages.cancel} />,
        onClick: onClose,
      },
      {
        label: <FormattedMessage {...messages.save} />,
        primary: saveEnabled,
        onClick: this.handleSave,
        disabled: !saveEnabled,
      },
    ];
    let content;
    if (processing) {
      content = (
        <ProgressBar
          className={style.spinner}
          type="circular"
          mode="indeterminate"
        />
      );
    } else if (roles.length === 0) {
      content = (
        <p className={style["no-user"]}>
          <FormattedMessage
            {...messages["user-has-no-access"]}
            values={{ selectedUser }}
          />
        </p>
      );
    } else if (roles.length === 1) {
      const roleName = <strong className={style.name}>{roles[0].name}</strong>;
      content = (
        <p className={style["no-user"]}>
          <FormattedMessage
            {...messages["user-has-single-role"]}
            values={{ selectedUser, roleName }}
          />
        </p>
      );
    } else {
      const SelectionComponent =
        selectionMode === SELECTION_MULTI ? Checkbox : RadioButton;
      content = (
        <div className={style["radio-container"]}>
          {roles.map((role) => {
            const onChange = (value) => {
              this.handleChange(role.id, selectionMode, value);
            };
            return (
              <SelectionComponent
                key={role.id}
                theme={radioButtonTheme}
                className={style["radio-buttons"]}
                checked={selectedRoleIds.includes(role.id)}
                disabled={disabledRoleIds.includes(role.id)}
                label={<RoleLabel {...role} />}
                onChange={onChange}
              />
            );
          })}
        </div>
      );
    }

    return (
      <Dialog
        active
        actions={actions}
        title={
          <FormattedMessage
            {...messages["title-user"]}
            values={{ selectedUser }}
          />
        }
        onEscKeyDown={onClose}
        theme={dialogTheme}
        type="small"
      >
        <p className={style.description}>
          <FormattedMessage
            {...messages.description}
            values={{ selectedUser }}
          />
        </p>
        {content}
      </Dialog>
    );
  }
}
ChangeProjectRoleDialog.propTypes = {
  onClose: PropTypes.func,
  userId: PropTypes.string.isRequired,
  projectId: PropTypes.string.isRequired,
  onSave: PropTypes.func,
  session: PropTypes.shape({
    call: PropTypes.func.isRequired,
  }),
  onError: PropTypes.func,
};

export default withSession(safeInjectIntl(ChangeProjectRoleDialog));
