// :copyright: Copyright (c) ftrack 2019

import { Component } from "react";
import Dialog from "react-toolbox/lib/dialog";
import { defineMessages, FormattedMessage, intlShape } from "react-intl";
import PropTypes from "prop-types";

import withSession from "../../util/hoc/with_session";
import safeInjectIntl from "../../util/hoc/safe_inject_intl";
import IconButton from "../../icon_button";
import TwoFactorVerification from "../two_factor_dialog/two_factor_verification";
import FormattedCode, {
  formatValueWithDashes,
} from "../two_factor_dialog/formatted_code";

import dialogTheme from "./dialog_theme.scss";
import style from "./style.scss";

const messages = defineMessages({
  title: {
    id: "ftrack-spark.components.generate-backup-codes-dialog.title",
    description: "Title message for dialog",
    defaultMessage: "Two-factor authentication backup codes",
  },
  subtitle: {
    id: "ftrack-spark-components.generate-backup-codes-dialog.subtitle",
    description: "Subtitle for the dialog",
    defaultMessage:
      "If you lose access to your authentication device, you can use one of these backup codes to sign " +
      "in to your account. Make a copy of these codes and store it somewhere safe.",
  },
  description: {
    id: "ftrack-spark-components.generate-backup-codes-dialog.description",
    description: "Subtitle for the dialog",
    defaultMessage:
      "Each code may be used only once and generating new codes will revoke any previously generated codes.",
  },
  "copy-button": {
    id: "ftrack-spark-components.generate-backup-codes-dialog.copy-button",
    description: "Copy button for the dialog",
    defaultMessage: "Copy codes",
  },
  "print-button": {
    id: "ftrack-spark-components.generate-backup-codes-dialog.print-button",
    description: "Cancel button for the dialog",
    defaultMessage: "Print codes",
  },
  "button-label-default": {
    id: "ftrack-spark-components.generate-backup-codes-dialog.button-label-default",
    description: "Button for getting codes",
    defaultMessage: "Verify and get codes",
  },
  "copied-code": {
    id: "ftrack-spark-components.generate-backup-codes-dialog.copied-code",
    description: "Message to user when codes are on clipboard",
    defaultMessage: "Codes copied to clipboard",
  },
  "print-window-title": {
    id: "ftrack-spark-components.generate-backup-codes-dialog.print-window-title",
    defaultMessage: "Two-factor authentication backup codes for ftrack",
  },
});

function isClipboardApiSupported() {
  return (
    window.navigator &&
    window.navigator.clipboard &&
    window.navigator.clipboard.writeText
  );
}

class GenerateBackupCodesDialog extends Component {
  constructor() {
    super();

    this.state = {
      processing: false,
      initialState: true,
      code: "",
      oneTimeCodes: "",
      error: false,
      copiedCode: null,
      printCode: false,
    };

    this.handleInputChanged = this.handleInputChanged.bind(this);
    this.handleVerification = this.handleVerification.bind(this);
    this.handleCopyingOfCodes = this.handleCopyingOfCodes.bind(this);
    this.handlePrintingOfCodes = this.handlePrintingOfCodes.bind(this);
  }

  handleInputChanged(value) {
    this.setState({ code: value });
  }

  handleCopyingOfCodes() {
    const { oneTimeCodes } = this.state;
    try {
      window.navigator.clipboard.writeText(oneTimeCodes).then(() => {
        this.setState({ copiedCode: true });
      });
    } catch (error) {
      this.setState({ copiedCode: false });
    }
  }

  handlePrintingOfCodes() {
    const { intl } = this.props;
    const { oneTimeCodes } = this.state;
    const myWindow = window.open("", "_blank");
    const title = intl.formatMessage(messages["print-window-title"]);
    myWindow.document.write(
      `<html>
            <head><title>${title}</title></head>
            <body>
            <h2>${title}</h2>
            <pre>${oneTimeCodes}</pre>
            </body>
            </html>`
    );
    myWindow.document.close();
    myWindow.focus();
    myWindow.print();
    myWindow.close();
    this.setState({ printCode: true });
  }

  handleVerification() {
    const { session } = this.props;
    const { code } = this.state;
    this.setState({ processing: true });
    session
      .call([{ action: "configure_otp", code }])
      .then(([response]) => {
        const codes = response.data.codes.map((x) => x);
        const oneTimeCodes = codes.reduce((result, item, index) => {
          const formattedItem = formatValueWithDashes(item);
          if (index % 2 === 0) {
            return `${result}${formattedItem}\t`;
          }
          return `${result}${formattedItem}\n`;
        }, "");

        this.setState({
          oneTimeCodes,
          processing: false,
          initialState: false,
        });
      })
      .catch((error) => {
        if (error.errorCode === "2fa_invalid_code") {
          this.setState({ processing: false, error: true });
        } else {
          if (this.props.onError) {
            this.props.onError(error);
          }
          this.setState({ processing: false });
        }
      });
  }

  render() {
    const { onClose } = this.props;
    const { initialState, oneTimeCodes, processing, error, code, copiedCode } =
      this.state;

    const actions = [];
    if (initialState === false) {
      if (isClipboardApiSupported()) {
        actions.push({
          label: <FormattedMessage {...messages["copy-button"]} />,
          onClick: this.handleCopyingOfCodes,
          primary: true,
        });
      }

      actions.push({
        label: <FormattedMessage {...messages["print-button"]} />,
        onClick: this.handlePrintingOfCodes,
        primary: true,
      });
    }
    return (
      <Dialog
        active
        title={<FormattedMessage {...messages.title} />}
        className={style.dialog}
        actions={actions}
        theme={dialogTheme}
        onEscKeyDown={onClose}
      >
        <IconButton
          icon="clear"
          onClick={onClose}
          className={style["close-button"]}
        />
        <p className={style.description}>
          <FormattedMessage {...messages.subtitle} />
        </p>
        <p className={style.description}>
          <FormattedMessage {...messages.description} />
        </p>
        {initialState ? (
          <TwoFactorVerification
            label={<FormattedMessage {...messages["button-label-default"]} />}
            onVerify={this.handleVerification}
            onChange={this.handleInputChanged}
            processing={processing}
            error={error}
            value={code}
            primary
          />
        ) : (
          <div className={style.codeContainer}>
            <FormattedCode className={style.code} value={oneTimeCodes} />
          </div>
        )}
        {copiedCode === true ? (
          <p className={style.copiedMessage}>
            <FormattedMessage {...messages["copied-code"]} />
          </p>
        ) : null}
      </Dialog>
    );
  }
}

GenerateBackupCodesDialog.propTypes = {
  intl: intlShape,
  onClose: PropTypes.func,
  onError: PropTypes.func,
  session: PropTypes.shape({
    call: PropTypes.func,
  }),
};

export default withSession(safeInjectIntl(GenerateBackupCodesDialog));
