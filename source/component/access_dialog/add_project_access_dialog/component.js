import { Component } from "react";
import Dialog from "react-toolbox/lib/dialog";
import { List, ListItem } from "react-toolbox/lib/list";
import { Button, IconButton } from "react-toolbox/lib/button";
import PropTypes from "prop-types";
import ProgressBar from "react-toolbox/lib/progress_bar";
import { defineMessages, FormattedMessage } from "react-intl";
import { compose, withProps } from "recompose";
import EntityAvatar from "../../entity_avatar";
import withSession from "../../util/hoc/with_session";
import ResourceSelector from "../../selector/resource_selector";
import RoleSelector from "./role_selector";

import style from "./style.scss";
import safeInjectIntl from "../../util/hoc/safe_inject_intl";
import listItemTheme from "./list_item_theme.scss";
import dialogTheme from "./dialog_theme.scss";

const messages = defineMessages({
  save: {
    id: "ftrack-spark-components.add-access-dialog.save",
    description: 'Text on "save"-button',
    defaultMessage: "Save",
  },
  cancel: {
    id: "ftrack-spark-components.add-access-dialog.cancel",
    description: 'Text on "cancel"-button',
    defaultMessage: "Cancel",
  },
  title: {
    id: "ftrack-spark-components.add-access-dialog.title",
    description: "Title text for dialog",
    defaultMessage: "Add user access to project",
  },
  "button-add": {
    id: "ftrack-spark-components.add-access-dialog.button-add",
    description: 'Text on "add"-button',
    defaultMessage: "Add",
  },
  "search-user-description": {
    id: "ftrack-spark-components.add-access-dialog.search-user-description",
    description: "Description for usage of dialog.",
    defaultMessage: "Select a user and role to add access to this project",
  },
  "search-user-hint": {
    id: "ftrack-spark-components.add-access-dialog.search-user-hint",
    description: "Information on how to use the selectors.",
    defaultMessage: "Search for a person by name or email address",
  },
  "user-label": {
    id: "ftrack-spark-components.add-access-dialog.user-label",
    description: 'Text for "user"-label',
    defaultMessage: "User",
  },
  "roles-label": {
    id: "ftrack-spark-components.add-access-dialog.roles-label",
    description: 'Text for "role"-label',
    defaultMessage: "Role",
  },
  "no-matching-users": {
    id: "ftrack-spark-components.add-access-dialog.no-matching-users",
    description: "Select field information if no users are matching",
    defaultMessage: "No matching users without access found",
  },
});

const UserSelector = compose(
  withProps({
    users: true,
  }),
  withSession
)(ResourceSelector);

const EntityAvatarWithSession = withSession(EntityAvatar);

function UserList({ users, onUserRemove, enableRemove, userId }) {
  return (
    <List className={style.userList}>
      {users.map((user) => (
        <ListItem
          theme={listItemTheme}
          className={style.list}
          key={`user-item-${user.id}`}
          caption={`${user.name}` || `${user.first_name} ${user.last_name}`}
          legend={`${user.role.name}`}
          rightActions={
            enableRemove && user.id !== userId
              ? [
                  <IconButton
                    icon="clear"
                    // eslint-disable-next-line react/jsx-no-bind
                    onClick={() => onUserRemove(user.id)}
                  />,
                ]
              : []
          }
        >
          <EntityAvatarWithSession entity={user} className={style["avatar"]} />
        </ListItem>
      ))}
    </List>
  );
}

UserList.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  users: PropTypes.array.isRequired,
  enableRemove: PropTypes.bool.isRequired,
  userId: PropTypes.string.isRequired,
  onUserRemove: PropTypes.func.isRequired,
};

class AddProjectAccessDialog extends Component {
  constructor() {
    super();

    this.state = {
      processing: false,
      selectedRole: null,
      selectedUser: null,
      add: [],
    };

    this.onUserChange = this.onUserChange.bind(this);
    this.onRoleChange = this.onRoleChange.bind(this);
    this.onAddClicked = this.onAddClicked.bind(this);
    this.onUserRemove = this.onUserRemove.bind(this);
    this.handleSave = this.handleSave.bind(this);
  }

  onAddClicked() {
    const { add } = this.state;
    const { selectedUser, selectedRole } = this.state;
    const nextAdd = [
      {
        thumbnail_id: selectedUser.data.thumbnailId,
        name: selectedUser.label,
        id: selectedUser.value,
        type: "user",
        role: {
          id: selectedRole.value,
          name: selectedRole.label,
        },
      },
      ...add,
    ];

    this.setState({ selectedRole: null, selectedUser: null, add: nextAdd });
  }

  onUserRemove(userId) {
    let { add } = this.state;
    if (add.some((candidate) => candidate.id === userId)) {
      add = add.filter((candidate) => candidate.id !== userId);
    }
    this.setState({ add });
  }

  onUserChange(selectedUser) {
    const selectedUserId = selectedUser && selectedUser.value;
    this.setState({ selectedUser, selectedUserId });
  }

  onRoleChange(selectedRole) {
    this.setState({ selectedRole });
  }

  handleSave() {
    const { projectId } = this.props;
    const { add } = this.state;
    this.setState({ processing: true });

    const operations = add.map((user) => ({
      action: "grant_user_security_role_project",
      role_id: user.role.id,
      user_id: user.id,
      project_id: projectId,
    }));

    this.props.session
      .call(operations)
      .then(() => {
        this.setState({ processing: false });
        this.props.onSave();
      })
      .catch((error) => {
        this.setState({ processing: false });
        if (this.props.onError) {
          this.props.onError(error);
        }
      });
  }

  render() {
    const { onClose, session, userId, projectId } = this.props;
    const { processing, selectedUser, add, selectedUserId, selectedRole } =
      this.state;
    const pendingChanges = add.length > 0;
    const actions = [
      {
        label: <FormattedMessage {...messages.cancel} />,
        onClick: onClose,
      },
      {
        label: <FormattedMessage {...messages.save} />,
        disabled: !pendingChanges || processing,
        primary: pendingChanges,
        onClick: this.handleSave,
      },
    ];

    let userFilters = `id is_not "${userId}" and is_active is true and
            id not_in (select id from User where user_security_roles.user_id is "${projectId}")`;

    if (add.length) {
      const quotedAdd = add.map((item) => `"${item.id}"`).join(", ");
      userFilters = `${userFilters} and id not_in (${quotedAdd})`;
    }

    return (
      <Dialog
        active
        actions={actions}
        theme={dialogTheme}
        onEscKeyDown={onClose}
        title={<FormattedMessage {...messages.title} />}
        type="small"
      >
        <div key="entry-section">
          <p className={style["users-description"]}>
            <FormattedMessage {...messages["search-user-description"]} />
          </p>
          <div className={style["users-fields"]}>
            <div className={style["select-user-field"]}>
              <label htmlFor="user" className={style["select-users-label"]}>
                <FormattedMessage {...messages["user-label"]} />
              </label>
              <UserSelector
                name="user"
                onFormattedValueChange={this.onUserChange}
                value={selectedUser}
                userFilters={userFilters}
                searchPromptText={
                  <FormattedMessage {...messages["no-matching-users"]} />
                }
              />
            </div>
            <div className={style["select-roles-field"]}>
              <label htmlFor="role" className={style["select-roles-label"]}>
                <FormattedMessage {...messages["roles-label"]} />
              </label>
              <RoleSelector
                name="role"
                userId={selectedUserId}
                onChange={this.onRoleChange}
                value={selectedRole}
                session={session}
              />
            </div>
            <Button
              className={style["add-button"]}
              disabled={(selectedUser && selectedRole) === null}
              label={<FormattedMessage {...messages["button-add"]} />}
              onClick={this.onAddClicked}
            />
          </div>
          <p className={style["text-hint"]}>
            <FormattedMessage {...messages["search-user-hint"]} />
          </p>
        </div>
        {processing ? (
          <ProgressBar
            className={style.spinner}
            type="circular"
            mode="indeterminate"
          />
        ) : (
          <UserList enableRemove users={add} onUserRemove={this.onUserRemove} />
        )}
      </Dialog>
    );
  }
}

AddProjectAccessDialog.propTypes = {
  onClose: PropTypes.func,
  onSave: PropTypes.func,
  onError: PropTypes.func,
  projectId: PropTypes.string,
  session: PropTypes.shape({
    call: PropTypes.func.isRequired,
  }),
  userId: PropTypes.string,
};

export default withSession(safeInjectIntl(AddProjectAccessDialog));
