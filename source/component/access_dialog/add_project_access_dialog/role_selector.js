// :copyright: Copyright (c) 2019 ftrack
import { Component } from "react";
import Select from "react-select";
import PropTypes from "prop-types";
import { defineMessages, FormattedMessage } from "react-intl";
import { SelectValue, SelectOption } from "../../selector/remote_selector";
import safeInjectIntl from "../../util/hoc/safe_inject_intl";

const messages = defineMessages({
  "first-select-user": {
    id: "ftrack-spark-components.user-role-selector.first-select-user",
    description: "Placeholder for selecting role when no user is selected",
    defaultMessage: "First select user",
  },
  "no-matching-roles": {
    id: "ftrack-spark-components.user-role-selector.no-matching-roles",
    description: "Select field information if no roles are matching",
    defaultMessage: "No matching roles found for this user",
  },
});

class RoleSelector extends Component {
  constructor(props) {
    super(props);

    this.state = {
      roles: [],
      loading: false,
    };

    if (this.props.userId) {
      this.state.loading = true;
      this.loadRoles(this.props.userId);
    }

    this.loadRoles = this.loadRoles.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.userId !== this.props.userId) {
      this.setState({
        loading: !!nextProps.userId,
        roles: [],
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.userId && this.props.userId !== prevProps.userId) {
      this.loadRoles(this.props.userId);
    }

    if (this.props.userId !== prevProps.userId) {
      this.props.onChange(null);
    }
  }

  loadRoles(userId) {
    this.props.session
      .query(
        `select id, name, type from SecurityRole where type is "PROJECT" and user_security_roles any (user_id is "${userId}") order by name`
      )
      .then((response) => {
        const roles = response.data.map((role) => ({
          data: role,
          value: role.id,
          label: role.name,
        }));

        const selectedRole = roles[0];
        this.setState({
          loading: false,
          roles,
        });

        if (selectedRole) {
          this.handleChange(selectedRole);
        }
      });
  }

  handleChange(selectedRole) {
    this.props.onChange(selectedRole);
  }

  render() {
    // eslint-disable-next-line no-unused-vars
    const { onChange, userId, ...props } = this.props;
    const { roles, loading } = this.state;
    const isDisabled = roles.length === 0;

    let placeholder = null;
    if (!userId) {
      placeholder = <FormattedMessage {...messages["first-select-user"]} />;
    } else if (!loading && isDisabled) {
      placeholder = <FormattedMessage {...messages["no-matching-roles"]} />;
    }

    return (
      <Select
        {...props}
        valueRenderer={SelectValue}
        optionRenderer={SelectOption}
        options={roles}
        tabSelectsValue={false}
        openOnFocus
        disabled={isDisabled}
        isLoading={loading}
        onChange={this.handleChange}
        placeholder={placeholder}
      />
    );
  }
}

RoleSelector.propTypes = {
  onChange: PropTypes.func,
  // eslint-disable-next-line react/forbid-prop-types
  value: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  session: PropTypes.object,
  userId: PropTypes.string,
};

export default safeInjectIntl(RoleSelector);
