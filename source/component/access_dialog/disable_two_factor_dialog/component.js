// :copyright: Copyright (c) 2019 ftrack

import { Component } from "react";
import Dialog from "react-toolbox/lib/dialog";
import PropTypes from "prop-types";
import { defineMessages, FormattedMessage } from "react-intl";

import TwoFactorVerification from "../two_factor_dialog/two_factor_verification";
import withSession from "../../util/hoc/with_session";
import IconButton from "../../icon_button";

import safeInjectIntl from "../../util/hoc/safe_inject_intl";
import style from "./style.scss";

const messages = defineMessages({
  title: {
    id: "ftack-spark-components.disable-two-factor-dialog.title",
    description: "Title for dialog",
    defaultMessage: "Disable two-factor authentication?",
  },
  subtitle: {
    id: "ftrack-spark-components.disable-two-factor-dialog.subtitle",
    description: "Informative text in dialog",
    defaultMessage:
      "Enter the 6-digit code received from your authentication " +
      "application to disable two-factor authentication for your account.",
  },
  "button-label": {
    id: "ftrack-spark-components.disable-two-factor-dialog.button-label",
    description: "Default text on button",
    defaultMessage: "Verify code and disable",
  },
});

class DisableTwoFactorDialog extends Component {
  constructor() {
    super();

    this.state = {
      code: "",
      processing: false,
      validationError: false,
    };

    this.handleVerification = this.handleVerification.bind(this);
    this.handleInputChanged = this.handleInputChanged.bind(this);
  }

  handleInputChanged(value) {
    this.setState({ code: value });
  }

  handleVerification() {
    const { session, onDisabled, onClose } = this.props;
    const { code } = this.state;
    this.setState({ processing: true });
    session
      .call([{ action: "disable_2fa", code }])
      .then(([response]) => {
        const success = response.data.success;
        this.setState({ success, processing: false });
        onDisabled();
        onClose();
      })
      .catch((error) => {
        if (error.errorCode === "2fa_invalid_code") {
          this.setState({ processing: false, validationError: true });
        } else {
          if (this.props.onError) {
            this.props.onError(error);
          }
          this.setState({ processing: false });
        }
      });
  }

  render() {
    const { onClose } = this.props;
    const { validationError, processing, code } = this.state;
    return (
      <Dialog
        active
        className={style.dialog}
        title={<FormattedMessage {...messages.title} />}
        onEscKeyDown={onClose}
      >
        <IconButton
          icon="clear"
          onClick={onClose}
          className={style["close-button"]}
        />
        <p className={style.subtitle}>
          <FormattedMessage {...messages.subtitle} />
        </p>
        <TwoFactorVerification
          value={code}
          onChange={this.handleInputChanged}
          onVerify={this.handleVerification}
          processing={processing}
          label={<FormattedMessage {...messages["button-label"]} />}
          error={validationError}
          primary
        />
      </Dialog>
    );
  }
}

DisableTwoFactorDialog.propTypes = {
  onClose: PropTypes.func,
  onError: PropTypes.func,
  onDisabled: PropTypes.func,
  session: PropTypes.shape({
    call: PropTypes.func,
  }),
};

export default withSession(safeInjectIntl(DisableTwoFactorDialog));
