// :copyright: Copyright (c) ftrack 2019

import Dialog from "react-toolbox/lib/dialog";
import Button from "react-toolbox/lib/button";
import FontIcon from "react-toolbox/lib/font_icon";
import { defineMessages, FormattedMessage } from "react-intl";
import PropTypes from "prop-types";

import Heading from "../../heading";
import safeInjectIntl from "../../util/hoc/safe_inject_intl";

import dialogTheme from "./dialog_theme.scss";
import style from "./style.scss";

const messages = defineMessages({
  "success-message": {
    id: "ftrack-spark-components.two-factor-dialog.success-message",
    description:
      "Success message when configuration of two-factor authentication" +
      "is done",
    defaultMessage:
      "You have now configured two-factor authentication successfully!",
  },
  "close-button": {
    id: "ftrack-spark-components.two-factor-dialog.close-button",
    description: "Text on cancellation button",
    defaultMessage: "Close",
  },
});

function ConfigureTwoFactorSuccessDialog({ onClick }) {
  return (
    <Dialog
      className={style.success}
      active
      disableScrolling
      theme={dialogTheme}
    >
      <FontIcon value="check" className={style.checkbox} />
      <Heading
        variant="headline"
        color="secondary"
        className={style["success-content"]}
      >
        <FormattedMessage {...messages["success-message"]} />
      </Heading>
      <Button
        onClick={onClick}
        label={<FormattedMessage {...messages["close-button"]} />}
        className={style["close-dialog-button"]}
        raised
        accent
      />
    </Dialog>
  );
}

ConfigureTwoFactorSuccessDialog.propTypes = {
  onClick: PropTypes.func,
};

export default safeInjectIntl(ConfigureTwoFactorSuccessDialog);
