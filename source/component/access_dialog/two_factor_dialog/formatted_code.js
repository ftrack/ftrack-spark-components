// :copyright: Copyright (c) 2019 ftrack
import PropTypes from "prop-types";
import classnames from "classnames";
import style from "./formatted_code.scss";

const everyFourCharactersRegex = new RegExp(".{4}", "g");

export function formatValueWithDashes(value) {
  return value.match(everyFourCharactersRegex).join("-");
}

function FormattedCode({ formatValue, value = "", className, ...props }) {
  const classes = classnames(style.root, className);
  let formattedValue = value;
  if (formatValue) {
    formattedValue = formatValue(value);
  }
  return (
    <pre className={classes} {...props}>
      <code>{formattedValue}</code>
    </pre>
  );
}

FormattedCode.propTypes = {
  formatValue: PropTypes.func,
  value: PropTypes.string,
  className: PropTypes.string,
};

export default FormattedCode;
