// :copyright: Copyright (c) 2019 ftrack

import PropTypes from "prop-types";
import { FormattedMessage, defineMessages } from "react-intl";
import Heading from "../../heading";
import safeInjectIntl from "../../util/hoc/safe_inject_intl";

import style from "./style.scss";

const messages = defineMessages({
  label: {
    id: "ftrack-spark-components.two-factor-dialog.label",
    description: "Step label",
    defaultMessage: "Step {number}",
  },
});

function Step({ title, image, number, children }) {
  return (
    <section className={style.box}>
      <div className={style["image-container"]}>{image}</div>
      <div className={style["inner-text"]}>
        <Heading variant="label" color="secondary">
          <FormattedMessage {...messages.label} values={{ number }} />
        </Heading>
        <Heading variant="title" color="default">
          {title}
        </Heading>
        <p>{children}</p>
      </div>
    </section>
  );
}
Step.propTypes = {
  image: PropTypes.node,
  children: PropTypes.node,
  number: PropTypes.number,
  title: PropTypes.node,
};

export default safeInjectIntl(Step);
