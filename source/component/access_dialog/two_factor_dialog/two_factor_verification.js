// :copyright: Copyright (c) 2019 ftrack

import { defineMessages, FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
import Input from "react-toolbox/lib/input";
import ProcessingButton from "../../processing_button";

import safeInjectIntl from "../../util/hoc/safe_inject_intl";
import style from "./style.scss";

const messages = defineMessages({
  "input-text": {
    id: "ftrack-spark-components.two-factor-dialog.input-text",
    description: "Default text on input field",
    defaultMessage: "Insert code...",
  },
  error: {
    id: "ftrack-spark-components.two-factor-dialog.error",
    description: "Error message",
    defaultMessage: "Wrong or expired code",
  },
  verifying: {
    id: "ftrack-spark-components.two-factor-dialog.verifying",
    description: "Text on button",
    defaultMessage: "Verifying...",
  },
});

function TwoFactorVerification({
  onChange,
  value,
  onVerify,
  error,
  processing,
  label,
  children,
  ...props
}) {
  return (
    <div>
      <Input
        label={<FormattedMessage {...messages["input-text"]} />}
        value={value}
        className={style.input}
        onChange={onChange}
        error={error ? <FormattedMessage {...messages.error} /> : null}
        onKeyPress={(e) => {
          if (e.key === "Enter") {
            e.preventDefault();
            onVerify();
          }
        }}
      />
      <ProcessingButton
        processing={processing}
        onClick={onVerify}
        className={style["verify-button"]}
        {...props}
      >
        {processing ? <FormattedMessage {...messages.verifying} /> : label}
      </ProcessingButton>
      {children}
    </div>
  );
}

TwoFactorVerification.propTypes = {
  onChange: PropTypes.func,
  onVerify: PropTypes.func,
  value: PropTypes.string,
  children: PropTypes.node,
  label: PropTypes.node,
  processing: PropTypes.bool,
  error: PropTypes.node,
};

export default safeInjectIntl(TwoFactorVerification);
