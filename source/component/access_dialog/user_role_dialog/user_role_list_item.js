// :copyright: Copyright (c) 2018 ftrack
import { Component } from "react";
import PropTypes from "prop-types";
import { defineMessages, intlShape } from "react-intl";
import { ListItem } from "react-toolbox/lib/list";
import { IconButton } from "react-toolbox/lib/button";
import listItemHoverActionTheme from "./list_item_hover_action_theme.scss";
import safeInjectIntl from "../../util/hoc/safe_inject_intl";

export const messages = defineMessages({
  "role-all-open-projects": {
    id: "ftrack-spark-components.user-role-editor.role-all-open-projects",
    defaultMessage: "All open projects",
  },
  "assigned-projects": {
    id: "ftrack-spark-components.user-role-editor.assigned-projects",
    defaultMessage: "Open projects with assigned tasks",
  },
});

// react-flip-move requires children to be class components.
// eslint-disable-next-line react/prefer-stateless-function
class UserRoleListItem extends Component {
  render() {
    const { intl, selectedRoleId, userRole, onSelect, onDelete } = this.props;
    const isSelected = selectedRoleId === userRole.security_role.id;
    const legendItems = userRole.user_security_role_projects.map(
      (item) => item.project.full_name
    );
    if (userRole.is_all_projects) {
      legendItems.unshift(
        intl.formatMessage(messages["role-all-open-projects"])
      );
    }

    if (userRole.security_role.type === "ASSIGNED") {
      legendItems.unshift(intl.formatMessage(messages["assigned-projects"]));
    }

    return (
      <ListItem
        theme={listItemHoverActionTheme}
        className={isSelected ? listItemHoverActionTheme.selected : null}
        leftIcon="supervisor_account"
        selectable
        caption={userRole.security_role.name}
        legend={legendItems.join(", ")}
        onClick={onSelect}
        rightActions={[<IconButton icon="delete" onClick={onDelete} />]}
      />
    );
  }
}

UserRoleListItem.propTypes = {
  intl: intlShape,
  selectedRoleId: PropTypes.string,
  userRole: PropTypes.shape({
    id: PropTypes.string.isRequired,
    security_role: PropTypes.PropTypes.shape({
      name: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
    }).isRequired,
    user_security_role_projects: PropTypes.PropTypes.arrayOf(
      PropTypes.PropTypes.shape({
        project: PropTypes.PropTypes.shape({
          full_name: PropTypes.string.isRequired,
        }).isRequired,
      })
    ),
  }).isRequired,
  onSelect: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

UserRoleListItem.defaultProps = {};

export default safeInjectIntl(UserRoleListItem);
