// :copyright: Copyright (c) 2018 ftrack
import { Component } from "react";
import PropTypes from "prop-types";
import { ListItem } from "react-toolbox/lib/list";
import FontIcon from "react-toolbox/lib/font_icon";
import { IconButton } from "react-toolbox/lib/button";
import style from "./style.scss";
import listItemHoverActionTheme from "./list_item_hover_action_theme.scss";
import { FormattedMessage, defineMessages } from "react-intl";

const messages = defineMessages({
  "private-access": {
    id: "ftrack-spark-components.user-role-editor.private-access",
    defaultMessage: "Private access",
  },
  "open-access": {
    id: "ftrack-spark-components.user-role-editor.open-access",
    defaultMessage: "Open access",
  },
});

// react-flip-move requires children to be class components.
// eslint-disable-next-line react/prefer-stateless-function
class ProjectListItem extends Component {
  render() {
    const { project, onDelete } = this.props;
    return (
      <ListItem
        theme={listItemHoverActionTheme}
        key={project.id}
        caption={project.full_name}
        legend={
          project.is_private ? (
            <span className={style.captionWithIcon}>
              <FontIcon className={style.captionIcon} value="lock" />{" "}
              <FormattedMessage {...messages["private-access"]} />
            </span>
          ) : (
            <span className={style.captionWithIcon}>
              <FontIcon className={style.captionIcon} value="lock_open" />{" "}
              <FormattedMessage {...messages["open-access"]} />
            </span>
          )
        }
        rightActions={[<IconButton icon="delete" onClick={onDelete} />]}
      />
    );
  }
}

ProjectListItem.propTypes = {
  project: PropTypes.shape({
    id: PropTypes.string.isRequired,
    full_name: PropTypes.string.isRequired,
    is_private: PropTypes.bool.isRequired,
  }).isRequired,
  onDelete: PropTypes.func.isRequired,
};

ProjectListItem.defaultProps = {};

export default ProjectListItem;
