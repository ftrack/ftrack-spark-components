// :copyright: Copyright (c) 2018 ftrack
import { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { FormattedMessage, defineMessages, intlShape } from "react-intl";
import Dialog from "react-toolbox/lib/dialog";
import { List } from "react-toolbox/lib/list";
import ProgressBar from "react-toolbox/lib/progress_bar";
import FlipMove from "react-flip-move";
import EmptyText from "../../empty_text";
import RemoteSelector from "../../selector";
import ProjectSelector from "../../selector/project_selector";
import style from "./style.scss";
import dialogTheme from "./dialog_theme.scss";
import listTheme from "./list_theme.scss";
import AddHeader from "./add_header";
import { TRANSITION_DURATION } from "../../util/constant";
import withSession from "../../util/hoc/with_session";
import safeInjectIntl from "../../util/hoc/safe_inject_intl";
import Overlay from "../../overlay";
import AllOpenProjectsListItem from "./all_open_projects_list_item";
import ProjectListItem from "./project_list_item";
import UserRoleListItem from "./user_role_list_item";

const messages = defineMessages({
  title: {
    id: "ftrack-spark-components.user-role-editor.title",
    defaultMessage: "Manage roles for {userName}",
  },
  "security-roles-header": {
    id: "ftrack-spark-components.user-role-editor.security-roles-header",
    defaultMessage: "Roles",
  },
  "projects-header": {
    id: "ftrack-spark-components.user-role-editor.projects-header",
    defaultMessage: "Projects",
  },
  "action-cancel": {
    id: "ftrack-spark-components.user-role-editor.action-cancel",
    defaultMessage: "Cancel",
  },
  "action-save": {
    id: "ftrack-spark-components.user-role-editor.save-changes",
    defaultMessage: "Save changes",
  },
  "no-role-selected": {
    id: "ftrack-spark-components.user-role-editor.no-role-selected",
    defaultMessage: "Select a role to view associated projects",
  },
  "no-roles": {
    id: "ftrack-spark-components.user-role-editor.no-roles",
    defaultMessage: "{userName} has no roles, add one above.",
  },
  "assigned-role": {
    id: "ftrack-spark-components.user-role-editor.assigned-role",
    defaultMessage:
      'The "{roleName}" role is automatically granted on any project with open access where {userName} is assigned to a task.',
  },
  "error-message": {
    id: "ftrack-spark-components.user-role-editor.error-message",
    defaultMessage: "An error occurred",
  },
  "no-matching-projects": {
    id: "ftrack-spark-components.user-role-editor.no-matching-projects",
    defaultMessage: "No matching projects found",
  },
  "no-matching-private-projects": {
    id: "ftrack-spark-components.user-role-editor.no-matching-private-projects",
    defaultMessage: "No matching private projects found",
  },
  "no-matching-roles": {
    id: "ftrack-spark-components.user-role-editor.no-matching-roles",
    defaultMessage: "No roles found",
  },
});

/** Return list of persisted user roles combined with pending operations */
function getUserRoles(persistedUserRoles, operations) {
  let userRoles = [...persistedUserRoles];
  operations.forEach((operation) => {
    if (operation.action === "add_user_security_role") {
      userRoles.push(operation.data);
    } else if (operation.action === "remove_user_security_role") {
      userRoles = userRoles.filter(
        (userRole) => userRole.security_role.id !== operation.role_id
      );
    }
  });

  // Update project list for role
  userRoles = userRoles.map((userRole) => {
    const nextUserRole = { ...userRole };
    operations.forEach((op) => {
      if (op.role_id === nextUserRole.security_role.id) {
        if (op.action === "grant_user_security_role_project") {
          if (op.all_open_projects === true) {
            nextUserRole.is_all_projects = true;
          } else {
            nextUserRole.user_security_role_projects = [
              ...nextUserRole.user_security_role_projects,
              { project: op.data },
            ];
          }
        } else if (op.action === "revoke_user_security_role_project") {
          if (op.all_open_projects === true) {
            nextUserRole.is_all_projects = false;
          } else {
            const index = nextUserRole.user_security_role_projects.findIndex(
              (candidate) => candidate.project.id === op.project_id
            );
            if (index !== -1) {
              nextUserRole.user_security_role_projects = [
                ...nextUserRole.user_security_role_projects.slice(0, index),
                ...nextUserRole.user_security_role_projects.slice(index + 1),
              ];
            }
          }
        }
      }
    });

    nextUserRole.user_security_role_projects =
      nextUserRole.user_security_role_projects.sort((a, b) =>
        a.project.full_name.localeCompare(b.project.full_name)
      );

    return nextUserRole;
  });

  userRoles = userRoles.sort((a, b) =>
    a.security_role.name.localeCompare(b.security_role.name)
  );
  return userRoles;
}

/** Return list of projects and isAllProjects flag for selected user role. */
function getProjects(userRoles, operations, selectedRoleId) {
  const selectedRole = userRoles.find(
    (candidate) => candidate.security_role.id === selectedRoleId
  );
  let isAllProjects = false;
  let isRoleAssigned = false;

  let projects = [];
  if (selectedRole) {
    isAllProjects = selectedRole.is_all_projects;
    isRoleAssigned = selectedRole.security_role.type === "ASSIGNED";
    projects = selectedRole.user_security_role_projects.map(
      (item) => item.project
    );
  }

  return { projects, isAllProjects, isRoleAssigned };
}

/** Operation action which is the inverts each action */
const inverseActionForAction = {
  add_user_security_role: "remove_user_security_role",
  remove_user_security_role: "add_user_security_role",
  grant_user_security_role_project: "revoke_user_security_role_project",
  revoke_user_security_role_project: "grant_user_security_role_project",
};

/** Identifying keys for each operation identified by action */
const operationKeysForAction = {
  add_user_security_role: ["user_id", "role_id"],
  remove_user_security_role: ["user_id", "role_id"],
  grant_user_security_role_project: [
    "user_id",
    "role_id",
    "is_all_projects",
    "project_id",
  ],
  revoke_user_security_role_project: [
    "user_id",
    "role_id",
    "is_all_projects",
    "project_id",
  ],
};

/**
 * Return next operations list with *newOperation* added.
 *
 * If there is an identical operation in the list, ignore the new operation.
 *
 * If there is an inverse operation in the list, remove it instead of adding
 * the new operation.
 *
 * If removing a role, also remove any pending operations for that role.
 */
function getNextOperations(operations, newOperation) {
  let nextOperations = [...operations];
  const action = newOperation.action;
  const inverseAction = inverseActionForAction[action];
  const keys = operationKeysForAction[action];

  const matchesOperation = (testAction, a, b) =>
    a.action === testAction && keys.every((key) => a[key] === b[key]);

  const existingOperation = nextOperations.findIndex((operation) =>
    matchesOperation(newOperation.action, operation, newOperation)
  );
  const inverseOperationIndex = nextOperations.findIndex((operation) =>
    matchesOperation(inverseAction, operation, newOperation)
  );

  if (existingOperation !== -1) {
    // Ignore duplicate operations.
  } else if (inverseOperationIndex !== -1) {
    // Remove inverse operation instead of adding new.
    nextOperations.splice(inverseOperationIndex, 1);
  } else {
    // Add new operation
    nextOperations.push(newOperation);

    // Remove pending project operations for role when removing role.
    if (newOperation.action === "remove_user_security_role") {
      nextOperations = nextOperations.filter((candidate) => {
        const isRoleProjectAction =
          candidate.action === "grant_user_security_role_project" ||
          candidate.action === "revoke_user_security_role_project";
        const isRemovedRole = candidate.role_id === newOperation.role_id;
        return !(isRemovedRole && isRoleProjectAction);
      });
    }
  }

  return nextOperations;
}

/** User role dialog */
class UserRoleDialog extends Component {
  constructor(props) {
    super(props);
    this.handleAddRole = this.handleAddRole.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.initialState = {
      loading: true,
      data: [],
      roles: [],
      projects: [],
      operations: [],
      error: null,
      selectedRoleId: null,
    };

    this.state = { ...this.initialState, userName: "" };
  }

  componentDidMount() {
    this.reloadData();
  }

  /** Load data for userId and update state. */
  reloadData() {
    const { userId, session } = this.props;
    const attributes = [
      "is_all_projects",
      "security_role.name",
      "security_role.type",
      "user_security_role_projects.project.id",
      "user_security_role_projects.project.full_name",
      "user_security_role_projects.project.is_private",
    ].join(",");
    session
      .query(
        `select ${attributes} from UserSecurityRole where user_id is "${userId}" order by security_role.name`
      )
      .then((response) => {
        this.updateState(response.data, [], null);
      })
      .catch((error) => {
        this.setState({ ...this.initialState, loading: false, error });
      });

    session
      .query(`select first_name, last_name from User where id is "${userId}"`)
      .then((response) =>
        this.setState({
          userName: `${response.data[0].first_name} ${response.data[0].last_name}`,
        })
      );
  }

  /** When a role is selected, update state. */
  handleRoleSelected(userRole) {
    const selectedRoleId = userRole.security_role.id;
    this.updateState(this.state.data, this.state.operations, selectedRoleId);
  }

  /** Add *newOperation* and update state. */
  addOperation(newOperation, nextSelectedRoleId) {
    const { operations, data } = this.state;
    const selectedRoleId =
      typeof nextSelectedRoleId === "undefined"
        ? this.state.selectedRoleId
        : nextSelectedRoleId;
    const nextOperations = getNextOperations(operations, newOperation);
    this.updateState(data, nextOperations, selectedRoleId);
  }

  /** Update state with new operations. */
  updateState(data, operations, selectedRoleId) {
    const roles = getUserRoles(data, operations);
    const { projects, isAllProjects, isRoleAssigned } = getProjects(
      roles,
      operations,
      selectedRoleId
    );
    this.setState({
      ...this.initialState,
      loading: false,
      data,
      operations,
      selectedRoleId,
      roles,
      projects,
      isAllProjects,
      isRoleAssigned,
    });
  }

  handleAddRole(value) {
    const roleId = value.data.id;
    this.addOperation(
      {
        action: "add_user_security_role",
        role_id: roleId,
        user_id: this.props.userId,
        data: {
          id: roleId,
          is_all_projects: false,
          user_security_role_projects: [],
          security_role: { ...value.data },
        },
      },
      roleId
    );
  }

  handleRemoveRole(userRole) {
    const roleId = userRole.security_role.id;
    let nextSelectedRoleId = this.state.selectedRoleId;
    if (this.state.selectedRoleId === roleId) {
      nextSelectedRoleId = null;
    }

    this.addOperation(
      {
        action: "remove_user_security_role",
        role_id: roleId,
        user_id: this.props.userId,
      },
      nextSelectedRoleId
    );
  }

  handleIsAllProjectsChange(roleId, value) {
    if (value) {
      this.addOperation({
        action: "grant_user_security_role_project",
        role_id: roleId,
        user_id: this.props.userId,
        project_id: null,
        all_open_projects: true,
      });
    } else {
      this.addOperation({
        action: "revoke_user_security_role_project",
        role_id: roleId,
        user_id: this.props.userId,
        project_id: null,
        all_open_projects: true,
      });
    }
  }

  handleRevokeProject(roleId, projectId) {
    this.addOperation({
      action: "revoke_user_security_role_project",
      role_id: roleId,
      user_id: this.props.userId,
      project_id: projectId,
    });
  }

  handleAddProject(roleId, project) {
    this.addOperation({
      action: "grant_user_security_role_project",
      role_id: roleId,
      user_id: this.props.userId,
      project_id: project.id,
      data: { ...project },
    });
  }

  handleSave() {
    const { session } = this.props;
    const { operations } = this.state;
    this.setState({ loading: true });

    // Omit any unknown keys for API operations.
    const apiOperations = operations.map(
      ({ action, role_id, user_id, project_id, all_open_projects }) => ({
        action,
        role_id,
        user_id,
        project_id,
        all_open_projects,
      })
    );

    session
      .call(apiOperations)
      .then((response) => this.props.onSave(operations, response))
      .catch((error) => this.setState({ loading: false, error }));
  }

  renderProjectsList() {
    const {
      selectedRoleId,
      isRoleAssigned,
      isAllProjects,
      roles,
      projects,
      userName,
    } = this.state;
    const selectedRole = roles.find(
      (candidate) => candidate.security_role.id === selectedRoleId
    );

    if (!selectedRoleId) {
      return (
        <EmptyText
          label={<FormattedMessage {...messages["no-role-selected"]} />}
        />
      );
    }

    if (isRoleAssigned) {
      return (
        <EmptyText
          label={
            <FormattedMessage
              {...messages["assigned-role"]}
              values={{
                userName,
                roleName: selectedRole.security_role.name,
              }}
            />
          }
        />
      );
    }

    return (
      <List theme={listTheme} key={selectedRoleId}>
        <FlipMove duration={TRANSITION_DURATION}>
          <AllOpenProjectsListItem
            key="all_open_projects"
            value={isAllProjects}
            onChange={(value) =>
              this.handleIsAllProjectsChange(selectedRoleId, value)
            }
          />
          {projects.map((project) => (
            <ProjectListItem
              key={project.id}
              project={project}
              onDelete={() =>
                this.handleRevokeProject(selectedRoleId, project.id)
              }
            />
          ))}
        </FlipMove>
      </List>
    );
  }

  renderUserRolesList() {
    const { roles, selectedRoleId, userName } = this.state;

    if (!roles || !roles.length) {
      return (
        <List theme={listTheme}>
          <EmptyText
            label={
              <FormattedMessage
                {...messages["no-roles"]}
                values={{ userName }}
              />
            }
          />
        </List>
      );
    }

    return (
      <List theme={listTheme}>
        <FlipMove duration={TRANSITION_DURATION}>
          {roles.map((userRole) => (
            <UserRoleListItem
              key={userRole.id}
              userRole={userRole}
              selectedRoleId={selectedRoleId}
              onSelect={() => this.handleRoleSelected(userRole)}
              onDelete={() => this.handleRemoveRole(userRole)}
            />
          ))}
        </FlipMove>
      </List>
    );
  }

  render() {
    const classes = classNames(style.dialog, this.props.className);
    const { intl } = this.props;
    const {
      roles,
      projects,
      operations,
      selectedRoleId,
      userName,
      isAllProjects,
    } = this.state;

    const dialogActions = [
      {
        label: intl.formatMessage(messages["action-cancel"]),
        onClick: this.props.onClose,
      },
      {
        label: intl.formatMessage(messages["action-save"]),
        primary: true,
        disabled: !operations.length,
        onClick: this.handleSave,
      },
    ];

    let roleFilter = 'type != "API"';
    if (roles.length) {
      const rolesIds = roles.map((role) => `"${role.security_role.id}"`);
      roleFilter += ` and id not_in (${rolesIds.join(",")})`;
    }

    const searchPromptText = isAllProjects ? (
      <FormattedMessage {...messages["no-matching-private-projects"]} />
    ) : (
      <FormattedMessage {...messages["no-matching-projects"]} />
    );
    let projectFilter = isAllProjects ? "is_private is True" : "";
    if (projects.length) {
      if (projectFilter) {
        projectFilter += " and ";
      }
      projectFilter += `id not_in (${projects
        .map((project) => `"${project.id}"`)
        .join(",")})`;
    }

    return (
      <Dialog
        active
        title={intl.formatMessage(messages.title, { userName })}
        theme={dialogTheme}
        className={classes}
        actions={dialogActions}
        onEscKeyDown={this.props.onClose}
      >
        {this.state.loading ? (
          <div className={style.sectionWrapper}>
            <ProgressBar
              className={style.loadingProgress}
              type="circular"
              mode="indeterminate"
            />
          </div>
        ) : null}

        {this.state.error ? (
          <div className={style.sectionWrapper}>
            <Overlay
              active
              icon="warning"
              message={intl.formatMessage(messages["error-message"])}
              details={this.state.error.message}
              dismissable
              onDismss={() => this.setState({ error: null })}
            />
          </div>
        ) : null}

        {!this.state.loading && !this.state.error ? (
          <div className={style.sectionWrapper}>
            <section className={style.section}>
              <AddHeader
                renderSelector={(props) => (
                  <RemoteSelector
                    entityType="SecurityRole"
                    extraFields={["type"]}
                    baseFilter={roleFilter}
                    searchPromptText={
                      <FormattedMessage {...messages["no-matching-roles"]} />
                    }
                    {...props}
                  />
                )}
                onAdd={this.handleAddRole}
              >
                <FormattedMessage {...messages["security-roles-header"]} />
              </AddHeader>
              {this.renderUserRolesList()}
            </section>
            <section className={style.section}>
              <AddHeader
                disabled={!selectedRoleId}
                renderSelector={(props) => (
                  <ProjectSelector
                    extraFields={["color", "is_private"]}
                    baseFilter={projectFilter}
                    searchPromptText={searchPromptText}
                    {...props}
                  />
                )}
                onAdd={(item) =>
                  this.handleAddProject(selectedRoleId, item.data)
                }
              >
                <FormattedMessage {...messages["projects-header"]} />
              </AddHeader>
              {this.renderProjectsList()}
            </section>
          </div>
        ) : null}
      </Dialog>
    );
  }
}

UserRoleDialog.propTypes = {
  userId: PropTypes.string.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  session: PropTypes.object.isRequired,
  intl: intlShape,
  className: PropTypes.string,
  onSave: PropTypes.func.isRequired,
  onClose: PropTypes.func,
};

UserRoleDialog.defaultProps = {};

export default safeInjectIntl(withSession(UserRoleDialog));
