// :copyright: Copyright (c) 2018 ftrack
import { Component } from "react";
import PropTypes from "prop-types";
import { FormattedMessage, defineMessages } from "react-intl";
import { ListItem } from "react-toolbox/lib/list";
import Switch from "react-toolbox/lib/switch";
import listItemTheme from "./list_item_theme.scss";
import switchTheme from "./switch_theme.scss";

const messages = defineMessages({
  "all-open-projects": {
    id: "ftrack-spark-components.user-role-editor.all-open-projects",
    defaultMessage: "All open projects on",
  },
  "all-open-projects-description": {
    id: "ftrack-spark-components.user-role-editor.all-open-projects-description",
    defaultMessage: "Grants permissions to all projects with open access",
  },
  "listed-projects": {
    id: "ftrack-spark-components.user-role-editor.listed-projects",
    defaultMessage: "All open projects off",
  },
  "listed-projects-description": {
    id: "ftrack-spark-components.user-role-editor.listed-projects-description",
    defaultMessage: "Only grants permissions on listed projects",
  },
});

// react-flip-move requires children to be class components.
// eslint-disable-next-line react/prefer-stateless-function
class AllOpenProjectsListItem extends Component {
  render() {
    const { value, onChange } = this.props;
    return (
      <ListItem
        theme={listItemTheme}
        caption={
          value ? (
            <FormattedMessage {...messages["all-open-projects"]} />
          ) : (
            <FormattedMessage {...messages["listed-projects"]} />
          )
        }
        legend={
          value ? (
            <FormattedMessage {...messages["all-open-projects-description"]} />
          ) : (
            <FormattedMessage {...messages["listed-projects-description"]} />
          )
        }
        rightActions={[
          <Switch
            key="switch"
            theme={switchTheme}
            checked={value}
            onChange={onChange}
          />,
        ]}
      />
    );
  }
}

AllOpenProjectsListItem.propTypes = {
  value: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};

AllOpenProjectsListItem.defaultProps = {};

export default AllOpenProjectsListItem;
