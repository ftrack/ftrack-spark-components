// :copyright: Copyright (c) 2018 ftrack
import PropTypes from "prop-types";
import classNames from "classnames";
import { withState, withHandlers, compose } from "recompose";
import Button from "react-toolbox/lib/button";
import { FormattedMessage, defineMessages } from "react-intl";
import safeInjectIntl from "../../util/hoc/safe_inject_intl";
import withSession from "../../util/hoc/with_session";
import Heading from "../../heading";
import style from "./style.scss";

const messages = defineMessages({
  add: {
    id: "ftrack-spark-components.add-header.add",
    defaultMessage: "Add",
    description: "Button label for adding new item to list",
  },
});

function AddHeader({
  children,
  renderSelector,
  session,
  value,
  onChange,
  onButtonClick,
  className,
  disabled,
  ...props
}) {
  const classes = classNames(
    style.header,
    {
      [style.headerDisabled]: disabled,
    },
    className
  );
  return (
    <header className={classes} {...props}>
      <Heading variant="subheading">{children}</Heading>
      <div className={style.addNewRow}>
        {renderSelector({
          className: style.addNewSelector,
          session,
          value,
          onFormattedValueChange: onChange,
        })}
        <Button onClick={onButtonClick} disabled={!value}>
          <FormattedMessage {...messages.add} />
        </Button>
      </div>
    </header>
  );
}

AddHeader.propTypes = {
  children: PropTypes.node.isRequired,
  renderSelector: PropTypes.func.isRequired,
  session: PropTypes.object.isRequired,
  value: PropTypes.any,
  onChange: PropTypes.func.isRequired,
  onButtonClick: PropTypes.func.isRequired,
  className: PropTypes.string,
  disabled: PropTypes.bool,
};

export default compose(
  (BaseComponent) => safeInjectIntl(BaseComponent),
  withSession,
  withState("value", "onChange", null),
  withHandlers({
    onButtonClick: (props) => () => {
      props.onChange(null);
      props.onAdd(props.value);
    },
  })
)(AddHeader);
