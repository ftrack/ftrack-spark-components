// :copyright: Copyright (c) 2018 ftrack
import PropTypes from "prop-types";

import classNames from "classnames";
import { defineMessages, FormattedMessage } from "react-intl";
import FlipMove from "react-flip-move";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import EntityAvatar from "../entity_avatar";
import style from "./style.scss";
import Badge from "@mui/material/Badge";

const messages = defineMessages({
  "tooltip-sync": {
    id: "ftrack-spark-components.avatar-list.tooltip-sync",
    description: "User tooltip when they are in sync.",
    defaultMessage: "{user} is in sync",
  },
  "tooltip-online": {
    id: "ftrack-spark-components.avatar-list.tooltip-online",
    description: "User tooltip when they are online.",
    defaultMessage: "{user} is online",
  },
  "tooltip-sync-paused": {
    id: "ftrack-spark-components.avatar-list.tooltip-sync-paused",
    description: "User tooltip when they have paused sync.",
    defaultMessage: "{user} has paused sync",
  },
});

export const PresenceIndicator = ({ presence, className }) => {
  const indicatorClasses = classNames(
    style["presence-indicator"],
    {
      [style.active]: presence === "sync" || presence === "sync-pending",
    },
    className
  );

  return <div className={indicatorClasses} />;
};

PresenceIndicator.propTypes = {
  presence: PropTypes.string,
  className: PropTypes.string,
};

function PresenceTooltip({ name, presence }) {
  let userPresence = (
    <FormattedMessage {...messages["tooltip-online"]} values={{ user: name }} />
  );
  if (presence === "sync-pending") {
    userPresence = (
      <FormattedMessage
        {...messages["tooltip-sync-paused"]}
        values={{ user: name }}
      />
    );
  } else if (presence === "sync") {
    userPresence = (
      <FormattedMessage {...messages["tooltip-sync"]} values={{ user: name }} />
    );
  }
  return userPresence;
}

function AvatarList(props) {
  const {
    maxNumberOfVisible,
    avatars,
    className,
    session,
    children,
    showPresenceTooltip,
    animation,
  } = props;
  const avatarsToShow =
    avatars.length > maxNumberOfVisible
      ? avatars.slice(0, maxNumberOfVisible - 1)
      : avatars;
  let numberOfHiddenAvatars = null;

  if (avatars.length > maxNumberOfVisible) {
    numberOfHiddenAvatars =
      avatars.length - (maxNumberOfVisible - 1) > 99
        ? "99+"
        : `+${avatars.length - (maxNumberOfVisible - 1)}`;
  }

  const avatarClasses = classNames(style.avatars, className);

  let listOfAvatars = avatarsToShow.map((avatar) => (
    <Badge
      overlap="circular"
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      key={`avatar-${avatar.entity.id}`}
      badgeContent={
        avatar.presence && avatar.presence !== "offline" ? (
          <PresenceIndicator
            presence={avatar.presence}
            className={classNames(style["align-presence-indicators"], {
              [style["presence-wave"]]: avatar.presence === "sync",
              [style["presence-pulse"]]: avatar.presence === "sync-pending",
            })}
          />
        ) : null
      }
    >
      <EntityAvatar
        entity={avatar.entity}
        session={session}
        color={avatar.entity.color}
        key={avatar.entity.id}
        tooltip={
          showPresenceTooltip ? (
            <PresenceTooltip
              name={avatar.entity.name}
              presence={avatar.presence}
            />
          ) : (
            true
          )
        }
      />
    </Badge>
  ));

  // Wrap avatars in FlipMove if *animation* is enabled.
  if (animation) {
    listOfAvatars = (
      <FlipMove
        leaveAnimation="accordionHorizontal"
        enterAnimation="accordionHorizontal"
        typeName={null}
      >
        {listOfAvatars}
      </FlipMove>
    );
  }

  return (
    <div className={avatarClasses}>
      {listOfAvatars}
      {numberOfHiddenAvatars && (
        <div className={style["number-of-hidden-container"]}>
          <span className={style["number-of-hidden"]}>
            {numberOfHiddenAvatars}
          </span>
        </div>
      )}
      {children}
    </div>
  );
}

AvatarList.propTypes = {
  className: PropTypes.string,
  avatars: PropTypes.array, // eslint-disable-line react/forbid-prop-types
  session: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  maxNumberOfVisible: PropTypes.number,
  children: PropTypes.node,
  showPresenceTooltip: PropTypes.bool,
  animation: PropTypes.bool,
};

AvatarList.defaultProps = {
  className: "",
  avatars: [],
  maxNumberOfVisible: 24,
  children: null,
};

export default safeInjectIntl(AvatarList);
