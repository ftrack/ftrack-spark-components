import { Fragment, useState, useEffect } from "react";
import centered from "@storybook/addon-centered/react";
import Button from "react-toolbox/lib/button";

import getSession from "../../story/get_session";
import AvatarList from ".";

const session = getSession();

export default {
  title: "AvatarList",
  decorators: [centered],
};

function AvatarListExample(args) {
  const [collaborators, setCollaborators] = useState([]);
  useEffect(() => {
    session
      // Just pick the first available ReviewSessionObject id and use that to fetch invitees
      .query("select id from ReviewSessionObject limit 1")
      .then(({ data: [{ id: reviewSessionObjectId }] }) => {
        const collaboratorsQueryString = `select id, name from ReviewSessionInvitee where review_session.review_session_objects.id is "${reviewSessionObjectId}" order by name limit 125`;

        let invitees = [];
        let collaborators = [];

        session.query(collaboratorsQueryString).then(({ data }) => {
          invitees = data;
          collaborators =
            invitees.length > 0
              ? invitees.map((collaborator) => {
                  return {
                    entity: {
                      name: collaborator.name,
                      id: collaborator.id,
                    },
                  };
                })
              : [];
          setCollaborators(collaborators);
        });
      });
  }, []);

  return <AvatarList session={session} avatars={collaborators} {...args} />;
}

export const SessionExample = (args) => (
  <div style={{ maxWidth: "320px" }}>
    <AvatarListExample {...args} />
  </div>
);

SessionExample.args = {
  maxNumberOfVisible: 24,
  animation: true,
  showPresenceTooltip: true,
};
SessionExample.storyName = "Session example";

const PRESENCE_ORDER = ["sync", "sync-pending", "online"];
/** Sort collaborators on presence and name.  */
function SortCollaboratorList(collaborators) {
  const sortedList = collaborators.sort((a, b) => {
    if (a.presence === b.presence) {
      return a.entity.name.localeCompare(b.entity.name);
    }
    return (
      PRESENCE_ORDER.indexOf(a.presence) - PRESENCE_ORDER.indexOf(b.presence)
    );
  });
  return sortedList;
}

const initialCollaborators = [
  {
    entity: { name: "Charlotte Andersson", id: "foo", color: "#008080" },
    presence: "sync",
    status: "approved",
  },
  {
    entity: { name: "Lucas", id: "bar", color: "#800080" },
    presence: "offline",
    status: "require_changes",
  },
  {
    entity: { name: "Jonas", id: "baz", color: "#ffc0cb" },
    presence: "online",
    status: "seen",
  },
  {
    entity: { name: "Helena", id: "baza", color: "#ffc0cb" },
    presence: "online",
    status: "require_changes",
  },
  {
    entity: { name: "Jane Doe", id: "foobar", color: "#fa8231" },
    presence: "sync-pending",
    status: null,
  },
];

function TestFlipAvatarList({ avatars: initialCollaborators, ...args }) {
  const [collaborators, onChangeOrder] = useState(initialCollaborators);
  useEffect(() => {
    onChangeOrder(initialCollaborators);
  }, [initialCollaborators]);
  return (
    <Fragment>
      <Button
        style={{ marginBottom: "16px" }}
        onClick={() => {
          onChangeOrder(
            SortCollaboratorList(
              collaborators.map((collaborator) => {
                return {
                  ...collaborator,
                  presence:
                    PRESENCE_ORDER[
                      Math.floor(Math.random() * PRESENCE_ORDER.length)
                    ],
                };
              })
            )
          );
        }}
      >
        Toggle presence
      </Button>
      <AvatarList session={session} avatars={collaborators} {...args} />
    </Fragment>
  );
}

export const SyncedStatuses = (args) => (
  <div style={{ maxWidth: "320px" }}>
    <TestFlipAvatarList {...args} />
  </div>
);

SyncedStatuses.args = {
  avatars: initialCollaborators,
  maxNumberOfVisible: 24,
  animation: true,
  showPresenceTooltip: true,
};

SyncedStatuses.storyName = "Synced statuses";
