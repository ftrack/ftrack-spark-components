..
    :copyright: Copyright (c) 2018 ftrack

######
Picker
######

Pickers provide a simple way to select a single value from a pre-determined set.

The DatePicker and TimePicker components available are localized versions of
React-toolbox's variants.
