// :copyright: Copyright (c) 2018 ftrack

import { defineMessages } from "react-intl";

export const okCancelMessages = defineMessages({
  okLabel: {
    id: "ftrack-spark-components.i18n-date-picker.okLabel",
    description: "Label of ok button in date picker.",
    defaultMessage: "Ok",
  },
  cancelLabel: {
    id: "ftrack-spark-components.i18n-date-picker.cancelLabel",
    description: "Label of cancel button in date picker.",
    defaultMessage: "Cancel",
  },
});
