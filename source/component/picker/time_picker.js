// :copyright: Copyright (c) 2018 ftrack
import { intlShape } from "react-intl";
import TimePicker from "react-toolbox/lib/time_picker";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import { okCancelMessages } from "./shared_messages";

function LocalizedTimePicker({ intl, ...props }) {
  return (
    <TimePicker
      cancelLabel={intl.formatMessage(okCancelMessages.cancelLabel)}
      okLabel={intl.formatMessage(okCancelMessages.okLabel)}
      {...props}
    />
  );
}

LocalizedTimePicker.propTypes = {
  intl: intlShape.isRequired,
};

export default safeInjectIntl(LocalizedTimePicker);
