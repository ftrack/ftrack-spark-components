// :copyright: Copyright (c) 2018 ftrack
import { defineMessages, intlShape } from "react-intl";
import DatePicker from "react-toolbox/lib/date_picker";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import { okCancelMessages } from "./shared_messages";

const datePickerLocaleMessages = defineMessages({
  months: {
    id: "ftrack-spark-components.i18n-date-picker.months",
    description: '"_"-separated list of month names.',
    defaultMessage:
      "January_February_March_April_May_June_July_August_September_October_November_December", // eslint-disable-line max-len
  },
  monthsShort: {
    id: "ftrack-spark-components.i18n-date-picker.months-short",
    description: '"_"-separated list of short month names.',
    defaultMessage: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec",
  },
  weekdays: {
    id: "ftrack-spark-components.i18n-date-picker.weekdays",
    description: '"_"-separated list of weekdays names (starting with sunday).',
    defaultMessage: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday",
  },
  weekdaysShort: {
    id: "ftrack-spark-components.i18n-date-picker.weekdays-short",
    description:
      '"_"-separated list of short weekdays names (starting with sunday).',
    defaultMessage: "Sun_Mon_Tue_Wed_Thu_Fri_Sat",
  },
  weekdaysLetter: {
    id: "ftrack-spark-components.i18n-date-picker.weekdays-letter",
    description:
      '"_"-separated list of day letters (starting with Sunday), leave "_" to reuse weekdays-short.', // eslint-disable-line max-len
    defaultMessage: "_",
  },
});

function LocalizedDatePicker({ intl, ...props }) {
  const locale = {};
  for (const [key, message] of Object.entries(datePickerLocaleMessages)) {
    locale[key] = intl.formatMessage(message).split("_").filter(String);
  }

  return (
    <DatePicker
      cancelLabel={intl.formatMessage(okCancelMessages.cancelLabel)}
      okLabel={intl.formatMessage(okCancelMessages.okLabel)}
      locale={locale}
      inputFormat={(date) =>
        intl.formatDate(date, {
          day: "numeric",
          month: "short",
          year: "numeric",
        })
      }
      {...props}
    />
  );
}

LocalizedDatePicker.propTypes = {
  intl: intlShape.isRequired,
};

export default safeInjectIntl(LocalizedDatePicker);
