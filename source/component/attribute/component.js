// :copyright: Copyright (c) 2017 ftrack

import PropTypes from "prop-types";

import { compose } from "recompose";
import { defineMessages, FormattedMessage } from "react-intl";

import { withSession, withSettings } from "../util/hoc";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import { DEFAULT_DISPLAY_BID_AS_DAYS } from "../util/constant";

const attributeMessages = {
  TypedContext: defineMessages({
    $linkedTo: {
      id: "attribute.TypedContext.$linkedTo",
      defaultMessage: "Linked to",
    },
    $latestVersions: {
      id: "attribute.TypedContext.$latestVersions",
      defaultMessage: "Latest versions",
    },
    $status: {
      id: "attribute.TypedContext.$status",
      defaultMessage: "Status",
    },
    status: {
      id: "attribute.TypedContext.status",
      defaultMessage: "Status",
    },
    $type: {
      id: "attribute.TypedContext.$type",
      defaultMessage: "Type",
    },
    type: {
      id: "attribute.TypedContext.type",
      defaultMessage: "Type",
    },
    parent: {
      id: "attribute.TypedContext.parent",
      defaultMessage: "Parent",
    },
    $priority: {
      id: "attribute.TypedContext.$priority",
      defaultMessage: "Priority",
    },
    priority: {
      id: "attribute.TypedContext.priority",
      defaultMessage: "Priority",
    },
    $objectType: {
      id: "attribute.TypedContext.$objectType",
      defaultMessage: "Object type",
    },
    $assignees: {
      id: "attribute.TypedContext.$assignees",
      defaultMessage: "Assignees",
    },
    bid: {
      id: "attribute.TypedContext.bid",
      defaultMessage:
        "Bid {isDisplayBidAsDays, select, true {days} false {hours}}",
    },
    description: {
      id: "attribute.TypedContext.description",
      defaultMessage: "Description",
    },
    start_date: {
      id: "attribute.TypedContext.start_date",
      defaultMessage: "Start date",
    },
    end_date: {
      id: "attribute.TypedContext.end_date",
      defaultMessage: "Due date",
    },
    name: {
      id: "attribute.TypedContext.name",
      defaultMessage: "Name",
    },
    link: {
      id: "attribute.TypedContext.link",
      defaultMessage: "Link",
    },
    id: {
      id: "attribute.TypedContext.id",
      defaultMessage: "Id",
    },
    $scope: {
      id: "attribute.TypedContext.$scope",
      defaultMessage: "Scope",
    },
    $project: {
      id: "attribute.TypedContext.$project",
      defaultMessage: "Project",
    },
    $query: {
      id: "attribute.TypedContext.$query",
      defaultMessage: "Query",
    },
    time_logged: {
      id: "attribute.TypedContext.time_logged",
      defaultMessage:
        "Worked {isDisplayBidAsDays, select, true {days} false {hours}}",
    },
    bid_time_logged_difference: {
      id: "attribute.TypedContext.bid_time_logged_difference",
      defaultMessage:
        "+/- {isDisplayBidAsDays, select, true {days} false {hours}}",
    },
    created_at: {
      id: "attribute.TypedContext.created_at",
      defaultMessage: "Created at",
    },
    $createdBy: {
      id: "attribute.TypedContext.$createdBy",
      defaultMessage: "Created by",
    },
    created_by: {
      id: "attribute.TypedContext.created_by",
      defaultMessage: "Created by",
    },
    $entities: {
      id: "attribute.TypedContext.$entities",
      defaultMessage: "Select",
    },
    $lists: {
      id: "attribute.TypedContext.$lists",
      defaultMessage: "Lists",
    },
    $outgoing_links: {
      id: "attribute.TypedContext.$outgoing_links",
      defaultMessage: "Outgoing links",
    },
    $incoming_links: {
      id: "attribute.TypedContext.$incoming_links",
      defaultMessage: "Incoming links",
    },
  }),
  AssetVersion: defineMessages({
    $linkedTo: {
      id: "attribute.AssetVersion.$linkedTo",
      defaultMessage: "Linked to",
    },
    $isLatestVersion: {
      id: "attribute.AssetVersion.$isLatestVersion",
      defaultMessage: "Is latest version",
    },
    id: {
      id: "attribute.AssetVersion.id",
      defaultMessage: "Id",
    },
    version: {
      id: "attribute.AssetVersion.version",
      defaultMessage: "Version",
    },
    asset: {
      id: "attribute.AssetVersion.asset",
      defaultMessage: "Asset",
    },
    comment: {
      id: "attribute.AssetVersion.comment",
      defaultMessage: "Description",
    },
    date: {
      id: "attribute.AssetVersion.date",
      defaultMessage: "Published at",
    },
    is_published: {
      id: "attribute.AssetVersion.is_published",
      defaultMessage: "Is published",
    },
    link: {
      id: "attribute.AssetVersion.link",
      defaultMessage: "Link",
    },
    $status: {
      id: "attribute.AssetVersion.$status",
      defaultMessage: "Status",
    },
    status: {
      id: "attribute.AssetVersion.status",
      defaultMessage: "Status",
    },
    $user: {
      id: "attribute.AssetVersion.$user",
      defaultMessage: "Published by",
    },
    user: {
      id: "attribute.AssetVersion.user",
      defaultMessage: "Published by",
    },
    $name: {
      id: "attribute.AssetVersion.$name",
      defaultMessage: "Name",
    },
    task: {
      id: "attribute.AssetVersion.task",
      defaultMessage: "Task",
    },
    $project: {
      id: "attribute.AssetVersion.$project",
      defaultMessage: "Project",
    },
    $publishedBy: {
      id: "attribute.AssetVersion.$publishedBy",
      defaultMessage: "Published by",
    },
    $assetType: {
      id: "attribute.AssetVersion.$assetType",
      defaultMessage: "Asset type",
    },
    $query: {
      id: "attribute.AssetVersion.$query",
      defaultMessage: "Query",
    },
    $lists: {
      id: "attribute.AssetVersion.$lists",
      defaultMessage: "Lists",
    },
    $asset_parent: {
      id: "attribute.AssetVersion.$asset.parent",
      defaultMessage: "Asset parent",
    },
    $outgoing_links: {
      id: "attribute.AssetVersion.$outgoing_links",
      defaultMessage: "Outgoing links",
    },
    $incoming_links: {
      id: "attribute.AssetVersion.$incoming_links",
      defaultMessage: "Incoming links",
    },
    $review_sessions: {
      id: "attribute.AssetVersion.$review_sessions",
      defaultMessage: "Client reviews",
    },
  }),
  Asset: defineMessages({
    name: {
      id: "attribute.Asset.name",
      defaultMessage: "Name",
    },
    type: {
      id: "attribute.Asset.type",
      defaultMessage: "Type",
    },
    $type: {
      id: "attribute.Asset.$type",
      defaultMessage: "Type",
    },
    parent: {
      id: "attribute.Asset.parent",
      defaultMessage: "Parent",
    },
  }),
  AssetType: defineMessages({
    name: {
      id: "attribute.AssetType.name",
      defaultMessage: "Name",
    },
  }),
  Type: defineMessages({
    name: {
      id: "attribute.Type.name",
      defaultMessage: "Name",
    },
    sort: {
      id: "attribute.Type.sort",
      defaultMessage: "Sort",
    },
    is_billable: {
      id: "attribute.Type.is_billable",
      defaultMessage: "Is billable",
    },
    $type: {
      id: "attribute.Type.type",
      defaultMessage: "Type",
    },
  }),
  Note: defineMessages({
    content: {
      id: "attribute.Note.content",
      defaultMessage: "Content",
    },
    completed_at: {
      id: "attribute.Note.completed_at",
      defaultMessage: "Completed at",
    },
    $is_completed: {
      id: "attribute.Note.$is_completed",
      defaultMessage: "Is completed",
    },
    is_todo: {
      id: "attribute.Note.is_todo",
      defaultMessage: "Is todo",
    },
    thread_activity: {
      id: "attribute.Note.thread_activity",
      defaultMessage: "Thread activity",
    },
    date: {
      id: "attribute.Note.date",
      defaultMessage: "Created at",
    },
    $author: {
      id: "attribute.Note.$author",
      defaultMessage: "Author",
    },
    $recipients: {
      id: "attribute.Note.$recipients",
      defaultMessage: "Recipients",
    },
    $note_labels: {
      id: "attribute.Note.$note_labels",
      defaultMessage: "Note labels",
    },
    $query: {
      id: "attribute.Note.$query",
      defaultMessage: "Query",
    },
  }),
  Project: defineMessages({
    start_date: {
      id: "attribute.Project.start_date",
      defaultMessage: "Start date",
    },
    end_date: {
      id: "attribute.Project.end_date",
      defaultMessage: "End date",
    },
    name: {
      id: "attribute.Project.name",
      defaultMessage: "Code",
    },
    full_name: {
      id: "attribute.Project.full_name",
      defaultMessage: "Full name",
    },
    link: {
      id: "attribute.Project.link",
      defaultMessage: "Project",
    },
    $project: {
      id: "attribute.Project.project",
      defaultMessage: "Project",
    },
    id: {
      id: "attribute.Project.id",
      defaultMessage: "Id",
    },
    status: {
      id: "attribute.Project.status",
      defaultMessage: "Status",
    },
    color: {
      id: "attribute.Project.color",
      defaultMessage: "Color",
    },
    $project_schema_name: {
      id: "attribute.Project.$project_schema_name",
      defaultMessage: "Workflow",
    },
    $managers: {
      id: "attribute.Project.$managers",
      defaultMessage: "Managers",
    },
    $scope: {
      id: "attribute.Project.$scope",
      defaultMessage: "Scope",
    },
    created_at: {
      id: "attribute.Project.created_at",
      defaultMessage: "Created at",
    },
    $createdBy: {
      id: "attribute.Project.$createdBy",
      defaultMessage: "Created by",
    },
    created_by: {
      id: "attribute.Project.created_by",
      defaultMessage: "Created by",
    },
  }),
  Status: defineMessages({
    name: {
      id: "attribute.Status.name",
      defaultMessage: "Name",
    },
    sort: {
      id: "attribute.Status.sort",
      defaultMessage: "Sort",
    },
    state: {
      id: "attribute.Status.state",
      defaultMessage: "State",
    },
  }),
  Priority: defineMessages({
    name: {
      id: "attribute.Priority.name",
      defaultMessage: "Name",
    },
    sort: {
      id: "attribute.Priority.sort",
      defaultMessage: "Sort",
    },
  }),
  State: defineMessages({
    name: {
      id: "attribute.State.name",
      defaultMessage: "Name",
    },
    short: {
      id: "attribute.State.short",
      defaultMessage: "Short",
    },
  }),
  CalendarEvent: defineMessages({
    name: {
      id: "attribute.CalendarEvent.name",
      defaultMessage: "Name",
    },
    leave: {
      id: "attribute.CalendarEvent.leave",
      defaultMessage: "Leave",
    },
    forecast: {
      id: "attribute.CalendarEvent.forecast",
      defaultMessage: "Include in forecast",
    },
    estimate: {
      id: "attribute.CalendarEvent.estimate",
      defaultMessage: "Estimate",
    },
    effort: {
      id: "attribute.CalendarEvent.effort",
      defaultMessage: "Effort",
    },
    $users: {
      id: "attribute.CalendarEvent.$users",
      defaultMessage: "Users",
    },
    $project: {
      id: "attribute.CalendarEvent.$project",
      defaultMessage: "Project",
    },
    $type: {
      id: "attribute.CalendarEvent.$type",
      defaultMessage: "Type",
    },
  }),
  Context: defineMessages({
    name: {
      id: "attribute.Context.name",
      defaultMessage: "Name",
    },
    link: {
      id: "attribute.Context.link",
      defaultMessage: "Link",
    },
    created_at: {
      id: "attribute.Context.created_at",
      defaultMessage: "Created at",
    },
    created_by: {
      id: "attribute.Context.created_by",
      defaultMessage: "Created by",
    },
    $createdBy: {
      id: "attribute.Context.$createdBy",
      defaultMessage: "Created by",
    },
  }),
  Group: defineMessages({
    $group: {
      id: "attribute.Group.$group",
      defaultMessage: "Group",
    },
  }),
  User: defineMessages({
    first_name: {
      id: "attribute.User.first_name",
      defaultMessage: "First name",
    },
    last_name: {
      id: "attribute.User.last_name",
      defaultMessage: "Last name",
    },
    $user: {
      id: "attribute.User.$user",
      defaultMessage: "User",
    },
    $group_membership: {
      id: "attribute.User.$group_membership",
      defaultMessage: "Group",
    },
    email: {
      id: "attribute.User.email",
      defaultMessage: "Email",
    },
    username: {
      id: "attribute.User.username",
      defaultMessage: "Username",
    },
    is_active: {
      id: "attribute.User.is_active",
      defaultMessage: "Enabled",
    },
  }),
  ReviewSession: defineMessages({
    name: {
      id: "attribute.ReviewSession.name",
      defaultMessage: "Name",
    },
    description: {
      id: "attribute.ReviewSession.description",
      defaultMessage: "Description",
    },
    start_date: {
      id: "attribute.ReviewSession.start_date",
      defaultMessage: "Start",
    },
    end_date: {
      id: "attribute.ReviewSession.end_date",
      defaultMessage: "End",
    },
    id: {
      id: "attribute.ReviewSession.id",
      defaultMessage: "Id",
    },
    created_at: {
      id: "attribute.ReviewSession.created_at",
      defaultMessage: "Created at",
    },
    passphrase_enabled: {
      id: "attribute.ReviewSession.passphrase_enabled",
      defaultMessage: "Is passphrase enabled",
    },
    $project: {
      id: "attribute.ReviewSession.project",
      defaultMessage: "Project",
    },
    $createdBy: {
      id: "attribute.ReviewSession.createdBy",
      defaultMessage: "Created by",
    },
    created_by: {
      id: "attribute.ReviewSession.created_by",
      defaultMessage: "Created by",
    },
    $reviewSession: {
      id: "attribute.ReviewSession.$reviewSession",
      defaultMessage: "Name",
    },
    is_open: {
      id: "attribute.ReviewSession.is_open",
      defaultMessage: "Is open",
    },
  }),
};

export function getLocalisedLabel(schema, attributeName) {
  if (!schema) {
    return false;
  }
  let entityType = schema.id;
  if (schema.alias_for && schema.alias_for.id === "Task") {
    // Pick i18n from typed context.
    entityType = "TypedContext";
  }

  return (
    attributeMessages[entityType] &&
    attributeMessages[entityType][attributeName]
  );
}

function AttributeLabel_({
  session,
  attribute,
  entityType,
  className,
  isDisplayBidAsDays,
}) {
  const schema = session.schemas.find(
    (candidate) => candidate.id === entityType
  );
  const i18nMessage = getLocalisedLabel(schema, attribute);
  if (!i18nMessage) {
    return <span>{attribute}</span>;
  }
  return (
    <span className={className}>
      <FormattedMessage {...i18nMessage} values={{ isDisplayBidAsDays }} />
    </span>
  );
}

AttributeLabel_.propTypes = {
  attribute: PropTypes.string.isRequired,
  entityType: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  isDisplayBidAsDays: PropTypes.bool.isRequired,
  session: PropTypes.shape({ schemas: PropTypes.array }).isRequired,
};
export const AttributeLabel = compose(
  safeInjectIntl,
  withSession,
  withSettings({ isDisplayBidAsDays: DEFAULT_DISPLAY_BID_AS_DAYS })
)(AttributeLabel_);
