import centered from "@storybook/addon-centered/react";
import Provider from "../../story/spark_decorator";
import { AttributeLabel } from ".";

export default {
  title: "Attribute",
  decorators: [(Story) => <Provider story={<Story />} />, centered],
};

export const TaskName = () => (
  <AttributeLabel entityType="Task" attribute="name" />
);

TaskName.storyName = "Task name";

TaskName.parameters = {
  info: "Attribute label task name",
};

export const AssetVersionProject = () => (
  <AttributeLabel entityType="AssetVersion" attribute="$project" />
);

AssetVersionProject.storyName = "Asset version project";

AssetVersionProject.parameters = {
  info: "Attribute label asset version project",
};

export const UnknownAttribute = () => (
  <AttributeLabel entityType="AssetVersion" attribute="foo" />
);

UnknownAttribute.storyName = "Unknown attribute";

UnknownAttribute.parameters = {
  info: "Attribute label of unknown attribute",
};

export const UnknownEntityType = () => (
  <AttributeLabel entityType="Bar" attribute="foo" />
);

UnknownEntityType.storyName = "Unknown entity type";

UnknownEntityType.parameters = {
  info: "Attribute label of unknown entity type",
};
