// :copyright: Copyright (c) 2021 ftrack

import { useRef, useState } from "react";
import PropTypes, { string } from "prop-types";
import { defineMessages, FormattedMessage } from "react-intl";
import ProgressBar from "react-toolbox/lib/progress_bar";

import EntityPickerListItem from "./entity_picker_list_item";
import { useInfiniteScroll, useScrollElementVisible } from "../util/hooks";

import styles from "./style.scss";

const messages = defineMessages({
  "no-matches-text": {
    id: "ftrack-spark-components.entity-picker.no-matches-text",
    defaultMessage: "Couldn't find a match. Try again.",
  },
  "no-items-text": {
    id: "ftrack-spark-components.entity-picker.no-items-text",
    defaultMessage: "We could not find anything to display.",
  },
});

function EntityPickerList({
  items,
  isLoading,
  onScrollToBottom,
  selectedIndex,
  canFetchMore,
  formatter,
  onHover,
  multiSelect,
  onSelected,
  error,
  selectedIds = [],
  disabledIds = [],
  disabledTooltip,
  entityType,
  emptyStateText,
}) {
  const scrollRef = useRef(null);
  const listRef = useRef(null);
  const [isHovering, setIsHovering] = useState(false);

  useInfiniteScroll(scrollRef, onScrollToBottom);
  useScrollElementVisible(listRef, selectedIndex, !isHovering);
  return (
    <div
      className={styles.listContainer}
      ref={listRef}
      onMouseEnter={() => setIsHovering(true)}
      onMouseLeave={() => setIsHovering(false)}
    >
      {items &&
        items.map((e, i) => (
          <EntityPickerListItem
            key={e.id}
            entity={e}
            active={i === selectedIndex}
            selected={selectedIds.includes(e.id)}
            onSelected={onSelected}
            disabled={disabledIds.includes(e.id)}
            onMouseEnter={() => onHover(i)}
            showToggle={multiSelect}
            formatter={formatter}
            entityType={entityType}
            disabledTooltip={disabledTooltip}
          />
        ))}
      {!items && !isLoading && (
        <p className={styles.noMatch}>
          <FormattedMessage {...messages["no-items-text"]} />
        </p>
      )}
      {items && items.length === 0 && !isLoading && (
        <p className={styles.noMatch}>
          {emptyStateText || (
            <FormattedMessage {...messages["no-matches-text"]} />
          )}
        </p>
      )}
      {isLoading && (
        <div className={styles.loadingContainer}>
          <ProgressBar
            type="circular"
            mode="indeterminate"
            theme={{
              circular: styles.circular,
              circle: styles.circle,
            }}
          />
        </div>
      )}
      {error && !isLoading && <span>{error.message}</span>}
      {items && !isLoading && canFetchMore && (
        <div id="scroll-ref" style={{ height: "1px" }} ref={scrollRef} />
      )}
    </div>
  );
}

EntityPickerList.propTypes = {
  items: PropTypes.array, // eslint-disable-line
  isLoading: PropTypes.bool,
  selectedIndex: PropTypes.number,
  onScrollToBottom: PropTypes.func,
  onSelected: PropTypes.func,
  onHover: PropTypes.func,
  canFetchMore: PropTypes.bool,
  multiSelect: PropTypes.bool,
  formatter: PropTypes.func,
  selectedIds: PropTypes.arrayOf(string),
  disabledIds: PropTypes.arrayOf(string),
  disabledTooltip: PropTypes.node,
  error: PropTypes.object, // eslint-disable-line
  entityType: PropTypes.string,
  emptyStateText: PropTypes.string,
};

export default EntityPickerList;
