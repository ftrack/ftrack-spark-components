// :copyright: Copyright (c) 2021 ftrack

import { useState, useEffect, useCallback, Fragment } from "react";
import PropTypes, { string } from "prop-types";
import { intlShape, defineMessages, FormattedMessage } from "react-intl";
import classNames from "classnames";
import { compose } from "redux";

import Dialog from "react-toolbox/lib/dialog";
import Button from "react-toolbox/lib/button";
import Chip from "react-toolbox/lib/chip";
import FontIcon from "react-toolbox/lib/font_icon";

import safeInjectIntl from "../util/hoc/safe_inject_intl";

import SearchField from "../search_field";
import IconButton from "../icon_button";
import EntityPickerList from "./entity_picker_list";
import { useSessionSearch, useSessionQuery } from "../util/hooks";
import { Entity } from "../formatter/entityUtils";

import buttonTheme from "./button_theme.scss";
import styles from "./style.scss";

const messages = defineMessages({
  "cancel-button-text": {
    id: "ftrack-spark-components.entity-picker-search.cancel-button-text",
    defaultMessage: "Cancel",
  },
  "done-button-text": {
    id: "ftrack-spark-components.entity-picker-search.done-button-text",
    defaultMessage: "Done",
  },
  "chip-text": {
    id: "ftrack-spark-components.entity-picker.chip-text",
    defaultMessage:
      "{numberOfIds, plural, " +
      "one {# selected item}" +
      "other {# selected items}" +
      "}",
  },
  "user-placeholder": {
    id: "ftrack-spark-components.entity-picker.user-placeholder",
    defaultMessage: "Search user by name or group name",
  },
  "group-placeholder": {
    id: "ftrack-spark-components.entity-picker.group-placeholder",
    defaultMessage: "Search group by name or parent group name",
  },
  "list-placeholder": {
    id: "ftrack-spark-components.entity-picker.list-placeholder",
    defaultMessage: "Search list by name or category name",
  },
  "context-placeholder": {
    id: "ftrack-spark-components.entity-picker.context-placeholder",
    defaultMessage: "Search by name or project name",
  },
  "project-placeholder": {
    id: "ftrack-spark-components.entity-picker.project-placeholder",
    defaultMessage: "Search project by name, date or status",
  },
  "component-placeholder": {
    id: "ftrack-spark-components.entity-picker.component-placeholder",
    defaultMessage: "Search component by name or file type",
  },
  "asset-placeholder": {
    id: "ftrack-spark-components.entity-picker.asset-placeholder",
    defaultMessage: "Search asset by name",
  },
  "asset-version-placeholder": {
    id: "ftrack-spark-components.entity-picker.asset-version-placeholder",
    defaultMessage: "Search asset version by name or parent names",
  },
  "review-placeholder": {
    id: "ftrack-spark-components.entity-picker.review-placeholder",
    defaultMessage: "Search review session by name",
  },
  "unknown-context": {
    id: "ftrack-spark-components.entity-picker.unknown-context",
    defaultMessage: "Unknown context",
    description: "Fallback label when unable to resolve project/object name",
  },
});

const DEFAULTS = {
  User: {
    projection: [
      ...Entity.User.labelProjection,
      "thumbnail_url",
      "thumbnail_id",
      "memberships.group.name",
    ],
    order: Entity.User.order,
    getPlaceholder: (intl) => intl.formatMessage(messages["user-placeholder"]),
  },
  Group: {
    projection: [
      ...Entity.Group.labelProjection,
      "parent.memberships.group.name",
    ],
    order: Entity.Group.order,
    getPlaceholder: (intl) => intl.formatMessage(messages["group-placeholder"]),
    filter: Entity.Group.filter,
  },
  List: {
    projection: [
      ...Entity.List.labelProjection,
      "category.name",
      "system_type",
      "project.full_name",
      "project.color",
    ],
    getPlaceholder: (intl) => intl.formatMessage(messages["list-placeholder"]),
    projectFilter: (projectId) => `project_id = '${projectId}'`,
    order: Entity.List.order,
  },
  get TypedContextList() {
    return this.List;
  },
  get AssetVersionList() {
    return this.List;
  },
  Context: {
    projection: [...Entity.Context.labelProjection, "link", "context_type"],
    getPlaceholder: (intl) =>
      intl.formatMessage(messages["context-placeholder"]),
    projectFilter: (projectId) => `project_id = '${projectId}'`,
    order: Entity.Context.order,
  },
  TypedContext: {
    projection: [
      ...Entity.TypedContext.labelProjection,
      "link",
      "type.name",
      "object_type.name",
      "status.color",
      "thumbnail_url",
      "thumbnail_id",
    ],
    getPlaceholder: (intl) =>
      intl.formatMessage(messages["context-placeholder"]),
    projectFilter: (projectId) => `project_id = '${projectId}'`,
    order: Entity.TypedContext.order,
  },
  Project: {
    projection: [
      ...Entity.Project.labelProjection,
      "start_date",
      "end_date",
      "thumbnail_url",
      "thumbnail_id",
      "status",
    ],
    order: Entity.Project.order,
    getPlaceholder: (intl) =>
      intl.formatMessage(messages["project-placeholder"]),
  },
  Asset: {
    projection: [
      ...Entity.Asset.labelProjection,
      "latest_version.link",
      "latest_version.thumbnail_url",
      "latest_version.thumbnail_id",
      "latest_version.status.color",
    ],
    order: Entity.Asset.order,
    getPlaceholder: (intl) => intl.formatMessage(messages["asset-placeholder"]),
    projectFilter: (projectId) => `parent.project.id = '${projectId}'`,
  },
  AssetVersion: {
    projection: [
      ...Entity.AssetVersion.labelProjection,
      "link",
      "thumbnail_url",
      "thumbnail_id",
      "status.color",
      "task.name",
      "task.type.id",
      "task.type.color",
      "task.type.name",
      "asset.parent.name",
      "components.name",
    ],
    order: Entity.AssetVersion.order,
    getPlaceholder: (intl) =>
      intl.formatMessage(messages["asset-version-placeholder"]),
    projectFilter: (projectId) => `asset.parent.project_id is '${projectId}'`,
  },
  ReviewSession: {
    projection: [
      ...Entity.ReviewSession.labelProjection,
      "project.full_name",
      "project.color",
      "is_open",
      "thumbnail_url",
      "thumbnail_id",
    ],
    getPlaceholder: (intl) =>
      intl.formatMessage(messages["review-placeholder"]),
    projectFilter: (projectId) => `project_id = '${projectId}'`,
    order: Entity.ReviewSession.order,
  },
};

const buildQuery = ({ entityType, projection, filter, order }) =>
  `
        select ${projection.join(",")}
        from ${entityType}
        ${filter ? `where ${filter}` : ""}
        ${order ? `order by ${order}` : ""}
    `;

const buildSearchExpression = ({
  entityType,
  projection,
  order = "",
  filter = "",
}) => {
  const projectionsLabel = projection.join(", ");
  return `select ${projectionsLabel}
            from ${entityType}
            ${filter && `where ${filter}`}
            ${order && `order by ${order}`}`;
};

const QUICK_SELECT = "QUICK";
const SINGLE_SELECT = "SINGLE";
const MULTI_SELECT = "MULTI";

function ContextToggle({ label, onToggle, enabled, canToggle }) {
  let chipTheme = styles.projectChip;
  if (enabled) {
    if (canToggle) {
      chipTheme = styles.projectChipSelected;
    } else {
      chipTheme = styles.projectChipSelectedAndCannotBeDisabled;
    }
  }

  return (
    <Chip theme={{ chip: chipTheme }} onClick={canToggle ? onToggle : () => {}}>
      <FontIcon
        className={classNames(styles.icon, {
          [styles.iconFade]: enabled,
        })}
        value="check"
      />
      <span>{label}</span>
    </Chip>
  );
}

ContextToggle.propTypes = {
  label: PropTypes.string,
  onToggle: PropTypes.func,
  enabled: PropTypes.bool,
  canToggle: PropTypes.bool,
};

function EntityPicker({
  open,
  entityType,
  objectTypeId,
  projection,
  order,
  formatter,
  placeholder,
  contextId,
  disableContextFilterToggle,
  canClose = true,
  doneText,
  onClose,
  onSelected,
  session,
  baseFilter = "",
  selected = [],
  disabledIds = [],
  disabledTooltip,
  mode = QUICK_SELECT,
  intl,
  emptyStateText,
}) {
  const hasConfiguredContextFilter =
    DEFAULTS[entityType].projectFilter !== undefined;
  const [contextFilterEnabled, setContextFilterEnabled] = useState(
    !!contextId && hasConfiguredContextFilter
  );
  const [searchValue, setSearchValue] = useState("");

  const { isLoading, data, error, fetchMore, canFetchMore } = useSessionSearch({
    session,
    baseExpression: buildSearchExpression({
      entityType,
      projection: projection || DEFAULTS[entityType].projection,
      order: order || DEFAULTS[entityType].order,
      filter: baseFilter || DEFAULTS[entityType].filter,
    }),
    entityType,
    value: searchValue,
    contextId: contextFilterEnabled && hasConfiguredContextFilter && contextId,
    objectTypeIds: objectTypeId ? [objectTypeId] : [],
  });
  const {
    isLoading: isLoadingSelected,
    data: selectedData,
    fetchMore: fetchMoreSelected,
    canFetchMore: canFetchMoreSelected,
    error: errorSelected,
  } = useSessionQuery({
    session,
    baseQuery: buildQuery({
      entityType,
      projection: projection || DEFAULTS[entityType].projection,
      order: order || DEFAULTS[entityType].order,
      filter: `id in (${selected.map((id) => `'${id}'`).join(",")})`,
    }),
    config: {
      limit: 25,
      pause: !selected || selected.length === 0,
    },
  });

  const { isLoading: isLoadingContextName, data: contextData } =
    useSessionQuery({
      session,
      baseQuery: `select link from Context where id = '${contextId}'`,
      config: {
        limit: 1,
        pause: !contextId,
      },
    });
  const contextName =
    contextData &&
    contextData.length &&
    Array.isArray(contextData[0].link) &&
    contextData[0].link.length &&
    contextData[0].link[0].name ? (
      contextData[0].link[0].name
    ) : (
      <FormattedMessage {...messages["unknown-context"]} />
    );

  const [selectedIndex, setSelectedIndex] = useState(0);
  const [selectedIds, setSelectedIds] = useState([]);
  const [selectedItems, setSelectedItems] = useState([]);
  const [showSelected, setShowSelected] = useState(false);

  useEffect(() => {
    setSelectedIds([]);
    setSelectedItems([]);
  }, [entityType]);

  useEffect(() => {
    if (selected && selected.length) {
      setSelectedIds(selected);
    }
  }, [selected]);

  useEffect(() => {
    if (selectedData) {
      setSelectedItems((prevItems) => [...prevItems, ...selectedData]);
    }
  }, [selectedData]);

  useEffect(() => {
    if (!selectedIds.length) {
      setShowSelected(false);
    }
  }, [selectedIds]);

  const handleSearchChange = (searchStr) => {
    setSearchValue(searchStr);
    setSelectedIndex(0);
    setShowSelected(false);
  };

  const handleSelected = (item) => {
    if (disabledIds.includes(item.id)) {
      return;
    }
    if (mode === MULTI_SELECT) {
      const isSelected = selectedIds.includes(item.id);
      if (isSelected) {
        // If it's already selected, the item should be toggled off
        setSelectedIds((ids) => ids.filter((i) => i !== item.id));
        setSelectedItems((items) => items.filter((i) => i.id !== item.id));
      } else {
        setSelectedIds((ids) => [...ids, item.id]);
        setSelectedItems((items) => [...items, item]);
      }
    } else if (mode === SINGLE_SELECT) {
      const isSelected = selectedIds.includes(item.id);
      if (isSelected) {
        setSelectedIds([]);
        setSelectedItems([]);
      } else {
        setSelectedIds([item.id]);
        setSelectedItems([item]);
      }
    } else if (mode === QUICK_SELECT) {
      onSelected({
        ...item,
        returnLabel: Entity[entityType].formatter(item),
      });
    }
  };

  const handleOnHover = (index) => {
    setSelectedIndex(index);
  };

  const handleFetchMoreData = useCallback(() => {
    if (showSelected) {
      if (selected.length > 0) {
        fetchMoreSelected();
      }
    } else {
      fetchMore();
    }
  }, [fetchMore, fetchMoreSelected, showSelected, selected.length]);

  const toggleShowSelected = () => {
    setShowSelected((prev) => !prev);
    setSelectedIndex(0);
  };

  const handleToggleContext = () => {
    setShowSelected(false);
    setContextFilterEnabled((curr) => !curr);
  };

  const handleKeyPress = (key) => {
    if (["Control+Enter", "Meta+Enter"].includes(key)) {
      if (mode === MULTI_SELECT) {
        onSelected(
          selectedItems.map((item) => ({
            ...item,
            returnLabel: Entity[entityType].formatter(item),
          }))
        );
      } else if (mode === SINGLE_SELECT) {
        const item = selectedItems[0];
        if (item && !disabledIds.includes(item.id)) {
          onSelected({
            ...selectedItems[0],
            returnLabel: Entity[entityType].formatter(selectedItems[0]),
          });
        } else if (!disabledIds.includes(item.id)) {
          onSelected(null);
        }
      } else if (mode === QUICK_SELECT) {
        const item = data[selectedIndex];
        handleSelected(item);
      }
    } else if (
      key === "ArrowUp" &&
      data &&
      data.length > 0 &&
      selectedIndex > 0
    ) {
      setSelectedIndex((currIndex) => currIndex - 1);
    } else if (key === "ArrowDown" && data && data.length - 1 > selectedIndex) {
      setSelectedIndex((currIndex) => currIndex + 1);
    } else if (key === "Enter" && data && data.length > 0) {
      const item = data[selectedIndex];
      if (item) {
        handleSelected(item);
      }
    }
  };

  const chipTheme = showSelected
    ? styles.chipButtonSelected
    : styles.chipButton;

  const searchPlaceholder =
    (DEFAULTS &&
      DEFAULTS[entityType] &&
      DEFAULTS[entityType].getPlaceholder(intl)) ||
    placeholder;

  return (
    <Dialog
      className={styles.dialog}
      active={open}
      onEscKeyDown={onClose}
      onOverlayClick={onClose}
      theme={{ body: styles.dialogBody }}
    >
      <div className={styles.container}>
        <div className={styles.topContainer}>
          <SearchField
            large
            disableClose
            debounce={600}
            resetIcon="backspace"
            onChange={handleSearchChange}
            onKeyPress={handleKeyPress}
            keys={[
              "ArrowUp",
              "ArrowDown",
              "Enter",
              "Control+Enter",
              "Meta+Enter",
            ]}
            placeholder={searchPlaceholder}
            additionalMatchesAvailable={canFetchMore}
            matches={
              data && !isLoading && Boolean(searchValue) ? data.length : null
            }
          />
          {mode === QUICK_SELECT && canClose && (
            <IconButton
              className={styles.closeButton}
              icon="close"
              onClick={onClose}
            />
          )}
        </div>
        <div className={styles.chipContainer}>
          {contextId && hasConfiguredContextFilter && !isLoadingContextName ? (
            <ContextToggle
              label={contextName}
              onToggle={handleToggleContext}
              enabled={contextFilterEnabled}
              canToggle={!disableContextFilterToggle}
            />
          ) : (
            <span />
          )}
          {mode !== QUICK_SELECT && (
            <Chip
              theme={{
                chip: !selectedIds.length ? styles.chipDisabled : chipTheme,
              }}
              onClick={toggleShowSelected}
            >
              <FontIcon
                className={classNames(styles.icon, {
                  [styles.iconFade]: showSelected,
                })}
                value="check"
              />
              <span>
                <FormattedMessage
                  {...messages["chip-text"]}
                  values={{
                    selectordinal: selectedIds.length,
                    numberOfIds: selectedIds.length,
                  }}
                />
              </span>
            </Chip>
          )}
        </div>
        {!error && (
          <Fragment>
            {!showSelected && (
              <EntityPickerList
                items={data}
                isLoading={isLoading}
                selectedIndex={selectedIndex}
                onScrollToBottom={handleFetchMoreData}
                onSelected={handleSelected}
                onHover={handleOnHover}
                canFetchMore={canFetchMore}
                multiSelect={mode !== QUICK_SELECT}
                selectedIds={selectedIds}
                disabledIds={disabledIds}
                disabledTooltip={disabledTooltip}
                error={error}
                formatter={formatter}
                entityType={entityType}
                emptyStateText={emptyStateText}
              />
            )}
            {showSelected && (
              <EntityPickerList
                items={selectedItems}
                isLoading={isLoadingSelected}
                selectedIndex={selectedIndex}
                onScrollToBottom={handleFetchMoreData}
                onSelected={handleSelected}
                onHover={handleOnHover}
                canFetchMore={canFetchMoreSelected}
                multiSelect={mode !== QUICK_SELECT}
                selectedIds={selectedIds}
                disabledIds={disabledIds}
                disabledTooltip={disabledTooltip}
                error={errorSelected}
                formatter={formatter}
                entityType={entityType}
                emptyStateText={emptyStateText}
              />
            )}
          </Fragment>
        )}
      </div>
      {mode !== QUICK_SELECT && (
        <div className={styles.buttonContainer}>
          {canClose ? (
            <Button
              theme={buttonTheme}
              label={<FormattedMessage {...messages["cancel-button-text"]} />}
              onClick={() => onClose()}
            />
          ) : (
            <div />
          )}
          <Button
            primary
            theme={buttonTheme}
            label={
              doneText || <FormattedMessage {...messages["done-button-text"]} />
            }
            onClick={() => {
              if (mode === MULTI_SELECT) {
                onSelected(
                  selectedItems.map((item) => ({
                    ...item,
                    returnLabel: Entity[entityType].formatter(item),
                  }))
                );
              } else if (mode === SINGLE_SELECT) {
                const item = selectedItems[0];
                if (item) {
                  onSelected({
                    ...selectedItems[0],
                    returnLabel: Entity[entityType].formatter(selectedItems[0]),
                  });
                } else {
                  onSelected(null);
                }
              }
            }}
          />
        </div>
      )}
    </Dialog>
  );
}

EntityPicker.propTypes = {
  session: PropTypes.object.isRequired, // eslint-disable-line
  baseFilter: PropTypes.string,
  entityType: PropTypes.string.isRequired,
  objectTypeId: PropTypes.string,
  projection: PropTypes.arrayOf(string),
  order: PropTypes.string,
  formatter: PropTypes.func, // eslint-disable-line
  placeholder: PropTypes.string,
  open: PropTypes.bool,
  contextId: PropTypes.string,
  disableContextFilterToggle: PropTypes.bool,
  onClose: PropTypes.func,
  onSelected: PropTypes.func,
  mode: PropTypes.oneOf(["QUICK", "SINGLE", "MULTI"]),
  selected: PropTypes.arrayOf(string),
  disabledIds: PropTypes.arrayOf(string),
  disabledTooltip: PropTypes.node,
  canClose: PropTypes.bool,
  doneText: PropTypes.string,
  emptyStateText: PropTypes.string,
  intl: intlShape,
};

export default compose(safeInjectIntl)(EntityPicker);
