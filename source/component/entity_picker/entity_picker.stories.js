// :copyright: Copyright (c) 2021 ftrack

import { Fragment } from "react";
import { action } from "@storybook/addon-actions";
import { SparkProvider } from "../util/hoc";
import getSession from "../../story/get_session";
import EntityPicker from ".";
import { ListItemChip } from "../entity_list_item";
import Breadcrumbs from "../breadcrumbs";
import { removeLastItem } from "./entity_picker_list_item";
import { Entity } from "../formatter/entityUtils";

const PROJECTS = {
  "No Project": undefined,
  Spring: "5671dcb0-66de-11e1-8e6e-f23c91df25eb",
  Thimble: "0b4f1470-3127-4647-9051-ac15eb4f1f87",
  "LOCAL DEV DB / Tests (do not delete)":
    "5671dcb0-66de-11e1-8e6e-f23c91df25eb",
  "LOCAL DEV DB / client review": "81b98c47-5910-11e4-901f-3c0754282242",
  "LOCAL DEV DB / FTrack": "95f5c536-4e65-11e1-afaa-f23c91df25eb",
  "LOCAL DEV DB / TestsDb (Do not delete)":
    "aa30f6c4-9811-11e1-95e3-f23c91df25eb",
  "LOCAL DEV DB / Manual Testing": "bf5b2e84-f03b-11e1-8d59-f23c91df25eb",
  "LOCAL DEV DB / Big Buck Bunny": "edb08a74-15e2-11e1-8054-0019bb4983d8",
};

const OBJECT_TYPES = {
  "No object type": undefined,
  Task: "11c137c0-ee7e-4f9c-91c5-8c77cec22b2c",
  Milestone: "01decdd1-51cb-11e3-9d5b-20c9d0831e59",
  Shot: "bad911de-3bd6-47b9-8b46-3476e237cb36",
};

export default {
  title: "Entity picker",
  decorators: [
    (Story) => (
      <SparkProvider session={getSession()}>
        <Story />
      </SparkProvider>
    ),
  ],
  argTypes: {
    mode: {
      options: ["QUICK", "SINGLE", "MULTI"],
      control: { type: "inline-radio" },
    },
    entityType: {
      options: Object.keys(Entity),
      control: { type: "select" },
    },
    contextId: {
      options: Object.keys(PROJECTS),
      mapping: PROJECTS,
      control: {
        type: "select",
        labels: Object.keys(PROJECTS).reduce(
          (acc, v) => ({ ...acc, [v]: v }),
          {}
        ),
      },
    },
    objectTypeId: {
      options: Object.keys(OBJECT_TYPES),
      mapping: OBJECT_TYPES,
      control: {
        type: "select",
        labels: Object.keys(PROJECTS).reduce(
          (acc, v) => ({ ...acc, [v]: v }),
          {}
        ),
      },
    },
    session: {
      table: {
        disable: true,
      },
    },
    onClose: {
      table: {
        disable: true,
      },
    },
    onSelected: {
      table: {
        disable: true,
      },
    },
    formatter: {
      table: {
        disable: true,
      },
    },
  },
};

const defaultArgs = {
  open: true,
  disableContextFilterToggle: false,
  onClose: action("close"),
  onSelected: action("onSelected"),
  session: getSession(),
  mode: "QUICK",
  entityType: "AssetVersion",
  contextId: "No Project",
  objectTypeId: "No object type",
};

export const EntityPickerExample = (args) => (
  <EntityPicker entityType="User" {...args} />
);

EntityPickerExample.args = {
  ...defaultArgs,
  entityType: "User",
};

EntityPickerExample.storyName = "Entity picker";

export const SuperOverrideTypedContextExample = (args) => (
  <EntityPicker {...args} />
);

SuperOverrideTypedContextExample.args = {
  ...defaultArgs,
  mode: "MULTI",
  entityType: "TypedContext",
  objectTypeId: "01decdd1-51cb-11e3-9d5b-20c9d0831e59",
  selected: ["42d13bb0-838e-4c98-8cc7-2b997925bd77"],
  placeholder: "Search by name",
  search: ["name"],
  projection: [
    "id",
    "name",
    "thumbnail_url",
    "thumbnail_id",
    "status.color",
    "link",
  ],
  formatter: ({ name, thumbnail_url, thumbnail_id, status, link }) => ({
    firstRow: name,
    secondRow: (
      <Fragment>
        <Breadcrumbs
          truncation="tooltip"
          items={removeLastItem(link).map(({ id, name: title }, i) => ({
            id,
            title,
            disabled: true,
          }))}
        />
        <ListItemChip icon="thumb_up" name={"Nice shot!"} truncation={false} />
      </Fragment>
    ),
    // eslint-disable-next-line camelcase
    media: thumbnail_url &&
      thumbnail_url.url && { thumbnail_id, thumbnail_url },
    status,
    returnLabel: name,
  }),
};

SuperOverrideTypedContextExample.storyName =
  "Super override, TypedContext example";
