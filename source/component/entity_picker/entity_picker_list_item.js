/* eslint-disable camelcase */
// :copyright: Copyright (c) 2021 ftrack

import { Fragment, useMemo } from "react";
import PropTypes from "prop-types";
import { defineMessages, FormattedMessage } from "react-intl";

import Breadcrumbs from "../breadcrumbs";
import EntityListItem, {
  ListItemChip,
  EntityThumbnail,
  EntityStatusBar,
  EntityListItemContent,
  EntityListItemRow,
  ListItemChips,
  EntityToggle,
} from "../entity_list_item";
import { DateFormatter } from "../formatter";
import styles from "./style.scss";
import { Entity } from "../formatter/entityUtils";

const messages = defineMessages({
  "open-review": {
    id: "ftrack-spark-components.entity-picker-list-item.open-review",
    defaultMessage: "Open",
  },
  "closed-review": {
    id: "ftrack-spark-components.entity-picker-list-item.closed-review",
    defaultMessage: "Closed",
  },
  "list-asset-version": {
    id: "ftrack-spark-components.entity-picker-list-item.list-asset-version",
    defaultMessage: "Asset version",
  },
  "list-task": {
    id: "ftrack-spark-components.entity-picker-list-item.list-task",
    defaultMessage: "Task",
  },
});

export const iconHelper = (entity, value) => {
  switch (entity) {
    case "Project":
      if (value === "active") return "toggle_on";
      if (value === "inactive") return "toggle_off";
      if (value === "hidden") return "visibility_off";
      return undefined;
    case "ReviewSession":
      if (value) return "lock_open";
      return "lock";
    case "TypedContext":
      if (value === "Shot") return "movie";
      if (value === "Sequence") return "folder";
      if (value === "Episode") return "video_library";
      if (value === "Task") return "assignment";
      if (value === "Asset Version") return "layers";
      if (value === "Asset") return "layers";
      if (value === "Asset Build") return "folder";
      if (value === "Folder") return "folder";
      if (value === "Milestone") return "flag";
      return undefined;
    default:
      return undefined;
  }
};

export const removeLastItem = (items = []) => items.slice(0, -1);

const entityFormatter = {
  User: ({
    first_name,
    last_name,
    thumbnail_url,
    thumbnail_id,
    memberships,
  }) => ({
    firstRow: Entity.User.formatter({ first_name, last_name }),
    secondRow: (
      <ListItemChips
        items={memberships.map((m) => ({
          id: m.id,
          name: m.group.name,
          icon: "group",
        }))}
      />
    ),
    media: thumbnail_url &&
      thumbnail_url.url && { thumbnail_id, thumbnail_url },
  }),
  Group: ({ name, parent }) => ({
    firstRow: Entity.Group.formatter({ name }),
    secondRow:
      parent && parent.link ? (
        <ListItemChips
          items={parent.link.map((m) => ({
            id: m.id,
            name: m.name,
            icon: "group",
          }))}
        />
      ) : null,
  }),
  List: ({ name, project, category, system_type }) => ({
    firstRow: Entity.List.formatter({ name }),
    secondRow: (
      <Fragment>
        {project && project.full_name && (
          <ListItemChip name={project.full_name} color={project.color} />
        )}
        {category && <ListItemChip name={category.name} icon="folder" />}
        <ListItemChip
          icon={system_type === "assetversion" ? "layers" : "assignment"}
          name={
            system_type === "assetversion" ? (
              <FormattedMessage {...messages["list-asset-version"]} />
            ) : (
              <FormattedMessage {...messages["list-task"]} />
            )
          }
        />
      </Fragment>
    ),
  }),
  Context: ({ name, context_type, link }) => ({
    firstRow: Entity.TypedContext.formatter({ name }),
    secondRow: (
      <Fragment>
        <Breadcrumbs
          truncation="tooltip"
          items={removeLastItem(link).map(({ id, name: title }) => ({
            id,
            title,
            disabled: true,
          }))}
        />
        <ListItemChip
          sentenceCase
          icon={iconHelper("TypedContext", context_type)}
          name={context_type}
          truncation={false}
        />
      </Fragment>
    ),
  }),
  TypedContext: ({
    name,
    object_type,
    thumbnail_url,
    status,
    thumbnail_id,
    link,
  }) => ({
    firstRow: Entity.TypedContext.formatter({ name }),
    secondRow: (
      <Fragment>
        <Breadcrumbs
          truncation="tooltip"
          items={removeLastItem(link).map(({ id, name: title }) => ({
            id,
            title,
            disabled: true,
          }))}
        />
        {object_type && object_type.name && (
          <ListItemChip
            sentenceCase
            icon={iconHelper("TypedContext", object_type.name)}
            name={object_type.name}
            truncation={false}
          />
        )}
      </Fragment>
    ),
    media: thumbnail_url &&
      thumbnail_url.url && { thumbnail_id, thumbnail_url },
    status,
  }),
  Project: ({
    full_name,
    start_date,
    end_date,
    thumbnail_url,
    thumbnail_id,
    status,
  }) => ({
    firstRow: Entity.Project.formatter({ full_name }),
    secondRow: (
      <Fragment>
        <ListItemChip
          sentenceCase
          name={status}
          icon={iconHelper("Project", status)}
        />
        <ListItemChip
          icon="date_range"
          name={
            <Fragment>
              <DateFormatter value={start_date} />
              <span> - </span>
              <DateFormatter value={end_date} />
            </Fragment>
          }
        />
      </Fragment>
    ),
    media: thumbnail_url &&
      thumbnail_url.url && { thumbnail_id, thumbnail_url },
  }),
  Component: ({ name, version, file_type = "" }) => ({
    firstRow: Entity.Component.formatter({ name }),
    secondRow: (
      <Fragment>
        {version && version.link && (
          <Breadcrumbs
            truncation="tooltip"
            items={removeLastItem(version.link).map(({ id, name: title }) => ({
              id,
              title,
              disabled: true,
            }))}
          />
        )}
        <ListItemChip
          icon="insert_drive_file"
          name={file_type ? file_type.replace(".", "").toUpperCase() : ""}
          truncation={false}
        />
      </Fragment>
    ),
  }),
  Asset: ({ name, latest_version }) => ({
    firstRow: Entity.Asset.formatter({ name }),
    secondRow: latest_version && latest_version.link && (
      <Breadcrumbs
        truncation="tooltip"
        items={removeLastItem(latest_version.link).map(
          ({ id, name: title }) => ({
            id,
            title,
            disabled: true,
          })
        )}
      />
    ),
    media: latest_version &&
      latest_version.thumbnail_url &&
      latest_version.thumbnail_url.url && {
        thumbnail_id: latest_version.thumbnail_id,
        thumbnail_url: latest_version.thumbnail_url,
      },
    status: latest_version && latest_version.status,
  }),
  AssetVersion: ({
    asset,
    version,
    thumbnail_url,
    thumbnail_id,
    link,
    status,
    task,
  }) => ({
    firstRow: (
      // Set specific style to show version number
      // when asset name gets truncated.
      <span className={styles.assetVersionContainer}>
        <span className={styles.assetVersionName}>
          {asset.name}&nbsp;-&nbsp;
        </span>
        <span className={styles.assetVersionNumber}>v{version}</span>
      </span>
    ),
    secondRow: (
      <Fragment>
        <Breadcrumbs
          truncation="tooltip"
          items={removeLastItem(link).map(({ id, name: title }) => ({
            id,
            title,
            disabled: true,
          }))}
        />
        {task && task.type && (
          <ListItemChip
            name={task.name}
            color={task.type.color}
            icon="assignment"
            truncation={false}
          />
        )}
      </Fragment>
    ),
    additional: task && task.name,
    media: thumbnail_url &&
      thumbnail_url.url && { thumbnail_id, thumbnail_url },
    status,
  }),
  ReviewSession: ({ name, project, is_open, thumbnail_id, thumbnail_url }) => ({
    firstRow: Entity.ReviewSession.formatter({ name }),
    secondRow: (
      <Fragment>
        {project && project.full_name && project.color && (
          <ListItemChip name={project.full_name} color={project.color} />
        )}
        <ListItemChip
          icon={iconHelper("ReviewSession", is_open)}
          name={
            is_open ? (
              <FormattedMessage {...messages["open-review"]} />
            ) : (
              <FormattedMessage {...messages["closed-review"]} />
            )
          }
        />
      </Fragment>
    ),
    media: thumbnail_url &&
      thumbnail_url.url && { thumbnail_id, thumbnail_url },
  }),
};

function getDefaultFormatter(entityType) {
  return entityFormatter[entityType];
}

function EntityPickerListItem({
  entity,
  entityType,
  onSelected,
  active,
  selected,
  disabled,
  showToggle,
  onMouseEnter,
  onMouseLeave,
  formatter = getDefaultFormatter(entityType),
  disabledTooltip,
}) {
  const item = useMemo(
    () => (entity && formatter ? formatter(entity) : {}),
    // dependency array below should also include formatter, but this causes issues when switching entityType,
    // since the entity prop and entityType might be mismatched for a render frame, causing errors.
    // once React 18 is released, I believe this will have been resolved since they will group state updates
    // in a more efficient way. try to readd formatter here and see if switching entityType in the storybook causes any issues.
    // JC
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [entity]
  );

  const handleSelect = () => {
    if (!disabled) {
      onSelected(entity);
    }
  };

  return (
    <EntityListItem
      disableShadows
      disableActiveIndicator
      disabled={disabled}
      active={active}
      onClick={handleSelect}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
    >
      {item.media && (
        <EntityThumbnail
          selectable={showToggle}
          selected={selected}
          disabled={disabled}
          disabledTooltip={disabledTooltip}
          onClick={handleSelect}
          media={{
            assetVersion: {
              thumbnail_id: item.media.thumbnail_id,
            },
            thumbnailUrl: item.media.thumbnail_url.url,
          }}
        />
      )}
      {!item.media && showToggle && (
        <EntityToggle
          selected={selected}
          disabled={disabled}
          disabledTooltip={disabledTooltip}
          onClick={handleSelect}
        />
      )}
      <EntityListItemContent>
        {item.firstRow && (
          <EntityListItemRow className={styles.firstRow}>
            <p className={styles.truncateText}>{item.firstRow}</p>
          </EntityListItemRow>
        )}
        {item.secondRow && (
          <EntityListItemRow className={styles.secondRow}>
            {item.secondRow}
          </EntityListItemRow>
        )}
      </EntityListItemContent>
      {item.status && <EntityStatusBar status={item.status} />}
    </EntityListItem>
  );
}

EntityPickerListItem.propTypes = {
  entity: PropTypes.shape({
    name: PropTypes.string,
  }),
  onSelected: PropTypes.func,
  active: PropTypes.bool,
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  selected: PropTypes.bool,
  disabled: PropTypes.bool,
  disabledTooltip: PropTypes.node,
  showToggle: PropTypes.bool,
  formatter: PropTypes.func,
  entityType: PropTypes.string,
};

export default EntityPickerListItem;
