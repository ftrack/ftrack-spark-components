// :copyright: Copyright (c) 2019 ftrack

import { Suspense } from "react";
import PropTypes from "prop-types";

const LazyLoad = ({ component: Component, fallback, ...rest }) => (
  <Suspense fallback={fallback}>
    <Component {...rest} />
  </Suspense>
);

LazyLoad.propTypes = {
  component: PropTypes.elementType,
  fallback: PropTypes.node,
};

LazyLoad.defaultProps = {
  fallback: "",
};

export default LazyLoad;
