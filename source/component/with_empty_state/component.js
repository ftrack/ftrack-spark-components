// :copyright: Copyright (c) 2017 ftrack

import { createElement } from "react";

import style from "./style.scss";

function withEmptyState(component, emptyState, isEmptyCondition) {
  return function render(props) {
    // Always render *component* as it may have dependencies on being mounted
    // to load data correctly.
    return (
      <div className={style.container}>
        {createElement(component, props)}
        {isEmptyCondition(props) ? (
          <div className={style["empty-state-wrapper"]}>
            <div>
              {createElement(emptyState, {
                originalProps: props,
              })}
            </div>
          </div>
        ) : null}
      </div>
    );
  };
}

export default withEmptyState;
