// :copyright: Copyright (c) 2018 ftrack

import PropTypes from "prop-types";

import { Component } from "react";
import { ProgressBar } from "react-toolbox";
import { defineMessages, intlShape } from "react-intl";

import safeInjectIntl from "../util/hoc/safe_inject_intl";
import style from "./style.scss";

const messages = defineMessages({
  "message-video-not-supported": {
    id: "ftrack-spark-components.media.video.message-video-not-supported",
    defaultMessage: "Your browser does not support the video tag.",
  },
});

class Video extends Component {
  constructor() {
    super();

    this.state = {
      height: 0,
      width: 0,
    };
  }

  componentWillMount() {
    /**
     * Load video "off-dome" to get original width and height
     * of the clip. Remove it when done.
     */
    const { url } = this.props;
    const video = document.createElement("video");
    video.setAttribute("style", "opacity: 0;");

    video.autoplay = true;
    video.oncanplay = (event) => {
      const height = event.target.offsetHeight;
      const width = event.target.offsetWidth;
      this.setState({ width, height });
      document.body.removeChild(video);
    };

    document.body.appendChild(video);
    video.src = url;
  }

  render() {
    const { url, intl } = this.props;
    const { width, height } = this.state;
    const { formatMessage } = intl;

    if (!height && !width) {
      return (
        <div className={style.loading}>
          <ProgressBar type="circular" mode="indeterminate" />
        </div>
      );
    }

    return (
      <div
        className={style["video-container"]}
        style={{ maxWidth: width, maxHeight: height }}
      >
        <video className={style.video} autoPlay controls>
          <source src={url} type="video/mp4" />
          {formatMessage(messages["message-video-not-supported"])}
        </video>
      </div>
    );
  }
}

Video.propTypes = {
  url: PropTypes.string.isRequired,
  intl: intlShape.isRequired,
};

export default safeInjectIntl(Video);
