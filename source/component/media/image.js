// :copyright: Copyright (c) 2018 ftrack

import PropTypes from "prop-types";

import { Component } from "react";
import { ProgressBar } from "react-toolbox";

import style from "./style.scss";

class Image extends Component {
  constructor() {
    super();

    this.state = {};
    this.state.isLoaded = false;
  }

  componentWillMount() {
    this.loadImage(this.props.url);
  }

  componentWillReceiveProps(nextProps) {
    this.loadImage(nextProps.url);
  }

  componentWillUnmount() {
    this.image = null;
  }

  // Load *url* in an off DOM image and set isLoaded when done.
  loadImage(url) {
    this.setState({ isLoaded: false });

    this.image = new window.Image();
    this.image.onload = () => {
      if (this.image) {
        this.setState({ isLoaded: true });
      }
    };
    this.image.src = url;
  }

  render() {
    const { url } = this.props;
    const { isLoaded } = this.state;

    if (!isLoaded) {
      return (
        <div className={style.loading}>
          <ProgressBar type="circular" mode="indeterminate" />
        </div>
      );
    }

    const maxWidth = this.image.width;
    const maxHeight = this.image.height;

    return (
      <div
        className={style["preview-image"]}
        style={{
          backgroundImage: `url(${url})`,
          maxWidth,
          maxHeight,
        }}
      />
    );
  }
}

Image.propTypes = {
  url: PropTypes.string.isRequired,
};

export default Image;
