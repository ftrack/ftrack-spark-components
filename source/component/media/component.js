// :copyright: Copyright (c) 2018 ftrack

import PropTypes from "prop-types";

import { Component } from "react";
import log from "loglevel";

import Video from "./video";
import Image from "./image";
import {
  SUPPORTED_IMG_FILE_TYPES,
  SUPPORTED_VIDEO_FILE_TYPES,
} from "../util/constant";

const MediaFormats = {
  "ftrackreview-image": "image",
};

const browserSupports = (format) => {
  const videoElement = document.createElement("video");
  let supported = false;

  try {
    if (videoElement.canPlayType) {
      if (format === "mp4") {
        supported = videoElement
          .canPlayType('video/mp4; codecs="avc1.42E01E"')
          .replace(/^no$/, "");
      } else if (format === "webm") {
        supported = videoElement
          .canPlayType('video/webm; codecs="vp8, vorbis"')
          .replace(/^no$/, "");
      }
    }
  } catch (error) {
    log.error("Error when testing browser media support:", error);
  }

  return supported;
};

SUPPORTED_IMG_FILE_TYPES.forEach((fileType) => {
  MediaFormats[`.${fileType}`] = "image";
});

SUPPORTED_VIDEO_FILE_TYPES.forEach((fileType) => {
  if (browserSupports(fileType)) {
    MediaFormats[`.${fileType}`] = "video";
  }
});

/**
 * Add 'ftrackreview-mp4' and 'ftrackreview-webm' as fallback when
 * file_type is not set on the media component
 */
if (Object.prototype.hasOwnProperty.call(MediaFormats, ".mp4")) {
  MediaFormats["ftrackreview-mp4"] = "video";
}

if (Object.prototype.hasOwnProperty.call(MediaFormats, ".webm")) {
  MediaFormats["ftrackreview-webm"] = "video";
}

const MediaComponents = {
  image: Image,
  video: Video,
};

class Media extends Component {
  constructor() {
    super();
    this.state = {
      url: null,
      format: "",
      fileType: "",
    };
  }

  componentWillMount() {
    const { component } = this.props;
    if (component) {
      this.setComponent(component);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.component !== this.props.component) {
      const { component } = nextProps;
      this.setComponent(component);
    }
  }

  setComponent(component) {
    const { session } = this.props;

    // Fall back to component name if file_type is not set.
    const fileType = component.file_type || component.name;
    const format = MediaFormats[fileType.trim().toLowerCase()];

    this.setState({
      url: component.url || session.getComponentUrl(component.id),
      format,
      fileType,
    });
  }

  render() {
    const { format, url } = this.state;

    if (!url || !format) {
      return null;
    }

    const MediaComponent = MediaComponents[format];
    return <MediaComponent url={url} />;
  }
}

Media.propTypes = {
  session: PropTypes.object.isRequired, // eslint-disable-line
  component: PropTypes.object, // eslint-disable-line
};

export default Media;
