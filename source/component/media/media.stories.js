import { Component } from "react";
import centered from "@storybook/addon-centered/react";

import getSession from "../../story/get_session";
import { Image, Video, Media } from ".";
import {
  getMediaComponentsForComponents,
  ASSET_VERSION_MEDIA_ATTRIBUTES,
} from "../util/get_media_component";

export default {
  title: "Media",
  decorators: [centered],
};

const containerStyle = {
  width: "1920px",
  height: "1080px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

export const _Image = () => (
  <div style={containerStyle}>
    <Image
      url={
        "https://www.ftrack.com/wp-content/uploads/2016/08/ftrack-logo-dark-x1.png"
      }
    />
  </div>
);

export const _Video = () => (
  <div style={containerStyle}>
    <Video url={"http://techslides.com/demos/sample-videos/small.mp4"} />
  </div>
);

export const ShowMediaComponent = () => (
  <div style={containerStyle}>
    <Media
      component={{
        name: "ftrackreview-mp4",
        file_type: ".mp4",
        id: "f6f48bc2-67c5-11e7-bd87-0a580ae40a16",
      }}
      session={getSession()}
    />
  </div>
);

ShowMediaComponent.storyName = "Show media component";

const COMPONENT_SERVER_LOCATION = {
  __entity_type__: "ComponentLocation",
  location_id: "3a372bde-05bc-11e4-8908-20c9d081909b",
  id: "a022e05e-970e-4da8-9ea9-95c02b102b1f",
  resource_identifier: "1478db4c-7a98-43a0-ae51-8443d75dffb5",
  url: {
    value: "http://example.com/1478db4c-7a98-43a0-ae51-8443d75dffb5",
  },
};

const COMPONENT_SERVER_LOCATION_NO_URL = {
  __entity_type__: "ComponentLocation",
  location_id: "3a372bde-05bc-11e4-8908-20c9d081909b",
  id: "a022e05e-970e-4da8-9ea9-95c02b102b1f",
  resource_identifier: "1478db4c-7a98-43a0-ae51-8443d75dffb5",
  url: null,
};

const COMPONENT_PDF = {
  name: "ftrackreview-pdf",
  component_locations: [COMPONENT_SERVER_LOCATION],
  file_type: ".pdf",
  __entity_type__: "FileComponent",
  system_type: "file",
  id: "1478db4c-7a98-43a0-ae51-8443d75dffb5",
  metadata: [],
};

const COMPONENT_RANDOM_PDF_NAME = {
  name: "Random PDF",
  component_locations: [COMPONENT_SERVER_LOCATION],
  file_type: ".pdf",
  __entity_type__: "FileComponent",
  system_type: "file",
  id: "1478db4c-7a98-43a0-ae51-8443d75dffb5",
  metadata: [
    {
      parent_id: "574e161e-893b-4f4f-8bab-0e99fa72cdf5",
      __entity_type__: "Metadata",
      value: '{"format": "pdf"}',
      key: "ftr_meta",
    },
  ],
};

const COMPONENT_IMAGE = {
  name: "image",
  component_locations: [COMPONENT_SERVER_LOCATION],
  file_type: ".jpg",
  __entity_type__: "FileComponent",
  system_type: "file",
  id: "51c4326c-5023-4ba0-8609-522d670c9b7b",
  metadata: [],
};

const COMPONENT_MP4 = {
  name: "ftrackreview-mp4",
  component_locations: [COMPONENT_SERVER_LOCATION],
  file_type: ".mp4",
  __entity_type__: "Component",
  system_type: "component",
  id: "574e161e-893b-4f4f-8bab-0e99fa72cdf5",
  metadata: [
    {
      parent_id: "574e161e-893b-4f4f-8bab-0e99fa72cdf5",
      __entity_type__: "Metadata",
      value: '{"frameRate": 25, "frameIn": 0, "frameOut": 25}',
      key: "ftr_meta",
    },
  ],
};

const COMPONENT_MP4_NO_META = {
  name: "ftrackreview-mp4",
  component_locations: [COMPONENT_SERVER_LOCATION],
  file_type: ".mp4",
  __entity_type__: "Component",
  system_type: "component",
  id: "574e161e-893b-4f4f-8bab-0e99fa72cdf5",
  metadata: [],
};

const COMPONENT_MP4_NO_URL = {
  name: "ftrackreview-mp4",
  component_locations: [COMPONENT_SERVER_LOCATION_NO_URL],
  file_type: ".mp4",
  __entity_type__: "Component",
  system_type: "component",
  id: "574e161e-893b-4f4f-8bab-0e99fa72cdf5",
  metadata: [
    {
      parent_id: "574e161e-893b-4f4f-8bab-0e99fa72cdf5",
      __entity_type__: "Metadata",
      value:
        '{"frameIn": 0, "format": "h264", "frameRate": 30.0, "source": "ee7b051c-7e1a-11e9-b98a-8c8590b2e7c5", "error": null, "frameOut": 465}',
      key: "ftr_meta",
    },
  ],
};

const COMPONENT_MP4_RANDOM_NAME = {
  name: "Foo",
  component_locations: [COMPONENT_SERVER_LOCATION],
  file_type: ".mp4",
  __entity_type__: "Component",
  system_type: "component",
  id: "574e161e-893b-4f4f-8bab-0e99fa72cdf5",
  metadata: [
    {
      parent_id: "574e161e-893b-4f4f-8bab-0e99fa72cdf5",
      __entity_type__: "Metadata",
      value:
        '{"frameIn": 0, "format": "h264", "frameRate": 30.0, "source": "ee7b051c-7e1a-11e9-b98a-8c8590b2e7c5", "error": null, "frameOut": 465}',
      key: "ftr_meta",
    },
  ],
};

const COMPONENT_MP4_LOW_RES = {
  name: "ftrackreview-mp4",
  component_locations: [COMPONENT_SERVER_LOCATION],
  file_type: ".mp4",
  __entity_type__: "Component",
  system_type: "component",
  id: "574e161e-893b-4f4f-8bab-0e99fa72cdf5",
  metadata: [
    {
      parent_id: "574e161e-893b-4f4f-8bab-0e99fa72cdf5",
      __entity_type__: "Metadata",
      value:
        '{"height": 720, "width": 1280,"frameIn": 0, "format": "h264", "frameRate": 30.0, "source": "ee7b051c-7e1a-11e9-b98a-8c8590b2e7c5", "error": null, "frameOut": 465}',
      key: "ftr_meta",
    },
    {
      parent_id: "574e161e-893b-4f4f-8bab-0e99fa72cdf5",
      __entity_type__: "Metadata",
      value: "e8ff1a23-7e1a-11e9-9345-8c8590b2e7c5",
      key: "source_component_id",
    },
  ],
};

const COMPONENT_MP4_HIGH_RES = {
  name: "ftrackreview-mp4",
  component_locations: [COMPONENT_SERVER_LOCATION],
  file_type: ".mp4",
  __entity_type__: "Component",
  system_type: "component",
  id: "574e161e-893b-4f4f-8bab-0e99fa72cdf5",
  metadata: [
    {
      parent_id: "574e161e-893b-4f4f-8bab-0e99fa72cdf5",
      __entity_type__: "Metadata",
      value:
        '{"height": 1200, "width": 2000,"frameIn": 0, "format": "h264", "frameRate": 30.0, "source": "ee7b051c-7e1a-11e9-b98a-8c8590b2e7c5", "error": null, "frameOut": 465}',
      key: "ftr_meta",
    },
    {
      parent_id: "574e161e-893b-4f4f-8bab-0e99fa72cdf5",
      __entity_type__: "Metadata",
      value: "e8ff1a23-7e1a-11e9-9345-8c8590b2e7c5",
      key: "source_component_id",
    },
  ],
};

const COMPONENT_MP4_TOO_HIGH_RES = {
  name: "ftrackreview-mp4",
  component_locations: [COMPONENT_SERVER_LOCATION],
  file_type: ".mp4",
  __entity_type__: "Component",
  system_type: "component",
  id: "574e161e-893b-4f4f-8bab-0e99fa72cdf5",
  metadata: [
    {
      parent_id: "574e161e-893b-4f4f-8bab-0e99fa72cdf5",
      __entity_type__: "Metadata",
      value:
        '{"height": 6000, "width": 1200,"frameIn": 0, "format": "h264", "frameRate": 30.0, "source": "ee7b051c-7e1a-11e9-b98a-8c8590b2e7c5", "error": null, "frameOut": 465}',
      key: "ftr_meta",
    },
    {
      parent_id: "574e161e-893b-4f4f-8bab-0e99fa72cdf5",
      __entity_type__: "Metadata",
      value: "e8ff1a23-7e1a-11e9-9345-8c8590b2e7c5",
      key: "source_component_id",
    },
  ],
};

const COMPONENT_MP4_ENCODING = {
  name: "ftrackreview-mp4",
  component_locations: [COMPONENT_SERVER_LOCATION],
  file_type: ".mp4",
  __entity_type__: "Component",
  system_type: "component",
  id: "76e3e911-ec0e-5ba3-bef4-c5b2dcca99a5",
  metadata: [
    {
      parent_id: "76e3e911-ec0e-5ba3-bef4-c5b2dcca99a5",
      __entity_type__: "Metadata",
      value: "encoding",
      key: "ftr_meta",
    },
  ],
};

const COMPONENT_UNKNOWN_TYPE = {
  name: "ftrackreview-unknown",
  component_locations: [COMPONENT_SERVER_LOCATION],
  file_type: ".mpg",
  __entity_type__: "Component",
  system_type: "component",
  id: "76e3e911-ec0e-5ba3-bef4-c5b2dcca99a5",
  metadata: [
    {
      parent_id: "76e3e911-ec0e-5ba3-bef4-c5b2dcca99a5",
      __entity_type__: "Metadata",
      value: "encoding",
      key: "ftr_meta",
    },
  ],
};

const COMPONENT_WEBM = {
  name: "ftrackreview-webm",
  component_locations: [COMPONENT_SERVER_LOCATION],
  file_type: ".webm",
  __entity_type__: "Component",
  system_type: "component",
  id: "95fe6048-72ae-5247-9e82-bbf0e2e57c07",
  metadata: [
    {
      parent_id: "95fe6048-72ae-5247-9e82-bbf0e2e57c07",
      __entity_type__: "Metadata",
      value:
        '{"frameRate": 24.0, "frameIn": 0, "frameOut": 68.016000000000005}',
      key: "ftr_meta",
    },
  ],
};

const COMPONENT_IMAGE_HIGH_RESOLUTION = {
  name: "ftrackreview-image",
  component_locations: [COMPONENT_SERVER_LOCATION],
  file_type: ".jpg",
  __entity_type__: "FileComponent",
  system_type: "file",
  id: "c9fe4005-ddc6-11e8-be98-8c8590a671fa",
  metadata: [
    {
      parent_id: "c9fe4005-ddc6-11e8-be98-8c8590a671fa",
      __entity_type__: "Metadata",
      value: '{"format": "image", "width": 2000, "height": 1600}',
      key: "ftr_meta",
    },
    {
      parent_id: "c9fe4005-ddc6-11e8-be98-8c8590a671fa",
      __entity_type__: "Metadata",
      value: "1478db4c-7a98-43a0-ae51-8443d75dffb5",
      key: "source_component_id",
    },
  ],
};

const COMPONENT_IMAGE_TOO_HIGH_RESOLUTION = {
  name: "ftrackreview-image",
  component_locations: [COMPONENT_SERVER_LOCATION],
  file_type: ".jpg",
  __entity_type__: "FileComponent",
  system_type: "file",
  id: "c9fe4005-ddc6-11e8-be98-8c8590a671fa",
  metadata: [
    {
      parent_id: "c9fe4005-ddc6-11e8-be98-8c8590a671fa",
      __entity_type__: "Metadata",
      value: '{"format": "image", "width": 20000, "height": 300}',
      key: "ftr_meta",
    },
    {
      parent_id: "c9fe4005-ddc6-11e8-be98-8c8590a671fa",
      __entity_type__: "Metadata",
      value: "1478db4c-7a98-43a0-ae51-8443d75dffb5",
      key: "source_component_id",
    },
  ],
};

const COMPONENT_IMAGE_LOW_RESOLUTION = {
  name: "ftrackreview-image",
  component_locations: [COMPONENT_SERVER_LOCATION],
  file_type: ".jpg",
  __entity_type__: "FileComponent",
  system_type: "file",
  id: "e171a151-ddc6-11e8-949b-8c8590a671fa",
  metadata: [
    {
      parent_id: "e171a151-ddc6-11e8-949b-8c8590a671fa",
      __entity_type__: "Metadata",
      value: '{"format": "image", "width": 320, "height": 240}',
      key: "ftr_meta",
    },
    {
      parent_id: "e171a151-ddc6-11e8-949b-8c8590a671fa",
      __entity_type__: "Metadata",
      value: "51c4326c-5023-4ba0-8609-522d670c9b7b",
      key: "source_component_id",
    },
  ],
};

const COMPONENT_RANDOM_IMAGE_NAME = {
  name: "Random image",
  component_locations: [COMPONENT_SERVER_LOCATION],
  file_type: ".jpg",
  __entity_type__: "FileComponent",
  system_type: "file",
  id: "e171a151-ddc6-11e8-949b-8c8590a671fa",
  metadata: [
    {
      parent_id: "e171a151-ddc6-11e8-949b-8c8590a671fa",
      __entity_type__: "Metadata",
      value: '{"format": "image"}',
      key: "ftr_meta",
    },
    {
      parent_id: "e171a151-ddc6-11e8-949b-8c8590a671fa",
      __entity_type__: "Metadata",
      value: "51c4326c-5023-4ba0-8609-522d670c9b7b",
      key: "source_component_id",
    },
  ],
};

function JSONFormat({ playable, notPlayable }) {
  return (
    <pre
      style={{
        flex: 1,
        overflow: "scroll",
        borderLeft: "2px solid",
        paddingLeft: "40px",
      }}
    >
      {playable.length ? (
        <p>
          <b>Playable:</b>
          <br />
          {JSON.stringify(playable, null, 2)}
        </p>
      ) : null}
      {notPlayable.length ? (
        <p>
          <b>Not playable:</b>
          <br />
          {JSON.stringify(notPlayable, null, 2)}
        </p>
      ) : null}
    </pre>
  );
}

function KeyTableFormat({ items }) {
  return (
    <table cellSpacing="15">
      <tr>
        <td>Name</td>
        <td>Encoding</td>
        <td>Playable</td>
        <td>URL</td>
        <td>File type</td>
        <td>Media type</td>
        <td>Resolution</td>
        <td>Problems</td>
      </tr>
      {items.map((item) => (
        <tr>
          <td>{item.name}</td>
          <td>{JSON.stringify(item.encoding)}</td>
          <td>{JSON.stringify(item.playable)}</td>
          <td>{item.value ? "<Set>" : "<Empty>"}</td>
          <td>{item.fileType}</td>
          <td>{item.type}</td>
          <td>
            {item.width || "-"} x {item.height || "-"}
          </td>
          <td>{item.playableProblems.join(", ")}</td>
        </tr>
      ))}
    </table>
  );
}

// eslint-disable-next-line
class TestMediaSortOrder extends Component {
  constructor(props) {
    super();
    const components = props.componentsData;
    this.state = {
      components,
      value: JSON.stringify(components, null, "\t"),
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const value = event.target.value;
    const components = JSON.parse(value);
    this.setState({ components, value });
  }

  render() {
    const items = getMediaComponentsForComponents(this.state.components);
    const playable = items.filter((item) => item.playable);
    const notPlayable = items.filter((item) => !item.playable);

    return (
      <div style={{ display: "flex", maxHeight: "100%" }}>
        <textarea
          style={{
            flex: 1,
            overflow: "scroll",
            padding: "10px",
            minHeight: "400px",
            minWidth: "400px",
          }}
          value={this.state.value}
          onChange={this.handleChange}
        />
        {this.props.format === "table" ? (
          <KeyTableFormat items={items} />
        ) : (
          <JSONFormat playable={playable} notPlayable={notPlayable} />
        )}
      </div>
    );
  }
}

export const ImageAndPdf = () => (
  <TestMediaSortOrder
    componentsData={[COMPONENT_RANDOM_IMAGE_NAME, COMPONENT_PDF]}
  />
);

ImageAndPdf.storyName = "Image and PDF";

export const ImagesWithDifferentResolutions = () => (
  <TestMediaSortOrder
    componentsData={[
      COMPONENT_IMAGE_LOW_RESOLUTION,
      COMPONENT_IMAGE_HIGH_RESOLUTION,
      COMPONENT_IMAGE_TOO_HIGH_RESOLUTION,
    ]}
  />
);

ImagesWithDifferentResolutions.storyName = "Images with different resolutions";

export const ImageVideoAndPdf = () => (
  <TestMediaSortOrder
    componentsData={[COMPONENT_RANDOM_IMAGE_NAME, COMPONENT_PDF, COMPONENT_MP4]}
  />
);

ImageVideoAndPdf.storyName = "Image, video and PDF";

export const ImagesAndOneWithoutMeta = () => (
  <TestMediaSortOrder
    componentsData={[COMPONENT_RANDOM_IMAGE_NAME, COMPONENT_IMAGE]}
  />
);

ImagesAndOneWithoutMeta.storyName = "Images and one without meta";

export const ImageAndWebm = () => (
  <TestMediaSortOrder componentsData={[COMPONENT_WEBM, COMPONENT_IMAGE]} />
);

ImageAndWebm.storyName = "Image and webm";

export const EncodingMp4AndWebm = () => (
  <TestMediaSortOrder
    componentsData={[COMPONENT_WEBM, COMPONENT_MP4_ENCODING]}
  />
);

EncodingMp4AndWebm.storyName = "Encoding mp4 and webm";

export const Mp4AndWebm = () => (
  <TestMediaSortOrder componentsData={[COMPONENT_WEBM, COMPONENT_MP4]} />
);

Mp4AndWebm.storyName = "MP4 and webm";

export const VideosOneWithoutUrl = () => (
  <TestMediaSortOrder componentsData={[COMPONENT_MP4, COMPONENT_MP4_NO_URL]} />
);

VideosOneWithoutUrl.storyName = "Videos, one without URL";

export const Mp4NoMeta = () => (
  <TestMediaSortOrder componentsData={[COMPONENT_MP4_NO_META]} />
);

Mp4NoMeta.storyName = "Mp4 no meta";

export const AllInATable = () => (
  <TestMediaSortOrder
    format="table"
    componentsData={[
      COMPONENT_PDF,
      COMPONENT_RANDOM_PDF_NAME,
      COMPONENT_IMAGE,
      COMPONENT_UNKNOWN_TYPE,
      COMPONENT_MP4,
      COMPONENT_MP4_NO_URL,
      COMPONENT_MP4_LOW_RES,
      COMPONENT_MP4_ENCODING,
      COMPONENT_WEBM,
      COMPONENT_IMAGE_HIGH_RESOLUTION,
      COMPONENT_MP4_TOO_HIGH_RES,
      COMPONENT_IMAGE_TOO_HIGH_RESOLUTION,
      COMPONENT_IMAGE_LOW_RESOLUTION,
      COMPONENT_MP4_HIGH_RES,
      COMPONENT_RANDOM_IMAGE_NAME,
      COMPONENT_MP4_RANDOM_NAME,
      COMPONENT_MP4_NO_META,
    ]}
  />
);

AllInATable.storyName = "All in a table";

class ShowMediaInfoForVersion extends Component {
  constructor() {
    super();
    this.state = { components: null };
  }

  componentDidMount() {
    const { session } = this.props;
    session.initializing
      .then(() => {
        return session.query(
          `select ${ASSET_VERSION_MEDIA_ATTRIBUTES.join(", ")} from
                    AssetVersion where id is "${this.props.assetVersionId}"`
        );
      })
      .then((response) => {
        const components = response.data[0].components;
        components.forEach((component) => {
          delete component.version;
          component.component_locations.forEach((componentLocation) => {
            delete componentLocation.component;
          });
        });
        this.setState({ components });
      });
  }

  render() {
    const { components } = this.state;
    if (!components) {
      return <div>Loading...</div>;
    }

    return <TestMediaSortOrder componentsData={this.state.components} />;
  }
}

export const ShowMediaComponentInfo = () => (
  <div style={containerStyle}>
    <ShowMediaInfoForVersion
      assetVersionId="b81324fd-273e-40b3-a9ee-8c63623ceb5e"
      session={getSession()}
    />
  </div>
);

ShowMediaComponentInfo.storyName = "Show media component info";
