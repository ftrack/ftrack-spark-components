// :copyright: Copyright (c) 2018 ftrack
export const FORMAT_FRAMES = "frames";
export const FORMAT_STANDARD = "standard";
export const FORMAT_TIMECODE = "timecode";

/** Return time parts based on *duration* in seconds */
export function durationToTimeParts(duration = 0) {
  let totalSeconds = duration;

  const hours = Math.floor(totalSeconds / (60.0 * 60.0));
  totalSeconds %= 60.0 * 60.0;

  const minutes = Math.floor(totalSeconds / 60.0);
  totalSeconds %= 60.0;

  const seconds = Math.round(totalSeconds);

  return {
    hours,
    minutes,
    seconds,
    frames: 0,
  };
}

/** Return time parts based on *frame* and *frameRate* */
export function framesToTimeParts(frame = 0, frameRate = 1) {
  const duration = Math.floor(frame / frameRate);
  const result = durationToTimeParts(duration);
  result.frames = Math.floor(frame % frameRate);

  return result;
}

/** Return *number* as a string, zero-padded to *digits*. */
export function formatNumber(number, digits = 2) {
  return number.toFixed(0).padStart(digits, "0");
}

/** Return formatted time on the format HH:MM:SS:FF from *timeParts*. */
export function formatTimecode({ hours, minutes, seconds, frames }) {
  return [hours, minutes, seconds, frames]
    .map((num) => formatNumber(num))
    .join(":");
}

/** Return formatted time on the format [HH:]MM:SS from *timeParts*. */
export function formatStandard({ hours, minutes, seconds }) {
  let formattedTime = `${formatNumber(minutes)}:${formatNumber(seconds)}`;
  if (hours) {
    formattedTime = `${formatNumber(hours)}:${formattedTime}`;
  }
  return formattedTime;
}

/**
 * Return *currentFrame*, *totalFrames*  formatted according to *format*.
 *
 * format can be one of:
 *
 *  * FORMAT_FRAMES: FF
 *  * FORMAT_TIMECODE: HH:MM:SS:FF
 *  * FORMAT_STANDARD: [HH:]MM:SS
 */
function getFormattedTime(format, currentFrame, totalFrames, frameRate) {
  if (format === FORMAT_FRAMES) {
    const total = totalFrames.toFixed(0);
    // Offset current frame display so that it starts at 1.
    const current = formatNumber(currentFrame + 1, total.length);
    return [current, total];
  } else if (format === FORMAT_TIMECODE) {
    const currentTime = framesToTimeParts(currentFrame, frameRate);
    const totalTime = framesToTimeParts(totalFrames, frameRate);
    return [formatTimecode(currentTime), formatTimecode(totalTime)];
  }

  // format === FORMAT_STANDARD
  const currentTime = framesToTimeParts(currentFrame, frameRate);
  const totalTime = framesToTimeParts(totalFrames, frameRate);
  return [formatStandard(currentTime), formatStandard(totalTime)];
}

export default getFormattedTime;
