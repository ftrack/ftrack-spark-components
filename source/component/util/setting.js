// :copyright: Copyright (c) 2017 ftrack
let allSettings = null;
let settingsLoading = null;

/**
 * Return promise resolved with all settings.
 * Will cache settings indefinitely.
 */
export function ensureSettings(session) {
  if (allSettings) {
    return Promise.resolve(allSettings);
  } else if (!settingsLoading) {
    settingsLoading = session
      .query("select name, group, value from Setting")
      .then((response) => {
        allSettings = response.data;
        return allSettings;
      });
  }

  return settingsLoading;
}

/** Return promise resolved with setting value or *defaultValue*. */
export function getSetting(session, group, name, defaultValue = null) {
  return ensureSettings(session).then((settings) => {
    for (const setting of settings) {
      if (setting.name === name && setting.group === group) {
        return setting.value;
      }
    }

    return defaultValue;
  });
}
