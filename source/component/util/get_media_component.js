// :copyright: Copyright (c) 2018 ftrack
import {
  SUPPORTED_VIDEO_FILE_TYPES,
  SUPPORTED_PDF_FILE_TYPES,
  SUPPORTED_IMG_FILE_TYPES,
  SERVER_LOCATION_ID,
  REVIEW_LOCATION_ID,
  MEDIA_SORT_ORDER,
  MEDIA_MAX_SIZE,
  LEGACY_MEDIA_COMPONENTS,
} from "./constant";

const REVIEW_META_KEY = "ftr_meta";

// Attributes to load for media components on AssetVersion's.
export const ASSET_VERSION_MEDIA_ATTRIBUTES = [
  "components.component_locations.location_id",
  "components.component_locations.url",
  "components.metadata",
  "components.name",
  "components.file_type",
  "thumbnail_id",
  "thumbnail_url",
];

export function getEntityThumbnail({ thumbnail_id: id, thumbnail_url: url }) {
  return id && url ? url.value : null;
}

/** Sort function for prioritizing media components. */
export function sortMediaComponent(a, b) {
  // Sort based on if media is playable.
  if (a.playable && !b.playable) {
    return -1;
  } else if (b.playable && !a.playable) {
    return 1;
  }
  if (!b.playable && !a.playable && b.encoding !== a.encoding) {
    // One of them is currently encoding.
    if (a.encoding) {
      return -1;
    }

    if (b.encoding) {
      return 1;
    }
  }
  const sortOrder = [...MEDIA_SORT_ORDER, "unknown"];
  // Sort based on media sort order.
  const typeSortOrderA = sortOrder.indexOf(a.type);
  const typeSortOrderB = sortOrder.indexOf(b.type);
  if (typeSortOrderA !== typeSortOrderB) {
    return typeSortOrderA - typeSortOrderB;
  }

  // Sort based on resolution.
  const resolutionSortOrderA = (a.width || 1) * (a.height || 1);
  const resolutionSortOrderB = (b.width || 1) * (b.height || 1);
  if (resolutionSortOrderA !== resolutionSortOrderB) {
    return resolutionSortOrderB - resolutionSortOrderA;
  }

  return a.name.localeCompare(b.name);
}

/** Return { encoding, frameRate?, frameIn?, frameOut?, format? } for component. */
export function getReviewMeta(component) {
  const meta = component.metadata.find(
    (candidate) => candidate.key === REVIEW_META_KEY
  );
  try {
    if (meta && meta.value === "encoding") {
      return { encoding: true };
    } else if (meta) {
      // Pluck values of interest from review meta.
      const metaValue = JSON.parse(meta.value);
      return [
        "frameRate",
        "frameIn",
        "frameOut",
        "format",
        "height",
        "width",
      ].reduce(
        (accumulator, property) => {
          if (metaValue[property]) {
            accumulator[property] = metaValue[property];
          }
          return accumulator;
        },
        { encoding: false }
      );
    }
  } catch (err) {
    // Empty
  }

  // Review meta does not exist or could not be extracted.
  if (component.name === "ftrackreview-image") {
    return { encoding: false };
  }

  return null;
}

/** Return first *componentLocations* matching *locationId* */
function findComponentLocation(componentLocations = [], locationId) {
  return componentLocations.find(
    (candidate) => candidate.location_id === locationId
  );
}

function getComponentLocation(component) {
  // Get first component location in review or server locations.
  let componentLocation = findComponentLocation(
    component.component_locations,
    REVIEW_LOCATION_ID
  );
  if (!componentLocation) {
    componentLocation = findComponentLocation(
      component.component_locations,
      SERVER_LOCATION_ID
    );
  }

  return componentLocation;
}

// Valid file types or component names for each media type.
const MEDIA_TYPE_FILE_TYPES = {
  video: SUPPORTED_VIDEO_FILE_TYPES,
  pdf: SUPPORTED_PDF_FILE_TYPES,
  image: SUPPORTED_IMG_FILE_TYPES,
};

/** Return one of one of *MEDIA_TYPE_FILE_TYPES* or unknown for *component*. */
export function getMediaTypeForComponent(component) {
  if (LEGACY_MEDIA_COMPONENTS[component.name]) {
    return LEGACY_MEDIA_COMPONENTS[component.name];
  }

  const extension =
    component.file_type &&
    component.file_type.substr(1, component.file_type.length);

  const type = Object.keys(MEDIA_TYPE_FILE_TYPES).find((candidate) =>
    MEDIA_TYPE_FILE_TYPES[candidate].includes(extension)
  );

  if (!type) {
    return "unknown";
  }
  return type;
}

const defaultMediaComponent = {
  id: null,
  name: "unknown",
  type: "unknown",
  frameRate: 1,
  frameIn: 0,
  frameOut: 0,
  encoding: false,
  playable: false,
  value: null,
};

/**
 * Return media component information for *components*.
 *
 * Media components are marked as playable or not and will provide other
 * information.
 *
 * The result is an array sorted by Playable, Media type (Video, PDF, Image),
 * Resolution (width x height) and name.
 *
 */
export function getMediaComponentsForComponents(components) {
  const mediaInfos = components.reduce((accumulator, component) => {
    const reviewMeta = getReviewMeta(component);
    const type = getMediaTypeForComponent(component);
    const location = getComponentLocation(component);
    const mediaMaxSize = MEDIA_MAX_SIZE[type];

    // Mark components as non-playable if they are not in a supported
    // review format, have a URL, have too high resolution, is encoding,
    // or does not have required metadata.
    const problems = [];

    if (!MEDIA_SORT_ORDER.includes(type)) {
      problems.push("type-not-supported");
    }

    let url = null;
    if (location) {
      url = location.url;
    } else {
      problems.push("no-location");
    }

    if (reviewMeta === null) {
      problems.push("no-ftr-meta");
    } else if (reviewMeta.format === "unknown") {
      problems.push("no-ftr-meta");
    } else {
      if (reviewMeta.encoding) {
        problems.push("encoding");
      }
      if (type === "video" && !reviewMeta.frameRate) {
        problems.push("no-frame-rate");
      }
      if (
        mediaMaxSize &&
        reviewMeta.height &&
        reviewMeta.width &&
        Math.max(reviewMeta.height, reviewMeta.width) > mediaMaxSize
      ) {
        problems.push("resolution-too-high");
      }
    }

    accumulator.push({
      ...defaultMediaComponent,
      ...(reviewMeta || {}),
      ...(url || {}),
      playable: problems.length === 0,
      playableProblems: problems,
      type,
      fileType: component.file_type,
      name: component.name,
      id: component.id,
    });

    return accumulator;
  }, []);

  // Sort media by priority.
  mediaInfos.sort(sortMediaComponent);

  return mediaInfos;
}

/**
 * Return media info for *component*.
 */
function getMediaComponent(components) {
  const selected = getMediaComponentsForComponents(components)[0];
  if (!components || !components.length) {
    return defaultMediaComponent;
  }
  return selected;
}

export default getMediaComponent;
