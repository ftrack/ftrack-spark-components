// :copyright: Copyright (c) 2017 ftrack
/* eslint-disable import/prefer-default-export */

/**
 * Return guessed name from *email*
 *
 * Retrieves the part before the at sign, replaces separators with space
 * and transform to title case.
 */
export function guessName(email) {
  let name = email.split("@")[0];
  name = name.replace(/[._-]/g, " ").replace(/\s\s+/g, " ");
  name = name.replace(
    /\w\S*/g,
    (word) => word.charAt(0).toUpperCase() + word.substr(1).toLowerCase()
  );
  return name;
}

/** Extract version number from *entityName* if ending with "Version 1" or " v1".*/
export function getVersionFromEntityNameIfPossible(entityName) {
  const versionLookup = (/[(?: v)|(?:Version )](\d+)$/.exec(entityName) || [
    false,
    false,
  ])[1]; // return false if not found
  if (versionLookup) {
    return versionLookup;
  }
  return false;
}

/**
 * Return initials for *inputString*.
 *
 * Can be used to generate initials for an user name or group name.
 * Eg. "Carl Claesson" -> "CC"
 */
export function getInitialsFromString(inputString = "") {
  return inputString
    .split(" ")
    .map((word) => word.charAt(0))
    .join("")
    .slice(0, 2)
    .toUpperCase();
}
