// :copyright: Copyright (c) 2017 ftrack

export const PROJECT_COLORS = [
  "#F44336",
  "#E91E63",
  "#9C27B0",
  "#673AB7",
  "#3F51B5",
  "#2196F3",
  "#03A9F4",
  "#00BCD4",
  "#009688",
  "#4CAF50",
  "#8BC34A",
  "#CDDC39",
  "#FFEB3B",
  "#FFC107",
  "#FF9800",
  "#FF5722",
  "#795548",
  "#9E9E9E",
  "#607D8B",
];

/** Generic entity type icons. */
export const ENTITY_TYPE_ICONS = {
  ReviewSessionObject: "ondemand_video",
  AssetVersion: "layers",
  TypedContext: "folder",
  Project: "lens",
};

/** Icons for different media types. */
export const MEDIA_TYPE_ICONS = {
  video: "theaters",
  image: "photo",
  pdf: "picture_as_pdf",
  unknown: "insert_drive_file",
};

/**
 * Image sizes used when generating thumbnails through thumbor.
 *
 * The amount of different sizes should be kept small so that it is likely that
 * resized images are already cached.
 */
export const THUMBNAIL_SIZES = {
  small: 100,
  medium: 300,
  large: 500,
  xlarge: 1280,
};

/** Background color for identifying different resource types. */
export const RESOURCE_TYPE_COLORS = {
  user: "#3498db",
  group: "#9b59b6",
  membership: "#3498db",
};

/** Date format used when encoding for the ftrack API. */
export const ENCODE_DATE_FORMAT = "YYYY-MM-DD";

/** Datetime format used when encoding for the ftrack API. */
export const ENCODE_DATETIME_FORMAT = "YYYY-MM-DDTHH:mm:ss";

/** ISO format used with json schema. */
export const DATETIME_ISO_FORMAT = "YYYY-MM-DDTHH:mm:ssZ";

/** The note category id for review session notes. */
export const REVIEW_SESSION_NOTE_CATEGORY =
  "42983ba0-53b0-11e4-916c-0800200c9a66";

// Components file a file type in this list will be displayed using an img
// element.
export const SUPPORTED_IMG_FILE_TYPES = ["png", "gif", "jpeg", "jpg", "bmp"];

export const SUPPORTED_VIDEO_FILE_TYPES = ["mp4", "webm", "mov"];

export const SUPPORTED_PDF_FILE_TYPES = ["pdf"];

export const MEDIA_SORT_ORDER = ["video", "pdf", "image"];

export const LEGACY_MEDIA_COMPONENTS = {
  "ftrackreview-image": "image",
  "ftrackreview-mp4": "video",
  "ftrackreview-webm": "video",
  "ftrackreview-pdf": "pdf",
};

export const MEDIA_MAX_SIZE = {
  video: 4096,
  image: 10240,
};

export const SERVER_LOCATION_ID = "3a372bde-05bc-11e4-8908-20c9d081909b";
export const REVIEW_LOCATION_ID = "cd41be70-8809-11e3-b98a-20c9d081909b";

/** Default duration for transitions */
export const TRANSITION_DURATION = 200;

export const SECONDS_IN_HOUR = 3600;

/** Default workday length is 8 hours. */
export const DEFAULT_WORKDAY_LENGTH = SECONDS_IN_HOUR * 8;

export const DEFAULT_DISPLAY_BID_AS_DAYS = false;

export const MENTION_REGEX =
  /@\{([\p{L}\p{Mn}0-9_ ()]*):([0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12})\}/gu;

export const UUID_REGEX =
  /\b[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-f]{4}-\b[0-9a-fA-F]{12}\b/;
