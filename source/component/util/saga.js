// :copyright: Copyright (c) 2019 ftrack

import { put, call } from "redux-saga/effects";
import log from "loglevel";

/** Function for error handling for Sagas. */
// eslint-disable-next-line import/prefer-default-export
export function catchAllErrors(saga, { errorActionCreator, onError } = {}) {
  const wrapped = function* wrappedTryCatch() {
    try {
      // eslint-disable-next-line prefer-rest-params
      yield call(saga, ...arguments);
    } catch (error) {
      log.error("Error in saga", saga, error);
      if (onError) {
        yield onError(error);
      }
      if (errorActionCreator) {
        yield put(errorActionCreator({ error }));
      }
    }
  };
  wrapped._original = saga;
  return wrapped;
}
