// :copyright: Copyright (c) 2019 ftrack

export function isTypedContextSubclass(model, schemas) {
  const schema = schemas.find((candidate) => candidate.id === model);

  if (schema && schema.alias_for && schema.alias_for.id === "Task") {
    return true;
  }
  return false;
}

/** Get schema ID for *objectTypeId* */
export function getObjectTypeSchemaId(schemas, objectTypeId) {
  const schema = schemas.find(
    (candidate) =>
      candidate.alias_for?.id === "Task" &&
      candidate.alias_for?.classifiers?.object_typeid === objectTypeId
  );

  if (!schema) {
    throw new Error(`Failed to get schema id for object type ${objectTypeId}`);
  }

  return schema.id;
}

export default isTypedContextSubclass;
