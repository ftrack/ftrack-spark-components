// :copyright: Copyright (c) 2016 ftrack
import { UUID_REGEX } from "../util/constant";

function getQueryFilter(queryString, attributes, baseFilter) {
  const words = queryString
    .split(" ")
    .map((word) => word.trim())
    .filter((word) => word);

  const joinedFilters = [];
  for (const word of words) {
    const attributeFilters = [];
    for (const attribute of attributes) {
      if (attribute === "id" && UUID_REGEX.test(word)) {
        attributeFilters.push(`id is "${word}"`);
      } else if (attribute !== "id") {
        attributeFilters.push(`${attribute} like "%${word}%"`);
      }
    }
    joinedFilters.push(`(${attributeFilters.join(" or ")})`);
  }
  if (joinedFilters.length && baseFilter) {
    return `(${baseFilter}) and (${joinedFilters.join(" and ")})`;
  } else if (joinedFilters.length) {
    return joinedFilters.join(" and ");
  } else if (baseFilter) {
    return baseFilter;
  }
  return "";
}

export default getQueryFilter;
