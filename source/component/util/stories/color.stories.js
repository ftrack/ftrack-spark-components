// :copyright: Copyright (c) 2018 ftrack

import { Component } from "react";
import centered from "@storybook/addon-centered/react";
import { withState, withHandlers, compose } from "recompose";
import { getForegroundColor, pickColorFromString } from "../color";
import getSession from "../../../story/get_session";

export default {
  title: "Color",
  decorators: [centered],
};

function ColorTest({ value, onValueChanged }) {
  const backgroundColor = value;
  const foregroundColor = getForegroundColor(value);
  return (
    <div>
      <input type="color" value={value} onChange={onValueChanged} />
      <hr style={{ height: "20px" }} />
      <div
        style={{
          width: "200px",
          height: "100px",
          lineHeight: "100px",
          textAlign: "center",
          backgroundColor,
          color: foregroundColor,
        }}
      >
        Foreground text
      </div>
    </div>
  );
}

const StatefulColorTest = compose(
  withState("value", "setValue", "#666666"),
  withHandlers({
    onValueChanged: (props) => (event) => props.setValue(event.target.value),
  })
)(ColorTest);

export const DynamicForegroundColor = () => <StatefulColorTest />;

DynamicForegroundColor.storyName = "Dynamic foreground color";

class PickColorFromStringTest extends Component {
  constructor() {
    super();
    this.state = { colors: ["#F0F"] };
  }

  componentDidMount() {
    const session = getSession();
    session.initializing
      .then(() =>
        session.call([
          {
            action: "query_server_information",
            values: ["default_colors"],
          },
        ])
      )
      .then(([{ default_colors: colors }]) => {
        this.setState({ colors });
      });
  }

  render() {
    const { value, onValueChanged } = this.props;
    const { colors } = this.state;
    const color = pickColorFromString(value, colors);
    const foregroundColor = getForegroundColor(color);

    return (
      <div>
        <input value={value} onChange={onValueChanged} />
        <div
          style={{
            backgroundColor: color,
            color: foregroundColor,
            padding: "16px",
            margin: "16px",
          }}
        >
          {color}
        </div>
      </div>
    );
  }
}

const StatefulPickColorFromStringTest = compose(
  withState("value", "setValue", "john.doe@example.com"),
  withHandlers({
    onValueChanged: (props) => (event) => props.setValue(event.target.value),
  })
)(PickColorFromStringTest);

export const RandomColorFromString = () => <StatefulPickColorFromStringTest />;

RandomColorFromString.storyName = "Random color from string";
