// :copyright: Copyright (c) 2018 ftrack

/** Return array of 8-bit RGB values for #NNN or #NNNNNN hex color. */
export function hexToRgb(hexColor) {
  let color = hexColor;
  if (color.indexOf("#") === 0) {
    color = color.substr(1);
  }

  // Split color to channels and expand 3-digit hex colors to 6-digit.
  const cssHexColorRe = new RegExp(`.{1,${color.length / 3}}`, "g");
  let channels = color.match(cssHexColorRe);
  if (channels && channels[0].length === 1) {
    channels = channels.map((channel) => channel + channel);
  }

  // Convert HEX channels to decimal
  return channels.map((channel) => parseInt(channel, 16));
}

/**
 * Get luminance of *color* in the range [0, 1]
 *
 * *color* should be either an array of 8-bit decimal rgb values or a hex string.
 *
 * Formula from: https://www.w3.org/TR/2008/REC-WCAG20-20081211/#relativeluminancedef
 */
export function getLuminance(color) {
  let rgbColor = color;
  if (!Array.isArray(rgbColor)) {
    rgbColor = hexToRgb(color);
  }

  // Convert 8-bit RGB color to relative luminance
  const rgb = rgbColor.map((channel) => {
    const c = channel / 255;
    return c <= 0.03928 ? c / 12.92 : Math.pow((c + 0.055) / 1.055, 2.4);
  });

  return Number(
    (0.2126 * rgb[0] + 0.7152 * rgb[1] + 0.0722 * rgb[2]).toFixed(3)
  ); // clamp to 3 digits
}

/** Return light or dark text color for use on *backgroundColor*. */
export function getForegroundColor(backgroundColor) {
  const lightColor = "white";
  const darkColor = "black";
  const luminanceLimit = 0.6;

  try {
    const luminance = getLuminance(backgroundColor);
    if (luminance < luminanceLimit) {
      return lightColor;
    }
  } catch (err) {
    // Ignore errors and return default
  }

  return darkColor;
}

/** Return a HEX color based on *text*.
 *
 * The result of this function is meant to produce stable colors based on
 * *text* and has an equivalent in Python. These two implementations
 * are meant to produce identical results. This is not the case for double
 * byte strings and thus this implementation is recommended for
 * email addresses and other strings that cannot contain such characters.
 */
export function pickColorFromString(text = "", colors = []) {
  const value = text
    .split("")
    .reduce(
      (accumulator, character, index) =>
        accumulator + character.charCodeAt(0) + index,
      0
    );
  return colors[value % colors.length];
}

export function pickReviewColor(syncUser, colors = []) {
  if (!syncUser) {
    return null;
  }

  const { name, email } = syncUser;
  return pickColorFromString(email || name || "", colors);
}
