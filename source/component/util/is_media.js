// :copyright: Copyright (c) 2018 ftrack

import { SUPPORTED_IMG_FILE_TYPES } from "./constant";

/** Return true if *component* is assumed to be consumable media. */
function isMedia(fileType = "") {
  const lowerCasedFileType = fileType.toLowerCase();
  return SUPPORTED_IMG_FILE_TYPES.includes(lowerCasedFileType);
}

export default isMedia;
