// :copyright: Copyright (c) 2021 ftrack

import { useRef, useEffect } from "react";

/**
 * Get previous value in react render cycle.
 *
 * *value* can be anything that needs to keep track of previous value.
 *
 * Example use do deep equal between complex objects / arrays.
 */
export default function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}
