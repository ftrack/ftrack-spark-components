// :copyright: Copyright (c) 2021 ftrack

import { useEffect } from "react";
import usePrevious from "./use_previous";

/**
 * Used to scroll a specfic list item index into view.
 *
 * *ref* ref to parent container for list.
 *
 * *currentIndex* index of item that should be scrolled into view.
 *
 *  *canScroll* if scroll should be active (optional) default true
 */
export default function useScrollElementVisible(
  ref,
  currentIndex,
  canScroll = true
) {
  const prevIndex = usePrevious(currentIndex);

  useEffect(
    () => {
      if (ref && canScroll) {
        const children = [].slice.call(ref.current.children);
        const selectedChild = children[currentIndex];
        if (selectedChild) {
          const containerHeight = ref.current.offsetHeight;
          const viewTop = ref.current.scrollTop;
          const viewBottom = viewTop + containerHeight;

          const itemHeight = selectedChild.offsetHeight;
          const itemTop = selectedChild.offsetTop;
          const itemBottom = itemTop + itemHeight;

          if (!prevIndex || prevIndex < currentIndex) {
            if (itemBottom > viewBottom) {
              ref.current.scrollTo({
                top: itemTop - (ref.current.offsetHeight - itemHeight),
                behavior: "smooth",
              });
            }
          } else if (prevIndex > currentIndex) {
            if (itemTop - itemHeight * 2 < viewTop) {
              ref.current.scrollTo({
                top: viewTop - itemHeight,
                behavior: "smooth",
              });
            }
          }
        }
      }
    },
    // TODO: Feel free to fix this eslint error if you got time
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [ref, prevIndex, canScroll]
  );
}
