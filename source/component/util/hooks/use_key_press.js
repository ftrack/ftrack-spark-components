// :copyright: Copyright (c) 2021 ftrack

import { useState, useCallback, useEffect } from "react";

/**
 * Listens to specific key strokes for a DOM item.
 *
 * *ref* ref to node to listen to keystrokes
 *
 * *targetKey* array of keynames to keep track of, example ["ArrowUp", "Enter", "Control+Enter"]
 *
 * *callback* function called when key is released.
 */
export default function useKeyPress(ref, targetKeys, callback) {
  const [map, setMap] = useState({});

  const upHandler = useCallback(
    ({ key }) => {
      setMap((currMap) => ({
        ...currMap,
        [key]: false,
      }));
    },
    // TODO: Feel free to fix this eslint error if you got time
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [callback, setMap, targetKeys]
  );

  const downHandler = useCallback(
    ({ key }) => {
      const possibleKeys = targetKeys.filter((k) => k.split("+").includes(key));
      const otherKeysPressed = Object.values({
        ...map,
        [key]: false,
      }).includes(true);
      if (possibleKeys.length) {
        if (
          possibleKeys.length === 1 &&
          possibleKeys[0].split("+").length === 1 &&
          !otherKeysPressed
        ) {
          callback(key);
        } else {
          let matchedCombination;

          const possibleNotCurrent = possibleKeys.filter((p) => p !== key);
          possibleNotCurrent.forEach((possibleKey) => {
            const keyArr = possibleKey.split("+").filter((k) => k !== key);
            const combinationLocked = keyArr.reduce((prev, curr) => {
              if (!prev) {
                return prev;
              }
              if (map[curr]) {
                return true;
              }
              return false;
            }, true);
            if (combinationLocked) {
              matchedCombination = possibleKey;
            }
          });
          if (matchedCombination) {
            callback(matchedCombination);
          } else if (!otherKeysPressed) {
            callback(key);
          }
        }
      }
      setMap((currMap) => ({
        ...currMap,
        [key]: true,
      }));
    },
    // TODO: Feel free to fix this eslint error if you got time
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [callback, setMap, targetKeys]
  );

  useEffect(() => {
    if (ref.current) {
      const keepRef = ref.current;
      keepRef.addEventListener("keydown", downHandler);
      keepRef.addEventListener("keyup", upHandler);

      return () => {
        keepRef.removeEventListener("keydown", downHandler);
        keepRef.removeEventListener("keyup", upHandler);
      };
    }
    return () => {};
  }, [ref, upHandler, downHandler]);
}
