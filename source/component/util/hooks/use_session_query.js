// :copyright: Copyright (c) 2021 ftrack

import { useState, useEffect, useCallback } from "react";

const buildQuery = (baseQuery, { limit = 10, offset = 0 }) => {
  let queryString = baseQuery;

  if (offset >= 0) {
    queryString += ` offset ${offset}`;
  }

  if (limit >= 0) {
    queryString += ` limit ${limit}`;
  }

  return queryString;
};

function useCallbackQuery(
  queryFn,
  baseQuery,
  { limit = 30, offset = 0, pause }
) {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [canFetchMore, setCanFetchMore] = useState(true);
  const [error, setError] = useState();

  const fetchData = (query, abortController, append = false) => {
    setIsLoading(true);
    setError();
    if (!append) {
      setData([]);
      setCanFetchMore(true);
    }
    queryFn(query, abortController)
      .then((response) => {
        const items = response.data;
        if (items.length < limit) {
          setCanFetchMore(false);
        }

        if (append) {
          setData((currentValues) => [...currentValues, ...items]);
        } else if (Array.isArray(items)) {
          setData(items);
        }
        setIsLoading(false);
      })
      .catch((e) => {
        if (e.name !== "AbortError") {
          setError(e);
          setIsLoading(false);
        }
      });
  };

  const fetchMore = useCallback(
    () => {
      if (!isLoading && canFetchMore) {
        const appendQuery = buildQuery(baseQuery, {
          limit,
          offset: data.length,
        });
        fetchData(appendQuery, undefined, true);
      }
    },
    // TODO: Feel free to fix this eslint error if you got time
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isLoading, canFetchMore, baseQuery]
  );

  useEffect(
    () => {
      if (!pause) {
        const query = buildQuery(baseQuery, { limit, offset });
        const abortController = new AbortController();
        if (query) {
          fetchData(query, abortController);
        }
        return () => {
          abortController.abort();
        };
      }
    },
    // TODO: Feel free to fix this eslint error if you got time
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [queryFn, baseQuery]
  );

  return {
    data,
    isLoading,
    fetchMore,
    canFetchMore,
    error,
  };
}

/**
 * Used to query data with a hook.
 *
 * *session* ftrack-session object.
 *
 * *baseQuery* query to be run, offset and limit will be added if config with limit is used.
 *
 * *config* object with property limit to handle fetch more data.
 *
 */
export function useSessionQuery({ session, baseQuery, config = {} }) {
  const queryFn = useCallback(
    (query, abortController) => session.query(query, { abortController }),
    // TODO: Feel free to fix this eslint error if you got time
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [baseQuery]
  );
  return useCallbackQuery(queryFn, baseQuery, config);
}

export function useSessionSearch({
  session,
  entityType,
  baseExpression = "",
  objectTypeIds = [],
  value = "",
  contextId,
}) {
  const terms = value.split(" ").filter((w) => w);
  const searchFn = useCallback(
    (query, abortController) =>
      session.search(
        {
          expression: query,
          entityType,
          terms,
          contextId,
          objectTypeIds,
        },
        { abortController }
      ),
    // TODO: Feel free to fix this eslint error if you got time
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [baseExpression, value, contextId, objectTypeIds.join(" "), entityType]
  );
  return useCallbackQuery(searchFn, baseExpression, { limit: 30 });
}
