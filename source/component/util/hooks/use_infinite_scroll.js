// :copyright: Copyright (c) 2021 ftrack

import { useCallback, useEffect } from "react";

/**
 * Used to keep track of when an object is scrolled into view.
 *
 * *scrollRef* ref to node to observe that will fire callback.
 *
 * *callback* function called when tracked node is scrolled into view.
 *
 */
export default function useInfiniteScroll(scrollRef, callback) {
  const scrollObserver = useCallback(
    (node) => {
      // eslint-disable-next-line
      new IntersectionObserver((entries) => {
        entries.forEach((en) => {
          if (en.intersectionRatio > 0) {
            callback();
          }
        });
      }).observe(node);
    },
    [callback]
  );

  useEffect(() => {
    if (scrollRef.current) {
      scrollObserver(scrollRef.current);
    }
  }, [scrollObserver, scrollRef]);
}
