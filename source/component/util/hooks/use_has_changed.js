// :copyright: Copyright (c) 2021 ftrack

import { isEqual } from "lodash";

import usePrevious from "./use_previous";

/**
 * Check if value have changed between rendering cycles.
 *
 * *value* can be anything that needs to do deep comparison.
 *
 * Example use do deep equal between complex objects / arrays.
 */
export default function useHasChanged(obj) {
  const prevVal = usePrevious(obj);
  return !isEqual(prevVal, obj);
}
