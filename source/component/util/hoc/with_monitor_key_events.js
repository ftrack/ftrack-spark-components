import { Component } from "react";

const withMonitorKeyPress = (keys) => (WrappedComponent) => {
  class WithMonitorKeyPress extends Component {
    constructor(props, context) {
      super(props, context);
      this.onKeyUp = this.onKeyUp.bind(this);
      this.onKeyDown = this.onKeyDown.bind(this);
      this.state = {};
    }

    componentWillMount() {
      const state = {};
      if (keys) {
        for (const keyCode in keys) {
          if (Object.prototype.hasOwnProperty.call(keys, keyCode)) {
            state[keys[keyCode]] = false;
          }
        }

        this.setState(state);
      }
    }

    componentDidMount() {
      document.body.addEventListener("keyup", this.onKeyUp);
      document.body.addEventListener("keydown", this.onKeyDown);
    }

    componentWillUnmount() {
      document.body.removeEventListener("keyup", this.onKeyUp);
      document.body.removeEventListener("keydown", this.onKeyDown);
    }

    onKeyDown(event) {
      const { keyCode } = event;
      const state = this.state;

      if (keys[keyCode]) {
        state[keys[keyCode]] = true;
        this.setState(state);
      }
    }

    onKeyUp(event) {
      const { keyCode } = event;
      const state = this.state;

      if (keys[keyCode]) {
        state[keys[keyCode]] = false;
        this.setState(state);
      }
    }

    render() {
      return <WrappedComponent {...this.state} {...this.props} />;
    }
  }

  return WithMonitorKeyPress;
};

export default withMonitorKeyPress;
