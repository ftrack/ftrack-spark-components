/*
 * Copyright 2015, Yahoo Inc.
 * Copyrights licensed under the New BSD License.
 * See the accompanying LICENSE file for terms.
 */

// Inspired by react-redux's `connect()` HOC factory function implementation:
// https://github.com/rackt/react-redux

import { Component } from "react";
import { intlShape, IntlProvider } from "react-intl";
import invariant from "invariant";

function getDisplayName(Component) {
  return Component.displayName || Component.name || "Component";
}

export default function safeInjectIntl(WrappedComponent, options = {}) {
  const { intlPropName = "intl", withRef = false } = options;

  class InjectIntl extends Component {
    constructor(props, context) {
      super(props, context);

      this.DeepWrappedComponent = null;
    }

    getWrappedInstance() {
      invariant(
        withRef,
        "[React Intl] To access the wrapped instance, " +
          "the `{withRef: true}` option must be set when calling: " +
          "`safeInjectIntl()`"
      );

      return this.refs.wrappedInstance;
    }

    render() {
      if (!this.context.intl) {
        // Only create the deep wrapped component once.
        if (!this.DeepWrappedComponent) {
          this.DeepWrappedComponent = safeInjectIntl(WrappedComponent);
        }
        const DeepWrappedComponent = this.DeepWrappedComponent;

        return (
          <IntlProvider locale={"en"}>
            <DeepWrappedComponent
              {...this.props}
              {...{ [intlPropName]: this.context.intl }}
              ref={withRef ? "wrappedInstance" : null}
            />
          </IntlProvider>
        );
      }

      return (
        <WrappedComponent
          {...this.props}
          {...{ [intlPropName]: this.context.intl }}
          ref={withRef ? "wrappedInstance" : null}
        />
      );
    }
  }
  InjectIntl.displayName = `InjectIntl(${getDisplayName(WrappedComponent)})`;
  InjectIntl.contextTypes = { intl: intlShape };
  InjectIntl.WrappedComponent = WrappedComponent;

  return InjectIntl;
}
