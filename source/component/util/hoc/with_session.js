// :copyright: Copyright (c) 2017 ftrack
import PropTypes from "prop-types";

function withSession(SessionlessComponent) {
  function WrappedComponent(props, context) {
    return <SessionlessComponent {...props} session={context.session} />;
  }

  WrappedComponent.contextTypes = {
    session: PropTypes.object,
  };

  return WrappedComponent;
}

export default withSession;
