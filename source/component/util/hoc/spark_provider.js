// :copyright: Copyright (c) 2018 ftrack
import PropTypes from "prop-types";

import { Component } from "react";

class SparkProvider extends Component {
  constructor() {
    super();
    this.state = { sessionReady: false };
  }

  getChildContext() {
    return { session: this.props.session };
  }

  componentDidMount() {
    this.props.session.initializing.then(() => {
      this.setState({ sessionReady: true });
    });
  }

  render() {
    if (!this.state.sessionReady) {
      return null;
    }

    return this.props.children;
  }
}

SparkProvider.childContextTypes = {
  session: PropTypes.object,
};

SparkProvider.propTypes = {
  session: PropTypes.object,
  children: PropTypes.node,
};

export default SparkProvider;
