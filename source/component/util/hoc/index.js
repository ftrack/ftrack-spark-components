// :copyright: Copyright (c) 2017 ftrack

/* eslint-disable react/no-multi-comp */

import PropTypes from "prop-types";

import { Component } from "react";
import { withReducer } from "react-redux-dynamic-reducer";
import { subspaced } from "redux-subspace-saga";
import injectSagas from "redux-sagas-dynamic-injector";
import {
  setDisplayName,
  wrapDisplayName,
  getContext,
  compose,
  mapProps,
} from "recompose";

import safeInjectIntl from "./safe_inject_intl";
import sizeMe from "react-sizeme";
import style from "./style.scss";

export { default as withSession } from "./with_session";
export { default as SparkProvider } from "./spark_provider";
export function withInjectSagas(sagas) {
  function wrapWithSagas(InnerComponentWithReducer) {
    class WrapperComponent extends Component {
      componentWillMount() {
        const { store } = this.context;
        const injectSagasAsync = injectSagas(store);
        injectSagasAsync(sagas);
      }

      render() {
        return <InnerComponentWithReducer {...this.props} />;
      }
    }

    WrapperComponent.contextTypes = {
      store: PropTypes.object.isRequired,
    };

    return WrapperComponent;
  }
  return wrapWithSagas;
}

/** Return a function that wraps a component in a redux subspace. */
export function withSubspace(inputReducer, inputSagas, defaultSubspace) {
  return function wrapComponent(InnerComponent) {
    const InnerComponentWithReducer = withReducer(
      inputReducer,
      defaultSubspace
    )(InnerComponent);

    const InnerComponentWithSagas = withInjectSagas(
      inputSagas.map((saga) =>
        subspaced((state) => state[defaultSubspace], defaultSubspace)(saga)
      ),
      defaultSubspace
    )(InnerComponentWithReducer);

    InnerComponentWithSagas.createInstance = (subspace) => {
      return withInjectSagas(
        inputSagas.map((saga) =>
          subspaced((state) => state[subspace], subspace)(saga)
        ),
        subspace
      )(InnerComponentWithReducer.createInstance(subspace));
    };

    return InnerComponentWithSagas;
  };
}

/** withIntl HOC for use by compose. */
export const withIntl = (BaseComponent) => safeInjectIntl(BaseComponent);

export const withAutoSize = (options = {}) => {
  const sizeMeOptions = Object.assign(
    {
      refreshRate: 1000,
      monitorHeight: true,
      monitorWidth: true,
    },
    options
  );

  return (BaseComponent) => {
    const AutoSize = sizeMe(sizeMeOptions)(({ size, ...props }) => (
      <div className={style["auto-size"]}>
        <BaseComponent width={size.width} height={size.height} {...props} />
      </div>
    ));

    if (process.env.NODE_ENV !== "production") {
      return setDisplayName(wrapDisplayName(BaseComponent, "withAutoSize"))(
        AutoSize
      );
    }

    return AutoSize;
  };
};

export const withSettings = (settings) =>
  compose(
    getContext({ settings: PropTypes.object }),
    mapProps(({ settings: contextSettings, ...props }) => {
      const settingsProps = {};
      Object.keys(settings).forEach((selected) => {
        settingsProps[selected] =
          (contextSettings && contextSettings[selected]) || settings[selected];
      });
      return { ...settingsProps, ...props };
    })
  );

export class SettingsProvider extends Component {
  getChildContext() {
    return { settings: this.props.settings };
  }

  render() {
    return this.props.children;
  }
}

SettingsProvider.childContextTypes = {
  settings: PropTypes.object,
};

SettingsProvider.propTypes = {
  settings: PropTypes.object,
  children: PropTypes.node,
};
