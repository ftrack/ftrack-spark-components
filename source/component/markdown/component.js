// :copyright: Copyright (c) 2018 ftrack
import { useState } from "react";
import PropTypes from "prop-types";
import MarkdownIt from "markdown-it";
import MarkdownItLinkAttributes from "markdown-it-link-attributes";
import classNames from "classnames";
import JsxParser from "react-jsx-parser";
import withTooltip from "react-toolbox/lib/tooltip";

import { withSession } from "../util/hoc";
import EntityAvatar from "../entity_avatar";

import MentionPlugin from "./mention_plugin";

import style from "./style.scss";
import tooltipTheme from "./tooltip_theme.scss";

const SpanWithToolTip = withTooltip("span");

const markdown = new MarkdownIt({
  linkify: true,
  breaks: true,
});

markdown.use(MarkdownItLinkAttributes, {
  attrs: {
    target: "_blank",
    rel: "noopener",
  },
});

markdown.use(MentionPlugin);

const MentionTooltip = ({ id, label, session }) => {
  const [user, setUser] = useState(null);
  const loadUser = () => {
    if (user === null) {
      session
        .query(
          `select first_name, last_name, thumbnail_id, thumbnail_url, id from User where id is ${id}`
        )
        .then((response) => {
          if (response.data) {
            setUser(response.data[0]);
          }
        });
    }
  };

  return (
    <SpanWithToolTip
      theme={tooltipTheme}
      tooltip={
        <div>
          <EntityAvatar
            session={session}
            size="small"
            entity={user || { name: label }}
          />
          <span className={style["tooltip-label"]}>
            {" "}
            {user ? `${user.first_name} ${user.last_name}` : label}
          </span>
        </div>
      }
      tooltipPosition="top"
    >
      <span className="mention" onMouseOver={loadUser}>
        @{label}
      </span>
    </SpanWithToolTip>
  );
};

MentionTooltip.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  session: PropTypes.object,
};

const replaceHtmlBreaks = (text) => text.replace(new RegExp("<br>", "g"), "\n");

/** Render markdown from *source*. */
const Markdown = ({ source, size, className, session }) => {
  const classes = classNames(
    style.markdown,
    {
      [style["size-small"]]: size === "small",
      [style["size-large"]]: size === "large",
    },
    className
  );

  return (
    <div className={classes}>
      <JsxParser
        bindings={{ session }}
        autoCloseVoidElements
        components={{ MentionTooltip }}
        jsx={markdown.render(replaceHtmlBreaks(source) || "")}
      />
    </div>
  );
};

Markdown.propTypes = {
  source: PropTypes.string.isRequired,
  size: PropTypes.oneOf(["small", "medium", "large"]),
  className: PropTypes.string,
  session: PropTypes.object,
};

Markdown.defaultProps = {
  size: "small",
};

export default withSession(Markdown);
export { markdown, replaceHtmlBreaks };
