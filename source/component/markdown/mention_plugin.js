import { MENTION_REGEX } from "../util/constant";

const replacer = (md) => {
  const arrayReplaceAt = md.utils.arrayReplaceAt;

  const splitTextToken = (text, level, Token) => {
    let token;
    let lastPos = 0;
    const nodes = [];

    text.replace(MENTION_REGEX, (match, p1, p2, offset) => {
      if (offset > lastPos) {
        token = new Token("text", "", 0);
        token.content = text.slice(lastPos, offset);
        nodes.push(token);
      }

      token = new Token("mention", "span", 0);
      token.attrs = [
        ["id", p2],
        ["label", p1],
      ];
      token.content = `@${p2}`;
      nodes.push(token);

      lastPos = offset + match.length;
    });

    if (lastPos < text.length) {
      token = new Token("text", "", 0);
      token.content = text.slice(lastPos);
      nodes.push(token);
    }

    return nodes;
  };

  return function mentionReplace(state) {
    let tokens;
    let token;
    let autolinkLevel = 0;
    const blockTokens = state.tokens;

    let i;
    let j;
    let l;
    for (j = 0, l = blockTokens.length; j < l; j++) {
      if (blockTokens[j].type === "inline") {
        tokens = blockTokens[j].children;

        for (i = tokens.length - 1; i >= 0; i--) {
          token = tokens[i];

          if (token.type === "link_open" || token.type === "link_close") {
            if (token.info === "auto") {
              autolinkLevel -= token.nesting;
            }
          }

          if (
            token.type === "text" &&
            autolinkLevel === 0 &&
            MENTION_REGEX.test(token.content)
          ) {
            blockTokens[j].children = tokens = arrayReplaceAt(
              tokens,
              i,
              splitTextToken(token.content, token.level, state.Token)
            );
          }
        }
      }
    }
  };
};

const renderer = (tokens, idx) => {
  const token = tokens[idx];
  return `<MentionTooltip id="${token.attrGet("id")}" label="${token.attrGet(
    "label"
  )}" session={session} />`;
};

/**
 * MentionPlugin parses and renders inline mentions of format @{text_mentioned:resource_id}
 */
const MentionPlugin = (md) => {
  md.renderer.rules.mention = renderer;
  md.core.ruler.push("mention", replacer(md));
};

export default MentionPlugin;
