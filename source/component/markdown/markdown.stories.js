// eslint-disable-next-line
import centered from "@storybook/addon-centered/react";
import { Input } from "react-toolbox";
import { withState } from "recompose";

import getSession from "../../story/get_session";

import { SparkProvider } from "../util/hoc";
import Markdown from ".";

function Provider({ story, session }) {
  return <SparkProvider session={session}>{story}</SparkProvider>;
}

export default {
  title: "Markdown",
  decorators: [
    (Story) => <Provider story={<Story />} session={getSession()} />,
    centered,
  ],
};

function ExampleMarkdownEditor({ content, setContent }) {
  return (
    <div style={{ width: "600px", display: "flex" }}>
      <div style={{ flex: "1" }}>
        <Input
          value={content}
          onChange={(value) => setContent(value)}
          multiline
        />
      </div>
      <div
        style={{
          flex: "1",
          border: "1px solid hotpink",
          marginLeft: "10px",
          padding: "20px",
          overflow: "hidden",
          scroll: "auto",
        }}
      >
        <Markdown source={content} />
      </div>
    </div>
  );
}

const StatefulMarkdownExample = withState(
  "content",
  "setContent",
  `
This is **markdown**:


* ftrack.com
* *is it not?*
* [Okay that is pretty cool](http://google.com)

`
)(ExampleMarkdownEditor);

export const MarkdownLargeSize = () => (
  <div style={{ border: "1px solid gray", padding: "24px" }}>
    <Markdown
      source="This is some example text for the large size"
      size="large"
    />
    <br />
    <Markdown source="This is some example text for the default size" />
  </div>
);

MarkdownLargeSize.storyName = "Markdown large size";

export const RenderMarkdown = () => <StatefulMarkdownExample />;

RenderMarkdown.storyName = "Render markdown";

RenderMarkdown.parameters = {
  info: "This example shows a basic Markdown component.",
};

export const MarkdownWithMentions = () => (
  <div style={{ border: "1px solid gray", padding: "24px" }}>
    <Markdown source="This is a group @{FX Animation:d86284ce-a059-11e9-82b8-d27cf242b68b} and a user @{Helena R:38b37412-a057-11e9-8346-d27cf242b68b} @{Helena Changedname:38b37412-a057-11e9-8346-d27cf242b68b}" />
  </div>
);

MarkdownWithMentions.storyName = "Markdown with mentions";
