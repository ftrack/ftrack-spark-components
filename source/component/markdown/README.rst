..
    :copyright: Copyright (c) 2018 ftrack

########
Markdown
########

A component that can be used to format markdown. E.g. to be used with the ftrack
Note content.

The Markdown components has the following dependencies:

    * markdown-it ^8.4.0

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
source                         Raw markdown input string to be formatted.
