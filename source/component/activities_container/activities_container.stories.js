import { Component } from "react";
import { action } from "@storybook/addon-actions";
// eslint-disable-next-line
import Button from "react-toolbox/lib/button";
import { SwitchInput } from "../form/field/switch";
import { Provider as ReduxProvider } from "react-redux";
import configureStore from "../../story/configure_store.js";
import getSession from "../../story/get_session";
import { withSubspace, SparkProvider } from "../util/hoc";
import ActivitiesList, {
  reducer as activitiesReducer,
  sagas as activitiesSagas,
} from ".";

function Provider({ story, store, session }) {
  return (
    <SparkProvider session={session}>
      <ReduxProvider store={store}>{story}</ReduxProvider>
    </SparkProvider>
  );
}

const projectId = "35604cac-679e-11e7-bd87-0a580ae40a16";
const assetVersionId = "aa8bd1f0-e357-11e8-8161-87a11f740139";
const assetId = "aa95e410-e357-11e8-8161-87a11f740139";
const reviewSessionId = "c0159830-64cb-11e4-b2bf-04012e2ed101";
const reviewSessionObjectId = "c098c7be-64cb-11e4-b2bf-04012e2ed101";

const store = configureStore((state = {}) => state, [], getSession());

const SizeDecorator = (storyFn) => (
  <div
    style={{
      overflow: "auto",
      position: "fixed",
      top: "0px",
      left: "0px",
      bottom: "0px",
      right: "0px",
      display: "flex",
      justifyContent: "center",
    }}
  >
    <div style={{ margin: "100px", width: "800px" }}>{storyFn()}</div>
  </div>
);

export default {
  title: "Activities container",
  decorators: [
    (Story) => (
      <Provider story={<Story />} store={store} session={getSession()} />
    ),
    SizeDecorator,
  ],
};

const ActivitiesListDefault = withSubspace(
  activitiesReducer,
  activitiesSagas,
  "activities-list-view-default"
)(ActivitiesList);

export const ActivitiesOnAProject = () => (
  <ActivitiesListDefault
    onActivityLinkClick={action("Link clicked")}
    clusterInterval={1}
    entity={{ id: projectId, type: "Project" }}
  />
);

ActivitiesOnAProject.storyName = "Activities on a project";

ActivitiesOnAProject.parameters = {
  info: "This example shows a list of activities fetched from a project.",
};

export const ActivitiesOnAReviewSession = () => (
  <ActivitiesListDefault
    onActivityLinkClick={action("Link clicked")}
    entity={{ id: reviewSessionId, type: "ReviewSession" }}
  />
);

ActivitiesOnAReviewSession.storyName = "Activities on a review session";

ActivitiesOnAReviewSession.parameters = {
  info: "This example shows a list of activities fetched from a review session.",
};

export const ActivitiesOnAReviewSessionObject = () => (
  <ActivitiesListDefault
    onActivityLinkClick={action("Link clicked")}
    entity={{ id: reviewSessionObjectId, type: "ReviewSessionObject" }}
  />
);

ActivitiesOnAReviewSessionObject.storyName =
  "Activities on a review session object";

ActivitiesOnAReviewSessionObject.parameters = {
  info: "This example shows a list of activities fetched from a review session object.",
};

export const ActivitiesOnAAssetVersion = () => (
  <ActivitiesListDefault
    onActivityLinkClick={action("Link clicked")}
    entity={{ id: assetVersionId, type: "AssetVersion" }}
  />
);

ActivitiesOnAAssetVersion.storyName = "Activities on a asset version";

ActivitiesOnAAssetVersion.parameters = {
  info: "This example shows a list of activities fetched from a asset version.",
};

export const ActivitiesOnAAsset = () => (
  <ActivitiesListDefault
    onActivityLinkClick={action("Link clicked")}
    entity={{ id: assetId, type: "Asset" }}
  />
);

ActivitiesOnAAsset.storyName = "Activities on a asset";

ActivitiesOnAAsset.parameters = {
  info: "This example shows a list of activities fetched from a asset.",
};

class ReloadableActivitiesListViewDefault extends Component {
  render() {
    return (
      <div>
        <Button
          label="Reload"
          onClick={() => {
            this.list.reload();
          }}
        />
        <ActivitiesListDefault
          onRef={(node) => {
            this.list = node;
          }}
          onActivityLinkClick={action("Link clicked")}
          clusterInterval={1}
          entity={{ id: projectId, type: "Project" }}
        />
      </div>
    );
  }
}

export const ActivitiesReloadReload = () => (
  <ReloadableActivitiesListViewDefault />
);

ActivitiesReloadReload.storyName = "Activities reload reload";

ActivitiesReloadReload.parameters = {
  info: "This example shows a list of notes that can be reloaded.",
};

export const ActivitiesOnAProjectNoEvents = () => (
  <ActivitiesListDefault
    onActivityLinkClick={action("Link clicked")}
    clusterInterval={1}
    entity={{ id: "foo", type: "Project" }}
  />
);

ActivitiesOnAProjectNoEvents.storyName = "Activities on a project (no events)";

ActivitiesOnAProjectNoEvents.parameters = {
  info: "This example shows an empty activities list fetched from a project.",
};

const activityTypesOnlyNotes = {
  notes: true,
};

// eslint-disable-next-line react/no-multi-comp
class NotesOnlyActivitiesList extends Component {
  constructor() {
    super();

    this.state = {
      clicked: false,
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange() {
    this.setState({ clicked: !this.state.clicked });
  }

  render() {
    return (
      <div>
        <SwitchInput
          onChange={this.handleChange}
          value={this.state.clicked}
          label="Show feedback only"
        />
        <ActivitiesListDefault
          onActivityLinkClick={action("Link clicked")}
          clusterInterval={1}
          entity={{ id: projectId, type: "Project" }}
          activityTypes={
            this.state.clicked ? activityTypesOnlyNotes : undefined
          }
        />
      </div>
    );
  }
}

export const ActivitiesOnNotesOnly = () => <NotesOnlyActivitiesList />;

ActivitiesOnNotesOnly.storyName = "Activities on notes only";
