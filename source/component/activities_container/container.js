// :copyright: Copyright (c) 2016 ftrack
import { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import loglevel from "loglevel";

import { Waypoint } from "react-waypoint";
import { FormattedRelative, FormattedDate } from "react-intl";

import { call, put, getContext } from "redux-saga/effects";
import { takeEvery } from "redux-saga";
import { operation } from "ftrack-javascript-api";
import moment from "moment";

import style from "./style.scss";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import { ENCODE_DATETIME_FORMAT } from "../util/constant";
import components from "../activity";
import ActivitySkeleton from "../activity/skeleton";

const logger = loglevel.getLogger("saga:activities");

const ACTIVITIES_LOAD = "ACTIVITIES_LOAD";
const ACTIVITIES_LOAD_NEXT_PAGE = "ACTIVITIES_LOAD_NEXT_PAGE";
const ACTIVITIES_LOADED = "ACTIVITIES_LOADED";

export const actions = {
  ACTIVITIES_LOAD,
  ACTIVITIES_LOAD_NEXT_PAGE,
  ACTIVITIES_LOADED,
};

const ALL_ACTIVITY_TYPES = {
  createdAssetVersion: true,
  createdReviewSessionObjects: true,
  createdReviewSession: true,
  collaboratorSetStatus: true,
  createdProject: true,
  userSharedVersionInReviewSession: true,
  renamedAsset: true,
  changeAsset: true,
  shareAssetVersion: true,
  notes: true,
};

/** Load activities action creator. */
export function activitiesLoad(id, type, clusterInterval, activityTypes) {
  return {
    type: ACTIVITIES_LOAD,
    payload: {
      entity: {
        id,
        type,
      },
      clusterInterval,
      activityTypes,
    },
  };
}

/** Load next page of activities action creator. */
export function activitiesLoadNextPage(
  id,
  type,
  nextOffset,
  clusterInterval,
  activityTypes
) {
  return {
    type: ACTIVITIES_LOAD_NEXT_PAGE,
    payload: {
      entity: {
        id,
        type,
      },
      nextOffset,
      clusterInterval,
      activityTypes,
    },
  };
}

/** Activities loaded action creator. */
export function activitiesLoaded(
  entity,
  items,
  nextOffset,
  firstPage,
  activityTypes
) {
  return {
    type: ACTIVITIES_LOADED,
    payload: {
      entity,
      items,
      nextOffset,
      firstPage,
      activityTypes,
    },
  };
}

/**
 * Reduce state for application-wide activities.
 */
export function reducer(state = {}, action) {
  let nextState = state;

  if (action.type === actions.ACTIVITIES_LOAD) {
    nextState = Object.assign({}, state, {
      entity: action.payload.entity,
      items: [],
      nextOffset: null,
      loading: true,
    });
  }
  if (action.type === actions.ACTIVITIES_LOAD_NEXT_PAGE) {
    nextState = Object.assign({}, state, {
      loading: true,
    });
  } else if (action.type === actions.ACTIVITIES_LOADED) {
    let items = [];

    if (action.payload.firstPage) {
      // Overwrite existing items.
      items = [...action.payload.items];
    } else {
      items = [...state.items, ...action.payload.items];
    }

    nextState = Object.assign({}, state, {
      items,
      nextOffset: action.payload.nextOffset,
      loading: false,
    });
  }

  return nextState;
}

/** Return list of activities attributes to use in select. */
function eventSelect() {
  const select = [
    "id",
    "data",
    "user_id",
    "created_at",
    "action",
    "insert",
    "parent_id",
    "parent_type",
  ];
  return `select ${select.join(", ")} from Event`;
}

const sortItems = (a, b) => {
  if (a.created_at.isSame(b.created_at)) {
    if (a.id < b.id) {
      return -1;
    }

    if (a.id > b.id) {
      return 1;
    }

    return 0;
  }

  if (a.created_at.isBefore(b.created_at.toDate())) {
    return 1;
  }

  return -1;
};

/** Return activity data from activity *item*.
 *
 * This function will process a raw input event *item* and return an object
 * containing `clusterIdentifier`, `type`, `lookup` and `data`.
 *
 * `type` is the determined type of activity, this maps to a React JSX class
 * to return such activity items (see components/actitivy).
 *
 * `clusterIdentifier` is a string that can be used to cluster together events.
 * An exampel is multiple asset versions created by a user: cluster_add_asset_version|<user-id>
 *
 * `lookup` is an array of additional data that must be looked up before the acitvity
 * information is complete.
 *
 * `data` is the complete data of the activity, intended to be used as props into
 * a React JSX activity item.
 *
 */
function processItem(item, inputData) {
  const parentId = item.parent_id;
  const rawEventData = JSON.parse(item.data);
  const lookup = [];
  const data = Object.assign({}, inputData);
  let type;
  let clusterIdentifier;
  if (
    item.action === "asset.published" &&
    rawEventData.assetid &&
    rawEventData.versionid
  ) {
    type = "asset_version";
    data.asset_id = rawEventData.assetid.new;
    data.asset_version_id = rawEventData.versionid.new;
    data.user_id = item.user_id;
    clusterIdentifier = `cluster_add_asset_version|${item.user_id}`;
    lookup.push({
      query: `select asset.name, version, thumbnail_url, thumbnail_id, id, asset.parent.name
            from AssetVersion where id is "${data.asset_version_id}"`,
      callback: ([{ asset, version, thumbnail_url, thumbnail_id, id }]) => ({
        assetVersion: {
          name: asset.name,
          version,
          thumbnail_url,
          thumbnail_id,
          id,
          parent:
            asset.parent.__entity_type__ !== "Project" ? asset.parent.name : "",
          type: "AssetVersion",
        },
      }),
    });
  }

  if (item.action === "db.all.review_session_object") {
    type = "review_session_object";
    clusterIdentifier = `cluster_manage_review_session|${rawEventData.review_session_id.new}-${item.user_id}`;
    lookup.push({
      query: `select name, review_session.name, review_session.id, asset_version.thumbnail_url, asset_version.thumbnail_id, 
                asset_version.version, asset_version.asset.parent.name from ReviewSessionObject where id is "${parentId}"`,
      callback: ([{ id, name, asset_version, review_session }]) => ({
        reviewSession: {
          name: review_session.name,
          id: review_session.id,
          type: "ReviewSession",
        },
        reviewSessionObject: {
          name,
          id,
          type: "ReviewSessionObject",
          thumbnail_url: asset_version.thumbnail_url,
          thumbnail_id: asset_version.thumbnail_id,
          version: asset_version.version,
          parent:
            asset_version.asset.parent.__entity_type__ !== "Project"
              ? asset_version.asset.parent.name
              : "",
        },
      }),
    });
  }

  if (item.action === "db.all.review_session") {
    type = "review_session";
    data.reviewSession = {
      name: rawEventData.name.new,
      id: parentId,
      type: "ReviewSession",
    };
    clusterIdentifier = `cluster_manage_review_session|${parentId}-${item.user_id}`;
  }

  if (item.action === "db.all.review_session_object_status") {
    type = "object_status";
    data.review_session_object_status_id = parentId;
    data.status = rawEventData.status.new;
    lookup.push({
      query: `select review_session_object.name, resource.first_name, resource.last_name, review_session_object.asset_version.version
            from ReviewSessionObjectStatus where id is "${parentId}"`,
      callback: ([{ review_session_object, resource }]) => ({
        reviewSessionObject: Object.assign(
          { type: "ReviewSessionObject" },
          review_session_object
        ),
        user: resource,
        version: review_session_object.asset_version.version,
      }),
    });
  }

  if (
    item.action === "db.all.note" &&
    rawEventData.parent_type.new === "review_session_object"
  ) {
    type = "note";
    data.noteText = rawEventData.text.new;
    lookup.push({
      query: `select name, id, asset_version.version, review_session, review_session.name
            from ReviewSessionObject where id is "${rawEventData.parent_id.new}"`,
      callback: ([{ name, id, asset_version, review_session }]) => ({
        entity: {
          name,
          id,
          version: asset_version.version,
          review_session,
          type: "ReviewSessionObject",
        },
      }),
    });
    // Review session notes can be written by collaborators, which are not
    // Stored as Event.user_id, so need to be fetched explicitly.
    lookup.push({
      query: `select id, first_name, last_name from BaseUser where id in (select user_id from Note where id is "${parentId}")`,
      callback: ([baseUser]) => ({ user: baseUser || data.user }),
    });
  }

  if (
    item.action === "db.all.note" &&
    rawEventData.parent_type.new === "task"
  ) {
    type = "note";
    data.noteText = rawEventData.text.new;
    lookup.push({
      query: `select name, id from Context where id is "${rawEventData.parent_id.new}"`,
      callback: ([{ name, id }]) => ({ entity: { name, id } }),
    });
  }

  if (
    item.action === "db.all.note" &&
    rawEventData.parent_type.new === "show"
  ) {
    type = "note";
    data.noteText = rawEventData.text.new;
    lookup.push({
      query: `select full_name, id from Project where id is "${rawEventData.parent_id.new}"`,
      callback: ([{ full_name, id }]) => ({
        entity: { name: full_name, id },
      }),
    });
  }

  if (
    item.action === "db.all.note" &&
    rawEventData.parent_type.new === "asset_version"
  ) {
    type = "note";
    data.noteText = rawEventData.text.new;
    lookup.push({
      query: `select asset.name, id, version
            from AssetVersion where id is "${rawEventData.parent_id.new}"`,
      callback: ([{ asset, id, version }]) => ({
        entity: { name: asset.name, id, version, type: "AssetVersion" },
      }),
    });
  }
  if (item.action === "db.all.show") {
    type = "project";
    lookup.push({
      query: `select full_name, id from Project where id is "${parentId}"`,
      callback: ([{ full_name, id }]) => ({
        project: { name: full_name, type: "Project", id },
      }),
    });
  }
  if (item.action === "db.all.asset") {
    type = "asset";
    data.isRename = !!rawEventData.name;
    data.isMove = !!rawEventData.context_id;
    lookup.push({
      query: `select name, id from Asset where id is "${parentId}"`,
      callback: ([{ name, id }]) => ({
        asset: {
          name,
          id,
          type: "Asset",
        },
      }),
    });
  }

  if (!type) {
    type = "unknown";
    Object.assign(data, item);
  }

  return {
    type,
    data,
    lookup,
    clusterIdentifier,
  };
}

/** Handle load activities and load next page *action*. */
function* loadActivitiesHandler(action) {
  let offset = false;
  const limit = 25;

  if (action.type === actions.ACTIVITIES_LOAD_NEXT_PAGE) {
    offset = action.payload.nextOffset;
  }

  const session = yield getContext("ftrackSession");
  const { entity, clusterInterval } = action.payload;
  const activityTypes = action.payload.activityTypes;
  const filters = [];
  const clusterFeed = !!clusterInterval;

  if (entity.type === "Project") {
    // The following activity items are generated for a project:
    //
    // * User created asset version. (aggregate if multiple)
    // * User created a review session object.
    // * User created Review session. (aggregate shared assets)
    // * User wrote a note.
    // * Collaborator wrote a note.
    // * Collaborator set a review session object status.
    // * User created the project. (this will be the initial event)

    if (activityTypes.createdReviewSession) {
      filters.push(
        `action is "db.all.review_session" and
                insert is "insert" and
                parent_id in (
                    select id from ReviewSession where project.id is "${entity.id}"
                )`
      );
    }
    if (activityTypes.createdReviewSessionObjects) {
      filters.push(
        `action is "db.all.review_session_object" and
                insert is "insert" and
                parent_id in (
                    select id from ReviewSessionObject where review_session.project.id is "${entity.id}"
                )`
      );
    }
    if (activityTypes.collaboratorSetStatus) {
      filters.push(
        `action is "db.all.review_session_object_status" and
                parent_id in (
                    select id from ReviewSessionObjectStatus where
                    review_session_object.review_session.project.id is "${entity.id}"
                )`
      );
    }
    if (activityTypes.createdAssetVersion) {
      filters.push(
        `action is "asset.published" and project_id is "${entity.id}" and insert is "insert"`
      );
    }
    if (activityTypes.notes) {
      filters.push(
        `action is "db.all.note" and project_id is "${entity.id}" and insert is "insert"`
      );
    }
    if (activityTypes.createdProject) {
      filters.push(
        `action is "db.all.show" and project_id is "${entity.id}" and insert is "insert"`
      );
    }
  } else if (entity.type === "ReviewSession") {
    // The following activity items are generated for a review session:
    //
    // * Collaborator wrote a note. (review session object or asset version)
    // * User wrote a note. (review session object or asset version)
    // * Collaborator set a review session object status.
    // * User shared a version in a review session.
    // * User created a review session. (this will be the initial event)

    // TODO:
    // Review session invitee changed.
    const reviewSessionObjects = yield session.query(
      `select id, version_id from ReviewSessionObject where review_session_id is "${entity.id}"`
    );
    const ids = reviewSessionObjects.data.reduce((accumulator, item) => {
      accumulator.push(item.id, item.version_id);
      return accumulator;
    }, []);
    if (ids.length && activityTypes.notes) {
      filters.push(
        `action is "db.all.note" and
                insert is "insert" and
                parent_id in (select id from Note where parent_id in (${ids.join(
                  ","
                )}))`
      );
    }
    if (activityTypes.collaboratorSetStatus) {
      filters.push(
        `action is "db.all.review_session_object_status" and
                parent_id in (
                    select id from ReviewSessionObjectStatus where
                    review_session_object.review_session_id is "${entity.id}"
                )`
      );
    }
    if (activityTypes.shareAssetVersion) {
      filters.push(
        `action is "db.all.review_session_object" and
                insert is "insert" and parent_id in (
                    select id from ReviewSessionObject where review_session_id is "${entity.id}"
                )`
      );
    }
    if (activityTypes.createdReviewSession) {
      filters.push(
        `action is "db.all.review_session" and insert is "insert" and parent_id is "${entity.id}"`
      );
    }
  } else if (entity.type === "ReviewSessionObject") {
    // The following activity items are generated for a review session object:
    //
    // * Collaborator wrote a note. (review session object or asset version)
    // * User wrote a note. (review session object or asset version)
    // * Collaborator set a review session object status.
    // * User shared a version in a review session (this will be the initial event)

    if (activityTypes.shareAssetVersion) {
      filters.push(
        `action is "db.all.review_session_object" and
                insert is "insert" and parent_id is "${entity.id}"`
      );
    }
    const {
      data: [{ version_id: assetVersionId }],
    } = yield session.query(
      `select version_id from ReviewSessionObject where id is "${entity.id}"`
    );

    if (activityTypes.notes) {
      filters.push(
        `action is "db.all.note" and
                insert is "insert" and
                parent_id in (
                    select id from Note where parent_id in ("${entity.id}", "${assetVersionId}")
                )`
      );
    }
    if (activityTypes.collaboratorSetStatus) {
      filters.push(
        `action is "db.all.review_session_object_status" and
                parent_id in (
                    select id from ReviewSessionObjectStatus where
                    review_session_object.id is "${entity.id}"
                )`
      );
    }
  } else if (entity.type === "AssetVersion") {
    // The following activity items are generated for an asset version:
    //
    // x User renamed asset
    // x User moved the asset to another parent
    // x User created a new asset version
    // x User shared an asset version in a review session
    // x Collaborator wrote a note (review session object or asset version)
    // x User wrote a note (review session object or asset version)
    // x Collaborator set a review session object status.

    // TODO:
    // User deleted asset version.
    // User changed asset_id on an asset version.

    const { data: reviewSessionObjects } = yield session.query(
      `select id from ReviewSessionObject where version_id is "${entity.id}"`
    );
    const ids = [entity.id, ...reviewSessionObjects.map((item) => item.id)];
    if (activityTypes.createdAssetVersion) {
      filters.push(
        `action is "asset.published" and parent_id is "${entity.id}" and insert is "insert"`
      );
    }
    if (activityTypes.shareAssetVersion) {
      filters.push(
        `action is "db.all.review_session_object" and
                insert is "insert" and
                parent_id in (select id from ReviewSessionObject where version_id is "${entity.id}")`
      );
    }
    if (activityTypes.notes) {
      filters.push(
        `action is "db.all.note" and insert is "insert" and
                parent_id in (
                    select id from Note where parent_id in (${ids.join(", ")})
                )`
      );
    }
    if (activityTypes.collaboratorSetStatus) {
      filters.push(
        `action is "db.all.review_session_object_status" and
                parent_id in (
                    select id from ReviewSessionObjectStatus where
                    review_session_object.version_id is "${entity.id}"
                )`
      );
    }
    if (activityTypes.changeAsset) {
      filters.push(
        `action is "db.all.asset" and
                insert is "update" and
                parent_id in (
                    select asset_id from AssetVersion where id is "${entity.id}"
                )`
      );
    }
  } else if (entity.type === "Asset") {
    // The following activity items are generated for an asset:
    //
    // * User renamed asset.
    // * User moved the asset to another parent.
    // x User created a new asset version.
    // x User shared an asset version in a review session.
    // x Collaborator wrote a note. (review session object or asset version)
    // x User wrote a note. (review session object or asset version)
    // x Collaborator set a review session object status.

    // TODO:
    // User deleted asset version.
    // User changed asset_id on an asset version.

    const { data: reviewSessionObjects } = yield session.query(
      `select id from ReviewSessionObject where asset_version.asset_id is "${entity.id}"`
    );
    const reviewSessionObjectIds = reviewSessionObjects.map((item) => item.id);
    const { data: assetVersions } = yield session.query(
      `select id from AssetVersion where asset_id is "${entity.id}"`
    );
    const assetVersionIds = assetVersions.map((item) => item.id);
    if (activityTypes.createdAssetVersion) {
      filters.push(
        `action is "asset.published" and insert is "insert" and parent_id in (
                    select id from AssetVersion where asset_id is "${entity.id}"
                )`
      );
    }
    if (activityTypes.shareAssetVersion) {
      filters.push(
        `action is "db.all.review_session_object" and insert is "insert" and
                parent_id in (
                    select id from ReviewSessionObject where asset_version.asset_id is "${entity.id}"
                )`
      );
    }
    if (reviewSessionObjectIds.length && activityTypes.notes) {
      filters.push(
        `action is "db.all.note" and insert is "insert" and
                parent_id in (
                    select id from Note where parent_id in (${reviewSessionObjectIds.join(
                      ", "
                    )})
                )`
      );
    }
    if (assetVersionIds.length && activityTypes.notes) {
      filters.push(
        `action is "db.all.note" and insert is "insert" and
                parent_id in (
                    select id from Note where parent_id in (${assetVersionIds.join(
                      ", "
                    )})
                )`
      );
    }
    if (activityTypes.collaboratorSetStatus) {
      filters.push(
        `action is "db.all.review_session_object_status" and
                parent_id in (
                    select id from ReviewSessionObjectStatus where
                    review_session_object.asset_version.asset_id is "${entity.id}"
                )`
      );
    }
    if (activityTypes.changeAsset) {
      filters.push(
        `action is "db.all.asset" and insert is "update" and parent_id is "${entity.id}"`
      );
    }
  } else {
    throw new Error(`Activities cannot be fetched for type: ${entity.type}.`);
  }

  let pageFilter = "";
  if (offset) {
    pageFilter =
      ` and (created_at < "${offset.date.format(ENCODE_DATETIME_FORMAT)}" or ` +
      `(created_at = "${offset.date.format(
        ENCODE_DATETIME_FORMAT
      )}" and id >  "${offset.id}"))`;
  }

  const queries = filters.map(
    (filter) =>
      `${eventSelect()} where ` +
      `(${filter}) ${pageFilter} ` +
      `order by created_at desc, id limit ${limit}`
  );

  logger.debug('Loading activities with "', queries, '" from action', action);
  const response = yield call(
    [session, session.call],
    queries.map((query) => operation.query(query))
  );

  logger.debug("Activities query result: ", response);

  let items = response.reduce(
    (accumulator, result) => accumulator.concat(result.data),
    []
  );

  items.sort(sortItems);
  items = items.slice(0, limit);

  const userIds = items.reduce((accumulator, item) => {
    if (item.user_id) {
      accumulator.push(`"${item.user_id}"`);
    }
    return accumulator;
  }, []);

  let users = [];
  if (userIds.length) {
    const usersResponse = yield session.query(
      `select first_name, last_name, thumbnail_id, thumbnail_url, id from User where id in (${userIds})`
    );
    users = usersResponse.data;
  }
  const processedItems = items.reduce((accumulator, item) => {
    const data = {
      user: users.find((user) => user.id === item.user_id),
      createdAt: moment(item.created_at).local(),
    };

    try {
      accumulator.push(processItem(item, data));
    } catch (e) {
      logger.error("Could not process item", item, data);
      accumulator.push({
        type: "error",
        data,
        lookup: [],
      });
    }
    return accumulator;
  }, []);

  const lookupCallbacks = processedItems.reduce(
    (accumulator, processedItem) => {
      accumulator.push(
        ...processedItem.lookup.map((lookupChunk) =>
          Object.assign({}, lookupChunk, { item: processedItem })
        )
      );

      return accumulator;
    },
    []
  );

  logger.debug(
    "Running addtional queries:",
    lookupCallbacks.map((item) => item.query)
  );
  const responseQueryResponses = yield call(
    [session, session.call],
    lookupCallbacks.map((item) => operation.query(item.query))
  );
  logger.debug("Activities additional query result:", responseQueryResponses);

  responseQueryResponses.forEach((responseItem, index) => {
    let callbackResult = {};
    try {
      callbackResult = lookupCallbacks[index].callback(responseItem.data);
    } catch (e) {
      logger.error(
        "Could not process item to extract callback result",
        responseItem
      );
      lookupCallbacks[index].item.type = "error";
    }

    // Mutate the data on activity item.
    Object.assign(lookupCallbacks[index].item.data, callbackResult);
  });

  const storyItems =
    clusterFeed === false
      ? processedItems
      : processedItems.reduce((accumulator, item, index) => {
          // Process items again and create clusters of events. If two events with the
          // same clusterIdentifier has occured within the time limit they will be merged
          // together.

          let previous = index > 0 ? accumulator[accumulator.length - 1] : null;
          if (previous === null) {
            // First item to be processed so no cluster.
            accumulator.push(item);
          } else if (
            previous.data.createdAt.diff(item.data.createdAt, "s") >
            clusterInterval
          ) {
            // Previous item happend outside of the cluster time limit.
            accumulator.push(item);
          } else if (
            item.clusterIdentifier &&
            item.clusterIdentifier === previous.clusterIdentifier
          ) {
            // A cluster has beem found and current item should be merged
            // into the previous.
            const [clusterType] = item.clusterIdentifier.split("|");
            if (previous.type !== clusterType) {
              // Previous item was not a clustered item and must be
              // overwritten.
              previous = accumulator[accumulator.length - 1] = {
                type: clusterType,
                clusterIdentifier: previous.clusterIdentifier,
                data: {
                  createdAt: previous.data.createdAt,
                  user: previous.data.user,
                  // Add it-self as a first item in items.
                  items: [previous],
                },
              };
            }
            previous.data.items.push(item);
          } else {
            accumulator.push(item);
          }
          return accumulator;
        }, []);

  let nextOffset = null;
  if (items.length && items.length >= limit) {
    const lastItem = items[items.length - 1];
    nextOffset = { date: lastItem.created_at, id: lastItem.id };
  }

  yield put(
    activitiesLoaded(
      {
        id: action.payload.entity.id,
        type: action.payload.entity.type,
      },
      storyItems,
      nextOffset,
      // Indicate if this is the first page or not.
      offset === false,
      activityTypes
    )
  );
}

/** Handle ACTIVITIES_LOAD action. */
export function* activitiesLoadSaga() {
  yield takeEvery(actions.ACTIVITIES_LOAD, loadActivitiesHandler);
}

/** Handle ACTIVITIES_LOAD action. */
export function* activitiesLoadNextPageSaga() {
  yield takeEvery(actions.ACTIVITIES_LOAD_NEXT_PAGE, loadActivitiesHandler);
}

export const sagas = [activitiesLoadSaga, activitiesLoadNextPageSaga];

// TODO: Render using Heading component for correct style
function Header({ children }) {
  return <div className={style.header}>{children}</div>;
}
Header.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

class ActivitiesList extends Component {
  componentDidMount() {
    const { entity, onLoad, onRef, loading, activityTypes } = this.props;

    if (!loading) {
      // Make sure that we only load if not already in loading state.
      onLoad(entity, activityTypes);
    }

    if (onRef) {
      onRef(this);
    }
  }

  componentDidUpdate(prevProps) {
    const { onLoad, entity, activityTypes } = this.props;
    const entityId = entity && entity.id;
    const prevPropsEntityId =
      prevProps && prevProps.entity && prevProps.entity.id;
    const prevActivityTypes = prevProps.activityTypes;
    if (
      activityTypes !== prevActivityTypes ||
      (entityId && entityId !== prevPropsEntityId)
    ) {
      onLoad(entity, activityTypes);
    }
  }

  reload() {
    const { entity } = this.props;
    this.props.onLoad(entity);
  }

  render() {
    const {
      items,
      entity,
      loading,
      nextOffset,
      onFetchMore,
      onActivityLinkClick,
      small,
      activityTypes,
    } = this.props;
    logger.debug("Rendering activities", entity, items);
    const yesterday = moment().startOf("day").subtract(1, "days");

    const activities = items.reduce((accumulator, item, index, all) => {
      const StoryComponent = components[item.type];
      const { createdAt } = item.data;
      const isHeader =
        // Always show header on first item.
        index === 0 ||
        // Or show header if item is on a different date.
        !moment(createdAt).isSame(all[index - 1].data.createdAt, "d");
      if (isHeader) {
        if (moment(createdAt).isAfter(yesterday)) {
          // Yesterday or today.
          accumulator.push(
            <Header key={`header-${index}`}>
              <FormattedRelative units="day" value={createdAt.toDate()} />
            </Header>
          );
        } else {
          // Earlier, show full date.
          accumulator.push(
            <Header key={`header-${index}`}>
              <FormattedDate
                value={createdAt.toDate()}
                month="long"
                day="2-digit"
              />
            </Header>
          );
        }
      }

      let component;
      if (StoryComponent) {
        component = (
          <StoryComponent
            small={small}
            {...item.data}
            onClick={onActivityLinkClick}
          />
        );
      } else {
        component = (
          <div>
            <p>{item.type}</p>
            <p>{JSON.stringify(item)}</p>
          </div>
        );
      }

      accumulator.push(
        <div
          key={`component-${index}`}
          className={style["activity-item-wrapper"]}
        >
          {component}
        </div>
      );

      return accumulator;
    }, []);

    const content = [];

    content.push(<div key="activities-list-inner">{activities}</div>);

    if (loading) {
      // Add loading indicator.
      content.push(<ActivitySkeleton key={"activity-skeleton"} />);
    }

    if (loading === false && nextOffset !== null && items.length) {
      // Only add way point if not loading and there are more items to load.
      activities.push(
        <Waypoint
          key="activities-list-waypoint"
          onEnter={() => onFetchMore(entity, nextOffset, activityTypes)}
          bottomOffset={-100}
        />
      );
    }

    return <div>{content}</div>;
  }
}

ActivitiesList.propTypes = {
  items: PropTypes.array.isRequired, // eslint-disable-line
  entity: PropTypes.shape({
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
  }).isRequired,
  loading: PropTypes.bool,
  nextOffset: PropTypes.shape({
    date: PropTypes.object.isRequired, // eslint-disable-line
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  }),
  onFetchMore: PropTypes.func,
  onLoad: PropTypes.func,
  onRef: PropTypes.func,
  onActivityLinkClick: PropTypes.func.isRequired,
  clusterInterval: PropTypes.number, // eslint-disable-line
  small: PropTypes.bool,
  // eslint-disable-next-line react/forbid-prop-types
  activityTypes: PropTypes.object,
};

ActivitiesList.defaultProps = {
  activityTypes: ALL_ACTIVITY_TYPES,
};

const mapStateToProps = (state) => {
  const items = (state && state.items) || [];
  const loading = (state && state.loading) || false;
  const nextOffset = state && state.nextOffset;

  return {
    items,
    nextOffset,
    loading,
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  onFetchMore: (entity, nextOffset, activityTypes) => {
    dispatch(
      activitiesLoadNextPage(
        entity.id,
        entity.type,
        nextOffset,
        props.clusterInterval,
        activityTypes
      )
    );
  },
  onLoad: (entity, activityTypes) => {
    dispatch(
      activitiesLoad(
        entity.id,
        entity.type,
        props.clusterInterval,
        activityTypes
      )
    );
  },
});

export default safeInjectIntl(
  connect(mapStateToProps, mapDispatchToProps)(ActivitiesList)
);
