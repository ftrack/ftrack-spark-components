// :copyright: Copyright (c) 2020 ftrack

import PropTypes from "prop-types";
import classNames from "classnames";
import ProgressBar from "react-toolbox/lib/progress_bar";
import FontIcon from "react-toolbox/lib/font_icon";

import style from "./style.scss";
import spinnerTheme from "./spinner_theme.scss";

function FileTypeIcon({ className, variant, label = "", loading }) {
  const longLabel = label.length > 5;

  // Adjust font-size depending on label length
  const labelClasses = classNames({
    [style["label-tiny"]]: variant === "tiny",
    [style["label-tiny-long-text"]]: variant === "tiny" && longLabel,
    [style["label-small"]]: variant === "small",
    [style["label-small-long-text"]]: variant === "small" && longLabel,
    [style["label-medium"]]: variant === "medium",
    [style["label-medium-long-text"]]: variant === "medium" && longLabel,
    [style["label-large"]]: variant === "large",
    [style["label-large-long-text"]]: variant === "large" && longLabel,
    [style["label-playlist"]]: variant === "playlist",
    [style["label-playlist-long-text"]]: variant === "playlist" && longLabel,
  });

  const newLabel = label ? label.replace(".", "") : "";
  const content = loading ? (
    <ProgressBar
      type="circular"
      mode="indeterminate"
      className={style[`spinner-${variant}`]}
      theme={spinnerTheme}
    />
  ) : (
    <p className={labelClasses}>
      {longLabel ? `${newLabel.substr(0, 5)}...` : newLabel}
    </p>
  );

  return (
    <div className={classNames(style.iconWrapper, className)}>
      <FontIcon
        value="insert_drive_file"
        variant={variant}
        className={style[`icon-${variant}`]}
      />
      {content}
    </div>
  );
}

FileTypeIcon.propTypes = {
  variant: PropTypes.string,
  label: PropTypes.string,
  className: PropTypes.string,
  loading: PropTypes.bool,
};

export default FileTypeIcon;
