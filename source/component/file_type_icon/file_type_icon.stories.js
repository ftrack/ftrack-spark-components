// :copyright: Copyright (c) 2020 ftrack

import centered from "@storybook/addon-centered/react";

import FileTypeIcon from ".";

export default {
  title: "FileTypeIcon",
  decorators: [centered],
};

export const IconLarge = () => (
  <div style={{ display: "flex", width: "500px", height: "600px" }}>
    <FileTypeIcon variant="large" label=".jpeg" />
    <FileTypeIcon variant="large" label="!" />
    <FileTypeIcon variant="large" label=".CONFIGFILE" />
    <FileTypeIcon variant="large" label=".CONFIGFILE" loading />
  </div>
);

IconLarge.storyName = "Icon, large";

export const IconMedium = () => (
  <div style={{ display: "flex", width: "300px", height: "600px" }}>
    <FileTypeIcon variant="medium" label=".pdf" />
    <FileTypeIcon variant="medium" label="!" />
    <FileTypeIcon variant="medium" label=".CONFIGFILE" />
    <FileTypeIcon variant="medium" label=".CONFIGFILE" loading />
  </div>
);

IconMedium.storyName = "Icon, medium";

export const IconSmall = () => (
  <div style={{ display: "flex", width: "200px", height: "600px" }}>
    <FileTypeIcon variant="small" label=".mp4" />
    <FileTypeIcon variant="small" label="!" />
    <FileTypeIcon variant="small" label=".CONFIGFILE" />
    <FileTypeIcon variant="small" label=".CONFIGFILE" loading />
  </div>
);

IconSmall.storyName = "Icon, small";

export const IconPlaylist = () => (
  <div style={{ display: "flex", width: "200px", height: "600px" }}>
    <FileTypeIcon variant="playlist" label=".mp4" />
    <FileTypeIcon variant="playlist" label="!" />
    <FileTypeIcon variant="playlist" label=".CONFIGFILE" />
    <FileTypeIcon variant="playlist" label=".CONFIGFILE" loading />
  </div>
);

IconPlaylist.storyName = "Icon, playlist";
