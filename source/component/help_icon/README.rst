..
    :copyright: Copyright (c) 2017 ftrack

########
HelpIcon
########

Displays a help icon with information shown in a tooltip on hover.

Used via the "ui:help" uiSchema configuration in Form components. See
create_project_dialog/form.js for example usage.


