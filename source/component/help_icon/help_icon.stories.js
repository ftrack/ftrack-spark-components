// :copyright: Copyright (c) 2017 ftrack
import centered from "@storybook/addon-centered/react";

import HelpIcon from ".";

export default {
  title: "Help icon",
  decorators: [centered],
};

export const Default = () => <HelpIcon text="Help description goes here" />;

export const Customization = () => (
  <HelpIcon
    icon="info_outline"
    style={{ color: "red", fontSize: "50px" }}
    tooltipPosition="bottom"
    text="Example customization"
  />
);
