// :copyright: Copyright (c) 2017 ftrack
import PropTypes from "prop-types";

import FontIcon from "react-toolbox/lib/font_icon";
import Tooltip from "react-toolbox/lib/tooltip";
import classNames from "classnames";

import style from "./style.scss";
import theme from "./theme.scss";

const TooltipFontIcon = Tooltip(FontIcon);

/**
 * HelpIcon component.
 */
function HelpIcon(props) {
  const { tooltipPosition, className, text, icon, ...otherProps } = props;
  const iconClassNames = classNames(style.icon, className);
  return (
    <TooltipFontIcon
      tooltipPosition={tooltipPosition}
      theme={theme}
      className={iconClassNames}
      tooltip={text}
      value={icon}
      {...otherProps}
    />
  );
}

HelpIcon.propTypes = {
  className: PropTypes.string,
  text: PropTypes.string,
  icon: PropTypes.string,
  tooltipPosition: PropTypes.string,
};

HelpIcon.defaultProps = {
  className: "",
  text: null,
  icon: "help_outline",
  tooltipPosition: "top",
};

export default HelpIcon;
