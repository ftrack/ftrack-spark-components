..
    :copyright: Copyright (c) 2019 ftrack

##########
IconButton
##########

Wraps the icon button component from react toolbox to tweak styling and
provide additional features.

Variants:

    * default
    * large-icon-negative
    * large-icon-positive

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
tooltip                        Display a tooltip on hover.
variant            default     One of the variants above.
active                         Display as `toggled`.
icon                           Material design icon name.
