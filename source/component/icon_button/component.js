// :copyright: Copyright (c) 2019 ftrack
import PropTypes from "prop-types";
import { IconButton as RTIconButton } from "react-toolbox/lib/button";
import withTooltip from "react-toolbox/lib/tooltip";
import classNames from "classnames";

import largePositiveTheme from "./large_positive_theme.scss";
import largeNegativeTheme from "./large_negative_theme.scss";
import defaultTheme from "./default_theme.scss";

const RTIconButtonWithTooltip = withTooltip(RTIconButton);

const THEMES = {
  default: defaultTheme,
  "large-icon-positive": largePositiveTheme,
  "large-icon-negative": largeNegativeTheme,
};

function IconButton({ variant, active, className, ...props }) {
  const Element = props.tooltip ? RTIconButtonWithTooltip : RTIconButton;
  const theme = THEMES[variant] || defaultTheme;
  const classes = classNames(className, {
    [theme.active]: active,
  });

  return <Element theme={theme} className={classes} {...props} />;
}

IconButton.propTypes = {
  tooltip: PropTypes.node,
  active: PropTypes.bool,
  variant: PropTypes.oneOf([
    "default",
    "large-icon-positive",
    "large-icon-negative",
  ]),
  className: PropTypes.string,
};

IconButton.defaultProps = {
  color: "default",
};

export default IconButton;
