// :copyright: Copyright (c) 2018 ftrack

import centered from "@storybook/addon-centered/react";

import IconButton from ".";

export default {
  title: "Icon button",
  decorators: [centered],
};

export const Default = () => (
  <IconButton variant="default" icon="info_outline" />
);

export const WithTooltip = () => (
  <IconButton variant="default" tooltip="Tooltip" icon="info_outline" />
);

WithTooltip.storyName = "With tooltip";

export const Positive = () => (
  <IconButton variant="large-icon-positive" icon="info_outline" />
);

export const PositiveActive = () => (
  <IconButton variant="large-icon-positive" active icon="info_outline" />
);

PositiveActive.storyName = "Positive, active";

export const Negative = () => (
  <IconButton variant="large-icon-negative" icon="info_outline" />
);

export const NegativeActive = () => (
  <IconButton variant="large-icon-negative" active icon="info_outline" />
);

NegativeActive.storyName = "Negative, active";
