// :copyright: Copyright (c) 2019 ftrack

import { useState } from "react";
import Panel, { ExpansionPanel } from ".";
import centered from "@storybook/addon-centered/react";

export default {
  title: "Panel",
  decorators: [centered],
};

export const BasicPanel = () => (
  <Panel
    style={{ width: "568px", height: "250px" }}
    title="Title for panel"
    description="Subtitle for panel"
    rightAction={<button style={{ padding: "2px 8px" }}>rightAction</button>}
  >
    <br />
    <ul>
      <p>Content for panel.</p>
    </ul>
  </Panel>
);

BasicPanel.storyName = "Basic panel";

function ExpansionPanelTest() {
  const [active, setActive] = useState(false);

  return (
    <ExpansionPanel
      style={{ width: "568px" }}
      title="Title for panel"
      onChange={setActive}
      active={active}
    >
      <p>Content for panel.</p>
    </ExpansionPanel>
  );
}
export const _ExpansionPanel = () => <ExpansionPanelTest />;

function AccordionStory() {
  const [expanded, setExpanded] = useState(false);

  const handleChange = (panel) => (isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <section style={{ width: "568px" }}>
      <ExpansionPanel
        title="1. Elit id id occaecat enim eiusmod labore."
        onChange={handleChange("foo")}
        active={expanded === "foo"}
      >
        <p>
          Ut sit aliqua labore nostrud proident esse sit dolore eu nisi dolor.
        </p>
      </ExpansionPanel>
      <ExpansionPanel
        title="2. Nulla pariatur do commodo fugiat tempor nostrud aute aliqua."
        onChange={handleChange("bar")}
        active={expanded === "bar"}
      >
        <p>
          Esse commodo aute culpa et ut labore consequat enim anim consequat.
          Esse in cupidatat consectetur occaecat duis proident occaecat amet.
          Exercitation et ea aliquip ex enim aute cillum proident elit duis
          incididunt amet. Culpa duis occaecat enim mollit excepteur proident
          elit Lorem occaecat dolor duis. Culpa est exercitation fugiat laboris
          consectetur do fugiat in sit cupidatat. Magna elit sint dolor irure
          enim.
        </p>
        <p>
          Veniam labore sunt qui anim quis pariatur. Eiusmod ipsum laboris id
          laboris laboris quis minim ullamco quis cupidatat. Consequat dolore
          aliqua tempor elit cupidatat laborum ea anim occaecat dolor amet
          laboris mollit excepteur. Do fugiat velit tempor enim. Voluptate
          officia qui cupidatat consequat occaecat nisi nisi enim enim deserunt
          veniam. Nulla labore qui cupidatat elit ipsum sit proident ea minim
          sint do ut Lorem. Proident laboris amet dolore excepteur ut consequat
          do aute deserunt nulla deserunt anim sit.
        </p>
        <p>
          Aute occaecat mollit duis culpa laborum sunt ad occaecat incididunt
          nisi ea aliqua aliqua. Sint in duis proident quis. Fugiat eiusmod
          nulla fugiat ullamco quis sint ut nostrud enim eu eu enim. Amet duis
          mollit cillum anim sit sint tempor reprehenderit labore.
        </p>
      </ExpansionPanel>
      <ExpansionPanel
        title="3. Veniam dolor proident mollit velit eu aute nulla aute magna."
        onChange={handleChange("xyz")}
        active={expanded === "xyz"}
      >
        <p>
          Officia nostrud sunt ea dolore proident cupidatat aliqua adipisicing.
        </p>
        <p>
          Consectetur sunt et officia proident laborum sunt mollit consectetur
          excepteur incididunt consectetur proident laborum.
        </p>
      </ExpansionPanel>
    </section>
  );
}
export const Accordion = () => <AccordionStory />;
