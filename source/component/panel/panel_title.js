// :copyright: Copyright (c) 2019 ftrack
import PropTypes from "prop-types";
import Heading from "../heading";
import classNames from "classnames";

import style from "./panel_title.scss";

function PanelTitle({
  children,
  rightAction,
  rightActionClassName,
  className,
  ...props
}) {
  const classes = classNames(style.heading, className);
  const rightActionClasses = classNames(
    style.rightAction,
    rightActionClassName
  );
  return (
    <Heading variant="title" className={classes} {...props}>
      {children}
      <div className={rightActionClasses}>{rightAction}</div>
    </Heading>
  );
}

PanelTitle.propTypes = {
  children: PropTypes.node,
  rightAction: PropTypes.func,
  className: PropTypes.string,
  rightActionClassName: PropTypes.string,
};

export default PanelTitle;
