// :copyright: Copyright (c) 2019 ftrack
import PropTypes from "prop-types";
import classNames from "classnames";
import PanelTitle from "./panel_title";
import style from "./style.scss";

export default function Panel({
  description,
  title,
  children,
  rightAction,
  titleProps,
  className,
  spacing = 2,
  background = "contained",
  contentClassName,
  ...props
}) {
  const classes = classNames(
    style.container,
    {
      [style.spacing0]: spacing === 0,
      [style.spacing1]: spacing === 1,
      [style.spacing2]: spacing === 2,
      [style.backgroundContained]: background === "contained",
    },
    className
  );

  const contentClasses = classNames(style.content, contentClassName);

  return (
    <div className={classes} {...props}>
      <PanelTitle rightAction={rightAction} {...titleProps}>
        {title}
      </PanelTitle>
      {description ? <p className={style.description}>{description}</p> : null}
      {children ? <div className={contentClasses}>{children}</div> : null}
    </div>
  );
}

Panel.propTypes = {
  background: PropTypes.oneOf([null, "contained"]),
  spacing: PropTypes.oneOf([0, 1, 2]),
  title: PropTypes.node,
  description: PropTypes.string,
  children: PropTypes.node,
  rightAction: PropTypes.func,
  className: PropTypes.string,
  contentClassName: PropTypes.string,
  titleProps: PropTypes.node,
};
