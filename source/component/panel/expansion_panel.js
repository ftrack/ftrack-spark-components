// :copyright: Copyright (c) 2019 ftrack
import PropTypes from "prop-types";
import Panel from "./panel";
import IconButton from "../icon_button";
import classNames from "classnames";
import style from "./expansion_panel.scss";

function ExpansionPanel({
  active,
  title,
  children,
  onChange,
  className,
  titleProps,
  ...props
}) {
  const classes = classNames(style.root, className);

  const rightAction = (
    <IconButton
      variant="default"
      icon="expand_more"
      className={active ? style.iconActive : style.icon}
    />
  );

  return (
    <Panel
      title={title}
      rightAction={rightAction}
      {...props}
      className={classes}
      titleProps={{
        ...titleProps,
        onClick: () => {
          onChange(!active);
        },
      }}
    >
      {active ? children : null}
    </Panel>
  );
}

ExpansionPanel.propTypes = {
  active: PropTypes.bool,
  title: PropTypes.string,
  children: PropTypes.node,
  onChange: PropTypes.func,
  className: PropTypes.string,
};

export default ExpansionPanel;
