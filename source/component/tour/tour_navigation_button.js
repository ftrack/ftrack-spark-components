// :copyright: Copyright (c) 2018 ftrack

import { Button } from "react-toolbox/lib/button";
import PropTypes from "prop-types";
import classNames from "classnames";
import style from "./style.scss";

function TourNavigationButton({ direction, ...props }) {
  const icon = direction === "left" ? "arrow_back" : "arrow_forward";

  const classes = classNames({
    [style.rightBtn]: direction === "right",
    [style.leftBtn]: direction === "left",
  });

  return <Button floating mini icon={icon} className={classes} {...props} />;
}

TourNavigationButton.propTypes = {
  onClick: PropTypes.func,
  direction: PropTypes.string,
};

export default TourNavigationButton;
