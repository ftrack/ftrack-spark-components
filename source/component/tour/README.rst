.. 
    :copyright: Copyright (c) 2018 ftrack

#################
Review Tour Guide
#################

The tour guide can be used to give users a tour of the application. It is built with an 
overlay which pops up when you start the tour. 

tour_dialog is the stateful component in this module. 
