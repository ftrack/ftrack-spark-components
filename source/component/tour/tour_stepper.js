// :copyright: Copyright (c) 2018 ftrack

import PropTypes from "prop-types";
import classNames from "classnames";
import style from "./style.scss";

function TourStepper({ onClick, currentIndex, steps }) {
  return (
    <ol className={style.stepWrapper}>
      {steps.map((step, dotIndex) => {
        const classes = classNames(style.stepper, {
          [style.stepperActive]: currentIndex === dotIndex,
        });
        return (
          <li
            key={dotIndex}
            className={classes}
            onClick={() => {
              onClick(dotIndex);
            }}
          />
        );
      })}
    </ol>
  );
}

TourStepper.propTypes = {
  currentIndex: PropTypes.number,
  onClick: PropTypes.func,
  steps: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.node,
      text: PropTypes.node,
      image: PropTypes.string,
    })
  ),
};

export default TourStepper;
