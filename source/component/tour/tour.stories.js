// :copyright: Copyright (c) 2018 ftrack

import { Component } from "react";
import { IconButton } from "react-toolbox/lib/button";
import centered from "@storybook/addon-centered/react";
import { action } from "@storybook/addon-actions";
import Dialog from "react-toolbox/lib/dialog";
import PropTypes from "prop-types";

import TourDialog from "./tour_dialog";
import RemoteConfigurationTour from "../remote_configuration_tour";

export default {
  title: "Tour",
  decorators: [centered],
};

const tourExampleUrl = "/tour_configurations.json";
const configExampleUrl = "/configuration.json";
const baseUrl = "https://images.unsplash.com/";

const steps = [
  {
    title: "Welcome to the tour",
    text: "Lorem *ipsum* **dolor** sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    image:
      "https://images.unsplash.com/photo-1520956571254-1fcfd1cdbec8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=975&q=80",
    layout: "image_right",
  },
  {
    title: "my-header-id-2",
    text: "my-text-id-2",
    image:
      "https://images.unsplash.com/photo-1520956571254-1fcfd1cdbec8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=975&q=80",
    layout: "image_right",
  },
  {
    title: "my-header-id-3",
    text: "my-text-id-3 my-text-id-3 my-text-id-3 my-text-id-3 my-text-id-3",
    image:
      "https://images.unsplash.com/photo-1544709116-983c98327d46?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1778&q=80",
    layout: "image_right",
  },
  {
    title: "my-header-id-4",
    text: "my-text-id-4",
    image:
      "https://images.unsplash.com/photo-1544709116-983c98327d46?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1778&q=80",
    layout: "image_right",
  },
  {
    title: "my-header-id-5",
    text: "my-text-id-5",
    image:
      "https://images.unsplash.com/photo-1544709116-983c98327d46?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1778&q=80",
    layout: "image_left",
  },
  {
    title: "And you're done!",
    text: "You are now ready to get to work!",
    layout: "center",
    actions: [
      {
        text: "Close",
        value: "close",
        primary: true,
        href: null,
      },
      {
        text: "Go here",
        value: "close",
        primary: false,
        href: null,
      },
    ],
  },
];

class TourDialogTestingStory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
    };
    this.handleToggle = this.handleToggle.bind(this);
  }

  handleToggle() {
    this.setState({ active: !this.state.active });
  }

  render() {
    return (
      <div>
        <IconButton icon="help_outline" onClick={this.handleToggle} />
        <TourDialog
          steps={steps}
          active={this.state.active}
          onEscKeyDown={this.handleToggle}
          onOverlayClick={this.handleToggle}
        />
      </div>
    );
  }
}

export const _TourDialog = () => <TourDialogTestingStory />;

_TourDialog.storyName = "TourDialog";

export const TestingOverlayStyle = () => (
  <div>
    <Dialog title="Testing the appearence of this tour overlay." active />
  </div>
);

TestingOverlayStyle.storyName = "Testing overlay style";

export const ConfigurationTour = () => (
  <RemoteConfigurationTour
    url={tourExampleUrl}
    baseUrl={baseUrl}
    onAction={action("onAction")}
  />
);

export const WhatsNew = () => (
  <RemoteConfigurationTour
    url={configExampleUrl}
    baseUrl={baseUrl}
    onAction={action("onAction")}
  />
);

WhatsNew.storyName = "Whats new";

TourDialog.propTypes = {
  steps: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.node,
      text: PropTypes.node,
      image: PropTypes.string,
      layout: PropTypes.string.isRequired,
    })
  ),
};
