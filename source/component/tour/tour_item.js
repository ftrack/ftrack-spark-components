// :copyright: Copyright (c) 2018 ftrack

import PropTypes from "prop-types";
import classNames from "classnames";
import { Button } from "react-toolbox/lib/button";
import Markdown from "../markdown";
import Heading from "../heading";

import style from "./style.scss";
import actionButtonTheme from "./action_button_theme.scss";

function TourItem({ title, text, image, layout, actions, onAction }) {
  const classes = classNames(style.row, {
    [style.imageLeft]: layout === "image_left",
    [style.imageRight]: layout === "image_right",
    [style.noImage]: layout === "center",
  });
  return (
    <div className={classes}>
      {image ? (
        <div className={style.pictureWrapper}>
          <div
            className={style.image}
            style={{
              backgroundImage: `url('${image}')`,
            }}
          />
        </div>
      ) : null}
      <div className={style.contentWrapper}>
        <Heading color="light" variant="display1" className={style.heading}>
          {title}
        </Heading>
        {typeof text === "string" ? (
          <Markdown source={text} size="large" className={style.text} />
        ) : (
          <Heading variant="headline" className={style.text}>
            {text}
          </Heading>
        )}
        {actions ? (
          <div className={style.buttonContainer}>
            {actions.map((action) => (
              <Button
                theme={actionButtonTheme}
                className={action.className}
                label={action.text}
                primary={action.primary}
                onClick={() => {
                  onAction(action.value);
                }}
                href={action.href}
                target={action.value === "link_external" ? "_blank" : ""}
                rel={
                  action.value === "link_external" ? "noopener noreferrer" : ""
                }
              />
            ))}
          </div>
        ) : null}
      </div>
    </div>
  );
}

TourItem.propTypes = {
  title: PropTypes.node,
  text: PropTypes.node,
  image: PropTypes.string,
  layout: PropTypes.string,
  onAction: PropTypes.func,
  actions: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string,
      value: PropTypes.string,
      primary: PropTypes.bool,
      href: PropTypes.node,
      className: PropTypes.string,
    })
  ),
};

TourItem.defaultProps = {
  layout: "image_right",
};

export default TourItem;
