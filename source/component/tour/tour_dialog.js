// :copyright: Copyright (c) 2018 ftrack

import { Component } from "react";
import Dialog from "react-toolbox/lib/dialog";
import SwipeableViews from "react-swipeable-views";
import { bindKeyboard } from "react-swipeable-views-utils";
import PropTypes from "prop-types";
import classNames from "classnames";

import TourNavigationButton from "../tour/tour_navigation_button";
import TourStepper from "../tour/tour_stepper";
import TourItem from "../tour/tour_item";

import style from "./style.scss";
import dialogTheme from "./dialog_theme.scss";

const BindKeyboardSwipeableViews = bindKeyboard(SwipeableViews);

class TourDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 0,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handlePrevious = this.handlePrevious.bind(this);
    this.handleNext = this.handleNext.bind(this);
  }

  handleChange(step) {
    this.setState({ currentIndex: step });
  }

  handlePrevious() {
    if (this.state.currentIndex === 0) {
      return;
    }
    this.handleChange(this.state.currentIndex - 1);
  }

  handleNext() {
    if (this.state.currentIndex >= this.props.steps.length - 1) {
      return;
    }
    this.handleChange(this.state.currentIndex + 1);
  }

  render() {
    const { currentIndex } = this.state;
    const { steps, onAction, ...props } = this.props;

    const classes = classNames(style.swipableWrapper, {
      [style.singleStep]: steps.length === 1,
    });

    return (
      <Dialog theme={dialogTheme} {...props}>
        {steps.length > 1 ? (
          <TourNavigationButton
            onClick={this.handlePrevious}
            hidden={this.state.currentIndex === 0}
            direction="left"
          />
        ) : null}
        <div>
          <div className={classes}>
            <BindKeyboardSwipeableViews
              disabled={steps.length === 1}
              resistance
              enableMouseEvents
              index={currentIndex}
              onChangeIndex={this.handleChange}
            >
              {steps.map((step, index) => (
                <TourItem key={index} onAction={onAction} {...step} />
              ))}
            </BindKeyboardSwipeableViews>
          </div>
          {steps.length > 1 ? (
            <TourStepper
              steps={steps}
              currentIndex={this.state.currentIndex}
              onClick={this.handleChange}
            />
          ) : null}
        </div>
        {steps.length > 1 ? (
          <TourNavigationButton
            onClick={this.handleNext}
            hidden={!steps[currentIndex + 1]}
            direction="right"
          />
        ) : null}
      </Dialog>
    );
  }
}

TourDialog.propTypes = {
  steps: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.node,
      text: PropTypes.node,
      image: PropTypes.string,
      layout: PropTypes.string,
    })
  ),
  onAction: PropTypes.func,
};

export default TourDialog;
