// :copyright: Copyright (c) 2018 ftrack
import PropTypes from "prop-types";
import compose from "recompose/compose";
import withHandlers from "recompose/withHandlers";
import withTooltip from "react-toolbox/lib/tooltip";
import { IconButton } from "react-toolbox/lib/button";
import { defineMessages, FormattedMessage, intlShape } from "react-intl";
import safeInjectIntl from "../util/hoc/safe_inject_intl";

import style from "./style.scss";

const TooltipIconButton = withTooltip(IconButton);

const messages = defineMessages({
  "current-text": {
    id: "ftrack-spark-components.page-indicator.current-text",
    defaultMessage: "Page {current} of {total}",
  },
  "previous-page-tooltip": {
    id: "ftrack-spark-components.page-indicator.previous-page-tooltip",
    description: "Tooltip for previous page button",
    defaultMessage: "Previous",
  },
  "next-page-tooltip": {
    id: "ftrack-spark-components.page-indicator.next-page-tooltip",
    description: "Tooltip for next page button",
    defaultMessage: "Next",
  },
});
function PageIndicatorBase({
  first,
  current,
  total,
  onPreviousClick,
  onNextClick,
  onChangePage, // eslint-disable-line
  intl, // eslint-disable-line
  ...props
}) {
  const lastPage = total - 1;
  const currentPage = Math.max(Math.min(current, lastPage), first);

  return (
    <span {...props}>
      <TooltipIconButton
        icon="skip_previous"
        onClick={onPreviousClick}
        disabled={currentPage <= first}
        tooltip={<FormattedMessage {...messages["previous-page-tooltip"]} />}
        tooltipPosition="bottom"
      />
      <span className={style.pageIndicatorText}>
        <FormattedMessage
          {...messages["current-text"]}
          values={{ current: currentPage + 1, total }}
        />
      </span>
      <TooltipIconButton
        icon="skip_next"
        onClick={onNextClick}
        disabled={currentPage >= lastPage}
        tooltip={<FormattedMessage {...messages["next-page-tooltip"]} />}
        tooltipPosition="bottom"
      />
    </span>
  );
}

PageIndicatorBase.propTypes = {
  current: PropTypes.number,
  first: PropTypes.number,
  total: PropTypes.number,
  onChangePage: PropTypes.func,
  onPreviousClick: PropTypes.func,
  onNextClick: PropTypes.func,
  intl: intlShape,
};

PageIndicatorBase.defaultProps = {
  current: 0,
  first: 0,
  total: 1,
};

const PageIndicator = compose(
  (BaseComponent) => safeInjectIntl(BaseComponent),
  withHandlers({
    onPreviousClick:
      ({ current, onChangePage }) =>
      (e) => {
        e.stopPropagation();
        onChangePage(current > 0 ? current - 1 : current);
      },
    onNextClick:
      ({ current, total, onChangePage }) =>
      (e) => {
        e.stopPropagation();
        onChangePage(current < total - 1 ? current + 1 : current);
      },
  })
)(PageIndicatorBase);

export default PageIndicator;
