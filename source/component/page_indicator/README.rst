..
    :copyright: Copyright (c) 2018 ftrack

##############
Page Indicator
##############

Simple component for displaying current page and allowing navigating to
previous and next pages.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
current            0           Current page number
total              1           Total number of pages
onChangePage                   Callback when page prev/next is clicked.
