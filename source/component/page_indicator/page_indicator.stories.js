// :copyright: Copyright (c) 2018 ftrack
import centered from "@storybook/addon-centered/react";
import compose from "recompose/compose";
import withState from "recompose/withState";

import PageIndicator from ".";

export default {
  title: "Page indicator",
  decorators: [centered],
};

const PageIndicatorTest = compose(withState("current", "onChangePage", 1))(
  PageIndicator
);

export const _PageIndicator = () => <PageIndicatorTest total={10} />;

_PageIndicator.storyName = "Page indicator";
