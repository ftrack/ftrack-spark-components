// :copyright: Copyright (c) 2018 ftrack

import PropTypes from "prop-types";

import { PureComponent, Fragment } from "react";

import { withHandlers, compose, branch } from "recompose";
import { defineMessages, FormattedMessage } from "react-intl";
import classNames from "classnames";
import { IconButton } from "react-toolbox/lib/button";
import withTooltip from "react-toolbox/lib/tooltip";
import moment from "moment";

import { getForegroundColor } from "../util/color";
import style from "./style.scss";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import { withSession, withSettings } from "../util/hoc";
import {
  DEFAULT_WORKDAY_LENGTH,
  DEFAULT_DISPLAY_BID_AS_DAYS,
} from "../util/constant";
import EntityAvatar from "../entity_avatar";
import Skeleton from "../skeleton";
import { ATTRIBUTE_LOADING } from "../dataview/symbol";

const messages = defineMessages({
  "value-true": {
    defaultMessage: "Yes",
    description: "Boolean attribute value true",
    id: "ftrack-spark-components.attribute.boolean-attribute.value-true",
  },
  "value-false": {
    defaultMessage: "No",
    description: "Boolean attribute value false",
    id: "ftrack-spark-components.attribute.boolean-attribute.value-false",
  },
});

const SpanWithTooltip = withTooltip("span");

/** Render text Formatter */
export function TextFormatter({ value }) {
  if (value === null) {
    return null;
  }
  return (
    <span>{typeof value === "string" ? value : JSON.stringify(value)}</span>
  );
}
TextFormatter.propTypes = { value: PropTypes.string };

export function VersionWithStatusFormatter({ value, onClick }) {
  if (!value || !value.length) {
    return null;
  }
  const result = [];
  value.forEach((data, index) => {
    if (index > 0 && index < value.length) {
      result.push(<span className={style.comma}>,</span>);
    }
    result.push(
      <span>
        <SpanWithTooltip tooltip={data.status.name}>
          <span
            className={style.circle}
            style={{ backgroundColor: data.status.color }}
          />
        </SpanWithTooltip>
        <LinkFormatter value={data.link} shortLabel onClick={onClick} />
      </span>
    );
  });
  return result;
}
VersionWithStatusFormatter.propTypes = {
  value: PropTypes.array, // eslint-disable-line react/forbid-prop-types
  onClick: PropTypes.func,
};

export function SentenceCaseTextFormatter({ value }) {
  return <p className={style["sentence-case"]}>{value}</p>;
}
SentenceCaseTextFormatter.propTypes = { value: PropTypes.string };

function BooleanFormatter_({ value }) {
  if (value === true) {
    return <FormattedMessage {...messages["value-true"]} />;
  }

  if (value === false) {
    return <FormattedMessage {...messages["value-false"]} />;
  }

  return <span />;
}

BooleanFormatter_.propTypes = { value: PropTypes.bool };

export const BooleanFormatter = safeInjectIntl(BooleanFormatter_);

export class LinkFormatter extends PureComponent {
  constructor() {
    super();

    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    const { value } = this.props;
    const lastItem = value[value.length - 1];
    this.props.onClick("link", { id: lastItem.id, type: lastItem.type });
  }

  render() {
    const { value, onClick, shortLabel } = this.props;
    const classes = classNames({
      [style["link-formatter-with-click"]]: onClick,
    });
    if (!value || !value.length) {
      return null;
    }
    let label = "";
    if (shortLabel) {
      label = value[value.length - 1].name;
    } else {
      label = value.map((item) => item.name).join(" / ");
    }

    return (
      <span onClick={onClick && this.onClick} className={classes} title={label}>
        {label}
      </span>
    );
  }
}
LinkFormatter.propTypes = {
  value: PropTypes.array, // eslint-disable-line react/forbid-prop-types
  onClick: PropTypes.func,
  shortLabel: PropTypes.bool,
};

LinkFormatter.defaultProps = {
  shortLabel: false,
};

export function FullnameFormatter({ first, last }) {
  return <span>{`${first || ""} ${last || ""}`}</span>;
}
FullnameFormatter.propTypes = {
  first: PropTypes.string.isRequired,
  last: PropTypes.string.isRequired,
};

/** Render formatted moment date.
 *   Note that the DateFormatter only modify the passed moment if *localize* is true.
 */
export function DateFormatter({ value, displayTooltip, localize }) {
  const clonedValue = moment.isMoment(value) ? moment(value) : null;

  if (!clonedValue) {
    return null;
  }
  const displayValue = localize ? clonedValue.local() : clonedValue;
  const formattedDate = (
    <TextFormatter
      value={
        displayValue &&
        displayValue.format &&
        displayValue.format("MMM Do YYYY")
      }
    />
  );
  if (!displayTooltip) {
    return formattedDate;
  }
  return (
    <SpanWithTooltip
      floating
      tooltip={
        displayValue &&
        displayValue.format &&
        displayValue.format("MMM Do YYYY HH:mm")
      }
    >
      {formattedDate}
    </SpanWithTooltip>
  );
}
DateFormatter.propTypes = {
  value: PropTypes.shape({
    format: PropTypes.func.isRequired,
  }),
  displayTooltip: PropTypes.bool,
  localize: PropTypes.bool,
};

DateFormatter.defaultProps = {
  displayTooltip: false,
  localize: false,
};

/** Render formatted moment date in user's timezone. */
export function LocalizedDateFormatter({ ...props }) {
  return <DateFormatter localize {...props} />;
}

function ThumbnailFormatter_({
  thumbnailId,
  session,
  onClick,
  height,
  disableThumbnailClick,
}) {
  // Render compact image and play button if row height is small.
  const className = classNames(style.image, {
    [style.compact]: height < 50,
    [style.large]: height > 80,
  });
  return (
    <div
      className={className}
      style={{
        backgroundImage: `url('${session.thumbnailUrl(thumbnailId, 500)}')`,
      }}
    >
      {!disableThumbnailClick && onClick && thumbnailId ? (
        <div className={style["button-container"]}>
          <IconButton icon="play_circle_filled" onClick={onClick} flat />
        </div>
      ) : null}
    </div>
  );
}
ThumbnailFormatter_.propTypes = {
  thumbnailId: PropTypes.string,
  session: PropTypes.shape({
    thumbnailUrl: PropTypes.func.isRequired,
  }),
  onClick: PropTypes.func,
  height: PropTypes.number,
  disableThumbnailClick: PropTypes.bool,
};
ThumbnailFormatter_.defaultProps = {
  disableThumbnailClick: false,
};
export const ThumbnailFormatter = compose(
  withHandlers({
    onClick: (props) => () =>
      props.onClick && props.onClick("thumbnail", props.thumbnailId),
  }),
  withSession
)(ThumbnailFormatter_);

export function AvatarThumbnailFormatter_({ user, session, height }) {
  let size = "medium";
  if (height > 80) {
    size = "large";
  } else if (height < 40) {
    size = "small";
  }

  return <EntityAvatar entity={user} size={size} session={session} />;
}
AvatarThumbnailFormatter_.propTypes = {
  session: PropTypes.shape({
    thumbnailUrl: PropTypes.func.isRequired,
  }),
  height: PropTypes.number,
  user: PropTypes.shape({
    thumnail_id: PropTypes.func.isRequired,
    first_name: PropTypes.func.isRequired,
    last_name: PropTypes.func.isRequired,
  }),
};
export const AvatarThumbnailFormatter = withSession(AvatarThumbnailFormatter_);

export function ColorLabelFormatter({ name, color }) {
  if (!name) {
    return <span />;
  }
  return (
    <div
      style={{
        backgroundColor: color,
        color: getForegroundColor(color),
      }}
      className={style["attribute-priority-status"]}
    >
      <span>{name}</span>
    </div>
  );
}
ColorLabelFormatter.propTypes = {
  name: PropTypes.string,
  color: PropTypes.string,
};

export function NamesFormatter({ names }) {
  if (!names) {
    return null;
  }
  const elements = names.reduce((accumulator, item) => {
    const element = (
      <FullnameFormatter first={item.first_name} last={item.last_name} />
    );
    if (accumulator === null) {
      return [element];
    }
    accumulator.push(", ", element);
    return accumulator;
  }, null);
  return <span>{elements}</span>;
}

NamesFormatter.propTypes = {
  names: PropTypes.array, // eslint-disable-line
};

export function SecondToHoursFormatter({ value }) {
  return <span>{((value || 0) / 3600.0).toFixed(2)}</span>;
}
SecondToHoursFormatter.propTypes = {
  value: PropTypes.number.isRequired,
};

function SecondToDaysFormatter_({ value, workdayLength }) {
  return <span>{((value || 0) / workdayLength).toFixed(2)}</span>;
}
SecondToDaysFormatter_.propTypes = {
  value: PropTypes.number.isRequired,
  workdayLength: PropTypes.number.isRequired,
};
export const SecondToDaysFormatter = withSettings({
  workdayLength: DEFAULT_WORKDAY_LENGTH,
})(SecondToDaysFormatter_);

function BidTimeFormatter_({ value, isDisplayBidAsDays }) {
  if (isDisplayBidAsDays) {
    return <SecondToDaysFormatter value={value} />;
  }
  return <SecondToHoursFormatter value={value} />;
}

BidTimeFormatter_.propTypes = {
  value: PropTypes.number.isRequired,
  isDisplayBidAsDays: PropTypes.bool.isRequired,
};

export const BidTimeFormatter = withSettings({
  isDisplayBidAsDays: DEFAULT_DISPLAY_BID_AS_DAYS,
})(BidTimeFormatter_);

/** Render color */
export function ColorFormatter({ value, height }) {
  return (
    <div
      className={style.color}
      style={{
        backgroundColor: value,
        lineHeight: (height && `${height - 8}px`) || undefined,
      }}
    >
      &#160;
    </div>
  );
}
ColorFormatter.propTypes = {
  value: PropTypes.string,
  height: PropTypes.number,
};

const SIZE_MAP = ["B", "KB", "MB", "GB", "TB", "PB"];
function convertBytes(value, iteration = 0) {
  if (value < 1024 || iteration >= SIZE_MAP.length - 1) {
    return {
      value,
      unit: SIZE_MAP[iteration],
    };
  }

  return convertBytes(value / 1024, iteration + 1);
}

export function StorageSizeFormatter({ value, ...props }) {
  const { value: size, unit } = convertBytes(value);
  // Return rounded value with 1 decimal precision.
  return (
    <span {...props}>
      {Math.round(size * 10) / 10} {unit}
    </span>
  );
}
StorageSizeFormatter.propTypes = {
  value: PropTypes.number.isRequired,
};

function LoadingState() {
  return <Skeleton className={style.loadingState} />;
}

export const withAttributeLoadingState = branch(
  ({ value }) => value === ATTRIBUTE_LOADING,
  () => LoadingState
);

export function ComponentFormatter({ value: { name, version }, onClick }) {
  const classes = classNames({
    [style["link-formatter-with-click"]]: !!version,
  });

  const props = {};
  if (version) {
    props.title = version.link.map(({ name }) => name).join(" / ");
    props.onClick = () =>
      onClick("link", { id: version.id, type: "AssetVersion" });
  }
  return (
    <span className={classes} {...props}>
      {name}
    </span>
  );
}

ComponentFormatter.propTypes = {
  value: PropTypes.object,
  onClick: PropTypes.func,
};

export function AssetFormatter({
  value: { name, parent, latest_version: version },
  onClick,
}) {
  const classes = classNames({
    [style["link-formatter-with-click"]]: !!version,
  });

  const props = {};
  if (parent) {
    props.title = [...parent.link.map(({ name }) => name), name].join(" / ");
    props.onClick = () =>
      onClick("link", { id: version.id, type: "AssetVersion" });
  }
  return (
    <span className={classes} {...props}>
      {name}
    </span>
  );
}

AssetFormatter.propTypes = {
  value: PropTypes.object,
  onClick: PropTypes.func,
};

export function ListsFormatter({ values = [], onClick }) {
  return values.map(({ name, id }, i) => (
    <Fragment key={id}>
      {!!i && <span>, </span>}
      <LinkFormatter
        value={[
          {
            name,
            id,
            type: "List",
          },
        ]}
        onClick={onClick}
      />
    </Fragment>
  ));
}

ListsFormatter.propTypes = {
  lists: PropTypes.array, // eslint-disable-line
  onClick: PropTypes.func,
};
