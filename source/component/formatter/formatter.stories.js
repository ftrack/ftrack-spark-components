import { action } from "@storybook/addon-actions";
// eslint-disable-next-line
import centered from "@storybook/addon-centered/react";
import moment from "moment";

import Provider from "../../story/spark_decorator";

import {
  TextFormatter,
  BooleanFormatter,
  LinkFormatter,
  FullnameFormatter,
  DateFormatter,
  ThumbnailFormatter,
  ColorLabelFormatter,
  NamesFormatter,
  SecondToHoursFormatter,
  SecondToDaysFormatter,
  ColorFormatter,
  StorageSizeFormatter,
  BidTimeFormatter,
} from ".";

const clickedAction = action("Clicked");

export default {
  title: "Formatter",
  decorators: [(Story) => <Provider story={<Story />} />, centered],
};

export const BooleanTrue = () => <BooleanFormatter value />;

BooleanTrue.storyName = "Boolean true";

BooleanTrue.parameters = {
  info: "Boolean formatter true",
};

export const BooleanFalse = () => <BooleanFormatter value={false} />;

BooleanFalse.storyName = "Boolean false";

BooleanFalse.parameters = {
  info: "Boolean formatter false",
};

export const Text = () => <TextFormatter value="Foo bar baz" />;

Text.parameters = {
  info: "Text formatter",
};

export const Fullname = () => <FullnameFormatter first="John" last="Doe" />;

Fullname.parameters = {
  info: "Fullname formatter",
};

export const FullnameEmpty = () => (
  <div style={{ width: "200px", height: "60px", border: "1px solid gray" }}>
    <FullnameFormatter />
  </div>
);

FullnameEmpty.storyName = "Fullname empty";

FullnameEmpty.parameters = {
  info: "Fullname empty formatter",
};

export const Names = () => (
  <NamesFormatter
    names={[
      {
        first_name: "John",
        last_name: "Doe",
      },
      {
        first_name: "Mattias",
        last_name: "Lagergren",
      },
      {
        first_name: "Björn",
        last_name: "Rydahl",
      },
    ]}
  />
);

Names.parameters = {
  info: "Names formatter",
};

export const Date = () => <DateFormatter value={moment()} />;

Date.parameters = {
  info: "Date formatter",
};

export const DateWithTooltip = () => (
  <DateFormatter displayTooltip value={moment()} />
);

DateWithTooltip.storyName = "Date with tooltip";

DateWithTooltip.parameters = {
  info: "Date formatter",
};

export const DateWithTooltipLocalized = () => {
  const date = moment.utc();
  return (
    <div>
      <p>
        Date in UTC: <DateFormatter displayTooltip value={date} />
      </p>
      <p>
        Localized date: <DateFormatter displayTooltip localize value={date} />
      </p>
    </div>
  );
};

DateWithTooltipLocalized.storyName = "Date with tooltip, Localized";

DateWithTooltipLocalized.parameters = {
  info: "Date formatter",
};

export const BadDate = () => {
  const date = "bad value";
  return (
    <div>
      <italic>Results should be empty and the page not crash</italic>
      <p>
        Bad date <DateFormatter displayTooltip value={date} />
      </p>
      <p>
        Bad date localize <DateFormatter displayTooltip localize value={date} />
      </p>
      <p>
        Date is undefined{" "}
        <DateFormatter displayTooltip localize value={undefined} />
      </p>
    </div>
  );
};

BadDate.storyName = "Bad date";

BadDate.parameters = {
  info: "Date formatter with bad date value",
};

export const Hours1Hour = () => <SecondToHoursFormatter value={3600} />;

Hours1Hour.storyName = "Hours (1 hour)";

Hours1Hour.parameters = {
  info: "Hours formatter",
};

export const Hours1AndAHalfHour = () => (
  <SecondToHoursFormatter value={3600 * 1.5} />
);

Hours1AndAHalfHour.storyName = "Hours (1 and a half hour)";

Hours1AndAHalfHour.parameters = {
  info: "Hours formatter",
};

export const HoursRounded = () => (
  <SecondToHoursFormatter value={3600 * 1.51231} />
);

HoursRounded.storyName = "Hours (rounded)";

HoursRounded.parameters = {
  info: "Hours formatter",
};

export const Days1Day = () => <SecondToDaysFormatter value={3600 * 24} />;

Days1Day.storyName = "Days (1 day)";

Days1Day.parameters = {
  info: "Days formatter",
};

export const Days1AndAHalfDay = () => (
  <SecondToDaysFormatter value={3600 * 24 * 1.5} />
);

Days1AndAHalfDay.storyName = "Days (1 and a half day)";

Days1AndAHalfDay.parameters = {
  info: "Days formatter",
};

export const DaysRounded = () => (
  <SecondToDaysFormatter value={3600 * 24 * 1.51231} />
);

DaysRounded.storyName = "Days (rounded)";

DaysRounded.parameters = {
  info: "Days formatter",
};

export const ReadingBidTimeSettings = () => (
  <div>
    <p>Days</p>
    <BidTimeFormatter value={3600 * 24 * 1.5} isDisplayBidAsDays />
    <p>Hours</p>
    <BidTimeFormatter value={3600 * 24 * 1.5} />
  </div>
);

ReadingBidTimeSettings.storyName = "Reading Bid time settings";

ReadingBidTimeSettings.parameters = {
  info: "Bid time setting formatter",
};

export const LabelAndColor = () => (
  <div style={{ width: "200px", height: "60px" }}>
    <ColorLabelFormatter name="Foo" color="#935BA2" />
  </div>
);

LabelAndColor.storyName = "Label and color";

LabelAndColor.parameters = {
  info: "Label and color formatter",
};

export const LabelAndColorEmpty = () => (
  <div style={{ width: "200px", height: "60px", border: "1px solid gray" }}>
    <ColorLabelFormatter />
  </div>
);

LabelAndColorEmpty.storyName = "Label and color empty";

LabelAndColorEmpty.parameters = {
  info: "Label and color empty formatter",
};

export const Thumbnail = () => (
  <div style={{ width: "200px", height: "200px" }}>
    <ThumbnailFormatter
      onClick={clickedAction}
      thumbnailId="866a6768-67c1-11e7-944a-0a580ae40a16"
    />
  </div>
);

Thumbnail.parameters = {
  info: "Thumbnail formatter",
};

export const Link = () => (
  <LinkFormatter value={[{ name: "Foo" }, { name: "Bar" }, { name: "Baz " }]} />
);

Link.parameters = {
  info: "Link formatter",
};

export const LinkClickable = () => (
  <LinkFormatter
    onClick={clickedAction}
    value={[{ name: "Foo" }, { name: "Bar" }, { name: "Baz " }]}
  />
);

LinkClickable.storyName = "Link clickable";

LinkClickable.parameters = {
  info: "Link formatter",
};

export const Color = () => (
  <div style={{ width: "16px", height: "30px" }}>
    <ColorFormatter value={"pink"} />
  </div>
);

Color.parameters = {
  info: "Color formatter",
};

export const Storage = () => (
  <table>
    {[
      1023,
      1024,
      1025,
      1024 * 1024,
      1024 * 1024 * 102,
      1024 * 1024 * 102 + 1024 * 500,
      1024 * 1024 * 1024,
      1024 * 1024 * 1024 * 1024,
      1024 * 1024 * 1024 * 1024 * 1024,
      1024 * 1024 * 1024 * 1024 * 1024 * 1024,
    ].map((value) => (
      <tr>
        <th>
          <td>{value} bytes</td>
        </th>
        <tr>
          <td>
            <StorageSizeFormatter value={value} />
          </td>
        </tr>
      </tr>
    ))}
  </table>
);

Storage.parameters = {
  info: "Storage formatter",
};
