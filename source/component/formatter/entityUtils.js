/* eslint-disable import/prefer-default-export */
/* eslint-disable camelcase */
// :copyright: Copyright (c) 2021 ftrack

import { defineMessages, FormattedMessage } from "react-intl";

const messages = defineMessages({
  user: {
    id: "ftrack-spark-components.entity-type.user",
    defaultMessage: "user",
  },
  group: {
    id: "ftrack-spark-components.entity-type.group",
    defaultMessage: "group",
  },
  list: {
    id: "ftrack-spark-components.entity-type.list",
    defaultMessage: "list",
  },
  context: {
    id: "ftrack-spark-components.entity-type.context",
    defaultMessage: "project or object",
  },
  "typed-context": {
    id: "ftrack-spark-components.entity-type.typed-context",
    defaultMessage: "object",
  },
  project: {
    id: "ftrack-spark-components.entity-type.project",
    defaultMessage: "project",
  },
  component: {
    id: "ftrack-spark-components.entity-type.component",
    defaultMessage: "component",
  },
  asset: {
    id: "ftrack-spark-components.entity-type.asset",
    defaultMessage: "asset",
  },
  "asset-version": {
    id: "ftrack-spark-components.entity-type.asset-version",
    defaultMessage: "asset version",
  },
  "review-session": {
    id: "ftrack-spark-components.entity-type.review-session",
    defaultMessage: "review session",
  },
});

export const Entity = {
  User: {
    linkProjection: ["user.id", "user.first_name", "user.last_name"],
    linkRelation: "UserCustomAttributeLink",
    labelProjection: ["id", "first_name", "last_name"],
    order: "first_name, last_name",
    formatter: ({ first_name, last_name }) => `${first_name} ${last_name}`,
    name: <FormattedMessage {...messages.user} />,
  },
  Group: {
    linkProjection: ["group.link"],
    linkRelation: "GroupCustomAttributeLink",
    labelProjection: ["id", "name", "link"],
    order: "link",
    formatter: ({ name }) => name,
    name: <FormattedMessage {...messages.group} />,
    filter: "local != true",
  },
  List: {
    linkProjection: [
      "list.id",
      "list.name",
      "list.project.id",
      "list.project.full_name",
    ],
    linkRelation: "ListCustomAttributeLink",
    labelProjection: ["id", "name"],
    order: "project.full_name, category.name, name",
    formatter: ({ name }) => name,
    name: <FormattedMessage {...messages.list} />,
  },
  get TypedContextList() {
    return this.List;
  },
  get AssetVersionList() {
    return this.List;
  },
  Context: {
    linkProjection: ["context.link"],
    linkRelation: "ContextCustomAttributeLink",
    labelProjection: ["id", "name"],
    order: "name",
    formatter: ({ name }) => name,
    name: <FormattedMessage {...messages.context} />,
  },
  TypedContext: {
    linkProjection: ["context.link"],
    linkRelation: "ContextCustomAttributeLink",
    labelProjection: ["id", "name"],
    order: "name",
    formatter: ({ name }) => name,
    name: <FormattedMessage {...messages["typed-context"]} />,
  },
  Project: {
    linkProjection: ["context.link"],
    linkRelation: "ContextCustomAttributeLink",
    labelProjection: ["id", "full_name"],
    order: "full_name",
    formatter: ({ full_name }) => full_name,
    name: <FormattedMessage {...messages.project} />,
  },
  Asset: {
    linkProjection: [
      "asset.name",
      "asset.parent.link",
      "asset.latest_version.id",
    ],
    linkRelation: "AssetCustomAttributeLink",
    labelProjection: ["id", "name"],
    order: "name",
    formatter: ({ name }) => name,
    name: <FormattedMessage {...messages.asset} />,
  },
  AssetVersion: {
    linkProjection: [
      "asset_version.link",
      "asset_version.status.name",
      "asset_version.status.color",
    ],
    linkRelation: "AssetVersionCustomAttributeLink",
    labelProjection: ["id", "asset.name", "version"],
    order: "asset.name, version desc",
    formatter: ({ asset: { name }, version }) => `${name} - v${version}`,
    name: <FormattedMessage {...messages["asset-version"]} />,
  },
  ReviewSession: {
    labelProjection: ["id", "name"],
    order: "created_at desc",
    formatter: ({ name }) => name,
    name: <FormattedMessage {...messages["review-session"]} />,
  },
};
