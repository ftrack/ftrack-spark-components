// :copyright: Copyright (c) 2020 ftrack
import PropTypes from "prop-types";

import { FormattedMessage, defineMessages } from "react-intl";

const messages = defineMessages({
  version: {
    id: "ftrack-spark-components.attribute-formatter-version.version",
    defaultMessage: "Version {versionNumber}",
    description: "Formatted asset version number",
  },
  "version-short": {
    id: "ftrack-spark-components.attribute-formatter-version.version-short",
    defaultMessage: "V{versionNumber}",
    description: "Short version of formatted asset version number",
  },
});

function VersionNumber({ version, format = "full" }) {
  if (format === "short") {
    return (
      <FormattedMessage
        {...messages["version-short"]}
        values={{ versionNumber: version }}
      />
    );
  }

  return (
    <FormattedMessage
      {...messages.version}
      values={{ versionNumber: version }}
    />
  );
}

VersionNumber.propTypes = {
  version: PropTypes.number,
  format: PropTypes.oneOf(["short", "full"]),
};

export default VersionNumber;
