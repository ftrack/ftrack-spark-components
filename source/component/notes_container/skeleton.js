import Skeleton from "../skeleton";

import style from "./style.scss";

const NoteSkeleton = () => (
  <div className={style["loading-skeleton"]}>
    <div className={style["skeleton-top-row"]}>
      <Skeleton className={style.avatar} />
      <div className={style["skeleton-header-text"]}>
        <Skeleton className={style["skeleton-username"]} />
        <Skeleton className={style["skeleton-date"]} />
      </div>
    </div>
    <Skeleton className={style["skeleton-content"]} />
    <Skeleton className={style["skeleton-content"]} />
    <Skeleton className={style["skeleton-content"]} />
  </div>
);

export default NoteSkeleton;
