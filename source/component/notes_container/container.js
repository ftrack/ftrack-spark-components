import { operation } from "ftrack-javascript-api";
import loglevel from "loglevel";
import moment from "moment";

// :copyright: Copyright (c) 2016 ftrack

import PropTypes from "prop-types";

import { Component } from "react";
import withTooltip from "react-toolbox/lib/tooltip";
import { defineMessages, intlShape, FormattedMessage } from "react-intl";
import classNames from "classnames";
import isNumber from "lodash/isNumber";
import { connect } from "react-redux";
import FontIcon from "react-toolbox/lib/font_icon";
import { Waypoint } from "react-waypoint";
import { takeEvery } from "redux-saga";
import {
  call,
  cancel,
  fork,
  take,
  getContext,
  put,
  cancelled,
  select,
} from "redux-saga/effects";
import FlipMove from "react-flip-move";
import { v4 as uuidV4 } from "uuid";
import scrollIntoViewIfNeeded from "scroll-into-view-if-needed";

import { catchAllErrors } from "../util/saga";
import { pickReviewColor } from "../util/color";
import EmptyText from "../empty_text";
import EntityLink from "../entity_link";
import components from "../note";
import {
  DATETIME_ISO_FORMAT,
  THUMBNAIL_SIZES,
  TRANSITION_DURATION,
} from "../util/constant";
import { withSession } from "../util/hoc";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import NoteSkeleton from "./skeleton";
import style from "./style.scss";
import tooltipTheme from "./tooltip_theme.scss";
import { getVersionFromEntityNameIfPossible } from "../util/string";
import isTypedContextSubclass from "../util/schema_operations";

const DEFAULT_NAMESPACE = "default";
const DEFAULT_SELECT = [
  "id",
  "content",
  "author.first_name",
  "author.last_name",
  "author.thumbnail_id",
  "author.thumbnail_url",
  "parent_id",
  "parent_type",
  "date",
  "note_components.component.file_type",
  "note_components.component.name",
  "note_components.component.id",
  "note_components.component.metadata.key",
  "note_components.thumbnail_url",
  "note_components.url",
  "metadata.key",
  "metadata.value",
  "in_reply_to_id",
  "completed_at",
  "completed_by.first_name",
  "completed_by.last_name",
  "completed_by.thumbnail_id",
  "completed_by.thumbnail_url",
  "is_todo",
  "thread_activity",
  "recipients.resource_id",
  "recipients.text_mentioned",
];

const SpanWithToolTip = withTooltip("span");
const ENTITY_INFORMATION_TOOLTIP_DELAY = 400;

function delay(ms) {
  return new Promise((resolve) => setTimeout(() => resolve(true), ms));
}

const { EditableNote, ReplyForm, NoteForm } = components;

const logger = loglevel.getLogger("saga:note");

const NOTES_LOAD = "NOTES_LOAD";
const NOTES_LOAD_NEXT_PAGE = "NOTES_LOAD_NEXT_PAGE";
const NOTES_RELOAD = "NOTES_RELOAD";
const NOTES_LOADED = "NOTES_LOADED";
const NOTES_LOAD_ERROR = "NOTES_LOAD_ERROR";
const NOTES_LOAD_CANCELLED = "NOTES_LOAD_CANCELLED";
const NOTES_LOAD_SINGLE = "NOTES_LOAD_SINGLE";
const NOTES_LOAD_SINGLE_COMPLETE = "NOTES_LOAD_SINGLE_COMPLETE";

const OPEN_NOTE_FORM = "OPEN_NOTE_FORM";
const HIDE_NOTE_FORM = "HIDE_NOTE_FORM";
const SUBMIT_NOTE_FORM = "SUBMIT_NOTE_FORM";
const NOTE_FORM_SUBMITTED = "NOTE_FORM_SUBMITTED";

const SUBMIT_NOTE = "SUBMIT_NOTE";
const REMOVE_NOTE = "REMOVE_NOTE";

const NOTE_SUBMITTED = "NOTE_SUBMITTED";
const NOTE_REMOVED = "NOTE_REMOVED";

const NOTE_SUBMIT_ERROR = "NOTE_SUBMIT_ERROR";
const NOTE_REMOVE_ERROR = "NOTE_REMOVE_ERROR";

const NOTE_TODO_COMPLETED_CHANGE = "NOTE_TODO_COMPLETED_CHANGE";
const NOTE_TODO_COMPLETED_CHANGED = "NOTE_TODO_COMPLETED_CHANGED";

const NOTE_HIGHLIGHT = "NOTE_HIGHLIGHT";

export const actions = {
  NOTES_LOAD,
  NOTES_LOAD_NEXT_PAGE,
  NOTES_RELOAD,
  NOTES_LOADED,
  NOTES_LOAD_ERROR,
  NOTES_LOAD_CANCELLED,
  NOTES_LOAD_SINGLE,
  NOTES_LOAD_SINGLE_COMPLETE,
  OPEN_NOTE_FORM,
  HIDE_NOTE_FORM,
  SUBMIT_NOTE_FORM,
  SUBMIT_NOTE,
  REMOVE_NOTE,
  NOTE_FORM_SUBMITTED,
  NOTE_SUBMITTED,
  NOTE_SUBMIT_ERROR,
  NOTE_REMOVED,
  NOTE_TODO_COMPLETED_CHANGE,
  NOTE_TODO_COMPLETED_CHANGED,
  NOTE_HIGHLIGHT,
};

const messages = defineMessages({
  "attribute-message-no-notes-found": {
    id: "ftrack-spark-components-notes-container-attribute-message-no-notes-found",
    defaultMessage: "There is no feedback yet",
  },
  "top-entity-information": {
    id: "ftrack-spark-components-notes-container-top-entity-information",
    description:
      "Is set before the items name when EntityInformation is in a tooltip",
    defaultMessage: "on",
  },
  "top-entity-information-version-number": {
    id: "ftrack-spark-components-notes-container-top-entity-information-version",
    description:
      "Text to display before item if comment related to ReviewSessionObject or AssetVersion",
    defaultMessage: "Version {versionNumber}",
  },
});

/** Load notes action creator. */
export function notesLoad(
  namespace,
  id,
  type,
  filters,
  sort,
  limit,
  includeAllVersions,
  noteSelect
) {
  return {
    type: NOTES_LOAD,
    meta: {
      namespace,
    },
    payload: {
      entity: {
        id,
        type,
      },
      filters,
      sort,
      limit,
      includeAllVersions,
      noteSelect,
    },
  };
}

/** Load next page of notes action creator. */
export function notesLoadNextPage(
  namespace,
  id,
  type,
  filters,
  sort,
  limit,
  nextOffset
) {
  return {
    type: NOTES_LOAD_NEXT_PAGE,
    meta: {
      namespace,
    },
    payload: {
      entity: {
        id,
        type,
      },
      filters,
      sort,
      limit,
      nextOffset,
    },
  };
}

/** Load notes action creator. */
export function notesReload(namespace) {
  return {
    type: NOTES_RELOAD,
    meta: {
      namespace,
    },
    payload: {},
  };
}

/** Change note todo completion. */
export function noteCompletionChange(namespace, id, isCompleted) {
  return {
    type: NOTE_TODO_COMPLETED_CHANGE,
    meta: {
      namespace,
    },
    payload: {
      id,
      isCompleted,
    },
  };
}

/** Change note todo completion. */
export function noteCompletedChanged(namespace, note) {
  return {
    type: NOTE_TODO_COMPLETED_CHANGED,
    meta: {
      namespace,
    },
    payload: {
      note,
    },
  };
}

/** Open notes form action creator. */
export function openNoteForm(namespace, formKey, data) {
  return {
    type: OPEN_NOTE_FORM,
    meta: {
      namespace,
    },
    payload: {
      formKey,
      data,
    },
  };
}

/** Hide notes form action creator. */
export function hideNoteForm(namespace, formKey, content) {
  return {
    type: HIDE_NOTE_FORM,
    meta: {
      namespace,
    },
    payload: {
      formKey,
      content,
    },
  };
}

/** Remove note action creator. */
export function removeNote(namespace, id) {
  return {
    type: REMOVE_NOTE,
    meta: {
      namespace,
    },
    payload: {
      id,
    },
  };
}

/** Notes loaded action creator. */
export function notesLoaded(namespace, entity, notes, nextOffset) {
  return {
    type: NOTES_LOADED,
    meta: {
      namespace,
    },
    payload: {
      entity,
      items: notes,
      nextOffset,
    },
  };
}

/** Notes highlight action creator. */
export function noteHighlight(namespace, noteId) {
  return {
    type: NOTE_HIGHLIGHT,
    meta: {
      namespace,
    },
    payload: {
      noteId,
    },
  };
}

/** Submit note form action creator. */
export function submitNoteForm(namespace, formKey, data, author) {
  return {
    type: SUBMIT_NOTE_FORM,
    meta: {
      namespace,
    },
    payload: {
      formKey,
      author,
      data,
    },
  };
}

/** Submit note form action creator. */
export function submitNote(
  namespace,
  data,
  author,
  { metadata, componentIds, labelIds } = {}
) {
  return {
    type: SUBMIT_NOTE,
    meta: {
      namespace,
    },
    payload: {
      data,
      metadata,
      componentIds,
      labelIds,
      author,
    },
  };
}

/** Notes submitted action creator. */
export function noteSubmitted(namespace, note, isUpdate) {
  return {
    type: NOTE_SUBMITTED,
    meta: {
      namespace,
    },
    payload: {
      note,
      isUpdate,
    },
  };
}

/** Notes submitted action creator. */
export function noteFormSubmitted(namespace, formKey, note, isUpdate) {
  return {
    type: NOTE_FORM_SUBMITTED,
    meta: {
      namespace,
    },
    payload: {
      formKey,
      note,
      isUpdate,
    },
  };
}

/** Note removed action creator. */
export function noteRemoved(namespace, id) {
  return {
    type: NOTE_REMOVED,
    meta: {
      namespace,
    },
    payload: {
      id,
    },
  };
}

/** Notes submitted action creator. */
export function noteSubmitError(namespace, error) {
  return {
    type: NOTE_SUBMIT_ERROR,
    meta: {
      namespace,
    },
    payload: {
      error,
    },
  };
}

/** Note removed action creator. */
export function noteRemoveError(namespace, error) {
  return {
    type: NOTE_REMOVE_ERROR,
    meta: {
      namespace,
    },
    payload: {
      error,
    },
  };
}

/** Note load error action creator. */
export function notesLoadError(namespace, error) {
  return {
    type: NOTES_LOAD_ERROR,
    meta: {
      namespace,
    },
    payload: {
      error,
    },
  };
}

/** Note load error action creator. */
export function notesLoadCancelled(namespace) {
  return {
    type: NOTES_LOAD_CANCELLED,
    meta: {
      namespace,
    },
    payload: {},
  };
}

/** Load note action creator. */
export function notesLoadSingle(namespace, noteId) {
  return {
    type: NOTES_LOAD_SINGLE,
    meta: {
      namespace,
    },
    payload: { noteId },
  };
}

/** Load note action creator. */
export function notesLoadSingleComplete(namespace, note) {
  return {
    type: NOTES_LOAD_SINGLE_COMPLETE,
    meta: {
      namespace,
    },
    payload: { note },
  };
}

const sortNotesCreator = (field) => (a, b) => {
  if (a[field].isSame(b[field])) {
    if (a.id > b.id) {
      return -1;
    }

    if (a.id < b.id) {
      return 1;
    }

    return 0;
  }

  if (a[field].toDate() < b[field].toDate()) {
    return 1;
  }

  return -1;
};

const sortNotesOnDate = sortNotesCreator("date");
const sortNotesOnThreadActivity = sortNotesCreator("thread_activity");

function sortNotesOnFrame(noteA, noteB) {
  const { frame: frameA = {} } = noteA;
  const { frame: frameB = {} } = noteB;

  if (isNumber(frameB.number) && isNumber(frameA.number)) {
    return frameA.number - frameB.number;
  }

  if (isNumber(frameA.number)) {
    return -1;
  }

  if (isNumber(frameB.number)) {
    return 1;
  }

  return 0;
}

/**
 * Reduce state for application-wide notes.
 */
export function reducer(multiState = {}, action) {
  const namespace = action.meta && action.meta.namespace;
  if (!namespace) {
    // Ignore all actions where namespace is not defined.
    return multiState;
  }

  const state = multiState[namespace] || { highlight: null };
  let nextState = state;

  if (action.type === actions.NOTES_LOAD) {
    nextState = Object.assign({}, state, {
      entity: action.payload.entity,
      noteSelect: action.payload.noteSelect,
      items: [],
      nextOffset: null,
      loading: true,
      // Record latest payload so that it can be used for reload.
      latestPayload: { ...action.payload },
    });
  }
  if (action.type === actions.NOTES_LOAD_NEXT_PAGE) {
    nextState = Object.assign({}, state, {
      loading: true,
    });
  } else if (action.type === actions.NOTES_LOADED) {
    const items = [...state.items, ...action.payload.items];

    nextState = Object.assign({}, state, {
      items,
      nextOffset: action.payload.nextOffset,
      loading: false,
    });
  } else if (action.type === actions.OPEN_NOTE_FORM) {
    const forms = Object.assign({}, state.forms);
    const exists = forms[action.payload.formKey] !== undefined;
    const initialState = exists ? {} : action.payload.data;

    forms[action.payload.formKey] = Object.assign(
      {},
      forms[action.payload.formKey],
      {
        state: "visible",
      },
      initialState
    );

    nextState = Object.assign({}, state, {
      forms,
    });
  } else if (action.type === actions.HIDE_NOTE_FORM) {
    const forms = Object.assign({}, state.forms);
    forms[action.payload.formKey] = Object.assign(
      {},
      state.forms[action.payload.formKey],
      {
        state: "hidden",
        content: action.payload.content,
      }
    );

    nextState = Object.assign({}, state, {
      forms,
    });
  } else if (action.type === actions.SUBMIT_NOTE_FORM) {
    const forms = Object.assign({}, state.forms);
    forms[action.payload.formKey] = Object.assign(
      {},
      state.forms[action.payload.formKey],
      {
        state: "pending",
      }
    );

    nextState = Object.assign({}, state, {
      forms,
    });
  } else if (action.type === actions.NOTE_SUBMITTED) {
    let items;
    const nextOffset = state.nextOffset;

    if (action.payload.isUpdate) {
      // Find and update the note if the operation is an update.
      items = state.items.map((note) => {
        if (note.id === action.payload.note.id) {
          return action.payload.note;
        } else if (note.id === action.payload.note.in_reply_to_id) {
          const parentNote = Object.assign({}, note);
          parentNote.replies = note.replies.map((reply) => {
            if (reply.id === action.payload.note.id) {
              return action.payload.note;
            }

            return reply;
          });
          return parentNote;
        }

        return note;
      });
    } else if (action.payload.note.in_reply_to_id) {
      items = state.items.map((note) => {
        if (note.id === action.payload.note.in_reply_to_id) {
          const replies = [...note.replies, action.payload.note];
          return Object.assign({}, note, { replies });
        }

        return note;
      });
    } else {
      items = [action.payload.note, ...state.items];
    }
    nextState = Object.assign({}, state, {
      items,
      nextOffset,
    });
  } else if (action.type === actions.NOTE_FORM_SUBMITTED) {
    const forms = Object.assign({}, state.forms);
    delete forms[action.payload.formKey];
    nextState = Object.assign({}, state, {
      forms,
    });
  } else if (action.type === actions.NOTE_REMOVED) {
    // Find and remove the note if the action is a note being removed.
    const items = [];
    const nextOffset = state.nextOffset;

    state.items.forEach((note) => {
      if (note.id !== action.payload.id) {
        let nextNote = note;

        const replies = [];
        note.replies.forEach((reply) => {
          if (reply.id !== action.payload.id) {
            replies.push(reply);
          }
        });

        // If length differ a matching note was removed and the
        // replies array must be updated.
        if (nextNote.replies.length !== replies.length) {
          nextNote = { ...note, replies };
        }
        items.push(nextNote);
      }
    });

    nextState = Object.assign({}, state, {
      items,
      nextOffset,
    });
  } else if (action.type === actions.NOTE_TODO_COMPLETED_CHANGE) {
    const pendingTodos = Object.assign({}, state.pendingTodos);
    pendingTodos[action.payload.id] = true;

    nextState = Object.assign({}, state, {
      pendingTodos,
    });
  } else if (action.type === actions.NOTE_TODO_COMPLETED_CHANGED) {
    // Find and update the note.
    const items = state.items.map((note) => {
      if (note.id === action.payload.note.id) {
        return action.payload.note;
      } else if (note.id === action.payload.note.in_reply_to_id) {
        const parentNote = Object.assign({}, note);
        parentNote.replies = note.replies.map((reply) => {
          if (reply.id === action.payload.note.id) {
            return action.payload.note;
          }

          return reply;
        });
        return parentNote;
      }

      return note;
    });
    const pendingTodos = Object.assign({}, state.pendingTodos);
    delete pendingTodos[action.payload.note.id];

    nextState = Object.assign({}, state, {
      pendingTodos,
      items,
    });
  } else if (action.type === actions.NOTES_LOAD_SINGLE_COMPLETE) {
    let nextItems = state.items;
    const { note } = action.payload;
    const inReplyToId = note.in_reply_to_id;
    if (inReplyToId) {
      const parentIndex = state.items.findIndex(
        (candidate) => candidate.id === inReplyToId
      );
      if (parentIndex !== -1) {
        const parentNote = state.items[parentIndex];
        const { replies } = parentNote;
        const replyIndex = replies.findIndex(
          (candidate) => candidate.id === note.id
        );
        let nextReplies;
        if (replyIndex === -1) {
          nextReplies = [...replies, note];
        } else {
          nextReplies = [...replies];
          nextReplies[replyIndex] = note;
        }
        nextItems = [...state.items];
        nextItems[parentIndex] = {
          ...parentNote,
          replies: nextReplies,
        };
      }
    } else {
      const noteIndex = state.items.findIndex(
        (candidate) => candidate.id === note.id
      );
      // Add or update note
      if (noteIndex === -1) {
        nextItems = [note, ...state.items];
      } else {
        nextItems = [...state.items];
        nextItems[noteIndex] = note;
      }

      // Resort items
      const { sort } = state.latestPayload;
      if (sort === "frame") {
        nextItems.sort(sortNotesOnFrame);
      } else {
        nextItems.sort(sortNotesOnThreadActivity);
      }
    }

    nextState = { ...state, items: nextItems };
  } else if (action.type === actions.NOTE_HIGHLIGHT) {
    nextState = Object.assign({}, state, {
      highlight: action.payload.noteId,
    });
  }

  if (state !== nextState) {
    return { ...multiState, [namespace]: nextState };
  }

  return multiState;
}

/** Load additional information on *notes* */
function* loadAdditionalNotesInformation(notes, session) {
  const relatedEntitiesSelect = "id, link, status.color, thumbnail_url";
  const reviewObjectAttributes = [
    "name",
    "description",
    "version",
    "asset_version.thumbnail_url",
    "review_session.name",
    "review_session_id",
    "review_session.project_id",
    "review_session.project.full_name",
  ];

  // TODO: This can be removed once merging works as expected.
  // It is required to ensure that AssetVersion contains link when
  // returned both as part of ReviewSessionObject and separately.
  const schema = session.schemas.find(
    (candidate) => candidate.id === "AssetVersion"
  );
  const hasLinkAttribute = (schema && schema.properties.link) || false;
  if (hasLinkAttribute) {
    reviewObjectAttributes.push("asset_version.link");
  }

  // Gather all version ids of assets publish on entity.
  const extraInformationQuery = {
    AssetVersion: `select ${relatedEntitiesSelect} from AssetVersion`,
    TypedContext: `select ${relatedEntitiesSelect} from TypedContext`,
    Project: "select id, link, thumbnail_url from Project",
    ReviewSessionObject: `select ${reviewObjectAttributes.join(
      ","
    )} from ReviewSessionObject`,
  };

  function processNoteMeta(note) {
    note.metadata.forEach((item) => {
      if (item.key === "reviewFrame") {
        try {
          note.frame = JSON.parse(item.value);
        } catch (error) {
          // Frame number has not been set correctly, do
          // nothing.
        }
      }

      if (item.key === "inviteeStatus") {
        note.inviteeStatus = item.value;
      }
    });
  }

  const noteParentIds = notes.reduce((accumulator, note) => {
    if (!accumulator[note.parent_type]) {
      accumulator[note.parent_type] = [];
    }
    accumulator[note.parent_type].push(note.parent_id);

    return accumulator;
  }, {});

  const relatedEntitiesResponse = yield call(
    [session, session.call],
    Object.keys(noteParentIds).map((model) => {
      let targetModel = model;
      const targetSchema = session.schemas.find(
        (schema) => schema.id === model
      );
      if (targetSchema.alias_for && targetSchema.alias_for.id === "Task") {
        targetModel = "TypedContext";
      }
      return operation.query(
        `${extraInformationQuery[targetModel]} where id in (${noteParentIds[model]})`
      );
    })
  );

  const extraInformation = {};
  relatedEntitiesResponse.forEach((item) => {
    item.data.forEach((partial) => (extraInformation[partial.id] = partial));
  });

  notes.forEach((note) => {
    if (extraInformation[note.parent_id]) {
      note.extraInformation = extraInformation[note.parent_id];
    }

    processNoteMeta(note);

    if (note.replies) {
      // Note replies has a category in the model that is not visible to
      // the end-user.
      // TODO: Change this when displaying frame numbers as tags.
      note.replies.forEach((reply) => {
        processNoteMeta(reply);
        delete reply.category;
      });

      // Order replies since there is garantuee that the they are ordered.
      note.replies.sort(sortNotesOnDate);
      note.replies.reverse();
    }
  });
}

/** Handle remove note *action*. */
function* removeNoteHandler(action) {
  const deleteOperation = operation.delete("Note", [action.payload.id]);

  const session = yield getContext("ftrackSession");

  try {
    yield call([session, session.call], [deleteOperation]);
  } catch (error) {
    yield put(noteRemoveError(action.meta.namespace, error));
    return;
  }

  yield put(noteRemoved(action.meta.namespace, action.payload.id));
}

function* submitNoteFormHandler(action) {
  yield put(
    submitNote(
      action.meta.namespace,
      action.payload.data,
      action.payload.author
    )
  );
  yield put(noteFormSubmitted(action.meta.namespace, action.payload.formKey));
}

/** Handle submit note *action*. */
function* submitNoteHandler(action) {
  const submitNoteOperations = [];
  const isUpdate = !!action.payload.data.id;
  const componentIds = action.payload.componentIds;
  const author = action.payload.author;
  const labelIds = action.payload.data.labelIds || [];
  const recipients = action.payload.data.recipients;

  const session = yield getContext("ftrackSession");
  if (isUpdate) {
    const noteId = action.payload.data.id;
    const existingNoteLabelsResponse = yield session.query(
      `select label_id from NoteLabelLink where note_id is "${noteId}"`
    );
    const existingNoteLabels = existingNoteLabelsResponse.data || [];
    const deletableNoteLabels = existingNoteLabels.filter(
      (noteLabel) => !labelIds.includes(noteLabel.label_id)
    );
    const newNoteLabelIds = labelIds.filter(
      (labelId) =>
        !existingNoteLabels.some((noteLabel) => noteLabel.label_id === labelId)
    );
    submitNoteOperations.push(
      operation.update("Note", [noteId], {
        content: action.payload.data.content,
        is_todo: action.payload.data.is_todo === true,
      })
    );
    submitNoteOperations.push(
      ...deletableNoteLabels.map((deletableNoteLabel) =>
        operation.delete("NoteLabelLink", [noteId, deletableNoteLabel.label_id])
      )
    );
    submitNoteOperations.push(
      ...newNoteLabelIds.map((labelId) =>
        operation.create("NoteLabelLink", {
          label_id: labelId,
          note_id: noteId,
        })
      )
    );
  } else {
    const data = {
      content: action.payload.data.content,
      parent_id: action.payload.data.parent_id,
      parent_type: action.payload.data.parent_type,
      in_reply_to_id: action.payload.data.in_reply_to_id,
      is_todo: action.payload.data.is_todo === true,
      user_id: author.id,
      id: uuidV4(),
    };

    submitNoteOperations.push(operation.create("Note", data));

    if (action.payload.metadata) {
      submitNoteOperations.push(
        ...action.payload.metadata.map((item) =>
          operation.create("Metadata", {
            ...item,
            parent_id: data.id,
            parent_type: "Note",
          })
        )
      );
    }

    if (componentIds) {
      submitNoteOperations.push(
        ...componentIds.map((componentId) =>
          operation.create("NoteComponent", {
            component_id: componentId,
            note_id: data.id,
          })
        )
      );
    }

    if (labelIds) {
      submitNoteOperations.push(
        ...labelIds.map((labelId) =>
          operation.create("NoteLabelLink", {
            label_id: labelId,
            note_id: data.id,
          })
        )
      );
    }

    if (recipients) {
      submitNoteOperations.push(
        ...recipients.map((recipient) =>
          operation.create("Recipient", {
            note_id: data.id,
            resource_id: recipient.resource_id,
            text_mentioned: recipient.text_mentioned,
          })
        )
      );
    }
  }

  let submitResponse;
  try {
    submitResponse = yield call([session, session.call], submitNoteOperations);
  } catch (error) {
    yield put(noteSubmitError(action.meta.namespace, error));
    throw error;
  }

  const noteId = submitResponse[0].data.id;

  if (isUpdate) {
    try {
      yield recipients.map((recipient) =>
        session.ensure(
          "Recipient",
          {
            note_id: noteId,
            resource_id: recipient.resource_id,
            text_mentioned: recipient.text_mentioned,
          },
          ["resource_id", "note_id"]
        )
      );
    } catch (error) {
      yield put(noteSubmitError(action.meta.namespace, error));
      throw error;
    }
  }

  const noteSelect = yield select(
    (state) => state.notes[action.meta.namespace].noteSelect
  );
  const query = `${noteSelect} where id is "${noteId}"`;

  const response = yield call([session, session.query], query);
  const [note] = response.data;

  yield loadAdditionalNotesInformation([note], session);

  yield put(noteSubmitted(action.meta.namespace, note, isUpdate));
}

/** Handle load notes and load next page *action*. */
function* loadNotesHandler(action) {
  let offset = 0;
  const limit = action.payload.limit;
  const sort = action.payload.sort;
  const includeAllVersions = action.payload.includeAllVersions;

  if (action.type === actions.NOTES_LOAD_NEXT_PAGE) {
    offset = action.payload.nextOffset;
  }

  const session = yield getContext("ftrackSession");
  const entity = action.payload.entity;
  const baseFilters = action.payload.filters;

  const filters = [];

  if (entity.type === "Project") {
    filters.push(`parent_id is "${entity.id}"`);
    filters.push(
      `parent_id in (select id from TypedContext where project_id is "${entity.id}")`
    );
    filters.push(
      `parent_id in (select id from AssetVersion where asset.context_id is "${entity.id}"` +
        `or asset.parent.project_id is "${entity.id}")`
    );
    filters.push(
      "parent_id in (select id from ReviewSessionObject where review_session.project_id " +
        ` is "${entity.id}" and review_session.is_moderated is False)`
    );
  } else if (
    isTypedContextSubclass(entity.type, session.schemas) ||
    entity.type === "TypedContext"
  ) {
    filters.push(`parent_id is "${entity.id}"`);
    filters.push(
      `parent_id in (select id from TypedContext where ancestors.id is "${entity.id}")`
    );
    filters.push(
      `parent_id in (select id from AssetVersion where asset.context_id is "${entity.id}"` +
        ` or asset.parent.ancestors.id is "${entity.id}"` +
        ` or task_id is "${entity.id}")`
    );
    filters.push(
      "parent_id in (select id from ReviewSessionObject where " +
        `(asset_version.asset.context_id is "${entity.id}" or ` +
        `asset_version.asset.parent.ancestors.id is "${entity.id}") and ` +
        "review_session.is_moderated is False)"
    );
  } else if (entity.type === "ReviewSessionObject") {
    if (includeAllVersions) {
      filters.push('parent_type is "review_session_object"');
    } else {
      filters.push(`parent_id is "${entity.id}"`);
    }
  } else if (entity.type === "ReviewSession") {
    filters.push(
      "parent_id in (select id from ReviewSessionObject where " +
        `review_session_id is "${entity.id}")`
    );
  } else if (entity.type === "AssetVersion") {
    if (includeAllVersions) {
      filters.push(
        `parent_id in (select id from AssetVersion where asset.versions.id is "${entity.id}")`
      );
      filters.push(
        "parent_id in (select id from ReviewSessionObject where " +
          `(asset_version.asset.versions.id is "${entity.id}" and ` +
          "review_session.is_moderated is False))"
      );
    } else {
      filters.push(`parent_id is "${entity.id}"`);
      filters.push(
        "parent_id in (select id from ReviewSessionObject where " +
          `(asset_version.id is "${entity.id}" and ` +
          "review_session.is_moderated is False))"
      );
    }
  } else {
    throw new Error(`Notes cannot be fetched for type: ${entity.type}.`);
  }

  let pageFilter = "";
  if (offset) {
    pageFilter =
      ` and (thread_activity < "${offset.date.format(
        "YYYY-MM-DDTHH:mm:ss"
      )}" or ` +
      `(thread_activity = "${offset.date.format(
        "YYYY-MM-DDTHH:mm:ss"
      )}" and id < "${offset.id}"))`;
  }

  const baseFiltersExpression = baseFilters ? `and (${baseFilters})` : "";
  const noteSelect = yield select(
    (state) => state.notes[action.meta.namespace].noteSelect
  );
  const queries = filters.map(
    (filter) =>
      `${noteSelect} where ` +
      `(${filter}) and not in_reply_to has () ${baseFiltersExpression} ${pageFilter} ` +
      `order by thread_activity desc, id desc limit ${limit}`
  );
  try {
    logger.debug('Loading notes with "', queries, '" from action', action);
    const response = yield call(
      [session, session.call],
      queries.map((query) => operation.query(query))
    );

    logger.debug("Notes query result: ", response);

    let notes = response.reduce(
      (accumulator, result) => accumulator.concat(result.data),
      []
    );
    notes.sort(sortNotesOnThreadActivity);
    notes = notes.splice(0, limit);
    yield loadAdditionalNotesInformation(notes, session);

    let nextOffset = null;

    if (notes.length && notes.length >= limit) {
      const lastNote = notes[notes.length - 1];
      nextOffset = { date: lastNote.thread_activity, id: lastNote.id };
    }

    if (sort === "frame") {
      notes.sort(sortNotesOnFrame);

      if (nextOffset !== null) {
        logger.warn("Notes sort on frame does not support paged results.");
      }
    }

    yield put(
      notesLoaded(
        action.meta.namespace,
        {
          id: action.payload.entity.id,
          type: action.payload.entity.type,
        },
        notes,
        nextOffset
      )
    );
  } catch (error) {
    yield put(notesLoadError(action.meta.namespace, error));
  } finally {
    const isCancelled = yield cancelled();
    if (isCancelled) {
      yield put(notesLoadCancelled(action.meta.namespace));
    }
  }
}

function* reloadNotesHandler(action) {
  const { entity, filters, sort, limit, includeAllVersions, noteSelect } =
    yield select((state) => state.notes[action.meta.namespace].latestPayload);

  yield put(
    notesLoad(
      action.meta.namespace,
      entity.id,
      entity.type,
      filters,
      sort,
      limit,
      includeAllVersions,
      noteSelect
    )
  );
}

function* notesLoadSingleHandler({ meta, payload }) {
  const { namespace } = meta;
  const { noteId } = payload;
  const session = yield getContext("ftrackSession");
  const noteState = yield select((state) => state.notes[namespace]);
  if (noteState) {
    const { noteSelect } = noteState;
    const noteResponse = yield session.query(
      `${noteSelect} where id is "${noteId}"`
    );
    const data = noteResponse.data[0];
    if (data) {
      yield loadAdditionalNotesInformation([data], session);
      yield put(notesLoadSingleComplete(namespace, data));
    }
  }
}

/** Handle todo change *action* */
function* changeTodoHandler(action) {
  const session = yield getContext("ftrackSession");

  const { id, isCompleted } = action.payload;
  let userId = null;
  let completedAt = null;

  if (isCompleted) {
    const response = yield session.query(
      `select id from User where username is "${session.apiUser}"`
    );
    userId = response.data[0].id;
    completedAt = moment().format(DATETIME_ISO_FORMAT);
  }

  yield session.update("Note", [id], {
    completed_by_id: userId,
    completed_at: completedAt,
  });
  const noteSelect = yield select(
    (state) => state.notes[action.meta.namespace].noteSelect
  );
  const updatedNoteResponse = yield session.query(
    `${noteSelect} where id is "${id}"`
  );

  yield put(
    noteCompletedChanged(action.meta.namespace, updatedNoteResponse.data[0])
  );
}

function* noteHighlightHandler(action) {
  if (action.payload.noteId) {
    // Reset note highlight.
    yield call(delay, 1000);
    yield put(noteHighlight(action.meta.namespace, null));
  }
}

/** Handle NOTES_LOAD action.
 *
 * Cancel concurrent note load tasks if they are in the same namespace.
 */
export function* notesLoadSaga() {
  const tasks = {};
  // eslint-disable-next-line no-constant-condition
  while (true) {
    const action = yield take(actions.NOTES_LOAD);
    const task = tasks[action.meta.namespace];
    if (task) {
      yield cancel(task);
    }
    tasks[action.meta.namespace] = yield fork(loadNotesHandler, action);
  }
}

/** Handle NOTES_RELOAD action. */
export function* notesReloadSaga() {
  yield takeEvery(actions.NOTES_RELOAD, catchAllErrors(reloadNotesHandler));
}

/** Handle NOTES_LOAD action.
 *
 * Cancel concurrent note load tasks if they are in the same namespace.
 */
export function* notesLoadNextPageSaga() {
  const tasks = {};
  // eslint-disable-next-line no-constant-condition
  while (true) {
    const action = yield take(actions.NOTES_LOAD_NEXT_PAGE);
    const task = tasks[action.meta.namespace];
    if (task) {
      yield cancel(task);
    }
    tasks[action.meta.namespace] = yield fork(loadNotesHandler, action);
  }
}

/** Handle SUBMIT_NOTE_FORM action. */
export function* noteSubmitSaga() {
  yield takeEvery(actions.SUBMIT_NOTE, catchAllErrors(submitNoteHandler));
}

/** Handle SUBMIT_NOTE_FORM action. */
export function* noteFormSubmitSaga() {
  yield takeEvery(actions.SUBMIT_NOTE_FORM, submitNoteFormHandler);
}

/** Handle REMOVE_NOTE action. */
export function* noteRemoveSaga() {
  yield takeEvery(actions.REMOVE_NOTE, removeNoteHandler);
}

/** Handle REMOVE_NOTE action. */
export function* noteTodoChangeSaga() {
  yield takeEvery(actions.NOTE_TODO_COMPLETED_CHANGE, changeTodoHandler);
}

/** Handle NOTE_HIGHLIGHT action. */
export function* noteHighlightSaga() {
  yield takeEvery(actions.NOTE_HIGHLIGHT, noteHighlightHandler);
}

export function* notesLoadSingleSaga() {
  yield takeEvery(actions.NOTES_LOAD_SINGLE, notesLoadSingleHandler);
}

export const sagas = [
  notesLoadSaga,
  notesReloadSaga,
  notesLoadNextPageSaga,
  noteSubmitSaga,
  noteFormSubmitSaga,
  noteRemoveSaga,
  noteTodoChangeSaga,
  noteHighlightSaga,
  notesLoadSingleSaga,
];

function editableNoteStateToProps() {
  return (multiState, props) => {
    const state = multiState.notes[props.namespace];
    const forms = (state && state.forms) || {};
    const pendingTodos = (state && state.pendingTodos) || {};
    const form = forms[`edit-${props.note.id}`] || {};
    const pendingTodo = pendingTodos[props.note.id] || false;

    return {
      content: form.content || props.note.content,
      recipients: form.recipients || props.note.recipients,
      pending: form.state === "pending",
      pendingTodo,
      collapsed: form.state === undefined || form.state === "hidden",
    };
  };
}

function editableNoteDispatchToProps() {
  return (dispatch, props) => {
    const formKey = `edit-${props.note.id}`;
    return {
      onShowForm: () => dispatch(openNoteForm(props.namespace, formKey, {})),
      onHideForm: ({ content }) =>
        dispatch(hideNoteForm(props.namespace, formKey, content)),
      onSubmitForm: ({ content, recipients, is_todo, labelIds }) => {
        const data = {
          content,
          recipients,
          labelIds,
          is_todo,
          id: props.note.id,
        };
        dispatch(submitNoteForm(props.namespace, formKey, data));
      },
      onRemove: () => dispatch(removeNote(props.namespace, props.note.id)),
      onTodoChange: () =>
        dispatch(
          noteCompletionChange(
            props.namespace,
            props.note.id,
            !props.note.completed_at
          )
        ),
    };
  };
}

const EditableNoteContainer = withSession(
  connect(editableNoteStateToProps, editableNoteDispatchToProps)(EditableNote)
);

function replyDispatchToProps() {
  return (dispatch, props) => {
    const { author, parentNote } = props;
    const formKey = `reply-${parentNote.id}`;
    return {
      onHideForm: ({ content }) =>
        dispatch(hideNoteForm(props.namespace, formKey, content)),
      onShowForm: () => dispatch(openNoteForm(props.namespace, formKey, {})),
      onSubmitForm: ({ content, recipients }) => {
        const data = {
          content,
          recipients,
          in_reply_to_id: parentNote.id,
          parent_id: parentNote.parent_id,
          parent_type: parentNote.parent_type,
        };
        dispatch(submitNoteForm(props.namespace, formKey, data, author));
      },
    };
  };
}

function replyStateToProps() {
  return (multiState, props) => {
    const state = multiState.notes[props.namespace];
    const forms = (state && state.forms) || {};
    const form = forms[`reply-${props.parentNote.id}`] || {};
    return {
      content: form.content || "",
      recipients: form.recipients || [],
      pending: form.state === "pending",
      collapsed: form.state === undefined || form.state === "hidden",
    };
  };
}

const ReplyFormContainer = connect(
  replyStateToProps,
  replyDispatchToProps
)(ReplyForm);

function newNoteStateToProps() {
  return (multiState, props) => {
    const state = multiState.notes[props.namespace];
    const forms = (state && state.forms) || {};
    const form = forms[`new-${props.entity.id}`] || {};
    const collapsed = !form.state || form.state === "hidden";

    return {
      autoFocus: !collapsed,
      content: form.content,
      recipients: form.recipients,
      pending: form.state === "pending",
      collapsed,
    };
  };
}

function newNoteDispatchToProps() {
  return (dispatch, props) => {
    const formKey = `new-${props.entity.id}`;
    const { author } = props;
    return {
      onExpand: () => dispatch(openNoteForm(props.namespace, formKey, {})),
      onClickOutside: ({ content, collapsed }) => {
        if (!collapsed) {
          dispatch(hideNoteForm(props.namespace, formKey, content));
        }
      },
      onSubmit: ({ content, entity, recipients }) => {
        const data = {
          content,
          recipients,
          parent_id: entity.id,
          parent_type: entity.type,
        };
        dispatch(submitNoteForm(props.namespace, formKey, data, author));
      },
    };
  };
}

const NewNoteFormContainer = connect(
  newNoteStateToProps,
  newNoteDispatchToProps
)(NoteForm);

function _NotesListEmptyText({ onActionButtonClick, hide, intl }) {
  const { formatMessage } = intl;

  if (hide) {
    return null;
  }

  const message = (
    <span>{formatMessage(messages["attribute-message-no-notes-found"])}</span>
  );
  const iconElement = <FontIcon value="message" />;
  return (
    <EmptyText
      className={style.empty}
      icon={iconElement}
      label={message}
      cover={false}
    />
  );
}

_NotesListEmptyText.propTypes = {
  onActionButtonClick: PropTypes.func,
  entity: PropTypes.shape({
    // eslint-disable-line
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
  }).isRequired,
  hide: PropTypes.bool,
  intl: intlShape.isRequired,
};

function mapEmptyTextStateToProps(multiState, props) {
  const state = multiState.notes[props.namespace];
  const forms = (state && state.forms) || {};
  const form = forms[`new-${props.entity.id}`] || {};
  const collapsed = !form.state || form.state === "hidden";

  return {
    hide: !collapsed,
  };
}

function mapEmptyTextDispatchToProps(dispatch, props) {
  const formKey = `new-${props.entity.id}`;
  return {
    onActionButtonClick: () =>
      dispatch(openNoteForm(props.namespace, formKey, {})),
  };
}

const NotesListEmptyText = connect(
  mapEmptyTextStateToProps,
  mapEmptyTextDispatchToProps
)(safeInjectIntl(_NotesListEmptyText));

class _EntityInformation extends Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const { data, onClick } = this.props;
    const { __entity_type__: type, id } = data;
    if (onClick) {
      onClick([{ id, type }]);
    }
  }

  render() {
    const { data, session, onClick, entityInformationTooltip, entity } =
      this.props;

    let thumbnailId;
    let thumbnailUrl;
    let link = [];
    let versionNumber = null;

    const { __entity_type__: type } = data;

    if (type === "ReviewSessionObject") {
      thumbnailId = data.asset_version.thumbnail_id;
      thumbnailUrl =
        data.asset_version.thumbnail_url &&
        data.asset_version.thumbnail_url.value;
      versionNumber = getVersionFromEntityNameIfPossible(data.version);
      const rsoName = versionNumber
        ? `${data.name} v${versionNumber}`
        : data.name;
      link = [
        {
          name: data.review_session.project.full_name,
          id: data.review_session.project_id,
        },
        { name: data.review_session.name, id: data.review_session_id },
        { name: rsoName, id: data.id },
      ];
    } else {
      thumbnailUrl = data.thumbnail_url && data.thumbnail_url.value;
      thumbnailId = data.thumbnail_id;
      link = data.link || [];
    }

    let url = thumbnailUrl;
    if (!url) {
      url = session.thumbnailUrl(thumbnailId, THUMBNAIL_SIZES.small);
    }

    const extraInformationClasses = classNames(
      style["extra-information"],
      onClick && style.clickable,
      {
        [style["extra-information-inline"]]: !entityInformationTooltip,
      }
    );

    const reviewSessionName = data.review_session && data.review_session.name;
    const entityName =
      (link[link.length - 1] && link[link.length - 1].name) || "Unknown entity";

    if (entityInformationTooltip) {
      const tooltip = (
        <div className={extraInformationClasses}>
          <EntityLink color="light" link={link} parent />
          <div
            className={style["thumbnail-with-tooltip"]}
            style={{ backgroundImage: `url('${url}')` }}
          />
        </div>
      );
      const entityInformationText = [];
      if (!versionNumber) {
        versionNumber = getVersionFromEntityNameIfPossible(entityName);
      }

      if (type === "ReviewSessionObject") {
        if (entity.type === "ReviewSessionObject") {
          entityInformationText.push(
            <FormattedMessage
              {...messages["top-entity-information-version-number"]}
              values={{ versionNumber }}
            />
          );
        } else if (entity.type === "ReviewSession") {
          entityInformationText.push(entityName);
        } else {
          entityInformationText.push(`${reviewSessionName}, ${entityName}`);
        }
      } else if (type === "AssetVersion") {
        if (["Asset", "AssetVersion"].includes(entity.type)) {
          entityInformationText.push(
            <FormattedMessage
              {...messages["top-entity-information-version-number"]}
              values={{ versionNumber }}
            />
          );
        } else {
          entityInformationText.push(entityName);
        }
      } else {
        entityInformationText.push(entityName);
      }

      return (
        <SpanWithToolTip
          onClick={this.handleClick}
          theme={tooltipTheme}
          tooltip={tooltip}
          tooltipDelay={ENTITY_INFORMATION_TOOLTIP_DELAY}
          tooltipPosition="top"
          className={style.onTooltip}
        >
          <FormattedMessage {...messages["top-entity-information"]} />{" "}
          <span className={style.tooltipSpan}>{entityInformationText}</span>
        </SpanWithToolTip>
      );
    }
    return (
      <div onClick={this.handleClick} className={extraInformationClasses}>
        <div
          className={style["thumbnail-without-tooltip"]}
          style={{ backgroundImage: `url('${url}')` }}
        />
        <EntityLink link={link} parent={false} />
      </div>
    );
  }
}

_EntityInformation.propTypes = {
  data: PropTypes.shape({
    thumbnail_id: PropTypes.string,
    link: PropTypes.array,
  }).isRequired,
  session: PropTypes.shape({
    thumbnailUrl: PropTypes.func.isRequired,
  }).isRequired,
  onClick: PropTypes.func,
  entityInformationTooltip: PropTypes.bool,
  entity: PropTypes.shape({
    id: PropTypes.string,
    type: PropTypes.string,
  }),
};

const EntityInformation = withSession(_EntityInformation);

// eslint-disable-next-line
class NoteThread_ extends Component {
  constructor() {
    super();

    this.handleRef = this.handleRef.bind(this);
    this.scrollNoteIntoViewIfNecessary =
      this.scrollNoteIntoViewIfNecessary.bind(this);
  }

  componentDidMount() {
    this.scrollNoteIntoViewIfNecessary(this.props);
  }

  componentDidUpdate(props) {
    if (this.props.highlightNoteId !== props.highlightNoteId) {
      this.scrollNoteIntoViewIfNecessary(this.props);
    }
  }

  handleRef(node) {
    this.node = node;
  }

  scrollNoteIntoViewIfNecessary({ note, highlightNoteId }) {
    const shouldComponentHighlight =
      highlightNoteId && note.id === highlightNoteId;
    if (shouldComponentHighlight && this.node) {
      scrollIntoViewIfNeeded(this.node, {
        block: "center",
        inline: "center",
        skipOverflowHiddenElements: "false",
        behavior: "smooth",
        scrollMode: "if-needed",
      });
    }
  }

  render() {
    const {
      note,
      onAttachmentClick,
      onFrameClick,
      author,
      entity,
      onEntityInformationClick,
      highlightNoteId,
      disableReply,
      className,
      entityInformationTooltip,
      colors,
      namespace,
      lastItem,
      canMention,
      showNoteLabelSelector,
      showCompletableNotes,
    } = this.props;

    // Add note components for each reply.
    const replies = (note.replies || []).map((reply) => (
      <EditableNoteContainer
        className={style.reply}
        entityInformationTooltip={entityInformationTooltip}
        onAttachmentClick={onAttachmentClick}
        onFrameClick={onFrameClick}
        note={reply}
        colors={colors}
        key={reply.id}
        author={author}
        namespace={namespace}
        canMention={canMention}
      />
    ));

    let entityInformation = null;
    if (note.parent_id !== entity.id && note.extraInformation) {
      entityInformation = (
        <EntityInformation
          data={note.extraInformation}
          onClick={onEntityInformationClick}
          entityInformationTooltip={entityInformationTooltip}
          entity={entity}
        />
      );
    }

    const parentNoteClass = classNames(style["parent-note-item"], {
      [style["highlight-note"]]: highlightNoteId && highlightNoteId === note.id,
    });

    const authorColor = note.author
      ? pickReviewColor(note.author, colors)
      : null;
    return (
      <div className={className} ref={this.handleRef}>
        <div
          className={parentNoteClass}
          style={{
            borderColor: authorColor,
          }}
        >
          <EditableNoteContainer
            entityInformationTooltip={entityInformationTooltip}
            entityInformation={entityInformation}
            onAttachmentClick={onAttachmentClick}
            onFrameClick={onFrameClick}
            note={note}
            author={author}
            onRef={this.onNoteRef}
            colors={colors}
            namespace={namespace}
            canMention={canMention}
            showNoteLabelSelector={showNoteLabelSelector}
            showCompletableNotes={showCompletableNotes}
          />
          {replies.length ? (
            <div className={style.replies}>{replies}</div>
          ) : null}
          {disableReply ? null : (
            <ReplyFormContainer
              key={`reply-note-form-${note.id}`}
              namespace={namespace}
              parentNote={note}
              author={author}
              canMention={canMention}
            />
          )}
        </div>
        {lastItem ? null : <hr className={style.noteSeparator} />}
      </div>
    );
  }
}
NoteThread_.propTypes = {
  className: PropTypes.string,
  note: PropTypes.object.isRequired, // eslint-disable-line
  entity: PropTypes.shape({
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
  }).isRequired,
  entitySelector: PropTypes.array, // eslint-disable-line
  author: PropTypes.shape({
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
  }).isRequired,
  onAttachmentClick: PropTypes.func,
  onFrameClick: PropTypes.func,
  onEntityInformationClick: PropTypes.func,
  disableReply: PropTypes.bool,
  highlightNoteId: PropTypes.bool,
  entityInformationTooltip: PropTypes.bool,
  colors: PropTypes.arrayOf(PropTypes.string),
  namespace: PropTypes.string,
  lastItem: PropTypes.bool,
  canMention: PropTypes.bool,
  showNoteLabelSelector: PropTypes.bool,
  showCompletableNotes: PropTypes.bool,
};

const NoteThread = connect((multiState, props) => ({
  highlightNoteId: multiState.notes[props.namespace].highlight,
}))(NoteThread_);

/** List an array of note *items* with support for editing and creating notes.
 *
 * The *entity* is the currently loaded entity, e.g. a task or a version. This
 * is used as parent when creating new notes.
 *
 * The *user* object is the active user and is used as author for any new notes.
 */
// eslint-disable-next-line
export class NotesList extends Component {
  componentDidMount() {
    const {
      entity,
      filters,
      sort,
      onLoad,
      onRef,
      pageSize,
      includeAllVersions,
      noteSelect,
    } = this.props;
    onLoad(
      entity,
      filters,
      sort,
      pageSize,
      includeAllVersions,
      noteSelect(this.props.session)
    );

    if (onRef) {
      onRef(this);
    }
  }

  componentWillReceiveProps({
    entity,
    sort,
    pageSize,
    filters,
    includeAllVersions,
    noteSelect,
  }) {
    if (
      entity.id !== this.props.entity.id ||
      sort !== this.props.sort ||
      filters !== this.props.filters ||
      includeAllVersions !== this.props.includeAllVersions
    ) {
      this.props.onLoad(
        entity,
        filters,
        sort,
        pageSize,
        includeAllVersions,
        noteSelect(this.props.session)
      );
    }
  }

  reload() {
    const { entity, sort, pageSize, filters, includeAllVersions, noteSelect } =
      this.props;
    this.props.onLoad(
      entity,
      filters,
      sort,
      pageSize,
      includeAllVersions,
      noteSelect(this.props.session)
    );
  }

  render() {
    const {
      items,
      entity,
      user,
      loading,
      nextOffset,
      onFetchMore,
      onAttachmentClick,
      disableReply,
      disableCreate,
      entitySelector,
      onEntityInformationClick,
      onFrameClick,
      pageSize,
      itemClassName,
      sort,
      colors,
      entityInformationTooltip,
      namespace,
      filters,
      noteSelect,
      includeAllVersions,
      canMention,
      showNoteLabelSelector,
      showCompletableNotes,
    } = this.props;
    logger.debug("Rendering notes", entity, user);

    const notes = [];
    items.forEach((note, index) => {
      // Add primary note component.
      notes.push(
        <NoteThread
          key={note.id}
          onAttachmentClick={onAttachmentClick}
          onEntityInformationClick={onEntityInformationClick}
          onFrameClick={onFrameClick}
          note={note}
          entity={entity}
          author={user}
          disableReply={disableReply}
          className={itemClassName}
          entityInformationTooltip={entityInformationTooltip}
          colors={colors}
          namespace={namespace}
          includeAllVersions={includeAllVersions}
          lastItem={index === items.length - 1}
          canMention={canMention}
          showCompletableNotes={showCompletableNotes}
          showNoteLabelSelector={showNoteLabelSelector}
        />
      );
    });

    const content = [];

    if (!disableCreate) {
      content.push(
        <NewNoteFormContainer
          key={`new-note-form-${entity.id}`}
          className={style["new-note-form"]}
          entity={entity}
          entitySelector={entitySelector}
          author={user}
          namespace={namespace}
          canMention={canMention}
        />
      );
    }

    if (notes.length) {
      content.push(
        <div key="note-list-inner" className={style["note-list-inner"]}>
          <FlipMove duration={TRANSITION_DURATION} leaveAnimation="fade">
            {notes}
          </FlipMove>
        </div>
      );
    }

    if (loading) {
      // Add loading indicator.
      content.push(<NoteSkeleton key="loading-skeleton" />);
    }

    if (!loading && !items.length) {
      content.push(
        <NotesListEmptyText
          key="notes-list-empty-state"
          entity={entity}
          disableCallToAction={disableCreate}
        />
      );
    }

    if (loading === false && nextOffset !== null && items.length) {
      // Only add way point if not loading and there are more items to load.
      notes.push(
        <Waypoint
          key="notes-list-waypoint"
          onEnter={() =>
            onFetchMore(entity, filters, sort, pageSize, nextOffset, noteSelect)
          }
          bottomOffset={-100}
        />
      );
    }

    return <div className={style["note-list"]}>{content}</div>;
  }
}

NotesList.propTypes = {
  items: PropTypes.array.isRequired, // eslint-disable-line
  entity: PropTypes.shape({
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
  }).isRequired,
  entitySelector: PropTypes.array, // eslint-disable-line
  user: PropTypes.shape({
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
  }).isRequired,
  loading: PropTypes.bool,
  nextOffset: PropTypes.shape({
    date: PropTypes.object.isRequired, // eslint-disable-line
    id: PropTypes.string.isRequired,
  }),
  onFetchMore: PropTypes.func,
  session: PropTypes.object,
  onLoad: PropTypes.func,
  onAttachmentClick: PropTypes.func,
  onFrameClick: PropTypes.func,
  onEntityInformationClick: PropTypes.func,
  disableReply: PropTypes.bool,
  disableCreate: PropTypes.bool,
  canMention: PropTypes.bool,
  showNoteLabelSelector: PropTypes.bool,
  showCompletableNotes: PropTypes.bool,
  pageSize: PropTypes.number.isRequired,
  itemClassName: PropTypes.string,
  namespace: PropTypes.string,
  sort: PropTypes.string,
  filters: PropTypes.string,
  onRef: PropTypes.func,
  noteSelect: PropTypes.func,
  entityInformationTooltip: PropTypes.bool,
  colors: PropTypes.arrayOf(PropTypes.string),
  includeAllVersions: PropTypes.bool,
};

NotesList.defaultProps = {
  disableReply: false,
  disableCreate: false,
  canMention: false,
  showNoteLabelSelector: false,
  showCompletableNotes: false,
  noteSelect(session) {
    const projections = [...DEFAULT_SELECT];
    const userSchema = session.schemas.find((schema) => schema.id === "User");
    if (userSchema.properties.email) {
      // Backend support for User.email in collaborator API is added in
      // 4.2.
      projections.push("author.email");
    }

    if (session.schemas.some((schema) => schema.id === "NoteLabel")) {
      // Backend support for NoteLabel in collaborator API is added in
      // 4.3.X
      projections.push(
        "note_label_links.label.name",
        "note_label_links.label.color"
      );
    } else {
      projections.push("category.name", "category.color");
    }

    // Add same attributes but with replies prefix to load the same data on
    // replies.
    projections.push(...projections.map((attribute) => `replies.${attribute}`));

    return `select ${projections.join(", ")} from Note`;
  },
  entitySelector: null,
  highlight: null,
  sort: "date",
  pageSize: 10,
  namespace: DEFAULT_NAMESPACE,
  includeAllVersions: false,
};

export const mapStateToProps = (multiState, props) => {
  const state = multiState.notes[props.namespace || DEFAULT_NAMESPACE];
  const items = (state && state.items) || [];
  const loading = (state && state.loading) || false;
  const nextOffset = state && state.nextOffset;

  return {
    items,
    nextOffset,
    loading,
  };
};

export const mapDispatchToProps = (dispatch, ownProps) => ({
  onFetchMore: (entity, filters, sort, limit, nextOffset) => {
    dispatch(
      notesLoadNextPage(
        ownProps.namespace || DEFAULT_NAMESPACE,
        entity.id,
        entity.type,
        filters,
        sort,
        limit,
        nextOffset
      )
    );
  },
  onLoad: (entity, filters, sort, limit, includeAllVersions, noteSelect) => {
    dispatch(
      notesLoad(
        ownProps.namespace || DEFAULT_NAMESPACE,
        entity.id,
        entity.type,
        filters,
        sort,
        limit,
        includeAllVersions,
        noteSelect
      )
    );
  },
});

export default withSession(
  connect(mapStateToProps, mapDispatchToProps)(NotesList)
);
