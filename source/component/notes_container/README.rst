..
    :copyright: Copyright (c) 2018 ftrack

###############
Notes container
###############

A component that can be used to load and list notes from an entity, create new
notes or reply to or edit existing notes. When the component is mounted it will
automatically fetch the notes.

The component requies that there is an ftrack API javascript session in the
react context. This can be accomplished by using the SparkProvider::


        import { SparkProvider } from 'ftrack-spark-components/util/hoc';

        ...

        // Create spark provider with javascript session.
        <SparkProvider session={session}>
            ...
        </SparkProvider>

Redux and sagas are used by the component to fetch data and keep the state. The
saga middleware must be created with a ftrack javascript api session as ftrackSession
in the context::

    # Create saga middleware with ftrackSession in the context.
    const sagaMiddleware = createSagaMiddleware({
        context: {
            ftrackSession: session,
        },
    });

The component has the following dependencies:

    * react-redux ^5.0.6
    * redux-saga ^0.16.0
    * moment ^2.20.
    * loglevel ^1.4.1
    * ftrack-javascript-api git+https://git@bitbucket.org/ftrack/ftrack-javascript-api.git#0.4.5

The following props are supported:

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
entity                         The ftrack entity to load. An object with id and type.
user                           The ftrack that is the currently active user to be the author of new notes.
onAttachmentClick              Callback when an attachment is clicked.
onEntityInformationClick       Callback when extra entity information is clicked.
disableReply       false       Disable reply forms.
disableCreate      false       Disable create note form.
onRef                          Callback to get the component reference.

The following methods are exposed if onRef callback is used:

================== =========== ======================================
Method               Default     Description
================== =========== ======================================
reload                         Reload the notes data.
