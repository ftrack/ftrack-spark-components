import { Component } from "react";
import { connect } from "react-redux";
import { combineReducers } from "redux";

import { action } from "@storybook/addon-actions";
// eslint-disable-next-line
import { IntlProvider } from "react-intl";

import { Provider as ReduxProvider } from "react-redux";
import Button from "react-toolbox/lib/button";

import configureStore from "../../story/configure_store.js";
import getSession from "../../story/get_session";
import { SparkProvider } from "../util/hoc";
import NotesListView, {
  NotesListCompact,
  reducer as noteReducer,
  sagas as noteSagas,
  noteHighlight,
} from ".";

import messages from "../../story/en-TEST.json";
import { compose, withProps } from "recompose";

function Provider({ story, store, session }) {
  if (!session.initialized) {
    return <span>API Session not ready</span>;
  }

  return (
    <SparkProvider session={session}>
      <ReduxProvider store={store}>{story}</ReduxProvider>
    </SparkProvider>
  );
}

const store = configureStore(
  combineReducers({ notes: noteReducer }),
  noteSagas,
  getSession()
);

const SizeDecorator = (storyFn) => (
  <div
    style={{
      overflow: "auto",
      position: "fixed",
      top: "0px",
      left: "0px",
      bottom: "0px",
      right: "0px",
      display: "flex",
      justifyContent: "center",
    }}
  >
    <div style={{ margin: "100px", width: "480px" }}>{storyFn()}</div>
  </div>
);

export default {
  title: "Notes container",
  decorators: [
    (Story) => (
      <Provider story={<Story />} store={store} session={getSession()} />
    ),
    SizeDecorator,
  ],
};

const entityInformation = action("Entity information clicked");
const frame = action("Frame clicked");

const NotesListViewDefault = compose(
  withProps({
    onEntityInformationClick: entityInformation,
    onAttachmentClick: action("Attachment clicked"),
    onFrameClick: frame,
  })
)(NotesListView);

const NoteSelector = connect(
  (state) => ({
    notes:
      (state.notes && state.notes.default && state.notes.default.items) || [],
  }),
  (dispatch) => ({
    onClick(noteId) {
      dispatch(noteHighlight("default", noteId));
    },
  })
)(({ notes, onClick }) => (
  <div style={{ width: "100px" }}>
    Click to highlight:
    {notes &&
      notes.map(({ id }, index) => (
        <div onClick={() => onClick(id)}>{index}</div>
      ))}
  </div>
));

const author = { id: "bcdf57b0-acc6-11e1-a554-f23c91df1211" };

export const NotesListCompactOnTask = () => (
  <div style={{ background: "#232a2e", padding: "1rem" }}>
    <NotesListCompact
      user={author}
      entity={{
        id: "19c727a0-3906-11e9-9ef3-af9c3e0b3fd3",
        type: "AssetVersion",
      }}
    />
  </div>
);

NotesListCompactOnTask.storyName = "NotesListCompact on task";

NotesListCompactOnTask.parameters = {
  info: "This example shows a list of notes fetched from a task.",
};

export const NotesOnTask = () => (
  <NotesListViewDefault
    user={author}
    entity={{
      id: "87ae9d38-c4dd-11e1-bf78-f23c91df25eb",
      type: "TypedContext",
    }}
  />
);

NotesOnTask.storyName = "Notes on task";

NotesOnTask.parameters = {
  info: "This example shows a list of notes fetched from a task.",
};

export const TestLocalization = () => (
  <IntlProvider locale="zh-CN" messages={messages}>
    <NotesListViewDefault
      user={author}
      entity={{
        id: "87ae9d38-c4dd-11e1-bf78-f23c91df25eb",
        type: "TypedContext",
      }}
    />
  </IntlProvider>
);

TestLocalization.storyName = "Test localization";

TestLocalization.parameters = {
  info: "This example shows a list of notes fetched from a task.",
};

export const NotesNoReplyOrCreate = () => (
  <NotesListViewDefault
    user={author}
    disableReply
    disableCreate
    entity={{
      id: "87ae9d38-c4dd-11e1-bf78-f23c91df25eb",
      type: "TypedContext",
    }}
  />
);

NotesNoReplyOrCreate.storyName = "Notes no reply or create";

NotesNoReplyOrCreate.parameters = {
  info: "This example shows a list of notes fetched from a task with reply and create disabled.",
};

export const NotesOnProject = () => (
  <div style={{ padding: "3.2rem", width: "38.3rem" }}>
    <NotesListViewDefault
      entityInformationTooltip
      user={author}
      entity={{
        id: "bd8be680-fc54-11e3-8b16-04011030cf01",
        type: "Project",
      }}
    />
  </div>
);

NotesOnProject.storyName = "Notes on project";

NotesOnProject.parameters = {
  info: "This example shows a list of notes fetched from a project.",
};

export const NotesWithoutRepliesUsingFilters = () => (
  <div style={{ padding: "3.2rem", width: "38.3rem" }}>
    <NotesListViewDefault
      user={author}
      entity={{
        id: "bd8be680-fc54-11e3-8b16-04011030cf01",
        type: "Project",
      }}
      filters="not replies any ()"
    />
  </div>
);

NotesWithoutRepliesUsingFilters.storyName =
  "Notes without replies (using filters)";

NotesWithoutRepliesUsingFilters.parameters = {
  info: "This example shows a list of notes fetched from a project with filters.",
};

export const NotesWithLabels = () => (
  <div style={{ padding: "3.2rem", width: "38.3rem" }}>
    <NotesListViewDefault
      user={author}
      category
      entity={{
        id: "bd8be680-fc54-11e3-8b16-04011030cf01",
        type: "Project",
      }}
      filters="not category_id is None"
    />
  </div>
);

NotesWithLabels.storyName = "Notes with labels";

NotesWithLabels.parameters = {
  info: "This example shows a list of notes that has labels.",
};

export const _2NamespacedNoteWidgets = () => (
  <div style={{ padding: "3.2rem", width: "100.0rem", display: "flex" }}>
    <NotesListViewDefault
      user={author}
      namespace="one"
      entity={{
        id: "bd8be680-fc54-11e3-8b16-04011030cf01",
        type: "Project",
      }}
    />
    <NotesListViewDefault
      user={author}
      namespace="two"
      entity={{
        id: "35604cac-679e-11e7-bd87-0a580ae40a16",
        type: "Project",
      }}
    />
  </div>
);

_2NamespacedNoteWidgets.storyName = "2 namespaced note widgets";

_2NamespacedNoteWidgets.parameters = {
  info: "This example shows a list of notes fetched from a project.",
};

export const NotesOnProject25PageSize = () => (
  <div style={{ padding: "3.2rem", width: "38.3rem" }}>
    <NotesListViewDefault
      user={author}
      pageSize={25}
      entity={{
        id: "bd8be680-fc54-11e3-8b16-04011030cf01",
        type: "Project",
      }}
    />
  </div>
);

NotesOnProject25PageSize.storyName = "Notes on project (25 page size)";

NotesOnProject25PageSize.parameters = {
  info: "This example shows a list of notes fetched from a project.",
};

export const NotesOnTypedContext = () => (
  <NotesListViewDefault
    user={author}
    entity={{
      id: "791bc4e2-fc55-11e3-9176-04011030cf01",
      type: "TypedContext",
    }}
  />
);

NotesOnTypedContext.storyName = "Notes on typed context";

NotesOnTypedContext.parameters = {
  info: "This example shows a list of notes fetched from a typed context.",
};

export const NotesOnAssetVersion = () => (
  <NotesListViewDefault
    entityInformationTooltip
    user={author}
    entity={{
      id: "d1996d40-11ae-11e4-8e9e-04011030cf01",
      type: "AssetVersion",
    }}
  />
);

NotesOnAssetVersion.storyName = "Notes on asset version";

NotesOnAssetVersion.parameters = {
  info: "This example shows a list of notes fetched from a asset version.",
};

export const NotesOnReviewSessionObject = () => (
  <NotesListViewDefault
    entityInformationTooltip
    user={author}
    entity={{
      id: "2d60c9e1-39c1-11e9-88ad-bf9eeb423744",
      type: "ReviewSessionObject",
    }}
    sort="date"
  />
);

NotesOnReviewSessionObject.storyName = "Notes on review session object";

NotesOnReviewSessionObject.parameters = {
  info: "This example shows a list of notes fetched from a review session object.",
};

export const NotesOnReviewSessionObjectOrderByFrame = () => (
  <NotesListViewDefault
    user={author}
    entity={{
      id: "c8c740a2-7860-11e4-8e7d-040132734d01",
      type: "ReviewSessionObject",
    }}
    sort="frame"
  />
);

NotesOnReviewSessionObjectOrderByFrame.storyName =
  "Notes on review session object order by frame";

NotesOnReviewSessionObjectOrderByFrame.parameters = {
  info: "This example shows an ordered list of notes fetched from a review session object.",
};

export const NotesOnReviewSessionObjectHighlight = () => (
  <div style={{ display: "flex" }}>
    <NoteSelector />
    <NotesListViewDefault
      entityInformationTooltip
      user={author}
      entity={{
        id: "c8c740a2-7860-11e4-8e7d-040132734d01",
        type: "ReviewSessionObject",
      }}
      sort="date"
    />
  </div>
);

NotesOnReviewSessionObjectHighlight.storyName =
  "Notes on review session object highlight";

NotesOnReviewSessionObjectHighlight.parameters = {
  info: "This example shows a list of notes fetched from a review session object highkight.",
};

export const NotesOnReviewSession = () => (
  <NotesListViewDefault
    user={author}
    entity={{
      id: "7f0f8932-2c0c-4c02-a386-5515a99fe3bf",
      type: "ReviewSession",
    }}
  />
);

NotesOnReviewSession.storyName = "Notes on review session";

NotesOnReviewSession.parameters = {
  info: "This example shows a list of notes fetched from a review session.",
};

export const NotesOnReviewSessionWithTooltip = () => (
  <NotesListViewDefault
    user={author}
    entityInformation=""
    entityInformationTooltip
    entity={{
      id: "28642919-4fdf-4a6b-9d28-7f7458a256b9",
      type: "ReviewSession",
    }}
  />
);

NotesOnReviewSessionWithTooltip.storyName =
  "Notes on review session with tooltip";

NotesOnReviewSessionWithTooltip.parameters = {
  info: "This example shows a list of notes fetched from a review session.",
};

export const NotesEmpty = () => (
  <NotesListViewDefault
    user={author}
    entity={{
      id: "ae7f71aa-b3ce-11e1-8ed3-f23c91df25eb",
      type: "TypedContext",
    }}
  />
);

NotesEmpty.storyName = "Notes empty";

NotesEmpty.parameters = {
  info: "This example shows a list of notes with empty state.",
};

export const NotesEmptyDisableCreate = () => (
  <NotesListViewDefault
    user={author}
    entity={{
      id: "ae7f71aa-b3ce-11e1-8ed3-f23c91df25eb",
      type: "TypedContext",
    }}
    disableCreate
  />
);

NotesEmptyDisableCreate.storyName = "Notes empty disable create";

NotesEmptyDisableCreate.parameters = {
  info: "This example shows a list of notes with empty state where create is diabled.",
};

class BordersWithColors extends Component {
  render() {
    const colors = [
      "#EF5B5B",
      "#20A39E",
      "#FFBA49",
      "#824C71",
      "#90AA86",
      "#77B6EA",
      "#D6C9C9",
    ];
    return (
      <div>
        <NotesListViewDefault
          colors={colors}
          user={author}
          entity={{
            id: "5e414986-67c2-11e7-94e7-0a580ae40a16",
            type: "ReviewSession",
          }}
        />
      </div>
    );
  }
}

export const NotesWithAuthorColorOnLeftBorder = () => <BordersWithColors />;

NotesWithAuthorColorOnLeftBorder.storyName =
  "Notes with Author color on left border";

NotesWithAuthorColorOnLeftBorder.parameters = {
  info: "This example shows a list of notes fetched from a review session.",
};

class ReloadableNotesListViewDefault extends Component {
  render() {
    return (
      <div>
        <Button
          label="Reload"
          onClick={() => {
            this.notes.reload();
          }}
        />
        <NotesListViewDefault
          user={author}
          onRef={(node) => {
            this.notes = node;
          }}
          entity={{
            id: "bd8be680-fc54-11e3-8b16-04011030cf01",
            type: "Project",
          }}
          disableCreate
        />
      </div>
    );
  }
}

export const NotesReload = () => <ReloadableNotesListViewDefault />;

NotesReload.storyName = "Notes reload";

NotesReload.parameters = {
  info: "This example shows a list of notes that can be reloaded.",
};

class SwitchEntityNotesListViewDefault extends Component {
  constructor() {
    super();

    this.state = {
      entities: [
        {
          id: "d1996d40-11ae-11e4-8e9e-04011030cf01",
          type: "AssetVersion",
        },
        {
          id: "791bc4e2-fc55-11e3-9176-04011030cf01",
          type: "TypedContext",
        },
      ],
      index: 0,
    };
  }

  render() {
    const { index, entities } = this.state;
    return (
      <div>
        <Button label="Entity A" onClick={() => this.setState({ index: 0 })} />
        <Button label="Entity B" onClick={() => this.setState({ index: 1 })} />
        <NotesListViewDefault user={author} entity={entities[index]} />
      </div>
    );
  }
}

export const NotesSwitchEntity = () => <SwitchEntityNotesListViewDefault />;

NotesSwitchEntity.storyName = "Notes switch entity";

NotesSwitchEntity.parameters = {
  info: "This example shows a list of notes and switching between entities.",
};
