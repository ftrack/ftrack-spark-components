// :copyright: Copyright (c) 2016 ftrack

import loglevel from "loglevel";
import { Component } from "react";
import { defineMessages, intlShape } from "react-intl";
import { connect } from "react-redux";
import FontIcon from "react-toolbox/lib/font_icon";
import FlipMove from "react-flip-move";
import PropTypes from "prop-types";
import classNames from "classnames";

import EmptyText from "../empty_text";
import { TRANSITION_DURATION } from "../util/constant";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import NoteSkeleton from "./skeleton";
import style from "./style.scss";
import NoteCompact from "../note/note_compact";
import { mapStateToProps, notesLoad, noteCompletionChange } from "./container";

const DEFAULT_NAMESPACE = "default";
const COMPACT_SELECT = [
  "id",
  "content",
  "author.first_name",
  "author.last_name",
  "parent_id",
  "parent_type",
  "date",
  "metadata.key",
  "metadata.value",
  "completed_at",
  "is_todo",
  "thread_activity",
];
const logger = loglevel.getLogger("saga:note");

const messages = defineMessages({
  "attribute-message-no-notes-found": {
    id: "ftrack-spark-components-notes-container-attribute-message-no-notes-found",
    defaultMessage: "There is no feedback yet",
  },
});

function _NotesListEmptyText({ hide, intl }) {
  const { formatMessage } = intl;

  if (hide) {
    return null;
  }

  const message = (
    <span>{formatMessage(messages["attribute-message-no-notes-found"])}</span>
  );
  const iconElement = <FontIcon value="message" />;
  return (
    <EmptyText
      className={classNames(style.empty)}
      icon={iconElement}
      label={message}
      cover={false}
    />
  );
}

_NotesListEmptyText.propTypes = {
  entity: PropTypes.shape({
    // eslint-disable-line
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
  }).isRequired,
  hide: PropTypes.bool,
  intl: intlShape.isRequired,
};

function mapEmptyTextStateToProps(multiState, props) {
  const state = multiState.notes[props.namespace];
  const forms = (state && state.forms) || {};
  const form = forms[`new-${props.entity.id}`] || {};
  const collapsed = !form.state || form.state === "hidden";

  return {
    hide: !collapsed,
  };
}

const NotesListEmptyText = connect(mapEmptyTextStateToProps)(
  safeInjectIntl(_NotesListEmptyText)
);

// eslint-disable-next-line
export class NotesListCompact extends Component {
  componentDidMount() {
    const { entity, filters, sort, onLoad, onRef, pageSize, noteSelect } =
      this.props;
    onLoad(entity, filters, sort, pageSize, noteSelect());

    if (onRef) {
      onRef(this);
    }
  }

  componentWillReceiveProps({ entity, sort, pageSize, filters, noteSelect }) {
    if (
      entity.id !== this.props.entity.id ||
      sort !== this.props.sort ||
      filters !== this.props.filters
    ) {
      this.props.onLoad(entity, filters, sort, pageSize, noteSelect());
    }
  }

  reload() {
    const { entity, sort, pageSize, filters, noteSelect } = this.props;
    this.props.onLoad(entity, filters, sort, pageSize, noteSelect());
  }

  render() {
    const { items, entity, user, loading, disableCreate } = this.props;
    logger.debug("Rendering notes", entity, user);

    const notes = [];
    items.forEach((note) => {
      // Add primary note component.
      notes.push(
        <NoteCompact
          key={note.id}
          completedAt={note.completed_at}
          onTodoChange={() =>
            this.props.onTodoChange(note.id, note.completed_at)
          }
          data={{ ...note, ...user }}
        />
      );
    });

    const content = [];

    if (notes.length) {
      content.push(
        <div key="note-list-inner">
          <FlipMove duration={TRANSITION_DURATION} leaveAnimation="fade">
            {notes}
          </FlipMove>
        </div>
      );
    }

    if (loading) {
      // Add loading indicator.
      content.push(<NoteSkeleton key="loading-skeleton" />);
    }

    if (!loading && !items.length) {
      content.push(
        <NotesListEmptyText
          key="notes-list-empty-state"
          entity={entity}
          disableCallToAction={disableCreate}
        />
      );
    }

    return <div className={style["note-list-compact"]}>{content}</div>;
  }
}

NotesListCompact.propTypes = {
  items: PropTypes.array.isRequired, // eslint-disable-line
  entity: PropTypes.shape({
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
  }).isRequired,
  entitySelector: PropTypes.array, // eslint-disable-line
  user: PropTypes.shape({
    id: PropTypes.string.isRequired,
    type: PropTypes.string,
  }).isRequired,
  loading: PropTypes.bool,
  nextOffset: PropTypes.shape({
    date: PropTypes.object.isRequired, // eslint-disable-line
    id: PropTypes.string.isRequired,
  }),
  onLoad: PropTypes.func,
  disableCreate: PropTypes.bool,
  includeReplies: PropTypes.bool,
  pageSize: PropTypes.number.isRequired,
  namespace: PropTypes.string,
  sort: PropTypes.string,
  filters: PropTypes.string,
  onRef: PropTypes.func,
  noteSelect: PropTypes.func,
  onTodoChange: PropTypes.func.isRequired,
  intl: intlShape.isRequired,
};

NotesListCompact.defaultProps = {
  disableCreate: false,
  includeReplies: false,
  entitySelector: null,
  highlight: null,
  sort: "date",
  noteSelect() {
    const projections = [...COMPACT_SELECT];
    return `select ${projections.join(", ")} from Note`;
  },
  pageSize: 5,
  namespace: DEFAULT_NAMESPACE,
};

export const mapDispatchToProps = (dispatch, ownProps) => ({
  onLoad: (entity, filters, sort, limit, noteSelect) => {
    dispatch(
      notesLoad(
        ownProps.namespace || DEFAULT_NAMESPACE,
        entity.id,
        entity.type,
        filters,
        sort,
        limit,
        false,
        noteSelect
      )
    );
  },
  onTodoChange: (noteId, completedAt) =>
    dispatch(noteCompletionChange(ownProps.namespace, noteId, !completedAt)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(safeInjectIntl(NotesListCompact));
