// :copyright: Copyright (c) 2019 ftrack

import PropTypes from "prop-types";
import classNames from "classnames";
import FontIcon from "react-toolbox/lib/font_icon";
import ProgressBar from "react-toolbox/lib/progress_bar";
import style from "./style.scss";

function AttachmentItem({
  className,
  thumbnailUrl,
  children,
  progress,
  ...props
}) {
  const classes = classNames(style.attachment, className);
  const imageStyle = thumbnailUrl
    ? { backgroundImage: `url('${thumbnailUrl}')` }
    : null;
  return (
    <li className={classes} style={imageStyle} {...props}>
      <p className={style.name}>{children}</p>
      <span className={style.icon}>
        <FontIcon value="delete" style={{ fontSize: "16px" }} />
      </span>
      {progress != null && progress < 100 ? (
        <span className={style.spinnerWrapper}>
          <ProgressBar
            className={style.spinner}
            type="circular"
            mode={progress > 0 ? "determinate" : "indeterminate"}
            value={progress}
          />
        </span>
      ) : null}
    </li>
  );
}

AttachmentItem.propTypes = {
  onClick: PropTypes.func,
  className: PropTypes.string,
  thumbnailUrl: PropTypes.string,
  children: PropTypes.node,
  progress: PropTypes.number,
};

export default AttachmentItem;
