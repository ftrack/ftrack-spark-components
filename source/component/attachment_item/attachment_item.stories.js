// :copyright: Copyright (c) 2019 ftrack

import centered from "@storybook/addon-centered/react";
import AttachmentItem from ".";

export default {
  title: "Attachment Item",
  decorators: [centered],
};

const image =
  "https://images.unsplash.com/photo-1551334787-21e6bd3ab135?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1651&q=80";

export const AttachmentFileCard = () => (
  <div>
    <AttachmentItem progress={50}>Attachment File</AttachmentItem>
    <AttachmentItem>File number with long name</AttachmentItem>
    <AttachmentItem>Attachment File</AttachmentItem>
    <AttachmentItem thumbnailUrl={image} />
  </div>
);

AttachmentFileCard.storyName = "Attachment file card";
