..
    :copyright: Copyright (c) 2017 ftrack

#############
Entity Avatar
#############

Displays *entity* (user, invitee, context) as an Avatar.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
className                      CSS Class to add to the root element.
session                        ftrack JS API session instance.
entity             {}          API entity instance
