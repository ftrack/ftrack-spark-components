import centered from "@storybook/addon-centered/react";

import EntityAvatar from ".";
import getTestSession from "../../story/get_session";

const session = getTestSession();

export default {
  title: "Entity avatar",
  decorators: [centered],
};

const TEST_COMPONENT_ID = "5c49841e-c86a-11e7-a604-3c0754289fd3";

export const NoEntity = () => <EntityAvatar session={session} />;

NoEntity.storyName = "No entity";

export const UserWithThumbnail = () => (
  <EntityAvatar
    session={session}
    entity={{
      thumbnail_id: TEST_COMPONENT_ID,
      first_name: "Mattias",
      last_name: "Lagergren",
    }}
  />
);

UserWithThumbnail.storyName = "User with thumbnail";

export const UserWithoutThumbnailSmall = () => (
  <EntityAvatar
    session={session}
    size="small"
    entity={{
      thumbnail_id: null,
      first_name: "Mattias",
      last_name: "Lagergren",
    }}
  />
);

UserWithoutThumbnailSmall.storyName = "User without thumbnail small";

export const UserWithoutThumbnailMedium = () => (
  <EntityAvatar
    session={session}
    size="medium"
    entity={{
      thumbnail_id: null,
      first_name: "Mattias",
      last_name: "Lagergren",
    }}
  />
);

UserWithoutThumbnailMedium.storyName = "User without thumbnail medium";

export const UserWithoutThumbnailLarge = () => (
  <EntityAvatar
    session={session}
    size="large"
    entity={{
      thumbnail_id: null,
      first_name: "Mattias",
      last_name: "Lagergren",
    }}
  />
);

UserWithoutThumbnailLarge.storyName = "User without thumbnail large";

export const Invitee = () => (
  <EntityAvatar
    session={session}
    entity={{
      name: "John doe",
    }}
  />
);

export const Project = () => (
  <EntityAvatar
    session={session}
    entity={{
      full_name: "A test project",
      name: "a_test_project",
    }}
  />
);

export const Task = () => (
  <EntityAvatar
    session={session}
    entity={{
      name: "Compositing",
    }}
  />
);
