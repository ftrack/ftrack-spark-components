// :copyright: Copyright (c) 2017 ftrack
import PropTypes from "prop-types";

import classNames from "classnames";
import Avatar from "@mui/material/Avatar";
import Tooltip from "@mui/material/Tooltip";
import { THUMBNAIL_SIZES } from "../util/constant";
/**
 * EntityAvatar component - displays *entity* (user, invitee, context) as an Avatar.
 */

function EntityAvatar({ session, entity, className, size, color, tooltip }) {
  const _classNames = classNames(className);

  let url = null;
  if (entity?.thumbnail_id) {
    url = entity.thumbnail_url && entity.thumbnail_url.value;
    if (!url) {
      url = session.thumbnailUrl(entity.thumbnail_id, THUMBNAIL_SIZES.small);
    }
  }

  let title = null;
  if (entity?.full_name) {
    title = entity.full_name;
  } else if (entity?.name) {
    title = entity.name;
  } else if (entity?.first_name) {
    title = entity.first_name;
    if (entity?.last_name) {
      title = `${title} ${entity.last_name}`;
    }
  }
  const tooltipProp = tooltip === true ? title : tooltip;

  return (
    <Tooltip title={tooltipProp || ""}>
      <Avatar
        className={_classNames}
        size={size}
        sx={
          color
            ? {
                backgroundColor: `${color}77`,
                borderColor: `${color}`,
              }
            : { border: "none" }
        }
        alt={title}
        src={url}
      >
        {title ? title.charAt(0) : ""}
      </Avatar>
    </Tooltip>
  );
}
EntityAvatar.propTypes = {
  session: PropTypes.shape({
    thumbnailUrl: PropTypes.func.isRequired,
  }),
  entity: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  className: PropTypes.string,
  tooltip: PropTypes.bool,
  size: PropTypes.oneOf(["small", "medium", "large"]),
  color: PropTypes.string,
};

EntityAvatar.defaultProps = {
  className: "",
  entity: {},
  tooltip: false,
  size: "medium",
};

export default EntityAvatar;
