import centered from "@storybook/addon-centered/react";

import EntityLink from ".";

export default {
  title: "Entity link",
  decorators: [centered],
};

const link = [
  { id: "1", name: "Project name" },
  { id: "2", name: "Folder" },
  { id: "3", name: "Subfolder" },
  { id: "4", name: "Task" },
];

const link1Levels = [{ id: "1", name: "Project" }];

const link2Levels = [
  { id: "1", name: "Project" },
  { id: "4", name: "Task" },
];

const link3Levels = [
  { id: "1", name: "Project" },
  { id: "3", name: "SH_020" },
  { id: "4", name: "Task" },
];

const link4Levels = [
  { id: "1", name: "Project" },
  { id: "2", name: "EP_020" },
  { id: "3", name: "SH_020" },
  { id: "4", name: "Task" },
];

const link5Levels = [
  { id: "1", name: "Project" },
  { id: "2", name: "EP_020" },
  { id: "3", name: "SQ_030" },
  { id: "4", name: "SH_020" },
  { id: "5", name: "Task" },
];

const longnames = [
  { id: "1", name: "Ham hock ullamco reprehenderit pariatur ea" },
  {
    id: "2",
    name: "Reprehenderit pariatur ea, short loin shank magna pork chop",
  },
  { id: "3", name: "Labore anim tongue andouille enim spare ribs" },
  { id: "4", name: "Task" },
];

export const DefaultConfiguration = () => <EntityLink link={link} />;

DefaultConfiguration.storyName = "Default configuration";

DefaultConfiguration.parameters = {
  info: `
Default params and deep link.
`,
};

export const LargeTitle = () => <EntityLink link={link} size="large" />;

LargeTitle.storyName = "Large title";

export const WithoutParent = () => <EntityLink link={link} parent={false} />;

WithoutParent.storyName = "Without parent";

export const WithoutAncestors = () => (
  <EntityLink link={link} ancestors={false} />
);

WithoutAncestors.storyName = "Without ancestors";

export const WithoutParentAndAncestors = () => (
  <EntityLink link={link} parent={false} ancestors={false} />
);

WithoutParentAndAncestors.storyName = "Without parent and ancestors";

const truncateStyle = {
  border: "1px solid red",
  maxWidth: "200px",
};

export const TruncateText = () => (
  <EntityLink link={longnames} style={truncateStyle} />
);

TruncateText.storyName = "Truncate text";

export const _1LevelLink = () => <EntityLink link={link1Levels} />;

_1LevelLink.storyName = "1-level link";

export const _2LevelLink = () => <EntityLink link={link2Levels} />;

_2LevelLink.storyName = "2-level link";

export const _3LevelLink = () => <EntityLink link={link3Levels} />;

_3LevelLink.storyName = "3-level link";

export const _4LevelLink = () => <EntityLink link={link4Levels} />;

_4LevelLink.storyName = "4-level link";

export const _5LevelLink = () => <EntityLink link={link5Levels} />;

_5LevelLink.storyName = "5-level link";
