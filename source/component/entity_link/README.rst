..
    :copyright: Copyright (c) 2018 ftrack

##########
EntityLink
##########

Entity link - display entity and parent names on multiple lines.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
className                      CSS Class to add to the root element.
link                           ftrack API entity link attribute. Array of { id, name } objects.
size               medium      Size of entity name: medium, large.
parent             true        Should display parent name as one row.
ancestors          true        Should display ancestors.
