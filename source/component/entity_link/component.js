// :copyright: Copyright (c) 2016 ftrack
import PropTypes from "prop-types";

import classNames from "classnames";

import style from "./style.scss";

/** Return *entity* name. */
function _getName(entity) {
  return entity.full_name || entity.name;
}

/** Return joined names from *entities*. */
function _joinNames(entities) {
  return entities.map(_getName).join(" / ");
}

/**
 * EntityLink Component - display entity and parent names on multiple lines.
 */
function EntityLink(props) {
  const { link, size, parent, ancestors, className, color, ...other } = props;
  const _classNames = classNames(
    style["entity-link"],
    {
      [style.colorLight]: color === "light",
    },
    className
  );

  const result = [];
  if (link && link.length) {
    const entity = link[link.length - 1];
    const entityAncestors = link.slice(0, -1);

    if (size === "large") {
      result.push(
        <h5 key={entity.id} className={style.entity}>
          {_getName(entity)}
        </h5>
      );
    } else {
      result.push(
        <p key={entity.id} className={style.entity}>
          {_getName(entity)}
        </p>
      );
    }

    const entityParent = entityAncestors[entityAncestors.length - 1];
    if (parent && entityParent) {
      result.push(
        <p key={entityParent.id} className={style["entity-parent"]}>
          {_getName(entityParent)}
        </p>
      );

      if (ancestors) {
        const parentAncestors = entityAncestors.slice(0, -1);
        result.push(
          <p
            key={`${entityParent.id}-ancestors`}
            className={style["entity-ancestors"]}
          >
            {_joinNames(parentAncestors)}
          </p>
        );
      }
    } else if (ancestors && entityAncestors.length) {
      result.push(
        <p key={`${entity.id}-ancestors`} className={style["entity-ancestors"]}>
          {_joinNames(entityAncestors)}
        </p>
      );
    }
  }

  return (
    <div className={_classNames} {...other}>
      {result}
    </div>
  );
}

EntityLink.propTypes = {
  link: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    })
  ).isRequired,
  size: PropTypes.oneOf(["medium", "large"]),
  parent: PropTypes.bool,
  ancestors: PropTypes.bool,
  className: PropTypes.string,
  color: PropTypes.string,
};

EntityLink.defaultProps = {
  className: "",
  size: "medium",
  parent: true,
  ancestors: true,
};

export default EntityLink;
