// :copyright: Copyright (c) 2017 ftrack
import centered from "@storybook/addon-centered/react";

import Skeleton from ".";

export default {
  title: "Skeleton",
  decorators: [centered],
};

export const _Skeleton = () => (
  <div style={{ width: "300px" }}>
    <h4>
      <Skeleton />
    </h4>
    <p>
      <Skeleton />
    </p>
    <hr />
    <h4>Title here</h4>
    <p>Smaller content</p>
  </div>
);

export const SkeletonNoAnimation = () => (
  <div style={{ width: "300px" }}>
    <h4>
      <Skeleton animation={null} />
    </h4>
    <p>
      <Skeleton animation={null} />
    </p>
    <hr />
    <h4>Title here</h4>
    <p>Smaller content</p>
  </div>
);

SkeletonNoAnimation.storyName = "Skeleton (no animation)";
