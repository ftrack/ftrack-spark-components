// :copyright: Copyright (c) 2018 ftrack
import PropTypes from "prop-types";

import classNames from "classnames";
import style from "./style.scss";

function Skeleton({ className, animation = "default", ...other }) {
  const classes = classNames(
    style.skeleton,
    {
      [style.animationDefault]: animation === "default",
    },
    className
  );

  return (
    <span className={classes} {...other}>
      {"\u00A0"}
    </span>
  );
}

Skeleton.propTypes = {
  animation: PropTypes.oneOf([null, "default"]),
  className: PropTypes.string,
};

Skeleton.defaultProps = {};

export default Skeleton;
