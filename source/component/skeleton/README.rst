..
    :copyright: Copyright (c) 2018 ftrack

########
Skeleton
########

The Skeleton component shows an animated background and can be used in place
of thext to indicate a loading state.

All props are spread to the rendered span element.
