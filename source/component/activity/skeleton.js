// :copyright: Copyright (c) 2018 ftrack
import Skeleton from "../skeleton";

import style from "./style.scss";

const ActivitySkeleton = () => (
  <div className={style["activity-skeleton"]}>
    <Skeleton className={style["avatar-skeleton"]} />
    <div className={style["activity-skeleton-body"]}>
      <Skeleton />
      <div className={style["skeleton-row"]}>
        <Skeleton className={style["icon-skeleton"]} />
        <div className={style["date-skeleton-container"]}>
          <Skeleton />
        </div>
      </div>
    </div>
  </div>
);

export default ActivitySkeleton;
