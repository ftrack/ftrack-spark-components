// :copyright: Copyright (c) 2018 ftrack
import { action } from "@storybook/addon-actions";
// eslint-disable-next-line
import centered from "@storybook/addon-centered/react";
import moment from "moment";

import {
  ObjectStatus,
  AssetVersion,
  Note,
  ReviewSession,
  ReviewSessionObject,
  ClusterManageReviewSession,
  ClusterCreateAssetVersion,
  EventActivityItem,
  Project,
  Asset,
  ErrorMessage,
} from ".";
import { SparkProvider } from "../util/hoc";
import getSession from "../../story/get_session";

const now = moment();
const user = {
  id: "foo",
  first_name: "John",
  last_name: "Doe",
  thumbnail_id: "35ade18f-707a-11e8-824d-8c8590b2e7c5",
};

export default {
  title: "Activity",
  decorators: [
    centered,
    (Story) => (
      <SparkProvider session={getSession()}>
        <Story />
      </SparkProvider>
    ),
  ],
};

const reviewSessionObject = {
  name: "<Review session object name>",
  id: "foo",
  type: "ReviewSessionObject",
  thumbnail_id: "6e6e5e87-4c50-4b65-9c34-3e30d79fc318",
};
const project = {
  name: "<Project name>",
  id: "foo",
  type: "Project",
  thumbnail_id: "6e6e5e87-4c50-4b65-9c34-3e30d79fc318",
};
const assetVersion = {
  name: "<Asset version name>",
  id: "foo",
  type: "AssetVersion",
  version: "2",
  thumbnail_id: "6e6e5e87-4c50-4b65-9c34-3e30d79fc318",
};
const folder = {
  name: "<Typed context name>",
  id: "foo",
  type: "TypedContext",
  thumbnail_id: "6e6e5e87-4c50-4b65-9c34-3e30d79fc318",
};
const reviewSession = {
  name: "<Review session name>",
  id: "foo",
  type: "ReviewSession",
};
const asset = {
  name: "<Asset name>",
  id: "foo",
  type: "Asset",
};

export const ActivityWrapperComponent = () => (
  <EventActivityItem
    createdBy={user}
    createdAt={now}
    onClick={action("Link clicked")}
    icon="ondemand_video"
  >
    <span>Content goes here...</span>
  </EventActivityItem>
);

ActivityWrapperComponent.storyName = "Activity wrapper component";

export const ObjectStatusApproved = () => (
  <ObjectStatus
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    reviewSessionObject={reviewSessionObject}
    status="approved"
  />
);

ObjectStatusApproved.storyName = "Object status approved";

export const ObjectStatusRequireChanges = () => (
  <ObjectStatus
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    reviewSessionObject={reviewSessionObject}
    status="require_changes"
  />
);

ObjectStatusRequireChanges.storyName = "Object status require_changes";

export const ObjectStatusSeen = () => (
  <ObjectStatus
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    reviewSessionObject={reviewSessionObject}
    status="seen"
  />
);

ObjectStatusSeen.storyName = "Object status seen";

export const ObjectStatusReset = () => (
  <ObjectStatus
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    reviewSessionObject={reviewSessionObject}
    status={undefined}
  />
);

ObjectStatusReset.storyName = "Object status reset";

export const CreatedProject = () => (
  <Project
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    project={project}
  />
);

CreatedProject.storyName = "Created project";

export const CreateAssetVersion = () => (
  <AssetVersion
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    assetVersion={assetVersion}
  />
);

CreateAssetVersion.storyName = "Create asset version";

export const CreateAssetVersionNoThumbnail = () => (
  <AssetVersion
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    assetVersion={Object.assign({}, assetVersion, {
      thumbnail_id: undefined,
    })}
  />
);

CreateAssetVersionNoThumbnail.storyName = "Create asset version no thumbnail";

export const NoteOnReviewSessionObject = () => (
  <Note
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    entity={reviewSessionObject}
  />
);

NoteOnReviewSessionObject.storyName = "Note on review session object";

export const NoteOnAssetVersion = () => (
  <Note
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    entity={assetVersion}
  />
);

NoteOnAssetVersion.storyName = "Note on asset version";

export const NoteOnProject = () => (
  <Note
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    entity={project}
  />
);

NoteOnProject.storyName = "Note on project";

export const NoteOnFolder = () => (
  <Note
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    entity={folder}
  />
);

NoteOnFolder.storyName = "Note on folder";

export const ReviewSessionCreated = () => (
  <ReviewSession
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    reviewSession={reviewSession}
  />
);

ReviewSessionCreated.storyName = "Review session created";

export const ReviewSessionObjectCreated = () => (
  <ReviewSessionObject
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    reviewSession={reviewSession}
    reviewSessionObject={reviewSessionObject}
  />
);

ReviewSessionObjectCreated.storyName = "Review session object created";

export const ReviewSessionObjectCreatedNoThumbnail = () => (
  <ReviewSessionObject
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    reviewSession={reviewSession}
    reviewSessionObject={Object.assign({}, reviewSessionObject, {
      thumbnail_id: undefined,
    })}
  />
);

ReviewSessionObjectCreatedNoThumbnail.storyName =
  "Review session object created no thumbnail";

export const AssetMoved = () => (
  <Asset
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    asset={asset}
    isMove
  />
);

AssetMoved.storyName = "Asset moved";

export const AssetRenamed = () => (
  <Asset
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    asset={asset}
    isRename
  />
);

AssetRenamed.storyName = "Asset renamed";

export const AssetMovedAndRenamed = () => (
  <Asset
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    asset={asset}
    isMove
    isRename
  />
);

AssetMovedAndRenamed.storyName = "Asset moved and renamed";

export const AssetOtherChange = () => (
  <Asset
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    asset={asset}
  />
);

AssetOtherChange.storyName = "Asset other change";

export const ClusterAddSingleReviewSessionObjects = () => (
  <ClusterManageReviewSession
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    items={[
      {
        type: "review_session_object",
        data: {
          reviewSessionObject,
          reviewSession,
          user,
          createdAt: now,
        },
      },
    ]}
  />
);

ClusterAddSingleReviewSessionObjects.storyName =
  "(Cluster) add single review session objects";

export const ClusterAdd2ReviewSessionObjects = () => (
  <ClusterManageReviewSession
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    items={[
      {
        type: "review_session_object",
        data: {
          reviewSessionObject,
          reviewSession,
          user,
          createdAt: now,
        },
      },
      {
        type: "review_session_object",
        data: {
          reviewSessionObject,
          reviewSession,
          user,
          createdAt: now,
        },
      },
    ]}
  />
);

ClusterAdd2ReviewSessionObjects.storyName =
  "(Cluster) add 2 review session objects";

export const ClusterCreatedReviewSessionAddWith2Assets = () => (
  <ClusterManageReviewSession
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    items={[
      {
        type: "review_session",
        data: {
          reviewSession,
          user,
          createdAt: now,
        },
      },
      {
        type: "review_session_object",
        data: {
          reviewSessionObject,
          reviewSession,
          user,
          createdAt: now,
        },
      },
      {
        type: "review_session_object",
        data: {
          reviewSessionObject,
          reviewSession,
          user,
          createdAt: now,
        },
      },
    ]}
  />
);

ClusterCreatedReviewSessionAddWith2Assets.storyName =
  "(Cluster) created review session add with 2 assets";

export const ClusterCreatedSingleAsset = () => (
  <ClusterCreateAssetVersion
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    items={[
      {
        type: "asset_version",
        data: {
          assetVersion,
          user,
          createdAt: now,
        },
      },
    ]}
  />
);

ClusterCreatedSingleAsset.storyName = "(Cluster) created single asset";

export const ClusterCreated2Assets = () => (
  <ClusterCreateAssetVersion
    user={user}
    createdAt={now}
    onClick={action("Link clicked")}
    items={[
      {
        type: "asset_version",
        data: {
          assetVersion,
          user,
          createdAt: now,
        },
      },
      {
        type: "asset_version",
        data: {
          assetVersion,
          user,
          createdAt: now,
        },
      },
    ]}
  />
);

ClusterCreated2Assets.storyName = "(Cluster) created 2 assets";

export const _ErrorMessage = () => <ErrorMessage />;

_ErrorMessage.storyName = "Error message";
