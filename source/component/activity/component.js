// :copyright: Copyright (c) 2018 ftrack
import { Component } from "react";
import PropTypes from "prop-types";
import FontIcon from "react-toolbox/lib/font_icon";
import classNames from "classnames";
import { withState, withHandlers, compose } from "recompose";
import Button from "react-toolbox/lib/button";
import {
  intlShape,
  defineMessages,
  FormattedDate,
  FormattedTime,
  FormattedMessage,
} from "react-intl";

import sizeMe from "react-sizeme";
import style from "./style.scss";
import { withSession } from "../util/hoc";
import EntityAvatar from "../entity_avatar";
import Markdown from "../markdown";
import FtrackIcon from "../ftrack_icon";
import Thumbnail from "../image/thumbnail";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import { getEntityThumbnail } from "../util/get_media_component";

const messages = defineMessages({
  "activity-message-project-create": {
    id: "ftrack-spark-components-activity-message-project-create",
    defaultMessage: "{user} created the project {link}",
    description: "Activity message for project creation",
  },
  "activity-message-project-first-entry": {
    id: "ftrack-spark-components-activity-message-project-first-entry",
    defaultMessage:
      "This is the beginning of your project. While the project develops, this " +
      "view will give you a historical overview of what has happened and when.",
    description: "Activity message for project creation",
  },
  "activity-message-added-note-to-review": {
    id: "ftrack-spark-components-activity-message-added-note-to-review",
    defaultMessage: "{user} gave feedback on {link} in {reviewSession}",
    description: "Activity message for added new note in a Review session.",
  },
  "activity-message-added-note-to": {
    id: "ftrack-spark-components-activity-message-added-note-to",
    defaultMessage: "{user} gave feedback on {link}",
    description: "Activity message for added new note",
  },
  "activity-message-added-new-version": {
    id: "ftrack-spark-components-activity-message-added-new-version",
    defaultMessage: "{user} added a new version of {link}",
    description: "Activity message for added new version",
  },
  "activity-message-added-new-first-asset": {
    id: "ftrack-spark-components-activity-message-added-new-first-asset",
    defaultMessage: "{user} added an asset {link}",
    description: "Activity message for added new version",
  },
  "activity-unknown-user": {
    id: "ftrack-spark-components-activity-unknown-user",
    defaultMessage: "Unknown",
    description: "Activity made by unknown user",
  },
  "activity-message-review-session-object-status-approved": {
    id: "ftrack-spark-components-activity-message-review-session-object-status-approved",
    defaultMessage: "{user} approved {link}",
    description: "Activity message for approved review session object",
  },
  "activity-message-review-session-object-status-require_changes": {
    id: "ftrack-spark-components-activity-message-review-session-object-status-require_changes",
    defaultMessage: "{user} require changes on {link}",
    description:
      "Activity message for required changes to review session object",
  },
  "activity-message-review-session-object-status-seen": {
    id: "ftrack-spark-components-activity-message-review-session-object-status-seen",
    defaultMessage: "{user} has seen {link}",
    description:
      "Activity message for collaborator has seen a review session object",
  },
  "activity-message-review-session-object-status-removed": {
    id: "ftrack-spark-components-activity-message-review-session-object-status-removed",
    defaultMessage: "{user} changed the status on {link}",
    description:
      "Activity message for collaborator has changed a review session object status",
  },
  "activity-message-created-review-session": {
    id: "ftrack-spark-components-activity-message-created-review-session",
    defaultMessage: "{user} created {link}",
    description: "Activity message for created review session",
  },
  "activity-message-created-review-session-object": {
    id: "ftrack-spark-components-activity-message-created-review-session-object",
    defaultMessage:
      "{user} added {reviewSessionObjectLink} to {reviewSessionLink}",
    description: "Activity message for created review session object",
  },
  "activity-message-asset-changed": {
    id: "ftrack-spark-components-activity-message-asset-changed",
    defaultMessage: "{user} changed {link}",
    description: "Changed asset.",
  },
  "activity-message-asset-renamed": {
    id: "ftrack-spark-components-activity-message-asset-renamed",
    defaultMessage: "{user} renamed {link}",
    description: "Renamed asset.",
  },
  "activity-message-asset-moved": {
    id: "ftrack-spark-components-activity-message-asset-moved",
    defaultMessage: "{user} moved {link}",
    description: "Moved asset.",
  },
  "activity-message-asset-moved-and-renamed": {
    id: "ftrack-spark-components-activity-moved-and-renamed",
    defaultMessage: "{user} moved and renamed {link}",
    description: "Moved and renamed asset.",
  },
  "activity-message-cluster-manage-session-created": {
    id: "ftrack-spark-components-activity-message-cluster-manage-session-created",
    defaultMessage: "{user} created {reviewSessionLink} with {assetsLink}",
    description: "Review session being created with assets",
  },
  "activity-message-cluster-manage-session-added": {
    id: "ftrack-spark-components-activity-message-cluster-manage-session-added",
    defaultMessage: "{user} added {assetsLink} to {reviewSessionLink}",
    description: "Multiple assets being added to something",
  },
  "activity-message-cluster-created-asset-version": {
    id: "ftrack-spark-components-activity-message-cluster-created-asset-version",
    defaultMessage: "{user} added {assetsLink}",
    description: "Multiple assets being created",
  },
  "activity-message-cluster-manage-session-number-of-assets": {
    id: "ftrack-spark-components-activity-message-cluster-manage-session-number-of-assets",
    defaultMessage:
      "{numberOfAssets, plural, =0 {no assets} one {one asset} other {# assets}}",
    description: "No, single or multiple assets",
  },
  "activity-message-error-not-interpret": {
    id: "ftrack-spark-components-activity-message-error-not-interpret",
    defaultMessage: "We found an activity that we could not interpret.",
    description: "Found an activity that could not be parsed.",
  },
  "activity-message-error-contact-support": {
    id: "ftrack-spark-components-activity-contact-support",
    defaultMessage: "Contact our support for more information.",
    description: "Contact support for more information.",
  },
  "activity-see-more-button": {
    id: "ftrack-spark-components.activity-see-more-button",
    defaultMessage: "See more",
    description: "Button to expand note view",
  },
});

const projectShape = PropTypes.shape({
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
});
const reviewSessionObjectShape = PropTypes.shape({
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
});
const reviewSessionShape = PropTypes.shape({
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
});
const assetVersionShape = PropTypes.shape({
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  thumbnail_id: PropTypes.string,
});
const assetShape = PropTypes.shape({
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
});
const noteEntityShape = PropTypes.oneOfType([
  projectShape,
  reviewSessionObjectShape,
  assetVersionShape,
]);

const EntityAvatarWithSession = withSession(EntityAvatar);

class PreviewImage extends Component {
  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.props.onClick([this.props.entity], "preview");
  }

  render() {
    const { entity } = this.props;
    const url = getEntityThumbnail(entity);

    return (
      <div onClick={this.onClick} className={style["image-container"]}>
        <Thumbnail rounded src={url} size="tiny" className={style.thumbnail} />
      </div>
    );
  }
}

PreviewImage.propTypes = {
  entity: PropTypes.shape({
    thumbnail_url: PropTypes.shape({
      url: PropTypes.string,
    }),
    thumbnail_id: PropTypes.string,
    name: PropTypes.string,
    id: PropTypes.string,
    type: PropTypes.string,
  }).isRequired,
  onClick: PropTypes.func.isRequired,
};

function User_({ user, intl }) {
  if (!user) {
    return <span>{intl.formatMessage(messages["activity-unknown-user"])}</span>;
  }

  if (user.name) {
    return <span>{user.name}</span>;
  }

  return <span>{`${user.first_name} ${user.last_name}`}</span>;
}
User_.propTypes = {
  user: PropTypes.oneOfType([
    PropTypes.shape({
      first_name: PropTypes.string.isRequired,
      last_name: PropTypes.string.isRequired,
    }),
    PropTypes.shape({
      name: PropTypes.string.isRequired,
    }),
  ]),
  intl: intlShape.isRequired,
};
const User = safeInjectIntl(User_);

function EventActivityItem_({
  children,
  createdAt,
  icon,
  createdBy,
  previews,
  intl,
  onClick,
  small,
  firstMessage,
}) {
  let avatar;
  if (createdBy) {
    avatar = <EntityAvatarWithSession entity={createdBy} />;
  } else if (firstMessage) {
    avatar = <FtrackIcon className={style["icon-logo"]} />;
  }

  let date;
  let dateTime;
  if (createdBy) {
    date = createdAt.toDate();
    dateTime = `${intl.formatTime(date)} ${intl.formatDate(date)}`;
  }

  const formattedDate = (
    <span>
      <FormattedDate
        value={date}
        year="numeric"
        day="2-digit"
        month="2-digit"
      />
      {" - "}
      <FormattedTime value={date} hour="numeric" minute="2-digit" />
    </span>
  );

  const previewElements = previews.map((entity, index) => (
    <PreviewImage
      entity={entity}
      onClick={onClick}
      key={`${index}-entity-${entity.id}`}
    />
  ));

  const bodyClasses = classNames(
    style["activity-item-body"],
    small && style.small
  );

  return (
    <div className={style["activity-item"]}>
      {!small ? <FontIcon className={style.icon} value={icon} /> : null}
      {avatar}
      <div className={bodyClasses}>
        {children}
        {previewElements.length ? (
          <div className={style.images}>{previewElements}</div>
        ) : null}
        {createdBy ? (
          <div className={style.info}>
            <span title={dateTime} className={style.date}>
              {formattedDate}
            </span>
          </div>
        ) : null}
      </div>
    </div>
  );
}

EventActivityItem_.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  createdAt: PropTypes.object.isRequired, // eslint-disable-line
  icon: PropTypes.string.isRequired,
  createdBy: PropTypes.object.isRequired, // eslint-disable-line
  previews: PropTypes.arrayOf(
    PropTypes.shape({
      thumbnail_id: PropTypes.string,
      name: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
      type: PropTypes.string.isRequired,
    })
  ),
  onClick: PropTypes.func.isRequired,
  intl: intlShape.isRequired,
  small: PropTypes.bool,
  firstMessage: PropTypes.bool,
};

EventActivityItem_.defaultProps = {
  previews: [],
};

export const EventActivityItem = safeInjectIntl(
  withSession(EventActivityItem_)
);

class LinkRelated extends Component {
  // eslint-disable-line
  constructor(props) {
    super(props);
    this.state = {};
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.props.onClick(this.props.items, "link");
  }

  render() {
    const { label } = this.props;
    return (
      <button role="link" onClick={this.onClick} className={style.link}>
        {label}
      </button>
    );
  }
}

LinkRelated.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      type: PropTypes.string,
    })
  ).isRequired,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

function Project_({ onClick, createdAt, user, project, small }) {
  return (
    <div>
      <EventActivityItem
        onClick={onClick}
        icon="lens"
        createdAt={createdAt}
        createdBy={user}
        small={small}
      >
        <FormattedMessage
          {...messages["activity-message-project-create"]}
          values={{
            user: <User user={user} />,
            link: (
              <LinkRelated
                onClick={onClick}
                label={project.name}
                items={[project]}
              />
            ),
          }}
        />
      </EventActivityItem>
      <br />
      <EventActivityItem onClick={onClick} icon="lens" firstMessage>
        <FormattedMessage
          {...messages["activity-message-project-first-entry"]}
        />
      </EventActivityItem>
    </div>
  );
}
Project_.propTypes = {
  createdAt: PropTypes.object.isRequired, // eslint-disable-line
  user: PropTypes.shape({
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,
    thumbnail_id: PropTypes.string,
  }),
  project: projectShape.isRequired,
  onClick: PropTypes.func.isRequired,
  small: PropTypes.bool,
};
export const Project = safeInjectIntl(Project_);

function ObjectStatus_({
  onClick,
  createdAt,
  user,
  status,
  reviewSessionObject,
  small,
}) {
  const intlString = `activity-message-review-session-object-status-${
    status || "removed"
  }`;
  const seenStatus = "activity-message-review-session-object-status-seen";

  const assetVersion = reviewSessionObject.asset_version
    ? `v${reviewSessionObject.asset_version.version}`
    : "";
  const entityInformationLink = `${reviewSessionObject.name} ${assetVersion}`;
  return (
    <EventActivityItem
      onClick={onClick}
      icon={seenStatus ? "people" : "ondemand_video"}
      createdAt={createdAt}
      createdBy={user}
      small={small}
    >
      <FormattedMessage
        {...messages[intlString]}
        values={{
          user: <User user={user} />,
          link: (
            <LinkRelated
              onClick={onClick}
              label={entityInformationLink}
              items={[reviewSessionObject]}
            />
          ),
        }}
      />
    </EventActivityItem>
  );
}
ObjectStatus_.propTypes = {
  createdAt: PropTypes.object.isRequired, // eslint-disable-line
  user: PropTypes.shape({
    name: PropTypes.isRequired,
  }),
  status: PropTypes.string.isRequired,
  reviewSessionObject: reviewSessionObjectShape.isRequired,
  onClick: PropTypes.func.isRequired,
  small: PropTypes.bool,
};
export const ObjectStatus = safeInjectIntl(ObjectStatus_);

function AssetVersion_({ onClick, user, createdAt, assetVersion, small }) {
  let entityInformationLink = "";
  if (assetVersion.version && assetVersion.parent) {
    entityInformationLink = `${assetVersion.parent} / ${assetVersion.name} v${assetVersion.version}`;
  } else if (assetVersion.version) {
    entityInformationLink = `${assetVersion.name} v${assetVersion.version}`;
  } else {
    entityInformationLink = `${assetVersion.name}`;
  }
  const firstAsset = assetVersion.version === 1;
  return (
    <EventActivityItem
      onClick={onClick}
      icon="layers"
      createdAt={createdAt}
      createdBy={user}
      small={small}
      previews={[
        Object.assign(
          {},
          {
            thumbnail_url: assetVersion.thumbnail_url,
            thumbnail_id: assetVersion.thumbnail_id,
            name: assetVersion.name,
            id: assetVersion.id,
            type: assetVersion.type,
          }
        ),
      ]}
    >
      <FormattedMessage
        {...messages[
          firstAsset
            ? "activity-message-added-new-first-asset"
            : "activity-message-added-new-version"
        ]}
        values={{
          user: <User user={user} />,
          link: (
            <LinkRelated
              onClick={onClick}
              label={entityInformationLink}
              items={[assetVersion]}
            />
          ),
        }}
      />
    </EventActivityItem>
  );
}
AssetVersion_.propTypes = {
  createdAt: PropTypes.object.isRequired, // eslint-disable-line
  user: PropTypes.shape({
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,
    thumbnail_id: PropTypes.string,
  }),
  assetVersion: assetVersionShape.isRequired,
  onClick: PropTypes.func.isRequired,
  small: PropTypes.bool,
};
export const AssetVersion = safeInjectIntl(AssetVersion_);

function Note_({
  onClick,
  user,
  createdAt,
  entity,
  small,
  noteText,
  clicked,
  onButtonClick,
  className,
  size,
  onSize,
}) {
  const { height } = size;
  const canExpand = height && height > 150;
  const fadeOutText = !clicked && canExpand;
  const classes = classNames(style["notes-activity"], className, {
    [style["note-bottom-fade"]]: fadeOutText,
    [style["note-bottom-fade-raised"]]: fadeOutText && small,
    [style["show-entire-note"]]: clicked,
  });

  const version = entity.version ? `v${entity.version}` : "";
  const entityInformationLink = `${entity.name} ${version}`;
  const reviewSessionName = entity.review_session
    ? entity.review_session.name
    : "";

  return (
    <EventActivityItem
      onClick={onClick}
      icon="feedback"
      createdAt={createdAt}
      createdBy={user}
      small={small}
    >
      {reviewSessionName ? (
        <FormattedMessage
          {...messages["activity-message-added-note-to-review"]}
          values={{
            user: <User user={user} />,
            link: (
              <LinkRelated
                onClick={onClick}
                label={entityInformationLink}
                items={[entity]}
              />
            ),
            reviewSession: (
              <LinkRelated
                label={reviewSessionName}
                onClick={onClick}
                items={[
                  {
                    type: entity.review_session.__entity_type__,
                    id: entity.review_session.id,
                  },
                ]}
              />
            ),
          }}
        />
      ) : (
        <FormattedMessage
          {...messages["activity-message-added-note-to"]}
          values={{
            user: <User user={user} />,
            link: (
              <LinkRelated
                onClick={onClick}
                label={entityInformationLink}
                items={[entity]}
              />
            ),
          }}
        />
      )}
      <div className={classes}>
        <MeasuredMarkdown source={noteText || ""} onSize={onSize} />
      </div>
      {!clicked && canExpand ? (
        <Button
          className={style["note-button"]}
          onClick={onButtonClick}
          label={<FormattedMessage {...messages["activity-see-more-button"]} />}
        />
      ) : null}
    </EventActivityItem>
  );
}
Note_.propTypes = {
  createdAt: PropTypes.object.isRequired, // eslint-disable-line
  user: PropTypes.shape({
    name: PropTypes.isRequired,
  }),
  entity: noteEntityShape.isRequired,
  onClick: PropTypes.func.isRequired,
  small: PropTypes.bool,
  clicked: PropTypes.bool,
  noteText: PropTypes.string,
  className: PropTypes.string,
  onButtonClick: PropTypes.func,
  onSize: PropTypes.func,
  // eslint-disable-next-line react/forbid-prop-types
  size: PropTypes.object,
};

const MeasuredMarkdown = sizeMe({ monitorHeight: true })(Markdown);

export const Note = compose(
  withState("clicked", "showFullText", false),
  withState("size", "onSize", {}),
  withHandlers({
    onButtonClick: (props) => () => props.showFullText(!props.clicked),
  }),
  safeInjectIntl
)(Note_);

function ReviewSession_({ onClick, user, createdAt, reviewSession, small }) {
  return (
    <EventActivityItem
      onClick={onClick}
      icon="ondemand_video"
      createdAt={createdAt}
      createdBy={user}
      small={small}
    >
      <FormattedMessage
        {...messages["activity-message-created-review-session"]}
        values={{
          user: <User user={user} />,
          link: (
            <LinkRelated
              onClick={onClick}
              label={reviewSession.name}
              items={[reviewSession]}
            />
          ),
        }}
      />
    </EventActivityItem>
  );
}
ReviewSession_.propTypes = {
  createdAt: PropTypes.object.isRequired, // eslint-disable-line
  user: PropTypes.shape({
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,
    thumbnail_id: PropTypes.string,
  }),
  reviewSession: reviewSessionShape.isRequired,
  onClick: PropTypes.func.isRequired,
  small: PropTypes.bool,
};
export const ReviewSession = safeInjectIntl(ReviewSession_);

function ReviewSessionObject_({
  onClick,
  user,
  createdAt,
  reviewSessionObject,
  reviewSession,
  small,
}) {
  const assetVersion = reviewSessionObject.version
    ? `v${reviewSessionObject.version}`
    : "";
  const entityInformationLink = reviewSessionObject.parent
    ? `${reviewSessionObject.parent} / ${reviewSessionObject.name} ${assetVersion}`
    : `${reviewSessionObject.name} ${assetVersion}`;

  return (
    <EventActivityItem
      onClick={onClick}
      icon="ondemand_video"
      createdAt={createdAt}
      createdBy={user}
      small={small}
      previews={[
        Object.assign(
          {},
          {
            thumbnail_url: reviewSessionObject.thumbnail_url,
            thumbnail_id: reviewSessionObject.thumbnail_id,
            name: reviewSessionObject.name,
            id: reviewSessionObject.id,
            type: reviewSessionObject.type,
          }
        ),
      ]}
    >
      <FormattedMessage
        {...messages["activity-message-created-review-session-object"]}
        values={{
          user: <User user={user} />,
          reviewSessionObjectLink: (
            <LinkRelated
              onClick={onClick}
              label={entityInformationLink}
              items={[reviewSessionObject]}
            />
          ),
          reviewSessionLink: (
            <LinkRelated
              onClick={onClick}
              label={reviewSession.name}
              items={[reviewSession]}
            />
          ),
        }}
      />
    </EventActivityItem>
  );
}
ReviewSessionObject_.propTypes = {
  createdAt: PropTypes.object.isRequired, // eslint-disable-line
  user: PropTypes.shape({
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,
    thumbnail_id: PropTypes.string,
  }),
  reviewSession: reviewSessionShape.isRequired,
  reviewSessionObject: reviewSessionObjectShape.isRequired,
  onClick: PropTypes.func.isRequired,
  small: PropTypes.bool,
};
export const ReviewSessionObject = safeInjectIntl(ReviewSessionObject_);

function Asset_({ onClick, user, createdAt, asset, isMove, isRename, small }) {
  let action = "changed";

  if (isMove) {
    action = "moved";
  }

  if (isRename) {
    action = "renamed";
  }

  if (isMove && isRename) {
    action = "moved-and-renamed";
  }

  return (
    <EventActivityItem
      onClick={onClick}
      icon="layers"
      createdAt={createdAt}
      createdBy={user}
      small={small}
    >
      <FormattedMessage
        {...messages[`activity-message-asset-${action}`]}
        values={{
          user: <User user={user} />,
          link: (
            <LinkRelated onClick={onClick} label={asset.name} items={[asset]} />
          ),
        }}
      />
    </EventActivityItem>
  );
}
Asset_.propTypes = {
  createdAt: PropTypes.object.isRequired, // eslint-disable-line
  user: PropTypes.shape({
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,
    thumbnail_id: PropTypes.string,
  }),
  asset: assetShape.isRequired,
  isMove: PropTypes.bool,
  isRename: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  small: PropTypes.bool,
};
export const Asset = safeInjectIntl(Asset_);

function ClusterManageReviewSession_({
  onClick,
  user,
  createdAt,
  items,
  intl,
  small,
}) {
  const mapOnType = items.reduce((accumulator, { type, data }) => {
    if (!accumulator[type]) {
      accumulator[type] = [];
    }

    accumulator[type].push(data);

    return accumulator;
  }, {});

  const previews = mapOnType.review_session_object.map((data) =>
    Object.assign(
      {},
      {
        thumbnail_url: data.reviewSessionObject.thumbnail_url,
        thumbnail_id: data.reviewSessionObject.thumbnail_id,
        name: data.reviewSessionObject.name,
        id: data.reviewSessionObject.id,
        type: data.reviewSessionObject.type,
      }
    )
  );
  let content;
  if (mapOnType.review_session && mapOnType.review_session.length) {
    // Review session has been created in this cluster.
    const [{ reviewSession }] = mapOnType.review_session;
    content = (
      <FormattedMessage
        {...messages["activity-message-cluster-manage-session-created"]}
        values={{
          user: <User user={user} />,
          reviewSessionLink: (
            <LinkRelated
              onClick={onClick}
              items={[reviewSession]}
              label={reviewSession.name}
            />
          ),
          assetsLink: (
            <LinkRelated
              onClick={onClick}
              items={[reviewSession]}
              label={intl.formatMessage(
                messages[
                  "activity-message-cluster-manage-session-number-of-assets"
                ],
                {
                  numberOfAssets:
                    (mapOnType.review_session_object &&
                      mapOnType.review_session_object.length) ||
                    0,
                }
              )}
            />
          ),
        }}
      />
    );
  } else {
    // No review session has been created in this cluster.
    const [{ reviewSession }] = mapOnType.review_session_object;
    content = (
      <FormattedMessage
        {...messages["activity-message-cluster-manage-session-added"]}
        values={{
          user: <User user={user} />,
          assetsLink: (
            <LinkRelated
              onClick={onClick}
              items={[reviewSession]}
              label={intl.formatMessage(
                messages[
                  "activity-message-cluster-manage-session-number-of-assets"
                ],
                {
                  numberOfAssets:
                    (mapOnType.review_session_object &&
                      mapOnType.review_session_object.length) ||
                    0,
                }
              )}
            />
          ),
          reviewSessionLink: (
            <LinkRelated
              onClick={onClick}
              items={[reviewSession]}
              label={reviewSession.name}
            />
          ),
        }}
      />
    );
  }

  return (
    <EventActivityItem
      onClick={onClick}
      icon="ondemand_video"
      createdAt={createdAt}
      createdBy={user}
      previews={previews}
      small={small}
    >
      {content}
    </EventActivityItem>
  );
}
ClusterManageReviewSession_.propTypes = {
  createdAt: PropTypes.object.isRequired, // eslint-disable-line
  user: PropTypes.shape({
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,
    thumbnail_id: PropTypes.string.isRequired,
  }),
  items: PropTypes.array, // eslint-disable-line
  intl: intlShape.isRequired,
  onClick: PropTypes.func.isRequired,
  small: PropTypes.bool,
};
export const ClusterManageReviewSession = safeInjectIntl(
  ClusterManageReviewSession_
);

function ClusterCreateAssetVersion_({
  onClick,
  user,
  createdAt,
  items,
  intl,
  small,
}) {
  const previews = items.map((item) =>
    Object.assign(
      {},
      {
        thumbnail_url: item.data.assetVersion.thumbnail_url,
        thumbnail_id: item.data.assetVersion.thumbnail_id,
        name: item.data.assetVersion.name,
        id: item.data.assetVersion.id,
        type: item.data.assetVersion.type,
      }
    )
  );
  return (
    <EventActivityItem
      onClick={onClick}
      icon="ondemand_video"
      createdAt={createdAt}
      createdBy={user}
      small={small}
      previews={previews}
    >
      <FormattedMessage
        {...messages["activity-message-cluster-created-asset-version"]}
        values={{
          user: <User user={user} />,
          assetsLink: (
            <LinkRelated
              onClick={onClick}
              items={items}
              label={intl.formatMessage(
                messages[
                  "activity-message-cluster-manage-session-number-of-assets"
                ],
                {
                  numberOfAssets: (items && items.length) || 0,
                }
              )}
            />
          ),
        }}
      />
    </EventActivityItem>
  );
}
ClusterCreateAssetVersion_.propTypes = {
  createdAt: PropTypes.object.isRequired, // eslint-disable-line
  user: PropTypes.shape({
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,
    thumbnail_id: PropTypes.string.isRequired,
  }),
  items: PropTypes.array, // eslint-disable-line
  intl: intlShape.isRequired,
  onClick: PropTypes.func.isRequired,
  small: PropTypes.bool,
};
export const ClusterCreateAssetVersion = safeInjectIntl(
  ClusterCreateAssetVersion_
);

function ErrorMessage_({ small }) {
  return (
    <EventActivityItem icon="error_outline" small={small}>
      <div className={style["error-message"]}>
        <FormattedMessage
          {...messages["activity-message-error-not-interpret"]}
        />
      </div>
      <div>
        <FormattedMessage
          {...messages["activity-message-error-contact-support"]}
        />
      </div>
    </EventActivityItem>
  );
}

ErrorMessage_.propTypes = {
  small: PropTypes.bool,
};

export const ErrorMessage = safeInjectIntl(ErrorMessage_);

// TODO: Feel free to fix this eslint error if you got time
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  object_status: ObjectStatus,
  asset_version: AssetVersion,
  note: Note,
  review_session: ReviewSession,
  review_session_object: ReviewSessionObject,
  cluster_manage_review_session: ClusterManageReviewSession,
  cluster_add_asset_version: ClusterCreateAssetVersion,
  project: Project,
  asset: Asset,
  error: ErrorMessage,
};
