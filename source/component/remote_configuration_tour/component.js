// :copyright: Copyright (c) 2019 ftrack

import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import log from "loglevel";
import ErrorBoundary from "react-error-boundary";
import TourDialog from "../tour";

function ConfigTour({ baseUrl, steps, ...props }) {
  return (
    <TourDialog
      steps={steps.map((step) => ({
        ...step,
        image: step.image ? `${baseUrl}${step.image}` : null,
        actions: step.actions,
      }))}
      {...props}
    />
  );
}

ConfigTour.propTypes = {
  steps: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.node,
      text: PropTypes.node,
      image: PropTypes.node,
      layout: PropTypes.string,
    })
  ),
  baseUrl: PropTypes.string,
};

const NullComponent = () => null;

function RemoteConfigurationTour({ url, ...props }) {
  const [config, setConfig] = useState(null);

  function fetchData() {
    // eslint-disable-next-line no-undef
    fetch(url)
      .then((response) => response.json())
      .then((response) => setConfig(response))
      .catch((error) => {
        log.error("Error when loading configuration data:", error);
      });
  }

  useEffect(
    () => {
      fetchData();
    },
    // TODO: Feel free to fix this eslint error if you got time
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  if (!config) {
    return null;
  }

  return (
    <ErrorBoundary FallbackComponent={NullComponent}>
      <ConfigTour active steps={config.steps} url={url} {...props} />
    </ErrorBoundary>
  );
}

RemoteConfigurationTour.propTypes = {
  url: PropTypes.string,
};

export default RemoteConfigurationTour;
