// :copyright: Copyright (c) 2018 ftrack

import { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import withHandlers from "recompose/withHandlers";
import { defineMessages, FormattedMessage, intlShape } from "react-intl";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import FontIcon from "react-toolbox/lib/font_icon";

import Breadcrumbs from "../breadcrumbs";
import safeInjectIntl from "../util/hoc/safe_inject_intl";
import VersionNumber from "../formatter/version_number";
import FileTypeIcon from "../file_type_icon";

import MediaPropType from "./media_prop_type";
import ShowMoreVersionsMenu from "./show_more_versions_menu";
import style from "./style.scss";

const messages = defineMessages({
  "media-type-tooltip": {
    id: "ftrack-spark-components.playlist.media-type-tooltip",
    description: "Tooltip for various media types.",
    defaultMessage: `{mediaType, select,
            video {Video}
            image {Image}
            pdf {PDF}
            other {Other}
        }`,
  },
  "stacked-versions-tooltip": {
    id: "ftrack-spark-components.playlist.stacked-versions-tooltip",
    description: "Tooltip for stacked versions icon",
    defaultMessage: "{number} versions",
  },
  "empty-playlist-message": {
    id: "ftrack-spark-components.playlist.empty-playlist-message",
    description: "Message to show in playlist when it is empty",
    defaultMessage: "No media added yet",
  },
});

/** Overlay text for playlist item */
function PlaylistItemOverlayText({ className, variant, tooltip, ...props }) {
  const classes = classNames(style.playlistItemOverlayText, className, {
    [style.playlistItemOverlayTitle]: variant === "title",
    [style.playlistItemOverlaySubtitle]: variant === "subtitle",
    [style.playlistItemOverlayPrimary]: variant === "primary",
  });

  if (variant === "title") {
    return <p title={tooltip} className={classes} {...props} />;
  }
  return <p className={classes} {...props} />;
}

PlaylistItemOverlayText.propTypes = {
  className: PropTypes.string,
  variant: PropTypes.string,
  tooltip: PropTypes.string,
};

/** Approval Status bar for playlist item */
function PlaylistItemStatus({ className, status }) {
  const classes = classNames(
    style.playListItemOverlayAssetVersionStatus,
    className,
    {
      [style.playlistItemStatusApproved]: status.status === "approved",
      [style.playlistItemStatusRequireChanges]:
        status.status === "require_changes",
    }
  );
  return <div className={classes} />;
}

PlaylistItemStatus.propTypes = {
  className: PropTypes.string,
  status: PropTypes.string,
};

/** Asset status bar for playlist item */
function PlaylistItemAssetStatus({ status }) {
  const classes = classNames(style.playListItemOverlayAssetVersionStatus);
  return (
    <div
      className={classes}
      style={{
        backgroundColor: status.color,
      }}
    />
  );
}

PlaylistItemAssetStatus.propTypes = {
  status: PropTypes.shape({
    color: PropTypes.string.isRequired,
  }),
};

function isApprovalStatus(status) {
  return status.status === "approved" || status.status === "require_changes";
}

/**
 * Generic playlist item
 */
class PlaylistItem extends Component {
  constructor(props) {
    super(props);
    this.state = { menuOpen: false };
  }

  componentDidMount() {
    if (this.props.active) {
      this.scrollIntoView();
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.active && !prevProps.active) {
      this.scrollIntoView();
    }
  }

  scrollIntoView() {
    if (this.node && this.node.scrollIntoView) {
      this.node.scrollIntoView({
        block: "center",
        inline: "center",
        behavior: "smooth",
      });
    }
  }

  render() {
    const {
      className,
      thumbnailUrl,
      fileType,
      encoding,
      children,
      extraChildren,
      active,
      disabled,
      versionSelected,
      assetId,
      currentVersion,
      versions,
      status,
      onItemClick,
      provided,
      ...props
    } = this.props;
    const classes = classNames(style.playlistItem, className, {
      [style.playlistItemActive]: active,
      [style.playlistItemMenuOpen]: this.state.menuOpen,
      [style.playlistItemVersionSelected]: versionSelected,
    });
    return (
      <div
        className={style.playlistItemContainer}
        ref={provided ? provided.innerRef : null}
        {...props}
      >
        <div
          ref={(node) => {
            this.node = node;
          }}
          className={classes}
        >
          <div
            className={style.playlistItemThumbnail}
            style={
              !disabled && thumbnailUrl
                ? { backgroundImage: `url('${thumbnailUrl}')` }
                : null
            }
          />
          {disabled || !thumbnailUrl ? (
            <FileTypeIcon
              className={style.playlistItemFileIcon}
              variant="playlist"
              label={fileType}
              loading={encoding}
            />
          ) : null}
          {(versionSelected || active) && (
            <span className={style["active-indicator"]} />
          )}
          <div className={style.playlistItemBackground} />
          <div className={style.playlistItemOverlay}>{children}</div>
          {extraChildren}
          {versions.length > 1 && (
            <ShowMoreVersionsMenu
              assetId={assetId}
              currentVersion={currentVersion}
              versions={versions}
              selectVersion={(version) => onItemClick(version)}
              menuOpen={this.state.menuOpen}
              setMenuOpen={(menuOpen) => this.setState({ menuOpen })}
            />
          )}
          {status && isApprovalStatus(status) && (
            <FontIcon
              data-ignoreonclick={assetId}
              value={status.status === "approved" ? "check" : "clear"}
              className={classNames(style["approval-icon"], {
                [style.approved]: status.status === "approved",
                [style.requiresChanges]: status.status === "require_changes",
              })}
            />
          )}
        </div>
      </div>
    );
  }
}

PlaylistItem.propTypes = {
  className: PropTypes.string,
  thumbnailUrl: PropTypes.string,
  fileType: PropTypes.string,
  encoding: PropTypes.bool,
  children: PropTypes.node,
  extraChildren: PropTypes.node,
  assetId: PropTypes.string,
  versionSelected: PropTypes.bool,
  currentVersion: PropTypes.string,
  versions: PropTypes.arrayOf(PropTypes.objectOf(MediaPropType)),
  status: PropTypes.shape({
    status: PropTypes.string.isRequired,
  }),
  active: PropTypes.bool,
  disabled: PropTypes.bool,
  object: MediaPropType,
  onItemClick: PropTypes.func,
  provided: PropTypes.object,
};

/**
 * Review session object playlist item.
 */
function MediaObjectPlaylistItemBase({
  object,
  playingMedia,
  versionSelected,
  ...props
}) {
  const { assetId, versions } = object;

  const {
    id: objectId,
    thumbnailUrl,
    fileType,
    encoding,
    status,
    link,
    name,
    version,
    assetVersion,
  } = playingMedia && playingMedia.assetId === assetId ? playingMedia : object;

  return (
    <PlaylistItem
      assetId={assetId}
      thumbnailUrl={thumbnailUrl}
      fileType={fileType}
      encoding={encoding}
      currentVersion={objectId}
      versions={versions || []}
      status={status}
      versionSelected={versionSelected}
      {...props}
    >
      {link && (
        <PlaylistItemOverlayText variant="subtitle">
          <Breadcrumbs
            truncation="tooltip"
            items={link.map(({ id, name: title }) => ({
              id,
              title,
              disabled: true,
            }))}
          />
        </PlaylistItemOverlayText>
      )}
      <PlaylistItemOverlayText variant="title" tooltip={name}>
        {name}
      </PlaylistItemOverlayText>
      <PlaylistItemOverlayText variant="primary">
        <VersionNumber version={version} />
        {versions && versions.length > 1 && (
          <span className={style.versionCount}>{` (${versions.length})`}</span>
        )}
      </PlaylistItemOverlayText>
      {assetVersion && assetVersion.status && (
        <PlaylistItemAssetStatus status={assetVersion.status} />
      )}
    </PlaylistItem>
  );
}

MediaObjectPlaylistItemBase.propTypes = {
  object: MediaPropType,
  playingMedia: MediaPropType,
  intl: intlShape.isRequired,
  versionSelected: PropTypes.bool,
};

export const MediaObjectPlaylistItem = withHandlers({
  onClick: (props) => (event) => {
    if (!props.versionSelected && !event.defaultPrevented) {
      props.onClick(props.object);
    }
  },
})(safeInjectIntl(MediaObjectPlaylistItemBase));

MediaObjectPlaylistItem.defaultProps = {
  disabled: false,
  versionSelected: false,
};

function DraggablePlaylistItem({
  media,
  index,
  activeItem,
  onClick,
  activeSequenceIds,
  canSort,
  ...props
}) {
  return (
    <Draggable isDragDisabled={!canSort} draggableId={media.id} index={index}>
      {(provided, snapshot) => (
        <MediaObjectPlaylistItem
          key={media.id}
          object={media}
          disabled={!media.playable}
          active={media.id === activeItem?.id}
          className={classNames({
            [style.notPlayableInSequence]:
              activeSequenceIds && !activeSequenceIds.includes(media.id),
          })}
          dragging={snapshot.dragging}
          onClick={onClick}
          provided={provided}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          {...props}
        />
      )}
    </Draggable>
  );
}

DraggablePlaylistItem.propTypes = {
  media: PropTypes.object,
  activeItem: PropTypes.object,
  onClick: PropTypes.func,
  index: PropTypes.number,
  activeSequenceIds: PropTypes.arrayOf(PropTypes.string),
  canSort: PropTypes.bool,
};

/**
 * Playlist, showing scrollable list of thumbnails.
 *
 * Currently only supports Review Session Objects
 */
const Playlist = ({
  className,
  items,
  activeItem,
  activeSequenceIds,
  onItemClick,
  canSort,
  onDragEnd,
  playingMedia,
  ...props
}) => {
  const classes = classNames(style.playlist, className);
  const playlist = !items.length ? (
    <div className={style.playlistEmpty}>
      <FormattedMessage {...messages["empty-playlist-message"]} />
    </div>
  ) : (
    <div className={classes} {...props}>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable
          direction="horizontal"
          droppableId="horizontal-playlist"
          isDropDisabled={!canSort}
        >
          {(provided) => (
            <div ref={provided.innerRef} {...provided.droppableProps}>
              {items.map((item, index) => (
                <DraggablePlaylistItem
                  key={item.id}
                  media={item}
                  index={index}
                  activeItem={activeItem}
                  activeSequenceIds={activeSequenceIds}
                  onClick={(event) => {
                    if (!event.defaultPrevented) {
                      onItemClick(event);
                    }
                  }}
                  onItemClick={onItemClick}
                  canSort={canSort}
                  playingMedia={playingMedia}
                />
              ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
    </div>
  );
  return playlist;
};

Playlist.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      playable: PropTypes.bool,
    })
  ),
  className: PropTypes.string,
  activeItem: PropTypes.object, // eslint-disable-line
  onItemClick: PropTypes.func,
  playingMedia: PropTypes.shape({
    id: PropTypes.string,
  }),
  intl: intlShape.isRequired,
  activeSequenceIds: PropTypes.arrayOf(PropTypes.string),
  onDragEnd: PropTypes.func,
  canSort: PropTypes.bool,
};

Playlist.defaultProps = {
  onDragEnd: () => {},
  canSort: false,
};

export default safeInjectIntl(Playlist);
