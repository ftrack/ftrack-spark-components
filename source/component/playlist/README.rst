..
    :copyright: Copyright (c) 2018 ftrack

########
Playlist
########

Playlist component, showing scrollable list of thumbnails.

Currently only supports Review Session Objects.

================== =========== ======================================
Prop               Default     Description
================== =========== ======================================
items                          List of review session objects.
activeItem                     Currently selected item.
onItemClick                    Callback when an item is selected.
