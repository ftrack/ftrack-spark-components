// :copyright: Copyright (c) 2018 ftrack
import { Component, useEffect, useState } from "react";
import centered from "@storybook/addon-centered/react";
import compose from "recompose/compose";
import withState from "recompose/withState";

import Playlist from ".";
import getSession from "../../story/get_session";
import { SparkProvider, withSession } from "../util/hoc";
import { ASSET_VERSION_MEDIA_ATTRIBUTES } from "../util/get_media_component";

export default {
  title: "Playlist",
  decorators: [
    (Story) => (
      <div style={{ padding: "32px", maxWidth: "100%" }}>{<Story />}</div>
    ),
    centered,
    (Story) => (
      <SparkProvider session={getSession()}>
        <Story />
      </SparkProvider>
    ),
  ],
};

function getFirstInviteeId(data) {
  for (const item of data) {
    for (const status of item.statuses) {
      if (status && status.review_session_invitee_id) {
        return status.review_session_invitee_id;
      }
    }
  }

  return null;
}

function setStatusForInvitee(item, inviteeId) {
  let status = null;
  for (const candidate of item.statuses) {
    if (candidate.review_session_invitee_id === inviteeId) {
      status = candidate;
      break;
    }
  }
  item.status = status;
  return item;
}

class ReviewSessionObjectPlaylistTestBase extends Component {
  constructor() {
    super();
    this.state = { items: [] };
  }

  componentDidMount() {
    const attributes = [
      "id",
      "asset_version.thumbnail_url",
      "asset_version.components",
      "asset_version.components.metadata",
      "name",
      "description",
      "version",
      "statuses.review_session_invitee_id",
      "statuses.status",
    ];
    this.props.session
      .query(`select ${attributes.join(",")} from ReviewSessionObject limit 50`)
      .then((response) => {
        const inviteeId = getFirstInviteeId(response.data);
        const items = response.data.map((item) =>
          setStatusForInvitee(item, inviteeId)
        );

        // Randomly mark components as playable.
        items.forEach((item) => {
          item.media = {
            id: "component-id",
            playable: Math.random() > 0.5,
          };
        });
        this.setState({ items });
      });
  }

  render() {
    const { session, ...props } = this.props;
    return <Playlist items={this.state.items} {...props} />;
  }
}

const ReviewSessionObjectPlaylistTest = compose(
  withState("activeItem", "onItemClick", ""),
  (BaseComponent) => withSession(BaseComponent)
)(ReviewSessionObjectPlaylistTestBase);

export const ReviewPlaylist = () => <ReviewSessionObjectPlaylistTest />;

const StudioReviewPlaylistTestBase = (props) => {
  const [items, setItems] = useState([]);

  useEffect(
    () => {
      const mediaProjection = [
        "asset.id",
        "asset.name",
        "link",
        "asset.type.short",
        "status.name",
        "status.color",
        "version",
        ...ASSET_VERSION_MEDIA_ATTRIBUTES,
      ].join(",");

      props.session
        .query(`select ${mediaProjection} from AssetVersion limit 50`)
        .then((response) => {
          // Randomly mark components as playable.
          const playableItems = response.data.map((item) => ({
            ...item,
            media: {
              id: "component-id",
              playable: Math.random() > 0.5,
            },
          }));
          setItems(playableItems);
        });
    },
    // TODO: Feel free to fix this eslint error if you got time
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return <Playlist items={items} {...props} />;
};

const StudioReviewPlaylistTest = compose(
  withState("activeItem", "onItemClick", ""),
  (BaseComponent) => withSession(BaseComponent)
)(StudioReviewPlaylistTestBase);

export const StudioPlaylist = () => <StudioReviewPlaylistTest />;
