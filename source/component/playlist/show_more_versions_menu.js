// :copyright: Copyright (c) 2018 ftrack

import { useRef } from "react";
import PropTypes from "prop-types";
import { Popper } from "@mui/material";

import FontIcon from "react-toolbox/lib/font_icon";
import useOnClickOutside from "use-onclickoutside";

import { MediaObjectPlaylistItem } from "./component";
import MediaPropType from "./media_prop_type";

import style from "./style.scss";

/** Show more menu for version selector in playlist */
const ShowMoreVersionsMenu = ({
  assetId,
  currentVersion,
  versions,
  selectVersion,
  menuOpen,
  setMenuOpen,
}) => {
  const menuRef = useRef(null);

  useOnClickOutside(menuRef, (e) => {
    const closestIgnore = e.target.closest("[data-ignoreonclick]");
    const ignoreonclick =
      closestIgnore &&
      closestIgnore.getAttribute("data-ignoreonclick") === assetId;
    if (!ignoreonclick) {
      setMenuOpen(false);
    }
  });

  return (
    <div
      data-ignoreonclick={assetId}
      ref={menuRef}
      className={style["show-more-container"]}
      onClick={(e) => {
        e.stopPropagation();
        setMenuOpen(!menuOpen);
      }}
    >
      {!menuOpen && (
        <FontIcon
          data-ignoreonclick={assetId}
          value={"expand_less"}
          className={style["show-more-button"]}
        />
      )}
      {menuRef.current && (
        <Popper
          open={menuOpen}
          anchorEl={menuRef.current.parentNode}
          placement="top-start"
          popperOptions={{
            modifiers: {
              preventOverflow: {
                padding: { left: 0, right: 0 },
                boundariesElement: "window",
              },
            },
          }}
        >
          <div data-ignoreonclick={assetId}>
            <div
              className={style["show-more-menu"]}
              style={{
                overflowY: versions.length > 5 ? "scroll" : "inherit",
              }}
            >
              {versions.map((version) => (
                <MediaObjectPlaylistItem
                  className={style["show-more-item"]}
                  key={version.id}
                  object={version}
                  disabled={!version.playable}
                  versionSelected={version.id === currentVersion}
                  onClick={() => selectVersion(version)}
                />
              ))}
            </div>
            <FontIcon
              data-ignoreonclick={assetId}
              value={"expand_more"}
              className={style["show-more-button"]}
              style={{
                right: versions.length > 5 ? "1.8rem" : "0.8rem",
              }}
            />
          </div>
        </Popper>
      )}
    </div>
  );
};

ShowMoreVersionsMenu.propTypes = {
  assetId: PropTypes.string,
  currentVersion: PropTypes.string,
  versions: PropTypes.arrayOf(MediaPropType),
  selectVersion: PropTypes.func,
  menuOpen: PropTypes.bool,
  setMenuOpen: PropTypes.func,
};

export default ShowMoreVersionsMenu;
