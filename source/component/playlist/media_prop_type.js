// :copyright: Copyright (c) 2021 ftrack

import PropTypes from "prop-types";

const MediaPropType = PropTypes.shape({
  id: PropTypes.string,
  name: PropTypes.string,
  thumbnailUrl: PropTypes.string,
  assetVersion: PropTypes.shape({
    status: PropTypes.shape({
      color: PropTypes.string,
    }),
  }),
  fileType: PropTypes.string,
  encoding: PropTypes.bool,
  status: PropTypes.shape({
    status: PropTypes.string,
  }),
  description: PropTypes.string,
  version: PropTypes.string,
});

export default MediaPropType;
