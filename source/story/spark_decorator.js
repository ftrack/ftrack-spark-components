import { Component } from "react";
import { Provider as ReduxProvider } from "react-redux";
import configureStore from "./configure_store.js";
import getSession from "./get_session";

import { SparkProvider } from "../component/util/hoc";

export default class Provider extends Component {
  constructor(props) {
    super(props);

    this.state = { sessionReady: false };
    this.session = getSession();
    this.store = configureStore((state = {}) => state, [], this.session);
  }

  componentDidMount() {
    this.session.initializing.then(() => this.setState({ sessionReady: true }));
  }

  render() {
    const { story } = this.props;
    const { sessionReady } = this.state;
    if (!sessionReady) {
      return null;
    }
    return (
      <SparkProvider session={this.session}>
        <ReduxProvider store={this.store}>{story}</ReduxProvider>
      </SparkProvider>
    );
  }
}
