// :copyright: Copyright (c) 2017 ftrack
function getFixedSizeDecorator(options = {}) {
  return function fixedSize(storyFn) {
    const style = Object.assign(
      {
        width: "400px",
        height: "400px",
        border: "1px solid rgba(0, 0, 0, 0.1)",
        padding: "1.6rem",
        overflowY: "auto",
      },
      options
    );

    return <div style={style}>{storyFn()}</div>;
  };
}

export default getFixedSizeDecorator;
