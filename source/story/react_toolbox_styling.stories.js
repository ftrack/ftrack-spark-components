// :copyright: Copyright (c) 2017 ftrack
import centered from "@storybook/addon-centered/react";
import { withState, withHandlers, compose } from "recompose";
import { Button, IconButton } from "react-toolbox/lib/button";
import {
  Card,
  CardMedia,
  CardTitle,
  CardText,
  CardActions,
} from "react-toolbox/lib/card";
import Input from "react-toolbox/lib/input";
import DatePicker from "react-toolbox/lib/date_picker";
import Checkbox from "react-toolbox/lib/checkbox";
import { Tab, Tabs } from "react-toolbox";
import gridBackgroundDecorator, {
  GridBackground,
} from "./grid_background_decorator";

export default {
  title: "React toolbox styling",
  decorators: [gridBackgroundDecorator, centered],
};

const GithubIcon = () => (
  <svg viewBox="0 0 284 277">
    <g>
      <path d="M141.888675,0.0234927555 C63.5359948,0.0234927555 0,63.5477395 0,141.912168 C0,204.6023 40.6554239,257.788232 97.0321356,276.549924 C104.12328,277.86336 106.726656,273.471926 106.726656,269.724287 C106.726656,266.340838 106.595077,255.16371 106.533987,243.307542 C67.0604204,251.890693 58.7310279,226.56652 58.7310279,226.56652 C52.2766299,210.166193 42.9768456,205.805304 42.9768456,205.805304 C30.1032937,196.998939 43.9472374,197.17986 43.9472374,197.17986 C58.1953153,198.180797 65.6976425,211.801527 65.6976425,211.801527 C78.35268,233.493192 98.8906827,227.222064 106.987463,223.596605 C108.260955,214.426049 111.938106,208.166669 115.995895,204.623447 C84.4804813,201.035582 51.3508808,188.869264 51.3508808,134.501475 C51.3508808,119.01045 56.8936274,106.353063 65.9701981,96.4165325 C64.4969882,92.842765 59.6403297,78.411417 67.3447241,58.8673023 C67.3447241,58.8673023 79.2596322,55.0538738 106.374213,73.4114319 C117.692318,70.2676443 129.83044,68.6910512 141.888675,68.63701 C153.94691,68.6910512 166.09443,70.2676443 177.433682,73.4114319 C204.515368,55.0538738 216.413829,58.8673023 216.413829,58.8673023 C224.13702,78.411417 219.278012,92.842765 217.804802,96.4165325 C226.902519,106.353063 232.407672,119.01045 232.407672,134.501475 C232.407672,188.998493 199.214632,200.997988 167.619331,204.510665 C172.708602,208.913848 177.243363,217.54869 177.243363,230.786433 C177.243363,249.771339 177.078889,265.050898 177.078889,269.724287 C177.078889,273.500121 179.632923,277.92445 186.825101,276.531127 C243.171268,257.748288 283.775,204.581154 283.775,141.912168 C283.775,63.5477395 220.248404,0.0234927555 141.888675,0.0234927555" />
    </g>
  </svg>
);

export const _Button = () => (
  <div>
    <div>
      <Button href="http://github.com/javivelasco" target="_blank" raised>
        <GithubIcon /> Github
      </Button>
      <Button icon="bookmark" label="Bookmark" accent />
      <Button icon="bookmark" label="Bookmark" raised primary />
      <Button icon="inbox" label="Inbox" flat />
      <Button icon="add" floating />
      <Button icon="add" floating accent mini />
      <IconButton icon="favorite" accent />
      <IconButton icon={<GithubIcon />} accent />
      <IconButton primary>
        <GithubIcon />
      </IconButton>
      <Button icon="add" label="Add this" flat primary />
      <Button icon="add" label="Add this" flat disabled />
    </div>
  </div>
);

const CheckboxWithState = compose(
  withState("isChecked", "handleClick", false),
  withHandlers({
    handleClick: (props) => () => {
      props.handleClick(!props.isChecked);
    },
  })
)(({ isChecked, handleClick }) => (
  <Checkbox checked={isChecked} onChange={handleClick} />
));

export const _Checkbox = () => (
  <div>
    <CheckboxWithState />
  </div>
);

const dummyText =
  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";

export const _Card = () => (
  <Card style={{ width: "350px" }}>
    <CardTitle
      avatar="https://placeimg.com/80/80/animals"
      title="Avatar style title"
      subtitle="Subtitle here"
    />
    <CardMedia aspectRatio="wide" image="https://placeimg.com/800/450/nature" />
    <CardTitle title="Title goes here" subtitle="Subtitle here" />
    <CardText>{dummyText}</CardText>
    <CardActions>
      <Button label="Action 1" />
      <Button label="Action 2" />
    </CardActions>
  </Card>
);

export const _Input = () => (
  <section>
    <Input type="text" label="Name" name="name" maxLength={16} />
    <Input type="text" hint="With Hint, no label" name="name" maxLength={16} />
    <Input type="text" label="Disabled field" disabled />
    <Input type="text" multiline label="Multiline" maxLength={20} />
    <Input type="email" label="Email address" icon="email" />
    <Input type="tel" label="Phone" name="phone" icon="phone" />
    <Input
      type="text"
      label="Required Field"
      hint="With Hint"
      required
      icon="share"
    />
    <Input
      type="text"
      label="error"
      error={
        <span>
          Error!!{" "}
          <a
            href="#!"
            onClick={(e) => {
              e.preventDefault();
            }}
          >
            ?
          </a>
        </span>
      }
    />
  </section>
);

export const _DatePicker = () => <DatePicker label="Birthdate" />;

_DatePicker.storyName = "DatePicker";

export const _Tabs = () => (
  <section>
    <Tabs index={0}>
      <Tab label="Primary">
        <GridBackground>Primary content</GridBackground>
      </Tab>
      <Tab label="Secondary" />
      <Tab label="Third" disabled>
        <small>Disabled content</small>
      </Tab>
      <Tab label="Fourth" hidden>
        <small>Fourth content hidden</small>
      </Tab>
      <Tab label="Fifth">
        <small>Fifth content</small>
      </Tab>
    </Tabs>
  </section>
);
