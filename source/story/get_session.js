// :copyright: Copyright (c) 2017 ftrack

import { Session } from "ftrack-javascript-api";

let session = null;

export default function getSession() {
  if (!session) {
    session = new Session(
      process.env.STORYBOOK_FTRACK_SERVER,
      process.env.STORYBOOK_FTRACK_API_USER,
      process.env.STORYBOOK_FTRACK_API_KEY
    );
  }

  return session;
}
