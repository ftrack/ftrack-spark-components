// :copyright: Copyright (c) 2016 ftrack

import { compose } from "redux";
import { applyMiddleware } from "redux-subspace";
import createSagaMiddleware from "redux-subspace-saga";

import { createStore } from "redux-dynamic-reducer";

export default function configureStore(
  rootReducer,
  sagas,
  session,
  initialState = {}
) {
  // Compose redux middleware
  const middleware = [];

  const sagaMiddleware = createSagaMiddleware({
    context: {
      ftrackSession: session,
    },
  });

  middleware.push(sagaMiddleware);

  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(...middleware))
  );
  sagas.map((saga) => sagaMiddleware.run(saga));
  store.runSaga = sagaMiddleware.run;

  // if (module.hot) {
  //     // Enable Webpack hot module replacement for reducers
  //     module.hot.accept('./reducer/root', () => {
  //         // eslint-disable-next-line global-require
  //         const nextRootReducer = require('./reducer/root').default;

  //         store.replaceReducer(nextRootReducer);
  //     });
  // }

  return store;
}
