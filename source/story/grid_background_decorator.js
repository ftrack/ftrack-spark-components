// :copyright: Copyright (c) 2018 ftrack
export function GridBackground({ children }) {
  const styles = {
    backgroundSize: "1.6rem 1.6rem",
    backgroundImage:
      "linear-gradient(to right, lightblue 1px, transparent 1px), linear-gradient(to bottom, lightblue 1px, transparent 1px)",
  };

  return <div style={styles}>{children}</div>;
}

function gridBackgroundDecorator(storyFn) {
  return <GridBackground>{storyFn()}</GridBackground>;
}

export default gridBackgroundDecorator;
