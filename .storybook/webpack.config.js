// :copyright: Copyright (c) 2019 ftrack
const path = require("path");
const themeName = process.env.FTRACK_THEME === "dark" ? "dark" : "light";
const themeFile = path.resolve(
  __dirname,
  "..",
  "source",
  "style",
  themeName,
  "theme.scss"
);
const nodeModulesPath = path.resolve(__dirname, "..", "node_modules");

module.exports = ({ config, mode }) => {
  // Load scss files using sass loaded and inject theme variables.
  config.module.rules.push({
    test: /(\.scss)$/,
    loaders: [
      { loader: "style-loader" },
      {
        loader: "css-loader",
        options: {
          modules: true,
          sourceMap: true,
          importLoaders: 1,
          localIdentName: "[name]--[local]--[hash:base64:8]",
        },
      },
      {
        loader: "sass-loader",
        options: {
          data: `@import "${themeFile}";`,
        },
      },
    ],
  });

  // Use ftrack's fork of react-toolbox.
  config.resolve.alias["react-toolbox"] = "@ftrack/react-toolbox";

  // Resolve sass extensions automatically
  config.resolve.extensions.push(".scss");

  // Resolve import from node modules (used by react toolbox)
  config.resolve.modules.push(nodeModulesPath);

  return config;
};
