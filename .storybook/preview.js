// :copyright: Copyright (c) 2021 ftrack
import CssBaseline from "@mui/material/CssBaseline";
import { StyledEngineProvider, ThemeProvider } from "@mui/material/styles";
import Container from "@mui/material/Container";
import log from "loglevel";
import { lightTheme, darkTheme } from "../source/style/mui_theme";

// Import global CSS.
import "../source/style/commons.scss";
import "../source/component/selector/style/index.scss";

// Set loglevel to show everything
log.setLevel("DEBUG");

/** Return if darkMode should be enabled by default. */
function getDefaultDarkMode() {
  return process.env.STORYBOOK_FTRACK_THEME === "dark";
}

export const globalTypes = {
  darkMode: getDefaultDarkMode(),
};

const withMuiTheme = (Story, context) => {
  let darkMode = context.globals.darkMode;
  if (typeof darkMode === "undefined") {
    darkMode = getDefaultDarkMode();
  }
  const theme = darkMode ? darkTheme : lightTheme;

  // Expose theme as a global object to be able to inspect in console.
  window.theme = theme;

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Container sx={{ overflow: "auto", height: "100vh" }}>
          <Story />
        </Container>
      </ThemeProvider>
    </StyledEngineProvider>
  );
};

export const decorators = [withMuiTheme];
