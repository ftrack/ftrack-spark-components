// :copyright: Copyright (c) 2021 ftrack

module.exports = {
  stories: ["../source/**/*.stories.js"],
  addons: [
    "@storybook/addon-actions",
    "@storybook/addon-controls",
    "storybook-tailwind-dark-mode",
  ],
  staticDirs: ["../source/static"],
  webpackFinal: async (config) => {
    return {
      ...config,
      resolve: {
        ...config.resolve,
        alias: {
          ...config.resolve.alias,
          // This forces Storybook to use latest versions of @emotion/react, ensuring the theme gets picked up
          // correctly. More info: https://github.com/mui-org/material-ui/issues/24282#issuecomment-796755133
          "@emotion/core": require.resolve("@emotion/react"),
          "emotion-theming": require.resolve("@emotion/react"),
          "@emotion/styled": require.resolve("@emotion/styled"),
        },
      },
    };
  },
};
